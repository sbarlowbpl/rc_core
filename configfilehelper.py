"""
Config File Helper Module

This module brings multiple config files into a CSV file
"""
import os

from RC_Core import loadsave, RS1_hardware
from RC_Core.RS1_hardware import PacketID
from RC_Core.packetid import Packets
import csv

exclude_packets = [PacketID.AUTO_LIMIT_CURRENT_DEMAND,
                   PacketID.BOOTLOADER,
                   PacketID.BOOTLOADER_BATCH,
                   PacketID.BOOTLOADER_STM,
                   PacketID.BOOT_BATCH_REPORT,
                   # PacketID.CHANGE_PAGE,
                   PacketID.CURRENT,
                   PacketID.CURRENT_DEMAND_DIRECT,
                   PacketID.DEFAULT_OPERATING_MODE,
                   PacketID.DEVICE_ID,
                   PacketID.DEVICE_ID_FOR_SERIAL_NUMBER,
                   # PacketID.ENCODER,
                   # PacketID.ICMU_PARAMETERS,
                   PacketID.ETH_IP_ADDRESS,
                   PacketID.ETH_PORT,
                   PacketID.EXPECTED_DEVICES,
                   PacketID.FACTORY_CLIMATE,
                   PacketID.FORMAT,
                   PacketID.HARDWARE_STATUS,
                   PacketID.HEARTBEAT_SET,
                   PacketID.ICMU_INNER_PARAMETERS,
                   PacketID.ICMU_INNER_WRITE_REGISTER,
                   PacketID.INDEXED_POSITION,
                   PacketID.INTERNAL_HUMIDITY,
                   PacketID.INTERNAL_PRESSURE,
                   PacketID.INTERNAL_TEMPERATURE,
                   PacketID.KM_BOX_OBSTACLE_00,
                   PacketID.KM_BOX_OBSTACLE_01,
                   PacketID.KM_BOX_OBSTACLE_02,
                   PacketID.KM_BOX_OBSTACLE_03,
                   PacketID.KM_BOX_OBSTACLE_04,
                   PacketID.KM_BOX_OBSTACLE_05,
                   PacketID.KM_CONFIGURATION,
                   PacketID.KM_CYLINDER_OBSTACLE_00,
                   PacketID.KM_CYLINDER_OBSTACLE_01,
                   PacketID.KM_CYLINDER_OBSTACLE_02,
                   PacketID.KM_CYLINDER_OBSTACLE_03,
                   PacketID.KM_CYLINDER_OBSTACLE_04,
                   PacketID.KM_CYLINDER_OBSTACLE_05,
                   PacketID.KM_END_POS,
                   # PacketID.KM_END_POS_LOCAL,
                   PacketID.KM_END_VEL,
                   PacketID.KM_END_VEL_LOCAL,
                   PacketID.KM_FLOAT_PARAMETERS,
                   PacketID.KM_JOINT_STATE_REQUEST,
                   PacketID.KM_MOUNT_POS_ROT,
                   PacketID.KM_POS_GAINS_ROTATE,
                   PacketID.KM_POS_GAINS_TRANSLATE,
                   PacketID.KM_POS_LIMIT_PITCH,
                   PacketID.KM_POS_LIMIT_ROLL,
                   PacketID.KM_POS_LIMIT_TRANSLATE,
                   PacketID.KM_POS_LIMIT_YAW,
                   PacketID.KM_VEL_GAINS_ROTATE,
                   PacketID.KM_VEL_GAINS_TRANSLATE,
                   PacketID.KM_VEL_LIMIT_ROTATE,
                   PacketID.KM_VEL_LIMIT_TRANSLATE,
                   PacketID.LED_INDICATOR_DEMAND,
                   PacketID.LOAD,
                   PacketID.MASTER_ARM_PREFIX,
                   PacketID.MODE,
                   PacketID.MOTOR_OFFSET_ANGLE,
                   PacketID.OPENLOOP,
                   PacketID.OVERDRIVE_ENABLE,
                   PacketID.OVERDRIVE_PARAMETERS,
                   PacketID.POSITION,
                   PacketID.POSITION_DEMAND_INNER,
                   PacketID.POSITION_VELOCITY_DEMAND,
                   PacketID.POS_PRESET_CAPTURE,
                   PacketID.POS_PRESET_ENABLE_LOCAL,
                   PacketID.POS_PRESET_GO,
                   PacketID.POS_PRESET_NAME_0,
                   PacketID.POS_PRESET_NAME_1,
                   PacketID.POS_PRESET_NAME_2,
                   PacketID.POS_PRESET_NAME_3,
                   PacketID.POS_PRESET_SET_0,
                   PacketID.POS_PRESET_SET_1,
                   PacketID.POS_PRESET_SET_2,
                   PacketID.POS_PRESET_SET_3,
                   PacketID.RC_BASE_EXT_DEVICE_IDS,
                   PacketID.RC_BASE_OPTIONS,
                   PacketID.RC_BASE_POSITION_SCALE,
                   PacketID.RC_BASE_VELOCITY_SCALE,
                   PacketID.RC_JOYSTICK_PARAMETERS,
                   PacketID.RELATIVE_POSITION,
                   PacketID.REQUEST,
                   PacketID.SYSTEM_RESET,
                   PacketID.RUN_TIME,
                   PacketID.SAVE,
                   PacketID.SERIAL_NUMBER,
                   PacketID.SET_DEFAULTS,
                   PacketID.SOFTWARE_VERSION,
                   PacketID.SUPPLY_VOLTAGE,
                   PacketID.TEST_PACKET,
                   PacketID.VELOCITY,
                   PacketID.VELOCITY_DEMAND_INNER,
                   PacketID.VERSION,
                   # PacketID.,
                   # PacketID.,
                   # PacketID.,
                   # PacketID.,
                   # PacketID.,
                   # PacketID.,
                   # PacketID.,
                   # PacketID.,
                   ]

CONFIG_FILE_LIST = ["configfiles/RS2_Axis_A.json",
                    "configfiles/RS2_Axis_C.json",
                    "configfiles/RS2_Axis_F.json",
                    "configfiles/largeMotor_8ohm.json",
                    "configfiles/config_Med_8mOhm_Motor.json",
                    "configfiles/config_Small_B_8mOhm_Motor.json"]

HEADER_ROW = ["Packet Name", "Packet ID", "Parameter"]
CSV_FILENAME = 'configmaster.csv'

def merge_configs_into_csv():
    packets = Packets()
    all_packet_ids = RS1_hardware.get_list_of_packet_ids()
    target_packets = [packets.get_by_id(id)
                      for id in all_packet_ids
                      if id not in exclude_packets]

    with open(CSV_FILENAME, 'w', newline='') as csvfile:
        f = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        file_list = CONFIG_FILE_LIST
        configs = [get_config_list_from_config_file(file_name)
                   for file_name in file_list]

        header_config_names = [file_name.split('/')[-1]
                               for file_name in CONFIG_FILE_LIST]
        header = HEADER_ROW + header_config_names
        f.writerow(header)

        for p in target_packets:
            if p is None:
                continue
            for param in p.data_names:
                row = [p.name, hex(p.packet_id), param]

                [row.append(
                    get_value_from_config(conf, p.packet_id, param))
                 for conf in configs]

                f.writerow(row)
                # print(p.name, hex(p.packet_id), param)


def get_config_list_from_config_file(file_path):
    def json_filter_function(json_str):
        try:
            a = json_str.split('"py/seq": ')[1]
            json_str_filtered = a.split('\n}')[0]
        except:
            json_str_filtered = json_str
        return json_str_filtered

    settings_object = loadsave.get_settings_with_filter_from_path(file_path, json_filter_function)
    return settings_object


def get_value_from_config(config_list, packet_id, param_name=''):
    packet_dict_list = [p_d
                   for p_d in config_list
                   if p_d["id"] == packet_id]
    try:
        packet_dict = packet_dict_list[0]
    except Exception as e:
        return None
    # print(packet_dict)
    if packet_dict is None:
        return None
    if param_name is '' or param_name is 'value':
        return packet_dict['values']
    if packet_dict['values'] is None:
        return None
    if param_name not in packet_dict['values']:
        return None
    return packet_dict['values'][param_name]


def split_csv_into_config_files():
    with open(CSV_FILENAME) as csvfile:
        r = csv.reader(csvfile, delimiter=',')
        config_names = []
        config_objects = []
        header_row = []

        prev_packet_id = 0
        for row_index, row in enumerate(r):
            if row_index == 0:
                # header row
                header_row = row
                for i in range(len(HEADER_ROW), len(header_row)):
                    config_names.append(header_row[i])
                    config_objects.append([])
                # print(config_names)
            else:
                # print(row)
                packet_id = int(row[1], 16)
                param_name = row[2]
                for config_idx, value in enumerate(row[len(HEADER_ROW):]):
                    if value is '':
                        value = None
                    if packet_id == prev_packet_id:
                        packet_dict = config_objects[config_idx][-1]
                        if packet_id != packet_dict['id']:
                            raise Exception
                        packet_dict['values'][param_name] = value
                    else:
                        packet_dict = {'id': packet_id,
                                       'name': RS1_hardware.get_name_of_packet_id(packet_id)}
                        if param_name == 'value':
                            packet_dict['values'] = value
                        else:
                            packet_dict['values'] = {param_name: value}
                        config_objects[config_idx].append(packet_dict)

                prev_packet_id = packet_id
    # print(config_objects)
    for conf_index, conf in enumerate(config_objects):
        cwd = os.getcwd()
        file_path = cwd + "/configfileoutput/" + config_names[conf_index]
        loadsave.save_settings_to_path(conf, file_path)


def main():
    merge_configs_into_csv()
    input("Open " + CSV_FILENAME + " in Excel. Edit, save and close, "
                                   "then press enter to generate output "
                                   "files in 'configfileoutput' folder")
    split_csv_into_config_files()


if __name__ == '__main__':
    # config_obj = get_config_list_from_config_file("configfiles/RS2_Axis_A.json")
    # get_value_from_config(config_obj, 9, '')

    # main()
    split_csv_into_config_files()

