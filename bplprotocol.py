import datetime

from serial import Serial

from RC_Core.RS1_hardware import PacketID, test, Mode, Gains, get_hardware_status_string
from RC_Core import RS1_hardware
import socket
from RC_Core.serial_device import SerialDevice
from RC_Core import serial_device_bpss
import time
import threading
from RC_Core.devicemanager.product import ProductAlpha5, RSProduct, ProductRT
from RC_Core.devicemanager import product
try:
    from hudpanels.modules.console import Console
except Exception as e:
    print(__name__, e)


def test_func(obj, value):
    print('bplprotocol.test_func called with value:', value)


BPSS_STRING_LIGHT_ON = b'#A1043D4CCCCDFFFF\r\n'
BPSS_STRING_LIGHT_OFF = b'#A10400000000FFFF\r\n'


class ExpectedPacketIndex:
    EP_DEVICE_ID = 0
    EP_PACKET_ID = EP_DEVICE_ID + 1
    EP_DATA = EP_PACKET_ID + 1
    EP_TIME = EP_DATA + 1
    EP_CALLBACK = EP_TIME + 1

EP_DEVICE_ID = ExpectedPacketIndex.EP_DEVICE_ID
EP_PACKET_ID = ExpectedPacketIndex.EP_PACKET_ID
EP_DATA = ExpectedPacketIndex.EP_DATA
EP_TIME = ExpectedPacketIndex.EP_TIME
EP_CALLBACK = ExpectedPacketIndex.EP_CALLBACK

REQUEST_TIMEOUT = 20

DO_SERIAL_TEST = False
DO_SERIAL_TEST_KM_VEL = False

requests_lost = 0
requests_made = 0
test_begin_time = time.time()


class BplSerial():
    instance = None
    packets_to_parse = []

    motor_data = {}
    deviceIDs = {0: 0x01, 1: 0x02, 2: 0x03, 3: 0x04, 4: 0x05, 5: 0x06, 6: 0x21, 7: 0x22, 8: 0xB0, 9: 0xB1, 10: 0xA1,
                 11: 0x11}
    connection_status = ''
    comport_status = 'closed'
    console_output = []

    connection_ipAddress = '192.168.2.1'
    connection_remoteIPAddress = '192.168.2.2'
    connection_udpPort = 6789

    bpss_device = None
    bpl_device = None

    poll_for_device_info = True
    error_count = 0

    half_duplex = True
    packet_queue = []

    light_on = False

    request_queue = []
    request_register = {'bookmark': {'device_id': 0, 'packet_id': 0},
                        'requests_pending': []}

    waiting_for_reply = False
    receive_timeout = 0.1
    send_queue = []

    velocities = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 0xB0: 0, 'KM_END_VEL': [0, 0, 0], 'KM_END_POS': [0, 0, 0, 0, 0, 0]}
    last_velocities = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 0xB0: 0, 'KM_END_VEL': [0, 0, 0], 'KM_END_POS': [0, 0, 0, 0, 0, 0]}
    request_list = [[1, PacketID.POSITION, PacketID.MODE, PacketID.CURRENT],
                    [2, PacketID.POSITION, PacketID.MODE, PacketID.CURRENT],
                    [3, PacketID.POSITION, PacketID.MODE, PacketID.CURRENT],
                    [4, PacketID.POSITION, PacketID.MODE, PacketID.CURRENT],
                    [5, PacketID.POSITION, PacketID.MODE, PacketID.CURRENT, PacketID.KM_END_POS],
                    # [0xB0, PacketID.POSITION],
                    ]


    run_send_loop = False

    expected_packets = []
    permanent_callbacks = []

    # r5m_instance1 = ProductR5M()
    # r5m_instance2 = ProductR5M(device_ids=[0x11, 0x12, 0x13, 0x14, 0x15, 0x16])
    # rt_instance = ProductRT()

    device_ids_received = []

    master_arm_port_name = ""

    def __init__(self, serial_dev=SerialDevice(), num_motors=10):
        BplSerial.instance = self
        # print('BplSerial __init__ called')
        self.serial_device = serial_dev
        self._init_motors(num_motors)

        self.master_arm_serial = None

    def send_loop(self):

        while self.run_send_loop:
            if self.comport_status is not 'closed':

                self.send_queued_packets()
                self.send_controls()
                for product in RSProduct.instances:

                    self.send_controls()
                    self.send_queued_packets()
                    self.send_controls()

                    for idx, request_list in enumerate(product.heartbeat_packets):
                        device_id = product.device_ids[idx]
                        this_request = []
                        for packet_index, packet_id in enumerate(request_list):
                            this_request.append(packet_id)

                        if this_request != [0] * 10:
                            self.packets_to_parse += self.serial_device.readdata()
                            self.serial_device.sendpacket(device_id, PacketID.REQUEST, bytearray(this_request))

                            if self.half_duplex:
                                start_time = time.time()
                                num_packets_received = 0
                                request_length = len(this_request) - this_request.count(0)
                                while num_packets_received < request_length:
                                    packets = self.serial_device.readdata()
                                    num_packets_received += len(packets)
                                    self.packets_to_parse += packets

                                    if time.time() > start_time + 0.1:
                                        # requests_lost += 1
                                        print("### Missed request from device id:", device_id) #, "Requests lost:",
                                        #       requests_lost, "/", requests_made, "\tMissed:",
                                        #       len(this_request) - num_packets_received, ' / ', len(this_request))
                                        # print("### Average rate of missed requests:",
                                        #       60 * requests_lost / (time.time() - test_begin_time), "/min")
                                        break

                                end_time = time.time()
                                if num_packets_received == request_length:
                                    print("Time to receive all requests from device id:", device_id, "\t\ttime:",
                                          '{:3.3f} seconds'.format(end_time - start_time))

                        if self.half_duplex:
                            self.send_controls()

                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()
                    self.send_controls()

                time.sleep(0.001)
            else:
                time.sleep(0.2)

    def send_queued_packets(self):
        while len(self.send_queue) > 0:
            packet = self.send_queue.pop(0)
            # print(__name__, 'send_queued_packets() packet to send:', packet, 'self.send_queue', self.send_queue)
            if isinstance(packet[EP_DATA], bytearray):
                data_to_recall = list(packet[EP_DATA])
            else:
                data_to_recall = packet[EP_DATA]
            self.serial_device.sendpacket(packet[EP_DEVICE_ID], packet[EP_PACKET_ID], packet[EP_DATA])
            if packet[EP_PACKET_ID] not in [PacketID.REQUEST, PacketID.BOOTLOADER]:
                # if isinstance(packet[2], bytearray):
                #     data_to_recall = []
                #     for b in packet[2]:
                #         data_to_recall.append(b)
                #     self.add_send_data_to_expected_packets(packet[0], packet[1], data_to_recall)
                # else:
                if len(packet) > 3:
                    callback = packet[3]
                else:
                    callback = None
                self.add_send_data_to_expected_packets(packet[EP_DEVICE_ID], packet[EP_PACKET_ID], data_to_recall, time.time(), callback)
                self.send_request_packets(packet[EP_DEVICE_ID], packet[EP_PACKET_ID])
                if self.half_duplex:
                    time.sleep(0.08)
            if self.half_duplex:
                time.sleep(0.08)
            self.packets_to_parse += self.serial_device.readdata()

    def add_send_data_to_expected_packets(self, device_id, packet_id, data, time_of_send, callback=None):
        """
        expected_packets list is
        [[device_id, packet_id, data, time_of_send, callback],
         [device_id, packet_id, data, time_of_send, callback], ...]
        :param int device_id:
        :param int packet:
        :param data:
        :param float time_of_send:
        :param callback: defaults to None
        :return:
        """
        if packet_id != PacketID.REQUEST:
            for packet in self.expected_packets:
                if packet[EP_DEVICE_ID] == device_id and packet[EP_PACKET_ID] == packet_id:
                    self.expected_packets.remove(packet)
            self.expected_packets.append([device_id, packet_id, data, time_of_send, callback])
            print(__name__, "add_send_data_to_expected_packets() device_id", device_id,
                  "packet_id", packet_id, "data", data)
        for item in self.expected_packets:
            if len(item) > EP_TIME and item[EP_TIME]:
                if item[EP_TIME] + REQUEST_TIMEOUT < time.time():
                    if item in self.expected_packets:
                        try:
                            print(__name__, 'add_send_data_to_expected_packets() removing',
                                  item, 'from', self.expected_packets)
                            self.expected_packets.remove(item)
                        except:
                            print(__name__, 'add_send_data_to_expected_packets() ERROR removing',
                                  item, 'from', self.expected_packets)

    def send_controls(self):
        self.packets_to_parse += self.serial_device.readdata()
        if DO_SERIAL_TEST:
            self.send_serial_test()
            return
        for product in RSProduct.instances:
            if hasattr(product, "master_arm"):
                new_state = product.master_arm.do_passthrough(self)

                # Stop the R5M moving back into position/velocity mode when pausing master arm
                if product.master_arm.enabled is False and new_state is True:
                    for i in range(len(product.control_modes)):
                        product.control_modes[i] = 0

                product.master_arm.enabled = new_state
                if product.master_arm.enabled:
                    continue
            for km_mode in [PacketID.KM_END_VEL, PacketID.KM_END_POS]:
                if km_mode in product.control_modes:
                    device_id = product.get_device_id_for_kinematics()
                    packet_id = km_mode
                    tx_data = product.control_values[packet_id]
                    self.serial_device.sendpacket(device_id, packet_id, tx_data)
                    # time.sleep(0.01)
                    if DO_SERIAL_TEST_KM_VEL:
                        self.send_serial_test()
            for i in range(len(product.control_modes)):
                control_mode = product.control_modes[i]
                if control_mode in [PacketID.POSITION, PacketID.CURRENT, PacketID.VELOCITY, PacketID.OPENLOOP]:
                    device_id = product.device_ids[i]
                    packet_id = control_mode
                    tx_data = product.control_values[control_mode][i]
                    if tx_data is not None:
                        self.serial_device.sendpacket(device_id, packet_id, float(tx_data))

            # time.sleep(0.001)
        time.sleep(0.005)

    test_data = 0

    def send_serial_test(self):
        for i in range(4):
            device_id = i + 1
            self.serial_device.sendpacket(device_id, PacketID.TEST_PACKET, self.test_data)
            time.sleep(0.01)
        self.test_data += 1
        if self.test_data > 0xFF:
            self.test_data = 0
        time.sleep(0.01)

    def do_master_arm_passthrough(self):
        if self.master_arm_port_name is '':
            return

        if self.master_arm_serial is None:
            self.master_arm_serial = Serial(baudrate=115200)
            try:
                self.master_arm_serial = Serial(port=self.master_arm_port_name, baudrate=115200)
            except:
                self.master_arm_serial = None
                return
        else:
            if self.master_arm_serial.port != self.master_arm_port_name:
                try:
                    if self.master_arm_serial.is_open:
                        self.master_arm_serial.close()
                    self.master_arm_serial = Serial(port=self.master_arm_port_name, baudrate=115200)
                except:
                    self.master_arm_serial = None
                    return
            if not self.master_arm_serial.is_open:
                try:
                    self.master_arm_serial.open()
                except:
                    self.master_arm_serial = None
        if self.master_arm_serial and self.master_arm_serial.is_open:
            try:
                num_bytes_waiting = self.master_arm_serial.in_waiting

                if num_bytes_waiting:
                    bytes_in = self.master_arm_serial.read(num_bytes_waiting)

                    if bytes_in:
                        if self.serial_device.comsType is "UDP":
                            self.serial_device.udp_socket.sendto(bytes_in, self.serial_device.remoteIPAddress)
                        elif self.serial_device.comsType is 'TCP':
                            with self.serial_device.tcp_lock:
                                self.serial_device.tcp_socket.sendall(bytes_in)
                        elif self.serial_device.serialPort.is_open:
                            self.serial_device.serialPort.write(bytes_in)
            except:
                self.master_arm_serial.close()

    def external_load(self):
        self.update_thread = threading.Thread(target=self.start_refresh)
        self.update_thread.setDaemon(True)
        self.update_thread.start()

        self.begin_send_thread()

    def begin_send_thread(self):
        self.run_send_loop = True
        self.test_thread = threading.Thread(target=self.send_loop)
        self.test_thread.setDaemon(True)
        self.test_thread.start()

    def stop_send_thread(self):
        self.run_send_loop = False
        while self.test_thread.is_alive():
            self.run_send_loop = False
            time.sleep(0.1)
            print(__name__, "stopping send thread")
        return True

    def send_packet(self, device_id, packet_id, data, callback=None):
        if True:

            for packet in self.send_queue:
                if packet[EP_DEVICE_ID] == device_id and packet[EP_PACKET_ID] == packet_id \
                        and packet_id not in [PacketID.REQUEST, PacketID.DEVICE_ID]:
                    self.send_queue.remove(packet)
            if [device_id, packet_id, data] not in self.send_queue or packet_id is PacketID.REQUEST:
                self.send_queue.append([device_id, packet_id, data, callback])
                # print(__name__, 'send_packet() self.send_queue', self.send_queue)

        else:
            with tcp_lock:
                self.serial_device.sendpacket(device_id, packet_id, data)

    def start_refresh(self):
        while True:
            self.updateMotor()
            time.sleep(0.005)

    def _init_motors(self, num_motors=6):
        alphalist = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
        empty_motor = self._init_motor_model()
        for num in range(0, num_motors):
            # print('init_motors num:', num)
            motor_name = alphalist[num]
            self.motor_data[motor_name] = empty_motor.copy()
            self.motor_data[motor_name]['deviceID'] = self.deviceIDs[num]
            # print(self.motor_data[motor_name])
        # print(self.motor_data)

    def _init_motor_model(self):
        empty_motor = {}
        empty_motor['demands'] = {PacketID.VELOCITY: 0}
        empty_motor['history'] = {PacketID.VELOCITY: -1}
        for a in dir(PacketID):
            if not a.startswith('__'):
                pid = getattr(PacketID, a)
                if a is 'CURRENT_LIMIT' or a is 'POSITION_LIMIT' or a is 'VELOCITY_LIMIT':
                    empty_motor[pid] = {'max': 0, 'min': 0}
                elif a is 'ELECTRICAL_VERSION' or a is 'MECHANICAL_VERSION' or a is 'SOFTWARE_VERSION':
                    empty_motor[pid] = [0, 0, 0]
                elif a is 'POSITION_GAIN' \
                        or a is 'VELOCITY_GAIN' \
                        or a is 'CURRENT_GAIN':
                    empty_motor[pid] = {'KP': 0, 'KI': 0, 'KD': 0, 'KF': 0, 'MI': 0}
                elif a is 'SERIAL_NUMBER' or a is 'MODEL_NUMBER' or a is 'VERSION':
                    empty_motor[pid] = -1.0
                elif a is 'MOTOR_PARAMETERS':
                    empty_motor[pid] = {'voltage': 0, 'current': 0, 'resistance': 0, 'direction': 0}
                elif a is 'POSITION_PARAMETERS' or a is 'VELOCITY_PARAMETERS' \
                        or a is 'CURRENT_PARAMETERS' or a is 'INPUT_VOLTAGE_PARAMETERS':
                    empty_motor[pid] = {'scale': 0, 'offset': 0, 'alpha': 0, 'beta': 0}
                elif a is 'KM_END_POS' or a is 'KM_END_VEL':
                    empty_motor[pid] = {'x': 0, 'y': 0, 'z': 0}
                else:
                    empty_motor[pid] = 0
        # print(empty_motor)
        return empty_motor

    def connect(self, port, baud='115200'):
        print(__name__, "connect() self.serial_device.serialPort", self.serial_device.serialPort)
        if (port == 'None'):
            self.serial_device.close()
            self.print('Disconnected')
            self._init_motors()
            if self.serial_device.comsType is "UDP":
                self.serial_device.udp_socket.close()
                self.comport_status = 'closed'
                self.connection_status = 'Disconnected'
                print('bplprotocol.py connect() comport_status:', self.comport_status, 'connection_status:',
                      self.connection_status)
                self.serial_device.comsType = "SERIAL"
        elif port == 'UDP':
            self.connectUDPServer()
        elif port == 'TCP':
            print(__name__, 'connect() IMPLEMENT ME')
        else:
            self.serial_device.baud = int(baud)
            self.serial_device.port = port
            self.serial_device.comsType = "SERIAL"
            print(__name__, "connect() Connecting", self.serial_device.port, self.serial_device.baud)
            self.print('Connecting to ' + port + ' ' + baud)
            self.comport_status = 'connecting'
            self.serial_device.open()
            if self.serial_device.serialPort.is_open:
                self.comport_status = 'open'
                self.print('Connected', 'green')
            else:
                self.comport_status = 'closed'
                self.print('Disconnected')
            self.serial_device.txDeviceId = 0x01
            self.serial_device.rxPacketId = 0x00
            self.serial_device.rxDeviceId = 0x00
        return self.comport_status == 'open'

    def connectUDPServer(self):
        self.serial_device.connection_status = "Connecting UDP"
        self.serial_device.ipAddress = self.connection_ipAddress
        self.serial_device.remoteIPAddress = (self.connection_remoteIPAddress, self.connection_udpPort)
        # self.serial_device.port = int(self.connection_udpPort)
        self.serial_device.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self.serial_device.udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serial_device.comsType = "UDP"

        try:
            self.serial_device.udp_socket.setblocking(0)
            # print('bplprotocol.py success line 106')
            self.serial_device.udp_socket.bind((self.serial_device.ipAddress, self.connection_udpPort))
            # print('bplprotocol.py success line 108')

            print(__name__, 'connectUDPServer() Connected UDP Server',
                  self.serial_device.ipAddress, self.serial_device.port)
            self.comport_status = 'open'
            self.print('Connected ' + self.serial_device.ipAddress + ':' + str(self.connection_udpPort), 'green')
            # print('self.serial_device.serialPort.is_open:', self.serial_device.serialPort.is_open)

        except:
            print("failed to connect")
            print(self.serial_device.port)
            print(self.serial_device.ipAddress)
            self.comport_status = 'closed'
            self.connection_status = 'Disconnected'
            self.print('Disconnected')

    def send_udp_test(self, message):
        self.serial_device.udp_socket.sendto(message, (self.connection_remoteIPAddress, int(self.connection_udpPort)))

    def get_connection_name(self):
        if self.serial_device.comsType == 'UDP':
            return 'BlueROV (UDP)'
        if self.serial_device.comsType == 'TCP':
            return 'TCP'
        if self.serial_device.serialPort.is_open:
            name = self.serial_device.serialPort.name
            return name
        return 'None'

    def print(self, text, color='black'):
        if len(self.console_output) > 200:
            self.console_output = self.console_output[-199:]
        if (type(text) is not str):
            text = str(text)
        # text = text[:27]
        if color is 'red':
            text = '[color=#ff0000]' + text + '[/color]'
        if color is 'green':
            text = '[color=#009000]' + text + '[/color]'
        d = datetime.time(time.localtime()[3], time.localtime()[4], time.localtime()[5], )
        time_text = '{:%H:%M:%S} '.format(d)
        # time_text = str(time.localtime()[3]) + ":" + str(time.localtime()[4]) + ":" + str(time.localtime()[5]) + " "
        self.console_output.append(time_text + text)
        try:
            Console.update_console_list(Console.instance, self.console_output)
        except Exception as e:
            print(__name__, 'print():', e)

    def reset_protocol_parameters(self):
        self.serial_device.txPacketId = 0
        self.serial_device.txData = 0
        self.serial_device.txDeviceId = 0

        self.serial_device.rxPacketId = 0
        self.serial_device.rxData = [0]
        self.serial_device.rxDeviceId = 0

        self.serial_device.incomplete_pckt = b''

        self.serial_device.packets = bytearray()
        self.serial_device.pckt = b''
        self.serial_device.decoded_pckt = b''

    def switch_to_bpl_protocol(self):
        if self.serial_device.protocol is "BPL":
            print("bplprotocol.py switch_to_bpl_protocol() BPL protocol already selected")
        else:
            if self.bpl_device is None:
                # instantiate a BPL device
                self.bpl_device = SerialDevice()
            if self.serial_device is not None:
                # bring the serialPort across to the new protocol
                self.bpl_device.serialPort = self.serial_device.serialPort
            # switch serial_device over to bpss_device
            self.serial_device = self.bpl_device
            print("bplprotocol.py switch_to_bpl_protocol() switched to BPL protocol")

    def switch_to_bpss_protocol(self):
        if self.serial_device.protocol is "BPSS":
            print("bplprotocol.py switch_to_bpss_protocol() BPSS protocol already selected")
        else:
            if self.bpss_device is None:
                # instantiate a BPSS device
                self.bpss_device = serial_device_bpss.SerialDevice()
            if self.serial_device is not None:
                # bring the serialPort across to the new protocol
                self.bpss_device.serialPort = self.serial_device.serialPort
            # switch serial_device over to bpss_device
            self.serial_device = self.bpss_device
            print("bplprotocol.py switch_to_bpl_protocol() switched to BPSS protocol")

    def send_velocity(self, DeviceID, slider_val, callback=None):
        if True:
            product.set_velocity(DeviceID, slider_val)
            # if DeviceID in self.velocities:
            #     self.velocities[DeviceID] = slider_val
        else:
            # print("bplprotocol.py send_velocity() DeviceID:", DeviceID, "slider_val:", slider_val)
            txDeviceId = DeviceID
            txData = float(slider_val)
            txPacketId = PacketID.VELOCITY
            self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_openloop(self, DeviceID, slider_val, callback=None):
        print("bplprotocol.py send_openloop() DeviceID:", DeviceID, "slider_val:", slider_val)
        txDeviceId = DeviceID
        txData = float(slider_val) / 100.
        txPacketId = PacketID.OPENLOOP
        if True:
            product.set_openloop(txDeviceId, txData)
        else:
            self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_position(self, DeviceID, slider_val, callback=None):
        print("bplprotocol.py send_position() DeviceID:", DeviceID, "slider_val:", slider_val)
        txDeviceId = DeviceID
        txData = float(slider_val)  # *6.2831853/360.0
        txPacketId = PacketID.POSITION
        if True:
            product.set_position(txDeviceId, txData)
        else:
            self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_current(self, DeviceID, slider_val, callback=None):
        print("bplprotocol.py send_current() DeviceID:", DeviceID, "slider_val:", slider_val)
        txDeviceId = DeviceID
        txData = slider_val
        txPacketId = PacketID.CURRENT
        self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_mode(self, DeviceID, checkboxValue, callback=None):
        # print("bplprotocol.py send_mode() device_id:", DeviceID, "value:", checkboxValue)
        if checkboxValue == False:
            txData = Mode.DISABLED
        else:
            txData = Mode.STANDBY
        txDeviceId = DeviceID
        txPacketId = PacketID.MODE
        self.send_packet(txDeviceId, txPacketId, txData, callback)

    ### CONFIG FUNCTIONS ###

    def send_factoryMode(self, DeviceID, checkboxValue, callback=None):
        if checkboxValue == False:
            txData = Mode.STANDBY
        else:
            txData = Mode.FACTORY
        txDeviceId = DeviceID
        txPacketId = PacketID.MODE
        self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_current_limit(self, DeviceID, max, min, callback=None):
        print(__name__, 'send_current_limit() DeviceID, max, min', DeviceID, max, min)
        txDeviceId = DeviceID
        txData = ([max, min])
        txPacketId = PacketID.CURRENT_LIMIT
        self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_velocity_limit(self, DeviceID, max, min, callback=None):
        txDeviceId = DeviceID
        txData = ([max, min])
        txPacketId = PacketID.VELOCITY_LIMIT
        self.send_packet(txDeviceId, txPacketId, txData, callback)

    def send_position_limit(self, DeviceID, max, min, callback=None):
        txDeviceId = DeviceID
        txData = ([max, min])
        txPacketId = PacketID.POSITION_LIMIT
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_km_limit(self, DeviceID, packet_id, max, min, callback=None):
        txDeviceId = DeviceID
        txData = ([max, min])
        txPacketId = packet_id
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_position_gains(self, DeviceID, kp, ki, kd, kf, mi, callback=None):
        self.send_gains(DeviceID, kp, ki, kd, kf, mi, PacketID.POSITION_GAIN, callback=callback)

    def send_velocity_gains(self, DeviceID, kp, ki, kd, kf, mi, callback=None):
        self.send_gains(DeviceID, kp, ki, kd, kf, mi, PacketID.VELOCITY_GAIN, callback=callback)

    def send_current_gains(self, DeviceID, kp, ki, kd, kf, mi, callback=None):
        self.send_gains(DeviceID, kp, ki, kd, kf, mi, PacketID.CURRENT_GAIN, callback=callback)

    def send_km_gains(self, DeviceID, packet_id, kp, ki, kd, kf, mi, callback=None):
        self.send_gains(DeviceID, kp, ki, kd, kf, mi, packet_id, callback=callback)

    def send_gains(self, device_id, kp, ki, kd, kf, mi, packet_id, callback=None):
        txDeviceId = device_id
        txData = ([kp, ki, kd, kf, mi])
        txPacketId = packet_id
        print(__name__, "send_gains", txPacketId)
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_heartrate_set(self, DeviceID, value, callback=None):
        if self.serial_device.protocol is not "BPSS":
            txData = bytearray([int(value)])
        else:
            txData = int(value)
        # txData = int(value)
        # print('HEARTRATE', txData, int(value))
        txDeviceId = DeviceID
        txPacketId = PacketID.HEARTBEAT_FREQUENCY_SET
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_heartbeat_packets(self, DeviceID,
                               packet_id_a=0, packet_id_b=0,
                               packet_id_c=0, packet_id_d=0,
                               packet_id_e=0, packet_id_f=0,
                               packet_id_g=0, packet_id_h=0,
                               packet_id_i=0, packet_id_j=0, callback=None):
        if self.serial_device.protocol is not "BPSS":
            txDeviceId = DeviceID

            txData = (bytearray([packet_id_a, packet_id_b,
                                 packet_id_c, packet_id_d,
                                 packet_id_e, packet_id_f,
                                 packet_id_g, packet_id_h,
                                 packet_id_i, packet_id_j]))

            txPacketId = PacketID.HEARTBEAT_SET
            print(__name__, "send_heartbeat_packets", txPacketId, txData)
            self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_manual_packet(self, DeviceID, PacketID, d1, d2, d3, d4, callback=None):
        txDeviceId = DeviceID
        txData = (bytearray([d1, d2, d3, d4]))
        txPacketId = PacketID
        print(__name__, "send_manual_packet", txPacketId)
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_request_packet(self, DeviceID, packetIDRequest, this_is_a_poll=True):
        if (this_is_a_poll and self.poll_for_device_info) \
                or not this_is_a_poll:
            print('BPLprotocol: send request packet. DevID:', DeviceID, 'Packet IDs requested:', packetIDRequest)
            txData = packetIDRequest
            txDeviceId = DeviceID
            txPacketId = PacketID.REQUEST
            self.serial_device.sendpacket(txDeviceId, txPacketId, txData)
            # self.send_packet(txDeviceId, txPacketId, txData, callback=None)
        # else:
        #     print('BPLprotocol: send request packet NOT SENT')

    def send_request_packets(self, DeviceID, d0=0, d1=0, d2=0, d3=0, d4=0, d5=0, d6=0, d7=0, d8=0, d9=0, callback=None):
        self.add_requests_to_expected_packets(DeviceID, [d0, d1, d2, d3, d4, d5, d6, d7, d8, d9], callback)
        txData = (bytearray([d0, d1, d2, d3, d4, d5, d6, d7, d8, d9]))
        txPacketId = PacketID.REQUEST
        self.send_packet(DeviceID, txPacketId, txData, callback)
        # self.request_queue.append([DeviceID, d0, d1, d2, d3, d4, d5, d6, d7, d8, d9])

    def add_requests_to_expected_packets(self, device_id, packets, callback=None):
        """

        :param int device_id:
        :param list packets: list of packets to add
        :return:
        """
        for packet in packets:
            if packet > 0 and [device_id, packet] not in self.expected_packets:
                self.expected_packets.append([device_id, packet, None, time.time(), callback])

    def add_to_permanent_callbacks(self, device_id, packet_id, callback):
        """
        Trigger a callback every time the specified device id and packet id are received.
        :param int device_id:
        :param list packets: list of packets to add
        :param callable callback: must accept arguments (self, device_id, packet_id, data)
        :return:
        """
        if [device_id, packet_id, None, None, callback] not in self.permanent_callbacks:
            self.permanent_callbacks.append([device_id, packet_id, None, None, callback])

    def send_model_number(self, DeviceID, value, callback=None):
        txData = value
        txDeviceId = DeviceID
        txPacketId = PacketID.MODEL_NUMBER
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_serial_number(self, DeviceID, value, callback=None):
        txData = value
        txDeviceId = DeviceID
        txPacketId = PacketID.SERIAL_NUMBER
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_bootloader(self, DeviceID, callback=None):
        print(__name__, 'send_bootloader to device id:', DeviceID)
        txDeviceId = 0xFF
        txData = 0
        txPacketId = PacketID.BOOTLOADER
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_save_configuration(self, DeviceID, callback=None):
        txData = 0
        txDeviceId = DeviceID
        txPacketId = PacketID.SAVE
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_load_configuration(self, DeviceID, callback=None):
        txData = 0
        txDeviceId = DeviceID
        txPacketId = PacketID.LOAD
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_set_defaults(self, DeviceID, callback=None):
        txData = 0
        txDeviceId = DeviceID
        txPacketId = PacketID.SET_DEFAULTS
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_format(self, DeviceID, callback=None):
        txData = 0
        txDeviceId = DeviceID
        txPacketId = PacketID.FORMAT
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_motor_parameters(self, DeviceID, voltage, current, resistance, direction, callback=None):
        '''
        Takes in device_id as int and parameters as object.
        Parameters object will have attributes: voltage, current, resistance
        '''
        print("bplprotocol.py send_motor_parameters() device id:", DeviceID,
              "[voltage, current, resistance, direction]",
              [voltage, current, resistance, float(direction)])
        txData = ([voltage, current,
                   resistance, float(direction)])
        txDeviceId = DeviceID
        txPacketId = PacketID.MOTOR_PARAMETERS
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_position_parameters(self, DeviceID, scale, offset, alpha, beta, callback=None):
        print("BPLPROTOCOL send_position_parameters() device_id:", DeviceID, "scale:", scale,
              'offset:', offset, "alpha:", alpha, 'beta:', beta)
        txDeviceId = DeviceID
        txPacketId = PacketID.POSITION_PARAMETERS
        txData = ([scale, offset, alpha, beta])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_velocity_parameters(self, DeviceID, scale, offset, alpha, beta, callback=None):
        print("BPLPROTOCOL send_velocity_parameters() device_id:", DeviceID, "scale:", scale,
              'offset:', offset, "alpha:", alpha, 'beta:', beta)
        txDeviceId = DeviceID
        txPacketId = PacketID.VELOCITY_PARAMETERS
        txData = ([scale, offset, alpha, beta])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_current_parameters(self, DeviceID, scale, offset, alpha, beta, callback=None):
        print("BPLPROTOCOL send_current_parameters() device_id:", DeviceID, "scale:", scale,
              'offset:', offset, "alpha:", alpha, 'beta:', beta)
        txDeviceId = DeviceID
        txPacketId = PacketID.CURRENT_PARAMETERS
        txData = ([scale, offset, alpha, beta])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_input_voltage_parameters(self, DeviceID, scale, offset, alpha, beta, callback=None):
        print("BPLPROTOCOL send_input_voltage_parameters() device_id:", DeviceID, "scale:", scale,
              'offset:', offset, "alpha:", alpha, 'beta:', beta)
        txDeviceId = DeviceID
        txPacketId = PacketID.INPUT_VOLTAGE_PARAMETERS
        txData = ([scale, offset, alpha, beta])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_coms_protocol(self, DeviceID, value, callback=None):
        '''
        value = 0 for BPL
        value = 1 for BPSS
        '''
        # if value == 1:
        #     txData = int(value)
        #     txDeviceId = DeviceID
        #     txPacketId = PacketID.COMS_PROTOCOL
        #     self.send_packet(txDeviceId, txPacketId, txData)
        # else:
        #     command_string = '#0180003000"
        txData = int(value)
        txDeviceId = DeviceID
        txPacketId = PacketID.COMS_PROTOCOL
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_device_type(self, DeviceID, value, callback=None):
        txData = int(value)
        txDeviceId = DeviceID
        txPacketId = PacketID.DEVICE_TYPE
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_compliance_gain(self, DeviceID, value, callback=None):
        txData = float(value)
        txDeviceId = DeviceID
        txPacketId = PacketID.COMPLIANCE_GAIN
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_current_hold_threshold(self, DeviceID, value, callback=None):
        txData = float(value)
        txDeviceId = DeviceID
        txPacketId = PacketID.CURRENT_HOLD_THRESHOLD
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_max_acceleration(self, DeviceID, value, callback=None):
        txData = float(value)
        txDeviceId = DeviceID
        txPacketId = PacketID.MAX_ACCELERATION
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_device_id(self, DeviceID, new_device_id, callback=None):
        txData = int(new_device_id)
        txDeviceId = DeviceID
        txPacketId = PacketID.DEVICE_ID
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_velocity(self, velocities, callback=None):

        txData = ([float(velocities[0]), float(velocities[1]), float(velocities[2])])
        if len(velocities) > 3:
            txData = ([float(velocities[0]), float(velocities[1]), float(velocities[2]),
                       float(velocities[3]), float(velocities[4]), float(velocities[5])])
        print(__name__, 'send_kinematics_velocity velocities', velocities)
        if True:
            product.set_km_end_vel(self.deviceIDs[4], txData)
            # self.velocities['KM_END_VEL'] = txData
            # print(__name__, 'send_kinematics_velocity() self.velocities',  self.velocities, self.last_velocities)
        else:
            txDeviceId = self.deviceIDs[4]
            txPacketId = PacketID.KM_END_VEL
            self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_position(self, pos_x, pos_y, pos_z, callback=None):
        print(__name__, 'send_kinematics_position() pos_x', pos_x, 'pos_y', pos_y, 'pos_z', pos_z)
        txData = ([float(pos_x), float(pos_y), float(pos_z), float(0), float(0), float(0)])
        if True:
            product.set_km_end_pos(self.deviceIDs[4], txData)
            # self.velocities['KM_END_POS'] = txData
            print(__name__, 'send_kinematics_position() txData', txData)
        else:
            txDeviceId = self.deviceIDs[4]
            txPacketId = PacketID.KM_END_POS
            self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_obstacle_box(self, device_id, obstacle_packet_id, x1, y1, z1, x2, y2, z2, callback=None):
        print(__name__, 'send_kinematics_obstacle() obstacle_packet_id, x1, y1, z1, x2, y2, z2',
              obstacle_packet_id, x1, y1, z1, x2, y2, z2)
        txPacketId = obstacle_packet_id
        txDeviceId = device_id
        txData = ([float(x1), float(y1), float(z1), float(x2), float(y2), float(z2)])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_obstacle_cylinder(self, device_id, obstacle_packet_id, x1, y1, z1, x2, y2, z2, radius, callback=None):
        print(__name__, 'send_kinematics_obstacle() obstacle_packet_id, x1, y1, z1, x2, y2, z2, radius',
              obstacle_packet_id, x1, y1, z1, x2, y2, z2, radius)
        txPacketId = obstacle_packet_id
        txDeviceId = device_id
        txData = ([float(x1), float(y1), float(z1), float(x2), float(y2), float(z2), float(radius)])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_config(self, device_id, km_enable, obs_enable, orientation, frame, callback=None):
        print(__name__, 'send_kinematics_config() km_enable', km_enable, 'obs_enable', obs_enable,
              'orientation', orientation, 'frame', frame)
        txPacketId = PacketID.KM_CONFIGURATION
        txDeviceId = device_id
        txData = (bytearray([int(km_enable), int(obs_enable), int(orientation), int(frame)]))
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_dh_parameters(self, device_id, dh_packet_id, d, a, alpha, theta_offset, theta_min, theta_max, callback=None):
        print(__name__, 'send_kinematics_dh_parameters() dh_packet_id, d, a, alpha, theta_offset, theta_min, theta_max',
              dh_packet_id, d, a, alpha, theta_offset, theta_min, theta_max)
        txPacketId = dh_packet_id
        txDeviceId = device_id
        txData = ([float(d), float(a), float(alpha), float(theta_offset), float(theta_min), float(theta_max)])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_kinematics_float_parameters(self, device_id, m_zero, lambda_translate, lambda_rotate,
                                         collision_fwd_time_steps,
                                         self_collision_radius, end_eff_collision_tolerance,
                                         callback=None):
        txPacketId = PacketID.KM_FLOAT_PARAMETERS
        txDeviceId = device_id
        txData = ([float(m_zero), float(lambda_translate), float(lambda_rotate), float(collision_fwd_time_steps),
                   float(self_collision_radius), float(end_eff_collision_tolerance)])
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_electrical_version(self, DeviceID, value, callback=None):
        txData = (bytearray(value))
        txDeviceId = DeviceID
        txPacketId = PacketID.ELECTRICAL_VERSION
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_software_version(self, DeviceID, value, callback=None):
        txData = (bytearray(value))
        txDeviceId = DeviceID
        txPacketId = PacketID.SOFTWARE_VERSION
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_mechanical_version(self, DeviceID, value, callback=None):
        txData = (bytearray(value))
        txDeviceId = DeviceID
        txPacketId = PacketID.MECHANICAL_VERSION
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    # def send_encoder_zero(self, DeviceID):
    #     txData = (bytearray([0x75, 0x09, 0x00, 0x00]))
    #     txDeviceId = DeviceID
    #     txPacketId = PacketID.ENCODER
    #     self.send_packet(txDeviceId, txPacketId, txData)

    # def send_encoder_direction(self, DeviceID, is_forward):
    #     if is_forward:
    #         txData = (bytearray([0x15, 0x00, 0x75, 0x01]))
    #     else:
    #         txData = (bytearray([0x15, 0x80, 0x75, 0x01]))
    #     txDeviceId = DeviceID
    #     txPacketId = PacketID.ENCODER
    #     self.send_packet(txDeviceId, txPacketId, txData)

    def send_encoder_parameters(self, device_id, direction=0, set_zero=0, callback=None):
        """

        :param device_id:
        :param direction: 0=NO_CHANGE, 1=FORWARD, 2=REVERSE
        :param set_zero: 0=NO_CHANGE, 1=SET_ZERO_TO_CURRENT_POSITION
        :return: nothing
        """
        txData = (bytearray([direction, set_zero]))
        txDeviceId = device_id
        txPacketId = PacketID.ICMU_PARAMETERS
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    # def get_limits_data(self, DeviceID):
    #     self.send_request_packet(DeviceID, ([PacketID.POSITION_LIMIT, PacketID.CURRENT_LIMIT, PacketID.VELOCITY_LIMIT]))

    def send_current_hold(self, device_id, current_hold_on, callback=None):
        """

        :param device_id:
        :param current_hold_on: BOOL
        :return: Nothing
        """
        data_out = 2 if current_hold_on else 1
        txData = (bytearray([data_out, 0]))
        txDeviceId = device_id
        txPacketId = PacketID.MODE_SETTINGS
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_compliance(self, device_id, compliance_on, callback=None):
        """

        :param device_id:
        :param compliance_on: BOOL
        :return: Nothing
        """
        data_out = 2 if compliance_on else 1
        txData = (bytearray([0, data_out]))
        txDeviceId = device_id
        txPacketId = PacketID.MODE_SETTINGS
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    def send_mode_settings(self, device_id, current_hold_on, compliance_on, callback=None):
        """

        :param device_id:
        :param current_hold_on:
        :param compliance_on: BOOL
        :return: Nothing
        """
        data_out = 2 if compliance_on else 1
        txData = (bytearray([2 if current_hold_on else 1,
                             2 if compliance_on else 1]))
        txDeviceId = device_id
        txPacketId = PacketID.MODE_SETTINGS
        self.send_packet(txDeviceId, txPacketId, txData, callback=callback)

    ### END CONFIG FUNCTIONS ###

    def send_openloop_increment(self, deviceID, increment_size):
        motor = self.get_motor_by_device_id(deviceID)
        if motor is not None:
            print(__name__, 'send_openloop_increment() deviceID', deviceID,
                  'increment_size', increment_size,
                  'motor[PacketID.OPENLOOP]', motor[PacketID.OPENLOOP])
            motor[PacketID.OPENLOOP] = max(min(motor[PacketID.OPENLOOP] + increment_size, 100), 0)
            self.send_openloop(deviceID, motor[PacketID.OPENLOOP])

    def toggle_light(self):
        if hasattr(self, 'light_on') and self.light_on:
            self.light_on = False
        else:
            self.light_on = True
        self.send_light_bpss(self.light_on)

    def send_light_bpss(self, turn_light_on):
        if turn_light_on:
            packet = BPSS_STRING_LIGHT_ON
        else:
            packet = BPSS_STRING_LIGHT_OFF
        try:
            self.serial_device.serialPort.write(packet)
            print(__name__, 'send_light_bpss() sent')
        except:
            print(__name__, 'send_light_bpss() NOT sent')

    def get_motor_by_device_id(self, deviceID, make_new=False):
        # deviceID = 1
        # print(self.motor_data.items())
        for key, value in self.motor_data.items():
            if value['deviceID'] == int(deviceID):
                # print(__name__, 'get_motor_by_device_id found motor', key, 'deviceID', value['deviceID'])
                return value
        print('bplprotocol.py get_motor_by_device_id() No motor with deviceID:', deviceID, '. Generating motor model')
        try:
            if make_new:
                new_motor = self._init_motor_model()
                for i in range(26):
                    next_letter = chr(ord('a') + i)
                    if next_letter not in self.motor_data:
                        self.motor_data[next_letter] = new_motor
                        self.motor_data[next_letter]['deviceID'] = deviceID
                        return self.motor_data[next_letter]
            return None
            # return self.motor_data['f']
        except:
            return None
        # raise Exception('ERROR: No motor with deviceID:', deviceID)

    '''
    for each packet in packets:
        motor = get_motor_by_device_id(packet.deviceID)
        data = 0
        pid = packet.rxPacketID
        if pid == PacketID.POSITION:
            data = packet.rxData[0]*360/6.2831853
        elif pid == PacketID.VELOCITY:
            data = packet.rxData[0]*360/6.2831853
        elif pid == PacketID.OPENLOOP:
            data = packet.rxData[0]*100
        elif pid == PacketID.MODE:
            data = packet.rxData[0]
            
            #TODO: What's all this?
            if self.modeMonitor==Mode.DISABLED:
                modeMonitor = 0
        elif pid == PacketID.POSITION_LIMIT:
            data = {'min': packet.rxData[0], 'max': packet.rxData[1])
                        
        elif (pid == PacketID.MODEL_NUMBER or pid == PacketID.SERIAL_NUMBER or pid == PacketID.VERSION):
            data = rxData
        else:
            data = packet.rxData[0]
            
        motor[packet.rxPacketID] = data
    '''

    # def receive_packets(self):
    #     num_of_packets = 0
    #     if self.serial_device.serialPort is None:
    #         if self.comport_status is 'open':
    #             self.print('Disconnected', 'red')
    #         self.comport_status = 'closed'
    #         # self.clk = Clock.schedule_once(self.updateMotor)
    #     elif not self.serial_device.serialPort.is_open and self.serial_device.comsType is 'SERIAL':
    #         self.comport_status = 'closed'
    #         # self.clk = Clock.schedule_once(self.updateMotor)
    #     elif (self.serial_device.comsType is 'SERIAL' and self.comport_status is not 'open'):
    #         # self.clk = Clock.schedule_once(self.updateMotor)
    #         pass
    #     else:
    #         if self.comport_status is not 'open':
    #             self.print('Connected', 'green')
    #         self.comport_status = 'open'
    #         packets_to_parse = self.serial_device.readdata()
    #         num_of_packets = len(packets_to_parse)
    #         if num_of_packets == 0:
    #             # self.clk = Clock.schedule_once(self.updateMotor)
    #             return num_of_packets
    #         for pckt in packets_to_parse:
    #             # todo: finish this
    #             rxdev_id, rxpacket_id, rxdata = self.serial_device.parse_packet_to_bytearray(pckt)
    #             # take the device_id and return the Product that it belongs to
    #             this_product = product.get_product_with_device_id(rxdev_id)
    #             if this_product is None:
    #                 self.print("No product with device id " + str(hex(rxdev_id)))
    #             else:
    #                 pass



    def updateMotor(self, *args):
        # print('BplSerial updateMotor() self.serial_device.serialPort.is_open:', self.serial_device.serialPort.is_open)
        # check serial connection first
        # t_0 = time.clock()
        # callback_start_time = 0
        # callback_end_time = 0
        num_of_packets = 0
        if self.serial_device.serialPort is None:
            if self.comport_status is 'open':
                self.print('Disconnected', 'red')
            self.comport_status = 'closed'
            # self.clk = Clock.schedule_once(self.updateMotor)
        elif not self.serial_device.serialPort.is_open and self.serial_device.comsType is 'SERIAL':
            self.comport_status = 'closed'
            # self.clk = Clock.schedule_once(self.updateMotor)
        elif (self.serial_device.comsType is 'SERIAL' and self.comport_status is not 'open'):
            # self.clk = Clock.schedule_once(self.updateMotor)
            if self.serial_device.serialPort.is_open:
                self.comport_status = 'open'
        else:
            if self.comport_status is not 'open':
                self.print('Connected', 'green')
            self.comport_status = 'open'
            num_of_packets = len(self.packets_to_parse)
            if num_of_packets == 0:
                # self.clk = Clock.schedule_once(self.updateMotor)
                return num_of_packets

            if len(self.packets_to_parse) > 100:
                self.packets_to_parse = self.packets_to_parse[-100:]
            while len(self.packets_to_parse) > 0:
                # t_0 = time.time()
                pckt = self.packets_to_parse.pop(0)
                rxdev_id, rxpacket_id, rxdata = self.serial_device.parsepacket(pckt)
                # parse_time = time.time()
                if rxdev_id == 0 and rxpacket_id == 0 and rxdata == 0:
                    num_of_packets = -1
                if int(rxdev_id) > 0:
                    # print(__name__, 'updateMotor() rxdev_id:', rxdev_id)
                    if rxdev_id not in self.device_ids_received:
                        self.device_ids_received.append(rxdev_id)
                    motor = self.get_motor_by_device_id(rxdev_id, make_new=True)
                    pid = rxpacket_id
                    pdata = rxdata
                    # for item in self.expected_packets:
                    #     if item[0] == rxdev_id and item[1] == pid:
                    #         if len(item) > 2:
                    #             if pdata == item[2]:
                    #                 print(__name__, 'updateMotor() data verified SUCCESS SUCCESS SUCCESS got:', pdata,
                    #                       'expected:', item[2], 'device_id:', rxdev_id, 'packet_id:', rxpacket_id)
                    #             else:
                    #                 print(__name__, 'updateMotor() data verified FAIL FAIL FAIL FAIL FAIL got:', pdata,
                    #                       'expected:', item[2], 'device_id:', rxdev_id, 'packet_id:', rxpacket_id)
                    #         self.expected_packets.remove(item)
                    if pid == PacketID.OPENLOOP:
                        data = pdata[0] * 100
                        # print('MODE data received:', data, pdata)
                    elif (pid == PacketID.SOFTWARE_VERSION
                          or pid == PacketID.ELECTRICAL_VERSION
                          or pid == PacketID.MECHANICAL_VERSION):
                        # print(__name__, 'updateMotor() VERSION pdata:', pdata[0], pdata[1], pdata[2])
                        data = [pdata[0], pdata[1], pdata[2]]
                    elif (pid == PacketID.VELOCITY_GAIN
                          or pid == PacketID.POSITION_GAIN
                          or pid == PacketID.CURRENT_GAIN
                          or pid == PacketID.KM_POS_GAINS_TRANSLATE
                          or pid == PacketID.KM_VEL_GAINS_TRANSLATE
                          or pid == PacketID.KM_POS_GAINS_ROTATE
                          or pid == PacketID.KM_VEL_GAINS_ROTATE):
                        data = {'KP': pdata[0], 'KI': pdata[1], 'KD': pdata[2], 'KF': pdata[3], 'MI': pdata[4]}
                        print(__name__, " updateMotor() gains. packet id:",
                              hex(pid), 'pdata:', pdata, 'data:', data)
                    elif (pid == PacketID.POSITION_LIMIT
                          or pid == PacketID.CURRENT_LIMIT
                          or pid == PacketID.VELOCITY_LIMIT
                          or pid == PacketID.KM_POS_LIMIT_TRANSLATE
                          or pid == PacketID.KM_VEL_LIMIT_TRANSLATE
                          or pid == PacketID.KM_POS_LIMIT_YAW
                          or pid == PacketID.KM_POS_LIMIT_PITCH
                          or pid == PacketID.KM_POS_LIMIT_ROLL
                          or pid == PacketID.KM_VEL_LIMIT_ROTATE):
                        if len(pdata) > 1:
                            data = {'min': pdata[1], 'max': pdata[0]}
                        print(__name__, 'updatemotor() VEL/CUR/POS LIMIT received. deviceID:', rxdev_id, 'Packet ID:',
                              pid, 'min:', data['min'], 'max:', data['max'])
                    elif (pid == PacketID.MODEL_NUMBER
                          or pid == PacketID.SERIAL_NUMBER
                          or pid == PacketID.VERSION):
                        if type(pdata) is float:
                            data = pdata
                        else:
                            data = pdata[0]
                        # print(__name__, 'Device ID:', motor['deviceID'], 'Packet ID:', pid, 'Data: ', data)
                    elif pid == PacketID.MOTOR_PARAMETERS:
                        # print('bplprotocol.py updaeMotor() MOTOR_PARAMETERS Device ID:',
                        #       motor['deviceID'], 'pdata: ', pdata)
                        data = {'voltage': pdata[0], 'current': pdata[1], 'resistance': pdata[2], 'direction': pdata[3]}
                        print(__name__, 'updateMotor() MOTOR_PARAMETERS Device ID:',
                              motor['deviceID'], 'Data: ', data)
                    elif (pid == PacketID.POSITION_PARAMETERS
                          or pid == PacketID.CURRENT_PARAMETERS
                          or pid == PacketID.VELOCITY_PARAMETERS
                          or pid == PacketID.INPUT_VOLTAGE_PARAMETERS):
                        data = {'scale': pdata[0], 'offset': pdata[1], 'alpha': pdata[2], 'beta': pdata[3]}
                        # print(__name__, 'updateMotor() Device ID:', motor['deviceID'], 'Packet ID:', pid, 'Data: ',
                        #       data)
                    elif pid == PacketID.KM_END_VEL or pid == PacketID.KM_END_POS:
                        data = {'x': pdata[0], 'y': pdata[1], 'z': pdata[2]}
                    elif pid >= PacketID.KM_BOX_OBSTACLE_00 and pid <= PacketID.KM_BOX_OBSTACLE_05:
                        # print(__name__, 'Device ID:', motor['deviceID'], 'Packet ID:', pid, 'pdata: ', pdata)
                        data = {'x1': pdata[0], 'y1': pdata[1], 'z1': pdata[2],
                                'x2': pdata[3], 'y2': pdata[4], 'z2': pdata[5]}
                        # print(__name__, 'updateMotor() Device ID:', motor['deviceID'], 'Packet ID:', pid, 'Data: ',
                        #       data)
                    elif pid >= PacketID.KM_CYLINDER_OBSTACLE_00 and pid <= PacketID.KM_CYLINDER_OBSTACLE_05:
                        # print(__name__, 'Device ID:', motor['deviceID'], 'Packet ID:', pid, 'pdata: ', pdata)
                        data = {'x1': pdata[0], 'y1': pdata[1], 'z1': pdata[2],
                                'x2': pdata[3], 'y2': pdata[4], 'z2': pdata[5], 'radius': pdata[6]}
                        # print(__name__, 'updateMotor() Device ID:', motor['deviceID'], 'Packet ID:', pid, 'Data: ',
                        #       data)
                    elif pid == PacketID.KM_CONFIGURATION:
                        # print(__name__, 'updatemotor() KM_CONFIGURATION', 'Device ID:', motor['deviceID'],
                        #       'pdata: ', pdata)
                        data = {'enable': pdata[0], 'obstacles': pdata[1], 'orientation': pdata[2], 'frame': pdata[3]}
                        # print(__name__, 'updatemotor() KM_CONFIGURATION', 'Device ID:', motor['deviceID'],
                        #       'pdata: ', pdata, 'data', data)
                    elif PacketID.KM_DH_PARAMETERS_0 <= pid <= PacketID.KM_DH_PARAMETERS_7:
                        print(__name__, 'updatemotor() KM_DH_PARAMETERS #', pid - PacketID.KM_DH_PARAMETERS_0,
                              'pdata: ', pdata, 'callbacks:', self.expected_packets)
                        data = {'d': pdata[0], 'a': pdata[1], 'alpha': pdata[2],
                                'theta_offset': pdata[3], 'theta_min': pdata[4], 'theta_max': pdata[5]}
                    elif pid == PacketID.KM_FLOAT_PARAMETERS:
                        print(__name__, 'updatemotor() KM_FLOAT_PARAMETERS pdata:', pdata)
                        data = {'m_zero': pdata[0], 'lambda_translate': pdata[1],
                                'lambda_rotate': pdata[2], 'collision_fwd_time_steps': pdata[3],
                                'self_collision_radius': pdata[4], 'end_eff_collision_tolerance': pdata[5]}
                    elif pid == PacketID.MODE_SETTINGS:
                        print(__name__, 'updatemotor() MODE_SETTINGS', 'Device ID:', motor['deviceID'],
                              'pdata: ', pdata)
                        data = {'current_hold': pdata[0], 'compliance': pdata[1]}
                        for item in data:
                            if data[item] == 0:
                                data[item] = 1
                    # elif pid == PacketID.KM_JOINT_STATE:
                    #     continue
                    else:
                        if type(pdata) is float:
                            data = pdata
                        else:
                            data = pdata[0]
                    print('bplprotocol.py updateMotor() Device ID:', hex(motor['deviceID']), 'Packet ID:',
                          RS1_hardware.get_name_of_packet_id(pid), pid, 'Data: ', data,
                          'device_ids_received', self.device_ids_received)
                    motor[pid] = data
                    # self.add_receipt_time_stamp(rxdev_id, motor, pid)
                    # search_start_time = time.time()
                    for item in (self.expected_packets + self.permanent_callbacks):
                        if (item[0] == rxdev_id or item[0] == 0xFF) and item[1] == pid:
                            if len(item) > 2 and item[EP_DATA] is not None:
                                callback = item[EP_CALLBACK]
                                if callable(callback):
                                    # callback_start_time = time.time()
                                    callback(rxdev_id, pid, rxdata)
                                    # callback_end_time = time.time()
                                if PacketID.KM_BOX_OBSTACLE_00 <= pid <= PacketID.KM_BOX_OBSTACLE_05:
                                    success = True
                                    for i in range(3):
                                        expected_data_1 = item[2][i]
                                        expected_data_2 = item[2][i + 3]
                                        if (expected_data_1 == rxdata[i] and expected_data_2 == rxdata[i+3]) \
                                                or (expected_data_1 == rxdata[i+3] and expected_data_2 == rxdata[i]):
                                            pass
                                        else:
                                            success = False
                                    if success:
                                        self.print('Recvd SUCCESS DID:' + str(rxdev_id) + ' ' + str(
                                            RS1_hardware.get_name_of_packet_id(pid))
                                                   + ' expected ' + str(item[2]) + ' got ' + str(data),
                                                   color='green')
                                    else:
                                        self.print('Recvd FAIL DID:' + str(rxdev_id) + ' ' + str(
                                            RS1_hardware.get_name_of_packet_id(pid))
                                                   + ' expected ' + str(item[2]) + ' got ' + str(data),
                                                   color='red')
                                    print(__name__, 'updateMotor() success:', success, success, success,
                                          'data verified got:', data,
                                          'expected:', item[2], 'device_id:', rxdev_id,
                                          'packet_id:', RS1_hardware.get_name_of_packet_id(pid),
                                          'expected_packets', self.expected_packets)
                                else:

                                    if data == item[2] or pdata == item[2]:
                                        # print(__name__, 'updateMotor() data verified SUCCESS SUCCESS SUCCESS got:', data,
                                        #       'expected:', item[2], 'device_id:', rxdev_id, 'packet_id:', rxpacket_id)
                                        self.print('Recvd SUCCESS DID:' + str(rxdev_id) + ' '
                                                   + str(RS1_hardware.get_name_of_packet_id(pid))
                                                   + ' expected ' + str(item[2]) + ' got ' + str(pdata),
                                                   color='green')
                                    else:
                                        print(__name__, 'updateMotor() data verified FAIL FAIL FAIL FAIL FAIL got:', data,
                                              'expected:', item[2], 'device_id:', rxdev_id, 'packet_id:', rxpacket_id)
                                        self.print('Recvd FAIL DID:' + str(rxdev_id) + ' '
                                                   + str(RS1_hardware.get_name_of_packet_id(pid))
                                                   + ' expected ' + str(item[2]) + ' got ' + str(pdata),
                                                   color='red')
                            else:
                                callback = item[EP_CALLBACK]
                                if callable(callback):
                                    # callback_start_time = time.time()
                                    callback(rxdev_id, pid, rxdata)
                                    # callback_end_time = time.time()
                            if item in self.expected_packets:
                                self.expected_packets.remove(item)
                        if len(item) > EP_TIME and item[EP_TIME]:
                            if item[EP_TIME] + REQUEST_TIMEOUT < time.time():
                                if item in self.expected_packets:
                                    print(__name__, 'updateMotor() removing', item, 'from', self.expected_packets)
                                    try:
                                        self.expected_packets.remove(item)
                                    except:
                                        print(__name__, 'updateMotor() could not remove', item)
                    # search_end_time = time.time()
                    if pid == PacketID.HARDWARE_STATUS:
                        # print("bplprotocol.py updateMotor() HARDWARE STATUS PACKET received. Device ID:",
                        #       rxdev_id, 'data:', data)
                        # self.print(str(self.error_count) + ": Dev ID " + str(self.serial_device.rxDeviceId) +
                        #            ", Err code " + str(data), color='red')
                        for i in range(8):
                            if data & (1 << i):
                                self.error_count += 1
                                self.print(str(self.error_count) + " ID " + str(rxdev_id) +
                                           " " + get_hardware_status_string(i + 1), color='red')
                    if pid == PacketID.COMS_PROTOCOL:
                        print(__name__, "bplprotocol.py updateMotor() COMS_PROTOCOL PACKET received. Device ID:",
                              rxdev_id, 'data:', data)
                    if pid == PacketID.MODE:
                        for r5m_instance in ProductAlpha5.instances:
                            r5m_instance.set_mode(rxdev_id, data)

                    # print(__name__, "updateMotor() time to execute: {:4.1F} ms".format((time.time() - t_0) * 1000),
                    #       'num_of_packets:', num_of_packets, "callback time:", callback_end_time - callback_start_time,
                    #       "callback ratio:", (callback_end_time - callback_start_time) / (time.time() - t_0),
                    #       "search time:", search_end_time - search_start_time,
                    #       "search ratio:", (search_end_time - search_start_time) / (time.time() - t_0),
                    #       "parse ratio:", (parse_time - t_0) / (time.time() - t_0))

                    ''' 
                    Sleep here so that this thread does not block the others 
                    when many packets are received in one hit
                    '''
                    # time.sleep(0.001) # Taken out of action. Not required at present.

            # print(__name__, "updateMotor() time to execute: {:4.1F} ms".format((time.clock() - t_0) * 1000),
            #       'num_of_packets:', num_of_packets, "callback time:", callback_end_time - callback_start_time,
            #       "callback ratio:", (callback_end_time - callback_start_time)/(time.clock() - t_0))
            # self.clk = Clock.schedule_once(self.updateMotor)
        return num_of_packets


if __name__ == '__main__':
    bpl = BplSerial()
    bpl.connect('COM10', '115200')

    bpl.send_request_packet(1, PacketID.POSITION_GAIN)
    while True:
        bpl.updateMotor(1)
