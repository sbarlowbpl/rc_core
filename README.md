#RC_Core

## Installation
To install rc_core you can do:\
`pip install git+https://username@bitbucket.org/sbarlowbpl/rc_core.git`

## Usage
rc_core is the communication package to be used with reachcontrol and
other devices as a tool between encapsulating the encoding of packets to
be sent trough UDP, TCP, Serial or CAN.

Example for UDP:
```
from RC_Core.commconnection import CommConnection, CommType
from RC_Core.commconnection_methods import parsepacket, encode_packet
from RC_Core.RS1_hardware import PacketID

class Test:
    def init_comms(self):
        """Initalise the arm controller communication objects with the ARM (Via UDP)"""
        self.connection = CommConnection()
        self.connection.set_type(CommType.UDP)
        self.connection.config_connection(12872, '127.0.0.1', timeout=0.000, server=False)
    
    def read_packet(self):
        """Read Position, return 0 if empty return 1 if got a information
         Currently only responding to packetID.POSITION"""
        packets = self.connection.read()

        if packets:
            for p in packets:
                packet = parsepacket(p)
                device_id = packet[0]
                packet_type = packet[1]
                data = packet[2]
            return 1
        else:
            return 0

    def send_packet(self):
        self.connection.send(<device_id>, PacketID.<PACKET TYPE>, [<DATA ARRAY>]))
```