import asyncio

from .connection_server import ConnectionServer
from .connection_client import ConnectionClient
from .device_connection import DeviceConnection
# from .managed_connection import ManagedConnection
# from .comms_instruction_id import CommsInstructionID

import asyncio, sys




def create_connection_server(communication_pipe):
    if sys.platform == 'win32':
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)
    else:
        loop = asyncio.get_event_loop()
    cs = loop.run_until_complete(async_create_connection_server(communication_pipe))
    loop.run_forever()


async def async_create_connection_server(communication_pipe):
    cs = ConnectionServer(communication_pipe)





