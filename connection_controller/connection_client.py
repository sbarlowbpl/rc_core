import asyncio
import logging
from typing import Dict

from colorlog import ColoredFormatter

from RC_Core.comm_type import CommType
from .device_connection import DeviceConnection
from RC_Core.connections import PipeConnection
from RC_Core.enums.packets import PacketID
from RC_Core.protocols import InstructionClientProtocol

import rc_logging
logger = rc_logging.getLogger(__name__)


class ConnectionClient:
    """
    Connection Client is the client that connect to the connection server over a pipe Connection.
    Is uses the RC_Core.protocols.InstructionClientProtocol to communicate instructions between the server and cline.
    Connections are described through device Ids.
    0xFF Device ID represents instructions that are broadcast to all the devices.

    Instructions are executed via instruction ID.
    Example
    DEVICE_ID, CONNECTION_INSTRUCTION_PACKET_ID, [SET_CONNECTION_SERIAL, PARAMS_Data]
    """

    instance = None

    connections: Dict[int, DeviceConnection] = None

    _instruction_protocol: InstructionClientProtocol = None

    def __init__(self, instruction_pipe):
        logger.info(f"Creating PipeConnection for Client instruction_connection handle: {instruction_pipe.fileno()}")
        instruction_connection = PipeConnection(instruction_pipe)
        self._instruction_protocol = InstructionClientProtocol(PacketID.COMMS_INSTRUCTION, instruction_connection)
        self.connections = {}

        loop = asyncio.get_event_loop()
        loop.create_task(instruction_connection.connect())

        ConnectionClient.instance = self

    async def create_connection(self) -> DeviceConnection:

        connection_id = self._get_free_connection_id()
        logger.info(f"Creating connection, ID: {connection_id}")
        self.connections[connection_id] = None
        connection = DeviceConnection(self._instruction_protocol, connection_id)
        await connection.set_type(CommType.NONE, None)

        logger.info(f"Created connection, ID: {connection_id} type: None")

        self.connections[connection_id] = connection
        return connection

    def get_connection(self, connection_id: int):
        if connection_id in self.connections:
            return self.connections[connection_id]
        else:
            logger.warning(f"Connection ID: {connection_id} does not exist")

    async def remove_connection(self, connection_id: int):
        if connection_id in self.connections:
            await self.connections[connection_id].set_type(CommType.NONE)
            del self.connections[connection_id]

    def _get_free_connection_id(self) -> int:
        i = 0
        while i in self.connections:
            i += 1
        return i
