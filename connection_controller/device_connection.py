import asyncio
import logging
import time
import warnings
from multiprocessing.connection import Connection as MultiprocessingConnection
from typing import Union, List, Optional, Callable, Set, Tuple, Dict

from colorlog import ColoredFormatter

from RC_Core.comm_type import CommType
from .comms_instruction_id import CommsInstructionID
from RC_Core.connections import PipeConnection
from RC_Core.protocols import BPLPacketProtocol, InstructionClientProtocol

import rc_logging
from ..enums.packets import PacketID

logger = rc_logging.getLogger(__name__)


class DeviceConnection:
    connection_type: CommType = None
    name: str = None
    _communication_pipe: MultiprocessingConnection = None
    _pipe_connection: PipeConnection = None
    _bpl_comm_protocol: BPLPacketProtocol = None

    packet_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, Union[List[float], List[int]]], None]]] = None

    heartbeat_callbacks: Dict[Tuple[int, int], Dict[Callable[[int, int, Union[List[float], List[int]]], None], float]] = None

    _connected = False

    _bytes_callbacks: Set[Callable[[bytes], None]] = None

    def __init__(self, _instruction_protocol: InstructionClientProtocol, connection_device_id: int, name: str = ""):
        self.connection_device_id = connection_device_id
        self._instruction_protocol = _instruction_protocol

        self._instruction_protocol.add_permanent_instruction_callback(self.connection_device_id,
                                                                      CommsInstructionID.GET_CONNECTED,
                                                                      self._connected_callback)
        self.name = name

        self._bytes_callbacks = set()

        self.connected_callbacks = set()

        self.packet_callbacks = {}

        self.heartbeat_callbacks = {}

    @property
    def connected(self) -> bool:

        return self._connected

    async def set_type(self, connection_type: CommType, parameters=None):
        # Set the connection Type over the ConnectionClient
        logger.info(f"Connection {self.connection_device_id} Setting Type: {connection_type}")
        communication_pipe = None

        if connection_type == CommType.NONE:
            communication_pipe = \
                await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                        CommsInstructionID.SET_CONNECTION_NONE,
                                                                        parameters)
        elif connection_type == CommType.SERIAL:
            communication_pipe = \
                await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                        CommsInstructionID.SET_CONNECTION_SERIAL,
                                                                        parameters)
        elif connection_type == CommType.UDP:
            communication_pipe = \
                await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                        CommsInstructionID.SET_CONNECTION_UDP,
                                                                        parameters)
        elif connection_type == CommType.TCP:
            communication_pipe = \
                await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                        CommsInstructionID.SET_CONNECTION_TCP,
                                                                        parameters)
        elif connection_type == CommType.SERIAL_FTDI:
            communication_pipe = \
                await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                        CommsInstructionID.SET_CONNECTION_SERIAL_FTDI,
                                                                        parameters)
            self._connected = True

        elif connection_type == CommType.SERIAL_LIBFTDI:
            raise NotImplementedError

        self.connection_type = connection_type
        logger.info(f"Connection {self.connection_device_id} Successfully set type: {connection_type}")

        await asyncio.sleep(0.05)

        if self._communication_pipe is None:
            logger.info(f"Connection {self.connection_device_id} creating new communication_pipe")
            # If communication_pipe is new then create new pipe
            # noinspection PyProtectedMember
            logger.info(
                f"_client_, Creating a new device_connection communication_pipe for Pipe: Handle: {communication_pipe.fileno()}")
            self.connection_type = connection_type
            self._communication_pipe = communication_pipe
            if self._bpl_comm_protocol:
                self._bpl_comm_protocol.connection.disconnect()
            # self._bpl_comm_protocol = communication_pipe

            self._pipe_connection = PipeConnection(communication_pipe)
            # asyncio.create_task(pipe_connection.connect())
            await self._pipe_connection.connect()

            if self._bpl_comm_protocol is not None:
                for key, callback_set in self.packet_callbacks.items():
                    device_id = key[0]
                    packet_id = key[1]
                    for callback in callback_set:
                        self._bpl_comm_protocol.remove_packet_callback(device_id, packet_id, callback)
                for key, callback_set in self.heartbeat_callbacks.items():
                    device_id = key[0]
                    packet_id = key[1]
                    for callback in callback_set:
                        self._bpl_comm_protocol.remove_packet_callback(device_id, packet_id, callback)

            self._bpl_comm_protocol = BPLPacketProtocol(self._pipe_connection)

            for key, callback_set in self.packet_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callback_set:
                    self._bpl_comm_protocol.add_packet_callback(device_id, packet_id, callback)

            for key, callback_set in self.heartbeat_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callback_set:
                    self._bpl_comm_protocol.add_packet_callback(device_id, packet_id, callback)


        else:
            logger.info(f"Connection {self.connection_device_id} communication_pipe already created")

    async def start(self):
        await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                CommsInstructionID.START_CONNECTION,
                                                                None)

    async def stop(self):
        await self._instruction_protocol.async_send_instruction(self.connection_device_id,
                                                                CommsInstructionID.STOP_CONNECTION,
                                                                None)

    def non_async_stop(self):
        self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.STOP_CONNECTION,
                                                    None)

    def send_bytes(self, data: bytes):
        if self._bpl_comm_protocol is not None:
            self._bpl_comm_protocol.connection.send_bytes(data)

    def send(self, device_id, packet_id, data: Union[int, float, List[float], List[int], bytes, bytearray]):
        """ Send a packet.
        Able to take in a varying range in input types for data.
        Will covert to bytes here and send.
        """
        if isinstance(data, list):
            data = [float(x) if isinstance(x, float) else int(x) for x in data]

        self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.SEND_PACKET,
                                                    (device_id, packet_id, data))


    async def request(self, device_id, packet_id, timeout: Optional[float] = 0.2):
        """ Request from device. Asyncronous. will await untill a resonse is received or it timeouts.
        if it times out """

        self.send(device_id, PacketID.REQUEST, [packet_id])
        if self._bpl_comm_protocol:
            try:
                return await self._bpl_comm_protocol.wait_for_packet(device_id, packet_id, timeout)

            except TimeoutError:
                # logger.warning("request() Request for {device_id, packet_id} timed out: {timeout}")
                raise TimeoutError
        else:
            logger.warning("request(): _bpl_comm_protocol is not initialised.")
            return []

    async def set(self, device_id: int, packet_id: int, data: Union[List[float], List[int], float, int], timeout=0.2):
        if self._bpl_comm_protocol:
            return await self._bpl_comm_protocol.set(device_id, packet_id, data, timeout)
        else:
            logger.warning("set(): _bpl_comm_protocol is not initialised.")
            return False

    def add_bytes_callback(self, callback):

        self._bytes_callbacks.add(callback)

        if self._pipe_connection:
            self._pipe_connection.add_bytes_callback(callback)

    def remove_bytes_callback(self, callback):

        if callback in self._bytes_callbacks:
            self._bytes_callbacks.remove(callback)

        if self._pipe_connection:
            self._pipe_connection.remove_bytes_callback(callback)


    def add_packet_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, Union[List[float], List[int]]], None]):

        key = (device_id, packet_id)

        if key not in self.packet_callbacks:
            self.packet_callbacks[key] = {callback}
        else:
            self.packet_callbacks[key].add(callback)

        if self._bpl_comm_protocol:
            self._bpl_comm_protocol.add_packet_callback(device_id, packet_id, callback)


    def remove_packet_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, Union[List[float], List[int]]], None]):

        key = (device_id, packet_id)
        if key in self.packet_callbacks:
            if callback in self.packet_callbacks[key]:
                self.packet_callbacks[key].remove(callback)

                if len(self.packet_callbacks[key]) == 0:
                    del self.packet_callbacks[key]

        if self._bpl_comm_protocol:
            self._bpl_comm_protocol.remove_packet_callback(device_id, packet_id, callback)

    _heartbeat_loop_task: Optional[asyncio.Task] = None
    def add_heartbeat_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, Union[List[float], List[int]]], None], frequency=10.0):

        key = (device_id, packet_id)

        if key not in self.heartbeat_callbacks:
            self.heartbeat_callbacks[key] = {callback: frequency}
        else:
            self.heartbeat_callbacks[key][callback] = frequency

        if self._heartbeat_loop_task is None or self._heartbeat_loop_task.done():
            self._heartbeat_loop_task = asyncio.create_task(self.heartbeat_loop())

        if self._bpl_comm_protocol:
            self._bpl_comm_protocol.add_packet_callback(device_id, packet_id, callback)
        # if self._bpl_comm_protocol:
        #     self._bpl_comm_protocol.add_heartbeat_callback(device_id, packet_id, callback)

    def remove_heartbeat_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, Union[List[float], List[int]]], None]):

        key = (device_id, packet_id)
        if key in self.heartbeat_callbacks:
            if callback in self.heartbeat_callbacks[key]:
                self.heartbeat_callbacks[key].pop(callback)

                if len(self.heartbeat_callbacks[key]) == 0:
                    del self.heartbeat_callbacks[key]

        if self._bpl_comm_protocol:
            self._bpl_comm_protocol.remove_packet_callback(device_id, packet_id, callback)
        # if self._bpl_comm_protocol:
        #     self._bpl_comm_protocol.remove_heartbeat_callback(device_id, packet_id, callback)

    max_heartbeat_frequency = 20

    async def heartbeat_loop(self):
        """
        Runs the heartbeat loop.
        The heartbeat loop looks at the heartbeat callbacks.
        The heartbeat callbacks have a device_id, packet_id, callback and heartbeat frequency.
        For any specific device_id, packet_id pair, heartbeats are requested only the maximum frequency of the set.

        """

        # Heart
        heartbeat_times: Dict[float, float] = {}

        while len(self.heartbeat_callbacks):
            # Request heartbeats
            # print("Running heartbeat loop")
            from RC_Core.connectionmanager import ConnectionManager
            if ConnectionManager.instance.run_connection_loop:

                current_time = time.time()

                request_packets: Dict[int, Set[int]] = {}

                for key, callbacks in self.heartbeat_callbacks.items():
                    device_id = key[0]
                    packet_id = key[1]

                    if len(callbacks) == 0:
                        continue

                    max_frequency = max(callbacks.values())

                    if max_frequency not in heartbeat_times:
                        heartbeat_times[max_frequency] = time.time()

                    # for callback, frequency in value.items():
                    #     if frequency not in heartbeat_times:
                    #         heartbeat_times[frequency] = time.time()

                    if current_time >= heartbeat_times[max_frequency]:
                        if device_id in request_packets:
                            request_packets[device_id].add(packet_id)
                        else:
                            request_packets[device_id] = {packet_id}

                        # self.send(device_id, PacketID.REQUEST, [packet_id])
                        # print("Requesting", device_id, packet_id)

                for device_id, packets in request_packets.items():
                    data_list = list(packets)
                    for i in range(((len(packets) // 10) + 1)):
                        self.send(device_id, PacketID.REQUEST, list(data_list[i:i+10]))
                    # print(f"requesting {device_id, data_list}")

                for freq, hb_time in heartbeat_times.items():
                    if current_time >= hb_time:
                        heartbeat_times[freq] = current_time + 1 / min(freq, self.max_heartbeat_frequency)

                if len(heartbeat_times) > 0:
                    next_time = min(heartbeat_times.values())

                    # Delete if next_time < current time. This means there are not more packets to serve
                    if next_time < current_time:
                        del heartbeat_times[list(heartbeat_times.keys())[list(heartbeat_times.values()).index(next_time)]]

                    await asyncio.sleep(next_time - current_time)

                else:
                    await asyncio.sleep(1 / self.max_heartbeat_frequency)

            else:
                await asyncio.sleep(1 / self.max_heartbeat_frequency)

    connected_callbacks: Set[Callable[[bool], None]] = None


    def _connected_callback(self, _, __, connected: bool):
        logger.debug(f"_connected_callback() received {connected}")
        self._connected = connected

        for callback in self.connected_callbacks:
            callback(connected)

    def toggle_half_duplex(self, state):

        if state:
            self.max_heartbeat_frequency = 10
        else:
            self.max_heartbeat_frequency = 20

        self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.TOGGLE_HALF_DUPLEX,
                                                    [state])

    def set_comms_protocol(self, protocol_type):
        self._bpl_comm_protocol.protocol_type = protocol_type
        self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.SET_COMMS_PROTOCOL,
                                                    [protocol_type])

    def create_master_arm_slave(self, slave_connection):

        if slave_connection and slave_connection.device_connection:
            self._instruction_protocol.send_instruction(self.connection_device_id,
                                                        CommsInstructionID.ADD_MA_SLAVE,
                                                        [slave_connection.device_connection.connection_device_id])
        else:
            self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.ADD_MA_SLAVE,
                                                    [None])

    def remove_master_arm_slave(self):
        self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.REMOVE_MA_SLAVE,
                                                    None)

    def send_control(self, device_id, packet_id, data):
        self._instruction_protocol.send_instruction(self.connection_device_id,
                                                    CommsInstructionID.SEND_CONTROL_PACKET,
                                                    (device_id, packet_id, data))