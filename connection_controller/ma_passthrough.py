import logging
import time
from typing import Optional, List, Union

from colorlog import ColoredFormatter

from RC_Core.RS1_hardware import Mode
from RC_Core.connection_controller.connection_forwarder import ConnectionForwarder
from RC_Core.connections import ConnectionBase
from RC_Core.enums.packets import PacketID
from RC_Core.protocols import BPLPacketProtocol
from RC_Core.protocols.bpl_protocol import BPLProtocol

PAUSE_MODES = {Mode.DISABLED, Mode.STANDBY, Mode.POSITION_HOLD, Mode.ZERO_VELOCITY}

MA_TIMEOUT = 3.0 # seconds

import rc_logging
logger = rc_logging.getLogger(__name__)

class BPLProtocolForwarder:

    BIDIRECTIONAL = 0
    A_TO_B = 1
    B_TO_A = 3
    DISABLED = 4
    # Forward once connection to another
    _connection_a: Optional[ConnectionBase] = None
    _protocol_a: Optional[BPLProtocol] = None
    _connection_b: Optional[ConnectionBase] = None
    _protocol_b: Optional[BPLProtocol] = None

    mode = BIDIRECTIONAL

    separator: Optional[bytes] = None

    @property
    def protocol_a(self):
        return self._protocol_a

    @protocol_a.setter
    def protocol_a(self, protocol):
        self._protocol_a = protocol
        if self._protocol_a is not None:
            self.connection_a = self._protocol_a.connection
        else:
            self.connection_a = None

    @property
    def protocol_b(self):
        return self._protocol_b

    @protocol_b.setter
    def protocol_b(self, protocol):
        self._protocol_b = protocol
        if self._protocol_b is not None:
            self.connection_b = self._protocol_b.connection
        else:
            self.connection_b = None

    @property
    def connection_a(self) -> ConnectionBase:
        return self._connection_a

    @connection_a.setter
    def connection_a(self, connection):
        if self._connection_a:
            self._connection_a.remove_bytes_callback(self._connection_a_callback)
        self._connection_a = connection

        self._a_incomplete_bytes = b''
        if self._connection_a is not None:

            if self._connection_b == self._connection_a:
                logger.warning("CONNECTION_A AND CONNECTION_B ARE THE SAME.")
            self._connection_a.add_bytes_callback(self._connection_a_callback)

    @property
    def connection_b(self) -> ConnectionBase:
        return self._connection_b

    @connection_b.setter
    def connection_b(self, connection: ConnectionBase):
        if self._connection_b:
            self._connection_b.remove_bytes_callback(self._connection_b_callback)
        self._connection_b = connection
        self._b_incomplete_bytes = b''
        if self._connection_b is not None:

            if self._connection_b == self._connection_a:
                logger.warning("CONNECTION_A AND CONNECTION_B ARE THE SAME")

            self._connection_b.add_bytes_callback(self._connection_b_callback)

    _a_incomplete_bytes: bytes = b''

    def _connection_a_callback(self, data: bytes):
        """ Callback to forward connection a to connection b"""

        if self.mode in {self.BIDIRECTIONAL, self.A_TO_B}:
            if self._protocol_b:

                if self.separator is None:
                    self._protocol_b.send_bytes(data)
                else:
                    index = data.rfind(self.separator) + 1
                    send_data = self._a_incomplete_bytes + data[:index]
                    self._a_incomplete_bytes = data[index:]
                    if send_data != b'':
                        self._protocol_b.send_bytes(send_data)

    _b_incomplete_bytes: bytes = b''

    def _connection_b_callback(self, data: bytes):
        """ Callback to forward connection b to connection a"""
        if self.mode in {self.BIDIRECTIONAL, self.B_TO_A}:
            if self._protocol_a:
                if self.separator is None:
                    self._protocol_a.send_bytes(data)

                else:
                    index = data.rfind(self.separator) + 1
                    data = self._b_incomplete_bytes + data[:index]
                    self._b_incomplete_bytes = data[index:]
                    if data != b'':
                        self._protocol_a.send_bytes(data)


class MAPassthrough:

    _master_connection = None  # Managed Connections
    _slave_connection = None   # Managed Connections

    _base_device_id: int = None

    _mode: int = Mode.DISABLED

    _master_bpl_protocol: Optional[BPLPacketProtocol] = None

    _master_slave_forwarder: BPLProtocolForwarder = None

    _slave_bpl_protocol: Optional[BPLPacketProtocol] = None

    def __init__(self, master_connection, slave_connection):
        self._base_device_id = 0xCE
        self._master_slave_forwarder = BPLProtocolForwarder()
        self._master_slave_forwarder.separator = b'\x00'
        self._master_slave_forwarder.mode = BPLProtocolForwarder.A_TO_B

        self.master_connection = master_connection
        self.slave_connection = slave_connection

        self.last_bytes_timestamp = time.perf_counter()

    @property
    def master_connection(self):
        return self._master_connection

    @master_connection.setter
    def master_connection(self, connection):
        if self._master_connection and self._master_connection.connection:
            self._master_connection.connection.remove_bytes_callback(self.master_bytes_callback)

        self._master_connection = connection
        if self._master_connection and self._master_connection.connection:
            self._master_connection.connection.add_bytes_callback(self.master_bytes_callback)

            self._master_bpl_protocol = self._master_connection.bpl_protocol
            # if self._master_bpl_protocol:
            #     self._master_bpl_protocol.connection = self._master_connection
            # else:
            #     self._master_bpl_protocol = BPLPacketProtocol(self._master_connection)

            self._master_bpl_protocol.add_packet_callback(self.base_device_id, PacketID.MODE, self.mode_callback)
        else:
            self._master_bpl_protocol = None

        self._master_slave_forwarder.protocol_a = self._master_bpl_protocol

    def master_bytes_callback(self, _: bytes):
        self.last_bytes_timestamp = time.perf_counter()

    @property
    def slave_connection(self):
        return self._slave_connection

    @slave_connection.setter
    def slave_connection(self, connection):
        self._slave_connection = connection

        if self._slave_connection and self._slave_connection.connection:

            self._slave_bpl_protocol = self._slave_connection.bpl_protocol
            # if self._slave_bpl_protocol:
            #     self._slave_bpl_protocol.connection = self._slave_connection
            # else:
            #     self._slave_bpl_protocol = BPLPacketProtocol(self._slave_connection)
        else:
            # if self._slave_bpl_protocol is not None:
            #     del self._slave_bpl_protocol
            self._slave_bpl_protocol = None

        logger.debug(f"Changing Slave protocol to {self._slave_bpl_protocol}")
        self._master_slave_forwarder.protocol_b = self._slave_bpl_protocol


        # self._master_slave_forwarder.connection_b = self._slave_connection

    @property
    def base_device_id(self) -> int:
        return self._base_device_id

    @property
    def passthrough_disabled(self) -> bool:
        if self._master_slave_forwarder.mode != ConnectionForwarder.DISABLED:
            return False
        else:
            return True

    @passthrough_disabled.setter
    def passthrough_disabled(self, state: bool):
        if state:
            self._master_slave_forwarder.mode = ConnectionForwarder.DISABLED
        else:
            self._master_slave_forwarder.mode = ConnectionForwarder.A_TO_B

    @property
    def mode(self):
        return self._mode

    def send_control_packet(self, device_id, packet_id, data) -> bool:
        if not self.is_paused and (time.perf_counter() - self.last_bytes_timestamp) < MA_TIMEOUT:
            return False
        else:
            self.passthrough_disabled = True

            if self._slave_bpl_protocol:
                self._slave_bpl_protocol.send_packet(device_id, packet_id, data)
        pass

    def mode_callback(self, device_id: int, packet_id: int, data: Union[List[float], List[int]]):
        if device_id == self.base_device_id and packet_id == PacketID.MODE:
            mode = data[0]
            self._mode = int(mode)

        # Disable the passthrough if it is exits paused mode.
        if self.passthrough_disabled and not self.is_paused:
            self.passthrough_disabled = False

    @property
    def is_paused(self) -> bool:
        if self._mode in PAUSE_MODES:
            return True
        else:
            return False

    def close(self):
        logger.debug("close")
        self.master_connection = None
        self.slave_connection = None

    def __del__(self):
        self.close()
        logger.debug("REMOVE_MA_SLAVE")


