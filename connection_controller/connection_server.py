import asyncio
import logging
import sys
from typing import Dict

from colorlog import ColoredFormatter


from RC_Core.connections import PipeConnection
from RC_Core.enums.packets import PacketID
from multiprocessing.connection import Connection as MultiprocessingConnection
from RC_Core.protocols import InstructionServerProtocol

from .managed_connection import ManagedConnection
from .comms_instruction_id import CommsInstructionID

import rc_logging
logger = rc_logging.getLogger(__name__)


class ConnectionServer:
    """ Connection Manager is the server that runs on a separate process that connects to all the
    connections needed for an application

    Uses the RC_Core.protocols.InstructionServerProtocol to communicate instructions between the server and client.

    Connections are described through Device IDs. 0xFF Device ID will mean instructions to broadcast to all the devices.

    Device IDs will describe each connection.
    01, 02, 03, 04, 05, 06, 07, 08, 09, 0A ....

    Instructions must be sent/ received using a specific COMMS_INSTRUCTION_PACKET_ID.

    Functions The Connection Manager must be able to do.

    SET_CONNECTION_NONE
    SET_CONNECTION_SERIAL  arguments: port, baudrate, bytesize, parity, stopbits
    SET_CONNECTION_UDP  arguments: IP, PORT
    SET_CONNECTION_TCP  arguments: IP, PORT
    SET_CONNECTION_SERIAL_FTDI     arguments: port, baudrate, bytesize, parity, stopbits

    START_CONNECTION: starts initiating comms (will enable reconnect when a connection is lost)

    STOP_CONNECTION: stops comms (will disable reconnect)

    GET_CONNECTED: Check if the connection is connected.
    connected does not always reflects if an active connection is detected. If this is false, it definitely is not
    connected. If True, it could still not be properly connected.


    connections are to be held in a dict.
    If connection does not exist, create it.


    """

    _instruction_protocol: InstructionServerProtocol = None

    connections: Dict[int, ManagedConnection] = None

    def __init__(self, instruction_pipe: MultiprocessingConnection):

        logger.info(f"Creating PipeConnection for Server instruction_connection handle: {instruction_pipe.fileno()}")
        instruction_connection = PipeConnection(instruction_pipe)
        self._instruction_protocol = InstructionServerProtocol(PacketID.COMMS_INSTRUCTION, instruction_connection)

        self._instruction_protocol.add_missed_instruction_callback(self._missed_instruction_callback)

        instruction_connection.add_connected_callback(self.connected_callback)

        self.connections = {}

        loop = asyncio.get_event_loop()
        loop.create_task(instruction_connection.connect())

    def _missed_instruction_callback(self, device_id, instruction_id, parameters):
        # Create a connection device for device_id
        # If it is a create instruction then create a new connection under device id.
        logger.info(f"Received missed instruction. {device_id, instruction_id, parameters}")

        if instruction_id in [CommsInstructionID.SET_CONNECTION_NONE,
                              CommsInstructionID.SET_CONNECTION_SERIAL,
                              CommsInstructionID.SET_CONNECTION_UDP,
                              CommsInstructionID.SET_CONNECTION_TCP]:

            if device_id in self.connections:
                logger.warning("Connection already exists. Deleting to accommodate")
                del self.connections[device_id]

            logger.info(f"Server: Creating connection ID: {device_id}")

            self.connections[device_id] = ManagedConnection(self, device_id, self._instruction_protocol)

            connection = self.connections[device_id]

            self._instruction_protocol.add_instruction_callback(device_id,
                                                                0xFF,
                                                                connection.instruction_callback)

            logger.info(f"Server: Created connection ID: {device_id}")

            return connection.instruction_callback(device_id, instruction_id, parameters)

        else:
            logger.warning(f"Inappropriate instruction id: {instruction_id}, Connection {device_id} does not exist")
            return None

    def connected_callback(self, state):

        logger.error("Detected that the pipe closed, Closing process")
        if not state:
            sys.exit()
