from enum import IntEnum


class CommsInstructionID(IntEnum):
    SET_CONNECTION_NONE = 1  # Use this to remove disconnect any current connection
    SET_CONNECTION_SERIAL = 2  # arguments, Port, baudrate, parity, bytesize, stopbits
    SET_CONNECTION_UDP = 3  # arguments, IP_ADDRESS, PORT
    SET_CONNECTION_TCP = 4
    SET_CONNECTION_SERIAL_FTDI = 5  # arguments, Port, baudrate, parity, bytesize, stopbits

    START_CONNECTION = 10  # Initiate a Connection
    STOP_CONNECTION = 11  # Pause a Connection
    GET_CONNECTED = 12

    TOGGLE_HALF_DUPLEX = 13  # Toggle Half duplex (True/ False)

    SET_COMMS_PROTOCOL = 14

    SEND_PACKET = 20  # Send packet device_id, packet_id, data
    REQUEST = 21

    SEND_CONTROL_PACKET = 22

    ADD_MA_SLAVE = 23
    REMOVE_MA_SLAVE = 24
