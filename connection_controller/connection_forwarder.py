import asyncio
import logging
from typing import Optional

from colorlog import ColoredFormatter

from RC_Core.connections import ConnectionBase

import rc_logging
logger = rc_logging.getLogger(__name__)


class ConnectionForwarder:

    BIDIRECTIONAL = 0
    A_TO_B = 1
    B_TO_A = 3
    DISABLED = 4
    # Forward once connection to another
    _connection_a: Optional[ConnectionBase] = None
    _connection_b: Optional[ConnectionBase] = None

    mode = BIDIRECTIONAL

    separator: Optional[bytes] = None

    @property
    def connection_a(self) -> ConnectionBase:
        return self._connection_a

    @connection_a.setter
    def connection_a(self, connection):
        if self._connection_a:
            self._connection_a.remove_bytes_callback(self._connection_a_callback)
        self._connection_a = connection

        self._a_incomplete_bytes = b''
        if self._connection_a is not None:

            if self._connection_b == self._connection_a:
                logger.warning("CONNECTION_A AND CONNECTION_B ARE THE SAME.")
            self._connection_a.add_bytes_callback(self._connection_a_callback)





    @property
    def connection_b(self) -> ConnectionBase:
        return self._connection_b

    @connection_b.setter
    def connection_b(self, connection: ConnectionBase):
        if self._connection_b:
            self._connection_b.remove_bytes_callback(self._connection_b_callback)
        self._connection_b = connection
        self._b_incomplete_bytes = b''
        if self._connection_b is not None:

            if self._connection_b == self._connection_a:
                logger.warning("CONNECTION_A AND CONNECTION_B ARE THE SAME")

            self._connection_b.add_bytes_callback(self._connection_b_callback)

    _a_incomplete_bytes: bytes = b''

    def _connection_a_callback(self, data: bytes):
        """ Callback to forward connection a to connection b"""

        if self.mode in {self.BIDIRECTIONAL, self.A_TO_B}:
            if self.connection_b:

                if self.separator is None:
                    self.connection_b.send_bytes(data)
                else:
                    index = data.rfind(self.separator) + 1
                    send_data = self._a_incomplete_bytes + data[:index]
                    self._a_incomplete_bytes = data[index:]
                    if send_data != b'':
                        self.connection_b.send_bytes(send_data)

    _b_incomplete_bytes: bytes = b''

    def _connection_b_callback(self, data: bytes):
        """ Callback to forward connection b to connection a"""
        if self.mode in {self.BIDIRECTIONAL, self.B_TO_A}:
            if self.connection_a:
                if self.separator is None:
                    self.connection_a.send_bytes(data)

                else:
                    index = data.rfind(self.separator) + 1
                    data = self._b_incomplete_bytes + data[:index]
                    self._b_incomplete_bytes = data[index:]
                    if data != b'':
                        self.connection_a.send_bytes(data)

