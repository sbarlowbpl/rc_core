import asyncio
import logging
from multiprocessing import Pipe
from multiprocessing.connection import Connection as MultiprocessingConnection
from typing import Optional

from colorlog import ColoredFormatter

from RC_Core.comm_type import CommType
from .comms_instruction_id import CommsInstructionID
from RC_Core.connection_controller.connection_forwarder import ConnectionForwarder
from RC_Core.connections import PipeConnection, ConnectionBase, SerialConnection, UDPConnection
from RC_Core.connections.tcp_connection import TCPConnection
from .ma_passthrough import MAPassthrough
from ..connections.serial_ftdi_connection import SerialFTDIConnection
from ..enums import CommsProtocol
from ..protocols import InstructionServerProtocol
from ..protocols.bpl_protocol import BPLPacketProtocol

import rc_logging
logger = rc_logging.getLogger(__name__)


class ManagedConnection:
    """
    Managed Server connection.
    """

    MA_MASTER = 0
    MA_SLAVE = 1

    _server_connection: PipeConnection = None
    _client_pipe: MultiprocessingConnection = None

    # connection_type: CommType = None

    device_id: int = None

    _connection: Optional[ConnectionBase] = None

    _connection_forwarder: ConnectionForwarder = None

    _bpl_protocol: Optional[BPLPacketProtocol] = None

    _ma_passthrough: Optional[MAPassthrough] = None
    _ma_type: Optional[int] = None

    half_duplex: bool = False

    comms_protocol = CommsProtocol.BPL

    @property
    def bpl_protocol(self) -> Optional[BPLPacketProtocol]:
        return self._bpl_protocol

    @property
    def connection_type(self) -> CommType:
        if self._connection is None:
            return CommType.NONE
        elif isinstance(self._connection, SerialConnection):
            return CommType.SERIAL
        elif isinstance(self._connection, UDPConnection):
            return CommType.UDP
        elif isinstance(self._connection, TCPConnection):
            return CommType.TCP
        elif isinstance(self._connection, SerialFTDIConnection):
            return CommType.SERIAL_FTDI

        else:
            raise TypeError(f"Unknown Connection Type {self._connection}")

    def __init__(self, connection_server, device_id: int, server_instruction_protocol: InstructionServerProtocol):
        # Get a pipe
        from . import ConnectionServer
        self._connection_server: ConnectionServer = connection_server

        server_pipe, self._client_pipe = Pipe()

        logger.info(f"Created server and client pipe for connection {device_id}: "
                    f"handle_a: {server_pipe.fileno()}, {self._client_pipe.fileno()}")
        self.device_id = device_id

        self.server_instruction_protocol: InstructionServerProtocol = server_instruction_protocol

        logger.info(f"Creating PipeConnection for server_pipe for connection: {device_id} handle: {server_pipe.fileno()}")
        self._server_connection = PipeConnection(server_pipe)

        self._connection = None

        asyncio.create_task(self._server_connection.connect())

        self._connection_forwarder = ConnectionForwarder()
        self._connection_forwarder.connection_b = self._server_connection

    @property
    def ma_passthrough(self):
        return self._ma_passthrough

    @ma_passthrough.setter
    def ma_passthrough(self, ma):
        self._ma_passthrough = ma

    def instruction_callback(self, _, instruction_id, parameters):
        if instruction_id == CommsInstructionID.SET_CONNECTION_NONE:
            return self.set_none(parameters)
        elif instruction_id == CommsInstructionID.SET_CONNECTION_SERIAL:
            return self.set_serial(parameters)
        elif instruction_id == CommsInstructionID.SET_CONNECTION_UDP:
            return self.set_udp(parameters)
        elif instruction_id == CommsInstructionID.SET_CONNECTION_TCP:
            return self.set_tcp(parameters)
        elif instruction_id == CommsInstructionID.SET_CONNECTION_SERIAL_FTDI:
            return self.set_serial_ftdi(parameters)
        elif instruction_id == CommsInstructionID.START_CONNECTION:
            return self.start(parameters)
        elif instruction_id == CommsInstructionID.STOP_CONNECTION:
            return self.stop(parameters)
        elif instruction_id == CommsInstructionID.GET_CONNECTED:
            return self.get_connected(parameters)
        elif instruction_id == CommsInstructionID.SEND_PACKET:
            if self._bpl_protocol:
                self._bpl_protocol.send_packet(*parameters)
            return 1

        elif instruction_id == CommsInstructionID.REQUEST:
            if self._bpl_protocol:
                asyncio.create_task(self._request(*parameters))
            return None

        elif instruction_id == CommsInstructionID.TOGGLE_HALF_DUPLEX:
            self.half_duplex = parameters[0]
            if self._bpl_protocol:
                self._bpl_protocol.half_duplex = parameters[0]

            return True

        elif instruction_id == CommsInstructionID.SET_COMMS_PROTOCOL:
            self.comms_protocol = parameters[0]
            if self._bpl_protocol:
                self._bpl_protocol.protocol_type = parameters[0]

            return True

        elif instruction_id == CommsInstructionID.ADD_MA_SLAVE:
            logger.info("Creating Master arm slave")
            slave_connection_id = parameters[0]

            if slave_connection_id == self.device_id:
                return False

            self._ma_type = self.MA_MASTER
            if slave_connection_id in self._connection_server.connections:
                # slave_connection = self._connection_server.connections[slave_connection_id].connection
                slave_managed_connection = self._connection_server.connections[slave_connection_id]
            else:
                # slave_connection = None
                slave_managed_connection = None

            if self._ma_passthrough:
                self._ma_passthrough.slave_connection = slave_managed_connection
            else:
                self._ma_passthrough = MAPassthrough(self, slave_managed_connection)

            # if self._ma_passthrough:
                # del self._ma_passthrough

            if slave_managed_connection:
                slave_managed_connection._ma_passthrough = self._ma_passthrough
                slave_managed_connection._ma_type = self.MA_SLAVE
            return True

        elif instruction_id == CommsInstructionID.REMOVE_MA_SLAVE:
            logger.debug("REMOVE_MA_SLAVE")
            if self._ma_passthrough:
                for managed_conn in self._connection_server.connections.values():

                    # Find the slave and set the save to none
                    if managed_conn.ma_passthrough is self._ma_passthrough and managed_conn is not self:
                        managed_conn.ma_passthrough = None
                        logger.info("Setting slave ma to none")

                self._ma_passthrough.close()
                del self._ma_passthrough
                self._ma_passthrough = None
            self._ma_type = None

            return True
        elif instruction_id == CommsInstructionID.SEND_CONTROL_PACKET:
            if self._ma_passthrough and self._ma_type == self.MA_SLAVE:
                self._ma_passthrough.send_control_packet(*parameters)
            else:
                if self._bpl_protocol:
                    self._bpl_protocol.send_packet(*parameters)
        else:
            logger.warning(f"Unknown instruction id: {instruction_id}")
        return None

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, connection: Optional[ConnectionBase]):
        if self._connection:
            self._connection.disconnect()
            self._connection.remove_connected_callback(self.connected_callback)

        self._connection = connection
        if self._connection:
            self._connection.add_connected_callback(self.connected_callback)

            if self._bpl_protocol:
                self._bpl_protocol.connection = self._connection
            else:
                self._bpl_protocol = BPLPacketProtocol(self._connection)
                if self._ma_passthrough:
                    if self._ma_type == self.MA_MASTER:
                        self._ma_passthrough.master_connection = self

                    elif self._ma_type == self.MA_SLAVE:
                        self._ma_passthrough.slave_connection = self

            self._bpl_protocol.half_duplex = self.half_duplex

            self._bpl_protocol.protocol_type = self.comms_protocol

        else:
            self._bpl_protocol = None

        self._connection_forwarder.connection_a = self._connection

    async def _request(self, device_id, packet_id, timeout):
        try:
            response = await self._bpl_protocol.request(device_id, packet_id, timeout)
        except TimeoutError as e:
            response = e
        self.server_instruction_protocol.send_instruction(self.device_id,
                                                          CommsInstructionID.REQUEST,
                                                          response)

    def set_none(self, _):
        logger.debug(f"ID: {self.device_id} set_none()")
        self.connection = None
        return self._client_pipe

    def set_serial(self, params):
        logger.debug(f"ID: {self.device_id} set_serial({params})")
        if self.connection_type == CommType.SERIAL:
            self.connection.reconfigure(*params)
        else:
            if self.connection:
                if self.connect_task and not self.connect_task.done():
                    logger.info("Cancelling connect_task")
                    self.connect_task.cancel()
                self.connection.disconnect()

            self.connection = SerialConnection(*params)
        return self._client_pipe

    def set_udp(self, params):
        logger.debug(f"ID: {self.device_id} set_udp({params})")
        if self.connection_type == CommType.UDP:
            self.connection.reconfigure(*params)
        else:
            if self.connection:
                if self.connect_task and not self.connect_task.done():
                    logger.info("Cancelling connect_task")
                    self.connect_task.cancel()
                self.connection.disconnect()
            self.connection = UDPConnection(*params)
        return self._client_pipe

    def set_tcp(self, params):
        logger.debug(f"ID: {self.device_id} set_tcp({params})")
        if self.connection_type == CommType.TCP:
            self.connection.reconfigure(*params)
        else:
            if self.connection:
                if self.connect_task and not self.connect_task.done():
                    logger.info("Cancelling connect_task")
                    self.connect_task.cancel()
                self.connection.disconnect()
            self.connection = TCPConnection(*params)
        return self._client_pipe

    def set_serial_ftdi(self, params):
        logger.debug(f"ID: {self.device_id} set_serial_ftdi({params})")
        if self.connection_type == CommType.SERIAL_FTDI:
            self.connection.reconfigure(*params)
        else:
            if self.connection:
                if self.connect_task and not self.connect_task.done():
                    logger.info("Cancelling connect_task")
                    self.connect_task.cancel()
                self.connection.disconnect()
            self.connection = SerialFTDIConnection(*params)
        return self._client_pipe


    connect_task: asyncio.Task = None
    def start(self, _):
        logger.debug(f"ID: {self.device_id} start()")

        if self.connect_task is not None and not self.connect_task.done():
            logger.info(f"start() cancelling connect_task")
            self.connect_task.cancel()

        if self.connection is not None:
            self.connect_task = asyncio.create_task(self.connection.connect())

        return 1

    def stop(self, _):
        logger.debug(f"ID: {self.device_id} stop()")
        if self.connection is not None:
            self.connection.disconnect()
        return False


    def get_connected(self, _):
        return self.connection.connected

    def connected_callback(self, connected: bool):
        logger.debug(f"connected_callback(): Sending {connected}")
        self.server_instruction_protocol.send_instruction(self.device_id, CommsInstructionID.GET_CONNECTED, connected)
