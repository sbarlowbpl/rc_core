# Serial functions for BPSS protocol

from kivy.app import App
import serial,struct,re, binascii
from cobs import cobs
from RC_Core.packet_structure import packet_format, Container
from crcmod import crcmod
from kivy.clock import Clock
from functools import partial
from RC_Core.construct import *
from RC_Core.RS1_hardware import PacketID, Mode
from array import array
import time

from RC_Core.packetid import Packets

IGNORE_CRC = True


def float_to_hex(f):
    output = hex(struct.unpack('<I', struct.pack('<f', f))[0])[2:].upper().encode()
    # print('float_to_hex() output:', output, 'length:', len(output))
    if f == 0:
        return b'00000000'
    # if output is b'0':
    #     return b'00000000'
    return output

def hex_to_float(h):
    # print("hex in:", h)
    try:
        return struct.unpack('!f', bytes.fromhex(h))[0]
    except:
        return 0

def hex_to_string(h):
    hex_data_in = h
    out_string = ''
    while hex_data_in is not '':
        this_char = int(hex_data_in[0:2], 16)
        hex_data_in = hex_data_in[2:]
        out_string = str(chr(this_char)) + out_string
    return out_string


def convert_to_bytes(parameter):
    # Convert numeric property velocity into bytes

    # print("actual data value = ", parameter)
    # print(type(parameter).__name__)
    if type(parameter).__name__ == 'float':
        # print('converting FLOAT')
        bytes_value = float_to_hex(parameter)
        # print("float received:", bytes_value, type(bytes_value))
    elif type(parameter).__name__ == 'int':
        # print('converting INT')
        # bytes_value = bytearray(struct.pack("b", parameter))
        # bytes_value = bytearray(struct.pack("i", parameter))
        bytes_value = b"%02X" % parameter
        # print("int received:", bytes_value)

    elif type(parameter).__name__ == 'list':
        # print('converting LIST')
        bytes_value = bytearray()
        for val in parameter:
            # bytes_value += bytearray(struct.pack("f", val))
            bytes_value = float_to_hex(val) + bytes_value
        # print(bytes_value)
    elif type(parameter).__name__ == 'bytearray':
        bytes_value = b''
        for byte in parameter:
            bytes_value = b"%02X" % byte + bytes_value
    else:
        return parameter

    # vel_bytes = bytes([self.velocity])
    # print("bytes_value is:", bytes_value)
    # print([ "0x%02x" % b for b in bytes_value ])
    return bytes_value


class SerialDevice():

    port=None
    baud=None
    ipAddress=None
    comsType='SERIAL'
    serialPort=serial.Serial()
    UDPClientsocket=None
    UDPServerSocket=None

    txPacketId=0
    txData=b''
    txDeviceId=0

    rxPacketId=0
    rxData=[0]
    rxDeviceId=0

    incomplete_pckt=b''

    packets=[]
    pckt=b''
    decoded_pckt=b''
    protocol = "BPSS"

    verbose = False # set True for print statements

    def __init__(self):
        self.float_packets_list = Packets().get_float_packet_ids()

    def open(self):
        
        print(self.baud)
        print(self.port)
        self.serialPort.baudrate = self.baud
        self.serialPort.port = self.port

    # open serial port
        if self.serialPort.is_open == False:
            try:
                self.serialPort.open()
            except:
                print ("Failed to connect")
                #exit()
        if self.serialPort.is_open:
            print("serial is open")
            return self.serialPort
        else:
            return None

    def close(self):
        if self.serialPort:
            self.serialPort.close()
        else:
            self.serialPort = serial.Serial()

    def scheduleReceive(self,dt):
        Clock.schedule_interval(self.readdata, dt)

    def sendpacket(self, device_id, packet_id, data_in):
        # send BPSS packet down serialDevice
        # crc16_func = crcmod.mkCrcFun(0x1A001, rev=False, initCrc=0x0, xorOut=0x0)
        # if type(self.txData).__name__ == 'float':

        txData=convert_to_bytes(data_in)
        # print(__name__, 'sendpacket() txData', txData)
        if self.verbose: print(txData)
        datalength = len(txData)
        # print(__name__, 'sendpacket() datalength', datalength)
        packet_length = datalength + 4

        # print("RAW MATERIALS:", data, packetID, deviceID, datalength)
        txPacket = bytearray(b'#')
        txPacket.extend(b"%02X" % device_id)
        txPacket.extend(b"%02X" % packet_id)
        txPacket.extend(txData)



        # crcValue=crc16_func(txPacket)
        manualCrcValue = calculate_checksum(txPacket)
        tx_crc, crc_bytes = self.convert_crc_to_int(manualCrcValue)

        txPacket.extend(b"%02X" % tx_crc)

        # print(__name__, "packet with crc", txPacket)
        # txPacket.extend(b"FFFF")
        txPacket.extend(b'\r\n')
        # print(__name__, 'sendpacket() txPacket', txPacket)

        # print('\narray check')
        #     # crc=0,
        #     # terminator=0x00))
        # #print("RAW PACKET", raw)
        # print([ "0x%02x" % b for b in txPacket ])

        if self.comsType=='SERIAL':
            if self.serialPort!=None:
                if self.serialPort.is_open:
                    # if self.verbose:
                    # print(__name__, "sendpacket() final packet check", txPacket)
                    # self.serialPort.setRTS(True)
                    self.serialPort.setRTS(False)
                    self.serialPort.write(txPacket)
                    self.serialPort.setRTS(True)
        elif self.comsType=='UDP':
            # print("sending UDP packet")
            # print (self.remoteIPAddress)
            # print('test')
            # packetTest=bytes([0x02, 0x61, 0x01, 0x01, 0x05, 0x60, 0x01, 0x08, 0x17,0x00])
            # print([ " 0x%02x" % b for b in packetTest])
            # print(packetTest)
            # print(type(packetTest))
            # self.UDPsocket.sendto(packetTest, self.remoteIPAddress)
            # print('SERIAL_DEVICE.py:164 sendpacket() packet')
            # print([ " 0x%02x" % b for b in packet])
            # print(packet)

            try:
                self.UDPsocket.sendto(txPacket, self.remoteIPAddress)
            except:
                print("ERROR: serial_device.py sendpacket(): remoteIPAddress:", self.remoteIPAddress, 'packet', txPacket)

        return txPacket

    def convert_crc_to_int(self, manualCrcValue):
        crc_string = hex(manualCrcValue)[2:]
        crc_byte_1 = crc_string[2:]
        crc_byte_2 = crc_string[:2]
        crc_bytes = crc_byte_1 + crc_byte_2
        tx_crc = int(crc_bytes, 16)
        return tx_crc, crc_bytes

    ###_________________________ Receive Function_____________________________###

    
    def readdata(self):

        # print('here')
        if self.comsType=='SERIAL':

            if self.serialPort!=None:
                if self.serialPort.is_open:
                    bytesToRead = 0

                    try:
                        bytesToRead = self.serialPort.in_waiting
                    except:
                        self.close()
                        # self.serialPort = None

                        # self.serialPort.is_open = False
                        # return

                    if bytesToRead:
                        # if self.verbose: print(__name__, "readdata() Bytes to read", bytesToRead)
                        # if self.verbose: print("incomplete packet")
                        # if self.verbose: print([ " 0x%02x" % b for b in self.incomplete_pckt ])
                        newBytes=self.serialPort.read(bytesToRead)
                        # if self.verbose: print("new bytes")
                        if self.verbose: print('New bytes:', newBytes)
                        buff = self.incomplete_pckt + newBytes

                        if self.verbose: print("buffer received is", buff)
                        # if self.verbose: print("here")
                        packets = re.split(b'\r\n', buff)

                        if buff[-1] != b'\r\n':
                            self.incomplete_pckt = packets.pop()

                        for i in range(len(packets)):
                            if b'$' in packets[i][1:]:
                                # print(__name__, 'readdata() packets[i]', packets[i])
                                start_index = packets[i].rfind(b'$', 1)
                                packets[i] = packets[i][start_index:]

                        # print(__name__, "readdata() list of packets is ", packets, "\nwith incomplete packet", self.incomplete_pckt)

                        # if self.verbose: print("list of packets is ", packets, "\nwith incomplete packet", self.incomplete_pckt)
                        return packets

                        # for self.pckt in packets:


                                    # print("unpacked data\n",rxData[0])  # unpacked float/s - need to loop for more than one number in the data field?
                    else:           # no more bytes to read
                        return []
                    # time.sleep(1)
                      #  return buff     # for debugging on single send and receive pairs (not timed)

                else:       # pass while waiting for send to finish
                    return []
        elif  self.comsType=='UDP':
            newBytes=None
            try:
                newBytes, self.remoteIPAddress = self.UDPsocket.recvfrom(4096)

            except:
                pass
            else:
                if newBytes!=None:
                    buff = self.incomplete_pckt + newBytes
                    # print(newBytes)
                    # print(self.remoteIPAddress)
                    # print("buffer received is", buff)
                    # print("here")
                    packets = re.split(b'\r\n', buff)


                    if buff[-1] != b'\r\n':
                        self.incomplete_pckt=packets.pop()
                    return packets
            return []


# parses packet data according to packet ID
# Must be called for every packet in the packets buffer                
    def parsepacket(self, packet_in):
        # print("packet before parsing:", self.pckt)
        if packet_in != b'' and len(packet_in)>10:
            # print("Valid packet:", self.pckt)

            packet = packet_in

            if packet[0:1] != b'$':
                print(__name__, "parsepacket() incorrect Length, packet is:", packet)
            else:
                # print(__name__, "parsepacket() good packet, packet is:", packet)

                # crc8_func = crcmod.mkCrcFun(0x14D, initCrc=0xFF, xorOut=0xFF)

                # #print([ "0x%02x" % b for b in decoded_pckt[:-4] ])
                # crcCheck=crc8_func(self.decoded_pckt[:-1])
                # self.rxCRC=self.decoded_pckt[-1]
                #
                #TODO: do CRC check
                rx_crc_string = packet[-4:]
                try:
                    rx_crc_int = int(rx_crc_string, 16)
                    data_to_checksum = bytearray(packet[:-4])
                    crc_check = calculate_checksum(data_to_checksum)
                except:
                    print(__name__, 'parsepacket() BAD CRC CHECK packet_in', packet_in)
                    crc_check = 0
                    return 0, 0, 0

                # crc_int, crc_bytes = self.convert_crc_to_int(crc_check)
                crc_string = hex(crc_check)[2:]
                crc_byte_1 = crc_string[2:]
                crc_byte_2 = crc_string[:2]
                crc_bytes = crc_byte_1 + crc_byte_2
                crc_int = int(crc_bytes, 16)

                if rx_crc_int == crc_int:
                    packet = packet[0:-4]
                    # print("CRC trimmed off", packet)
                    # print("\n start decode")
                    # print([ " 0x%02x" % b for b in decoded_pckt ])

                    try:
                        rxDeviceId = int(packet[1:3], 16)
                    except:
                        print(__name__, 'FAILED rxDeviceId = int(packet[1:3], 16)', packet)
                        return
                    packet = packet[3:]
                    # print("device ID trimmed off:", packet)
                    try:
                        rxPacketId = packet[0:2].decode()
                    except:
                        print(__name__, 'FAILED rxPacketId = packet[0:2].decode()', packet)
                        return

                    try:
                        rxPacketId = int(rxPacketId, 16)
                    except:
                        print(__name__, 'FAILED rxPacketId = packet[0:2].decode()', packet)
                        return

                    packet = packet[2:]
                    # print("data:", packet)

                    try:
                        rxData = packet.decode()
                    except:
                        print(__name__, 'FAILED rxData = packet.decode()', packet)
                        return

                    # print("data:", rxData, type(rxData))
                    # print(rxDeviceId)

                    #print('crc check value:',crcCheck)

                    if self.verbose: print('rxData:', rxData)
                    if False:
                        pass
                    # elif(rxPacketId == PacketID.MODEL_NUMBER):
                    #     print('MODEL_NUMBER data:', rxData)
                    #     rxData = hex_to_float(rxData[0:8])
                    #     # rxData=str(rxData,'utf-8')
                    # elif (rxPacketId == PacketID.VERSION
                    #         or rxPacketId == PacketID.SERIAL_NUMBER):
                    #     print('Model/Serial/Version data:', rxData)
                    #     rxData = hex_to_float(rxData[0:8])
                    elif(rxPacketId == PacketID.POSITION_LIMIT
                            or rxPacketId == PacketID.VELOCITY_LIMIT
                            or rxPacketId == PacketID.CURRENT_LIMIT):
                        # print([" 0x%02x" % b for b in rxData[0:4]])
                        maxbyte = rxData[8:16]
                        maxbyte = hex_to_float(maxbyte)
                        minbyte = rxData[0:8]
                        minbyte = hex_to_float(minbyte)
                        print('maxbyte is:', maxbyte, 'minbyte is:', minbyte)
                        rxData = maxbyte, minbyte
                    elif(rxPacketId == PacketID.KM_END_POS
                            or rxPacketId == PacketID.KM_END_VEL):
                        x = rxData[16:24]
                        y = rxData[8:16]
                        z = rxData[0:8]
                        print('x:', x, 'y:', y, 'z:', z)
                        x = hex_to_float(x)
                        y = hex_to_float(y)
                        z = hex_to_float(z)
                        print('x:', x, 'y:', y, 'z:', z)
                        rxData = x, y, z
                    elif rxPacketId == PacketID.MODE:
                        print('MODE packet', "Packet ID:", rxPacketId, "data:", rxData)
                        rxData = [int(rxData, 16)]
                    else:
                        if rxPacketId in self.float_packets_list:
                            # print(__name__, "parsepacket() float packet", rxData, "len(rxData)/8", len(rxData)/8)
                            data_to_process = rxData
                            rxData = []
                            for i in range(int(len(data_to_process) / 8)):
                                this_hex = data_to_process[-8:]
                                data_to_process = data_to_process[:-8]
                                rxData.append(hex_to_float(this_hex))
                                # print(__name__, "parsepacket() this_hex", this_hex)
                        else:
                            print(__name__, "parsepacket() byte packet", rxData)
                            data_to_process = rxData
                            rxData = []
                            for i in range(int(len(data_to_process) / 2)):
                                this_hex = data_to_process[-2:]
                                data_to_process = data_to_process[:-2]
                                rxData.append(int(this_hex, 16))
                                print(__name__, "parsepacket() this_hex", this_hex)
                            # rxData = [int(rxData, 16)]

                        # if len(rxData) >= 4:
                        #     #TODO: check if this works
                        #     rxData = [hex_to_float(rxData)]
                        #     print("Packet ID:", rxPacketId, "float data:", rxData, type(rxData))
                        # else:
                        #     print("Packet ID:", rxPacketId, "data:", rxData)
                        #     rxData = [int(rxData, 16)]
                        # if self.verbose: print('PARSE float:', rxData)
                        # num_of_bytes = len(rxData)
                        # print(__name__, "parsepacket() num_of_bytes", num_of_bytes, "len(rxData)", len(rxData))


                    # if self.verbose: print(rxPacketId)

                    return rxDeviceId, rxPacketId, rxData
                else:
                    print("CRC Error in packet:", packet_in, "crc calculated:", crc_bytes)
        return 0, 0, 0


def calculate_checksum(input_buffer):
    """
    Function that computes the CRC16 value for an array of sequential bytes
    stored in memory.
    NB: Types uint8 and uint16 represent unsigned 8 and 16 bit integers
    respectively.
    @param buf Pointer to the start of the buffer to compute the CRC16 for.
    @param len The number of bytes in the buffer to compute the CRC16 for.
    @result The new CRC16 value.
    :param input_buffer:
    """

    poly = 0xA001
    crc = 0
    input_to_process = reverse_data_bytes(input_buffer[1:])
    for b in range(0, len(input_to_process), 2):
        two_chars = input_to_process[b:b+2]
        v = int(two_chars, 16)
        for i in range(8):
            if (v & 0x01) ^ (crc & 0x01):
                crc >>= 1
                crc ^= poly
            else:
                crc >>= 1
            v >>= 1
    return crc

def reverse_data_bytes(input_bytes):
    header, data_to_sort = input_bytes[:4], input_bytes[4:]
    new_data = bytearray(b'')
    for i in range(0, len(data_to_sort), 2):
        start_idx = len(data_to_sort) - i - 2
        end_idx = start_idx + 2
        new_byte = data_to_sort[start_idx:end_idx]
        new_data.extend(new_byte)
    output = header
    output.extend(new_data)
    return output

if __name__ == '__main__':
    test_serial = SerialDevice()
    test_serial.baud = 115200
    test_serial.port = 'COM43'
    test_serial.open()

    while True:

        for pckt_id in [bytearray([PacketID.CURRENT_LIMIT, PacketID.MODE, PacketID.CURRENT_PARAMETERS])]:

            for pckt in test_serial.readdata():
                # for pckt in self.serial_device.packets:
                print("received packet", test_serial.parsepacket(pckt))

            # time.sleep(0.5)

            key_in = input("Press s to send or any key to receive")

            if key_in == 's':
                txData = pckt_id
                txDeviceId = 0xB0
                txPacketId = PacketID.REQUEST
                test_serial.sendpacket(txDeviceId, txPacketId, txData)

            if key_in == '1':
                txData = 1.0
                txDeviceId = 0xB0
                txPacketId = PacketID.VELOCITY
                test_serial.sendpacket(txDeviceId, txPacketId, txData)

            if key_in == '2':
                txData = 0.0
                txDeviceId = 0xB0
                txPacketId = PacketID.VELOCITY
                test_serial.sendpacket(txDeviceId, txPacketId, txData)

            if key_in == '3':
                txData = -1.0
                txDeviceId = 0xB0
                txPacketId = PacketID.VELOCITY
                test_serial.sendpacket(txDeviceId, txPacketId, txData)

            if key_in == 'f':
                txData = 0
                txDeviceId = 0xB0
                txPacketId = PacketID.FORMAT
                test_serial.sendpacket(txDeviceId, txPacketId, txData)


            if key_in == 'b':
                txData = 0
                txDeviceId = 0x03
                txPacketId = PacketID.COMS_PROTOCOL
                test_serial.sendpacket(txDeviceId, txPacketId, txData)
