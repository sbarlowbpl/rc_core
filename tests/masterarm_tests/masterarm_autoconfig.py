import time
import threading
import unittest
from copy import deepcopy
from typing import List

from RC_Core.devicemanager.masterarm.masterarm_config_loopup_table import MasterArmDefaultLookupTable
from RC_Core.packetid import Packets

from RC_Core.commconnection import CommConnection, CommType
from RC_Core.connectionmanager import ConnectionManager
from RC_Core.devicemanager.product import ProductBravo7, ProductAlpha5, MasterArmV2, ProductBravo5, MasterArm, \
    ProductType
from RC_Core.test.masterarm_tests.masterarm_config_sim import MasterArmConfigSim

SERIAL_PORT = 'COM35'
UDP_IP = '127.0.0.1'
UDP_PORT = 9876
COMM_TYPE = CommType.TCP




class MasterArmAutoConfigTest(unittest.TestCase):
    '''MasterArmAutoConfigTest
    Tests auto config settings for the master arm.
    '''

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager: ConnectionManager = ConnectionManager.get_instance()
        self.masterarm: MasterArm = MasterArm()
        self.maconn = CommConnection()
        self.maconn.set_type(COMM_TYPE)
        self.sim_thread = None
        self.sim = None
        if COMM_TYPE == CommType.SERIAL:
            self.maconn.config_connection(SERIAL_PORT, None)
        elif COMM_TYPE in [CommType.UDP, CommType.TCP]:
            self.sim_thread = threading.Thread(target=self.autoconfig_sim)
            self.sim_thread.setDaemon(True)
            self.sim_thread.start()
            self.maconn.config_connection(UDP_PORT, UDP_IP)
        self.maconn.connect()
        self.masterarm.set_connection(self.maconn)
        self.masterarm.set_masterarm_axis_b_rotate = True
        self.masterarm.use_default_settings = True
        self.product = None
        self.received_packet = False
        self.confirm = False
        self.confirm_pos_scale = []
        self.temp_grabbed_posscale = []
        self.connection_manager.external_load()
        time.sleep(0.2)

    def tearDown(self) -> None:
        '''
        Close
        :return:
        '''
        self.connection_manager.remove_self()
        if self.sim_thread:
            if self.sim:
                self.sim.kill_self = True
                self.sim.auto_connect_thread.join()
            self.sim_thread.join()
            self.sim = None
        if ConnectionManager.instance:
            ConnectionManager.get_instance().remove_self()
        time.sleep(0.2)
        self.maconn.close()
        if self.maconn in CommConnection.connections:
            CommConnection.connections.remove(self.maconn)
        self.masterarm.remove_self()
        self.product.remove_self()
        self.maconn = None
        self.connection_manager = None
        self.product = None
        self.received_packet = False
        time.sleep(0.2)


    def autoconfig_sim(self):
        self.sim: MasterArmConfigSim = MasterArmConfigSim((UDP_IP, UDP_PORT))
        print(__name__, 'autoconfig_sim():', 'sim started')

    def test_alpha5_upright_masterarm_autoconfig(self):
        '''
        Tests whether the auto config works.
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.master_arm = self.masterarm
        self.confirm = False
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed:
                expected_ext_devid_config, expected_pos_scale_config = \
                    self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                            self.masterarm.masterarm_model_number,
                                                                            rotation='upright')
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, msg='Alpha 5 Upright autoconfig not set')

    def test_alpha5_inverted_masterarm_autoconfig(self):
        '''
        Tests whether alpha 5 with inverted status gets correct auto config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.set_inverted(True)
        self.product.master_arm = self.masterarm
        self.product.master_arm.masterarm_axis_b_rotate = True
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed:
                expected_ext_devid_config, expected_pos_scale_config = \
                    self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                            self.masterarm.masterarm_model_number,
                                                                            rotation='inverted')
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, msg='Alpha 5 Inverted autoconfig not set')

    def test_bravo7_masterarm_autoconfig(self):
        '''
        Tests whether Bravo 7 auto-configs
        :return:
        '''
        self.product: ProductBravo7 = ProductBravo7()
        self.product.master_arm = self.masterarm
        self.product.master_arm.masterarm_axis_b_rotate = True
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed:
                expected_ext_devid_config, expected_pos_scale_config = \
                    self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                            self.masterarm.masterarm_model_number,
                                                                            rotation='upright')
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, msg='Bravo 7 autoconfig not set')

    def test_bravo5_masterarm_autoconfig(self):
        '''
        Tests whether Bravo 5 auto-configs
        :return:
        '''
        self.product: ProductBravo5 = ProductBravo5()
        self.product.master_arm = self.masterarm
        self.product.master_arm.masterarm_axis_b_rotate = True
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed:
                expected_ext_devid_config, expected_pos_scale_config = \
                    self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                            self.masterarm.masterarm_model_number,
                                                                            rotation='upright')
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, msg='Bravo 5 autoconfig not set')

    def test_z_alpha5_custom_config(self):
        '''
        Tests whether Alpha 5 with master arm accepts custom config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.master_arm = self.masterarm
        self.product.master_arm.masterarm_axis_b_rotate = True
        custom_external_device_ids = [0, 0xC2, 0xC5, 0xC1, 0xC3, 0xC4, 0, 0xCA]
        self.masterarm.custom_external_device_ids = deepcopy(custom_external_device_ids)
        custom_position_scale = [1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0]
        self.masterarm.custom_position_scale = deepcopy(custom_position_scale)
        self.masterarm.use_default_settings = False
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:
                expected_ext_devid_config = custom_external_device_ids
                expected_pos_scale_config = custom_position_scale
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, 'Failed to set custom mappings')

    def test_z_alpha5_custom_extdev_only_config(self):
        '''
        Tests whether Alpha 5 with master arm accepts custom config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.master_arm = self.masterarm
        self.product.master_arm.masterarm_axis_b_rotate = True
        custom_external_device_ids = [0, 0xC2, 0xC5, 0xC1, 0xC3, 0xC4, 0, 0xCA]
        self.masterarm.custom_external_device_ids = deepcopy(custom_external_device_ids)
        # custom_position_scale = [1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0]
        # self.masterarm.custom_position_scale = deepcopy(custom_position_scale)
        self.masterarm.use_default_settings = False

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:
                expected_ext_devid_config, expected_pos_scale_config = \
                    self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                            self.masterarm.masterarm_model_number,
                                                                            rotation='upright')
                expected_ext_devid_config = custom_external_device_ids
                expected_pos_scale_config = deepcopy(expected_pos_scale_config)
                expected_velocity_scale_mapping = \
                    self.product.master_arm.get_velocity_direction_corrected_scale(expected_pos_scale_config)
                expected_velocity_scale_mapping[1] = \
                    expected_velocity_scale_mapping[1] / abs(expected_velocity_scale_mapping[1]) * 6.0
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config and \
                        self.masterarm.velocity_scale == expected_velocity_scale_mapping:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, 'Failed to set custom mappings')

    def test_z_alpha5_custom_posscale_only_config(self):
        '''
        Tests whether Alpha 5 with master arm accepts custom config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.master_arm = self.masterarm
        self.product.master_arm.masterarm_axis_b_rotate = True
        # custom_external_device_ids = [0, 0xC2, 0xC5, 0xC1, 0xC3, 0xC4, 0, 0xCA]
        # self.masterarm.custom_external_device_ids = deepcopy(custom_external_device_ids)
        custom_position_scale = [1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0]
        self.masterarm.custom_position_scale = deepcopy(custom_position_scale)
        self.masterarm.use_default_settings = False

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:
                expected_ext_devid_config, expected_pos_scale_config = \
                    self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                            self.masterarm.masterarm_model_number,
                                                                            rotation='upright')
                expected_ext_devid_config = \
                    deepcopy(expected_ext_devid_config)
                expected_pos_scale_config = custom_position_scale
                expected_velocity_scale_mapping = \
                    deepcopy(self.masterarm.get_velocity_direction_corrected_scale(expected_pos_scale_config))
                expected_velocity_scale_mapping[1] = \
                    expected_velocity_scale_mapping[1] / abs(expected_velocity_scale_mapping[1]) * 6.0
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config and \
                        self.masterarm.velocity_scale == expected_velocity_scale_mapping:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, 'Failed to set custom mappings')

    def test_z_bravo7_custom_config(self):
        '''
        Tests whether Bravo 7 with master arm accepts custom config
        :return:
        '''
        self.product: ProductBravo7 = ProductBravo7()
        self.product.master_arm = self.masterarm
        custom_external_device_ids = [0, 0xC2, 0xC1, 0xC4, 0xC3, 0xC6, 0xC5, 0xC7]
        self.masterarm.custom_external_device_ids = deepcopy(custom_external_device_ids)
        custom_position_scale = [1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0]
        self.masterarm.custom_position_scale = deepcopy(custom_position_scale)
        self.masterarm.use_default_settings = False
        self.product.master_arm.masterarm_axis_b_rotate = True
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:
                expected_ext_devid_config = custom_external_device_ids
                expected_pos_scale_config = custom_position_scale
                if self.masterarm.external_device_ids == expected_ext_devid_config and \
                        self.masterarm.position_scale == expected_pos_scale_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True

        self.assertTrue(False, 'Failed to set custom mappings')

    def test_z_get_model_number(self):
        '''
        Get master arm model number - for verifying what these tests are running off
        :return:
        '''
        self.product: ProductBravo5 = ProductBravo5()
        self.product.master_arm = self.masterarm
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.masterarm.connection and self.masterarm.connection.connected and \
                    self.masterarm.masterarm_version_confirmed:
                if int(self.masterarm.masterarm_model_number) in MasterArmV2.fn4_model_numbers:
                    version = '4 function'
                    print(__name__, 'test_get_model_number():', int(self.masterarm.masterarm_model_number), version)
                    self.assertTrue(True)
                elif int(self.masterarm.masterarm_model_number) in MasterArmV2.fn5_model_numbers:
                    version = '5 function'
                    print(__name__, 'test_get_model_number():', int(self.masterarm.masterarm_model_number), version)
                    self.assertTrue(True)
                elif int(self.masterarm.masterarm_model_number) in MasterArmV2.fn7_model_numbers:
                    version = '7 function'
                    print(__name__, 'test_get_model_number():', int(self.masterarm.masterarm_model_number), version)
                    self.assertTrue(True)
                else:
                    self.assertTrue(False, msg='Master arm model number ' +
                                               str(int(self.masterarm.masterarm_model_number)) +
                                               ' does not match listed number.')
                return
        self.assertTrue(False, 'Did not get model number.')

    def confirm_pos_scale_callback(self, device_id, packet_id, data):
        if data:
            self.confirm_pos_scale = list(data[:8])


if __name__ == '__main__':
    unittest.main()






