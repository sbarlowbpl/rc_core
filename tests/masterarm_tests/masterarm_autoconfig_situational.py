import time
import threading
import unittest
from copy import deepcopy
from typing import Union

from RC_Core.packetid import Packets

from RC_Core.commconnection import CommConnection, CommType
from RC_Core.connectionmanager import ConnectionManager
from RC_Core.devicemanager.product import ProductBravo7, ProductAlpha5, MasterArmV2, ProductBravo5, MasterArm


SERIAL_PORT = 'COM35'




class MasterArmSituationalAutoConfigTest(unittest.TestCase):
    '''MasterArmSituationalAutoConfigTest
    Tests auto config settings for the master arm in specific sitauations
    '''

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager: ConnectionManager = ConnectionManager.get_instance()
        self.masterarm: MasterArm = MasterArm()
        self.maconn: CommConnection = CommConnection()
        self.maconn.set_type(CommType.SERIAL)
        self.maconn.config_connection(SERIAL_PORT, None)
        self.maconn.connect()
        self.masterarm.set_connection(self.maconn)
        self.masterarm.use_default_settings = True
        self.product: Union[ProductAlpha5, ProductBravo7, ProductBravo5, None] = None
        self.connection_manager.external_load()
        self.confirm_pos_scale = []
        self.temp_grabbed_posscale = []
        self.confirm = False
        time.sleep(0.2)

    def tearDown(self) -> None:
        '''
        Close
        :return:
        '''
        self.connection_manager.remove_self()
        if ConnectionManager.instance:
            ConnectionManager.get_instance().remove_self()
        time.sleep(0.2)
        self.maconn.close()
        if self.maconn in CommConnection.connections:
            CommConnection.connections.remove(self.maconn)
        self.masterarm.remove_self()
        self.product.remove_self()
        self.masterarm = None
        self.product = None
        time.sleep(0.2)

    def test_reconnect_autoconfig_joystick_tests(self):
        '''
        Test the situation where master arm disconnects and reconnects, ensuring settings still valid (joystick for axis b)
        :return:
        '''
        # Create product and set masterarm settings
        set_masterarm_axis_b_rotate = False
        self.masterarm.set_masterarm_axis_b_rotate = set_masterarm_axis_b_rotate
        self.product: ProductBravo7 = ProductBravo7()
        self.product.master_arm = self.masterarm


        expected_ext_devid_config = []
        expected_pos_scale_config = []

        # Run sequence
        ts = time.time()
        timeout = 30.0
        conn_stage = 0
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if conn_stage == 0 and self.maconn.connected:
                print(__name__, 'test_reconnect_autoconfig_joystick_tests():', 'Disconnect the Master Arm')
            elif not self.maconn.connected:
                conn_stage = 1
                print(__name__, 'test_reconnect_autoconfig_joystick_tests():', 'Connect the Master Arm')
            elif conn_stage == 1 and self.maconn.connected and self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:

                # Get expected masterarm settings
                if len(expected_ext_devid_config) == 0:
                    expected_ext_devid_config, expected_pos_scale_config = \
                        self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                                self.masterarm.masterarm_model_number,
                                                                                rotation='upright')
                    expected_pos_scale_config[2] = -1.0 * expected_pos_scale_config[2]
                    print(__name__, 'test_reconnect_autoconfig_joystick_tests():', expected_ext_devid_config, expected_pos_scale_config)
                # time.sleep(3.0)
                if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            break
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True
                else:
                    continue
                    # self.assertTrue(False, 'Master arm autoconfig mismatch')
                    # return
        else:
            self.assertTrue(False, 'Failed set initial masterarm config')
            return

        # Run sequence again
        time.sleep(1.0)
        ts = time.time()
        timeout = 30.0
        conn_stage = 0
        self.confirm = False
        self.temp_grabbed_posscale = []
        self.confirm_pos_scale = []
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if conn_stage == 0 and self.maconn.connected:
                print(__name__, 'test_reconnect_autoconfig_tests():', 'Disconnect the Master Arm')
            elif not self.maconn.connected:
                conn_stage = 1
                print(__name__, 'test_reconnect_autoconfig_tests():', 'Connect the Master Arm')
            elif conn_stage == 1 and self.maconn.connected and self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:
                if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True
                else:
                    continue
                    # self.assertTrue(False, 'Master arm autoconfig mismatch on second phase')
                    # return
        else:
            self.assertTrue(False, 'Failed set masterarm config on second phase')
            return

    def test_reconnect_autoconfig_rotate_tests(self):
        '''
        Test the situation where master arm disconnects and reconnects, ensuring settings still valid (rotate for axis b)
        :return:
        '''
        # Create product and set masterarm settings
        self.product: ProductBravo7 = ProductBravo7()
        self.product.master_arm = self.masterarm
        set_masterarm_axis_b_rotate = True
        self.masterarm.set_masterarm_axis_b_rotate = set_masterarm_axis_b_rotate

        expected_ext_devid_config = []
        expected_pos_scale_config = []

        # Run sequence
        ts = time.time()
        timeout = 30.0
        conn_stage = 0
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if conn_stage == 0 and self.maconn.connected:
                print(__name__, 'test_reconnect_autoconfig_rotate_tests():', 'Disconnect the Master Arm')
            elif not self.maconn.connected:
                conn_stage = 1
                print(__name__, 'test_reconnect_autoconfig_rotate_tests():', 'Connect the Master Arm')
            elif conn_stage == 1 and self.maconn.connected and self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:

                # Get expected masterarm settings
                if len(expected_ext_devid_config) == 0:
                    expected_ext_devid_config, expected_pos_scale_config = \
                        self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                                self.masterarm.masterarm_model_number,
                                                                                rotation='upright')
                    print(__name__, 'test_reconnect_autoconfig_rotate_tests():', expected_ext_devid_config, expected_pos_scale_config)
                    # if expected_pos_scale_config and not set_masterarm_axis_b_rotate:
                    #     expected_pos_scale_config[2] = -1.0 * expected_pos_scale_config[2]
                # time.sleep(3.0)
                if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            break
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True
                else:
                    # self.assertTrue(False, 'Master arm autoconfig mismatch')
                    # return
                    continue
        else:
            self.assertTrue(False, 'Failed set initial masterarm config')
            return

        # Run sequence again
        time.sleep(1.0)
        ts = time.time()
        timeout = 30.0
        conn_stage = 0
        self.confirm = False
        self.temp_grabbed_posscale = []
        self.confirm_pos_scale = []
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if conn_stage == 0 and self.maconn.connected:
                print(__name__, 'test_reconnect_autoconfig_rotate_tests():', 'Disconnect the Master Arm')
            elif not self.maconn.connected:
                conn_stage = 1
                print(__name__, 'test_reconnect_autoconfig_rotate_tests():', 'Connect the Master Arm')
            elif conn_stage == 1 and self.maconn.connected and self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:
                if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            self.assertTrue(True)
                            return
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True
                else:
                    continue
                    # self.assertTrue(False, 'Master arm autoconfig mismatch on second phase')
                    # return
        else:
            self.assertTrue(False, 'Failed set masterarm config on second phase')
            return



    def test_reconnect_autoconfig_joystick_to_rotate_tests(self):
        '''
        Test to ensure switching between joystick and rotate
        :return:
        '''
        # Create product and set masterarm settings
        set_masterarm_axis_b_rotate = False
        self.masterarm.set_masterarm_axis_b_rotate = set_masterarm_axis_b_rotate
        self.product: ProductBravo7 = ProductBravo7()
        self.product.master_arm = self.masterarm


        expected_ext_devid_config = []
        expected_pos_scale_config = []

        # Run sequence
        ts = time.time()
        timeout = 30.0
        conn_stage = 0
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if conn_stage == 0 and self.maconn.connected:
                print(__name__, 'test_reconnect_autoconfig_joystick_tests():', 'Disconnect the Master Arm')
            elif not self.maconn.connected:
                conn_stage = 1
                print(__name__, 'test_reconnect_autoconfig_joystick_tests():', 'Connect the Master Arm')
            elif conn_stage == 1 and self.maconn.connected and self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:

                # Get expected masterarm settings
                if len(expected_ext_devid_config) == 0:
                    expected_ext_devid_config, expected_pos_scale_config = \
                        self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                                self.masterarm.masterarm_model_number,
                                                                                rotation='upright')
                    expected_pos_scale_config[2] = -1.0 * expected_pos_scale_config[2]
                    print(__name__, 'test_reconnect_autoconfig_joystick_tests():', expected_ext_devid_config, expected_pos_scale_config)
                # time.sleep(3.0)
                if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            break
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True
                else:
                    continue
                    # self.assertTrue(False, 'Master arm autoconfig mismatch')
                    # return
        else:
            self.assertTrue(False, 'Failed set initial masterarm config')
            return

        # Get expected masterarm settings
        if len(expected_ext_devid_config) == 0:
            expected_ext_devid_config, expected_pos_scale_config = \
                self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                        self.masterarm.masterarm_model_number,
                                                                        rotation='upright')
            print(__name__, 'test_reconnect_autoconfig_joystick_tests():', expected_ext_devid_config,
                  expected_pos_scale_config)

        set_masterarm_axis_b_rotate = False
        self.masterarm.set_masterarm_axis_b_rotate = set_masterarm_axis_b_rotate

        # Run sequence again
        time.sleep(1.0)
        ts = time.time()
        timeout = 10.0
        conn_stage = 0
        self.confirm = False
        self.temp_grabbed_posscale = []
        self.confirm_pos_scale = []
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                if self.confirm:
                    Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                           self.confirm_pos_scale_callback)
                    if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                        continue
                    elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                        self.assertTrue(True)
                        return
                if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                    self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                self.confirm = True
                # self.assertTrue(False, 'Master arm autoconfig mismatch on second phase')
                # return
        else:
            self.assertTrue(False, 'Failed set masterarm config on second phase')
            return

    def test_reconnect_autoconfig_rotate_to_joystick_tests(self):
        '''
        Test to ensure switching between rotate and joystick
        :return:
        '''
        # Create product and set masterarm settings
        set_masterarm_axis_b_rotate = True
        self.masterarm.set_masterarm_axis_b_rotate = set_masterarm_axis_b_rotate
        self.product: ProductBravo7 = ProductBravo7()
        self.product.master_arm = self.masterarm


        expected_ext_devid_config = []
        expected_pos_scale_config = []

        # Run sequence
        ts = time.time()
        timeout = 30.0
        conn_stage = 0
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if conn_stage == 0 and self.maconn.connected:
                print(__name__, 'test_reconnect_autoconfig_joystick_tests():', 'Disconnect the Master Arm')
            elif not self.maconn.connected:
                conn_stage = 1
                print(__name__, 'test_reconnect_autoconfig_joystick_tests():', 'Connect the Master Arm')
            elif conn_stage == 1 and self.maconn.connected and self.masterarm.masterarm_version_confirmed and self.masterarm.confirmed_position_scale:

                # Get expected masterarm settings
                if len(expected_ext_devid_config) == 0:
                    expected_ext_devid_config, expected_pos_scale_config = \
                        self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                                self.masterarm.masterarm_model_number,
                                                                                rotation='upright')
                    print(__name__, 'test_reconnect_autoconfig_joystick_tests():', expected_ext_devid_config, expected_pos_scale_config)
                # time.sleep(3.0)
                if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                    if self.confirm:
                        Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                               self.confirm_pos_scale_callback)
                        if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                            continue
                        elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                            break
                    if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                        self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                    self.confirm = True
                else:
                    continue
                    # self.assertTrue(False, 'Master arm autoconfig mismatch')
                    # return
        else:
            self.assertTrue(False, 'Failed set initial masterarm config')
            return

        # Get expected masterarm settings
        if len(expected_ext_devid_config) == 0:
            expected_ext_devid_config, expected_pos_scale_config = \
                self.product.master_arm_mapppings.get_masterarm_mapping(self.product,
                                                                        self.masterarm.masterarm_model_number,
                                                                        rotation='upright')
            expected_pos_scale_config[2] = -1.0 * expected_pos_scale_config[2]
            print(__name__, 'test_reconnect_autoconfig_joystick_tests():', expected_ext_devid_config,
                  expected_pos_scale_config)

        set_masterarm_axis_b_rotate = True
        self.masterarm.set_masterarm_axis_b_rotate = set_masterarm_axis_b_rotate

        # Run sequence again
        time.sleep(1.0)
        ts = time.time()
        timeout = 10.0
        conn_stage = 0
        self.confirm = False
        self.temp_grabbed_posscale = []
        self.confirm_pos_scale = []
        while time.time() < (ts + timeout):
            time.sleep(1.0)
            if self.masterarm.position_scale == expected_pos_scale_config and self.masterarm.external_device_ids == expected_ext_devid_config:
                if self.confirm:
                    Packets.RC_BASE_POSITION_SCALE.request(self.masterarm.connection, 0xCE,
                                                           self.confirm_pos_scale_callback)
                    if self.confirm_pos_scale == expected_pos_scale_config and len(self.confirm_pos_scale) == 0:
                        continue
                    elif self.confirm_pos_scale == expected_pos_scale_config and self.confirm_pos_scale == self.temp_grabbed_posscale:
                        self.assertTrue(True)
                        return
                if self.temp_grabbed_posscale == [] or len(self.temp_grabbed_posscale) == 0:
                    self.temp_grabbed_posscale = deepcopy(self.masterarm.position_scale)
                self.confirm = True
                # self.assertTrue(False, 'Master arm autoconfig mismatch on second phase')
                # return
        else:
            self.assertTrue(False, 'Failed set masterarm config on second phase')
            return


    def confirm_pos_scale_callback(self, device_id, packet_id, data):
        if data:
            self.confirm_pos_scale = list(data[:8])


if __name__ == '__main__':
    unittest.main()