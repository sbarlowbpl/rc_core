import time
import unittest

import serial
from RC_Core.RS1_hardware import PacketID

from RC_Core.commconnection_methods import parsepacket
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2
from RC_Core.packetid import Packets

from RC_Core.devicemanager.product import ProductAlpha5, MasterArm


from RC_Core.connectionmanager import ConnectionManager

from RC_Core.commconnection import CommConnection, CommType


TARGET_ENCODER_DIRECTION = 1
POSITION_PARAM_SCALE = 1.0

MASTER_ARM_PORT = 'COM23'



class MasterArmConfigTest(unittest.TestCase):
    '''
    Tests for master arm configs
    '''
    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager = ConnectionManager.get_instance()
        fake_conn = CommConnection()
        self.product = ProductAlpha5()
        self.product.set_new_heartbeat_array([])
        master_arm = MasterArm()
        master_arm_conn = CommConnection()
        master_arm_conn.set_type(CommType.SERIAL)
        master_arm_conn.config_connection(MASTER_ARM_PORT, None, baudrate=115200, parity=serial.PARITY_NONE)
        master_arm.set_connection(master_arm_conn)
        self.product.master_arm = master_arm
        self.product.master_arm.connection.connect()
        self.connection_manager.external_load()

    def tearDown(self) -> None:
        '''
        Close connection after finished test
        :return:
        '''
        self.product.master_arm.connection.close()
        CommConnection.connections.remove(self.product.master_arm.connection)
        self.connection_manager = None
        self.product = None

    def test_master_arm_encoder_directions(self):
        '''
        Test encoder directions on master arm
        :return:
        '''
        if not self.product.master_arm.connection.connected:
            self.assertTrue(False, msg='Not connected')
            return

        # Wait for auto-config
        timeout = 5.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            if self.product.master_arm.masterarm_version_confirmed and self.product.master_arm.masterarm_model_number:
                break
        if not self.product.master_arm.masterarm_version_confirmed and self.product.master_arm.masterarm_model_number:
            self.assertTrue(False, msg='Failed to get master arm version')
            return

        # set device_ids
        device_ids = []
        if self.product.master_arm.masterarm_model_number in MasterArmV2.fn7_model_numbers:
            device_ids = [0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7]
        elif self.product.master_arm.masterarm_model_number in MasterArmV2.fn5_model_numbers:
            device_ids = [0xC2, 0xC3, 0xC4, 0xC5]
        elif self.product.master_arm.masterarm_model_number in MasterArmV2.fn4_model_numbers:
            device_ids = [0xC2, 0xC3, 0xC4]
        else:
            self.assertTrue(False, msg='Model number error: ' + str(self.product.master_arm.masterarm_model_number))
            return

        # Get encoder directions
        timeout = 10.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            all_set = True
            for device_id in device_ids:
                if self.product.master_arm.devices[device_id].ICMU_PARAMETERS.rx_data['direction'] != TARGET_ENCODER_DIRECTION:
                    self.product.master_arm.devices[device_id].ICMU_PARAMETERS.request(self.product.master_arm.connection, device_id)
                    all_set = False
                    time.sleep(0.1)
            if all_set:
                break
            time.sleep(0.1)

        for device_id in device_ids:
            if self.product.master_arm.devices[device_id].ICMU_PARAMETERS.rx_data['direction'] != TARGET_ENCODER_DIRECTION:
                self.assertTrue(False, msg='Encoder direction wrong or not found for device id:'
                                           + str(hex(device_id)) + ' value: '
                                           + str(self.product.master_arm.devices[device_id].ICMU_PARAMETERS.rx_data['direction']))
                return

        all_encoders = []
        for device_id in device_ids:
            all_encoders.append((str(hex(device_id)), self.product.master_arm.devices[device_id].ICMU_PARAMETERS.rx_data['direction']))

        self.product.master_arm.connection.close()
        self.connection_manager.run_connection_loop = False
        time.sleep(0.5)
        print(__name__, 'test_master_arm_encoder_directions():', 'All encoder parameters correct: ', str(all_encoders))
        self.assertTrue(True)

    def test_position_parameters_scale(self):
        '''
        Test position parameters scale on master arm
        :return:
        '''
        if not self.product.master_arm.connection.connected:
            self.assertTrue(False, msg='Not connected')
            return

        # Wait for auto-config
        timeout = 5.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            if self.product.master_arm.masterarm_version_confirmed and self.product.master_arm.masterarm_model_number:
                break
        if not self.product.master_arm.masterarm_version_confirmed and self.product.master_arm.masterarm_model_number:
            self.assertTrue(False, msg='Failed to get master arm version')
            return

        # set device_ids
        device_ids = []
        if self.product.master_arm.masterarm_model_number in MasterArmV2.fn7_model_numbers:
            device_ids = [0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7]
        elif self.product.master_arm.masterarm_model_number in MasterArmV2.fn5_model_numbers:
            device_ids = [0xC2, 0xC3, 0xC4, 0xC5]
        elif self.product.master_arm.masterarm_model_number in MasterArmV2.fn4_model_numbers:
            device_ids = [0xC2, 0xC3, 0xC4]
        else:
            self.assertTrue(False, msg='Model number error: ' + str(self.product.master_arm.masterarm_model_number))
            return

        # Get position parameters scales
        timeout = 10.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            all_set = True
            for device_id in device_ids:
                if self.product.master_arm.devices[device_id].POSITION_PARAMETERS.rx_data['scale'] != POSITION_PARAM_SCALE:
                    self.product.master_arm.devices[device_id].POSITION_PARAMETERS.request(self.product.master_arm.connection, device_id)
                    all_set = False
                    time.sleep(0.1)
            if all_set:
                break
            time.sleep(0.1)

        for device_id in device_ids:
            if self.product.master_arm.devices[device_id].POSITION_PARAMETERS.rx_data['scale'] != POSITION_PARAM_SCALE:
                self.assertTrue(False, msg='Position parameters scale wrong or not found for device id: '
                                           + str(hex(device_id)) + ' value: '
                                           + str(self.product.master_arm.devices[device_id].POSITION_PARAMETERS.rx_data['scale']))
                return

        all_scales = []
        for device_id in device_ids:
            all_scales.append((str(hex(device_id)), self.product.master_arm.devices[device_id].POSITION_PARAMETERS.rx_data['scale']))

        self.product.master_arm.connection.close()
        self.connection_manager.run_connection_loop = False
        time.sleep(0.5)
        print(__name__, 'test_position_parameters_scale():', 'All position parameters scale correct:', str(all_scales))
        self.assertTrue(True)




if __name__ == '__main__':
    unittest.main()