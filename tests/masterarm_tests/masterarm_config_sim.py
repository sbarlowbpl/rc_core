import os, sys, time
import socket
import struct
import threading

from RC_Core.enums.packets import PacketID
from RC_Core.packetid import Packets

from RC_Core.commconnection_methods import parsepacket, packet_splitter, encode_packet

from RC_Core.RS1_hardware import Mode, DeviceType, get_name_of_packet_id

from RC_Core.devicemanager.product import ProductAlpha5, MasterArm, MasterArmV2




class MasterArmConfigSim():
    '''MasterArmConfigSim
    Master Arm auto config simulation.
    This allows to test auto-configuration of master arms, without a physical master arm.
    '''
    MODEL_NUMBER = 7201.0
    EXTERNAL_DEVICE_IDS = [0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7]
    POSITION_SCALE = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    VELOCITY_SCALE = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    BASE_OPTIONS = [2, 0, 1, 0xC5, Mode.POSITION_HOLD, 0, 0, 0, 0, 0]

    DEFAULT_OPERATING_MODE = Mode.POSITION_VELOCITY_CONTROL

    JOY_ROT_DEVICE_TYPE = DeviceType.JOYSTICK_X_PROXY_ESTIMATOR

    END_EFF_DEVICE_ID = 0xC0
    JOY_ROT_DEVICE_ID = 0xC2
    BASE_DEVICE_ID = 0xCE

    BASE_MODE = BASE_OPTIONS[4]


    def __init__(self, ip_port):
        '''
        Init
        :param ip_port:
        '''
        self.kill_self = False
        self.ip_port = ip_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.ip_port)
        self.sock.setblocking(False)
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])
        self.tcp_conn = None
        self.start_time = time.time()
        self.wait_for_connection()

    def wait_for_connection(self):
        '''
        Wait for connection
        :return:
        '''
        while self.tcp_conn is None and not self.kill_self:
            time.sleep(0.5)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
                tcp_conn.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')
        self.auto_connect_thread = threading.Thread(target=self.auto_loop)
        self.auto_connect_thread.setDaemon(True)
        self.auto_connect_thread.start()

    def auto_loop(self):
        '''
        Main run loop
        :return:
        '''
        time.sleep(0.005)
        while not self.kill_self:
            # Read and action current packets
            packets = self._read_tcp()
            for packet_raw in packets:
                device_id, packet_id, data = parsepacket(packet_raw)

                # Handle BASE settings
                if device_id == self.BASE_DEVICE_ID:
                    # Settings data
                    if packet_id == PacketID.RC_BASE_EXT_DEVICE_IDS:
                        self.EXTERNAL_DEVICE_IDS = list(data)
                    elif packet_id == PacketID.RC_BASE_POSITION_SCALE:
                        self.POSITION_SCALE = list(data)
                    elif packet_id == PacketID.RC_BASE_VELOCITY_SCALE:
                        self.VELOCITY_SCALE = list(data)
                    elif packet_id == PacketID.DEFAULT_OPERATING_MODE:
                        self.DEFAULT_OPERATING_MODE = data[0]
                    elif packet_id == PacketID.RC_BASE_OPTIONS:
                        self.BASE_OPTIONS = list(data)


                    # Requests
                    elif packet_id == PacketID.REQUEST:
                        data_name_list = []
                        for req_packet_id in data:
                            data_name_list.append(get_name_of_packet_id(req_packet_id))
                        print(__name__, 'auto_loop():', 'Requests for 0xCE:', data_name_list)
                        if PacketID.RC_BASE_EXT_DEVICE_IDS in data:
                            self._send_tcp(device_id, PacketID.RC_BASE_EXT_DEVICE_IDS, bytearray(self.EXTERNAL_DEVICE_IDS))
                        if PacketID.RC_BASE_POSITION_SCALE in data:
                            self._send_tcp(device_id, PacketID.RC_BASE_POSITION_SCALE, self.POSITION_SCALE)
                        if PacketID.RC_BASE_VELOCITY_SCALE in data:
                            self._send_tcp(device_id, PacketID.RC_BASE_VELOCITY_SCALE, self.VELOCITY_SCALE)
                        if PacketID.MODEL_NUMBER in data:
                            self._send_tcp(device_id, PacketID.MODEL_NUMBER, [self.MODEL_NUMBER])
                        if PacketID.RC_BASE_OPTIONS in data:
                            self._send_tcp(device_id, PacketID.RC_BASE_OPTIONS, bytearray(self.BASE_OPTIONS))
                        if PacketID.DEFAULT_OPERATING_MODE in data:
                            self._send_tcp(device_id, PacketID.DEFAULT_OPERATING_MODE, bytearray([self.DEFAULT_OPERATING_MODE]))
                        if PacketID.MODE in data:
                            self._send_tcp(device_id, PacketID.MODE, bytearray([self.BASE_MODE]))


                elif device_id == self.JOY_ROT_DEVICE_ID:
                    if packet_id == PacketID.DEVICE_TYPE:
                        self.JOY_ROT_DEVICE_TYPE = data[0]
                    elif packet_id == PacketID.REQUEST:
                        if PacketID.DEVICE_TYPE in data:
                            self._send_tcp(device_id, PacketID.DEVICE_TYPE, bytearray([self.JOY_ROT_DEVICE_TYPE]))

            self._send_tcp(None, None, None, append_only=False)
            time.sleep(0.005)
        try:
            self.tcp_conn.shutdown(socket.SHUT_RDWR)
        except:
            pass
        try:
            self.sock.shutdown(socket.SHUT_RDWR)
        except:
            pass
        self.sock.close()
        self.tcp_conn.close()
        self.sock = None
        self.tcp_conn = None
        # self.tcp_conn = None
        # self.sock = None

    def _read_tcp(self, num_bytes=4096):
        '''
        Read incoming TCP data
        :param num_bytes:
        :return:
        '''
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        '''
        Send data via TCP connection
        :param device_id:
        :param packet_id:
        :param data_in:
        :param append_only:
        :return:
        '''
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1




if __name__ == '__main__':
    maconfigsim = MasterArmConfigSim(('', 9876))
    while not maconfigsim.kill_self:
        time.sleep(1.0)