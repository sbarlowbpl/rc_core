import os, sys, time
import threading
import unittest

from RC_Core.RS1_hardware import Mode

from RC_Core.devicemanager.product import MasterArm, ProductBravo7

from RC_Core.commconnection import CommConnection, CommType

from RC_Core.connectionmanager import ConnectionManager
from test.masterarm_tests.masterarm_config_sim import MasterArmConfigSim
from test.product_sims.bravo7_simwave_pos_sim import BRavo7SineWaveSimulation

MA_SERIAL_PORT = 'COM35'
MA_TCP_IP = '127.0.0.1'
MA_TCP_PORT = 9876
MA_COMM_TYPE = CommType.SERIAL

PRODUCT_CLASS = ProductBravo7
PR_SERIAL_PORT = 'COM8'
PR_TCP_IP = '127.0.0.1'
PR_TCP_PORT = 7890
PR_COMM_TYPE = CommType.TCP



class MasterArmAutoConfigTest(unittest.TestCase):
    '''MasterArmAutoConfigTest
    Tests auto config settings for the master arm.
    '''

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager: ConnectionManager = ConnectionManager.get_instance()

        # MA settings
        self.masterarm: MasterArm = MasterArm()
        self.maconn = CommConnection()
        self.maconn.set_type(MA_COMM_TYPE)
        self.ma_sim_thread = None
        self.ma_sim = None
        if MA_COMM_TYPE == CommType.SERIAL:
            self.maconn.config_connection(MA_SERIAL_PORT, None)
        elif MA_COMM_TYPE in [CommType.UDP, CommType.TCP]:
            self.ma_sim_thread = threading.Thread(target=self.ma_sim_loop)
            self.ma_sim_thread.setDaemon(True)
            self.ma_sim_thread.start()
            self.maconn.config_connection(MA_TCP_PORT, MA_TCP_IP)
        self.maconn.connect()
        self.masterarm.set_connection(self.maconn)
        self.masterarm.set_masterarm_axis_b_rotate = True
        self.masterarm.use_default_settings = True

        # Product settings
        self.product = PRODUCT_CLASS()
        self.productconn = CommConnection()
        self.productconn.set_type(PR_COMM_TYPE)
        self.product_sim_thread = None
        self.product_sim = None
        if PR_COMM_TYPE == CommType.SERIAL:
            self.productconn.config_connection(MA_SERIAL_PORT, None)
        elif PR_COMM_TYPE in [CommType.UDP, CommType.TCP]:
            self.product_sim_thread = threading.Thread(target=self.product_sim_loop)
            self.product_sim_thread.setDaemon(True)
            self.product_sim_thread.start()
            self.productconn.config_connection(PR_TCP_PORT, PR_TCP_IP)
        self.productconn.connect()
        self.product.set_connection(self.productconn)
        self.product.master_arm = self.masterarm

        # Load connection
        self.connection_manager.external_load()
        time.sleep(0.2)

    def tearDown(self) -> None:
        '''
        Close
        :return:
        '''
        self.connection_manager.remove_self()
        time.sleep(1.0)
        if self.ma_sim:
            if self.ma_sim:
                self.ma_sim.kill_self = True
                self.ma_sim.auto_connect_thread.join()
            self.ma_sim_thread.join()
            self.ma_sim = None
        if self.product_sim:
            if self.product_sim:
                self.product_sim.kill_self = True
                # self.product_sim.auto_connect_thread.join()
            self.product_sim_thread.join()
            self.product_sim = None
        self.maconn.close()
        self.productconn.close()
        self.masterarm.remove_self()
        self.product.remove_self()
        CommConnection.connections = []
        time.sleep(0.2)

    def ma_sim_loop(self):
        '''
        Start masterarm sim
        :return:
        '''
        self.ma_sim: MasterArmConfigSim = MasterArmConfigSim((MA_TCP_IP, MA_TCP_PORT))
        print(__name__, 'ma_sim_loop():', 'sim started')

    def product_sim_loop(self):
        '''
        Start product sim
        :return:
        '''
        bravo7_sim_ip_port = (PR_TCP_IP, PR_TCP_PORT)
        bravo7_sim_device_ids = [1, 2, 3, 4, 5, 6, 7]
        bravo7_mcu_device_id = 0x0D
        bravo7_sim_base_device_id = 0x0E
        self.product_sim = BRavo7SineWaveSimulation(bravo7_sim_ip_port, bravo7_sim_device_ids, bravo7_mcu_device_id,
                                              bravo7_sim_base_device_id)
        print(__name__, 'product_sim_loop():', 'sim started')

    def test_go_pospreset_go(self):
        '''
        Test go, send preset, then go again
        :return:
        '''
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                break
        else:
            self.assertTrue(False, msg='Did not connect and auto-config')
            return

        ts = time.time()
        timeout = 10.0
        print(__name__, 'test_go_pospreset_go():', 'Set master arm to paused state.')
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                if not self.masterarm.masterarm_is_active:
                    break
        else:
            self.assertTrue(False, msg='Master arm not detected in pause state.')
            return

        ts = time.time()
        timeout = 10.0
        print(__name__, 'test_go_pospreset_go():', 'Unpause master arm.')
        stage = 0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                if self.masterarm.masterarm_is_active and stage == 0:
                    # Wait for 5s of active movement
                    stage = 1
                    ts = time.time()
                    timeout = 1.0
        else:
            if stage == 0:
                self.assertTrue(False, msg='Master arm not detected to become active.')
                return

        ts = time.time()
        timeout = 10.0
        print(__name__, 'test_go_pospreset_go():', 'Hold stow.')
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                if self.masterarm.masterarm_run_mode == Mode.POSITION_PRESET:
                    break
        else:
            self.assertTrue(False, msg='Master arm not detected to go STOW pos preset state.')
            return

        ts = time.time()
        timeout = 10.0
        print(__name__, 'test_go_pospreset_go():', 'Release stow.')
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                if self.masterarm.masterarm_run_mode != Mode.POSITION_PRESET:
                    break
        else:
            self.assertTrue(False, msg='Master arm not detected to go STOW pos preset state.')
            return

        ts = time.time()
        timeout = 10.0
        print(__name__, 'test_go_pospreset_go():', 'Unpause master arm.')
        stage = 0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                if self.masterarm.masterarm_is_active and stage == 0:
                    # Wait for 5s of active movement
                    stage = 1
                    ts = time.time()
                    timeout = 1.0
        else:
            if stage == 0:
                self.assertTrue(False, msg='Master arm not detected to become active.')
                return

        ts = time.time()
        timeout = 10.0
        print(__name__, 'test_go_pospreset_go():', 'Pause master arm.')
        stage = 0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.maconn.connected and self.productconn.connected and self.masterarm.confirmed_position_scale:
                if not self.masterarm.masterarm_is_active:
                    break
        else:
            self.assertTrue(False, msg='Master arm not detected to become active.')
            return

        self.assertTrue(True)



if __name__ == '__main__':
    unittest.main()