import os, sys, time
import unittest
from copy import deepcopy
from typing import Union

from RC_Core.packetid import PacketPrototype

from RC_Core.commconnection import CommConnection, CommType

from RC_Core.connectionmanager import ConnectionManager
from RC_Core.devicemanager.hid_masterarm.hidmasterarm import HIDMasterArm
from RC_Core.devicemanager.hid_masterarm.hidmasterarm_enum import HIDMasterArmEnum
from RC_Core.devicemanager.product import ProductBravo7, ProductAlpha5, MasterArmV2

SERIAL_PORT = 'COM7'



class HIDOperationTests(unittest.TestCase):
    '''HIDOperationTests
    Test HID Master Arm operations
    '''
    connection: Union[CommConnection, None] = None
    product: Union[ProductAlpha5, None] = None
    hid_masterarm: Union[HIDMasterArm, None] = None

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager: ConnectionManager = ConnectionManager.get_instance()
        if self.connection:
            self.connection.close()
            if self.connection in CommConnection.connections:
                CommConnection.connections.remove(self.connection)
        self.connection: CommConnection = CommConnection()
        self.connection.set_type(CommType.SERIAL)
        self.connection.config_connection(SERIAL_PORT, None)
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.set_connection(self.connection)
        self.product.set_new_heartbeat_array([])
        self.hid_masterarm: HIDMasterArm = HIDMasterArm()
        self.hid_masterarm.masterarm_axis_b_rotate = True
        self.hid_masterarm.set_target_product(self.product)
        # self.connection.connect()
        self.connection_manager.external_load()
        time.sleep(1.0)

    def tearDown(self) -> None:
        '''
        Close
        :return:
        '''
        self.hid_masterarm.remove_self()
        self.hid_masterarm = None
        self.connection_manager.remove_self()
        self.connection_manager = None
        self.product.remove_self()
        self.product = None
        self.received_packet = False
        self.connection.close()
        if self.connection in CommConnection.connections:
            CommConnection.connections.remove(self.connection)

    def test_boundary_conditions(self):
        '''
        Test hid master arm handles boundary conditions correctly
        :return:
        '''
        if self.product.connection is None or not self.product.connection.connected or \
                not self.product.master_arm.connected:
            self.assertTrue(False, msg='Not Connected')
            return
        self.hid_masterarm.masterarm_axis_b_rotate = True
        self.hid_masterarm.set_active(True)
        print(__name__, 'test_boundary_conditions():', 'Rotate axis b on the hid master arm to go past the 0/360 boundary.')
        timeout = 20.0
        ts = time.time()
        last_position_axis_b: Union[float, None] = None
        last_indexed_position: Union[float, None] = None
        forward_correct = False
        reverse_correct = False
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if not self.hid_masterarm.connected: # or self.hid_masterarm.masterarm_is_active:
                self.assertTrue(False, 'Not connected or active')
                return
            if self.hid_masterarm.raw_target_positions:
                position_axis_b = self.hid_masterarm.raw_target_positions[2]
                if last_position_axis_b is not None and last_indexed_position is not None:
                    if (position_axis_b - last_position_axis_b) > (0.5* 2 * 3.1415):
                        if abs(last_indexed_position - self.hid_masterarm.index_position_offsets[2]) < (0.5* 2 * 3.1415):
                            forward_correct = True
                            print(__name__, 'test_boundary_conditions():', 'Forward verified, reverse direction')
                    elif (position_axis_b - last_position_axis_b) < -(0.5* 2 * 3.1415):
                        if abs(last_indexed_position - self.hid_masterarm.index_position_offsets[2]) < (0.5 * 2 * 3.1415):
                            print(__name__, 'test_boundary_conditions():', 'Reverse verified, reverse direction')
                            reverse_correct = True
                last_position_axis_b = self.hid_masterarm.raw_target_positions[2]
                last_indexed_position = self.hid_masterarm.index_position_offsets[2]
            if forward_correct and reverse_correct:
                self.assertTrue(True)
                return
        else:
            self.assertTrue(False, 'Not verified')





if __name__ == '__main__':
    unittest.main()
