import os, sys, time
import unittest
from copy import deepcopy

from RC_Core.packetid import PacketPrototype

from RC_Core.commconnection import CommConnection, CommType

from RC_Core.connectionmanager import ConnectionManager
from RC_Core.devicemanager.hid_masterarm.hidmasterarm import HIDMasterArm
from RC_Core.devicemanager.hid_masterarm.hidmasterarm_enum import HIDMasterArmEnum
from RC_Core.devicemanager.product import ProductBravo7, ProductAlpha5, MasterArmV2
# from test.product_connection_tests.udp_tcp_arm_test import TCPArmTest


SERIAL_PORT = 'COM26'

class HIDMasterArmButtonLogicTest(unittest.TestCase):
    '''HIDMasterArmButtonLogicTest
    Tests button logic - stow, deploy, pause, etc.
    '''
    connection = None

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager: ConnectionManager = ConnectionManager.get_instance()
        if self.connection:
            self.connection.close()
            if self.connection in CommConnection.connections:
                CommConnection.connections.remove(self.connection)
        self.connection: CommConnection = CommConnection()
        self.connection.set_type(CommType.SERIAL)
        self.connection.config_connection(SERIAL_PORT, None)
        # self.connection.set_type(CommType.TCP)
        # self.connection.config_connection(6789, '127.0.0.1')
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.set_connection(self.connection)
        self.product.set_new_heartbeat_array([])
        self.hid_masterarm: HIDMasterArm = HIDMasterArm()
        self.hid_masterarm.masterarm_axis_b_rotate = True
        self.hid_masterarm.set_target_product(self.product)
        # self.connection.connect()
        time.sleep(0.1)
        self.connection_manager.external_load()

    def tearDown(self) -> None:
        '''
        Close
        :return:
        '''
        self.hid_masterarm.remove_self()
        self.hid_masterarm = None
        self.connection_manager.remove_self()
        self.connection_manager = None
        self.product.remove_self()
        self.product = None
        self.received_packet = False
        self.connection.close()
        if self.connection in CommConnection.connections:
            CommConnection.connections.remove(self.connection)

    def test_unpause_pause(self):
        '''
        Tests pause and unpaused logic
        :return:
        '''
        if not self.product.connection.connected:
            self.assertTrue(False, msg='Alpha5 not connected')

        # Wait for connection
        print(__name__, 'test_unpause_pause():',
              'Connect the HID master arm make make sure it is paused')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available and \
                    not self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm not connected')

        print(__name__, 'test_unpause_pause():',
              'Click and release to unpause.')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm was not set as active')

        print(__name__, 'test_unpause_pause():',
              'Hold down pause button for 3 seconds, and wait for release command in console')
        ts = time.time()
        timeout = 15.0
        tpause = 0
        stage = 0
        stated_release = False
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if stage == 0 and not self.hid_masterarm.masterarm_is_active:
                stage = 1
                tpause = time.time()
            elif stage == 1 and time.time() > (tpause + 3.0):
                if not stated_release:
                    stated_release = True
                    print(__name__, 'test_unpause_pause():',
                          'Release pause button.')
                if self.hid_masterarm.masterarm_is_active:
                    break
        else:
            self.assertTrue(False, 'HID master arm was not set pause and release')

        # Wait for connection
        print(__name__, 'test_unpause_pause():',
              'Double click the pause button to unpause')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.PAUSE] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    not self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm did not unpause on double click')
        self.assertTrue(True)


    def test_stow_logic(self):
        '''
        Tests pause and unpaused logic
        :return:
        '''
        if not self.product.connection.connected:
            self.assertTrue(False, msg='Alpha5 not connected')

        # Wait for connection
        print(__name__, 'test_stow_logic():',
              'Connect the HID master arm make make sure it is paused')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available and \
                    not self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm not connected')

        print(__name__, 'test_stow_logic():',
              'Hold down the stow button.')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_PRESSED and \
                    self.hid_masterarm.stow_command_go:
                break
        else:
            self.assertTrue(False, 'STOW is not active')

        print(__name__, 'test_stow_logic():',
              'Release the stow button.')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    not self.hid_masterarm.stow_command_go:
                break
        else:
            self.assertTrue(False, 'STOW was not released')

        print(__name__, 'test_stow_logic():',
              'Double click the stow button to permanently command STOW.')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    self.hid_masterarm.stow_command_go:
                break
        else:
            self.assertTrue(False, 'STOW not in permanent go mode')


        print(__name__, 'test_stow_logic():',
              'Click STOW once to stop.')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    not self.hid_masterarm.stow_command_go and not self.hid_masterarm.masterarm_is_active:
                break
        else:
            if self.hid_masterarm.button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_RELEASED:
                self.assertTrue(False, 'STOW button did not detect its release')
            elif self.hid_masterarm.stow_command_go:
                self.assertTrue(False, 'STOW GO is still active')
            elif self.hid_masterarm.masterarm_is_active:
                self.assertTrue(False, 'HID master arm did not pause once STOW was stopped')
        self.assertTrue(True)

    def test_deploy_logic(self):
        '''
        Tests pause and unpaused logic
        :return:
        '''
        if not self.product.connection.connected:
            self.assertTrue(False, msg='Alpha5 not connected')

        # Wait for connection
        print(__name__, 'test_deploy_logic():',
              'Connect the HID master arm make make sure it is paused')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available and \
                    not self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm not connected')

        print(__name__, 'test_deploy_logic():',
              'Hold down the deploy button.')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_PRESSED and \
                    self.hid_masterarm.deploy_command_go:
                break
        else:
            self.assertTrue(False, 'DEPLOY is not active')

        print(__name__, 'test_deploy_logic():',
              'Release the deploy button.')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    not self.hid_masterarm.deploy_command_go:
                break
        else:
            self.assertTrue(False, 'DEPLOY was not released')

        print(__name__, 'test_deploy_logic():',
              'Double click the deploy button to permanently command DEPLOY.')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    self.hid_masterarm.deploy_command_go:
                break
        else:
            self.assertTrue(False, 'DEPLOY not in permanent go mode')


        print(__name__, 'test_deploy_logic():',
              'Click DEPLOY once to stop.')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    not self.hid_masterarm.deploy_command_go and not self.hid_masterarm.masterarm_is_active:
                break
        else:
            if self.hid_masterarm.button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_RELEASED:
                self.assertTrue(False, 'DEPLOY button did not detect its release')
            elif self.hid_masterarm.deploy_command_go:
                self.assertTrue(False, 'DEPLOY GO is still active')
            elif self.hid_masterarm.masterarm_is_active:
                self.assertTrue(False, 'HID master arm did not pause once DEPLOY was stopped')
        self.assertTrue(True)

    def test_set_stow(self):
        '''
        Tests set of stow.
        :return:
        '''
        if not self.product.connection.connected:
            self.assertTrue(False, msg='Alpha5 not connected')

        # Wait for connection
        print(__name__, 'test_set_stow():',
              'Connect the HID master arm make make sure it is paused')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available and \
                    not self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm not connected')

        # Get current position preset
        print(__name__, 'test_set_stow():', 'Requesting current STOW preset on Alpha 5')
        base_devid = self.product.get_base_device_id()
        stow_pos_preset: PacketPrototype = self.product.devices[base_devid].POS_PRESET_SET_0
        timeout = 10
        ts = time.time()
        current_stow_setpoint = {}
        while time.time() < (ts + timeout):
            if stow_pos_preset.rx_data[stow_pos_preset.data_names[0]] is not None:
                current_stow_setpoint = deepcopy(stow_pos_preset.rx_data)
                break
            stow_pos_preset.request(self.product.connection, base_devid)
            time.sleep(0.2)
        else:
            self.assertTrue(False, 'Failed to get POS_PRESET_SET_1 data on Alpha 5')
        stow_pos_preset.reset_rx_data()

        # Go to STOW position
        time.sleep(3)
        print(__name__, 'test_set_stow():',
              'Double click the stow button to move to current STOW position')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    self.hid_masterarm.stow_command_go:
                break
        else:
            self.assertTrue(False, 'STOW not in permanent go mode')

        # Move to another position
        time.sleep(3)
        print(__name__, 'test_set_stow():',
              'Unpause, move hid master arm to move Alpha 5 to another position, then pause.',
              'The new position MUST NOT be the same as the current STOW position')
        ts = time.time()
        timeout = 15.0
        self.hid_masterarm.set_active(False)
        was_active = False
        while time.time() < (ts + timeout):
            if self.hid_masterarm.masterarm_is_active:
                was_active = True
            if time.time() > (ts + 7.0) and not self.hid_masterarm.masterarm_is_active and was_active:
                break
            time.sleep(0.1)
        else:
            self.assertTrue(False, 'HID master arm was not detected to have moved Alpha 5 position')

        # Capture STOW
        print(__name__, 'test_set_stow():',
              'Click and hold pause button, then click and hold STOW button until commanded to '
              'release (to set current position as STOW position).')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.stow_save_sent:
                print(__name__, 'test_set_stow():',
                      'Release pause and stow buttons')
                break
        else:
            self.assertTrue(False, 'STOW position preset was not set')

        # Get new position preset
        print(__name__, 'test_set_stow():', 'Requesting current STOW preset on Alpha 5')
        timeout = 10
        ts = time.time()
        while time.time() < (ts + timeout):
            if stow_pos_preset.rx_data[stow_pos_preset.data_names[0]] is not None:
                for dname in stow_pos_preset.data_names:
                    if current_stow_setpoint[dname] != stow_pos_preset.rx_data[dname]:
                        self.assertTrue(True)
                        return
                self.assertTrue(False, 'New STOW position did not set')
            stow_pos_preset.request(self.product.connection, base_devid)
            time.sleep(0.2)
        else:
            self.assertTrue(False, 'Failed to get POS_PRESET_SET_1 data on Alpha 5 (2)')

    def test_set_deploy(self):
        '''
        Tests set of deploy.
        :return:
        '''
        if not self.product.connection.connected:
            self.assertTrue(False, msg='Alpha5 not connected')

        # Wait for connection
        print(__name__, 'test_set_deploy():',
              'Connect the HID master arm make make sure it is paused')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available and \
                    not self.hid_masterarm.masterarm_is_active:
                break
        else:
            self.assertTrue(False, 'HID master arm not connected')

        # Get current position preset
        print(__name__, 'test_set_deploy():', 'Requesting current DEPLOY preset on Alpha 5')
        base_devid = self.product.get_base_device_id()
        deploy_pos_preset: PacketPrototype = self.product.devices[base_devid].POS_PRESET_SET_1
        timeout = 10
        ts = time.time()
        current_deploy_setpoint = {}
        while time.time() < (ts + timeout):
            if deploy_pos_preset.rx_data[deploy_pos_preset.data_names[0]] is not None:
                current_deploy_setpoint = deepcopy(deploy_pos_preset.rx_data)
                break
            deploy_pos_preset.request(self.product.connection, base_devid)
            time.sleep(0.2)
        else:
            self.assertTrue(False, 'Failed to get POS_PRESET_SET_1 data on Alpha 5')
        deploy_pos_preset.reset_rx_data()

        # Go to DEPLOY position
        time.sleep(3)
        print(__name__, 'test_set_deploy():',
              'Double click the deploy button to move to current DEPLOY position')
        ts = time.time()
        timeout = 10.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_RELEASED and \
                    self.hid_masterarm.deploy_command_go:
                break
        else:
            self.assertTrue(False, 'DEPLOY not in permanent go mode')

        # Move to another position
        time.sleep(3)
        print(__name__, 'test_set_deploy():',
              'Unpause, move hid master arm to move Alpha 5 to another position, then pause.',
              'The new position MUST NOT be the same as the current DEPLOY position')
        ts = time.time()
        timeout = 15.0
        self.hid_masterarm.set_active(False)
        was_active = False
        while time.time() < (ts + timeout):
            if self.hid_masterarm.masterarm_is_active:
                was_active = True
            if time.time() > (ts + 7.0) and not self.hid_masterarm.masterarm_is_active and was_active:
                break
            time.sleep(0.1)
        else:
            self.assertTrue(False, 'HID master arm was not detected to have moved Alpha 5 position')

        # Capture DEPLOY
        print(__name__, 'test_set_deploy():',
              'Click and hold pause button, then click and hold DEPLOY button until commanded to '
              'release (to set current position as DEPLOY position).')
        ts = time.time()
        timeout = 15.0
        while time.time() < (ts + timeout):
            time.sleep(0.1)
            if self.hid_masterarm.deploy_save_sent:
                print(__name__, 'test_set_deploy():',
                      'Release pause and deploy buttons')
                break
        else:
            self.assertTrue(False, 'DEPLOY position preset was not set')

        # Get new position preset
        print(__name__, 'test_set_deploy():', 'Requesting current DEPLOY preset on Alpha 5')
        timeout = 10
        ts = time.time()
        while time.time() < (ts + timeout):
            if deploy_pos_preset.rx_data[deploy_pos_preset.data_names[0]] is not None:
                for dname in deploy_pos_preset.data_names:
                    if current_deploy_setpoint[dname] != deploy_pos_preset.rx_data[dname]:
                        self.assertTrue(True)
                        return
                self.assertTrue(False, 'New DEPLOY position did not set')
            deploy_pos_preset.request(self.product.connection, base_devid)
            time.sleep(0.2)
        else:
            self.assertTrue(False, 'Failed to get POS_PRESET_SET_1 data on Alpha 5 (2)')














if __name__ == '__main__':
    unittest.main()