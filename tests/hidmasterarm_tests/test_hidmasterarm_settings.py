import os, sys, time
import unittest
from copy import deepcopy

from RC_Core.commconnection import CommConnection, CommType

from RC_Core.connectionmanager import ConnectionManager
from RC_Core.devicemanager.hid_masterarm.hidmasterarm import HIDMasterArm
from RC_Core.devicemanager.hid_masterarm.hidmasterarm_enum import HIDMasterArmEnum
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2
from RC_Core.devicemanager.product import ProductBravo7, ProductAlpha5, ProductBravo5
from test.product_connection_tests.udp_tcp_arm_test import TCPArmTest


SERIAL_PORT = 'COM7'



class HIDMasterArmAutoConfigTest(unittest.TestCase):
    '''HIDMasterArmAutoConfigTest
    Tests auto config settings for the HID master arm. Assumes it is a 5fn.
    '''

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager: ConnectionManager = ConnectionManager.get_instance()
        self.hid_masterarm: HIDMasterArm = HIDMasterArm()
        self.hid_masterarm.masterarm_axis_b_rotate = True
        self.product = None
        self.received_packet = False
        self.connection_manager.external_load()

    def tearDown(self) -> None:
        '''
        Close
        :return:
        '''
        self.connection_manager = None
        self.product = None
        self.received_packet = False

    def test_alpha5_upright_hidmasterarm_autoconfig(self):
        '''
        Tests whether the auto config works.
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.hid_masterarm.set_target_product(self.product)
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn5_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn5_model_numbers[0], 0)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Alpha 5 Upright autoconfig not set')

    def test_alpha5_upright_hidmasterarm_7fn_autoconfig(self):
        '''
        Tests whether the auto config works.
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.hid_masterarm.set_target_product(self.product)
        self.hid_masterarm.masterarm_model_number = MasterArmV2.fn7_model_numbers[0]
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn7_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn7_model_numbers[0], 0)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Alpha 5 7fn Upright autoconfig not set')

    def test_alpha5_inverted_hidmasterarm_autoconfig(self):
        '''
        Tests whether alpha 5 with inverted status gets correct auto config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.product.set_inverted(True)
        self.hid_masterarm.set_target_product(self.product)
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn5_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn5_model_numbers[0], 1)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Alpha 5 Inverted autoconfig not set')

    def test_alpha5_axis_b_joystick_hidmasterarm_autoconfig(self):
        '''
        Tests whether Alpha 5 config with joystick control for axis b gets set
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.hid_masterarm.set_target_product(self.product)
        self.hid_masterarm.masterarm_axis_b_rotate = False
        expected_ext_devid_config = \
            deepcopy(self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn5_model_numbers[0], 0))
        expected_pos_scale_config = \
            deepcopy(self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn5_model_numbers[0], 0))
        expected_pos_scale_config[2] = -1.0 * expected_pos_scale_config[2]

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Alpha 5 axis b joystick autoconfig not set')

    def test_bravo7_hidmasterarm_autoconfig(self):
        '''
        Tests whether Bravo 7 auto-configs
        :return:
        '''
        self.product: ProductBravo7 = ProductBravo7()
        self.hid_masterarm.set_target_product(self.product)
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn5_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn5_model_numbers[0], 0)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Bravo 7 autoconfig not set')

    def test_bravo7_hidmasterarm_7fn_autoconfig(self):
        '''
        Tests whether the auto config works.
        :return:
        '''
        self.product: ProductBravo7 = ProductBravo7()
        self.hid_masterarm.set_target_product(self.product)
        self.hid_masterarm.masterarm_model_number = MasterArmV2.fn7_model_numbers[0]
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn7_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn7_model_numbers[0], 0)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Bravo 7 7fn Upright autoconfig not set')

    def test_bravo5_hidmasterarm_autoconfig(self):
        '''
        Tests whether Bravo 5 auto-configs
        :return:
        '''
        self.product: ProductBravo5 = ProductBravo5()
        self.hid_masterarm.set_target_product(self.product)
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn5_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn5_model_numbers[0], 0)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Bravo 5 autoconfig not set')

    def test_bravo5_hidmasterarm_7fn_autoconfig(self):
        '''
        Tests whether the auto config works.
        :return:
        '''
        self.product: ProductBravo5 = ProductBravo5()
        self.hid_masterarm.set_target_product(self.product)
        self.hid_masterarm.masterarm_model_number = MasterArmV2.fn7_model_numbers[0]
        expected_ext_devid_config = \
            self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn7_model_numbers[0], 0)
        expected_pos_scale_config = \
            self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn7_model_numbers[0], 0)

        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Bravo 5 7fn Upright autoconfig not set')

    def test_alpha5_custom_pos_scale_hidmasterarm_autoconfig(self):
        '''
        Checks whether setting a custom config does not get overwritten by auto-config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.hid_masterarm.set_target_product(self.product)
        expected_ext_devid_config = \
            deepcopy(self.product.master_arm_mapppings.get_external_device_ids(MasterArmV2.fn5_model_numbers[0], 0))
        expected_pos_scale_config = [1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0]

        # Set custom settings
        self.hid_masterarm.use_default_settings = False
        self.hid_masterarm.target_position_scale = expected_pos_scale_config
        self.hid_masterarm.target_ext_device_ids = expected_ext_devid_config

        # Wait 2.0s to ensure it has not changed by autoconfig
        time.sleep(2.0)
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Alpha 5 custom config autoconfig not set')

    def test_alpha5_custom_ext_devs_hidmasterarm_autoconfig(self):
        '''
        Checks whether setting a custom config does not get overwritten by auto-config
        :return:
        '''
        self.product: ProductAlpha5 = ProductAlpha5()
        self.hid_masterarm.set_target_product(self.product)
        expected_ext_devid_config = \
            [0x00, 0xC1, 0x00, 0x00, 0x00, 0xC5, 0xC3, 0xC2]
        expected_pos_scale_config = \
            deepcopy(self.product.master_arm_mapppings.get_position_scale(MasterArmV2.fn5_model_numbers[0], 0))
        expected_pos_scale_config[2] = -1.0 * expected_pos_scale_config[2]

        # Set custom settings
        self.hid_masterarm.use_default_settings = False
        self.hid_masterarm.target_position_scale = expected_pos_scale_config
        self.hid_masterarm.target_ext_device_ids = expected_ext_devid_config

        # Wait 2.0s to ensure it has not changed by autoconfig
        time.sleep(2.0)
        timeout = 10.0
        ts = time.time()
        while time.time() < (ts + timeout):
            time.sleep(0.2)
            if self.hid_masterarm.connected and self.hid_masterarm.data_available:
                if self.hid_masterarm.target_ext_device_ids == expected_ext_devid_config and \
                        self.hid_masterarm.target_position_scale == expected_pos_scale_config:
                    self.assertTrue(True)
                    return
        self.assertTrue(False, msg='Alpha 5 custom config autoconfig not set')

if __name__ == '__main__':
    unittest.main()






