from multiprocessing import Pipe

from RC_Core.connection_server import ConnectionServer, ConnectionClient, CommType
import asyncio




import logging
logging.basicConfig(handlers=[], level=logging.DEBUG)



async def main():
    server_pipe, child_pipe = Pipe()
    cs = ConnectionServer(server_pipe)

    cc = ConnectionClient(child_pipe)

    await asyncio.sleep(1.0)
    print("Creating connection")
    connection = await cc.create_connection()
    print("Done connection")
    await asyncio.sleep(1.0)
    print("Hello Creating Serial Connection")
    await connection.set_type(CommType.SERIAL, ["COM32", 115200, 8, 'N', 1])
    await asyncio.sleep(0.5)
    print("Created")
    await connection.start()
    await asyncio.sleep(0.5)

    while True:
        await asyncio.sleep(1.0)
        print("Hello Main loop")
        connection.send_bytes(b'Hello World')

        # child_pipe.send_bytes(b'HELLO WORLD')


if __name__ == '__main__':
    import sys

    if sys.platform == 'win32':
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)

    loop = asyncio.get_event_loop()

    loop.run_until_complete(main())
