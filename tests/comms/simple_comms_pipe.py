import asyncio
import multiprocessing
import time
from multiprocessing.connection import Connection, Pipe

from RC_Core.connections import PipeConnection
import logging

logger_connection = logging.getLogger("RC_Core.comms.connections")
logger_connection.setLevel(logging.DEBUG)

import sys
if sys.platform == 'win32':
    loop = asyncio.ProactorEventLoop()
    asyncio.set_event_loop(loop)

def child_process_reader_responder(connection: Connection):
    loop = asyncio.get_event_loop()

    pipe_connection = PipeConnection(connection)

    loop.run_until_complete(pipe_connection.connect())

    pipe_connection.add_bytes_callback(bytes_callback)


    # pipe_connection: PipeProtocol = loop.run_until_complete(create_pipe_protocol_connection(connection))

    loop.run_until_complete(async_loop(pipe_connection))

def bytes_callback(data):

    print(len(data))
    print("DATA", {data})

async def async_loop(pipe_connection):
    ii = 0
    while True:
        await asyncio.sleep(2)
        print(f"LOOPING {ii}")
        ii = ii+1
        # pipe_connection.send_bytes(b"GDAY MATES")


if __name__ == '__main__':
    parent_connection, child_connection = Pipe(duplex=True)
    p = multiprocessing.Process(target=child_process_reader_responder, args=(child_connection, ))

    p.start()

    i=0
    while True:
        string_data = f"Hello {i}"
        data = string_data.encode("ascii")
        print(f"Parent Sending {data}")
        parent_connection.send_bytes(data)

        if parent_connection.poll(0.1):
            response = parent_connection.recv_bytes()
            print(f"ParentReceived response {response}")
        else:
            print("Parent received no response")
        i+=1
        time.sleep(1)