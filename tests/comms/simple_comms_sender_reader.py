
import serial
import time

port = "COM33"
baud = 115200
period = 0.1

if __name__ == '__main__':

    serial_device = serial.Serial(port, baud)
    serial_device.timeout = 0.1
    serial_device.write_timeout = 0.05
    i = 0
    while True:


        string_data = f"Hello {i}"
        data = string_data.encode("ascii")
        print(f"Sending  {data}")
        try:
            serial_device.write(data)
        except:
            print(f"Could Not Write")

        read_data = serial_device.read(4096)
        if read_data:
            print(f"Received {read_data}")
        else:
            print("Received No Response")

        i += 1


