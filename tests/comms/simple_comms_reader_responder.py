
import serial

port = "COM33"
baud = 115200


if __name__ == '__main__':

    serial_device = serial.Serial(port, baud)
    serial_device.timeout = 0.0
    while True:
        data = serial_device.read(4096)
        if data:
            print(f"Received {data}")
            response_data = data + b' ACK'
            print(f"Sending  {response_data}")
            serial_device.write(response_data)