import socket
import time

port = "COM33"
baud = 115200
period = 0.1

IP_ADDRESS = "127.0.0.1"
PORT = 7890

if __name__ == '__main__':

    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.setblocking(True)
    print("Attempting to connect to:", (IP_ADDRESS, PORT))
    tcp.settimeout(0.05)
    tcp.connect((IP_ADDRESS, PORT))
    print("Successfully connected")
    #
    # serial_device = serial.Serial(port, baud)
    # serial_device.timeout = 0.1
    # serial_device.write_timeout = 0.05
    i = 0
    while True:

        data = b'\x06\x01`\x05\x05\xdc\x00'
        # data = string_data.encode("ascii")
        print(f"Sending  {data}")
        try:
            tcp.send(data)
        except:
            print(f"Could Not Write")

        read_data = b''
        try:
            read_data = tcp.recv(1028)
        except Exception:
            print("Timed out")
        if read_data:
            print(f"Received {read_data}")
        else:
            print("Received No Response")

        i += 1

