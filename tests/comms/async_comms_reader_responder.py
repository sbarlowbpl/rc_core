import asyncio

# from RC_Core.comms.connection import SerialConnection, create_serial_connection
import serial

from RC_Core.comms.connection import SerialConnection
from RC_Core.comms.protocols import SerialProtocol, create_serial_protocol_connection
import logging
logging.basicConfig()

# logging.getLogger().setLevel(logging.DEBUG)

logger_protocols = logging.getLogger("RC_Core.comms.protocols")
logger_protocols.setLevel(logging.DEBUG)
logger_connection = logging.getLogger("rc_core.comms.connection")
logger_connection.setLevel(logging.WARNING)

port = "COM32"
baud = 115200


async def async_main():
    serial_connection = SerialConnection(port, baud)

    await serial_connection.connect()

    # serial_connection: SerialProtocol = await create_serial_protocol_connection(port, baud)

    # serial_connection.bytes_callbacks.add(read_callback)
    await asyncio.sleep(2)
    serial_connection.disconnect()
    # serial_connection.close()
    # serial_connection.transport.serial.close()

    i = 0
    while True:
        await asyncio.sleep(2)
        print(f"Loop {i}")
        i += 1


def read_callback(data: bytes):

    print(f"Received bytes {data}")
    pass


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(async_main())

