import os, sys, time
import unittest

from RC_Core.commconnection import CommConnection, CommType


from RC_Core.commconnection_methods import parsepacket
from RC_Core.packetid import Packets




class MassSendRequestTest(unittest.TestCase):
    '''
    Tests whether the connection send queue is handled properly
    '''

    def setUp(self) -> None:
        '''
        setUp
        :return:
        '''
        self.packets: Packets = Packets()
        self.connection: CommConnection = CommConnection()
        self.connection.set_type(CommType.SERIAL)
        self.connection.connected = True    # Force set connected even if not. This does not actually need to send/request successfully to test

    def tearDown(self) -> None:
        '''
        tearDown
        :return:
        '''
        self.connection.close()
        self.connection = None

    def fake_callback(self, device_id: int, packet_id: int, data: list):
        pass

    def test_request_buffer_does_not_fill(self):
        '''
        Tests whether mass spamming requests of the same kind does not fill connection.send_queue
        :return:
        '''
        for i in range(100):
            self.packets.MODE.request(self.connection, 5, self.fake_callback)
            self.packets.SOFTWARE_VERSION.request(self.connection, 3, self.fake_callback)
        print(__name__, 'test_request_buffer_does_not_fill():',
              'Length of buffer + data:', len(self.connection.send_queue), self.connection.send_queue)
        if len(self.connection.send_queue) == 2:
            self.assertTrue(True)
        else:
            msg = 'Length of request buffer too long: ' + str(len(self.connection.send_queue))
            self.assertTrue(False, msg=msg)

    def test_send_buffer_does_not_fill(self):
        '''
        Tests whether mass spamming requests of the same kind does not fill connection.send_queue
        :return:
        '''
        for i in range(100):
            self.packets.MODE.send(self.connection, 5, [i])
            self.packets.SOFTWARE_VERSION.send(self.connection, 3, [i, i+1, i+2])
        print(__name__, 'test_request_buffer_does_not_fill():',
              'Length of buffer + data:', len(self.connection.send_queue), self.connection.send_queue)
        if len(self.connection.send_queue) == 2:
            self.assertTrue(True)
        else:
            msg = 'Length of send buffer too long: ' + str(len(self.connection.send_queue))
            self.assertTrue(False, msg=msg)

    def test_combined_buffer_does_not_fill(self):
        '''
        Tests whether mass spamming requests of the same kind does not fill connection.send_queue
        :return:
        '''
        for i in range(100):
            self.packets.MODE.send(self.connection, 5, [i])
            self.packets.SOFTWARE_VERSION.send(self.connection, 3, [i, i+1, i+2])
            self.packets.MODE.request(self.connection, 5, self.fake_callback)
            self.packets.SOFTWARE_VERSION.request(self.connection, 3, self.fake_callback)
        print(__name__, 'test_request_buffer_does_not_fill():',
              'Length of buffer + data:', len(self.connection.send_queue), self.connection.send_queue)
        if len(self.connection.send_queue) == 4:
            self.assertTrue(True)
        else:
            msg = 'Length of send/request buffer too long: ' + str(len(self.connection.send_queue))
            self.assertTrue(False, msg=msg)




if __name__ == '__main__':
    unittest.main()