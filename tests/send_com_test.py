comport = "COM32"
baud = 115200

frequency = 20

import serial
import time

if __name__ == '__main__':

    ser = serial.Serial(comport, baud)
    # ser.port = comport
    # ser
    # ser.open()
    send_time = time.time() + 1/frequency
    while True:
        if time.time() >= send_time:
            ser.write(b'Packet ')
            send_time += 1/frequency
