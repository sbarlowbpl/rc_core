import time
import unittest

import serial

from RC_Core.commconnection_methods import parsepacket
from RC_Core.packetid import Packets

from RC_Core.devicemanager.product import ProductAlpha5

from RC_Core.connectionmanager import ConnectionManager

from RC_Core.commconnection import CommConnection, CommType
from RC_Core.test.product_connection_tests.udp_tcp_arm_test import TCPArmTest, UDPArmTest

SERIAL_PORT = "COM9"
IP = '127.0.0.1'
UDP_PORT = 12646
TCP_PORT = 7890
DEVICE_ID = 1
USE_UDP_TCP_SIMULATIONS = True





class ProductConnectionTest(unittest.TestCase):

    def setUp(self) -> None:
        '''
        Main setup
        :return:
        '''
        self.connection_manager = ConnectionManager.get_instance()
        fake_conn = CommConnection()
        self.product = ProductAlpha5()
        self.product.set_new_heartbeat_array([])
        self.received_packet = False
        self.connection_manager.external_load()

    def tearDown(self) -> None:
        '''
        Close connection after finished test
        :return:
        '''
        self.connection.close()
        CommConnection.connections.remove(self.connection)
        self.connection_manager = None
        self.product = None
        self.received_packet = False

    def test_serial(self):
        '''
        Test connection to serial object and request data.
        This must be tested on a real product.
        :return:
        '''
        # Connect
        self.connection = CommConnection()
        self.connection.set_type(CommType.SERIAL)
        self.connection.config_connection(SERIAL_PORT, None, baudrate=115200, parity=serial.PARITY_NONE)
        self.connection.connect()
        if not self.connection.connected:
            self.assertTrue(False, msg='Could not connect to SERIAL port ' + SERIAL_PORT)

        # Request packet
        timeout = 3.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            self.product.devices[DEVICE_ID].DEVICE_ID.request(self.connection, DEVICE_ID)
            if self.product.devices[DEVICE_ID].DEVICE_ID.rx_data['value']:
                print(__name__, 'test_serial():', 'Received device_id:', hex(DEVICE_ID))
                self.assertTrue(True)
                return
            time.sleep(0.1)
        self.assertTrue(False, msg='Did not receive DEVICE_ID packet')

    def test_udp(self):
        '''
        Test connection to udp object and request data.
        This can be tested on a simulated product.
        :return:
        '''
        if USE_UDP_TCP_SIMULATIONS:
            updtest = UDPArmTest(IP, UDP_PORT)

        # Connect
        self.connection = CommConnection()
        self.connection.set_type(CommType.UDP)
        self.connection.config_connection(UDP_PORT, IP, baudrate=115200, parity=serial.PARITY_NONE)
        self.connection.connect()
        if not self.connection.connected:
            self.assertTrue(False, msg='Could not connect to UDP port / IP: ' + str(UDP_PORT) + ' / ' + IP)

        # Request packet
        timeout = 3.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            self.product.devices[DEVICE_ID].DEVICE_ID.request(self.connection, DEVICE_ID)
            if self.product.devices[DEVICE_ID].DEVICE_ID.rx_data['value']:
                print(__name__, 'test_udp():', 'Received device_id:', hex(DEVICE_ID))
                self.assertTrue(True)
                return
            time.sleep(0.1)
        self.assertTrue(False, msg='Did not receive DEVICE_ID packet')

    def test_tcp(self):
        '''
        Test connection to tcp object and request data.
        This can be tested on a simulated product.
        :return:
        '''
        if USE_UDP_TCP_SIMULATIONS:
            tcptest = TCPArmTest(IP, TCP_PORT)
            time.sleep(0.5)

        # Connect
        self.connection = CommConnection()
        self.connection.set_type(CommType.TCP)
        self.connection.config_connection(TCP_PORT, IP, baudrate=115200, parity=serial.PARITY_NONE)
        self.connection.connect()
        if not self.connection.connected:
            self.assertTrue(False, msg='Could not connect to TCP port / IP: ' + str(TCP_PORT) + ' / ' + IP)

        # Request packet
        timeout = 3.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            self.product.devices[DEVICE_ID].DEVICE_ID.request(self.connection, DEVICE_ID)
            if self.product.devices[DEVICE_ID].DEVICE_ID.rx_data['value']:
                print(__name__, 'test_tcp():', 'Received device_id:', hex(DEVICE_ID))
                self.assertTrue(True)
                return
            time.sleep(0.1)
        self.assertTrue(False, msg='Did not receive DEVICE_ID packet')


if __name__ == '__main__':
    unittest.main()