import socket
import threading
import time
from typing import Union, List

from RC_Core.commconnection import CommConnection


from RC_Core.commconnection_methods import encode_packet, packet_splitter, parsepacket
from RC_Core.enums.packets import PacketID

DEVICE_ID = 1


class UDPArmTest():
    '''UDPArmTest
    Test UDP connection
    '''
    def __init__(self, IP, UDP_PORT):
        '''
        Start UPD sim
        :param IP:
        :param UDP_PORT:
        '''
        self.IP_PORT = (IP, UDP_PORT)
        self.auto_connect_thread = threading.Thread(target=self.main_thread)
        self.auto_connect_thread.setDaemon(True)
        self.auto_connect_thread.start()

    def main_thread(self):
        # Set socket
        self.timeout = 0
        self.socket = None
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False)
        self.socket.bind(self.IP_PORT)
        self.socket.sendto(bytes([1]), self.IP_PORT)
        self.incomplete_pckt = bytearray([])
        self.target_addr = None
        # Wait for request
        timeout = 3.0
        start_time = time.time()
        while time.time() < (timeout + start_time):
            packets = self._read_udp()
            if packets and len(packets) > 0:
                # print(__name__, 'main_thread():', packets)
                for packet in packets:
                    packet = parsepacket(packet)
                    if packet[0]:
                        print(__name__, 'main_thread():', hex(packet[0]), hex(packet[1]), ["0x%02X" % d for d in packet[2]])
                        if packet[0] == DEVICE_ID and packet[1] == PacketID.REQUEST and packet[2][0] == PacketID.DEVICE_ID:
                            if self.target_addr:
                                send_data = encode_packet(DEVICE_ID, PacketID.DEVICE_ID, bytes([DEVICE_ID]))
                                a = self.socket.sendto(send_data, self.target_addr)
                                pass

    def _read_udp(self, num_bytes=4000):
        try:
            while True:
                newBytes, addr = self.socket.recvfrom(num_bytes)
                self.target_addr = addr
                if newBytes and newBytes != b'':
                    # print('incoming (dt:', round(time.time()-self.time_last_read_bytes, 3), '):',
                    # ["0x%02X" % db for db in newBytes])
                    self.time_last_read_bytes = time.time()
                    self.time_of_last_bytes = time.time()
                    self.incomplete_pckt += newBytes
        except BlockingIOError:
            self.time_last_read_bytes = time.time()
            packets = bytes([])
            try:
                packets, incomplete_packet = packet_splitter(self.incomplete_pckt)
                if incomplete_packet is not None:
                    self.incomplete_pckt = incomplete_packet
            except IndexError:
                self.incomplete_pckt = bytearray([])
            if packets is None or packets == [b''] or packets == []:
                return []
            return packets
        except socket.timeout:
            # print('incoming (dt:', round(time.time()-self.time_last_read_bytes, 3), '):',
            # ["0x%02X" % db for db in newBytes])
            self.time_last_read_bytes = time.time()
            packets, incomplete_packet = packet_splitter(self.incomplete_pckt)
            if incomplete_packet is not None:
                self.incomplete_pckt = incomplete_packet
            return packets





class TCPArmTest():
    '''TCPArmTest
    Test TCP connection
    '''
    def __init__(self, IP, TCP_PORT):
        '''
        Connect, then wait for request
        :param IP:
        :param TCP_PORT:
        '''
        self.IP_PORT = (IP, TCP_PORT)
        self.auto_connect_thread = threading.Thread(target=self.main_thread)
        self.auto_connect_thread.setDaemon(True)
        self.auto_connect_thread.start()

    def main_thread(self):
        # Set socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.IP_PORT)
        self.tcp_conn = None
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])

        # Wait for connection
        while self.tcp_conn is None:
            time.sleep(0.05)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')

        # Wait for request
        timeout = 3.0
        start_time = time.time()
        while time.time() < (start_time + timeout):
            packets_raw = self._read_tcp()
            for packet_raw in packets_raw:
                packet = parsepacket(packet_raw)
                if packet[1] == PacketID.REQUEST and PacketID.DEVICE_ID in packet[2]:
                    self._send_tcp(DEVICE_ID, PacketID.DEVICE_ID, bytes([DEVICE_ID]), append_only=False)

        self.tcp_conn.close()
        self.tcp_conn = None

    def _read_tcp(self, num_bytes=4096):
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1