import os, sys, time
import socket

from RC_Core.commconnection_methods import parsepacket, packet_splitter, encode_packet

from RC_Core.RS1_hardware import Mode, PacketID

from RC_Core.devicemanager.product import ProductRT, ProductRL






class RLSim():

    def __init__(self, ip_port):
        self.ip_port = ip_port
        self.device_id = 0xa1
        self.openloop = 0.0
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.ip_port)
        self.sock.setblocking(False)
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])
        self.tcp_conn = None
        self.start_time = time.time()
        self.wait_for_connection()

    def wait_for_connection(self):
        # Wait for connection
        while self.tcp_conn is None:
            time.sleep(0.5)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')
        self.auto_loop()

    def auto_loop(self):
        time.sleep(0.005)
        while True:
            # Read and action current packets
            packets = self._read_tcp()
            for packet_raw in packets:
                packet = parsepacket(packet_raw)
                if packet[0] == self.device_id:
                    if packet[1] == PacketID.OPENLOOP:
                        self.openloop = float(packet[2][0])
                    if packet[1] == PacketID.REQUEST:
                        if PacketID.OPENLOOP in packet[2]:
                            self._send_tcp(self.device_id, PacketID.OPENLOOP, [float(self.openloop)])

            self._send_tcp(None, None, None, append_only=False)
            time.sleep(0.005)

    def _read_tcp(self, num_bytes=4096):
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1





class RTSim():

    def __init__(self, ip_port):
        self.ip_port = ip_port
        self.device_id = 0xb0
        self.position = 3.14159/2.0
        self.velocity = 0.0
        self.mode = Mode.POSITION_CONTROL
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.ip_port)
        self.sock.setblocking(False)
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])
        self.tcp_conn = None
        self.start_time = time.time()
        self.wait_for_connection()

    def wait_for_connection(self):
        # Wait for connection
        while self.tcp_conn is None:
            time.sleep(0.5)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')
        self.auto_loop()

    def auto_loop(self):
        time.sleep(0.005)
        time_last = time.time()
        while True:
            dt = time.time() - time_last
            if self.mode == Mode.VELOCITY_CONTROL:
                self.position += (dt*self.velocity)
                if self.position > (3.0/4.0*3.14159):
                    self.position = (3.0/4.0*3.14159)
                elif self.position < (1.0/6.0*3.14159):
                    self.position = (1.0/6.0*3.14159)
            # Read and action current packets
            packets = self._read_tcp()
            for packet_raw in packets:
                packet = parsepacket(packet_raw)
                if packet[0] == self.device_id:
                    if packet[1] == PacketID.POSITION:
                        self.position = float(packet[2][0])
                        self.mode = Mode.POSITION_CONTROL
                        self.velocity = 0.0
                    elif packet[1] == PacketID.VELOCITY:
                        self.velocity = float(packet[2][0])
                        self.mode = Mode.VELOCITY_CONTROL
                    elif packet[1] == PacketID.REQUEST:
                        if PacketID.POSITION in packet[2]:
                            self._send_tcp(self.device_id, PacketID.POSITION, [float(self.position)])
                        if PacketID.VELOCITY in packet[2]:
                            self._send_tcp(self.device_id, PacketID.VELOCITY, [float(self.velocity)])
                        if PacketID.MODE in packet[2]:
                            self._send_tcp(self.device_id, PacketID.MODE, [int(self.mode)])

            self._send_tcp(None, None, None, append_only=False)
            time_last = time.time()
            time.sleep(0.005)


    def _read_tcp(self, num_bytes=4096):
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1











if __name__ == '__main__':
    rl_sim = RLSim(('', 9876))
    # rt_sim = RTSim(('', 9876))