import math
import os, sys, time
import socket

from RC_Core.RS1_hardware import PacketID, Mode
from RC_Core.packetid import Packets
from RC_Core.commconnection_methods import parsepacket, packet_splitter, encode_packet

SIN_WAVE_FREQ = 1.0/30.0

class Alpha5SineWaveSimulation():

    def __init__(self, ip_port, device_ids, base_device_id):
        self.ip_port = ip_port
        self.device_ids = device_ids
        self.base_device_id = base_device_id
        self.velocity_output = [0] * len(self.device_ids)
        self.position_output = [0] * len(self.device_ids)
        self.current_output = [0] * len(self.device_ids)
        self.mode_output = [Mode.VELOCITY_CONTROL] * len(self.device_ids)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.ip_port)
        self.sock.setblocking(False)
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])
        self.tcp_conn = None
        self.start_time = time.time()
        self.preset_name_0 = [80, 110, 97, 109, 101, 32, 110, 48]
        self.preset_name_1 = [80, 110, 97, 109, 101, 32, 110, 49]
        self.preset_name_2 = [80, 110, 97, 109, 101, 32, 110, 50]
        self.preset_name_3 = [80, 110, 97, 109, 101, 32, 110, 51]

        self.wait_for_connection()

    def wait_for_connection(self):
        # Wait for connection
        while self.tcp_conn is None:
            time.sleep(0.5)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')
        self.auto_loop()

    def auto_loop(self):
        time_loop = time.time()
        time.sleep(0.005)
        while True:
            dt = time.time() - time_loop
            time_loop = time.time()
            self._increment_sine_wave_simulation(dt)
            packets = self._read_tcp()
            for packet_raw in packets:
                packet = parsepacket(packet_raw)
                device_id_index = None
                try:
                    device_id_index = self.device_ids.index(int(packet[0]))
                    if device_id_index is None:
                        continue
                except ValueError:
                    continue
                if packet[1] == PacketID.REQUEST:
                    for request in packet[2]:
                        if request == int(PacketID.POSITION):
                            self._send_tcp(packet[0], PacketID.POSITION, [float(self.position_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.VELOCITY):
                            self._send_tcp(packet[0], PacketID.VELOCITY, [float(self.velocity_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.CURRENT):
                            self._send_tcp(packet[0], PacketID.CURRENT, [float(self.current_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.MODE):
                            self._send_tcp(packet[0], PacketID.MODE, bytearray([self.mode_output[device_id_index]]), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_0):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_0, bytearray(self.preset_name_0), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_1):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_1, bytearray(self.preset_name_1), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_2):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_2, bytearray(self.preset_name_2), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_3):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_3, bytearray(self.preset_name_3), append_only=True)
                elif packet[1] == PacketID.POS_PRESET_NAME_0:
                    self.preset_name_0 = list(packet[2])
                elif packet[1] == PacketID.POS_PRESET_NAME_1:
                    self.preset_name_1 = list(packet[2])
                elif packet[1] == PacketID.POS_PRESET_NAME_2:
                    self.preset_name_2 = list(packet[2])
                elif packet[1] == PacketID.POS_PRESET_NAME_3:
                    self.preset_name_3 = list(packet[2])

            self._send_tcp(None, None, None, append_only=False)
            time.sleep(0.005)

    def _increment_sine_wave_simulation(self, dt):
        time_now = time.time() - self.start_time
        # for i, val in enumerate(self.device_ids):
        #     pos = 1.0*math.sin(2*3.14*SIN_WAVE_FREQ*time_now + i*2*3.14/len(self.device_ids))
        #     vel = 2*3.14*SIN_WAVE_FREQ*math.cos(2*3.14*SIN_WAVE_FREQ*time_now + i*2*3.14/len(self.device_ids))
        #     cur = 0.1*vel
        #     self.position_output[i] = pos
        #     self.velocity_output[i] = vel
        #     self.current_output[i] = cur
        pos_c = 2*3.1415 * math.sin(2 * 3.14 * SIN_WAVE_FREQ * time_now + 2 * 2 * 3.14 / len(self.device_ids))
        vel_c = 2 * 3.14 * SIN_WAVE_FREQ * math.cos(2 * 3.14 * SIN_WAVE_FREQ * time_now + 2 * 2 * 3.14 / len(self.device_ids))
        self.position_output = [5.0, 0, pos_c, 1.6, 0]
        self.velocity_output = [0, 0, vel_c, 0, 0]
        # print(self.position_output[2], self.velocity_output[2], self.current_output[2])

    def _read_tcp(self, num_bytes=4096):
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1












if __name__ == '__main__':
    r5m_sim_ip_port = ('', 6789)
    r5m_sim_device_ids = [1, 2, 3, 4, 5]
    r5m_sim_base_device_id = 5
    r5m_sim = Alpha5SineWaveSimulation(r5m_sim_ip_port, r5m_sim_device_ids, r5m_sim_base_device_id)
