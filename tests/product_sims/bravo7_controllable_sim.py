import math
import os, sys, time
import socket
from copy import deepcopy
from random import uniform
from typing import List, Union

from RC_Core.RS1_hardware import Mode, get_name_of_packet_id, DeviceType
from RC_Core.enums.packets import PacketID
from RC_Core.packetid import Packets
from RC_Core.commconnection_methods import parsepacket, packet_splitter, encode_packet

SIN_WAVE_FREQ = 1/4

class BRavo7SineWaveSimulation():

    def __init__(self, ip_port, device_ids, ma_device_ids, mcu_device_id, base_device_id):
        self.ip_port = ip_port
        self.active_device_ids = device_ids
        self.masterarm_device_ids = ma_device_ids
        self.device_ids = deepcopy(device_ids)
        self.device_ids.append(mcu_device_id)
        self.device_ids.append(base_device_id)
        self.base_device_id = base_device_id
        self.mcu_device_id = mcu_device_id
        self.device_types = [DeviceType.ROTATE] * len(self.device_ids)
        self.device_types[0] = DeviceType.LINEAR
        self.velocity_output = [0] * len(self.active_device_ids)
        self.position_output = [0.0, 0.0, 2.50, 0.0, 0.99, 1.92, 0.0]
        self.current_output = [0] * len(self.active_device_ids)
        self.torque_output = [0] * len(self.active_device_ids)
        self.velocity_control = [0] * len(self.active_device_ids)
        self.indexed_position = [0] * len(self.active_device_ids)
        self.save_counter = [0] * len(self.device_ids)
        self.current_limits = [600] * len(self.active_device_ids)
        self.factory_current_limits = [1000] * len(self.active_device_ids)
        self.mode_output = [Mode.VELOCITY_CONTROL] * len(self.device_ids)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.ip_port)
        self.sock.setblocking(False)
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])
        self.tcp_conn = None

        self.preset_name_0: List[int] = [80, 110, 97, 109, 101, 32, 110, 48]
        self.preset_name_1: List[int] = [80, 110, 97, 109, 101, 32, 110, 49]
        self.preset_name_2: List[int] = [80, 110, 97, 109, 101, 32, 110, 50]
        self.preset_name_3: List[int] = [80, 110, 97, 109, 101, 32, 110, 51]
        self.kill_self = False
        self.km_mount_pos_rot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        self.box_obstacle_2 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_3 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_4 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_5 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_2 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_3 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_4 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_5 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        self.LINK_TRANSFORMS = []
        self.LINK_TRANSFORMS.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.LINK_TRANSFORMS.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.LINK_TRANSFORMS.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.LINK_TRANSFORMS.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.LINK_TRANSFORMS.append([0.0, 0.0, 182.0, 0.0, 0.0, 3.14])
        self.LINK_TRANSFORMS.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.LINK_TRANSFORMS.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        self.preset_pos_0 = deepcopy(self.position_output)
        self.preset_pos_1 = deepcopy(self.position_output)
        self.preset_pos_2 = deepcopy(self.position_output)
        self.preset_pos_3 = deepcopy(self.position_output)

        self.torque_limit = [-10.0, 10.0]

        # Factory Config STATUS tab items - to test Request/Send All functionality
        self.ati_ft_message = [1, 3, 5, 9, 7, 5, 200]
        self.ati_ft_reading = [1.0, 3.0, 5.0, 9.0, 7.0, 5.0]
        self.climate = [33.0, 0.2, 99.0]
        self.hardware_status = [0, 0, 0, 0]
        self.power = [126.24]
        self.supply_voltage = [0.0]
        self.software_version = [99, 99, 99]
        self.version = [9999.0]

        self.start_time = time.time()
        self.wait_for_connection()

    def wait_for_connection(self):
        # Wait for connection
        while self.tcp_conn is None and not self.kill_self:
            time.sleep(0.5)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')
        self.auto_loop()

    def auto_loop(self):
        time_loop = time.time()
        time.sleep(0.005)
        self.last_sent_force_heartbeat = time.time()

        while not self.kill_self:

            # Force heartbeat - can be commented off
            # if time.time() > (self.last_sent_force_heartbeat + 1.0/2.0):
            #     self.last_sent_force_heartbeat = time.time()
            #     for device_id in self.active_device_ids:
            #         device_id_index = self.device_ids.index(device_id)
            #         self._send_tcp(device_id, PacketID.TORQUE_OUTPUT, self.torque_output[device_id_index], append_only=True)

            dt = time.time() - time_loop
            time_loop = time.time()

            self._increment(dt)

            packets = self._read_tcp()
            for packet_raw in packets:
                packet = parsepacket(packet_raw)
                device_id_index: Union[int, None] = None
                device_id = int(packet[0])
                if packet[1] == PacketID.REQUEST:
                    for request in packet[2]:
                        if request not in [PacketID.POSITION, PacketID.MODE, PacketID.VELOCITY, PacketID.CURRENT, PacketID.TORQUE_OUTPUT]:
                            print(__name__, 'auto_loop():', 'device id, request:', device_id, get_name_of_packet_id(request))
                try:
                    device_id_index: Union[int, None] = None
                    if device_id in self.device_ids:
                        device_id_index: Union[int, None] = self.device_ids.index(device_id)
                    elif device_id in self.masterarm_device_ids:
                        device_id_index: Union[int, None] = self.masterarm_device_ids.index(device_id)
                    if device_id_index is None:
                        continue
                except ValueError:
                    continue
                if packet[1] == PacketID.REQUEST:
                    # if packet[0] > 7:
                    #     print(__name__, 'auto_loop():', '0x0E Request:', ["0x%02X" % d for d in packet[2]])
                    for request in packet[2]:
                        if request == int(PacketID.POSITION):
                            self._send_tcp(packet[0], PacketID.POSITION, [float(self.position_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.VELOCITY):
                            self._send_tcp(packet[0], PacketID.VELOCITY, [float(self.velocity_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.CURRENT):
                            self._send_tcp(packet[0], PacketID.CURRENT, [float(self.current_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.MODE):
                            self._send_tcp(packet[0], PacketID.MODE, bytearray([self.mode_output[device_id_index]]), append_only=True)
                        elif request == int(PacketID.CURRENT_LIMIT):
                            self._send_tcp(packet[0], PacketID.CURRENT_LIMIT, [-float(self.current_limits[0]), float(self.current_limits[0])], append_only=True)
                        elif request == int(PacketID.CURRENT_LIMIT_FACTORY):
                            self._send_tcp(packet[0], PacketID.CURRENT_LIMIT_FACTORY, [-float(self.factory_current_limits[0]), float(self.factory_current_limits[0])], append_only=True)
                        elif request == int(PacketID.LINK_TRANSFORM):
                            self._send_tcp(packet[0], PacketID.LINK_TRANSFORM, self.LINK_TRANSFORMS[device_id_index], append_only=True)
                        elif request == int(PacketID.DEVICE_TYPE):
                            self._send_tcp(packet[0], PacketID.DEVICE_TYPE, bytearray([self.device_types[device_id_index]]), append_only=True)
                        elif request == int(PacketID.MECHANICAL_VERSION):
                            self._send_tcp(packet[0], PacketID.MECHANICAL_VERSION, [9.0, 9.0, 9.0], append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_0):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_0, bytearray(self.preset_name_0), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_1):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_1, bytearray(self.preset_name_1), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_2):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_2, bytearray(self.preset_name_2), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_3):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_3, bytearray(self.preset_name_3), append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_02):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_02, self.box_obstacle_2, append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_03):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_03, self.box_obstacle_3, append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_04):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_04, self.box_obstacle_4, append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_05):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_05, self.box_obstacle_5, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_02):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_02, self.cyl_obstacle_2, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_03):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_03, self.cyl_obstacle_3, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_04):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_04, self.cyl_obstacle_4, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_05):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_05, self.cyl_obstacle_5, append_only=True)
                        elif request == int(PacketID.TORQUE_LIMIT):
                            self._send_tcp(packet[0], PacketID.TORQUE_LIMIT, self.torque_limit, append_only=True)
                        elif request == int(PacketID.TORQUE_OUTPUT):
                            self._send_tcp(packet[0], PacketID.TORQUE_OUTPUT, self.torque_output[device_id_index], append_only=True)
                        elif request == int(PacketID.SAVE):
                            self._send_tcp(packet[0], PacketID.SAVE, bytearray([self.save_counter[device_id_index]]), append_only=True)
                        elif request == int(PacketID.ATI_FT_MESSAGE):
                            self._send_tcp(packet[0], PacketID.ATI_FT_MESSAGE, bytearray(self.ati_ft_message), append_only=True)
                        elif request == int(PacketID.ATI_FT_READING):
                            self._send_tcp(packet[0], PacketID.ATI_FT_READING, self.ati_ft_reading, append_only=True)
                        elif request == int(PacketID.FACTORY_CLIMATE):
                            self._send_tcp(packet[0], PacketID.FACTORY_CLIMATE, self.climate, append_only=True)
                        elif request == int(PacketID.HARDWARE_STATUS):
                            self._send_tcp(packet[0], PacketID.HARDWARE_STATUS, bytearray(self.hardware_status), append_only=True)
                        elif request == int(PacketID.INTERNAL_TEMPERATURE):
                            self._send_tcp(packet[0], PacketID.INTERNAL_TEMPERATURE, [self.climate[0]], append_only=True)
                        elif request == int(PacketID.INTERNAL_PRESSURE):
                            self._send_tcp(packet[0], PacketID.INTERNAL_PRESSURE, [self.climate[1]], append_only=True)
                        elif request == int(PacketID.INTERNAL_HUMIDITY):
                            self._send_tcp(packet[0], PacketID.INTERNAL_HUMIDITY, [self.climate[2]], append_only=True)
                        elif request == int(PacketID.POWER):
                            self._send_tcp(packet[0], PacketID.POWER, list(self.power), append_only=True)
                        elif request == int(PacketID.SOFTWARE_VERSION):
                            self._send_tcp(packet[0], PacketID.SOFTWARE_VERSION, bytearray(self.software_version), append_only=True)
                        elif request == int(PacketID.SUPPLY_VOLTAGE):
                            self._send_tcp(packet[0], PacketID.SUPPLY_VOLTAGE, list(self.supply_voltage), append_only=True)
                        elif request == int(PacketID.VERSION):
                            self._send_tcp(packet[0], PacketID.VERSION, list(self.version), append_only=True)
                        # else:
                        #     print(__name__, 'auto_loop():', 'Requesting', get_name_of_packet_id(request), 'for device id', "0x{:02X}".format(packet[0]))
                else:
                    if packet[1] == PacketID.CURRENT_LIMIT:
                        self.current_limits[device_id_index] = abs(packet[2][1])
                    elif packet[1] == PacketID.POS_PRESET_NAME_0:
                        self.preset_name_0 = list(packet[2])
                    elif packet[1] == PacketID.POS_PRESET_NAME_1:
                        self.preset_name_1 = list(packet[2])
                    elif packet[1] == PacketID.POS_PRESET_NAME_2:
                        self.preset_name_2 = list(packet[2])
                    elif packet[1] == PacketID.POS_PRESET_NAME_3:
                        self.preset_name_3 = list(packet[2])

                    elif packet[1] == PacketID.KM_BOX_OBSTACLE_02:
                        self.box_obstacle_2 = list(packet[2])
                    elif packet[1] == PacketID.KM_BOX_OBSTACLE_03:
                        self.box_obstacle_3 = list(packet[2])
                    elif packet[1] == PacketID.KM_BOX_OBSTACLE_04:
                        self.box_obstacle_4 = list(packet[2])
                    elif packet[1] == PacketID.KM_BOX_OBSTACLE_05:
                        self.box_obstacle_5 = list(packet[2])

                    elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_02:
                        self.cyl_obstacle_2 = list(packet[2])
                    elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_03:
                        self.cyl_obstacle_3 = list(packet[2])
                    elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_04:
                        self.cyl_obstacle_4 = list(packet[2])
                    elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_05:
                        self.cyl_obstacle_5 = list(packet[2])

                    elif packet[1] == PacketID.POS_PRESET_CAPTURE:
                        if int(packet[2][0]) == 0:
                            self.preset_pos_0 = deepcopy(self.position_output)
                        elif int(packet[2][0]) == 1:
                            self.preset_pos_1 = deepcopy(self.position_output)
                        elif int(packet[2][0]) == 2:
                            self.preset_pos_2 = deepcopy(self.position_output)
                        elif int(packet[2][0]) == 3:
                            self.preset_pos_3 = deepcopy(self.position_output)

                    elif packet[1] == PacketID.KM_CONFIGURATION:
                        self.km_configuration = list(packet[2])

                    elif packet[1] == PacketID.POS_PRESET_GO:
                        if int(packet[2][0]) == 0:
                            self.position_output = deepcopy(self.preset_pos_0)
                        elif int(packet[2][0]) == 1:
                            self.position_output = deepcopy(self.preset_pos_1)
                        elif int(packet[2][0]) == 2:
                            self.position_output = deepcopy(self.preset_pos_2)
                        elif int(packet[2][0]) == 3:
                            self.position_output = deepcopy(self.preset_pos_3)

                    elif packet[1] == PacketID.INDEXED_POSITION:
                        self.position_output[device_id_index] = self.indexed_position[device_id_index] + packet[2][0]
                        self.mode_output[device_id_index] = Mode.INDEXED_POSITION_CONTROL
                        self.velocity_control[device_id_index] = 0
                        pass
                    elif packet[1] == PacketID.VELOCITY:
                        self.velocity_control[device_id_index] = packet[2][0]
                        self.mode_output[device_id_index] = Mode.VELOCITY_CONTROL
                        self.indexed_position[device_id_index] = self.position_output[device_id_index]
                    elif packet[1] == PacketID.POSITION:
                        self.position_output[device_id_index] = packet[2][0]
                        self.velocity_control[device_id_index] = 0
                        self.mode_output[device_id_index] = Mode.POSITION_CONTROL
                        self.indexed_position[device_id_index] = self.position_output[device_id_index]
                    elif packet[1] == PacketID.POSITION_VELOCITY_DEMAND:
                        self.velocity_control[device_id_index] = packet[2][0]
                        self.mode_output[device_id_index] = Mode.POSITION_VELOCITY_CONTROL
                    elif packet[1] in [PacketID.MODE]:
                        self.mode_output[device_id_index] = packet[2][0]
                        if packet[2][0] not in [Mode.INDEXED_POSITION_CONTROL, Mode.POSITION_VELOCITY_CONTROL]:
                            self.indexed_position[device_id_index] = self.position_output[device_id_index]
                        elif packet[2][0] not in [Mode.VELOCITY_CONTROL, Mode.POSITION_VELOCITY_CONTROL]:
                            self.velocity_control[device_id_index] = 0

                    elif packet[1] == PacketID.TORQUE_LIMIT:
                        self.torque_limit = list(packet[2])

                    elif packet[1] == PacketID.SAVE:
                        if self.mode_output[device_id_index] == Mode.FACTORY:
                            self.save_counter[device_id_index] += 1

                    elif packet[1] == PacketID.ICMU_PARAMETERS:
                        print(__name__, 'auto_loop():', 'ICMU_PARAMETERS', packet[2])

                    # Factory config tests STATUS tab
                    elif packet[1] == PacketID.ATI_FT_MESSAGE:
                        self.ati_ft_message = list(packet[2])
                    elif packet[1] == PacketID.ATI_FT_READING:
                        self.ati_ft_reading = list(packet[2])
                    elif packet[1] == PacketID.FACTORY_CLIMATE:
                        self.climate = list(packet[2])
                    elif packet[1] == PacketID.HARDWARE_STATUS:
                        self.hardware_status = list(packet[2])
                    elif packet[1] == PacketID.INTERNAL_TEMPERATURE:
                        self.climate[0] = packet[2][0]
                    elif packet[1] == PacketID.INTERNAL_PRESSURE:
                        self.climate[1] = packet[2][0]
                    elif packet[1] == PacketID.INTERNAL_HUMIDITY:
                        self.climate[2] = packet[2][0]
                    elif packet[1] == PacketID.POWER:
                        self.power = list(packet[2])
                    elif packet[1] == PacketID.SUPPLY_VOLTAGE:
                        self.supply_voltage = list(packet[2])
                    elif packet[1] == PacketID.SOFTWARE_VERSION:
                        self.software_version = list(packet[2])
                    elif packet[1] == PacketID.VERSION:
                        self.version = list(packet[2])



                    else:
                        print(__name__, 'auto_loop():', 'Command', get_name_of_packet_id(packet[1]), 'for device id', "0x{:02X}".format(packet[0]))

            self._send_tcp(None, None, None, append_only=False)
            time.sleep(0.005)
        self.tcp_conn.close()
        self.sock.close()

    def _increment(self, dt):
        for device_id in self.active_device_ids:
            device_id_index = self.device_ids.index(device_id)
            if self.mode_output[device_id_index] == Mode.VELOCITY_CONTROL:
                self.position_output[device_id_index] += (dt*self.velocity_control[device_id_index])
                self.current_output[device_id_index] = 200*self.velocity_control[device_id_index]
            elif self.mode_output[device_id_index] == Mode.POSITION_VELOCITY_CONTROL:
                self.position_output[device_id_index] += (dt * self.velocity_control[device_id_index])
                self.current_output[device_id_index] = 200 * self.velocity_control[device_id_index]
            self.current_output[device_id_index] += 20.0*uniform(-1.0, 1.0)
            self.torque_output[device_id_index] = 10* self.current_output[device_id_index]

    # def _increment_sine_wave_simulation(self, dt):
    #     time_now = time.time() - self.start_time
    #     for i, val in enumerate(self.active_device_ids):
    #         pos = 1.0*math.sin(2*3.14*SIN_WAVE_FREQ*time_now + i*2*3.14/len(self.active_device_ids))
    #         vel = 2*3.14*SIN_WAVE_FREQ*math.cos(2*3.14*SIN_WAVE_FREQ*time_now + i*2*3.14/len(self.active_device_ids))
    #         cur = self.current_limits[i] * math.cos(2 * 3.14 * SIN_WAVE_FREQ * time_now + i * 2 * 3.14 / len(self.device_ids))
    #         trq = cur*2.0
    #
    #         self.position_output[i] = pos
    #         self.velocity_output[i] = vel
    #         self.current_output[i] = cur
    #         self.torque_output[i] = trq


        # self.position_output = [5.0, 0, 0.3, 1.6, 0]
        # print(self.position_output[2], self.velocity_output[2], self.current_output[2])

    def _read_tcp(self, num_bytes=4096):
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1












if __name__ == '__main__':
    bravo7_sim_ip_port = ('127.0.0.1', 7890)
    bravo7_sim_device_ids = [1, 2, 3, 4, 5, 6, 7]
    bravo7_sim_ma_device_ids = [0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7]
    bravo7_mcu_device_id = 0x0D
    bravo7_sim_base_device_id = 0x0E
    bravo7_sim = BRavo7SineWaveSimulation(bravo7_sim_ip_port, bravo7_sim_device_ids,
                                          bravo7_sim_ma_device_ids, bravo7_mcu_device_id,
                                          bravo7_sim_base_device_id)
