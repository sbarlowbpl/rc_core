from random import random, randrange, uniform

import math
import os, sys, time
import socket
from copy import deepcopy

from RC_Core.RS1_hardware import Mode, get_name_of_packet_id, DeviceType
from RC_Core.enums.packets import PacketID
from RC_Core.packetid import Packets
from RC_Core.commconnection_methods import parsepacket, packet_splitter, encode_packet

SIN_WAVE_FREQ = 1/4

class Alpha5SineWaveSimulation():

    def __init__(self, ip_port, device_ids, ma_device_ids, base_device_id):
        self.ip_port = ip_port
        self.device_ids = device_ids
        self.masterarm_device_ids = ma_device_ids
        self.base_device_id = base_device_id
        self.velocity_output = [0] * len(self.device_ids)
        self.position_output = [0] * len(self.device_ids)
        self.current_output = [0] * len(self.device_ids)
        self.velocity_control = [0] * len(self.device_ids)
        self.indexed_position = [0] * len(self.device_ids)
        self.mode_output = [Mode.VELOCITY_CONTROL] * len(self.device_ids)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind(self.ip_port)
        self.sock.setblocking(False)
        self.incomplete_pckt = bytearray([])
        self.send_lock = False
        self.send_freq_queue = bytearray([])
        self.tcp_conn = None
        self.start_time = time.time()

        self.preset_name_0 = [80, 110, 97, 109, 101, 32, 110, 48]
        self.preset_name_1 = [80, 110, 97, 109, 101, 32, 110, 49]
        self.preset_name_2 = [80, 110, 97, 109, 101, 32, 110, 50]
        self.preset_name_3 = [80, 110, 97, 109, 101, 32, 110, 51]
        self.preset_pos_0 = deepcopy(self.position_output)
        self.preset_pos_1 = deepcopy(self.position_output)
        self.preset_pos_2 = deepcopy(self.position_output)
        self.preset_pos_3 = deepcopy(self.position_output)
        self.km_configuration = [1, 1, 1, 1]

        self.current_limits = [600] * len(self.device_ids)
        self.factory_current_limits = [1000] * len(self.device_ids)
        self.km_mount_pos_rot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_2 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_3 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_4 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.box_obstacle_5 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_2 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_3 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_4 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.cyl_obstacle_5 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        self.probe_dhparams_0 = [60.0, 0.0, 0.0, 1.5707000494003296, 1.0, 1.0]
        self.probe_dhparams_1 = [0.0, 110.0, 1.5707999467849731, 0.0, -1.0, -1.0]
        self.probe_kmfloatparams = [0.0, 0.0, 0.0, 0.0, 30.0, 0.0]

        self.wait_for_connection()

    def wait_for_connection(self):
        # Wait for connection
        while self.tcp_conn is None:
            time.sleep(0.5)
            self.sock.listen(1)
            tcp_conn = None
            addr = None
            try:
                tcp_conn, addr = self.sock.accept()
            except BlockingIOError:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            except socket.timeout:
                print(__name__, '_after_load_wait_for_connection():', 'Could not connect')
                continue
            self.tcp_conn = tcp_conn
            self.tcp_conn.setblocking(False)
            print(__name__, '_after_load_wait_for_connection():', 'connected to tcp socket')
        self.auto_loop()

    def auto_loop(self):
        time_loop = time.time()
        time.sleep(0.005)
        send_alt_freq = 5.0
        time_last_alt = time.time()

        while True:
            dt = time.time() - time_loop
            time_loop = time.time()

            self._increment(dt)

            if time.time() > (time_last_alt + 1.0/send_alt_freq):
                self._send_tcp(self.base_device_id, PacketID.HARDWARE_STATUS, bytearray([0xc4,0x04,0,0]), append_only=True)
                time_last_alt = time.time()

            packets = self._read_tcp()
            for packet_raw in packets:
                packet = parsepacket(packet_raw)
                device_id_index = None
                device_id = int(packet[0])
                try:
                    device_id_index = None
                    if device_id in self.device_ids:
                        device_id_index = self.device_ids.index(device_id)
                    elif device_id in self.masterarm_device_ids:
                        device_id_index = self.masterarm_device_ids.index(device_id)
                    if device_id_index is None:
                        continue
                except ValueError:
                    continue
                if packet[1] == PacketID.REQUEST and device_id in self.device_ids:
                    for request in packet[2]:
                        if request == int(PacketID.POSITION):
                            self._send_tcp(packet[0], PacketID.POSITION, [float(self.position_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.VELOCITY):
                            self._send_tcp(packet[0], PacketID.VELOCITY, [float(self.velocity_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.CURRENT):
                            self._send_tcp(packet[0], PacketID.CURRENT, [float(self.current_output[device_id_index])], append_only=True)
                        elif request == int(PacketID.MODE):
                            self._send_tcp(packet[0], PacketID.MODE, bytearray([self.mode_output[device_id_index]]), append_only=True)

                        elif request == int(PacketID.POS_PRESET_NAME_0):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_0, bytearray(self.preset_name_0), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_1):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_1, bytearray(self.preset_name_1), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_2):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_2, bytearray(self.preset_name_2), append_only=True)

                        elif request == int(PacketID.DEVICE_TYPE):
                            if packet[0] == self.device_ids[0]:
                                self._send_tcp(packet[0], PacketID.DEVICE_TYPE, bytearray([DeviceType.LINEAR]), append_only=True)
                            else:
                                self._send_tcp(packet[0], PacketID.DEVICE_TYPE, bytearray([DeviceType.ROTATE]), append_only=True)
                        elif request == int(PacketID.POS_PRESET_NAME_3):
                            self._send_tcp(packet[0], PacketID.POS_PRESET_NAME_3, bytearray(self.preset_name_3), append_only=True)
                        elif request == int(PacketID.CURRENT_LIMIT):
                            self._send_tcp(packet[0], PacketID.CURRENT_LIMIT, [-float(self.current_limits[0]), float(self.current_limits[0])], append_only=True)
                        elif request == int(PacketID.CURRENT_LIMIT_FACTORY):
                            self._send_tcp(packet[0], PacketID.CURRENT_LIMIT_FACTORY, [-float(self.factory_current_limits[0]), float(self.factory_current_limits[0])], append_only=True)
                        elif request == int(PacketID.SERIAL_NUMBER):
                            self._send_tcp(packet[0], PacketID.SERIAL_NUMBER, [float(1234)], append_only=True)
                        elif request == int(PacketID.KM_MOUNT_POS_ROT):
                            self._send_tcp(packet[0], PacketID.KM_MOUNT_POS_ROT, self.km_mount_pos_rot, append_only=True)
                        elif request == int(PacketID.KM_END_POS):
                            self._send_tcp(packet[0], PacketID.KM_END_POS, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], append_only=True)

                        elif request == int(PacketID.KM_BOX_OBSTACLE_02):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_02, self.box_obstacle_2, append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_03):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_03, self.box_obstacle_3, append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_04):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_04, self.box_obstacle_4, append_only=True)
                        elif request == int(PacketID.KM_BOX_OBSTACLE_05):
                            self._send_tcp(packet[0], PacketID.KM_BOX_OBSTACLE_05, self.box_obstacle_5, append_only=True)

                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_02):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_02, self.cyl_obstacle_2, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_03):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_03, self.cyl_obstacle_3, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_04):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_04, self.cyl_obstacle_4, append_only=True)
                        elif request == int(PacketID.KM_CYLINDER_OBSTACLE_05):
                            self._send_tcp(packet[0], PacketID.KM_CYLINDER_OBSTACLE_05, self.cyl_obstacle_5, append_only=True)

                        elif request == int(PacketID.KM_CONFIGURATION):
                            self._send_tcp(packet[0], PacketID.KM_CONFIGURATION, bytearray(self.km_configuration), append_only=True)

                        elif request == int(PacketID.KM_DH_PARAMETERS_0) and device_id == self.device_ids[0]:
                            self._send_tcp(packet[0], PacketID.KM_DH_PARAMETERS_0, self.probe_dhparams_0, append_only=True)
                        elif request == int(PacketID.KM_DH_PARAMETERS_1) and device_id == self.device_ids[0]:
                            self._send_tcp(packet[0], PacketID.KM_DH_PARAMETERS_1, self.probe_dhparams_1, append_only=True)
                        elif request == int(PacketID.KM_FLOAT_PARAMETERS) and device_id == self.device_ids[0]:
                            self._send_tcp(packet[0], PacketID.KM_FLOAT_PARAMETERS, self.probe_kmfloatparams, append_only=True)

                        else:
                            print(__name__, 'auto_loop():', 'Request missed:', "0x{:02X}".format(packet[0]), get_name_of_packet_id(request))

                elif packet[1] == PacketID.POS_PRESET_NAME_0:
                    self.preset_name_0 = list(packet[2])
                elif packet[1] == PacketID.POS_PRESET_NAME_1:
                    self.preset_name_1 = list(packet[2])
                elif packet[1] == PacketID.POS_PRESET_NAME_2:
                    self.preset_name_2 = list(packet[2])
                elif packet[1] == PacketID.POS_PRESET_NAME_3:
                    self.preset_name_3 = list(packet[2])

                elif packet[1] == PacketID.POS_PRESET_CAPTURE:
                    if int(packet[2][0]) == 0:
                        self.preset_pos_0 = deepcopy(self.position_output)
                    elif int(packet[2][0]) == 1:
                        self.preset_pos_1 = deepcopy(self.position_output)
                    elif int(packet[2][0]) == 2:
                        self.preset_pos_2 = deepcopy(self.position_output)
                    elif int(packet[2][0]) == 3:
                        self.preset_pos_3 = deepcopy(self.position_output)

                elif packet[1] == PacketID.KM_CONFIGURATION:
                    self.km_configuration = list(packet[2])

                elif packet[1] == PacketID.POS_PRESET_GO:
                    if int(packet[2][0]) == 0:
                        self.position_output = deepcopy(self.preset_pos_0)
                    elif int(packet[2][0]) == 1:
                        self.position_output = deepcopy(self.preset_pos_1)
                    elif int(packet[2][0]) == 2:
                        self.position_output = deepcopy(self.preset_pos_2)
                    elif int(packet[2][0]) == 3:
                        self.position_output = deepcopy(self.preset_pos_3)

                elif packet[1] == PacketID.CURRENT_LIMIT:
                    self.current_limits[device_id_index] = abs(packet[2][1])
                elif packet[1] == PacketID.KM_MOUNT_POS_ROT:
                    self.km_mount_pos_rot = list(packet[2])

                elif packet[1] == PacketID.KM_BOX_OBSTACLE_02:
                    self.box_obstacle_2 = list(packet[2])
                elif packet[1] == PacketID.KM_BOX_OBSTACLE_03:
                    self.box_obstacle_3 = list(packet[2])
                elif packet[1] == PacketID.KM_BOX_OBSTACLE_04:
                    self.box_obstacle_4 = list(packet[2])
                elif packet[1] == PacketID.KM_BOX_OBSTACLE_05:
                    self.box_obstacle_5 = list(packet[2])

                elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_02:
                    self.cyl_obstacle_2 = list(packet[2])
                elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_03:
                    self.cyl_obstacle_3 = list(packet[2])
                elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_04:
                    self.cyl_obstacle_4 = list(packet[2])
                elif packet[1] == PacketID.KM_CYLINDER_OBSTACLE_05:
                    self.cyl_obstacle_5 = list(packet[2])

                elif packet[1] == PacketID.INDEXED_POSITION:
                    self.position_output[device_id_index] = self.indexed_position[device_id_index] + packet[2][0]
                    self.mode_output[device_id_index] = Mode.INDEXED_POSITION_CONTROL
                    self.velocity_control[device_id_index] = 0
                    pass
                elif packet[1] == PacketID.VELOCITY:
                    self.velocity_control[device_id_index] = packet[2][0]
                    self.mode_output[device_id_index] = Mode.VELOCITY_CONTROL
                    self.indexed_position[device_id_index] = self.position_output[device_id_index]
                elif packet[1] == PacketID.POSITION:
                    self.position_output[device_id_index] = packet[2][0]
                    self.velocity_control[device_id_index] = 0
                    self.mode_output[device_id_index] = Mode.POSITION_CONTROL
                    self.indexed_position[device_id_index] = self.position_output[device_id_index]
                elif packet[1] in [PacketID.MODE]:
                    self.mode_output[device_id_index] = packet[2][0]
                    if packet[2][0] not in [Mode.INDEXED_POSITION_CONTROL]:
                        self.indexed_position[device_id_index] = self.position_output[device_id_index]
                    elif packet[2][0] not in [Mode.VELOCITY_CONTROL]:
                        self.velocity_control[device_id_index] = 0

                elif packet[1] == PacketID.KM_DH_PARAMETERS_0 and device_id == self.device_ids[0]:
                    self.probe_dhparams_0 = list(packet[2])
                elif packet[1] == PacketID.KM_DH_PARAMETERS_1 and device_id == self.device_ids[0]:
                    self.probe_dhparams_1 = list(packet[2])
                elif packet[1] == PacketID.KM_FLOAT_PARAMETERS and device_id == self.device_ids[0]:
                    self.probe_kmfloatparams = list(packet[2])

                else:
                    print(__name__, 'auto_loop():', 'Command missed:', "0x{:02X}".format(packet[0]),
                          get_name_of_packet_id(packet[1]))

            self._send_tcp(None, None, None, append_only=False)
            time.sleep(0.005)

    def _increment(self, dt):
        for device_id in self.device_ids:
            device_id_index = self.device_ids.index(device_id)
            if self.mode_output[device_id_index] == Mode.VELOCITY_CONTROL:
                self.position_output[device_id_index] += (dt*self.velocity_control[device_id_index])
                self.current_output[device_id_index] = 50*self.velocity_control[device_id_index]
            self.current_output[device_id_index] += 4*uniform(-1.0, 1.0)

    def _read_tcp(self, num_bytes=4096):
        try:
            newBytes = self.tcp_conn.recv(num_bytes)
            if newBytes and newBytes != b'':
                buff = bytearray([])
                buff += self.incomplete_pckt
                buff += newBytes
                try:
                    packets, incomplete_packet = packet_splitter(buff)
                    if incomplete_packet is not None:
                        self.incomplete_pckt = incomplete_packet
                    return packets
                except IndexError as e:
                    return []
            return []
        except socket.timeout:
            return []
        except BlockingIOError:
            return []
        except socket.error as e:
            print(__name__, "_read_tcp():", "TCP socket is broken or not open", e)
            return []
        except Exception as e:
            print(__name__, '_read_tcp():', 'Alt Error:', e)
            return []

    def _send_tcp(self, device_id, packet_id, data_in, append_only=False):
        try:
            data = data_in
            if data_in is not None and data_in != []:
                if device_id is not None:
                    data = encode_packet(device_id, packet_id, data_in)
                self.send_freq_queue += data
            if append_only:
                return 1
            while self.send_lock:
                print(__name__, '_send_tcp():', 'send lock')
                continue
            if len(self.send_freq_queue) <= 0:
                return 1
            self.send_lock = True
            s = self.tcp_conn.sendall(self.send_freq_queue)
            self.send_freq_queue = bytearray([])
            self.send_lock = False
            return s
        except socket.error as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'socket error', e)
            return -1
        except Exception as e:
            self.send_lock = False
            self.send_freq_queue = bytearray([])
            print(__name__, '_send_tcp():', 'alt error', e)
            return -1












if __name__ == '__main__':
    r5m_sim_ip_port = ('127.0.0.1', 6789)
    r5m_sim_device_ids = [1, 2, 3, 4, 5]
    r5m_sim_ma_device_ids = [0xC1, 0xC2, 0xC3, 0xC4, 0xC5]
    r5m_sim_base_device_id = 5
    r5m_sim = Alpha5SineWaveSimulation(r5m_sim_ip_port, r5m_sim_device_ids, r5m_sim_ma_device_ids, r5m_sim_base_device_id)
