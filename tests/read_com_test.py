comport = "COM35"
baud = 115200


import serial
import time

if __name__ == '__main__':

    ser = serial.Serial(comport, baud, timeout=0)
    # ser.port = comport
    # ser
    # ser.open()
    last_time = time.time()
    while True:
        try:

            bytes_data = ser.read(1024)
            if bytes_data:
                print(f"Received: {bytes_data}, Frequency: {1/(time.time()-last_time) :.2f} Hz")
                last_time = time.time()
                # print()
        except Exception as e:
            pass
        # time.sleep(1/frequency)