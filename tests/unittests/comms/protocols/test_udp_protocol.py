import socket
import time
import unittest
import asyncio

from RC_Core.comms.protocols import UDPProtocol, create_udp_protocol_connection

IP_ADDRESS = "127.0.0.1"
PORT = 1234


class UDPProtocolTest(unittest.TestCase):

    protocol: UDPProtocol = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.protocol:
            self.protocol.close()

    def test_create_udp_protocol_connection(self):
        self.loop.run_until_complete(create_udp_protocol_connection(IP_ADDRESS, PORT))

    def test_create_and_close(self):
        self.loop.run_until_complete(self.async_test_create_and_close())

    async def async_test_create_and_close(self):
        self.protocol = await create_udp_protocol_connection(IP_ADDRESS, PORT)

        self.protocol.close()
        await asyncio.sleep(0.0)
        self.assertFalse(self.protocol.connected)

    def test_create_and_connect(self):
        self.loop.run_until_complete(self.async_test_create_and_connect())

    async def async_test_create_and_connect(self):
        self.protocol = await create_udp_protocol_connection(IP_ADDRESS, PORT)
        self.assertTrue(self.protocol.connected)
        self.protocol.close()
        await asyncio.sleep(0.0)
        self.assertFalse(self.protocol.connected)


class UDPProtocolTest2(unittest.TestCase):
    data_received = b''
    protocol: UDPProtocol = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()
        self.protocol: UDPProtocol = self.loop.run_until_complete(self._create_udp())
        self.protocol.bytes_callbacks.add(self.bytes_callback)
        self.assertTrue(self.protocol.connected)
        self.data_received = b''

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server_socket.bind(("127.0.0.1", PORT))
        self.server_socket.settimeout(1)

    async def _create_udp(self) -> UDPProtocol:
        return await create_udp_protocol_connection(IP_ADDRESS, PORT)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def tearDown(self):
        self.data_received = b''
        self.protocol.close()
        self.server_socket.close()

    def test_send_comms(self):
        self.loop.run_until_complete(self.async_test_send_comms())

    async def async_test_send_comms(self):
        msg = b'Hello World'
        self.protocol.write(msg)
        data, address = self.server_socket.recvfrom(len(msg))
        self.assertEqual(msg, data)

    def test_send_receive_comms(self):
        self.loop.run_until_complete(self.async_test_send_receive_comms())

    async def async_test_send_receive_comms(self):
        msg = b'Hello World'
        self.protocol.write(msg)
        data, address = self.server_socket.recvfrom(len(msg))
        self.assertEqual(msg, data)
        msg2 = b'hELLO wORLD'
        self.server_socket.sendto(msg2, address)
        await asyncio.sleep(0.000000001)
        self.assertEqual(msg2, self.data_received)
