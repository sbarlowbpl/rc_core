import multiprocessing
import socket
import threading
import time
import unittest
import asyncio

from RC_Core.comms.protocols.tcp_protocol import TCPProtocol, create_tcp_protocol_connection

PORT = 1234

IP_ADDRESS = "127.0.0.1"



import logging
comms_logger = logging.getLogger("RC_Core.comms")
comms_logger.setLevel(logging.DEBUG)


class TCPProtocolTest(unittest.TestCase):

    protocol: TCPProtocol = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.protocol:
            self.protocol.close()

    def test_create_udp_protocol_connection_invalid_ip(self):
        with self.assertRaises(socket.gaierror):
            self.loop.run_until_complete(create_tcp_protocol_connection("INVALID_IP", PORT))

    def test_create_udp_protocol_no_server(self):
        with self.assertRaises(ConnectionRefusedError):
            self.loop.run_until_complete(create_tcp_protocol_connection(IP_ADDRESS, PORT))


class TCPProtocolTest2(unittest.TestCase):

    protocol: TCPProtocol = None

    _close_tcp_server_flag: bool = False

    def setUp(self) -> None:
        self._close_tcp_server_flag = False
        self.loop = asyncio.get_event_loop()

        self.tcp_thread = threading.Thread(target=self._create_tcp_server)
        self.tcp_thread.start()
        time.sleep(0.001)
        # self.tcp_process = threading.Thread(target=create_tcp_server, args=(IP_ADDRESS, PORT))
        # self.tcp_process.start()

    def tearDown(self) -> None:
        self._close_tcp_server_flag = True

        if self.protocol:
            self.protocol.close()
        # self.tcp_server.close()
        # self.tcp_process.terminate()
        self.tcp_thread.join(timeout=0.5)

    def _create_tcp_server(self):
        self.tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_server.bind((IP_ADDRESS, PORT))
        print("listening", time.perf_counter())
        self.tcp_server.listen()
        self.tcp_server.settimeout(2.0)
        print("Accepting", time.perf_counter())
        self.server_conn, addr = self.tcp_server.accept()
        print("Accepted", addr, time.perf_counter())
        while not self._close_tcp_server_flag:
            time.sleep(0.05)
        self.tcp_server.close()
        self.server_conn.close()

    def test_create_udp_protocol_with_available_server(self):
        self.loop.run_until_complete(self.async_test_create_udp_protocol_with_available_server())

    async def async_test_create_udp_protocol_with_available_server(self):
        self.protocol = await create_tcp_protocol_connection(IP_ADDRESS, PORT)
        self.assertTrue(self.protocol.connected)

    def test_create_and_close(self):
        self.loop.run_until_complete(self.async_test_create_and_close())

    async def async_test_create_and_close(self):
        self.protocol = await create_tcp_protocol_connection(IP_ADDRESS, PORT)
        self.assertTrue(self.protocol.connected)

        self.protocol.close()
        await asyncio.sleep(0.0)
        self.assertFalse(self.protocol.connected)

    def test_create_and_server_close(self):
        self.loop.run_until_complete(self.async_test_create_and_server_close())

    async def async_test_create_and_server_close(self):
        self.protocol = await create_tcp_protocol_connection(IP_ADDRESS, PORT)
        self.assertTrue(self.protocol.connected)

        self._close_tcp_server_flag = True
        await asyncio.sleep(0.1)
        self.assertFalse(self.protocol.connected)

    def test_create_and_write(self):
        self.loop.run_until_complete(self.async_test_create_and_write())

    async def async_test_create_and_write(self):
        self.protocol = await create_tcp_protocol_connection(IP_ADDRESS, PORT)
        self.assertTrue(self.protocol.connected)
        time.sleep(0.05)
        msg = b'Hello World'
        self.protocol.write(msg)
        data = self.server_conn.recv(len(msg))
        self.assertEqual(msg, data)
        await asyncio.sleep(0.0)
        self.assertTrue(self.protocol.connected)


class TCPProtocolTest3(unittest.TestCase):

    protocol: TCPProtocol = None

    _close_tcp_server_flag: bool = False

    received_data = b''

    server_conn = None

    tcp_server = None

    def setUp(self) -> None:
        self._close_tcp_server_flag = False
        self.loop = asyncio.get_event_loop()
        self.received_data = b''

        self.tcp_thread = threading.Thread(target=self._create_tcp_server)
        self.tcp_thread.start()
        time.sleep(0.1)

        self.loop.run_until_complete(self.create_protocol())

    async def create_protocol(self):
        self.protocol = await create_tcp_protocol_connection(IP_ADDRESS, PORT)

        self.assertTrue(self.protocol.connected)
        self.protocol.bytes_callbacks.add(self._bytes_callback)

    def _bytes_callback(self, data: bytes):
        self.received_data += data

    def tearDown(self) -> None:
        self._close_tcp_server_flag = True

        if self.protocol:
            self.protocol.close()
        # self.tcp_server.close()
        # self.tcp_process.terminate()
        self.tcp_thread.join(timeout=0.5)

    def _create_tcp_server(self):
        self.tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_server.bind((IP_ADDRESS, PORT))
        print("listening", time.perf_counter())
        self.tcp_server.listen()
        self.tcp_server.settimeout(2.0)
        print("Accepting", time.perf_counter())
        self.server_conn, addr = self.tcp_server.accept()
        print("Accepted", addr, time.perf_counter())
        while not self._close_tcp_server_flag:
            time.sleep(0.01)
        self.tcp_server.close()
        self.server_conn.close()

    def test_read(self):
        self.loop.run_until_complete(self.async_test_read())

    async def async_test_read(self):
        await asyncio.sleep(0.01)
        msg = b'Hello World'
        self.server_conn.send(msg)
        await asyncio.sleep(0.01)
        self.assertEqual(msg, self.received_data)
        await asyncio.sleep(0.01)
        self.assertTrue(self.protocol.connected)