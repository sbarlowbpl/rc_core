import time
import unittest
import asyncio

import serial
from serial import Serial

from RC_Core.comms.protocols import SerialProtocol, create_serial_protocol_connection

COM_PORT_A = "COM32"
COM_PORT_B = "COM33"


class ACom0ComTest(unittest.TestCase):
    """ As a part of testing, Com0Com is used http://com0com.sourceforge.net/ to connect a virtual com pair between
    COM_PORT_A and COM_PORT_B defined above. This test case tests if that has been configured correctly"""

    def test_com_port_a(self):
        serial_connection = Serial(COM_PORT_A)

        self.assertTrue(serial_connection.is_open)
        time.sleep(0.0000001)
        serial_connection.close()
        self.assertFalse(serial_connection.is_open)

    def test_com_port_b(self):
        serial_connection = Serial(COM_PORT_B)

        self.assertTrue(serial_connection.is_open)
        time.sleep(0.000001)
        serial_connection.close()
        self.assertFalse(serial_connection.is_open)

    def test_communication_a_to_b(self):
        serial_a = Serial(COM_PORT_A)
        serial_b = Serial(COM_PORT_B)

        msg = b'Hello World'
        serial_a.write(msg)
        recv_msg = serial_b.read(len(msg))

        self.assertEqual(msg, recv_msg)
        serial_a.close()
        serial_b.close()

    def test_communication_b_to_a(self):
        serial_a = Serial(COM_PORT_A)
        serial_b = Serial(COM_PORT_B)

        msg = b'Hello World'
        serial_b.write(msg)
        recv_msg = serial_a.read(len(msg))

        self.assertEqual(msg, recv_msg)
        serial_a.close()
        serial_b.close()


class SerialProtocolTest1(unittest.TestCase):
    protocol: SerialProtocol = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def tearDown(self):
        if self.protocol:
            self.protocol.close()

    def test_create_serial_protocol_connection(self):
        loop = asyncio.get_event_loop()
        self.protocol = loop.run_until_complete(create_serial_protocol_connection(COM_PORT_A, 115200, 8, 'N', 1))
        self.assertTrue(self.protocol.connected)

    def test_create_and_close_serial_protocol(self):
        self.loop.run_until_complete(self.async_test_create_and_close_serial_protocol())

    async def async_test_create_and_close_serial_protocol(self):
        self.protocol = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, 'N', 1)
        self.protocol.close()
        await asyncio.sleep(0.0)
        self.assertFalse(self.protocol.connected)

    def test_connect_invalid_serial_port(self):
        self.loop.run_until_complete(self.async_test_connect_invalid_serial_port())

    async def async_test_connect_invalid_serial_port(self):

        with self.assertRaises(serial.SerialException):
            await create_serial_protocol_connection("INVALID_COM_PORT", 115200, 8, 'N', 1)

    def test_connect_invalid_baudrate(self):
        self.loop.run_until_complete(self.async_test_connect_invalid_baudrate())

    async def async_test_connect_invalid_baudrate(self):
        with self.assertRaises(ValueError):
            await create_serial_protocol_connection(COM_PORT_A, -2, 8, 'N', 1)

    def test_connect_invalid_bytesize(self):
        self.loop.run_until_complete(self.async_test_connect_invalid_bytesize())

    async def async_test_connect_invalid_bytesize(self):
        with self.assertRaises(ValueError):
            await create_serial_protocol_connection(COM_PORT_A, 115200, 0, 'N', 1)

    def test_connect_invalid_parity(self):
        self.loop.run_until_complete(self.async_test_connect_invalid_parity())

    async def async_test_connect_invalid_parity(self):
        with self.assertRaises(ValueError):
            await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "2", 1)

    def test_connect_invalid_stopbits(self):
        self.loop.run_until_complete(self.async_test_connect_invalid_stopbits())

    async def async_test_connect_invalid_stopbits(self):
        with self.assertRaises(ValueError):
            await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 33)

    def test_connect_taken_port(self):

        serial_port = Serial(COM_PORT_A, 115200, 8, "N", 1)
        self.loop.run_until_complete(self.async_test_connect_taken_port())
        serial_port.close()

    async def async_test_connect_taken_port(self):
        with self.assertRaises(serial.SerialException):
            await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)

    def test_reconnect_taken_port(self):
        self.loop.run_until_complete(self.async_test_reconnect_taken_port())

    async def async_test_reconnect_taken_port(self):
        serial_port = Serial(COM_PORT_A, 115200, 8, "N", 1)
        with self.assertRaises(serial.SerialException):
            await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)

        serial_port.close()
        self.protocol = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)

    def test_maintain_taken_port(self):
        """ Test to ensure that if a SerialProtocol is connected another serial connection cannot take it"""
        self.loop.run_until_complete(self.async_test_maintain_taken_port())

    async def async_test_maintain_taken_port(self):
        self.protocol = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)
        with self.assertRaises(serial.SerialException):
            serial_port = Serial(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)

    def test_release_taken_port(self):
        """ Test to ensure that if a SerialProtocol is connected another serial connection cannot take it, and if
        it closes it then it is release and another device can take it."""
        self.loop.run_until_complete(self.async_test_release_taken_port())

    async def async_test_release_taken_port(self):
        self.protocol = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)
        with self.assertRaises(serial.SerialException):
            serial_port = Serial(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)
        self.protocol.close()
        await asyncio.sleep(0)
        self.assertFalse(self.protocol.connected)
        serial_port = Serial(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(serial_port.is_open)

    def test_cannot_set_connected_property(self):
        """Ensure that connected is a protected property and cannot be set"""
        self.loop.run_until_complete(self.async_test_cannot_set_connected_property())

    async def async_test_cannot_set_connected_property(self):
        self.protocol = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)
        with self.assertRaises(AttributeError):
            self.protocol.connected = False
        self.assertTrue(self.protocol.connected)

    def test_close_port_twice(self):
        self.loop.run_until_complete(self.async_test_cannot_set_connected_property())

    async def async_test_close_port_twice(self):
        self.protocol = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)
        self.assertTrue(self.protocol.connected)
        self.protocol.close()
        await asyncio.sleep(0)
        self.assertFalse(self.protocol.connected)
        await asyncio.sleep(0)
        self.protocol.close()

    def test_open_port_twice(self):
        self.loop.run_until_complete(self.async_test_cannot_set_connected_property())

    async def async_test_open_port_twice(self):
        protocol1 = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)

        with self.assertRaises(serial.SerialException):
            protocol2 = await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)
            protocol2.close()
        self.assertTrue(protocol1.connected)
        protocol1.close()


class SerialProtocolTest2(unittest.TestCase):
    """ Tests cases for send and received serial data"""

    data_received = b''
    protocol: SerialProtocol = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()
        self.protocol: SerialProtocol = self.loop.run_until_complete(self._create_serial_port())
        self.protocol.bytes_callbacks.add(self.bytes_callback)
        self.assertTrue(self.protocol.connected)

    @staticmethod
    async def _create_serial_port() -> SerialProtocol:
        return await create_serial_protocol_connection(COM_PORT_A, 115200, 8, "N", 1)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def tearDown(self):
        self.data_received = b''
        self.protocol.close()

    def test_receive_comms(self):
        self.loop.run_until_complete(self.async_test_receive_comms())

    async def async_test_receive_comms(self):
        serial_port = Serial(COM_PORT_B, 115200, 8, "N", 1)
        msg = b'Hello World'
        serial_port.write(msg)
        serial_port.flush()
        await asyncio.sleep(0.0000001)
        self.assertEqual(msg, self.data_received)

    def test_send_comms(self):
        self.loop.run_until_complete(self.async_test_send_comms())

    async def async_test_send_comms(self):
        serial_port = Serial(COM_PORT_B, 115200, 8, "N", 1)
        msg = b'Hello World'
        self.protocol.write(msg)
        await asyncio.sleep(0.0000001)
        data = serial_port.read(len(msg))
        self.assertEqual(msg, data)
