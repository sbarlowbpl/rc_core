import asyncio
import unittest
import multiprocessing
import sys


from RC_Core.comms.protocols import PipeProtocol, create_pipe_protocol_connection


class PipeProtocolTest1(unittest.TestCase):
    protocol = None


    @classmethod
    def setUpClass(cls) -> None:
        pass
        # asyncio.new_event_loop()
        # if sys.platform == 'win32':
        #     loop = asyncio.ProactorEventLoop()
        #     asyncio.set_event_loop(loop)

    @classmethod
    def tearDownClass(cls) -> None:
        pass

        # loop = asyncio.get_event_loop()
        # loop.run_until_complete(asyncio.sleep(0.01))
        # loop.run_until_complete(loop.shutdown_asyncgens())
        # loop.stop()
        # loop.close()
        # asyncio.set_event_loop(None)

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.protocol:
            self.protocol.close()
            del self.protocol

    def test_open_pipe_protocol(self):
        self.loop.run_until_complete(self.async_test_open_pipe_protocol())

    async def async_test_open_pipe_protocol(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.protocol: PipeProtocol = await create_pipe_protocol_connection(pipe_a)
        self.assertTrue(self.protocol.connected)

    def test_open_close_pipe_protocol(self):
        self.loop.run_until_complete(self.async_test_open_close_pipe_protocol())

    async def async_test_open_close_pipe_protocol(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.protocol: PipeProtocol = await create_pipe_protocol_connection(pipe_a)
        self.assertTrue(self.protocol.connected)
        self.protocol.close()
        self.assertFalse(self.protocol.connected)

    def test_cannot_set_connected_property(self):
        self.loop.run_until_complete(self.async_test_cannot_set_connected_property())

    async def async_test_cannot_set_connected_property(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.protocol = await create_pipe_protocol_connection(pipe_a)
        self.assertTrue(self.protocol.connected)
        with self.assertRaises(AttributeError):
            self.protocol.connected = False
        self.assertTrue(self.protocol.connected)

    def test_closed_pipe(self):
        self.loop.run_until_complete(self.async_test_closed_pipe())

    async def async_test_closed_pipe(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        pipe_a.close()
        with self.assertRaises(ConnectionRefusedError):
            self.protocol = await create_pipe_protocol_connection(pipe_a)
            self.assertFalse(self.protocol.connected)

    def test_closed_pipe_other_side(self):
        self.loop.run_until_complete(self.async_test_closed_pipe_other_side())

    async def async_test_closed_pipe_other_side(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        pipe_b.close()
        # with self.assertRaises(ConnectionRefusedError):
        self.protocol = await create_pipe_protocol_connection(pipe_a)
        # await asyncio.sleep(0.000000001)
        # self.assertFalse(self.protocol.connected)

class PipeProtocolTest2(unittest.TestCase):

    data_received = b''
    protocol: PipeProtocol = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.pipe_b = pipe_b
        self.protocol: PipeProtocol = self.loop.run_until_complete(self._create_pipe_port(pipe_a))
        self.protocol.bytes_callbacks.add(self.bytes_callback)
        self.assertTrue(self.protocol.connected)

    @staticmethod
    async def _create_pipe_port(pipe) -> PipeProtocol:
        return await create_pipe_protocol_connection(pipe)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def tearDown(self):
        self.data_received = b''
        self.protocol.close()


    def test_receive_comms(self):
        self.loop.run_until_complete(self.async_test_receive_comms())

    async def async_test_receive_comms(self):
        msg = b'Hello World'
        self.pipe_b.send_bytes(msg)
        await asyncio.sleep(0.000000000001)
        self.assertEqual(msg, self.data_received)

    def test_send_comms(self):
        self.loop.run_until_complete(self.async_test_send_comms())

    async def async_test_send_comms(self):
        msg = b'Hello World'
        self.protocol.write(msg)
        await asyncio.sleep(0.00000001)
        data = self.pipe_b.recv_bytes()
        self.assertEqual(msg, data)




