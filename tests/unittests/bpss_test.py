import unittest

from RC_Core.commconnection_methods import _encode_packet_bpss, _parsepacket_bpss


class BPSSPacketTestCase(unittest.TestCase):

    def test_encode_and_parse(self, initial_device_id, initial_packet_id, initial_data):

        result = _encode_packet_bpss(initial_device_id, initial_packet_id, initial_data)

        print(result)

        check_parse_bytes = result[:-2]

        check_parse_bytes[0] = b'$'[0]

        device_id, packet_id, data = _parsepacket_bpss(check_parse_bytes)

        self.assertAlmostEqual(initial_device_id, device_id)
        self.assertAlmostEqual(initial_packet_id, packet_id)

        self.assertAlmostEqual(initial_data, data[0])

    def test1(self):

        initial_device_id = 1
        initial_packet_id = 3
        initial_data = 0.3

        self.test_encode_and_parse(initial_device_id, initial_packet_id, initial_data)

    def test2(self):

        initial_device_id = 1
        initial_packet_id = 3
        initial_data = 0.5

        self.test_encode_and_parse(initial_device_id, initial_packet_id, initial_data)

    def test3(self):

        initial_device_id = 1
        initial_packet_id = 3
        initial_data = 0.5

        self.test_encode_and_parse(initial_device_id, initial_packet_id, initial_data)