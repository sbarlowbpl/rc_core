import sys
import unittest
import asyncio
import multiprocessing

from RC_Core.connections import ConnectionBase, PipeConnection
from RC_Core.protocols.bpl_protocol import BPLProtocol, encode_packet, parse_packet, packet_splitter


class BPLProtocolTest(unittest.TestCase):
    connection: ConnectionBase = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def test_simple_setup(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.connection = PipeConnection(pipe_a)
        self.loop.run_until_complete(self.connection.connect())

        bpl_protocol = BPLProtocol(self.connection)

    def test_packet_splitter_complete(self):
        data = b'hello\x00world\x00'
        result = [b'hello', b'world']
        split, incomplete = packet_splitter(data)

        self.assertEqual(split, result)
        self.assertEqual(incomplete, b'')

    def test_packet_splitter_incomplete(self):
        data = b'hello\x00world\x00hell'
        result = [b'hello', b'world']
        incomplete_result = b'hell'
        split, incomplete = packet_splitter(data)

        self.assertEqual(split, result)
        self.assertEqual(incomplete, b'hell')

    def test_packet_encode(self):
        device_id = 1
        packet_id = 1
        data = b'hello world'

        expected_result = b'\x10hello world\x01\x01\x0f\xa7\x00'

        encoded = encode_packet(device_id, packet_id, data)

        self.assertEqual(encoded, expected_result)

    def test_parse_packet(self):
        data = b'\x10hello world\x01\x01\x0f\xa7'

        device_id, packet_id, data = parse_packet(data)

        self.assertEqual(device_id, 1)
        self.assertEqual(packet_id, 1)
        self.assertEqual(data, b'hello world')


class BPLProtocolTest2(unittest.TestCase):
    connection: ConnectionBase = None

    last_received_packet = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

        pipe_a, self.pipe_b = multiprocessing.Pipe()
        self.connection = PipeConnection(pipe_a)
        self.loop.run_until_complete(self.connection.connect())

        self.bpl_protocol = BPLProtocol(self.connection)

        self.loop.run_until_complete(asyncio.sleep(0.1))
        self.last_received_packet = None

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def packet_callback(self, device_id, packet_id, data: bytes):
        self.last_received_packet = (device_id, packet_id, data)

    def test_receive_data(self):
        self.loop.run_until_complete(self.async_test_receive_data())

    async def async_test_receive_data(self):
        device_id = 1
        packet_id = 2
        data = b'Hello World'

        self.bpl_protocol.add_packet_callback(device_id, packet_id, self.packet_callback)

        msg = encode_packet(device_id, packet_id, data)
        self.pipe_b.send_bytes(msg)

        await asyncio.sleep(0.1)

        self.assertIsNotNone(self.last_received_packet)

        self.assertEqual(self.last_received_packet[0], device_id)
        self.assertEqual(self.last_received_packet[1], packet_id)
        self.assertEqual(self.last_received_packet[2], data)

    def test_receive_data_FF(self):
        self.loop.run_until_complete(self.async_test_receive_data_FF())

    async def async_test_receive_data_FF(self):
        device_id = 1
        packet_id = 2
        data = b'Hello World'

        self.bpl_protocol.add_packet_callback(0xFF, packet_id, self.packet_callback)

        msg = encode_packet(device_id, packet_id, data)
        self.pipe_b.send_bytes(msg)

        await asyncio.sleep(0.1)

        self.assertIsNotNone(self.last_received_packet)

        self.assertEqual(self.last_received_packet[0], device_id)
        self.assertEqual(self.last_received_packet[1], packet_id)
        self.assertEqual(self.last_received_packet[2], data)

    def test_send_data(self):
        self.loop.run_until_complete(self.async_test_receive_data())

    async def async_test_send_data(self):
        device_id = 1
        packet_id = 2
        data = b'Hello World'

        self.bpl_protocol.send_packet_bytes(device_id, packet_id, data)
        await asyncio.sleep(0.1)

        # msg = encode_packet(device_id, packet_id, data)
        data = self.pipe_b.recv_bytes()
        recv_dev, recv_packet_id, recv_data = parse_packet(data[:-1])

        self.assertEqual(recv_dev, device_id)
        self.assertEqual(recv_packet_id, packet_id)
        self.assertEqual(recv_data, data)





