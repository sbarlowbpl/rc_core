import multiprocessing
import struct
import sys
import unittest
import asyncio
from typing import Union, List

from RC_Core.connections import PipeConnection
from RC_Core.protocols.bpl_protocol import BPLPacketProtocol, encode_packet, parse_packet


class BPLPacketProtocolTest(unittest.TestCase):

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

        pipe_a, self.pipe_b = multiprocessing.Pipe()
        self.connection = PipeConnection(pipe_a)
        self.loop.run_until_complete(self.connection.connect())
        self.bpl_protocol = BPLPacketProtocol(self.connection)
        self.loop.run_until_complete(asyncio.sleep(0.1))
        self.last_received_packet = None

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def packet_callback(self, device_id, packet_id, data: Union[List[float], List[int]]):
        self.last_received_packet = (device_id, packet_id, data)

    def test_receive_data_ints(self):
        self.loop.run_until_complete(self.async_test_receive_data_ints())

    async def async_test_receive_data_ints(self):
        device_id = 1
        packet_id = 1
        data = [1,2,3,4]

        self.bpl_protocol.add_packet_callback(device_id, packet_id, self.packet_callback)

        msg = encode_packet(device_id, packet_id, bytearray(data))

        self.pipe_b.send_bytes(msg)

        await asyncio.sleep(0.1)

        self.assertIsNotNone(self.last_received_packet)

        self.assertEqual(self.last_received_packet[0], device_id)
        self.assertEqual(self.last_received_packet[1], packet_id)
        self.assertEqual(self.last_received_packet[2], data)

    def test_receive_data_floats(self):
        self.loop.run_until_complete(self.async_test_receive_data_floats())

    async def async_test_receive_data_floats(self):
        device_id = 1
        packet_id = 2
        data = [1.1, 2.2, 3.3, 4.4]

        packet_data = bytes(struct.pack('%sf' % len(data), *data))

        self.bpl_protocol.add_packet_callback(device_id, packet_id, self.packet_callback)

        msg = encode_packet(device_id, packet_id, packet_data)

        self.pipe_b.send_bytes(msg)

        await asyncio.sleep(0.1)

        self.assertIsNotNone(self.last_received_packet)

        self.assertEqual(self.last_received_packet[0], device_id)
        self.assertEqual(self.last_received_packet[1], packet_id)

        for idx, i in enumerate(self.last_received_packet[2]):
            self.assertAlmostEqual(i, data[idx], delta=0.1)

    def test_send_packet_ints(self):
        self.loop.run_until_complete(self.async_test_send_packet_ints())

    async def async_test_send_packet_ints(self):
        device_id = 1
        packet_id = 1
        send_data = [1,2,3,4]

        self.bpl_protocol.send_packet(device_id, packet_id, send_data)
        await asyncio.sleep(0.1)

        data = self.pipe_b.recv_bytes()
        recv_dev, recv_packet_id, recv_data = parse_packet(data[:-1])

        self.assertEqual(recv_dev, device_id)
        self.assertEqual(recv_packet_id, packet_id)
        self.assertEqual(recv_data, bytes(send_data))

    def test_send_packet_floats(self):
        self.loop.run_until_complete(self.async_test_send_packet_floats())

    async def async_test_send_packet_floats(self):
        device_id = 1
        packet_id = 2
        send_data = [1.1,2.2,3.3,4.4]

        self.bpl_protocol.send_packet(device_id, packet_id, send_data)
        await asyncio.sleep(0.1)

        data = self.pipe_b.recv_bytes()
        recv_dev, recv_packet_id, recv_data = parse_packet(data[:-1])

        self.assertEqual(recv_dev, device_id)
        self.assertEqual(recv_packet_id, packet_id)
        self.assertEqual(recv_data, bytes(struct.pack('%sf' % len(send_data), *send_data)))