import unittest
import asyncio

from RC_Core.connections.serial_ftdi_connection import SerialFTDIConnection

from unittest.mock import Mock

ftd2xx = Mock()

serial = Mock()
serial.tools.list_ports = Mock()

TEST_PORT = "COM99"


class SerialFTDITest(unittest.TestCase):
    connection: SerialFTDIConnection = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def test_create(self):
        self.loop.run_until_complete(self.async_test_create())


    async def async_test_create(self):
        self.connection = SerialFTDIConnection(TEST_PORT)

        self.assertFalse(self.connection.connected)

        await self.connection.connect()

        await asyncio.sleep(0.1)
        self.assertFalse(self.connection.connected)

    def test_mock_serial(self):
        # comport_list = serial.tools.list_ports.comports()



        comport_list = serial.tools.list_ports.comports()

        print(comport_list)


class SerialFTDITest2(unittest.TestCase):
    connection: SerialFTDIConnection = None

    def setUp(self) -> None:

        def get_comport_list():
            return ["COM1", "COM2", "COM3", "COM99"]

        serial.tools.list_ports.comports = get_comport_list

        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def test_create(self):
        pass
