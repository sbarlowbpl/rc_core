import socket
import unittest
import asyncio

from RC_Core.connections import UDPServerConnection

IP = "127.0.0.1"
PORT = 1234

import logging
logging.basicConfig(handlers = [])


class UDPServerConnectionTest(unittest.TestCase):

    connection: UDPServerConnection = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def test_create_connection(self):
        self.connection = UDPServerConnection(IP, PORT)
        self.assertFalse(self.connection.connected)

    def test_create_and_open(self):
        self.loop.run_until_complete(self.async_test_create_and_open())

    async def async_test_create_and_open(self):
        self.connection = UDPServerConnection(IP,PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        self.assertTrue(self.connection.connected)

    def test_create_and_open_and_close(self):
        self.loop.run_until_complete(self.async_test_create_and_open_and_close())

    async def async_test_create_and_open_and_close(self):
        self.connection = UDPServerConnection(IP, PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        self.assertFalse(self.connection.connected)

    def test_port_taken(self):
        self.loop.run_until_complete(self.async_test_port_taken())

    async def async_test_port_taken(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_socket.bind((IP, PORT))
        server_socket.settimeout(1)

        self.connection = UDPServerConnection(IP, PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(1)
        self.assertFalse(self.connection.connected)
        server_socket.close()
        self.connection.disconnect()

    def test_port_taken_reconnect_to_it(self):
        self.loop.run_until_complete(self.async_test_port_taken_reconnect_to_it())

    async def async_test_port_taken_reconnect_to_it(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_socket.bind((IP, PORT))
        server_socket.settimeout(1)

        self.connection = UDPServerConnection(IP, PORT)
        self.connection.reconnect_period = 0.05
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        self.assertFalse(self.connection.connected)
        await asyncio.sleep(self.connection.reconnect_period * 1.1)
        self.assertFalse(self.connection.connected)
        server_socket.close()
        await asyncio.sleep(self.connection.reconnect_period * 2)
        self.assertTrue(self.connection.connected)


class UDPServerConnectionTest2(unittest.TestCase):
    data_received = b''

    connection: UDPServerConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        self.connection = UDPServerConnection(IP,PORT)

        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.client_socket.bind((IP, 0))
        self.client_socket.settimeout(1)

        self.connection.add_bytes_callback(self.bytes_callback)
        self.loop.run_until_complete(self.connection.connect())
        self.loop.run_until_complete(asyncio.sleep(0))
        self.assertTrue(self.connection.connected)

    def tearDown(self) -> None:
        self.connection.disconnect()
        self.client_socket.close()

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def test_receive_comms(self):
        self.loop.run_until_complete(self.async_test_receive_comms())

    async def async_test_receive_comms(self):
        msg = b'Hello World'
        self.client_socket.sendto(msg, (IP, PORT))
        await asyncio.sleep(0.01)
        self.assertEqual(msg, self.data_received)

    def test_receive_send_comms(self):
        self.loop.run_until_complete(self.async_test_receive_send_comms())

    async def async_test_receive_send_comms(self):
        msg = b'Hello World'
        self.client_socket.sendto(msg, (IP, PORT))
        await asyncio.sleep(0.01)
        self.assertEqual(msg, self.data_received)
        msg2 = b'hELLO wORLD'
        self.connection.send_bytes(msg2)
        data, address = self.client_socket.recvfrom(len(msg))
        self.assertEqual(msg2, data)