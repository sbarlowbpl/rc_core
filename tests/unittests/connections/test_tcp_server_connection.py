import socket
import unittest
import asyncio
import threading

from RC_Core.connections.tcp_server_connection import TCPServerConnection


PORT = 1234

IP = "localhost"

import logging

logging.basicConfig(handlers=[])
logger = logging.getLogger("tcp_server_connection")
logger.setLevel(logging.DEBUG)


class TCPServerTest(unittest.TestCase):
    connection: TCPServerConnection = None
    tcp_client: socket.socket = None

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

        if self.tcp_client:
            self.tcp_client.close()

        self.loop.run_until_complete(asyncio.sleep(0.7))

    def test_create_tcp_server(self):
        tcp = TCPServerConnection(PORT)
        self.assertFalse(tcp.connected)

    def test_attempt_connect(self):
        self.loop.run_until_complete(self.async_test_attempt_connect())

    async def async_test_attempt_connect(self):
        self.connection = TCPServerConnection(PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0.2)
        self.assertFalse(self.connection.connected)

    def test_attempt_connect_success(self):
        self.loop.run_until_complete(self.async_test_attempt_connect_success())

    async def async_test_attempt_connect_success(self):
        self.connection = TCPServerConnection(PORT)
        self.assertFalse(self.connection.connected)

        asyncio.create_task(self.connection.connect())
        self.assertFalse(self.connection.connected)

        self.tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_client.settimeout(5)
        t = threading.Thread(target=self.tcp_client.connect, args=((IP, PORT),))
        t.start()
        await asyncio.sleep(0.7)
        self.assertTrue(self.connection.connected)







class TCPServerTest2(unittest.TestCase):
    connection: TCPServerConnection = None
    tcp_client: socket.socket = None

    data_received = b''

    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()
        self.data_received = b''

        self.loop.run_until_complete(self.create_server())

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

        if self.tcp_client:
            self.tcp_client.close()

    async def create_server(self):
        self.connection = TCPServerConnection(PORT)
        self.connection.add_bytes_callback(self.bytes_callback)
        self.assertFalse(self.connection.connected)

        asyncio.create_task(self.connection.connect())

        self.tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_client.settimeout(5)
        t = threading.Thread(target=self.tcp_client.connect, args=((IP, PORT),))
        t.start()
        await asyncio.sleep(0.55)
        self.assertTrue(self.connection.connected)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def test_receive(self):
        self.loop.run_until_complete(self.async_test_receive())

    async def async_test_receive(self):
        msg = b'Hello World'
        self.tcp_client.send(msg)
        await asyncio.sleep(0.05)

        self.assertEqual(self.data_received, msg)

    def test_send(self):
        self.loop.run_until_complete(self.async_test_send())

    async def async_test_send(self):
        msg = b'Hello World'
        self.connection.send_bytes(msg)
        await asyncio.sleep(0.05)

        data = self.tcp_client.recv(len(msg))

        self.assertEqual(data, msg)

