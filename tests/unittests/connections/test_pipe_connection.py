import multiprocessing
import unittest
import asyncio
import sys

from RC_Core.connections import PipeConnection
#
# if sys.platform == 'win32':
#     loop = asyncio.ProactorEventLoop()
#     asyncio.set_event_loop(loop)


class PipeConnectionTest1(unittest.TestCase):

    connection: PipeConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def test_create_pipe_connection(self):

        self.loop.run_until_complete(self.async_test_create_pipe_connection())

    async def async_test_create_pipe_connection(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.connection = PipeConnection(pipe_a)
        self.assertFalse(self.connection.connected)

    def test_connect_pipe_connection(self):
        self.loop.run_until_complete(self.async_test_connect_pipe_connection())

    async def async_test_connect_pipe_connection(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        self.connection = PipeConnection(pipe_a)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)

    def test_connect_closed_pipe(self):
        self.loop.run_until_complete(self.async_test_connect_closed_pipe())

    async def async_test_connect_closed_pipe(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        pipe_a.close()
        self.connection = PipeConnection(pipe_a)
        self.assertFalse(self.connection.connected)

        # with self.assertRaises(ConnectionRefusedError):
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)

    def test_connect_closed_pipe_other_side(self):
        self.loop.run_until_complete(self.async_test_connect_closed_pipe_other_side())

    async def async_test_connect_closed_pipe_other_side(self):
        pipe_a, pipe_b = multiprocessing.Pipe()
        pipe_b.close()
        self.connection = PipeConnection(pipe_a)
        self.assertFalse(self.connection.connected)

        # with self.assertRaises(ConnectionRefusedError):
        await self.connection.connect()
        await asyncio.sleep(0.000001)
        self.assertFalse(self.connection.connected)


class PipeConnectionTest2(unittest.TestCase):
    data_received = b''

    connection: PipeConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        pipe_a, pipe_b = multiprocessing.Pipe()
        self.pipe_b = pipe_b
        self.connection = PipeConnection(pipe_a)

        self.connection.add_bytes_callback(self.bytes_callback)

        self.loop.run_until_complete(self.connection.connect())
        self.loop.run_until_complete(asyncio.sleep(0))
        self.assertTrue(self.connection.connected)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def tearDown(self):
        self.data_received = b''
        self.connection.disconnect()
        self.loop.run_until_complete(asyncio.sleep(0.001))

    def test_receive_comms(self):
        self.loop.run_until_complete(self.async_test_receive_comms())

    async def async_test_receive_comms(self):
        msg = b'Hello World'
        self.pipe_b.send_bytes(msg)
        await asyncio.sleep(0.001)
        self.assertEqual(msg, self.data_received)
        self.pipe_b.close()

    def test_send_comms(self):
        self.loop.run_until_complete(self.async_test_send_comms())

    async def async_test_send_comms(self):
        msg = b'Hello World'
        self.connection.send_bytes(msg)
        await asyncio.sleep(0.01)
        data = self.pipe_b.recv_bytes()
        self.assertEqual(msg, data)