import time
import unittest
import asyncio
import socket
import multiprocessing
import threading

from RC_Core.connections.tcp_connection import TCPConnection

IP_ADDRESS = "127.0.0.1"
PORT = 1234

import logging

logging.basicConfig(handlers=[])
logger = logging.getLogger("tcp_connection")
logger.setLevel(logging.DEBUG)

comms_logger = logging.getLogger("RC_Core.connections")
comms_logger.setLevel(logging.DEBUG)

class TCPConnectionTest(unittest.TestCase):

    connection: TCPConnection = None

    tcp_server = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

        self.loop.run_until_complete(asyncio.sleep(0.2))

    def test_create_connection(self):
        self.connection = TCPConnection(IP_ADDRESS, PORT)
        self.assertFalse(self.connection.connected)

    def test_connect(self):
        self.loop.run_until_complete(self.async_test_connect())

    async def async_test_connect(self):
        self.connection = TCPConnection(IP_ADDRESS, PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0.1)
        self.assertFalse(self.connection.connected)


class TCPConnectionTest2(unittest.TestCase):
    connection: TCPConnection = None

    _close_tcp_server_flag: bool = False

    def setUp(self) -> None:
        self._close_tcp_server_flag = False
        self.loop = asyncio.get_event_loop()

        self.tcp_thread = threading.Thread(target=self._create_tcp_server)
        self.tcp_thread.start()

    def tearDown(self) -> None:
        self._close_tcp_server_flag = True

        if self.connection:
            self.connection.disconnect()
        self.tcp_server.close()
        self.tcp_thread.join(timeout=0.5)

    def _create_tcp_server(self):
        self.tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_server.bind((IP_ADDRESS, PORT))
        self.tcp_server.listen()
        self.tcp_server.settimeout(1.0)
        conn, addr = self.tcp_server.accept()
        while not self._close_tcp_server_flag:
            time.sleep(0.05)
        self.tcp_server.close()
        conn.close()

    def test_connect(self):
        self.loop.run_until_complete(self.async_test_connect())

    async def async_test_connect(self):
        self.connection = TCPConnection(IP_ADDRESS, PORT)
        await self.connection.connect()
        await asyncio.sleep(0.1)
        self.assertTrue(self.connection.connected)

    def test_connect_disconnect(self):
        self.loop.run_until_complete(self.async_test_connect_disconnect())

    async def async_test_connect_disconnect(self):
        self.connection = TCPConnection(IP_ADDRESS, PORT)
        await self.connection.connect()
        await asyncio.sleep(0.1)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        self.assertFalse(self.connection.connected)


class TCPConnectionTest3(unittest.TestCase):
    connection: TCPConnection = None

    _close_tcp_server_flag: bool = False

    tcp_server_data_received = b''

    tcp_conn: socket.socket = None

    def setUp(self) -> None:
        self._close_tcp_server_flag = False
        self.loop = asyncio.get_event_loop()

        self.tcp_thread = threading.Thread(target=self._create_tcp_server)
        self.tcp_thread.start()

        self.tcp_server_data_received = b''

    def tearDown(self) -> None:
        self._close_tcp_server_flag = True

        if self.connection:
            print("Disconnecting")
            self.connection.disconnect()
        # self.tcp_server.close()
        print("Joining")
        self.tcp_thread.join(timeout=5.5)
        print("Joined")

    def _create_tcp_server(self):
        self.tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_server.bind(("", PORT))
        self.tcp_server.listen()
        self.tcp_server.settimeout(1.0)
        self.tcp_conn, addr = self.tcp_server.accept()

        self.tcp_conn.settimeout(0.01)
        self.tcp_server.setblocking(False)
        while not self._close_tcp_server_flag:
            try:
                print("REad")
                data = self.tcp_conn.recv(1024)
                self.tcp_server_data_received += data
            except socket.timeout:
                pass

        self.tcp_server.close()
        self.tcp_conn.close()
        print("CLOSED SERVERS")

    def test_tcp_send(self):
        self.loop.run_until_complete(self.async_test_tcp_send())

    async def async_test_tcp_send(self):
        self.connection = TCPConnection(IP_ADDRESS, PORT)
        await self.connection.connect()
        self.assertTrue(self.connection.connected)
        msg = b'Hello World'
        self.connection.send_bytes(msg)
        await asyncio.sleep(0.0001)
        self.assertEqual(self.tcp_server_data_received, msg)


class TCPConnectionTest4(unittest.TestCase):
    connection: TCPConnection = None

    _close_tcp_server_flag: bool = False

    tcp_server_data_received = b''

    tcp_conn: socket.socket = None

    data_received = b''

    def setUp(self) -> None:
        self._close_tcp_server_flag = False
        self.loop = asyncio.get_event_loop()

        self.tcp_thread = threading.Thread(target=self._create_tcp_server)
        self.tcp_thread.start()

        self.loop.run_until_complete(self._setup_tcp_client())

        self.tcp_server_data_received = b''
        self.data_received = b''

    def tearDown(self) -> None:
        self._close_tcp_server_flag = True

        if self.connection:
            self.connection.disconnect()
        self.tcp_server.close()
        self.tcp_thread.join(timeout=0.5)

    def _create_tcp_server(self):
        self.tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_server.bind(("", PORT))
        self.tcp_server.listen()
        self.tcp_server.settimeout(1.0)
        self.tcp_conn, addr = self.tcp_server.accept()
        self.tcp_conn.settimeout(0.01)
        self.tcp_server.settimeout(0.05)
        while not self._close_tcp_server_flag:
            try:
                data = self.tcp_conn.recv(256)
                self.tcp_server_data_received += data
            except socket.timeout:
                pass

        self.tcp_server.close()
        self.tcp_conn.close()

    async def _setup_tcp_client(self):
        self.connection = TCPConnection(IP_ADDRESS, PORT)
        self.connection.add_bytes_callback(self.bytes_callback)
        await self.connection.connect()
        await asyncio.sleep(0.1)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def test_receive(self):
        self.loop.run_until_complete(self.async_test_receive())

    async def async_test_receive(self):
        msg = b'Hello World'
        self.tcp_conn.send(msg)
        await asyncio.sleep(0.01)
        self.assertEqual(msg, self.data_received)