import unittest
import serial
import asyncio

from RC_Core.connections import SerialConnection

COM_PORT_A = "COM32"
COM_PORT_B = "COM33"


class SerialConnectionTest(unittest.TestCase):
    connection: SerialConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def tearDown(self):
        if self.connection:
            self.connection.disconnect()
            self.loop.run_until_complete(asyncio.sleep(0.1))

    def test_serial_closed_first(self):
        self.loop.run_until_complete(self.async_test_serial_closed_first())

    async def async_test_serial_closed_first(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)

    def test_serial_open(self):
        self.loop.run_until_complete(self.async_test_serial_open())

    async def async_test_serial_open(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)

    def test_open_invalid_port(self):
        self.loop.run_until_complete(self.async_test_open_invalid_port())

    async def async_test_open_invalid_port(self):
        self.connection = SerialConnection("INVALID_PORT")
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)

    def test_open_invalid_port_twice(self):
        self.loop.run_until_complete(self.async_test_open_invalid_port_twice())

    async def async_test_open_invalid_port_twice(self):
        self.connection = SerialConnection("INVALID_PORT")
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)

    def test_open_valid_port_twice(self):
        self.loop.run_until_complete(self.async_test_open_valid_port_twice())

    async def async_test_open_valid_port_twice(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)

    def test_close_port(self):
        self.loop.run_until_complete(self.async_test_close_port())

    async def async_test_close_port(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)

    def test_close_and_reopen_port(self):
        self.loop.run_until_complete(self.async_test_close_and_reopen_port())

    async def async_test_close_and_reopen_port(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)

    def test_close_and_reopen_port_from_different_connection(self):
        self.loop.run_until_complete(self.async_test_close_and_reopen_port_from_different_connection())

    async def async_test_close_and_reopen_port_from_different_connection(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        await asyncio.sleep(0)

        connection_2 = SerialConnection(COM_PORT_A)
        self.assertFalse(connection_2.connected)
        await connection_2.connect()
        await asyncio.sleep(0)
        self.assertTrue(connection_2.connected)
        connection_2.disconnect()

    def test_close_and_reopen_port_from_different_connection_and_reconnect_fails(self):
        self.loop.run_until_complete(
            self.async_test_close_and_reopen_port_from_different_connection_and_reconnect_fails())

    async def async_test_close_and_reopen_port_from_different_connection_and_reconnect_fails(self):
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        await asyncio.sleep(0)
        connection_2 = SerialConnection(COM_PORT_A)
        self.assertFalse(connection_2.connected)
        await connection_2.connect()
        await asyncio.sleep(0)
        self.assertTrue(connection_2.connected)

        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)
        connection_2.disconnect()

    def test_fail_fail_taken_port(self):
        """ Obtain a port using serial, Trying to connect should fail. but it should try to reconnect,
        reconnecting should succeed"""
        self.loop.run_until_complete(self.async_test_fail_taken_port())

    async def async_test_fail_taken_port(self):
        serial_comm = serial.Serial(COM_PORT_A)
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)

    def test_fail_taken_port_reconnect_success(self):
        """ Obtain a port using serial, Trying to connect should fail. but it should try to reconnect,
        reconnecting should succeed"""
        self.loop.run_until_complete(self.async_test_fail_taken_port_reconnect_success())

    async def async_test_fail_taken_port_reconnect_success(self):
        serial_comm = serial.Serial(COM_PORT_A)
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)
        serial_comm.close()
        await asyncio.sleep(self.connection.reconnect_period*1.05)
        self.assertTrue(self.connection.connected)

    def test_reconnect_period_dont_connect_too_soon(self):
        """ Obtain a port using serial, Trying to connect should fail. but it should try to reconnect,
        reconnecting should succeed"""
        self.loop.run_until_complete(self.async_test_reconnect_period_dont_connect_too_soon())

    async def async_test_reconnect_period_dont_connect_too_soon(self):
        serial_comm = serial.Serial(COM_PORT_A)
        self.connection = SerialConnection(COM_PORT_A)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)
        serial_comm.close()
        await asyncio.sleep(self.connection.reconnect_period * 0.2)
        self.assertFalse(self.connection.connected)
        await asyncio.sleep(self.connection.reconnect_period * 0.9)
        self.assertTrue(self.connection.connected)

    def test_set_reconnect_period(self):
        """ Obtain a port using serial, Trying to connect should fail. but it should try to reconnect,
        reconnecting should succeed"""
        self.loop.run_until_complete(self.async_test_set_reconnect_period())

    async def async_test_set_reconnect_period(self):
        serial_comm = serial.Serial(COM_PORT_A)
        self.connection = SerialConnection(COM_PORT_A)
        reconnect_period = 0.05
        self.connection.reconnect_period = reconnect_period
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0)
        self.assertFalse(self.connection.connected)
        serial_comm.close()
        await asyncio.sleep(reconnect_period * 0.2)
        self.assertFalse(self.connection.connected)
        await asyncio.sleep(reconnect_period * 0.8)
        self.assertTrue(self.connection.connected)


class SerialConnectionTest2(unittest.TestCase):
    data_received = b''
    connection: SerialConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()
        self.connection = SerialConnection(COM_PORT_A)

        self.connection.add_bytes_callback(self.bytes_callback)

        self.loop.run_until_complete(self.connection.connect())
        self.loop.run_until_complete(asyncio.sleep(0))
        self.assertTrue(self.connection.connected)

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def tearDown(self):
        self.data_received = b''
        self.connection.disconnect()
        self.loop.run_until_complete(asyncio.sleep(0.001))

    def test_receive_comms(self):
        self.loop.run_until_complete(self.async_test_receive_comms())

    async def async_test_receive_comms(self):
        serial_port = serial.Serial(COM_PORT_B, 115200, 8, "N", 1)
        msg = b'Hello World'
        serial_port.write(msg)
        serial_port.flush()
        await asyncio.sleep(0.0000001)
        self.assertEqual(msg, self.data_received)
        serial_port.close()

    def test_send_comms(self):
        self.loop.run_until_complete(self.async_test_send_comms())

    async def async_test_send_comms(self):
        serial_port = serial.Serial(COM_PORT_B, 115200, 8, "N", 1)
        msg = b'Hello World'
        self.connection.send_bytes(msg)
        await asyncio.sleep(0.0000001)
        data = serial_port.read(len(msg))
        self.assertEqual(msg, data)

    def test_receive_comms_after_reconnection(self):
        self.loop.run_until_complete(self.async_test_receive_comms_after_reconnection())

    async def async_test_receive_comms_after_reconnection(self):
        self.connection.disconnect()
        await asyncio.sleep(0.0)
        self.connection.reconnect_period = 0.05
        self.assertFalse(self.connection.connected)
        serial_port_a = serial.Serial(COM_PORT_A)
        await self.connection.connect()
        await asyncio.sleep(self.connection.reconnect_period * 0.1)
        self.assertFalse(self.connection.connected)
        serial_port_a.close()
        await asyncio.sleep(self.connection.reconnect_period * 1.0)
        self.assertTrue(self.connection.connected)

        serial_port = serial.Serial(COM_PORT_B, 115200, 8, "N", 1)
        msg = b'Hello World'
        serial_port.write(msg)
        serial_port.flush()
        await asyncio.sleep(0.02)
        self.assertEqual(msg, self.data_received)
        serial_port.close()
