import socket
import sys
import unittest
import asyncio

from RC_Core.connections import UDPConnection

IP_ADDRESS = "127.0.0.1"
PORT = 1234

if sys.platform == 'win32':
    loop = asyncio.ProactorEventLoop()
    asyncio.set_event_loop(loop)


class UDPConnectionTest1(unittest.TestCase):
    connection: UDPConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        if self.connection:
            self.connection.disconnect()

    def test_create_connection(self):
        self.loop.run_until_complete(self.async_test_create_connection())

    async def async_test_create_connection(self):
        self.connection = UDPConnection(IP_ADDRESS, PORT)
        self.assertFalse(self.connection.connected)

    def test_create_and_open(self):
        self.loop.run_until_complete(self.async_test_create_and_open())

    async def async_test_create_and_open(self):
        self.connection = UDPConnection(IP_ADDRESS, PORT)
        self.assertFalse(self.connection.connected)

        await self.connection.connect()
        await asyncio.sleep(0.1)
        self.assertTrue(self.connection.connected)

    def test_create_and_open_and_close(self):
        self.loop.run_until_complete(self.async_test_create_and_open_and_close())

    async def async_test_create_and_open_and_close(self):
        self.connection = UDPConnection(IP_ADDRESS, PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0.01)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        self.assertFalse(self.connection.connected)

    def test_invalid_ip(self):
        self.loop.run_until_complete(self.async_test_invalid_ip())

    async def async_test_invalid_ip(self):
        self.connection = UDPConnection("INVALID_IP_ADDRESS", PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0.01)
        self.connection.send_bytes(b'Hello World')

    def test_disconnect_reconnect(self):
        self.loop.run_until_complete(self.async_test_disconnect_reconnect())

    async def async_test_disconnect_reconnect(self):
        self.connection = UDPConnection("127.0.0.5", PORT)
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0.01)
        self.assertTrue(self.connection.connected)
        self.connection.disconnect()
        self.assertFalse(self.connection.connected)
        await self.connection.connect()
        await asyncio.sleep(0.01)
        self.assertTrue(self.connection.connected)


class UDPConnectionTest2(unittest.TestCase):
    data_received = b''

    connection: UDPConnection = None

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        self.connection = UDPConnection(IP_ADDRESS, PORT)

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server_socket.bind((IP_ADDRESS, PORT))
        self.server_socket.settimeout(1)

        self.connection.add_bytes_callback(self.bytes_callback)
        self.loop.run_until_complete(self.connection.connect())
        self.loop.run_until_complete(asyncio.sleep(0.0001))
        self.assertTrue(self.connection.connected)

    def tearDown(self) -> None:
        self.data_received = b''
        self.connection.disconnect()
        self.loop.run_until_complete(asyncio.sleep(0.001))
        self.server_socket.close()

    def bytes_callback(self, data: bytes):
        self.data_received += data

    def test_send_comms(self):
        self.loop.run_until_complete(self.async_test_send_comms())

    async def async_test_send_comms(self):

        msg = b'Hello World'
        self.connection.send_bytes(msg)
        data, address = self.server_socket.recvfrom(len(msg))
        # await asyncio.sleep(0.01)
        self.assertEqual(msg, data)

    def test_send_receive_comms(self):
        self.loop.run_until_complete(self.async_test_send_receive_comms())

    async def async_test_send_receive_comms(self):

        msg = b'Hello World'
        print("Sending Bytes")
        # await asyncio.sleep(0.01)
        self.connection.send_bytes(msg)
        # await asyncio.sleep(0.01)
        data, address = self.server_socket.recvfrom(len(msg))
        # await asyncio.sleep(0.01)
        self.assertEqual(msg, data)

        msg2 = b'hELLO wORLD'
        self.server_socket.sendto(msg2, address)
        await asyncio.sleep(0.02)
        self.assertEqual(msg2, self.data_received)
