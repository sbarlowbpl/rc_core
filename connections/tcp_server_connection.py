import socket
import logging
import asyncio
import time
from typing import Set, Callable, Tuple, Optional

from colorlog import ColoredFormatter

from RC_Core.connections import ConnectionBase

DEFAULT_CONNECTION_LOOP_PERIOD = 0.005

import rc_logging
logger = rc_logging.getLogger(__name__)


class TCPServerConnection(ConnectionBase):

    _socket: Optional[socket.socket] = None
    _connected: bool = False
    _connection_loop_period: float = DEFAULT_CONNECTION_LOOP_PERIOD

    _reconnect: bool = False
    _reconnect_loop_task: Optional[asyncio.Task] = None

    reconnect_period: float = 1.0

    _reply_address: Optional[Tuple[str, int]] = None

    _listening_socket: Optional[socket.socket] = None

    def __init__(self, port: int):
        self._port: int = port
        self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for created/lost connections
        self._connected_callbacks: Set[Callable[[bool], None]] = set()

    @property
    def port(self) -> int:
        return self._port

    @property
    def connected(self) -> bool:
        return self._socket is not None

    def reconfigure(self, port: int):

        if port != self._port:
            self._port: int = port
            if self.connected:
                self.disconnect()
                asyncio.create_task(self.connect())

    async def connect(self):
        logger.info(f"Connecting TCP_Server to Port: {self.port}")

        loop = asyncio.get_running_loop()
        self._reconnect = True

        if self.connected:
            logger.info(f"TCP Server is already connected")
            return

        if self._reconnect_loop_task is not None and not self._reconnect_loop_task.done():
            logger.warning(f"Already trying to create TCP server at Port: {self.port}")
            return

        self._listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self._listening_socket.bind(("", self.port))
            logger.debug(f"TCP Server bound to: {self.port}")
            self._listening_socket.settimeout(0.1)
            self._listening_socket.listen()
            self._socket, self._reply_address = await loop.run_in_executor(None, self._listening_socket.accept)
            self._socket.setblocking(False)
            logger.info(f"TCP Server connected to: {self._reply_address}")
            self._listening_socket.close()
            self._listening_socket = None
            asyncio.create_task(self._connection_loop())

            # Toggle Connected
            for callback in self._connected_callbacks:
                callback(True)
            return
        except OSError:
            self._listening_socket.close()
            self._listening_socket = None
            logger.warning(f"connect(): TCP Server Connection Failed out: {self.port}")
            if self._reconnect_loop_task is None or self._reconnect_loop_task.done():
                self. _reconnect_loop_task = asyncio.create_task(self._reconnect_loop())
            pass

    async def _reconnect_loop(self):
        loop = asyncio.get_running_loop()
        # await asyncio.sleep(self.reconnect_period)
        while self._reconnect:
            start_time = time.perf_counter()
            self._listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                logger.info(f"TCP Server Attempting to connect")
                self._listening_socket.bind(("", self.port))
                self._listening_socket.settimeout(self.reconnect_period)
                self._listening_socket.listen()
                self._socket, self._reply_address = await loop.run_in_executor(None, self._listening_socket.accept)
                self._socket.setblocking(False)
                logger.info(f"TCP Server connected to: {self._reply_address}")
                self._listening_socket.close()
                self._listening_socket = None
                asyncio.create_task(self._connection_loop())

                # Toggle Connected
                for callback in self._connected_callbacks:
                    callback(True)
                return
            except OSError as e:
                if self._listening_socket is not None:
                    self._listening_socket.close()

                dt: float = time.perf_counter() - start_time
                logger.warning(f"connect(): TCP Server Connection Failed out: {self.port}: {e}")
                await asyncio.sleep(self._connection_loop_period - dt)

    async def _connection_loop(self):
        self._connected = True
        while self._connected:
            start_time = time.perf_counter()
            try:
                data, self._reply_address = self._socket.recvfrom(4096)
            except BlockingIOError as e:
                data = b''

            if data:
                logger.debug(f"Received data : {data} from TCP {self._reply_address}")
                # Run byte callbacks
                for callback in self._bytes_callbacks:
                    callback(data)

            # dt is the time it took to run the code
            dt: float = time.perf_counter() - start_time
            await asyncio.sleep(self._connection_loop_period - dt)

    def add_bytes_callback(self, callback: Callable[[bytes], None]):
        self._bytes_callbacks.add(callback)
        pass

    def remove_bytes_callback(self, callback):
        if callback in self._bytes_callbacks:
            self._bytes_callbacks.remove(callback)

    def add_connected_callback(self, callback: Callable[[bool], None]):
        self._connected_callbacks.add(callback)

    def remove_connected_callback(self, callback: Callable[[bool], None]):
        if callback in self._connected_callbacks:
            self._connected_callbacks.remove(callback)
        pass

    def disconnect(self):
        """ Stops the connection loop and closes to socket"""
        self._connected = False
        self._reconnect = False

        if self._socket:
            self._socket.close()

        if self._listening_socket:
            self._listening_socket.close()
        self._listening_socket = None
        self._socket = None
        self._reply_address = None

        # Toggle Connected
        for callback in self._connected_callbacks:
            callback(False)

    def send_bytes(self, data: bytes):
        if self.connected:
            self._socket.sendto(data, self._reply_address)