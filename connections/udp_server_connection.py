import socket
import logging
import asyncio
import time
from typing import Set, Callable, Tuple, Optional, Union

from colorlog import ColoredFormatter

from RC_Core import rc_logging
from RC_Core.comms.protocols import create_udp_protocol_connection
from RC_Core.comms.protocols.udp_server_protocol import UDPServerProtocol, create_udp_server_protocol_connection
from RC_Core.connections import ConnectionBase

DEFAULT_CONNECTION_LOOP_PERIOD = 0.005


logger = rc_logging.getLogger(__name__)



class UDPServerConnection(ConnectionBase):
    """ Create a UDP Connection.

        UDP Connection is a client that connects to a ip address and port.
        Currently uses a connection loop to run the connection.
        No support to change ports/addresses on the go as of now.

        Usage:
        udp_conn = UDPConnection(ip, port)

        udp_conn.connect()

        udp.add_bytes_callback()

        udp.send_bytes(b'HelloWorld')

        udp.disconnect()


        """
    _protocol: Union[UDPServerProtocol, None] = None

    _reconnect: bool = True
    reconnect_period: float = 1.0

    _socket_closed: bool = True

    def __init__(self, ip_address: str, port: int):
        self._ip_address: str = ip_address
        self._port: int = port
        self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for created/lost connections
        self._connected_callbacks: Set[Callable[[bool], None]] = set()

        self.connecting_lock = asyncio.Lock()

    @property
    def ip_address(self) -> str:
        return self._ip_address

    @property
    def port(self) -> int:
        return self._port

    @property
    def connected(self) -> bool:
        if self._protocol and self._protocol.connected:
            return True
        else:
            return False

    def reconfigure(self, ip_address: str, port: int):
        if (ip_address, port) != (self._ip_address, self._port):
            self._ip_address: str = ip_address
            self._port: int = port
            if self.connected:
                self.disconnect()
                asyncio.create_task(self.connect())

    async def connect(self):
        logger.info(f"Attempting to connect to {(self.ip_address, self.port)}")
        self._reconnect = True
        if self.connecting_lock.locked():
            logger.warning(f"Already Attempting to connect to {(self.ip_address, self.port)}")
            return -2

        async with self.connecting_lock:

            if self.connected:
                logger.warning(f"Already Connected to {(self.ip_address, self.port)}")
                return -3

            try:
                self._protocol = await create_udp_server_protocol_connection(self.ip_address, self.port)
                self._protocol.bytes_callbacks.update(self._bytes_callbacks)
                self._protocol.connection_lost_callbacks.add(self._lost_connection)

                # Toggle Connected
                for callback in self._connected_callbacks:
                    callback(True)

                return 1
            except socket.error as e:
                logger.warning(f"Could not connect to UDP {(self.ip_address, self.port)}, {e}")

                if self._reconnect:
                    logger.info(f"Creating Reconnection Loop")
                    asyncio.create_task(self._reconnect_loop())
                pass

    async def _reconnect_loop(self):
        if self.connecting_lock.locked():
            logging.error("self.connecting_lock is still locked")
            return
        async with self.connecting_lock:

            await asyncio.sleep(self.reconnect_period)
            while self._reconnect:
                logger.info(f"Attempting to connect to {(self.ip_address, self.port)}")

                try:
                    self._protocol = await create_udp_server_protocol_connection(self.ip_address, self.port)
                    self._protocol.bytes_callbacks.update(self._bytes_callbacks)
                    self._protocol.connection_lost_callbacks.add(self._lost_connection)

                    # Toggle Connected
                    for callback in self._connected_callbacks:
                        callback(True)

                    return 1
                except socket.error:
                    logger.warning(f"Could not connect to UDP {(self.ip_address, self.port)}")
                    await asyncio.sleep(self.reconnect_period)

    def add_bytes_callback(self, callback: Callable[[bytes], None]):
        self._bytes_callbacks.add(callback)
        if self._protocol:
            self._protocol.bytes_callbacks.add(callback)

    def remove_bytes_callback(self, callback):
        if callback in self._bytes_callbacks:
            self._bytes_callbacks.remove(callback)
        if self._protocol:
            if callback in self._protocol.bytes_callbacks:
                self._protocol.bytes_callbacks.remove(callback)

    def add_connected_callback(self, callback: Callable[[bool], None]):
        self._connected_callbacks.add(callback)

    def remove_connected_callback(self, callback: Callable[[bool], None]):
        if callback in self._connected_callbacks:
            self._connected_callbacks.remove(callback)
        pass

    def _lost_connection(self):
        logger.warning(f"UDP {self.ip_address, self.port} Lost Connection")
        self.protocol = None

        if self._reconnect:
            asyncio.create_task(self.connect())

        for callback in self._connected_callbacks:
            callback(False)

    def disconnect(self):
        """ Stops the connection loop and closes to socket"""
        logger.info(f"Disconnect UDP from {self.ip_address, self.port}")

        self._reconnect = False
        if self._protocol:
            self._protocol.close()

        for callback in self._connected_callbacks:
            callback(False)


    def send_bytes(self, data: bytes):
        if self.connected:
            try:
                # logger.debug(f"Sending {data}")
                self._protocol.write(data)
            except OSError as e:
                logger.warning(f"UDP Connection {self.ip_address, self.port} Unable to send: {e}")

# class UDPServerConnection(ConnectionBase):
#
#     _socket: socket.socket = None
#
#     _connection_loop_period: float = DEFAULT_CONNECTION_LOOP_PERIOD
#
#     reconnect_period: float = 1.0
#
#     _reconnect: bool = False
#
#     _socket_closed: bool = True
#
#     _reply_address: Tuple[str, int] = None
#
#     _reconnect_loop_task: Optional[asyncio.Task] = None
#
#     def __init__(self, port: int):
#         self._port: int = port
#         self._bytes_callbacks: Set[Callable[[bytes], None]] = set()
#
#         # Callbacks for created/lost connections
#         self._connected_callbacks: Set[Callable[[bool], None]] = set()
#
#     @property
#     def port(self):
#         return self._port
#
#     @property
#     def connected(self):
#         return not self._socket_closed
#
#     def reconfigure(self, port: int):
#
#         if port != self._port:
#             self._port: int = port
#             if self.connected:
#                 self.disconnect()
#                 asyncio.create_task(self.connect())
#
#     async def connect(self):
#         logger.info(f"Connecting UDP Server to Port: {self.port}")
#
#         self._reconnect = True
#         if self.connected:
#             logger.info(f"UDP Server Connection already created.")
#             return
#
#         if self._reconnect_loop_task is not None and not self._reconnect_loop_task.done():
#             logger.warning(f"Already trying to create server at to Port: {self.port}")
#             return
#
#         self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#         self._socket.setblocking(False)
#         try:
#             self._socket.bind(("", self.port))
#             self._socket_closed = False
#             asyncio.create_task(self._connection_loop())
#
#             # Toggle Connected
#             for callback in self._connected_callbacks:
#                 callback(True)
#             return
#
#         except OSError as e:
#             self._socket.close()
#             logger.warning(f'Unable to create UDP_Server at port {self.port}: {e}')
#             if self._reconnect:
#                 if self._reconnect_loop_task is None or not self._reconnect_loop_task.done():
#                     logger.info("Starting reconnect_loop")
#                     self._reconnect_loop_task = asyncio.create_task(self._reconnect_loop())
#             return
#
#     async def _reconnect_loop(self):
#         await asyncio.sleep(self.reconnect_period)
#         while self._reconnect:
#             self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#             self._socket.setblocking(False)
#             try:
#                 self._socket.bind(("", self.port))
#                 self._socket_closed = False
#                 asyncio.create_task(self._connection_loop())
#
#                 # Toggle Connected
#                 for callback in self._connected_callbacks:
#                     callback(True)
#                 return
#             except OSError as e:
#                 self._socket.close()
#                 logger.warning(f'Unable to create UDP_Server at port {self.port}: {e}')
#                 await asyncio.sleep(self.reconnect_period)
#
#     async def _connection_loop(self):
#         while self.connected:
#             start_time = time.perf_counter()
#             try:
#                 data, self._reply_address = self._socket.recvfrom(4096)
#             except BlockingIOError:
#                 data = b''
#             if data:
#                 # Run byte callbacks
#                 logger.debug(f"UDP Server Connection {self.port} received {data}.")
#                 for callback in self._bytes_callbacks:
#                     callback(data)
#
#             # dt is the time it took to run the code
#             dt: float = time.perf_counter() - start_time
#             await asyncio.sleep(self._connection_loop_period - dt)
#
#     def add_bytes_callback(self, callback: Callable[[bytes], None]):
#         self._bytes_callbacks.add(callback)
#         pass
#
#     def remove_bytes_callback(self, callback):
#         if callback in self._bytes_callbacks:
#             self._bytes_callbacks.remove(callback)
#
#     def add_connected_callback(self, callback: Callable[[bool], None]):
#         self._connected_callbacks.add(callback)
#
#     def remove_connected_callback(self, callback: Callable[[bool], None]):
#         if callback in self._connected_callbacks:
#             self._connected_callbacks.remove(callback)
#         pass
#
#     def disconnect(self):
#         logger.info(f"Disconnect UDP to Port: {self.port}")
#         self._reconnect = False
#         if self.connected:
#             self._socket.close()
#             self._socket_closed = True
#         else:
#             logger.info(f"UDP Server already Disconnected")
#
#             # Toggle Connected
#         for callback in self._connected_callbacks:
#             callback(False)
#
#     def send_bytes(self, data: bytes):
#         if self.connected and self._reply_address:
#             self._socket.sendto(data, self._reply_address)
