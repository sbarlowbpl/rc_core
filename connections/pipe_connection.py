import asyncio
from typing import Union, Set, Callable
import logging
from multiprocessing.connection import Connection as MultiProcessingConnection

from colorlog import ColoredFormatter

from RC_Core import rc_logging
from RC_Core.connections import ConnectionBase
from RC_Core.comms.protocols import PipeProtocol, create_pipe_protocol_connection

logger = rc_logging.getLogger(__name__)


class PipeConnection(ConnectionBase):
    protocol: Union[PipeProtocol, None] = None
    _reconnect: bool = False
    reconnect_period: float = 1.0  # seconds
    pipe: MultiProcessingConnection = None

    def __init__(self, pipe: MultiProcessingConnection):
        logger.debug(f"__init__(): Creating Pipe Connection for {pipe._handle}")
        self.pipe = pipe
        # Callback for any bytes received
        self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for connections that are made/lost
        self._connected_callbacks: Set[Callable[[bool], None]] = set()

        self.connecting_lock = asyncio.Lock()

    def reconfigure(self, pipe: MultiProcessingConnection):
        self.pipe = pipe
        if self.connected:
            self.disconnect()
            asyncio.create_task(self.connect())

    @property
    def connected(self) -> bool:
        if self.protocol and self.protocol.connected:
            return True
        else:
            return False

    async def connect(self):
        logger.debug(f"connect(): Attempting to Connect to Pipe for: {self.pipe._handle}")

        self._reconnect = True
        if self.connecting_lock.locked():
            logger.warning(f"Already Attempting to connect to {(self.pipe._handle)}")
            return -2

        async with self.connecting_lock:
            try:
                self.protocol = await create_pipe_protocol_connection(self.pipe)
                self.protocol.bytes_callbacks.update(self._bytes_callbacks)
                self.protocol.connection_lost_callbacks.add(self._lost_connection)

                # Toggle Connected
                for callback in self._connected_callbacks:
                    callback(True)

                return

            except (TypeError, ConnectionError) as e:
                logger.warning(f"Could not connect to Pipe {e}, Pipe Handle: {self.pipe._handle}")
                # await asyncio.sleep(self.reconnect_period)

            while self._reconnect:
                logger.info(f"Attempting to Connect Pipe Handle: {self.pipe._handle}")
                try:
                    self.protocol = await create_pipe_protocol_connection(self.pipe)
                    self.protocol.bytes_callbacks.update(self._bytes_callbacks)
                    self.protocol.connection_lost_callbacks.add(self._lost_connection)

                    for callback in self._connected_callbacks:
                        callback(True)
                    return

                except (TypeError, ConnectionError) as e:
                    logger.warning(f"Could not connect to Pipe {e} Handle: {self.pipe._handle}")
                    await asyncio.sleep(self.reconnect_period)
            pass

    def add_bytes_callback(self, callback: Callable[[bytes], None]):
        self._bytes_callbacks.add(callback)
        if self.protocol:
            self.protocol.bytes_callbacks.add(callback)
        pass

    def remove_bytes_callback(self, callback: Callable[[bytes], None]):
        self._bytes_callbacks.remove(callback)
        if self.protocol and callback in self.protocol.bytes_callbacks:
            self.protocol.bytes_callbacks.remove(callback)
        pass

    def add_connected_callback(self, callback: Callable[[bool], None]):
        self._connected_callbacks.add(callback)

    def remove_connected_callback(self, callback: Callable[[bool], None]):
        if callback in self._connected_callbacks:
            self._connected_callbacks.remove(callback)
        pass

    def _lost_connection(self):
        logger.warning(f"Pipe Lost Connection Handle: {self.pipe._handle}")
        self.protocol = None

        for callback in self._connected_callbacks:
            callback(False)

        if self._reconnect:
            asyncio.create_task(self.connect())

    def disconnect(self):
        logger.debug(f"disconnect(): Disconnecting Pipe Handle: {self.pipe._handle}")
        self._reconnect = False
        if self.protocol:
            self.protocol.close()

        for callback in self._connected_callbacks:
            callback(False)

    def send_bytes(self, data) -> int:
        if self.connected:
            # return self.protocol.write(data)
            self.protocol.write(data)
        else:
            return -1
