import socket
import logging
import asyncio
import time
from typing import Set, Callable, Tuple, Optional, Union

from colorlog import ColoredFormatter

from RC_Core.comms.protocols.tcp_protocol import TCPProtocol, create_tcp_protocol_connection
from RC_Core.connections import ConnectionBase

DEFAULT_CONNECTION_LOOP_PERIOD = 0.005


import rc_logging
logger = rc_logging.getLogger(__name__)


class TCPConnection(ConnectionBase):
    """ TCP Connection.
           Currently uses a connection loop to run the connection.
           No support to change ports/addresses on the go as of now.

           Usage:
           tcp_conn = TCPConnection(ip, port)

           tcp_conn.connect()

           tcp_conn.add_bytes_callback()

           tcp_conn.send_bytes(b'HelloWorld')

           tcp_conn.disconnect()


           """
    _protocol: Union[TCPProtocol, None] = None

    _reconnect: bool = True
    reconnect_period: float = 1.0

    _socket_closed: bool = True

    def __init__(self, ip_address: str, port: int):
        self._ip_address: str = ip_address
        self._port: int = port
        self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for created/lost connections
        self._connected_callbacks: Set[Callable[[bool], None]] = set()

        self.connecting_lock = asyncio.Lock()


    @property
    def ip_address(self) -> str:
        return self._ip_address

    @property
    def port(self) -> int:
        return self._port

    @property
    def connected(self) -> bool:
        if self._protocol and self._protocol.connected:
            return True
        else:
            return False

    def reconfigure(self, ip_address: str, port: int):
        logger.debug(f"reconfigure({ip_address, port})")
        if (ip_address, port) != (self._ip_address, self._port):
            self._ip_address: str = ip_address
            self._port: int = port
            if self.connected:
                self.disconnect()
                asyncio.create_task(self.connect())

    async def connect(self):
        logger.debug("connect()")
        logger.info(f"Attempting to connect to {(self.ip_address, self.port)}")
        self._reconnect = True
        if self.connecting_lock.locked():
            logger.warning(f"Already Attempting to connect to {(self.ip_address, self.port)}")
            return -2

        async with self.connecting_lock:

            if self.connected:
                logger.warning(f"Already Connected to {(self.ip_address, self.port)}")
                return -3

            try:
                self._protocol = await create_tcp_protocol_connection(self.ip_address, self.port)
                self._protocol.bytes_callbacks.update(self._bytes_callbacks)
                self._protocol.connection_lost_callbacks.add(self._lost_connection)

                # Toggle Connected
                for callback in self._connected_callbacks:
                    callback(True)

                return 1
            except socket.error as e:
                logger.warning(f"Could not connect to TCP {(self.ip_address, self.port)}, {e}")

                if self._reconnect:

                    logger.info(f"Creating Reconnection Loop")
                    asyncio.create_task(self._reconnect_loop())
                return

    async def _reconnect_loop(self):
        if self.connecting_lock.locked():
            logging.error("self.connecting_lock is still locked")
            return
        async with self.connecting_lock:
            await asyncio.sleep(self.reconnect_period)
            while self._reconnect:
                logger.info(f"Attempting to connect to {(self.ip_address, self.port)}")

                try:
                    self._protocol = await create_tcp_protocol_connection(self.ip_address, self.port)
                    self._protocol.bytes_callbacks.update(self._bytes_callbacks)
                    self._protocol.connection_lost_callbacks.add(self._lost_connection)

                    # Toggle Connected
                    for callback in self._connected_callbacks:
                        callback(True)

                    return 1
                except socket.error:
                    logger.warning(f"Could not connect to TCP {(self.ip_address, self.port)}")
                    await asyncio.sleep(self.reconnect_period)

    def add_bytes_callback(self, callback: Callable[[bytes], None]):
        logger.debug("add_bytes_callback()")
        self._bytes_callbacks.add(callback)
        if self._protocol:
            self._protocol.bytes_callbacks.add(callback)

    def remove_bytes_callback(self, callback):
        logger.debug("remove_bytes_callback()")

        if callback in self._bytes_callbacks:
            self._bytes_callbacks.remove(callback)
        if self._protocol:
            if callback in self._protocol.bytes_callbacks:
                self._protocol.bytes_callbacks.remove(callback)

    def add_connected_callback(self, callback: Callable[[bool], None]):
        self._connected_callbacks.add(callback)

    def remove_connected_callback(self, callback: Callable[[bool], None]):
        if callback in self._connected_callbacks:
            self._connected_callbacks.remove(callback)
        pass

    def _lost_connection(self):
        logger.warning(f"UDP {self.ip_address, self.port} Lost Connection")
        self.protocol = None

        if self._reconnect:
            asyncio.create_task(self.connect())

        for callback in self._connected_callbacks:
            callback(False)

    def disconnect(self):
        logger.debug("disconnect()")

        """ Stops the connection loop and closes to socket"""
        logger.info(f"Disconnect TCP from {self.ip_address, self.port}")

        self._reconnect = False
        if self._protocol:
            self._protocol.close()

        for callback in self._connected_callbacks:
            callback(False)

    def send_bytes(self, data: bytes):
        if self.connected:
            try:
                self._protocol.write(data)
            except OSError as e:
                logger.warning(f"TCP Connection {self.ip_address, self.port} Unable to send: {e}")

