import asyncio
import platform
import time
from typing import Union, Set, Callable, Optional
import logging

import serial
import serial.tools.list_ports
from colorlog import ColoredFormatter

try:
    from ftd2xx import defines as ftdidef
except OSError:
    class ftdidef:
        OK = 0
        INVALID_HANDLE = 1
        DEVICE_NOT_FOUND = 2
        DEVICE_NOT_OPENED = 3
        IO_ERROR = 4
        INSUFFICIENT_RESOURCES = 5
        INVALID_PARAMETER = 6
        INVALID_BAUD_RATE = 7
        DEVICE_NOT_OPENED_FOR_ERASE = 8
        DEVICE_NOT_OPENED_FOR_WRITE = 9
        FAILED_TO_WRITE_DEVICE = 10
        EEPROM_READ_FAILED = 11
        EEPROM_WRITE_FAILED = 12
        EEPROM_ERASE_FAILED = 13
        EEPROM_NOT_PRESENT = 14
        EEPROM_NOT_PROGRAMMED = 15
        INVALID_ARGS = 16
        NOT_SUPPORTED = 17
        OTHER_ERROR = 18

        # List Devices flags
        LIST_NUMBER_ONLY = 0x80000000
        LIST_BY_INDEX = 0x40000000
        LIST_ALL = 0x20000000

        # OpenEx flags
        OPEN_BY_SERIAL_NUMBER = 1
        OPEN_BY_DESCRIPTION = 2
        OPEN_BY_LOCATION = 4

        # Device Identifiers
        DEVICE_232BM = 0
        DEVICE_232AM = 1
        DEVICE_100AX = 2
        DEVICE_UNKNOWN = 3
        DEVICE_2232C = 4
        DEVICE_232R = 5
        DEVICE_2232H = 6
        DEVICE_4232H = 7
        DEVICE_232H = 8
        DEVICE_X_SERIES = 9

        # Driver Types
        DRIVER_TYPE_D2XX = 0
        DRIVER_TYPE_VCP = 1

        # Word Lengths
        BITS_8 = 8
        BITS_7 = 7

        # Stop Bits
        STOP_BITS_1 = 0
        STOP_BITS_2 = 2

        # Parity
        PARITY_NONE = 0
        PARITY_ODD = 1
        PARITY_EVEN = 2
        PARITY_MARK = 3
        PARITY_SPACE = 4

        # Flow Control
        FLOW_NONE = 0x0000
        FLOW_RTS_CTS = 0x0100
        FLOW_DTR_DSR = 0x0200
        FLOW_XON_XOFF = 0x0400

        # Purge RX and TX Buffers
        PURGE_RX = 1
        PURGE_TX = 2

        # Notification Events
        EVENT_RXCHAR = 1
        EVENT_MODEM_STATUS = 2
        EVENT_LINE_STATUS = 4

        MAX_DESCRIPTION_SIZE = 256



from RC_Core.connections import ConnectionBase

import rc_logging
logger = rc_logging.getLogger(__name__)

if platform.system() == "Windows":

    import ftd2xx as ftdi
    from ftd2xx import defines as ftdidef

    FTDI_TIMEOUT = 2  # ms (ints)


    class SerialFTDIConnection(ConnectionBase):
        _ftdi_serial: ftdi.FTD2XX = None

        _reconnect: bool = True

        reconnect_period: float = 1.0

        _connection_loop_period = 0.0001

        def __init__(self, port: str,
                     baudrate: int = 115200,
                     bytesize: int = ftdidef.BITS_8,
                     parity: str = ftdidef.PARITY_EVEN,
                     stopbits: int = ftdidef.STOP_BITS_1):
            logger.debug(f"__init__(): Creating SerialFTDIConnection({port, baudrate, bytesize, parity, stopbits})")
            self.port = port
            self.baudrate = baudrate

            if stopbits == serial.STOPBITS_ONE:
                stopbits = ftdidef.STOP_BITS_1
            elif stopbits == serial.STOPBITS_TWO:
                stopbits = ftdidef.STOP_BITS_2
            if parity == serial.PARITY_NONE:
                parity = ftdidef.PARITY_NONE
            elif parity == serial.PARITY_EVEN:
                parity = ftdidef.PARITY_EVEN
            elif parity == serial.PARITY_ODD:
                parity = ftdidef.PARITY_ODD
            elif parity == serial.PARITY_MARK:
                parity = ftdidef.PARITY_MARK
            elif parity == serial.PARITY_SPACE:
                parity = ftdidef.PARITY_SPACE

            if bytesize == serial.EIGHTBITS:
                bytesize = ftdidef.BITS_8
            elif bytesize == serial.SEVENBITS:
                bytesize = ftdidef.BITS_7



            self.bytesize = bytesize
            self.parity = parity
            self.stopbits = stopbits

            # Callback for any bytes received
            self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

            # Callbacks for created/lost connections
            self._connected_callbacks: Set[Callable[[bool], None]] = set()

            self.connecting_lock = asyncio.Lock()

        def reconfigure(self, port: str,
                        baudrate: int = 115200,
                        bytesize: int = ftdidef.BITS_8,
                        parity: str = ftdidef.PARITY_EVEN,
                        stopbits: int = ftdidef.STOP_BITS_1):

            if ((port, baudrate, bytesize, parity, stopbits)
                    != (self.port, self.baudrate, self.bytesize, self.parity, self.stopbits)):

                self.port = port
                self.baudrate = baudrate

                if stopbits == serial.STOPBITS_ONE:
                    stopbits = ftdidef.STOP_BITS_1
                elif stopbits == serial.STOPBITS_TWO:
                    stopbits = ftdidef.STOP_BITS_2
                if parity == serial.PARITY_NONE:
                    parity = ftdidef.PARITY_NONE
                elif parity == serial.PARITY_EVEN:
                    parity = ftdidef.PARITY_EVEN
                elif parity == serial.PARITY_ODD:
                    parity = ftdidef.PARITY_ODD
                elif parity == serial.PARITY_MARK:
                    parity = ftdidef.PARITY_MARK
                elif parity == serial.PARITY_SPACE:
                    parity = ftdidef.PARITY_SPACE

                if bytesize == serial.EIGHTBITS:
                    bytesize = ftdidef.BITS_8
                elif bytesize == serial.SEVENBITS:
                    bytesize = ftdidef.BITS_7

                self.bytesize = bytesize
                self.parity = parity
                self.stopbits = stopbits
                if self.connected:
                    self.disconnect()
                    asyncio.create_task(self.connect())

        @property
        def connected(self) -> bool:
            if self._ftdi_serial is None:
                return False
            else:
                try:
                    self._ftdi_serial.getStatus()
                    return True
                except ftdi.DeviceError:
                    return False

        def connect_no_async(self):
            try:
                index = self._parse_ftdi_ports(self.port)
            except ValueError:
                logger.warning(f"Could not open serial device {self.port}")
                return -1
            try:
                self._ftdi_serial = ftdi.open(index)
                self._ftdi_serial.setBaudRate(self.baudrate)
                self._ftdi_serial.setDataCharacteristics(self.bytesize, self.stopbits, self.parity)
                self._ftdi_serial.setTimeouts(1, 1)
                self._ftdi_serial.setLatencyTimer(2)

                # Toggle Connected
                # for callback in self._connected_callbacks:
                #     callback(True)
                # asyncio.create_task(self._connection_loop())
                return 0
            except ftdi.DeviceError as e:
                if self._ftdi_serial:
                    self._ftdi_serial.close()
                return -1

        async def connect(self):

            self._reconnect = True
            if self.connecting_lock.locked():
                logger.warning(f"connect() Already Attempting to connect to {self.port}")
                return -2

            async with self.connecting_lock:
                if self.connected:
                    logger.warning("connect() FTDI Connection already connected")
                    return

                try:
                    index = self._parse_ftdi_ports(self.port)
                except ValueError:
                    logger.warning(f"connect() Could not open serial device {self.port}")
                else:
                    try:
                        await asyncio.sleep(0)
                        logger.debug(f"connect(): OPENED FTDI CONNECTION")
                        self._ftdi_serial = ftdi.open(index)
                        self._ftdi_serial.setBaudRate(self.baudrate)
                        self._ftdi_serial.setDataCharacteristics(self.bytesize, self.stopbits, self.parity)
                        self._ftdi_serial.setTimeouts(FTDI_TIMEOUT, FTDI_TIMEOUT)
                        self._ftdi_serial.setLatencyTimer(2)

                        # Toggle Connected
                        for callback in self._connected_callbacks:
                            callback(True)
                        asyncio.create_task(self._connection_loop())
                        return 0
                    except ftdi.DeviceError as e:
                        if self._ftdi_serial:
                            self._ftdi_serial.close()
                        logger.warning(f"connect() Could not open serial_ftdi device {self.port}, {e}")

                # Unsuccessful, initiate the reconnect loop

                    asyncio.create_task(self._reconnect_loop())
                pass

        async def _reconnect_loop(self):
            if self.connecting_lock.locked():
                logging.error("reconnect() self.connecting_lock is still locked")
                return
            with self.connecting_lock:
                await asyncio.sleep(self.reconnect_period)

                while self._reconnect:

                    start_time = time.perf_counter()
                    logger.warning("reconnect() Attempting to open FTDI Connection")
                    try:
                        index = self._parse_ftdi_ports(self.port)
                    except ValueError:
                        logger.warning(f"reconnect() Could not open serial device {self.port}")
                    else:
                        try:

                            self._ftdi_serial = ftdi.open(index)
                            self._ftdi_serial.setBaudRate(self.baudrate)
                            self._ftdi_serial.setDataCharacteristics(self.bytesize, self.stopbits, self.parity)
                            self._ftdi_serial.setTimeouts(1, 1)
                            self._ftdi_serial.setLatencyTimer(2)

                            # Toggle Connected
                            for callback in self._connected_callbacks:
                                callback(True)

                            asyncio.create_task(self._connection_loop())
                            return
                        except ftdi.DeviceError as e:
                            logger.warning(f"reconnect() Could not open serial_ftdi device {self.port}")

                            if self._ftdi_serial:
                                self._ftdi_serial.close()

                    await asyncio.sleep(self.reconnect_period - (time.perf_counter() - start_time))

        def read_bytes(self, size=4096):
            try:

                data = self._ftdi_serial.read(size)
                if data:
                    logger.debug(f"Received data2: {data} from FTDI {self.port}")

                return data

            except ftdi.DeviceError:
                return b''

        async def _connection_loop(self):

            while True:
                start_time = time.perf_counter()
                self._last_time = time.time()
                try:
                    start = time.perf_counter()
                    num_bytes = self._ftdi_serial.getQueueStatus()
                    if num_bytes:
                        data = self._ftdi_serial.read(num_bytes)

                        # logger.info(f"Read time: {time.perf_counter() - start}")
                        if data:
                            # logger.debug(f"Received data: {data} from FTDI {self.port}")

                            for callback in self._bytes_callbacks:
                                callback(data)

                except ftdi.DeviceError:
                    # Trigger reconnect
                    # if self._ftdi_serial:
                    #     self._ftdi_serial.close()
                    # asyncio.create_task(self._reconnect_loop())
                    #
                    # # Toggle not Connected
                    # for callback in self._connected_callbacks:
                    #     callback(False)

                    pass

                dt: float = time.perf_counter() - start_time
                await asyncio.sleep(self._connection_loop_period - (time.perf_counter() - start_time))

        def add_bytes_callback(self, callback: Callable[[bytes], None]):
            self._bytes_callbacks.add(callback)

        def remove_bytes_callback(self, callback):
            if callback in self._bytes_callbacks:
                self._bytes_callbacks.remove(callback)

        def add_connected_callback(self, callback: Callable[[bool], None]):
            self._connected_callbacks.add(callback)

        def remove_connected_callback(self, callback: Callable[[bool], None]):
            if callback in self._connected_callbacks:
                self._connected_callbacks.remove(callback)
            pass

        def disconnect(self):
            self._reconnect = False

            logger.debug(f"disconnect(): Disconnecting Serial_FTDI")

            try:
                if self._ftdi_serial:
                    logger.debug(f"disconnect(): Disconnecting Serial_FTDI_OBJECT")
                    self._ftdi_serial.close()
            except ftdi.DeviceError as e:
                logger.warning(f"Error closing connection {e}")

            # Toggle Connected
            for callback in self._connected_callbacks:
                callback(False)

        def send_bytes(self, data: bytes):
            if self.connected:
                # logger.debug(f"Writing data: {data} to FTDI {self.port}")
                self._ftdi_serial.write(data)


        @staticmethod
        def _parse_ftdi_ports(port: str) -> int:
            """
            Return the ftdi port index for give port name.

            If it doesnt exist, it will raises an OSError.
            """

            # Get COM port hardware id if COM port of selected name available
            pyserial_comport_hardware_id = None
            comport_list = serial.tools.list_ports.comports()
            total_com_port_count = len(comport_list)
            for port_from_comport_list, desc, hwid in sorted(comport_list):
                # Check if COM port names match
                if port == port_from_comport_list:
                    pyserial_comport_hardware_id = hwid

            # If no COM ports match
            if pyserial_comport_hardware_id is None:
                raise ValueError(f"Unable to find serial ftdi index for {port}")

            # Get COM port serial device id from hardware id list of pyserial
            pyserial_comport_serial_id = (pyserial_comport_hardware_id.split("SER=", 1)[1])[:8]

            # Check ftdi device index list for COM port that matches user selected COM port
            for index in range(total_com_port_count):
                try:
                    ftdi_target = ftdi.open(index)
                    ftdi_comport_serial_id = str((ftdi_target.getDeviceInfo())["serial"])
                    ftdi_comport_serial_id = (ftdi_comport_serial_id.split("b'", 1)[1]).split("'", 1)[0]

                    # If serial device ids match between pyserial and ftd2xx driver port selections,
                    # return ftd2xx port index
                    if pyserial_comport_serial_id == ftdi_comport_serial_id:
                        ftdi_target.close()
                        logger.info(f"Found FTDI Device at {pyserial_comport_serial_id, index}")
                        return index
                    ftdi_target.close()
                except ftdi.DeviceError:
                    logger.warning(f"Could not open {index}")

            raise ValueError(f"Unable to find serial ftdi index for {port}")

elif platform.system() == "Linux":
    import pylibftdi as libftdi

    FTDI_TIMEOUT = 2  # ms (ints)


    class SerialFTDIConnection(ConnectionBase):
        _ftdi_serial: Optional[libftdi.Device] = None

        _reconnect: bool = True

        reconnect_period: float = 1.0

        _connection_loop_period = 0.0001

        def __init__(self, port: str,
                     baudrate: int = 115200,
                     bytesize: int = 8,
                     parity: str = 2,
                     stopbits: int = 1):
            logger.debug(f"__init__(): Creating SerialFTDIConnection({port, baudrate, bytesize, parity, stopbits})")

            if stopbits == serial.STOPBITS_ONE:
                stopbits = ftdidef.STOP_BITS_1
            elif stopbits == serial.STOPBITS_TWO:
                stopbits = ftdidef.STOP_BITS_2
            if parity == serial.PARITY_NONE:
                parity = ftdidef.PARITY_NONE
            elif parity == ftdidef.PARITY_EVEN:
                parity = ftdidef.PARITY_EVEN
            elif parity == serial.PARITY_ODD:
                parity = ftdidef.PARITY_ODD
            if bytesize == ftdidef.BITS_8:
                bytesize = ftdidef.BITS_8
            elif bytesize == serial.SEVENBITS:
                bytesize = ftdidef.BITS_7

            self.port = port
            self.baudrate = baudrate
            self.bytesize = bytesize
            self.parity = parity
            self.stopbits = stopbits

            # Callback for any bytes received
            self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

            # Callbacks for created/lost connections
            self._connected_callbacks: Set[Callable[[bool], None]] = set()

            self.connecting_lock = asyncio.Lock()

        def reconfigure(self, port: str,
                        baudrate: int = 115200,
                        bytesize: int = 8,
                        parity: str = 2,
                        stopbits: int = 1):

            if stopbits == serial.STOPBITS_ONE:
                stopbits = ftdidef.STOP_BITS_1
            elif stopbits == serial.STOPBITS_TWO:
                stopbits = ftdidef.STOP_BITS_2
            if parity == serial.PARITY_NONE:
                parity = ftdidef.PARITY_NONE
            elif parity == ftdidef.PARITY_EVEN:
                parity = ftdidef.PARITY_EVEN
            elif parity == serial.PARITY_ODD:
                parity = ftdidef.PARITY_ODD
            if bytesize == ftdidef.BITS_8:
                bytesize = ftdidef.BITS_8
            elif bytesize == serial.SEVENBITS:
                bytesize = ftdidef.BITS_7

            if ((port, baudrate, bytesize, parity, stopbits)
                    != (self.port, self.baudrate, self.bytesize, self.parity, self.stopbits)):

                self.port = port
                self.baudrate = baudrate
                self.bytesize = bytesize
                self.parity = parity
                self.stopbits = stopbits
                if self.connected:
                    self.disconnect()
                    asyncio.create_task(self.connect())

        @property
        def connected(self) -> bool:
            if self._ftdi_serial is None:
                return False
            else:
                try:
                    if self._ftdi_serial.closed:
                        return False
                    else:
                        return True
                except libftdi.FtdiError:
                    return False

        @staticmethod
        def _get_serial_device_matching_port(pyserial_list, port):
            '''
            Get matching serial device for ftdi
            :param pyserial_list:
            :param port:
            :return:
            '''
            for pyserial_dev in pyserial_list:
                if pyserial_dev.device == port:
                    return pyserial_dev
            return None

        @staticmethod
        def _get_libftdi_device_number(libftdi_list, pyserial_dev):
            '''
            Get device number for libftdi
            :param libftdi_list:
            :param pyserial_dev:
            :return:
            '''
            index_number = 0
            for ftdi_dev in libftdi_list:
                print(ftdi_dev[2], pyserial_dev.serial_number)
                if ftdi_dev[2] == pyserial_dev.serial_number:
                    return index_number
                index_number += 1
            return None

        def connect_no_async(self):
            # try:
            #     index = self._parse_ftdi_ports(self.port)
            # except ValueError:
            #     logger.warning(f"Could not open serial device {self.port}")
            #     return -1
            try:

                libftdi_list = libftdi.Driver().list_devices()
                pyserial_list = serial.tools.list_ports.comports()
                pyserial_dev = self._get_serial_device_matching_port(pyserial_list, self.port)
                libftdi_number = None
                index = None
                if pyserial_dev:
                    index = self._get_libftdi_device_number(libftdi_list, pyserial_dev)
                if index is None:
                    self._ftdi_serial = None
                    logger.info(f"connect_no_async(): Serial device could not be opened with port: {self.port}")
                    return
                self._ftdi_serial = libftdi.Device(device_index=index)
                self._ftdi_serial.baudrate = 115200
                self._ftdi_serial.ftdi_fn.ftdi_set_line_property(self.bytesize, self.stopbits, self.parity)
                self._ftdi_serial.ftdi_fn.ftdi_set_latency_timer(2)
                self._ftdi_serial.ftdi_fn.usb_read_timeout = 5 * 1000
                logger.info(f'Libftdi connected')
                # self.connected = True

                #
                # self._ftdi_serial = ftdi.open(index)
                # self._ftdi_serial.setBaudRate(self.baudrate)
                # self._ftdi_serial.setDataCharacteristics(self.bytesize, self.stopbits, self.parity)
                # self._ftdi_serial.setTimeouts(FTDI_TIMEOUT, FTDI_TIMEOUT)
                # self._ftdi_serial.setLatencyTimer(2)

                # Toggle Connected
                for callback in self._connected_callbacks:
                    callback(True)
                asyncio.create_task(self._connection_loop())
                return 0
            except Exception as e:
                if self._ftdi_serial:
                    self._ftdi_serial.close()
                return -1

        async def connect(self):

            self._reconnect = True
            if self.connecting_lock.locked():
                logger.warning(f"Already Attempting to connect to {self.port}")
                return -2

            async with self.connecting_lock:
                if self.connected:
                    logger.warning("FTDI Connection already connected")
                    return

                try:
                    libftdi_list = libftdi.Driver().list_devices()
                    pyserial_list = serial.tools.list_ports.comports()
                    pyserial_dev = self._get_serial_device_matching_port(pyserial_list, self.port)
                    libftdi_number = None
                    index = None
                    if pyserial_dev:
                        index = self._get_libftdi_device_number(libftdi_list, pyserial_dev)
                    if index is None:
                        self._ftdi_serial = None
                        logger.info(f"connect_no_async(): Serial device could not be opened with port: {self.port}")
                        return
                    self._ftdi_serial = libftdi.Device(device_index=index)
                    self._ftdi_serial.baudrate = 115200
                    self._ftdi_serial.ftdi_fn.ftdi_set_line_property(self.bytesize, self.stopbits, self.parity)
                    self._ftdi_serial.ftdi_fn.ftdi_set_latency_timer(2)
                    self._ftdi_serial.ftdi_fn.usb_read_timeout = 5 * 1000
                    logger.info(f'Libftdi connected')
                    # self.connected = True
                    for callback in self._connected_callbacks:
                        callback(True)

                    asyncio.create_task(self._connection_loop())
                    return 0
                except Exception as e:
                    if self._ftdi_serial:
                        self._ftdi_serial.close()

                    # Unsuccessful, initiate the reconnect loop

                    asyncio.create_task(self._reconnect_loop())
                pass

        async def _reconnect_loop(self):
            if self.connecting_lock.locked():
                logging.error("self.connecting_lock is still locked")
                return
            with self.connecting_lock:
                await asyncio.sleep(self.reconnect_period)

                while self._reconnect:

                    start_time = time.perf_counter()
                    logger.warning("Attempting to open FTDI Connection")
                    try:
                        libftdi_list = libftdi.Driver().list_devices()
                        pyserial_list = serial.tools.list_ports.comports()
                        pyserial_dev = self._get_serial_device_matching_port(pyserial_list, self.port)
                        libftdi_number = None
                        index = None
                        if pyserial_dev:
                            index = self._get_libftdi_device_number(libftdi_list, pyserial_dev)
                        if index is None:
                            self._ftdi_serial = None
                            logger.info(f"connect_no_async(): Serial device could not be opened with port: {self.port}")
                            return
                        self._ftdi_serial = libftdi.Device(device_index=index)
                        self._ftdi_serial.baudrate = 115200
                        self._ftdi_serial.ftdi_fn.ftdi_set_line_property(self.bytesize, self.stopbits, self.parity)
                        self._ftdi_serial.ftdi_fn.ftdi_set_latency_timer(2)
                        self._ftdi_serial.ftdi_fn.usb_read_timeout = 5 * 1000
                        logger.info(f'Libftdi connected')
                        # self.connected = True
                        for callback in self._connected_callbacks:
                            callback(True)

                        asyncio.create_task(self._connection_loop())
                        return
                    except ftdi.DeviceError as e:
                        logger.warning(f"Could not open serial_ftdi device {self.port}")

                        if self._ftdi_serial:
                            self._ftdi_serial.close()

                    await asyncio.sleep(self.reconnect_period - (time.perf_counter() - start_time))

        def read_bytes(self, size=4096):
            try:
                data = self._ftdi_serial.read(size)
                if data:
                    logger.debug(f"Received data2: {data} from FTDI {self.port}")

                return data

            except ftdi.DeviceError:
                return b''

        async def _connection_loop(self):

            while True:
                start_time = time.perf_counter()
                try:
                    data = self._ftdi_serial.read(1024)
                    if data:
                        logger.debug(f"Received data: {data} from FTDI {self.port}")

                        for callback in self._bytes_callbacks:
                            callback(data)

                except libftdi.FtdiError:
                    # Trigger reconnect
                    # if self._ftdi_serial:
                    #     self._ftdi_serial.close()
                    # asyncio.create_task(self._reconnect_loop())
                    #
                    # # # Toggle not Connected
                    # for callback in self._connected_callbacks:
                    #     callback(False)

                    pass

                dt: float = time.perf_counter() - start_time
                await asyncio.sleep(self._connection_loop_period - dt)

        def add_bytes_callback(self, callback: Callable[[bytes], None]):
            self._bytes_callbacks.add(callback)

        def remove_bytes_callback(self, callback):
            if callback in self._bytes_callbacks:
                self._bytes_callbacks.remove(callback)

        def add_connected_callback(self, callback: Callable[[bool], None]):
            self._connected_callbacks.add(callback)

        def remove_connected_callback(self, callback: Callable[[bool], None]):
            if callback in self._connected_callbacks:
                self._connected_callbacks.remove(callback)
            pass

        def disconnect(self):
            self._reconnect = False

            logger.debug(f"disconnect(): Disconnecting Serial_FTDI")

            try:
                if self._ftdi_serial:
                    logger.debug(f"disconnect(): CLOSING FTDI OBJECT Serial")
                    self._ftdi_serial.close()
            except ftdi.DeviceError as e:
                logger.warning(f"Error closing connection {e}")

            # Toggle Connected
            for callback in self._connected_callbacks:
                callback(False)

        def send_bytes(self, data: bytes):
            if self.connected:
                logger.debug(f"Writing data: {data} to FTDI {self.port}")
                self._ftdi_serial.write(data)
