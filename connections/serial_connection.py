import asyncio
from typing import Union, Set, Callable, Optional
import serial
import logging

from colorlog import ColoredFormatter

from RC_Core import rc_logging
from RC_Core.connections import ConnectionBase
from RC_Core.comms.protocols import SerialProtocol, create_serial_protocol_connection

logger = rc_logging.getLogger(__name__)


class SerialConnection(ConnectionBase):
    """ Serial Connection
    Arguments: port, baudrate, bytesize, parity, stopbits
    """

    protocol: Union[SerialProtocol, None] = None
    _reconnect: bool = True
    _reconnect_loop_task: Optional[asyncio.Task] = None
    reconnect_period: float = 1.0  # seconds

    def __init__(self, port: str,
                 baudrate: int = 115200,
                 bytesize: int = serial.EIGHTBITS,
                 parity: str = serial.PARITY_NONE,
                 stopbits: int = serial.STOPBITS_ONE):
        logger.debug(f"__init__(): Creating SerialConnection({port, baudrate, bytesize, parity, stopbits})")
        self.port = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits

        # Callback for any bytes received
        self._bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for created/lost connections
        self._connected_callbacks: Set[Callable[[bool], None]] = set()

        self.connecting_lock = asyncio.Lock()

    def reconfigure(self, port: str,
                    baudrate: int = 115200,
                    bytesize: int = serial.EIGHTBITS,
                    parity: str = serial.PARITY_NONE,
                    stopbits: int = serial.STOPBITS_ONE):
        if ((port, baudrate, bytesize, parity, stopbits)
                != (self.port, self.baudrate, self.bytesize, self.parity, self.stopbits)):
            self.port = port
            self.baudrate = baudrate
            self.bytesize = bytesize
            self.parity = parity
            self.stopbits = stopbits
            if self.connected:
                self.disconnect()
                asyncio.create_task(self.connect())

    @property
    def connected(self) -> bool:
        if self.protocol and self.protocol.connected:
            return True
        else:
            return False

    async def connect(self) -> int:
        logger.debug(f"connect(): Connecting Serial")

        self._reconnect = True
        if self.connecting_lock.locked():
            logger.warning(f"connect()Already Attempting to connect to {self.port}")
            return -2

        async with self.connecting_lock:
            logger.info(f"connect()Attempting to Connect to serial port {self.port}")

            if self.connected:
                logger.warning(f"connect()Already Connected to serial port {self.port}")
                return -3

            try:
                self.protocol = await create_serial_protocol_connection(self.port,
                                                                        self.baudrate,
                                                                        self.bytesize,
                                                                        self.parity,
                                                                        self.stopbits)

                self.protocol.bytes_callbacks.update(self._bytes_callbacks)
                self.protocol.connection_lost_callbacks.add(self._lost_connection)

                # Toggle Connected
                for callback in self._connected_callbacks:
                    callback(True)

                return 1

            except serial.SerialException as e:
                logger.warning(f"connect() Could not connect to serial port: {e}")
                if self._reconnect:
                    logger.warning(f"connect()Initiating Reconnecting loop")
                    self._reconnect_loop_task = asyncio.create_task(self._reconnect_loop())
                return -1

    async def _reconnect_loop(self):
        if self.connecting_lock.locked():
            logging.error("reconnect()self.connecting_lock is still locked")
            return
        async with self.connecting_lock:
            await asyncio.sleep(self.reconnect_period)
            while self._reconnect:
                logger.info(f"reconnect() Attempting to Connect to serial port {self.port}")
                try:
                    self.protocol = await create_serial_protocol_connection(self.port,
                                                                            self.baudrate,
                                                                            self.bytesize,
                                                                            self.parity,
                                                                            self.stopbits)

                    self.protocol.bytes_callbacks.update(self._bytes_callbacks)
                    self.protocol.connection_lost_callbacks.add(self._lost_connection)

                    # Toggle Connected
                    for callback in self._connected_callbacks:
                        callback(True)
                    return

                except serial.SerialException as e:
                    logger.warning(f"reconnect() Could not connect to serial port {e}")
                    await asyncio.sleep(self.reconnect_period)

    def add_bytes_callback(self, callback: Callable[[bytes], None]):
        self._bytes_callbacks.add(callback)
        if self.protocol:
            self.protocol.bytes_callbacks.add(callback)
        pass

    def remove_bytes_callback(self, callback: Callable[[bytes], None]):
        self._bytes_callbacks.remove(callback)
        if self.protocol and callback in self.protocol.bytes_callbacks:
            self.protocol.bytes_callbacks.remove(callback)
        pass

    def add_connected_callback(self, callback: Callable[[bool], None]):
        self._connected_callbacks.add(callback)

    def remove_connected_callback(self, callback: Callable[[bool], None]):
        if callback in self._connected_callbacks:
            self._connected_callbacks.remove(callback)
        pass

    def _lost_connection(self):
        logger.warning(f"Serial {self.port} Lost Connection")
        self.protocol = None

        if self._reconnect:
            asyncio.create_task(self.connect())

        for callback in self._connected_callbacks:
            callback(False)

    def disconnect(self):
        logger.debug(f"disconnect(): Disconnecting Serial")

        self._reconnect = False
        if self.protocol:
            self.protocol.close()

        for callback in self._connected_callbacks:
            callback(False)

    def send_bytes(self, data) -> int:
        # logger.debug(f"send_bytes(): Send Bytes")
        if self.connected:
            try:
                result = self.protocol.write(data)
                # result = self.protocol.transport.serial.write(data)
                # self.protocol.transport.serial.flush()
                return result

            except Exception as e:

                logger.warning(f"Error writing to serial {e}")
                return -1
        else:
            return -1
