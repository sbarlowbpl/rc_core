from typing import Callable


class ConnectionBase:
    """ Connection Base Class
    Base Class for Connection Objects.

    Features:
    - Connect and Disconnect at will.
    - Connections that are lost (not through disconnecting) will reconnect.

    """
    @property
    def connected(self) -> bool:
        raise NotImplementedError

    def reconfigure(self, *args):
        raise NotImplementedError

    async def connect(self):
        raise NotImplementedError

    def add_bytes_callback(self, callback: Callable[[bytes], None]):
        raise NotImplementedError

    def remove_bytes_callback(self, callback):
        raise NotImplementedError

    def add_connected_callback(self, callback: Callable[[bool], None]):
        raise NotImplementedError

    def remove_connected_callback(self, callback: Callable[[bool], None]):
        raise NotImplementedError

    def disconnect(self):
        raise NotImplementedError

    def send_bytes(self, data: bytes):
        raise NotImplementedError

    def flush(self):
        pass
