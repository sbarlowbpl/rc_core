# Serial functions
import socket
import threading

# from kivy.app import App
import serial,struct,re, binascii
from cobs import cobs
from RC_Core.packet_structure import packet_format, Container
from crcmod import crcmod
# from kivy.clock import Clock
from functools import partial
from RC_Core.construct import *
from RC_Core.RS1_hardware import PacketID, Mode
from array import array
import time

       # Convert numeric property velocity into bytes
from RC_Core.packetid import Packets


def convert_to_bytes(parameter):
    # print("actual data value = ", parameter)
    # print(type(parameter).__name__)
    if type(parameter).__name__ == 'float':
        # print('converting FLOAT')
        bytes_value = bytearray(struct.pack("f", parameter))
    elif type(parameter).__name__ == 'int':
        # print('converting INT')
        # bytes_value = bytearray(struct.pack("b", parameter))
        bytes_value = bytearray(struct.pack("i", parameter))

    elif type(parameter).__name__ == 'list':
        # print('converting LIST')
        bytes_value= bytearray()
        for val in parameter:
            bytes_value +=bytearray(struct.pack("f", val))
        # print(["0x%02x" % b for b in bytes_value])
    else:
        return parameter
    # print("bytes_value is:", ["0x%02x" % b for b in bytes_value])
    return bytes_value


class SerialDevice():

    port=None
    baud=115200
    ipAddress=None
    comsType='SERIAL'
    serialPort=serial.Serial()

    udp_socket = None
    remoteIPAddress = ('192.168.2.2', 6789)

    incomplete_pckt=b''

    decoded_pckt=b''
    protocol = "BPL"

    verbose = False # set True for print statements

    write_time = 0
    parse_packet_errors = 0

    crc8_func = None
    tcp_socket = None
    tcp_lock = None

    def __init__(self):
        self.crc8_func = crcmod.mkCrcFun(0x14D, initCrc=0xFF, xorOut=0xFF)
        self.float_packets_list = Packets().get_float_packet_ids()

    def open(self):
        if self.serialPort is None:
            self.serialPort = serial.Serial()
        print(self.baud)
        print(self.port)
        if self.serialPort.is_open:
            self.close()
        self.serialPort.baudrate = self.baud
        self.serialPort.port = self.port
        self.serialPort.timeout = 0
        self.serialPort.write_timeout = None
        # self.serialPort.inter_byte_timeout = 0.1

    # open serial port
        if self.serialPort.is_open == False:
            try:
                self.serialPort.open()
            except:
                print ("Failed to connect")
                #exit()
        if self.serialPort.is_open:
            print("serial is open")
            return self.serialPort
        else:
            return None

    def close(self):
        if self.serialPort:
            # if self.serialPort.is_open:
            #     self.serialPort.close()
            self.serialPort.close()
        else:
            self.serialPort = serial.Serial()

    def connect_tcp(self, ip_address_string, port_number):
        """
        opens a tcp socket to the specified ip address and port number
        :param ip_address_string:
        :param port_number:
        :return:
        """
        self.tcp_ip_address = ip_address_string
        self.tcp_port_number = port_number
        # Create a TCP/IP
        self.tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect the socket to the port where the server is listening
        server_address = (ip_address_string, port_number)
        print(__name__, 'connect_tcp() connecting to %s port %s' % server_address)
        try:
            self.tcp_socket.connect(server_address)
            self.comsType = 'TCP'
            time.sleep(0.1)
            self.tcp_socket.setblocking(0)
            self.comport_status = 'open'
            self.tcp_lock = threading.Lock()
            return True
        except ConnectionRefusedError:
            print(__name__, 'connect_tcp() ERROR CONNECTION REFUSED')
            return False
        except TimeoutError:
            print(__name__, 'connect_tcp() ERROR CONNECTION TIMEOUT')
            return False

    def reconnect_tcp(self):
        if self.tcp_socket:
            self.tcp_socket.close()
            if self.connect_tcp(self.tcp_ip_address, self.tcp_port_number):
                return True
        else:
            self.comsport_status = 'closed'

    def scheduleReceive(self,dt):
        Clock.schedule_interval(self.readdata, dt)

    #send BPL packet down serialDevice
    def sendpacket(self, device_id, packet_id, data_in):

        # if type(self.txData).__name__ == 'float':
        # self.txData=convert_to_bytes(self.txData)
        txData = convert_to_bytes(data_in)
        if self.verbose:
            print(txData)
        # datalength = len(self.txData)
        datalength = len(txData)

        #print(datalength)
        packet_length = datalength + 4
        # print(__name__, 'sendpacket() device_id:', device_id, 'packet_id:', "0x%02x" % packet_id,
        #       'data:', ["0x%02x" % b for b in txData], 'length', datalength)

        # print("RAW MATERIALS:", data, packetID, deviceID, datalength)
        # print('packet ID:',self.txPacketId)
        # print('device ID:',self.txDeviceId)

        txPacket = txData
        txPacket.append(packet_id)
        txPacket.append(device_id)
        txPacket.append(packet_length)
        crcValue=self.crc8_func(txPacket)
        txPacket.append(crcValue)
        # print('\narray check')
        #     # crc=0,
        #     # terminator=0x00))
        # #print("RAW PACKET", raw)
        # print([ "0x%02x" % b for b in txPacket ])

        ## COBS ENCODE
        encoded = cobs.encode(txPacket)
        #print("COBS ENCODED PACKET:", encoded)
        if self.verbose: print('\nencoded packet check')
        if self.verbose: print([ "0x%02x" % b for b in encoded ])

        ## ADD TERMINATOR BYTE
        packet = encoded + b"\x00"
        # packet = bytearray(encoded).append(0x0)
        if self.comsType == 'SERIAL':
            if self.serialPort != None:
                if self.serialPort.is_open:
                    # if self.verbose:
                    # print(int(time.time()), 'SERIAL_DEVICE.py sendpacket() final packet check', ["0x%02x" % b for b in packet])

                    # new_time = time.time()
                    # # print(__name__, 'time since last write:', self.write_time - new_time)
                    # time_diff = new_time - self.write_time
                    # if time_diff < 0.002:
                    #     # print(time_diff)
                    #     time.sleep(0.002 - time_diff)

                    try:
                        # self.serialPort.send_break(0.002)
                        self.serialPort.write(packet)

                    except:
                        print(__name__, 'sendpacket ERROR', self.serialPort)
                        self.close()
                        self.open()
                    # self.write_time = time.time()

                    # self.serialPort.setRTS(True)
                    # print(__name__, 'sendpacket() self.serialPort.inter_byte_timeout', self.serialPort.inter_byte_timeout)
                    # print(__name__, 'sendpacket() self.serialPort.write(packet)', self.serialPort.write(packet))

                    # try:
                    #     # self.serialPort.setRTS(False)
                    #     print(__name__, 'sendpacket() self.serialPort.write(packet)', self.serialPort.write(packet))
                    #     # self.serialPort.setRTS(True)
                    # except:
                    #     self.close()
                    # #     pass

        elif self.comsType == 'UDP':
            # print("sending UDP packet")
            # print (self.remoteIPAddress)
            # print('test')
            # packetTest=bytes([0x02, 0x61, 0x01, 0x01, 0x05, 0x60, 0x01, 0x08, 0x17,0x00])
            # print([ " 0x%02x" % b for b in packetTest])
            # print(packetTest)
            # print(type(packetTest))
            # self.udp_socket.sendto(packetTest, self.remoteIPAddress)


            # print('SERIAL_DEVICE.py:164 sendpacket() packet', [ " 0x%02x" % b for b in packet])


            # print([ " 0x%02x" % b for b in packet])
            # print(packet)

            try:
                self.udp_socket.sendto(packet, self.remoteIPAddress)
            except:
                print("ERROR: serial_device.py:167 sendpacket(): remoteIPAddress:", self.remoteIPAddress, 'packet', packet)
        elif self.comsType == 'TCP':
            if self.tcp_lock:
                try:
                    with self.tcp_lock:
                        self.tcp_socket.sendall(packet)
                except ConnectionResetError as e:
                    print(__name__, "sendpacket() TCP ConnectionResetError:", e)
                    self.reconnect_tcp()
                except ConnectionAbortedError as e:
                    print(__name__, "sendpacket() TCP ConnectionAbortedError:", e, "with packet", packet)
                    self.reconnect_tcp()
                except OSError as e:
                    print(__name__, "sendpacket() TCP OSError:", e, "with packet", packet)
                    self.reconnect_tcp()
        return packet

###_________________________ Receive Function_____________________________###
    def readdata(self):
        if self.comsType == 'SERIAL':
            if self.serialPort:
                if self.serialPort.is_open:
                    bytesToRead = 0

                    try:
                        bytesToRead = self.serialPort.in_waiting
                    except:
                        self.close()

                    if bytesToRead:
                        # if self.verbose: print("Bytes to read", bytesToRead)
                        # if self.verbose: print("incomplete packet")
                        # if self.verbose: print([ " 0x%02x" % b for b in self.incomplete_pckt ])
                        newBytes=self.serialPort.read(bytesToRead)
                        # if self.verbose: print("new bytes")
                        # if self.verbose:
                        # print('New bytes:', [ " 0x%02x" % b for b in newBytes ])
                        buff = self.incomplete_pckt + newBytes

                        if self.verbose:
                            print("buffer received is", [ " 0x%02x" % b for b in buff ])
                        # if self.verbose: print("here")
                        packets = re.split(b'\x00', buff)
                        if self.verbose: print(packets)

                        if buff[-1] != b'0x00':
                            self.incomplete_pckt=packets.pop()

                        if self.verbose:
                        # print("list of packets is ", packets, "\nwith incomplete packet", self.incomplete_pckt)
                            for pkt in packets:
                                print(__name__, 'readdata()', [" 0x%02x" % b for b in pkt])
                            print('incomplete', [" 0x%02x" % b for b in self.incomplete_pckt])
                        return packets

                        # for self.pckt in packets:
                            # print("unpacked data\n",self.rxData[0])  # unpacked float/s - need to loop for more than one number in the data field?

                    else:           # no more bytes to read
                        return []
                    # time.sleep(1)
                      #  return buff     # for debugging on single send and receive pairs (not timed)

                else:       # pass while waiting for send to finish
                    return []
        elif self.comsType=='UDP':
            newBytes=None
            try:
                newBytes, self.remoteIPAddress = self.udp_socket.recvfrom(4096)
                # newBytes, self.remoteIPAddress = self.udp_socket.recvfrom(8192)
            except:
                print(__name__, "readata() ERROR UDP receive")
            else:
                if newBytes!=None:
                    buff = self.incomplete_pckt + newBytes
                    # print(newBytes)
                    # print(self.remoteIPAddress)
                    # print("buffer received is", buff)
                    # print("here")
                    packets = re.split(b'\x00', buff)

                    if buff[-1] != b'0x00':
                        self.incomplete_pckt=packets.pop()
                    return packets
            return []
        elif self.comsType == 'TCP':
            new_bytes = ''
            try:
                with self.tcp_lock:
                    new_bytes = self.tcp_socket.recv(4096)
            except:
                # print(__name__, "readata() ERROR TCP receive")
                return []
            else:
                if new_bytes:
                    buff = self.incomplete_pckt + new_bytes

                    packets = re.split(b'\x00', buff)

                    if buff[-1] != b'0x00':
                        self.incomplete_pckt = packets.pop()
                    return packets
            return []

# parses packet data according to packet ID
# Must be called for every packet in the packets buffer
    def parsepacket(self, packet_in):
        # print('serial_device.py parsepacket() pckt:', ["0x%02x" % b for b in self.pckt])
        if packet_in != b'' and len(packet_in) > 3:
            decoded_pckt = cobs.decode(packet_in)  ## Cobs decode
            if decoded_pckt[-2] != len(decoded_pckt): # check that length is correct. Generally would only happen on first packet or due to bad transmission
                self.parse_packet_errors += 1
                print(__name__, 'error count:', self.parse_packet_errors," #################################################### #################################################### #################################################### #################################################### ####################################################"
                                "PARSEPACKET() incorrect Length, length is:", len(decoded_pckt), [ " 0x%02x" % b for b in decoded_pckt ])
                # decoded_pckt = cobs.decode(self.pckt)  ## Cobs decode
                # # print("\n start decode")
                # print([ " 0x%02x" % b for b in decoded_pckt ])
            else:

                # print('pckt in packets is:',pckt)
                # print('serial_device.py parsepacket() decoded_pckt:', ["0x%02x" % b for b in decoded_pckt])
                #print([ "0x%02x" % b for b in decoded_pckt[:-4] ])
                crcCheck=self.crc8_func(decoded_pckt[:-1])
                rxCRC=decoded_pckt[-1]
                if crcCheck == rxCRC:
                # print("\n start decode")
                #     print(__name__, 'parsepacket() decoded_pckt:', ["0x%02x" % b for b in decoded_pckt ])
                #     i = 1
                #     floatData = []
                #     data = []
                    rxDeviceId=decoded_pckt[-3]
                    rxPacketId=decoded_pckt[-4]
                    rxData=decoded_pckt[:-4]
                    decoded_pckt = b''
                    # print(rxDeviceId)

                    #print('crc check value:',crcCheck)

                    if self.verbose: print('LIST rxData:', rxData)

                    rxData = bytearray(rxData) # convert list to bytearray

                    # if self.verbose: print('BYTEARRAY rxData:', rxData)

                    if rxPacketId in self.float_packets_list:
                        data_to_process = rxData
                        rxData = []
                        for i in range(int(len(data_to_process) / 4)):
                            this_float_byte = data_to_process[0:4]
                            data_to_process = data_to_process[4:]
                            rxData.append(struct.unpack("f", this_float_byte)[0])
                    else:
                        out_data = []
                        for b in rxData:
                            out_data.append(b)
                        rxData = out_data

                    if self.verbose: print('PARSE float:', rxData)
                    return rxDeviceId, rxPacketId, rxData

                # print(
                #       "device_id:", rxDeviceId,
                #       "\npacket_id:", rxPacketId,
                #       "\nCRC:", rxCRC,
                #       '\ncrc check: ',crcCheck,
                #       "\ndata:", rxData)

                else:
                    print("CRC Error")
                    print([" 0x%02x" % b for b in packet_in])
        return 0, 0, 0

    # returns the raw data without consideration for float or int packet
    def parse_packet_to_bytearray(self, packet_in):
        # print('serial_device.py parsepacket() pckt:', ["0x%02x" % b for b in self.pckt])
        if packet_in != b'' and len(packet_in) > 3:
            decoded_pckt = cobs.decode(packet_in)  ## Cobs decode
            if decoded_pckt[-2] != len(
                    decoded_pckt):  # check that length is correct. Generally would only happen on first packet or due to bad transmission
                self.parse_packet_errors += 1
                print(__name__, 'error count:', self.parse_packet_errors,
                      " ####################################################",
                      "PARSEPACKET() incorrect Length, length is:", len(decoded_pckt),
                      [" 0x%02x" % b for b in decoded_pckt])

            else:
                # print('serial_device.py parsepacket() decoded_pckt:', ["0x%02x" % b for b in decoded_pckt])
                if self.crc8_func is None:
                    self.crc8_func = crcmod.mkCrcFun(0x14D, initCrc=0xFF, xorOut=0xFF)
                    print(__name__, 'parsepacket():', 'initialised crc checking function')

                # print([ "0x%02x" % b for b in decoded_pckt[:-4] ])
                crcCheck = self.crc8_func(decoded_pckt[:-1])
                rxCRC = decoded_pckt[-1]
                if crcCheck == rxCRC:
                    #     print(__name__, 'parsepacket() decoded_pckt:', ["0x%02x" % b for b in decoded_pckt ])
                    rxDeviceId = decoded_pckt[-3]
                    rxPacketId = decoded_pckt[-4]
                    rxData = decoded_pckt[:-4]

                    if self.verbose: print('LIST rxData:', rxData)

                    rxData = bytearray(rxData)  # convert list to bytearray

                    return rxDeviceId, rxPacketId, rxData

                # print(
                #       "device_id:", rxDeviceId,
                #       "\npacket_id:", rxPacketId,
                #       "\nCRC:", rxCRC,
                #       '\ncrc check: ',crcCheck,
                #       "\ndata:", rxData)

                else:
                    print("CRC Error", [" 0x%02x" % b for b in packet_in])
        return None, None, None

    @staticmethod
    def bytearray_to_float(barray):
        data_to_process = barray
        rxData = []
        for i in range(int(len(data_to_process) / 4)):
            this_float_byte = data_to_process[0:4]
            data_to_process = data_to_process[4:]
            rxData.append(struct.unpack("f", this_float_byte)[0])
        return rxData


def udp_test():
    ser = SerialDevice()
    connectUDPServer(ser)

def connectUDPServer(self, ip_address="192.168.2.2", udp_port=6789):
    self.remoteIPAddress = (ip_address, udp_port)
    self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.comsType = "UDP"

    try:
        self.udp_socket.setblocking(0)
        self.udp_socket.bind((self.serial_device.ipAddress, self.connection_udpPort))
        print(__name__, 'connectUDPServer() Connected UDP Server',
              self.ipAddress, self.port)
    except:
        print("failed to connect", self.ipAddress, self.port)


if __name__ == "__main__":

    ser = SerialDevice()
    # ser.port = "COM28" # DTech 485
    # ser.port = "COM35"  # bare board 232
    # ser.port = "COM5"  # Red 485
    # ser.port = "COM27"  # Red 485
    # ser.port = "COM29" # Blue/Silver 232 Prolific
    # ser.port = "COM26"  # Blue/White 232
    # ser.port = "COM38" # Blue/Silver 232
    # ser.port = "COM10" # Blue/White 232

    # time.sleep(1)
    #
    # request_list = [PacketID.POSITION]
    #
    # gap = 3
    # ser.open()

    if ser.connect_tcp("localhost", 10000):
        ser.sendpacket(1, 1, 0.123)
        time.sleep(0.5)
        packets = ser.readdata()
        for packet in packets:
            print("packet received", ["0x%02x" % b for b in packet])

    while True:
        time.sleep(1)

    # ser.sendpacket(0x5, 0x01, 1)

    requests_lost = 0
    requests_made = 0
    test_begin_time = time.time()

    # request_packets = [PacketID.KM_CONFIGURATION]
    #
    # device_id = 5
    # packet_id = 0x60
    # data = bytearray(request_packets)
    # ser.sendpacket(device_id, packet_id, data)

    # device_id = 0xFF
    # packet_id = PacketID.SYSTEM_RESET
    # data = bytearray([0])
    # ser.sendpacket(device_id, packet_id, data)

    start = time.time()
    while start + 0.1 > time.time():
        packets = ser.readdata()
        for packet in packets:
            print("packet received", ["0x%02x" % b for b in packet])
            ser.parsepacket(packet)

    request_device_id = 5

    while(1):
        time.sleep(0.01)
        device_id = 0x1
        # packet_id = PacketID.MODE
        # ser.sendpacket(device_id, packet_id, Mode.VELOCITY_CONTROL)
        packet_id = PacketID.VELOCITY
        ser.sendpacket(device_id, packet_id, float(-0.))
        device_id = 0x2
        # packet_id = PacketID.MODE
        # ser.sendpacket(device_id, packet_id, Mode.VELOCITY_CONTROL)
        packet_id = PacketID.VELOCITY
        ser.sendpacket(device_id, packet_id, float(-0.0))
        # device_id = 0x3
        # packet_id = PacketID.MODE
        # ser.sendpacket(device_id, packet_id, Mode.VELOCITY_CONTROL)
        # packet_id = PacketID.VELOCITY
        # ser.sendpacket(device_id, packet_id, float(0.50))
        # device_id = 0x4
        # packet_id = PacketID.MODE
        # ser.sendpacket(device_id, packet_id, Mode.VELOCITY_CONTROL)
        # packet_id = PacketID.VELOCITY
        # ser.sendpacket(device_id, packet_id, float(0.0))
        device_id = 0x5
        # packet_id = PacketID.MODE
        # ser.sendpacket(device_id, packet_id, Mode.VELOCITY_CONTROL)
        packet_id = PacketID.KM_END_VEL
        data = [float(0.0), float(-1.0), float(0.0)]
        ser.sendpacket(device_id, packet_id, data)
        # packet_id = PacketID.VELOCITY
        # ser.sendpacket(device_id, packet_id, float(0.0))

        time.sleep(0.001)
        request_packets = [PacketID.MODE, PacketID.POSITION, PacketID.VELOCITY, PacketID.CURRENT]


        request_device_id += 1
        if request_device_id > 5:
            request_device_id = 1

        if request_device_id == 5:
            request_packets = [PacketID.MODE, PacketID.POSITION, PacketID.VELOCITY, PacketID.CURRENT, PacketID.KM_END_POS]
        device_id = request_device_id
        packet_id = 0x60
        data = bytearray(request_packets)
        requests_made += 1
        ser.sendpacket(device_id, packet_id, data)
        start_time = time.time()
        num_packets_received = 0

        while num_packets_received < len(request_packets):
            packets = ser.readdata()
            num_packets_received += len(packets)
            for packet in packets:
                print("packet received", ["0x%02x" % b for b in packet])
                ser.parsepacket(packet)

            if time.time() > start_time + 0.5:
                requests_lost += 1
                print("### Missed request from device id:", device_id, "Requests lost:", requests_lost, "/", requests_made, "\tMissed:", len(request_packets)-num_packets_received, ' / ', len(request_packets))
                print("### Average rate of missed requests:", 60 * requests_lost/(time.time() - test_begin_time), "/min")
                break

        end_time = time.time()
        if num_packets_received == len(request_packets):
            print("Time to receive all requests from device id:", device_id, "\t\ttime:", '{:3.3f} seconds'.format(end_time - start_time), "\t\tRequests lost:", requests_lost, "/", requests_made)

        # packets = ser.readdata()
        # for packet in packets:
        #     print("packet received", ["0x%02x" % b for b in packet])
        #     ser.parsepacket(packet)

# for i in range(6):
        #     ser.sendpacket(0xB0, 0x60, bytearray(request_list))
        #     time.sleep(gap)
        #     # some_key = input("press a key to continue")
        #     packets = ser.readdata()
        #     for packet in packets:
        #         print("packet received", ["0x%02x" % b for b in packet])
        #         ser.parsepacket(packet)

        # ser.sendpacket(1, 0x2, time.time())
        # time.sleep(gap)
        # packets = ser.readdata()
        # for packet in packets:
        #     print("packet received", packet)
        #     ser.parsepacket(packet)

        # ser.sendpacket(2, 0x60, bytearray(request_list))
        # time.sleep(gap)
        # packets = ser.readdata()
        # for packet in packets:
        #     ser.parsepacket(packet)
        #
        # ser.sendpacket(3, 0x60, bytearray(request_list))
        # time.sleep(gap)
        # packets = ser.readdata()
        # for packet in packets:
        #     ser.parsepacket(packet)
        #
        # ser.sendpacket(4, 0x60, bytearray(request_list))
        # time.sleep(gap)
        # packets = ser.readdata()
        # for packet in packets:
        #     ser.parsepacket(packet)
        #
        # ser.sendpacket(5, 0x60, bytearray(request_list))
        # time.sleep(gap)
        # packets = ser.readdata()
        # for packet in packets:
        #     ser.parsepacket(packet)


        # time.sleep(gap)
        # ser.serialPort.send_break()
        # ser.sendpacket(1, 0x2, float(1.0))
        #
        # # time.sleep(gap)
        # ser.sendpacket(2, 0x2, float(1.0))
        #
        # # time.sleep(gap)
        # ser.sendpacket(3, 0x2, float(1.0))
        #
        # # time.sleep(gap)
        # ser.sendpacket(4, 0x2, float(1.0))
        #
        # # time.sleep(gap)
        # ser.sendpacket(5, 0x2, float(1.0))
        #
        #
        # # time.sleep(gap)
        # ser.sendpacket(1, 0x2, float(0.0))
        # # time.sleep(gap)
        # ser.sendpacket(2, 0x2, float(0.0))
        # # time.sleep(gap)
        # ser.sendpacket(3, 0x2, float(0.0))
        # # time.sleep(gap)
        # ser.sendpacket(4, 0x2, float(0.0))
        # # time.sleep(gap)
        # ser.sendpacket(5, 0x2, float(0.0))
        #
