"""
This File will download config files from the bravo_excel_master.xlxs and generate configuration files from that.
"""
import datetime
import os
import urllib
import urllib.request
import webbrowser
from urllib.error import HTTPError

import browser_cookie3
import xlrd
import RC_Core.RS1_hardware, RC_Core.loadsave

SHEET_INDEX = 0  # The sheet index that contains the parameters.
HEADER_ROWS = 2  # Number of Header rows on the excel document

product_config_url_map = {"Alpha": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=ffc97d8a%2Db89f%2D4315%2Db3e0%2Dc2d5d18f18c7",
                          "Bravo": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=8c3e390d%2D5acb%2D4cae%2Db746%2Dcc91b9c1d477",
                          "Master Arm": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=a513ec31%2Dd561%2D4c4a%2Db8b7%2D23522d9765d5",
                          "Reach Tilt": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=a89a8daa%2Dafea%2D46c0%2D9982%2D6da261e14bc5"}

sharepoint_url = "https://theblueprintlab.sharepoint.com/sites/BlueprintLab"
destination_file = "config_file.xlsx"

output_directory = "../configfileoutput/"

HEADER_ROW = ["Packet Name", "Packet ID", "Parameter"]


def add_rev(filename_in, rev):
    name_list = filename_in.split(".")
    if len(name_list) > 1:
        name_list[-2] += rev
        filename_out = ".".join(name_list)
    else:
        filename_out = filename_in + rev
    return filename_out


if __name__ == '__main__':

    print("Config File generator: - This will download the config file from SharePoint, then generate the "
          "config files from it")

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dest_path = os.path.join(dir_path, destination_file)

    if os.path.exists(dest_path):
        print("Removing Existing .xlsx file")
        os.remove(dest_path)

    request_string = ""
    product_list = list(product_config_url_map.keys())
    for idx, product in enumerate(product_list):
        request_string += f"\t [{idx}]: {product}\n"

    invalid = False
    input_index = input(f"Which product would you like to generate config files for?\n{request_string} ")
    product_name = ""
    try:
        product_name = product_list[int(input_index)]
    except (TypeError, IndexError, ValueError):
        invalid = True
    while invalid:
        input_index = input(f"Please select from the following options:\n{request_string}")
        try:
            product_name = product_list[int(input_index)]
            invalid = False
        except (TypeError, IndexError, ValueError):
            invalid = True

    file_url = product_config_url_map[product_name]

    cj = browser_cookie3.load()
    opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))

    try:
        url_open = opener.open(file_url)
    except HTTPError as e:
        print("Error, please log into SharePoint through your default browser, and then retry running this script.")
        webbrowser.open(sharepoint_url)
        exit()

    download = url_open.read()

    print(f"Downloading xlxs file for {product_name}.")
    with open(dest_path, 'wb') as out_file:
        out_file.write(download)

    workbook = xlrd.open_workbook(dest_path)
    sheet = workbook.sheet_by_index(SHEET_INDEX)

    config_names = []
    header_row = []
    config_objects = []
    prev_packet_id = 0

    file_revision = input("Enter revision number or press enter use date stamp (_cfRevYYMMDD)")
    if not file_revision:
        today = datetime.datetime.today()
        file_revision = f"_cfRev{f'{today.year}'[2:]:2}{today.month:02}{today.day:02}"

    print("Generating Config Objects.")
    for rowx in range(sheet.nrows):
        row = sheet.row_values(rowx)
        if rowx < HEADER_ROWS-1:
            continue

        if rowx == HEADER_ROWS -1:

            header_row = row
            for i in range(len(HEADER_ROW), len(header_row)):
                file_name = add_rev(header_row[i], file_revision)
                config_names.append(file_name)
                config_objects.append([])
            # print(config_names)

        # values = sheet.row_values(rowx)
        else:
            packet_id = int(row[1], 16)  # 1 is the Packet ID Column.
            param_name = row[2]  # 2 is the Parameter Name Column
            for config_idx, value in enumerate(row[len(HEADER_ROW):]):
                if value is '':
                    value = None
                if packet_id == prev_packet_id:
                    packet_dict = config_objects[config_idx][-1]
                    if packet_id != packet_dict['id']:
                        raise Exception
                    packet_dict['values'][param_name] = value
                else:
                    packet_dict = {'id': packet_id,
                                   'name': RC_Core.RS1_hardware.get_name_of_packet_id(packet_id)}
                    if param_name == 'value':
                        packet_dict['values'] = value
                    else:
                        packet_dict['values'] = {param_name: value}
                    config_objects[config_idx].append(packet_dict)
            prev_packet_id = packet_id

    print("Writing Config Files.")
    for conf_index, conf in enumerate(config_objects):
        cwd = os.getcwd()
        file_path = os.path.join(dir_path,output_directory,config_names[conf_index])
        RC_Core.loadsave.save_settings_to_path(conf, file_path)
        # print(values)


