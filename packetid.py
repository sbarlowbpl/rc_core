import asyncio
import inspect
from copy import deepcopy
from typing import List, Union

# from RC_Core.callbackobject import TC_DEVICE_ID, TC_PACKET_ID, TC_DATA, TC_TIME, TC_CALLBACK, CallbackObject
from RC_Core import RS1_hardware, paramgroup
# from RC_Core.commconnection_methods import CommMethodGlobals
from RC_Core.callbackobject import CallbackObject
from RC_Core.enums.packets import PacketID

from RC_Core.paramgroup import ParamGroup


class PacketPrototype():
    ''' Packet Prototype
    Packet prototypes are bound to the selected packet id.
    Contains functions to send and receive data from the given packet id.
    '''
    packet_id = 0
    data_type = None
    uses_no_change = False
    data_comments = None
    instances = []

    def __init__(self, packet_id: int, data_type: type = None, data_names: List[str] = None,
                 alt_data_names: Union[List[str], list] = None, data_comments: List[str] = None,
                 default_values: list = None, uses_no_change: bool = False,
                 parameter_groups: ParamGroup = ParamGroup.NONE):
        '''
        Initialise packet prototype instance from packet id
        :param packet_id:
        :param data_type: float or int depending on the data the packet id uses
        :param data_names: list of name for each data paremeter
        :param alt_data_names: list of deprecated names for each data parameter if required
        :param data_comments: dispaly information for each parameter, used in Factory Config
        :param default_values: If packet does not require using all the paremeters, a default value can be set
        :param uses_no_change:
        '''
        if default_values is None:
            default_values = []
        if data_names is None:
            data_names = ['value']
        PacketPrototype.instances.append(self)
        self.rx_data: dict = {}
        self.data_names: List[str] = []
        self.alt_data_names: Union[List[str], None] = None
        self.packet_id: int = packet_id
        self.callbacks = []
        if data_type in [int, float]:
            self.data_type = data_type
        if isinstance(data_names, list):
            self.data_names = data_names
            self.alt_data_names = alt_data_names
        for d in self.data_names:
            self.rx_data[d] = None
        if default_values:
            self.default_values: list = default_values
        else:
            self.default_values: list = [None] * len(data_names)
        self.uses_no_change = uses_no_change
        if data_comments is None:
            self.data_comments: List[str] = None
        else:
            len_comments = len(data_names) if len(data_names) >= len(data_comments) else len(data_comments)
            self.data_comments: List[str] = data_comments[:len_comments]
        self.parameter_groups: ParamGroup = parameter_groups

    @property
    def data_length(self):
        '''
        :return: The maximum data length for the packet id
        '''
        return len(self.data_names)

    @property
    def name(self):
        '''
        :return:
        '''
        packets_ref = Packets()
        packetprototype_name = packets_ref.get_name_from_packetprototype(self)
        if packetprototype_name is not None:
            return packetprototype_name
        else:
            return RS1_hardware.get_name_of_packet_id(self.packet_id)

    @staticmethod
    def _send_filter(packet, device_id, packet_id):
        '''
        Filter duplicate send packets
        :param packet:
        :param device_id:
        :param callback:
        :return:
        '''
        if packet[0] == device_id and packet[1] == packet_id \
                and packet_id not in [PacketID.REQUEST, PacketID.DEVICE_ID]:
            return False
        else:
            return True

    def send(self, connection, device_id: int, data: Union[List[float], List[int]] = None):
        """
        :param device_id:
        :param list data: must include one item for each value in self.data_names
        :return:
        """
        if not connection:
            return

        data_to_send = []
        if self.data_type == None:
            data_to_send = 0
        else:
            if len(data) > self.data_length:
                # If the length of data to send is greater than the maximum packet's data length, raise error.
                raise Exception("Expected " + str(self.data_length) + " data items got " + str(len(data)))
            elif len(data) < self.data_length:
                # If the length of data is less than the maximum, add default values is possible other error.
                for i in range(len(data), len(self.default_values)):
                    if self.default_values[i] == None:
                        raise Exception("Cannot send NoneType packet")
                    data.append(self.default_values[i])

            # Convert data to int (8-bit) or float types.
            if self.data_type is float:
                data_to_send = []
                for d in data:
                    data_to_send.append(float(d))
            elif self.data_type is int:
                data_to_send = []
                for d in data:
                    data_to_send.append(int(d))
                data_to_send = bytearray(data_to_send)

        # If a duplicate request exists, remove it, and then add the new request
        if connection.connected:
            # packets_to_remove = []
            # for packet in connection.send_queue:
            #     if packet[TC_DEVICE_ID] == device_id and packet[TC_PACKET_ID] == self.packet_id \
            #             and self.packet_id not in [PacketID.REQUEST, PacketID.DEVICE_ID]:
            #         packets_to_remove.append(packet)
            # for packet in packets_to_remove:
            #     if packet in connection.send_queue:
            #         connection.send_queue.remove(packet)
            connection.send_queue = list(
                filter(lambda x: PacketPrototype._send_filter(x, device_id, self.packet_id), connection.send_queue))
            if [device_id, self.packet_id, data_to_send] \
                    not in connection.send_queue or self.packet_id is PacketID.REQUEST:
                connection.send_queue.append([device_id, self.packet_id, data_to_send])

    def receive(self, rx_data: Union[List[float], List[int]]):
        """
        Accepts list of data in the same order as self.data_names
        :param rx_data:
        :return:
        """
        for i, name in enumerate(self.data_names):
            if len(rx_data) > i:
                self.rx_data[name] = rx_data[i]
            else:
                break
        # Deal with callbacks, or delete if it cant be called
        for callback in self.callbacks:
            if callable(callback):
                callback(rx_data=self.rx_data)
            else:
                self.remove_callback(callback)

    def add_callback(self, callback):
        if callback not in self.callbacks:
            self.callbacks.append(callback)

    def remove_callback(self, callback):
        for callback_option in self.callbacks:
            if callback_option == callback:
                self.callbacks.remove(callback_option)

    @staticmethod
    def _request_filter(packet, device_id, packet_id, callback):
        '''
        Filter duplicate requests
        :param packet:
        :param device_id:
        :param callback:
        :return:
        '''
        if packet[0] == device_id and packet[1] == PacketID.REQUEST and \
                packet[2] == bytearray([packet_id]) and packet[3] == callback:
            return False
        else:
            return True

    def request(self, connection, device_id: int, callback=None,
                timeout: float = 0.5, timeout_callback=None, retry_counter: int = 0):
        '''
        Requests from the chosen device (from device id) the current data relating to the packet_id.
        If a target callback function is added, add the function to the list.
        :param connection: Target connection to send the request to
        :param device_id: Target device (by device id) to send the request to
        :param callback: should receive four arguments: self, device_id, packet_id, data
        :param timeout: Timeout to remove the callback from temporary callbacks list
        :param timeout_callback: If timeout reached, and alternate callback can be triggered.
        :param retry_counter: If needed, the request can be resent X times based on the timeout.
        :return:
        '''

        if callback is None:
            callback = self.request_callback
        from RC_Core.connectionmanager import ConnectionManager
        if not ConnectionManager.instance.run_connection_loop:


            return
        if self.data_type is not None:

            from RC_Core.commconnection_methods import CommMethodGlobals
            if CommMethodGlobals.BPSS_PROTOCOL:
                if connection and connection.connected:
                    # Create temporary callback object and add to temporary callback list of the connection
                    connection.temporary_callbacks.append(CallbackObject(connection,
                                                                         device_id,
                                                                         self.packet_id,
                                                                         callback, timeout=timeout,
                                                                         timeout_callback=timeout_callback,
                                                                         retry_counter=retry_counter))

                    # packets_to_remove = []
                    # for packet in connection.send_queue:
                    #     if packet[0] == device_id and packet[1] == PacketID.REQUEST and \
                    #             packet[2] == bytearray([self.packet_id]) and packet[3] == self.request_callback:
                    #         packets_to_remove.append(packet)
                    # for packet in packets_to_remove:
                    #     if packet in connection.send_queue:
                    #         connection.send_queue.remove(packet)
                    connection.send_queue = list(
                        filter(lambda x: PacketPrototype._request_filter(x, device_id, self.packet_id, callback),
                               connection.send_queue))
                    if [device_id, PacketID.REQUEST, bytearray([self.packet_id]), self.request_callback] \
                            not in connection.send_queue or self.packet_id is PacketID.REQUEST:
                        connection.send_queue.append(
                            [device_id, PacketID.REQUEST, bytearray([self.packet_id]), self.request_callback])
                return

            asyncio.create_task(self.async_request(connection, device_id, callback, timeout, timeout_callback, retry_counter))
            # if connection and connection.connected:
            #     # Create temporary callback object and add to temporary callback list of the connection
            #     connection.temporary_callbacks.append(CallbackObject(connection,
            #                                                          device_id,
            #                                                          self.packet_id,
            #                                                          callback, timeout=timeout,
            #                                                          timeout_callback=timeout_callback,
            #                                                          retry_counter=retry_counter))
            #
            #     # packets_to_remove = []
            #     # for packet in connection.send_queue:
            #     #     if packet[0] == device_id and packet[1] == PacketID.REQUEST and \
            #     #             packet[2] == bytearray([self.packet_id]) and packet[3] == self.request_callback:
            #     #         packets_to_remove.append(packet)
            #     # for packet in packets_to_remove:
            #     #     if packet in connection.send_queue:
            #     #         connection.send_queue.remove(packet)
            #     connection.send_queue = list(
            #         filter(lambda x: PacketPrototype._request_filter(x, device_id, self.packet_id, callback),
            #                connection.send_queue))
            #     if [device_id, PacketID.REQUEST, bytearray([self.packet_id]), self.request_callback] \
            #             not in connection.send_queue or self.packet_id is PacketID.REQUEST:
            #         connection.send_queue.append(
            #             [device_id, PacketID.REQUEST, bytearray([self.packet_id]), self.request_callback])

    async def async_request(self, connection, device_id: int, callback=None,
                timeout: float = 0.5, timeout_callback=None, retry_counter: int = 0):

        # if CommMethodGlobals.BPSS_PROTOCOL:
        #     # self.connection.send
        #     connection.send(device_id, PacketID.REQUEST, [self.packet_id])


            # data = encode_packet(device_id, PacketID.REQUEST, [packet_id])
            # self.send_bytes(data)
            # return


        if connection is None or not connection.connected:
            return

        for i in range(retry_counter+1):
            response = None
            try:
                response = await connection.request(device_id, self.packet_id, timeout=timeout)
                if callback is not None and callable(callback):
                    callback(device_id, self.packet_id, response)

                    return
            except TimeoutError:
                continue

        if timeout_callback is not None and callable(timeout_callback):
            timeout_callback()
        return


    def request_callback(self, device_id, packet_id, data):
        if packet_id == self.packet_id:
            # print(__name__, 'request_callback() device_id, packet_id, data', device_id, packet_id, data)
            self.receive(data)


    def set_permanent_callback(self, connection, device_id, callback):
        '''
        Used to set a permanent callback
        :param connection:
        :param device_id:
        :param callback: callback target function to add to the list
        :return:
        '''

        if connection is not None:
            if connection:
                connection.add_packet_callback(device_id, self.packet_id, callback)

        from RC_Core.commconnection_methods import CommMethodGlobals
        if CommMethodGlobals.BPSS_PROTOCOL:
            previously_set = False
            for callback_object in connection.permanent_callbacks:
                if callback_object.device_id == device_id and callback_object.packet_id == self.packet_id and \
                        callback_object.callback == callback:
                    previously_set = True
                    break
            if not previously_set:
                connection.permanent_callbacks.append(CallbackObject(connection, device_id, self.packet_id, callback))

    def remove_permanent_callback(self, connection, device_id, callback):
        '''
        Used to remove a permanent callback that has been set
        :param connection:
        :param device_id:
        :param callback: callback target function to remove from the list
        :return:
        '''

        if connection is not None:
            if connection:
                connection.remove_packet_callback(device_id, self.packet_id, callback)
        from RC_Core.commconnection_methods import CommMethodGlobals
        if CommMethodGlobals.BPSS_PROTOCOL:
            try:
                for callback_object in connection.permanent_callbacks:
                    if callback_object.device_id == device_id and callback_object.packet_id == self.packet_id and \
                            callback_object.callback == callback:
                        connection.permanent_callbacks.remove(callback_object)
            except:
                print(__name__, 'remove_permanent_callback():', 'Failed to remove callback!')
                pass

    def test_data_matches_rx(self, tx_data, rx_data=None):
        if rx_data is None:
            rx_data = self.rx_data
        try:
            if len(tx_data) != len(rx_data):
                return False
        except:
            return False
        if self.uses_no_change:
            for data_name, tx_value in tx_data.items():
                if tx_value == RS1_hardware.NCValue.NO_CHANGE:
                    pass
                else:
                    rx_value = rx_data[data_name]
                    if rx_value == tx_value:
                        pass
                    else:
                        return False
            return True
        else:
            for data_name, tx_value in tx_data.items():
                rx_value = rx_data[data_name]
                if rx_value == tx_value:
                    pass
                else:
                    return False
            return True

    def reset_rx_data(self):
        for dname in self.data_names:
            self.rx_data[dname] = None

    def is_in_group(self, group: ParamGroup):
        return paramgroup.is_in_group(self.parameter_groups, group)

class Packets:
    ''' Packets Class
    List of all Packet Prototype classes based on all packet ids.
    '''

    def __init__(self):
        '''
        Make deepcopy of each PacketPrototype instance in this class
        to avoid instances of Packets class sharing rx_values dicts.
        '''
        members = inspect.getmembers(Packets)
        self.packetprototypes_attributelist = []
        for key, value in members:
            if isinstance(value, PacketPrototype):
                self.__dict__[key] = deepcopy(value)

        self.total_packets_list = []
        self.common_packets_list = []

        self.name_packet_prototype_dict = {}

        attrlist = inspect.getmembers(self)
        for member in attrlist:
            if isinstance(member[1], PacketPrototype):
                self.packetprototypes_attributelist.append(member)
                self.total_packets_list.append(member[1])
                self.name_packet_prototype_dict[member[0]] = member[1]


                if member[1] in [self.VELOCITY, self.POSITION, self.CURRENT,
                                 self.RELATIVE_POSITION, self.INDEXED_POSITION]:
                    self.common_packets_list.append(member[1])

    def get_by_id(self, packet_id):
        '''
        Returns the Packet Prototype class for the given packet id
        :param packet_id:
        :return: Packet Prototype of the input packet id
        '''
        for p in self.common_packets_list:
            if p.packet_id == packet_id:
                return p
        for p in self.total_packets_list:
            if p.packet_id == packet_id:
                return p
        else:
            return None

    def get_all(self):
        '''
        Returns all packet prototypes for every packet id
        :return:
        '''
        return self.total_packets_list

    def get_name_from_packetprototype(self, packetprototype: PacketPrototype):
        '''
        Returns packetprototype's name
        :return:
        '''
        for attr in self.packetprototypes_attributelist:
            name = attr[0]
            packetprototyperef = attr[1]
            if packetprototype.packet_id == packetprototyperef.packet_id:
                return name
        return None

    def get_float_packet_ids(self):
        '''
        Returns all packet prototypes that has the data type as a float
        :return:
        '''
        all_members = self.get_all()
        float_packet_id_list = []
        for member in all_members:
            if member.data_type == float:
                float_packet_id_list.append(member.packet_id)
        return float_packet_id_list

    MODE = PacketPrototype(PacketID.MODE, data_type=int, data_comments=['Axis Mode: Position, Velocity, etc...'],
                           parameter_groups=ParamGroup.CONTROLS | ParamGroup.STATUS)

    VELOCITY = PacketPrototype(PacketID.VELOCITY, data_type=float, data_comments=['Axis Velocity (radians/s)'],
                               parameter_groups=ParamGroup.CONTROLS | ParamGroup.GAINS | ParamGroup.LIMITS)
    POSITION = PacketPrototype(PacketID.POSITION, data_type=float, data_comments=['Axis Position (radians)'],
                               parameter_groups=ParamGroup.CONTROLS | ParamGroup.LIMITS | ParamGroup.GAINS)
    OPENLOOP = PacketPrototype(PacketID.OPENLOOP, data_type=float, data_comments=['Axis Openloop Control (radians/s)'],
                               parameter_groups=ParamGroup.CONTROLS)
    CURRENT = PacketPrototype(PacketID.CURRENT, data_type=float, data_comments=['Axis Current (mA)'],
                              parameter_groups=ParamGroup.CONTROLS | ParamGroup.GAINS | ParamGroup.LIMITS)

    POSITION_VELOCITY_DEMAND = PacketPrototype(PacketID.POSITION_VELOCITY_DEMAND, data_type=float,
                                               data_names=["position", "velocity"],
                                               parameter_groups=ParamGroup.CONTROLS)
    ABS_POSITION_VELOCITY_DEMAND = PacketPrototype(PacketID.ABS_POSITION_VELOCITY_DEMAND, data_type=float,
                                                   data_names=["position", "velocity"],
                                                   parameter_groups=ParamGroup.NONE)
    RELATIVE_POSITION = PacketPrototype(PacketID.RELATIVE_POSITION, data_type=float,
                                        parameter_groups=ParamGroup.CONTROLS)
    INDEXED_POSITION = PacketPrototype(PacketID.INDEXED_POSITION, data_type=float,
                                       parameter_groups=ParamGroup.CONTROLS)

    INDEXED_POSITION_OFFSET = PacketPrototype(PacketID.INDEXED_POSITION_OFFSET, data_type=float,
                                              parameter_groups=ParamGroup.NONE)

    DEMAND_TIMEOUT = PacketPrototype(PacketID.DEMAND_TIMEOUT,
                                     data_type=float,
                                     data_comments=["Timeout for use of the POSITION_VELOCITY_DEMAND in ms"],
                                     parameter_groups=ParamGroup.BRAVO)

    POWER = PacketPrototype(PacketID.POWER, data_type=float, data_comments=['Power draw in W'],
                            parameter_groups=ParamGroup.STATUS)
    SUPPLY_VOLTAGE = PacketPrototype(PacketID.SUPPLY_VOLTAGE, data_type=float,
                                     parameter_groups=ParamGroup.STATUS)
    REQUEST = PacketPrototype(PacketID.REQUEST, data_type=int,
                              data_names=["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
                              parameter_groups=ParamGroup.NONE)
    SERIAL_NUMBER = PacketPrototype(PacketID.SERIAL_NUMBER, data_type=float,
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.SETTINGS | ParamGroup.BRAVO |
                                                     ParamGroup.MA_ROTATE |
                                                     ParamGroup.MA_END |
                                                     ParamGroup.MA_BASE)
    MODEL_NUMBER = PacketPrototype(PacketID.MODEL_NUMBER, data_type=float,
                                   parameter_groups=ParamGroup.ALPHA | ParamGroup.SETTINGS | ParamGroup.BRAVO |
                                                    ParamGroup.MA_ROTATE |
                                                    ParamGroup.MA_END |
                                                    ParamGroup.MA_BASE)
    VERSION = PacketPrototype(PacketID.VERSION, data_type=float,
                              parameter_groups=ParamGroup.STATUS)
    DEVICE_ID_FOR_SERIAL_NUMBER = PacketPrototype(PacketID.DEVICE_ID_FOR_SERIAL_NUMBER, data_type=int,
                                                  data_names=['device_id', 'serial_a', 'serial_b',
                                                              'serial_c', 'serial_d'],
                                                  data_comments=['Device ID to change to. Device ID will only change'
                                                                 'for board with matching serial number.',
                                                                 'Serial number 1000s digit.'
                                                                 '\nDevice ID will only change'
                                                                 'for board with matching serial number.',
                                                                 'Serial number 100s digit'
                                                                 '\nDevice ID will only change'
                                                                 'for board with matching serial number.',
                                                                 'Serial number 10s digit'
                                                                 '\nDevice ID will only change'
                                                                 'for board with matching serial number.',
                                                                 'Serial number 1s digit'
                                                                 '\nDevice ID will only change'
                                                                 'for board with matching serial number.'],
                                                  parameter_groups=ParamGroup.NONE)
    DEVICE_ID = PacketPrototype(PacketID.DEVICE_ID, data_type=int, data_comments=['Axis Device ID'],
                                parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO |
                                                 ParamGroup.MA_ROTATE |
                                                 ParamGroup.MA_END |
                                                 ParamGroup.MA_BASE | ParamGroup.SETTINGS)
    DEVICE_ID_FOR_TIMESTAMP = PacketPrototype(PacketID.DEVICE_ID_FOR_TIMESTAMP, data_type=int,
                                              data_names=['device_id', 'a', 'b',
                                                          'c', 'd'],
                                              data_comments=['Device ID to change to. Device ID will only change'
                                                             'for board with matching timestamp. (Big-endian)',
                                                             'Timestamp a'
                                                             '\nDevice ID will only change'
                                                             'for board with matching timestamp.',
                                                             'Timestamp b'
                                                             '\nDevice ID will only change'
                                                             'for board with matching timestamp.',
                                                             'Timestamp c'
                                                             '\nDevice ID will only change'
                                                             'for board with matching timestamp.',
                                                             'Timestamp d'
                                                             '\nDevice ID will only change'
                                                             'for board with matching timestamp.'],
                                              parameter_groups=ParamGroup.NONE)
    INTERNAL_HUMIDITY = PacketPrototype(PacketID.INTERNAL_HUMIDITY, data_type=float,
                                        data_comments=['Humidity in %'],
                                        parameter_groups=ParamGroup.STATUS)
    INTERNAL_TEMPERATURE = PacketPrototype(PacketID.INTERNAL_TEMPERATURE, data_type=float,
                                           data_comments=['Temperature in degrees Celsius'],
                                           parameter_groups=ParamGroup.STATUS)
    INTERNAL_PRESSURE = PacketPrototype(PacketID.INTERNAL_PRESSURE, data_type=float,
                                        data_comments=['Pressure in Bar'],
                                        parameter_groups=ParamGroup.STATUS)

    FACTORY_CLIMATE = PacketPrototype(PacketID.FACTORY_CLIMATE, data_type=float,
                                      data_names=['temperature', 'pressure', 'humidity'],
                                      data_comments=['temperature reading at completion of assembly',
                                                     'pressure reading at completion of assembly',
                                                     'humidity reading at completion of assembly'],
                                      parameter_groups=ParamGroup.STATUS | ParamGroup.PARAMS)

    CLIMATE_PARAMETERS = PacketPrototype(PacketID.CLIMATE_PARAMETERS, data_type=float,
                                         data_names=['k_margin', 'unused_1', 'unused_2', 'unused_3'],
                                         data_comments=['Margin of pressure/temperature '
                                                        'before declaring a pressure leak',
                                                        'not used', 'not used', 'not used', ],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)

    DEVICE_TYPE = PacketPrototype(PacketID.DEVICE_TYPE, data_type=int,
                                  parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO |
                                                   ParamGroup.MA_ROTATE | ParamGroup.MA_END | ParamGroup.SETTINGS)
    HARDWARE_STATUS = PacketPrototype(PacketID.HARDWARE_STATUS, data_type=int,
                                      data_names=['a', 'b', 'c', 'd'],
                                      parameter_groups=ParamGroup.STATUS)
    MASTER_ARM_PREFIX = PacketPrototype(PacketID.MASTER_ARM_PREFIX, data_type=int,
                                        data_comments=["For Alpha and Bravo products.\n"
                                                       "Master Arm device ID prefix to respond to.\n"
                                                       "(Default: 0xC0 = 192)"],
                                        parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)
    DEFAULT_OPERATING_MODE = PacketPrototype(PacketID.DEFAULT_OPERATING_MODE, data_type=int,
                                             data_comments=["Master Arm. Mode to enter when leaving pause state.\n"
                                                            "Send to base board only.\n"
                                                            "Modes: POSITION, VELOCITY, RELATIVE_POSITION.\n"
                                                            "Auto saves on send."],
                                             parameter_groups=ParamGroup.MA_BASE | ParamGroup.BRAVO)
    RUN_TIME = PacketPrototype(PacketID.RUN_TIME, data_type=float, data_names=['session_time_s', 'life_time_h'],
                               data_comments=["Number of seconds elapsed since startup",
                                              "Number of hours elapsed since manufacture"],
                               parameter_groups=ParamGroup.NONE)

    COMS_PROTOCOL = PacketPrototype(PacketID.COMS_PROTOCOL, data_type=int,
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)

    HEARTBEAT_FREQUENCY_SET = PacketPrototype(PacketID.HEARTBEAT_FREQUENCY_SET, data_type=int,
                                              data_comments=['Axis Heartbeat Frequency (Hz)'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.SETTINGS)
    HEARTBEAT_SET = PacketPrototype(PacketID.HEARTBEAT_SET, data_type=int,
                                    data_names=["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
                                    parameter_groups=ParamGroup.NONE)

    HEARTBEAT_INT_FREQUENCY_SET = PacketPrototype(PacketID.HEARTBEAT_INT_FREQUENCY_SET, data_type=int,
                                                  data_comments=['Axis Internal Heartbeat Frequency (Hz)'],
                                                  parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.SETTINGS)
    HEARTBEAT_INT_SET = PacketPrototype(PacketID.HEARTBEAT_INT_SET, data_type=int,
                                        data_names=["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
                                        parameter_groups=ParamGroup.NONE)

    SAVE = PacketPrototype(PacketID.SAVE, data_type=int,
                           parameter_groups=ParamGroup.NONE)
    LOAD = PacketPrototype(PacketID.LOAD, data_type=None,
                           parameter_groups=ParamGroup.NONE)
    SET_DEFAULTS = PacketPrototype(PacketID.SET_DEFAULTS, data_type=None,
                                   parameter_groups=ParamGroup.NONE)
    FORMAT = PacketPrototype(PacketID.FORMAT, data_type=None,
                             parameter_groups=ParamGroup.NONE)
    CHANGE_PAGE = PacketPrototype(PacketID.CHANGE_PAGE, data_type=None,
                                  parameter_groups=ParamGroup.NONE)

    CURRENT_LIMIT = PacketPrototype(PacketID.CURRENT_LIMIT, data_type=float, data_names=['max', 'min'],
                                    data_comments=['Maximum Current (mA)', 'Minimum Current (mA)'],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.LIMITS)
    VELOCITY_LIMIT = PacketPrototype(PacketID.VELOCITY_LIMIT, data_type=float, data_names=['max', 'min'],
                                     parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.LIMITS)
    POSITION_LIMIT = PacketPrototype(PacketID.POSITION_LIMIT, data_type=float, data_names=['max', 'min'],
                                     parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.LIMITS)

    CURRENT_LIMIT_FACTORY = PacketPrototype(PacketID.CURRENT_LIMIT_FACTORY, data_type=float, data_names=['max', 'min'],
                                            data_comments=['Factory Limit. Maximum Current (mA)',
                                                           'Factory Limit. Minimum Current (mA)'],
                                            parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.LIMITS)
    VELOCITY_LIMIT_FACTORY = PacketPrototype(PacketID.VELOCITY_LIMIT_FACTORY, data_type=float,
                                             data_names=['max', 'min'],
                                             data_comments=['Factory Limit', 'Factory Limit'],
                                             parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.LIMITS)
    POSITION_LIMIT_FACTORY = PacketPrototype(PacketID.POSITION_LIMIT_FACTORY, data_type=float,
                                             data_names=['max', 'min'],
                                             data_comments=['Factory Limit', 'Factory Limit'],
                                             parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.LIMITS)
    VELOCITY_LIMIT_INNER_FACTORY = PacketPrototype(PacketID.VELOCITY_LIMIT_INNER_FACTORY, data_type=float,
                                                   data_names=['max', 'min'],
                                                   data_comments=['Factory Limit', 'Factory Limit'],
                                                   parameter_groups=ParamGroup.BRAVO | ParamGroup.LIMITS)
    POSITION_GAIN = PacketPrototype(PacketID.POSITION_GAIN, data_type=float,
                                    data_names=['KP', 'KI', 'KD', 'KF', 'MI', 'KO'],
                                    default_values=[None, None, None, None, None, 1],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.GAINS)
    VELOCITY_GAIN = PacketPrototype(PacketID.VELOCITY_GAIN, data_type=float,
                                    data_names=['KP', 'KI', 'KD', 'KF', 'MI', 'KO'],
                                    default_values=[None, None, None, None, None, 1],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.GAINS)
    CURRENT_GAIN = PacketPrototype(PacketID.CURRENT_GAIN, data_type=float,
                                   data_names=['KP', 'KI', 'KD', 'KF', 'MI', 'KO'],
                                   default_values=[None, None, None, None, None, 1],
                                   parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.GAINS)

    AUTO_LIMIT_CURRENT_DEMAND = PacketPrototype(PacketID.AUTO_LIMIT_CURRENT_DEMAND, data_type=float,
                                                parameter_groups=ParamGroup.CONTROLS)
    AUTO_LIMIT_DEMAND = PacketPrototype(PacketID.AUTO_LIMIT_DEMAND, data_type=float,
                                        data_names=['current', 'velocity'],
                                        data_comments=['Max current during auto limit',
                                                       'Max velocity during auto limit'],
                                        parameter_groups=ParamGroup.NONE)
    AUTO_LIMIT_AT_STARTUP = PacketPrototype(PacketID.AUTO_LIMIT_AT_STARTUP, data_type=float,
                                            data_names=['current', 'velocity', 'delay'],
                                            data_comments=['Max current during auto limit',
                                                           'Max velocity during auto limit',
                                                           'Duration in seconds after startup before'
                                                           'starting auto limit.'],
                                            parameter_groups=ParamGroup.BRAVO)

    POSITION_PARAMETERS = PacketPrototype(PacketID.POSITION_PARAMETERS, data_type=float,
                                          data_names=['scale', 'offset', 'alpha', 'beta', 'max_delta'],
                                          data_comments=['', '', '', '',
                                                         'maximum change in position reading before\n'
                                                         'flagging a POSITION_INVALID error.'],
                                          default_values=[None, None, None, None, 0.1],
                                          parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO |
                                                           ParamGroup.PARAMS | ParamGroup.MA_ROTATE)
    VELOCITY_PARAMETERS = PacketPrototype(PacketID.VELOCITY_PARAMETERS, data_type=float,
                                          data_names=['scale', 'offset', 'alpha', 'beta'],
                                          parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)
    ADAPTIVE_PARAMETERS = PacketPrototype(PacketID.ADAPTIVE_PARAMETERS, data_type=float,
                                          data_names=['enabled', 'alpha', 'beta', 'spare1', 'spare2', 'spare3',
                                                      'spare4',
                                                      'spare5'],
                                          parameter_groups=ParamGroup.ALPHA | ParamGroup.PARAMS)
    CURRENT_PARAMETERS = PacketPrototype(PacketID.CURRENT_PARAMETERS, data_type=float,
                                         data_names=['scale', 'offset', 'alpha', 'beta'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)
    INPUT_VOLTAGE_PARAMETERS = PacketPrototype(PacketID.INPUT_VOLTAGE_PARAMETERS, data_type=float,
                                               data_names=['scale', 'offset', 'alpha', 'beta'],
                                               parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)
    OVERDRIVE_ENABLE = PacketPrototype(PacketID.OVERDRIVE_ENABLE, data_type=int,
                                       data_names=['enable'],
                                       parameter_groups=ParamGroup.BRAVO |
                                                        ParamGroup.PARAMS)
    OVERDRIVE_PARAMETERS = PacketPrototype(PacketID.OVERDRIVE_PARAMETERS, data_type=float,
                                           data_names=['scale', 'start_temp_limit', 'max_temp_limit', 'alpha'],
                                           data_comments=['scalar applied to currentControl maxSetpoint and minSetpoint'
                                                          'Value of 1.0 provides no overdrive. '
                                                          '1.2 gives 20% overdrive.',
                                                          'temperature threshold to start ramping down overdrive',
                                                          'temperature threshold to end ramping down overdrive.\n'
                                                          'At this temp, overdrive has no effect.',
                                                          'For low pass filter in overdrive temp check. '
                                                          'Not used in RS1 V4.14.'],
                                           parameter_groups=ParamGroup.BRAVO | ParamGroup.PARAMS)
    MOTOR_PARAMETERS = PacketPrototype(PacketID.MOTOR_PARAMETERS, data_type=float,
                                       data_names=['voltage', 'current', 'resistance', 'direction',
                                                   'inductance', 'pole_pairs'],
                                       data_comments=['',
                                                      '',
                                                      '',
                                                      '',
                                                      'Max change in motor PWM output per step\n'
                                                      'Value in range 0.0 - 1.0',
                                                      'Motor Inductance'
                                                      'Number of pole pairs in the motor'],
                                       default_values=[None, None, None, None, 0.1, 0.01],
                                       parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)
    MOTOR_OFFSET_ANGLE = PacketPrototype(PacketID.MOTOR_OFFSET_ANGLE, data_type=float,
                                         data_comments=['Motor electrical offset angle'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.MA_ROTATE)
    OPENLOOP_PARAMETERS = PacketPrototype(PacketID.OPENLOOP_PARAMETERS, data_type=float,
                                          data_names=['max_output_delta', 'overshoot_limit'],
                                          data_comments=['Max change in motor PWM output per step\n'
                                                         'Value in range 0.0 - 1.0',
                                                         'In radians or mm. Position overshoot amount where motor '
                                                         'output\nis zero. Output ramps down from pos limit to '
                                                         'this point.'],
                                          parameter_groups=ParamGroup.PARAMS | ParamGroup.BRAVO | ParamGroup.ALPHA)

    MOTOR_PARAMETERS_2 = PacketPrototype(PacketID.MOTOR_PARAMETERS_2, data_type=float,
                                         data_names=['voltage', 'current', 'resistance', 'direction',
                                                     'inductance', 'pole_pairs', 'inertia', 'damping', 'friction', 'kt',
                                                     'kv', 'gear_ratio'],
                                         data_comments=['',
                                                        '',
                                                        '',
                                                        '',
                                                        'Motor Inductance',
                                                        'Number of pole pairs in the motor',
                                                        'Motor inertia [Kg m^2]',
                                                        'Motor damping [Ns/m] (read only)',
                                                        'Motor friction [N] (read only)',
                                                        'Motor torque constant [Nm/A]\n'
                                                        'Gradient of the torque/current curve',
                                                        'RPM when 1V is applied to the motor',
                                                        'Gear box input/output relationship'],
                                         default_values=[None, None, None, None, None, None,
                                                         None, None, None, None, None, None],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)

    BACKUP_PARAMETERS = PacketPrototype(PacketID.BACKUP_PARAMETERS, data_type=float,
                                        data_names=['friction', 'damping', 'spare1', 'spare2',
                                                    'spare3', 'spare4', 'spare5', 'spare6', 'spare7', 'spare8',
                                                    ],
                                        data_comments=['Backed up motor friction',
                                                        'Backed up motor damping',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        ''],
                                        parameter_groups=ParamGroup.NONE)

    ACCELERATION_PARAMETERS = PacketPrototype(PacketID.ACCELERATION_PARAMETERS, data_type=float,
                                              data_names=['inner_alpha', 'outer_alpha'],
                                              data_comments=['Inner acceleration scaling factor',
                                                             'Outer acceleration scaling factor'],
                                              default_values=[None, None],
                                              parameter_groups=ParamGroup.BRAVO)

    TORQUE_LIMIT = PacketPrototype(PacketID.TORQUE_LIMIT, data_type=float,
                                   data_names=['max', 'min'],
                                   data_comments=['Maximum torque limit [N/m]',
                                                  'Minimum torque limit [N/m]'],
                                   default_values=[None, None],
                                   parameter_groups=ParamGroup.BRAVO | ParamGroup.LIMITS)

    TORQUE_OUTPUT = PacketPrototype(PacketID.TORQUE_OUTPUT, data_type=float,
                                    data_comments=['Output torque [N/m]'],
                                    default_values=[None],
                                    parameter_groups=ParamGroup.CONTROLS)

    IMU_READING = PacketPrototype(PacketID.IMU_READING, data_type=float,
                                  data_names=['acc_x', 'acc_y', 'acc_z',
                                              'gyro_x', 'gyro_y', 'gyro_z',
                                              'mag_x', 'mag_y', 'mag_z'],
                                  data_comments=['mm/s^2',
                                                 'mm/s^2',
                                                 'mm/s^2',
                                                 'rad/s',
                                                 'rad/s',
                                                 'rad/s',
                                                 'uT',
                                                 'uT',
                                                 'uT'])

    IMU_GRAVITY = PacketPrototype(PacketID.IMU_GRAVITY, data_type=float,
                                  data_names=['x', 'y', 'z'],
                                  data_comments=['Normalised vector for direction of gravity'])

    TEMPERATURE_PARAMETERS = PacketPrototype(PacketID.TEMPERATURE_PARAMETERS, data_type=float,
                                             data_names=['max', 'min', 'scale', 'alpha'],
                                             data_comments=['max temperature before current output scaling',
                                                            'min temperature. Not used V4.14.',
                                                            'Scale applied to current limit when over temperature. '
                                                            'Not used V4.14.',
                                                            'Temperature measurement low pass filter alpha'],
                                             parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)

    ETH_IP_ADDRESS = PacketPrototype(PacketID.ETH_IP_ADDRESS, data_type=int,
                                     data_names=['a', 'b', 'c', 'd'],
                                     data_comments=['Four bytes of IP address.\n'
                                                    'E.G. For 192.168.2.4 set a=192 b=168 c=2 d=4'],
                                     parameter_groups=ParamGroup.BRAVO)
    ETH_PORT = PacketPrototype(PacketID.ETH_PORT, data_type=float,
                               data_comments=['UDP port number'],
                               parameter_groups=ParamGroup.BRAVO)

    MAX_ACCELERATION = PacketPrototype(PacketID.MAX_ACCELERATION, data_type=float,
                                       parameter_groups=ParamGroup.PARAMS | ParamGroup.BRAVO | ParamGroup.ALPHA)
    CURRENT_HOLD_THRESHOLD = PacketPrototype(PacketID.CURRENT_HOLD_THRESHOLD, data_type=float,
                                             parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)
    CURRENT_HOLD_PARAMETERS = PacketPrototype(PacketID.CURRENT_HOLD_PARAMETERS, data_type=float,
                                              data_names=['threshold', 'alpha'],
                                              data_comments=['Current threshold above which current setpoint is\n'
                                                             'maintained until velocity demand changes direction',
                                                             'Low pass filter on current measurement for current hold'],
                                              parameter_groups=ParamGroup.PARAMS)
    COMPLIANCE_GAIN = PacketPrototype(PacketID.COMPLIANCE_GAIN, data_type=float,
                                      parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.GAINS)
    COMPLIANCE_PARAMETERS = PacketPrototype(PacketID.COMPLIANCE_PARAMETERS, data_type=float,
                                            data_names=['compliance_gain', 'stop_compliance_factor',
                                                        'trigger_alpha', 'beta'],
                                            alt_data_names=[['wind_down_alpha'], ['backdrive_efficiency'],
                                                            ['alpha'], []],
                                            data_comments=['Compliance Gain. How far to move when in compliance.\n '
                                                           'How easily to move with current.',
                                                           'Stop Compliance Factor. Fraction of factory current limit\n'
                                                           'to fall to before leaving compliance.',
                                                           'Deprecated RS1-V4.14+',
                                                           'Deprecated RS1-V4.14+'],
                                            parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.PARAMS)

    # 3 bytes for each version packet
    # [major, submaj, minor]
    ELECTRICAL_VERSION = PacketPrototype(PacketID.ELECTRICAL_VERSION, data_type=int,
                                         data_names=['major', 'submajor', 'min'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.SETTINGS | ParamGroup.BRAVO |
                                                          ParamGroup.MA_ROTATE |
                                                          ParamGroup.MA_END |
                                                          ParamGroup.MA_BASE)
    MECHANICAL_VERSION = PacketPrototype(PacketID.MECHANICAL_VERSION, data_type=int,
                                         data_names=['major', 'submajor', 'min'],
                                         parameter_groups=ParamGroup.SETTINGS |
                                                          ParamGroup.ALPHA |
                                                          ParamGroup.BRAVO |
                                                          ParamGroup.MA_ROTATE |
                                                          ParamGroup.MA_END |
                                                          ParamGroup.MA_BASE)
    SOFTWARE_VERSION = PacketPrototype(PacketID.SOFTWARE_VERSION, data_type=int,
                                       data_names=['major', 'submajor', 'min'],
                                       parameter_groups=ParamGroup.ALPHA |
                                                        ParamGroup.BRAVO |
                                                        ParamGroup.MA_ROTATE |
                                                        ParamGroup.MA_END |
                                                        ParamGroup.MA_BASE |
                                                        ParamGroup.SETTINGS)

    LED_INDICATOR_DEMAND = PacketPrototype(PacketID.LED_INDICATOR_DEMAND, data_type=int, data_names=['index', 'state'],
                                           data_comments=["Master Arm. Index: 0 = Pause, 1 = Stow, 2 = Deploy.\n"
                                                          "State: 0 = OFF, 1 = ON, 2 = FLASH_FAST, 3 = FLASH_SLOW,\n"
                                                          "4 = LATCH, 5 = MOMENTARY, 6 = MOMENTARY_FLASH_ON,\n"
                                                          "7 = MOMENTARY_FLASH_OFF."],
                                           parameter_groups=ParamGroup.NONE)

    BOOTLOADER = PacketPrototype(PacketID.BOOTLOADER, data_type=None,
                                 parameter_groups=ParamGroup.NONE)
    BOOTLOADER_BATCH = PacketPrototype(PacketID.BOOTLOADER_BATCH, data_type=int,
                                       data_names=['board_num_0', 'board_num_1', 'board_num_2',
                                                   'firmware_0', 'firmware_1', 'firmware_2',
                                                   'device_id_start', 'device_id_end',
                                                   'operation', 'progress_status'],
                                       data_comments=['Board part number digits',
                                                      'Board part number digits',
                                                      'Board part number digits',
                                                      'Firmware version number digits',
                                                      'Firmware version number digits',
                                                      'Firmware version number digits',
                                                      'First device id in sequence',
                                                      'Last device id in sequence',
                                                      'Operation code. 0: stop. 1: write. 2: verify.',
                                                      'Progress/status. 0-100 % progress. 101-255 status/error codes.'],
                                       parameter_groups=ParamGroup.NONE)
    BOOT_BATCH_REPORT = PacketPrototype(PacketID.BOOT_BATCH_REPORT, data_type=int,
                                        data_names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'],
                                        data_comments=['Status/progress of each board during bootloader batch '
                                                       'operation. Fields in this packet do not correspond to set '
                                                       'device ids. '
                                                       '\nRather, if BOOTLOADER_BATCH has device_id_start: 3 '
                                                       '\nand device_id_end: 6, this packet will present status of'
                                                       ' device id 3 in field "a", device id 4 in field "b", etc.'
                                                       '\nRemaining fields will return empty .'],
                                        parameter_groups=ParamGroup.NONE)

    KM_CONFIGURATION = PacketPrototype(PacketID.KM_CONFIGURATION, data_type=int,
                                       data_names=['enable', 'obstacles', 'orientation', 'frame'],
                                       uses_no_change=True,
                                       parameter_groups=ParamGroup.PARAMS |
                                                        ParamGroup.KINEMATICS)
    KM_END_POS = PacketPrototype(PacketID.KM_END_POS, data_type=float,
                                 data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'],
                                 parameter_groups=ParamGroup.STATUS)
    KM_END_VEL = PacketPrototype(PacketID.KM_END_VEL, data_type=float,
                                 data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'],
                                 parameter_groups=ParamGroup.STATUS)

    # KM_END_POS_LOCAL = PacketPrototype(PacketID.KM_END_POS_LOCAL, data_type=float,
    #                                    data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'])
    KM_END_VEL_LOCAL = PacketPrototype(PacketID.KM_END_VEL_LOCAL, data_type=float,
                                       data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'],
                                       parameter_groups=ParamGroup.NONE)
    KM_END_VEL_WORK = PacketPrototype(PacketID.KM_END_VEL_WORK, data_type=float,
                                      data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'],
                                      parameter_groups=ParamGroup.NONE)
    WORK_FRAME = PacketPrototype(PacketID.WORK_FRAME, data_type=float,
                                 data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'],
                                 parameter_groups=ParamGroup.NONE)
    KM_MOUNT_POS_ROT = PacketPrototype(PacketID.KM_MOUNT_POS_ROT, data_type=float,
                                       data_names=['x', 'y', 'z', 'Rx', 'Ry', 'Rz'],
                                       data_comments=["Packet intended for the Base Index Arm \n "
                                                      "Purpose: To define the origin (X, Y, Z, in mm."
                                                      " Rx, Ry, Rx in rad)\n"
                                                      "Used in determining the inverse kinematics"],
                                       parameter_groups=ParamGroup.NONE)

    KM_BOX_OBSTACLE_00 = PacketPrototype(PacketID.KM_BOX_OBSTACLE_00, data_type=float,
                                         data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_BOX_OBSTACLE_01 = PacketPrototype(PacketID.KM_BOX_OBSTACLE_01, data_type=float,
                                         data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_BOX_OBSTACLE_02 = PacketPrototype(PacketID.KM_BOX_OBSTACLE_02, data_type=float,
                                         data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_BOX_OBSTACLE_03 = PacketPrototype(PacketID.KM_BOX_OBSTACLE_03, data_type=float,
                                         data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_BOX_OBSTACLE_04 = PacketPrototype(PacketID.KM_BOX_OBSTACLE_04, data_type=float,
                                         data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_BOX_OBSTACLE_05 = PacketPrototype(PacketID.KM_BOX_OBSTACLE_05, data_type=float,
                                         data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2'],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)

    KM_CYLINDER_OBSTACLE_00 = PacketPrototype(PacketID.KM_CYLINDER_OBSTACLE_00, data_type=float,
                                              data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2', 'radius'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_CYLINDER_OBSTACLE_01 = PacketPrototype(PacketID.KM_CYLINDER_OBSTACLE_01, data_type=float,
                                              data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2', 'radius'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_CYLINDER_OBSTACLE_02 = PacketPrototype(PacketID.KM_CYLINDER_OBSTACLE_02, data_type=float,
                                              data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2', 'radius'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_CYLINDER_OBSTACLE_03 = PacketPrototype(PacketID.KM_CYLINDER_OBSTACLE_03, data_type=float,
                                              data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2', 'radius'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_CYLINDER_OBSTACLE_04 = PacketPrototype(PacketID.KM_CYLINDER_OBSTACLE_04, data_type=float,
                                              data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2', 'radius'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)
    KM_CYLINDER_OBSTACLE_05 = PacketPrototype(PacketID.KM_CYLINDER_OBSTACLE_05, data_type=float,
                                              data_names=['x1', 'y1', 'z1', 'x2', 'y2', 'z2', 'radius'],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO | ParamGroup.KINEMATICS)

    KM_FLOAT_PARAMETERS = PacketPrototype(PacketID.KM_FLOAT_PARAMETERS, data_type=float,
                                          data_names=['m_zero', 'lambda_translate', 'lambda_rotate',
                                                      'collision_fwd_time_steps', 'self_collision_radius',
                                                      'end_eff_collision_tolerance'],
                                          parameter_groups=ParamGroup.ALPHA |
                                                           ParamGroup.KINEMATICS |
                                                           ParamGroup.PARAMS)

    KM_FLOAT_PARAMETERS_2 = PacketPrototype(PacketID.KM_FLOAT_PARAMETERS_2, data_type=float,
                                            data_names=['obs_reaction_dist',
                                                        'obs_equilib_dist',
                                                        'self_reaction_dist',
                                                        'self_equilib_dist',
                                                        'max_pen_correction',
                                                        'max_vel_in_pen',
                                                        'vel_half'],
                                            data_comments=['Distance (mm) to start reacting/slowing down when '
                                                           'near an obstacle.',
                                                           'Distance to keep from an obstacle (if closer than this '
                                                           'distance, then a velocity demand will be made to move the '
                                                           'arm towards equilibrium distance)',
                                                           'Distance (mm) to start reacting/slowing down when near a '
                                                           'self collision',
                                                           'Distance to keep from another joint (if closer than this '
                                                           'distance, then a velocity demand will be made to move the '
                                                           'arm towards equilibrium distance) ',
                                                           'Maximum allowed penetration for correction, '
                                                           'any further will result in using the max_vel_in_pen',
                                                           ' Maximum allowed joining velocity when in penetration, '
                                                           'or when constraints cannot be solved)',
                                                           'Allowed cartesian Velocity when the distance is halfway in '
                                                           'between the equilibrium distance and reaction distance'],
                                            parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO |
                                                             ParamGroup.KINEMATICS | ParamGroup.PARAMS)

    # KM_JOINT_STATE = 0xB2
    KM_JOINT_STATE = PacketPrototype(PacketID.KM_JOINT_STATE, data_type=float,
                                     data_names=['position', 'velocity', 'velocity_setpoint'],
                                     parameter_groups=ParamGroup.NONE)
    # KM_JOINT_STATE_REQUEST = 0xB3

    KM_DH_PARAMETERS_0 = PacketPrototype(PacketID.KM_DH_PARAMETERS_0, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_1 = PacketPrototype(PacketID.KM_DH_PARAMETERS_1, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_2 = PacketPrototype(PacketID.KM_DH_PARAMETERS_2, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_3 = PacketPrototype(PacketID.KM_DH_PARAMETERS_3, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_4 = PacketPrototype(PacketID.KM_DH_PARAMETERS_4, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_5 = PacketPrototype(PacketID.KM_DH_PARAMETERS_5, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_6 = PacketPrototype(PacketID.KM_DH_PARAMETERS_6, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    KM_DH_PARAMETERS_7 = PacketPrototype(PacketID.KM_DH_PARAMETERS_7, data_type=float,
                                         data_names=['d', 'a', 'alpha', 'theta_offset', 'theta_min', 'theta_max'],
                                         parameter_groups=ParamGroup.BRAVO | ParamGroup.BRAVO | ParamGroup.ALPHA | ParamGroup.KINEMATICS)

    KM_POS_LIMIT_TRANSLATE = PacketPrototype(PacketID.KM_POS_LIMIT_TRANSLATE, data_type=float,
                                             data_names=['max', 'min'],
                                             parameter_groups=ParamGroup.KINEMATICS)
    KM_VEL_LIMIT_TRANSLATE = PacketPrototype(PacketID.KM_VEL_LIMIT_TRANSLATE, data_type=float,
                                             data_names=['max', 'min'],
                                             parameter_groups=ParamGroup.KINEMATICS)
    KM_POS_LIMIT_YAW = PacketPrototype(PacketID.KM_POS_LIMIT_YAW, data_type=float,
                                       data_names=['max', 'min'],
                                       parameter_groups=ParamGroup.KINEMATICS)
    KM_POS_LIMIT_PITCH = PacketPrototype(PacketID.KM_POS_LIMIT_PITCH, data_type=float,
                                         data_names=['max', 'min'],
                                         parameter_groups=ParamGroup.KINEMATICS)
    KM_POS_LIMIT_ROLL = PacketPrototype(PacketID.KM_POS_LIMIT_ROLL, data_type=float,
                                        data_names=['max', 'min'],
                                        parameter_groups=ParamGroup.KINEMATICS)
    KM_VEL_LIMIT_ROTATE = PacketPrototype(PacketID.KM_VEL_LIMIT_ROTATE, data_type=float,
                                          data_names=['max', 'min'],
                                          parameter_groups=ParamGroup.KINEMATICS)

    KM_POS_GAINS_TRANSLATE = PacketPrototype(PacketID.KM_POS_GAINS_TRANSLATE, data_type=float,
                                             data_names=['KP', 'KI', 'KD', 'KF', 'MI'],
                                             parameter_groups=ParamGroup.KINEMATICS)
    KM_VEL_GAINS_TRANSLATE = PacketPrototype(PacketID.KM_VEL_GAINS_TRANSLATE, data_type=float,
                                             data_names=['KP', 'KI', 'KD', 'KF', 'MI'],
                                             parameter_groups=ParamGroup.KINEMATICS)
    KM_POS_GAINS_ROTATE = PacketPrototype(PacketID.KM_POS_GAINS_ROTATE, data_type=float,
                                          data_names=['KP', 'KI', 'KD', 'KF', 'MI'],
                                          parameter_groups=ParamGroup.KINEMATICS)
    KM_VEL_GAINS_ROTATE = PacketPrototype(PacketID.KM_VEL_GAINS_ROTATE, data_type=float,
                                          data_names=['KP', 'KI', 'KD', 'KF', 'MI'],
                                          parameter_groups=ParamGroup.KINEMATICS)

    KM_JOINT_STATE_REQUEST = PacketPrototype(PacketID.KM_JOINT_STATE_REQUEST, data_type=int,
                                             parameter_groups=ParamGroup.NONE)

    ## Removed Encoder packet. Made redundant from icmu software.
    # ENCODER = PacketPrototype(PacketID.ICMU_WRITE_REGISTER, data_type=int,
    #                           data_names=['address1', 'data1', 'address2', 'data2'],
    #                           parameter_groups=ParamGroup.NONE)
    ICMU_WRITE_REGISTER = PacketPrototype(PacketID.ICMU_WRITE_REGISTER, data_type=int,
                                          data_names=['address1', 'data1'],
                                          parameter_groups=ParamGroup.NONE)
    ICMU_PARAMETERS = PacketPrototype(PacketID.ICMU_PARAMETERS, data_type=int,
                                      data_names=['direction', 'set_zero'],
                                      default_values=[None, 0],
                                      parameter_groups=ParamGroup.MA_ROTATE | ParamGroup.PARAMS)

    # RS2 Specific - Needs refining and sub-parameters
    VELOCITY_DEMAND_INNER = PacketPrototype(PacketID.VELOCITY_DEMAND_INNER, data_type=float,
                                            parameter_groups=ParamGroup.CONTROLS)
    POSITION_DEMAND_INNER = PacketPrototype(PacketID.POSITION_DEMAND_INNER, data_type=float,
                                            parameter_groups=ParamGroup.CONTROLS)
    CURRENT_DEMAND_DIRECT = PacketPrototype(PacketID.CURRENT_DEMAND_DIRECT, data_type=float,
                                            parameter_groups=ParamGroup.CONTROLS)
    ICMU_INNER_PARAMETERS = PacketPrototype(PacketID.ICMU_INNER_PARAMETERS, data_type=int,
                                            data_names=['direction', 'set_zero'],
                                            parameter_groups=ParamGroup.BRAVO | ParamGroup.PARAMS)
    ICMU_INNER_WRITE_REGISTER = PacketPrototype(PacketID.ICMU_INNER_WRITE_REGISTER, data_type=int,
                                                data_names=['address1', 'data1'],
                                                parameter_groups=ParamGroup.NONE)
    # ICMU_PARAMETERS = PacketPrototype(PacketID.ICMU_PARAMETERS, data_type=float)
    ICMU_RAW_STREAM = PacketPrototype(PacketID.ICMU_RAW_STREAM, data_type=int,
                                      data_names=['num_readings_lsb', 'num_readings_msb',
                                                  'period_ms_lsb', 'period_ms_msb',
                                                  'masterRaw_lsb', 'masterRaw_msb',
                                                  'noniusRaw_lsb','noniusRaw_msb'])
    ICMU_READ_REGISTER = PacketPrototype(PacketID.ICMU_READ_REGISTER, data_type=int,
                                         data_names=['address', 'value'])
    ICMU_INNER_READ_REGISTER = PacketPrototype(PacketID.ICMU_INNER_READ_REGISTER, data_type=int,
                                               data_names=['address', 'value'])
    VELOCITY_LIMIT_INNER = PacketPrototype(PacketID.VELOCITY_LIMIT_INNER, data_type=float,
                                           data_names=['max', 'min'],
                                           parameter_groups=ParamGroup.BRAVO | ParamGroup.LIMITS)
    POSITION_GAINS_INNER = PacketPrototype(PacketID.POSITION_GAINS_INNER, data_type=float,
                                           data_names=['KP', 'KI', 'KD', 'KF', 'MI', 'KO'],
                                           default_values=[None, None, None, None, None, 1],
                                           parameter_groups=ParamGroup.BRAVO | ParamGroup.GAINS)
    VELOCITY_GAINS_INNER = PacketPrototype(PacketID.VELOCITY_GAINS_INNER, data_type=float,
                                           data_names=['KP', 'KI', 'KD', 'KF', 'MI', 'KO'],
                                           default_values=[None, None, None, None, None, 1],
                                           parameter_groups=ParamGroup.BRAVO | ParamGroup.GAINS)
    CURRENT_GAINS_DIRECT = PacketPrototype(PacketID.CURRENT_GAINS_DIRECT, data_type=float,
                                           data_names=['KP', 'KI', 'KD', 'KF', 'MI', 'KO'],
                                           default_values=[None, None, None, None, None, 1],
                                           parameter_groups=ParamGroup.BRAVO | ParamGroup.GAINS)
    VELOCITY_INNER_PARAMETERS = PacketPrototype(PacketID.VELOCITY_INNER_PARAMETERS, data_type=float,
                                                data_names=['scale', 'offset', 'alpha', 'beta'],
                                                parameter_groups=ParamGroup.BRAVO | ParamGroup.PARAMS)

    VELOCITY_CONSTRAINT = PacketPrototype(PacketID.VELOCITY_CONSTRAINT, data_type=float,
                                          data_names=['max', 'min'],
                                          parameter_groups=ParamGroup.NONE)

    EXPECTED_DEVICES = PacketPrototype(PacketID.EXPECTED_DEVICES, data_type=int,
                                       data_names=['dev_0', 'dev_1', 'dev_2', 'dev_3', 'dev_4', 'dev_5',
                                                   'dev_6', 'dev_7', 'dev_8', 'dev_9'],
                                       parameter_groups=ParamGroup.BRAVO)

    LINK_TRANSFORM = PacketPrototype(PacketID.LINK_TRANSFORM, data_type=float,
                                     data_names=['x', 'y', 'z', 'rx', 'ry', 'rz'],
                                     parameter_groups=ParamGroup.BRAVO)

    LINK_ATTACHMENT_0 = PacketPrototype(PacketID.LINK_ATTACHMENT_0, data_type=float,
                                        data_names=['model_number', 'x', 'y', 'z', 'rx', 'ry', 'rz'],
                                        parameter_groups=ParamGroup.BRAVO)
    LINK_ATTACHMENT_1 = PacketPrototype(PacketID.LINK_ATTACHMENT_1, data_type=float,
                                        data_names=['model_number', 'x', 'y', 'z', 'rx', 'ry', 'rz'],
                                        parameter_groups=ParamGroup.BRAVO)
    LINK_ATTACHMENT_2 = PacketPrototype(PacketID.LINK_ATTACHMENT_2, data_type=float,
                                        data_names=['model_number', 'x', 'y', 'z', 'rx', 'ry', 'rz'],
                                        parameter_groups=ParamGroup.BRAVO)
    LINK_ATTACHMENT_3 = PacketPrototype(PacketID.LINK_ATTACHMENT_3, data_type=float,
                                        data_names=['model_number', 'x', 'y', 'z', 'rx', 'ry', 'rz'],
                                        parameter_groups=ParamGroup.BRAVO)

    LINK_END_EFFECTOR_OFFSET = PacketPrototype(PacketID.LINK_END_EFFECTOR_OFFSET, data_type=float,
                                               data_names=['x', 'y', 'z', 'rx', 'ry', 'rz'],
                                               data_comments=['Determines the end effector position and orientation '
                                                              'offset, '],
                                               parameter_groups=ParamGroup.BRAVO)
    MODE_SETTINGS = PacketPrototype(PacketID.MODE_SETTINGS, data_type=int,
                                    data_names=['current_hold', 'compliance'],
                                    uses_no_change=True,
                                    parameter_groups=ParamGroup.PARAMS | ParamGroup.ALPHA | ParamGroup.BRAVO)

    BOOTLOADER_STM = PacketPrototype(PacketID.BOOTLOADER_STM, data_type=None,
                                     parameter_groups=ParamGroup.NONE)

    TEST_PACKET = PacketPrototype(PacketID.TEST_PACKET, data_type=int,
                                  parameter_groups=ParamGroup.NONE)
    SYSTEM_RESET = PacketPrototype(PacketID.SYSTEM_RESET, data_type=int,
                                   data_comments=[
                                       "Reset packet, (For the tx2, 0 for program restart."],
                                   parameter_groups=ParamGroup.NONE)

    POS_PRESET_GO = PacketPrototype(PacketID.POS_PRESET_GO, data_type=int,
                                    data_comments=["Index of pos preset to perform"],
                                    parameter_groups=ParamGroup.NONE)
    POS_PRESET_CAPTURE = PacketPrototype(PacketID.POS_PRESET_CAPTURE, data_type=int,
                                         data_comments=["Index of pos preset to capture to"],
                                         parameter_groups=ParamGroup.NONE)
    POS_PRESET_SET_0 = PacketPrototype(PacketID.POS_PRESET_SET_0, data_type=float,
                                       data_names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
                                       data_comments=["Set position preset values in rad/mm"],
                                       parameter_groups=ParamGroup.ALPHA)
    POS_PRESET_SET_1 = PacketPrototype(PacketID.POS_PRESET_SET_1, data_type=float,
                                       data_names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
                                       data_comments=["Set position preset values in rad/mm"],
                                       parameter_groups=ParamGroup.ALPHA)
    POS_PRESET_SET_2 = PacketPrototype(PacketID.POS_PRESET_SET_2, data_type=float,
                                       data_names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
                                       data_comments=["Set position preset values in rad/mm"],
                                       parameter_groups=ParamGroup.ALPHA)
    POS_PRESET_SET_3 = PacketPrototype(PacketID.POS_PRESET_SET_3, data_type=float,
                                       data_names=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
                                       data_comments=["Set position preset values in rad/mm"],
                                       parameter_groups=ParamGroup.ALPHA)
    POS_PRESET_NAME_0 = PacketPrototype(PacketID.POS_PRESET_NAME_0, data_type=int,
                                        data_names=['0', '1', '2', '3', '4', '5', '6', '7'],
                                        data_comments=["Name of position preset 0 (0-8 character in utf-8 format)"],
                                        parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)
    POS_PRESET_NAME_1 = PacketPrototype(PacketID.POS_PRESET_NAME_1, data_type=int,
                                        data_names=['0', '1', '2', '3', '4', '5', '6', '7'],
                                        data_comments=["Name of position preset 1 (0-8 characters in utf-8 format))"],
                                        parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)
    POS_PRESET_NAME_2 = PacketPrototype(PacketID.POS_PRESET_NAME_2, data_type=int,
                                        data_names=['0', '1', '2', '3', '4', '5', '6', '7'],
                                        data_comments=["Name of position preset 2 (0-8 characters in utf-8 format))"],
                                        parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)
    POS_PRESET_NAME_3 = PacketPrototype(PacketID.POS_PRESET_NAME_3, data_type=int,
                                        data_names=['0', '1', '2', '3', '4', '5', '6', '7'],
                                        data_comments=["Name of position preset 3 (0-8 characters in utf-8 format))"],
                                        parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)
    POS_PRESET_ENABLE_LOCAL = PacketPrototype(PacketID.POS_PRESET_ENABLE_LOCAL, data_type=int,
                                              data_comments=["0=OFF, 1=ON. "
                                                             "Allow local pos presets to set the position setpoint. "
                                                             "Used when kinematics is not running. "
                                                             "Should be disabled when collisions "
                                                             "could occur without kinematics."],
                                              parameter_groups=ParamGroup.ALPHA | ParamGroup.BRAVO)

    POS_SEQ_PARAMETERS = PacketPrototype(PacketID.POS_SEQ_PARAMETERS, data_type=int,
                                         data_names=['enable', 'not_used_1', 'not_used_2', 'not_used_3', ],
                                         parameter_groups=ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    POS_SEQ_SET_0 = PacketPrototype(PacketID.POS_SEQ_SET_0, data_type=int,
                                    data_names=['a', 'b', 'c', 'd'],
                                    data_comments=['0=DISABLED, 1=ENABLED\nDisabling makes POS_PRESET_GO '
                                                   'set single pos presets.\n'
                                                   'Enabling makes POS_PRESET_GO launch the sequence.'],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    POS_SEQ_SET_1 = PacketPrototype(PacketID.POS_SEQ_SET_1, data_type=int,
                                    data_names=['a', 'b', 'c', 'd'],
                                    data_comments=['Pos preset index for each step of the sequencer.'],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    POS_SEQ_SET_2 = PacketPrototype(PacketID.POS_SEQ_SET_2, data_type=int,
                                    data_names=['a', 'b', 'c', 'd'],
                                    data_comments=['Pos preset index for each step of the sequencer.'],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.KINEMATICS)
    POS_SEQ_SET_3 = PacketPrototype(PacketID.POS_SEQ_SET_3, data_type=int,
                                    data_names=['a', 'b', 'c', 'd'],
                                    data_comments=['Pos preset index for each step of the sequencer.'],
                                    parameter_groups=ParamGroup.ALPHA | ParamGroup.KINEMATICS)

    RC_BASE_VELOCITY_SCALE = PacketPrototype(PacketID.RC_BASE_VELOCITY_SCALE, data_type=float,
                                             data_names=['end_eff', 'a', 'b', 'c', 'd', 'e', 'f', 'g'],
                                             data_comments=["Set velocity scale factor for Master Arm base board ONLY"],
                                             parameter_groups=ParamGroup.MA_BASE)
    RC_BASE_POSITION_SCALE = PacketPrototype(PacketID.RC_BASE_POSITION_SCALE, data_type=float,
                                             data_names=['end_eff', 'a', 'b', 'c', 'd', 'e', 'f', 'g'],
                                             data_comments=["Set position scale factor for Master Arm base board ONLY"],
                                             parameter_groups=ParamGroup.MA_BASE)
    RC_BASE_OPTIONS = PacketPrototype(PacketID.RC_BASE_OPTIONS, data_type=int,
                                      data_names=['jaw_vel_joystick_axis_select',
                                                  'joy_x_enabled_in_pause_mode',
                                                  'joy_y_enabled_in_pause_mode',
                                                  'pos_preset_device_id',
                                                  'default_pause_mode',
                                                  'not_assigned_f',
                                                  'not_assigned_g',
                                                  'not_assigned_h',
                                                  'not_assigned_i',
                                                  'not_assigned_j'],
                                      alt_data_names=[[], [], [], [], ['not_assigned_e'], [], [], [], [], []],
                                      data_comments=["jaw_vel_joystick_axis_select "
                                                     "0: None, 1: X, 2: Y. Default=2",

                                                     "joy_x_enabled_in_pause_mode "
                                                     "Maintain joystick x axis control during pause. "
                                                     "0: Disable, 1: Enable",

                                                     "joy_y_enabled_in_pause_mode "
                                                     "Maintain joystick y axis control during pause. "
                                                     "0: Disable, 1: Enable",

                                                     "Device id to address pos preset packets to. "
                                                     "RS1: 0xC5, RS2: 0xCE"],
                                      default_values=[None, None, None, None, None, 0, 0, 0, 0, 0],
                                      parameter_groups=ParamGroup.MA_BASE)
    RC_BASE_EXT_DEVICE_IDS = PacketPrototype(PacketID.RC_BASE_EXT_DEVICE_IDS, data_type=int,
                                             data_names=['end', 'a', 'b', 'c', 'd', 'e', 'f', 'g'],
                                             parameter_groups=ParamGroup.MA_BASE)
    RC_JOYSTICK_PARAMETERS = PacketPrototype(PacketID.RC_JOYSTICK_PARAMETERS, data_type=float,
                                             data_names=['deadband', 'center', 'lower_limit', 'upper_limit'],
                                             data_comments=['joystick deadband (def: 0.1, 0.0 - 1.0)',
                                                            'joystick center (def: 0.5, 0.0 - 1.0)',
                                                            'joystick lower limit (def: 0.0, 0.0 - 1.0)',
                                                            'joystick upper limit (def: 1.0, 0.0 - 1.0)'],
                                             parameter_groups=ParamGroup.MA_END)

    KM_COLLISION_FLAG = PacketPrototype(PacketID.KM_COLLISION_FLAG, data_type=int,
                                        data_names=["collision_type", "no_previous_joints", "collision_obstacle_index"])

    KM_COLLISION_COORDS = PacketPrototype(PacketID.KM_COLLISION_COORDS, data_type=float,
                                          data_names=["x", "y", "z"])
    ATI_FT_READING = PacketPrototype(PacketID.ATI_FT_READING, data_type=float,
                                     data_names=['fx', 'fy', 'fz', 'tx', 'ty', 'tz'],
                                     parameter_groups=ParamGroup.STATUS)
    ATI_FT_MESSAGE = PacketPrototype(PacketID.ATI_FT_MESSAGE, data_type=int,
                                     data_names=['a', 'b', 'c', 'd', 'e', 'f', 'g'],
                                     parameter_groups=ParamGroup.STATUS)

    PATH_PLANNING_PARAMETERS = PacketPrototype(PacketID.PATH_PLANNING_PARAMETERS, data_type=float,
                                               data_names=["search_time",
                                                           "max_iterations",
                                                           "optimisation_method",
                                                           "optimisation_param_a",
                                                           "optimisation_param_b",
                                                           "optimisation_param_c",
                                                           "optimisation_param_d"])

    ETH_PARAMETERS = PacketPrototype(PacketID.ETH_PARAMETERS, data_type=int,
                                     parameter_groups=ParamGroup.BRAVO,
                                     data_names=["mac_0",
                                                 "mac_1",
                                                 "mac_2",
                                                 "mac_3",
                                                 "mac_4",
                                                 "mac_5",
                                                 "ip_a",
                                                 "ip_b",
                                                 "ip_c",
                                                 "ip_d",
                                                 "sn_a",
                                                 "sn_b",
                                                 "sn_c",
                                                 "sn_d",
                                                 "gw_a",
                                                 "gw_b",
                                                 "gw_c",
                                                 "gw_d",
                                                 "dns_a",
                                                 "dns_b",
                                                 "dns_c",
                                                 "dns_d",
                                                 "dhcp_mode"],
                                     data_comments=["mac_address",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "ip_address",
                                                    "",
                                                    "",
                                                    "",
                                                    "subnet_mask",
                                                    "",
                                                    "",
                                                    "",
                                                    "gateway",
                                                    "",
                                                    "",
                                                    "",
                                                    "dns",
                                                    "",
                                                    "",
                                                    "",
                                                    "dhcp_mode 1 is static, 2 is dhcp"])  # 1 is static, # 2 is dhcp

    _ethernet_socket_data_names = \
        ["sn_type",
         "flag",
         "sn_port_h",
         "sn_port_l",
         "sn_remote_ip_a",
         "sn_remote_ip_b",
         "sn_remote_ip_c",
         "sn_remote_ip_d",
         "sn_remote_port_h",
         "sn_remote_port_l"]
    ETH_SOCKET_0_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_0_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])

    ETH_SOCKET_1_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_1_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])
    ETH_SOCKET_2_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_2_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])
    ETH_SOCKET_3_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_3_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])
    ETH_SOCKET_4_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_4_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])
    ETH_SOCKET_5_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_5_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])
    ETH_SOCKET_6_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_6_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])

    ETH_SOCKET_7_PARAMETERS = PacketPrototype(PacketID.ETH_SOCKET_7_PARAMETERS,
                                              data_type=int,
                                              data_names=_ethernet_socket_data_names[:])

    PACKET_PARAMETERS = PacketPrototype(PacketID.PACKET_PARAMETERS,
                                        data_type=int,
                                        data_names=["device_id_prefix",
                                                    "force_prefix",
                                                    "enable_forward_option"])

    IP_DEVICE_ID_MAP = PacketPrototype(PacketID.IP_DEVICE_ID_MAP,
                                       data_type=int,
                                       data_names=["a_ip_a",
                                                   "a_ip_b",
                                                   "a_ip_c",
                                                   "a_ip_d",
                                                   "a_port_h",
                                                   "a_port_l",
                                                   "b_ip_a",
                                                   "b_ip_b",
                                                   "b_ip_c",
                                                   "b_ip_d",
                                                   "b_port_h",
                                                   "b_port_l",
                                                   "c_ip_a",
                                                   "c_ip_b",
                                                   "c_ip_c",
                                                   "c_ip_d",
                                                   "c_port_h",
                                                   "c_port_l",
                                                   "d_ip_a",
                                                   "d_ip_b",
                                                   "d_ip_c",
                                                   "d_ip_d",
                                                   "d_port_h",
                                                   "d_port_l",
                                                   "e_ip_a",
                                                   "e_ip_b",
                                                   "e_ip_c",
                                                   "e_ip_d",
                                                   "e_port_h",
                                                   "e_port_l",
                                                   "f_ip_a",
                                                   "f_ip_b",
                                                   "f_ip_c",
                                                   "f_ip_d",
                                                   "f_port_h",
                                                   "f_port_l"
                                                   ])

    BUS_STATE = PacketPrototype(PacketID.BUS_STATE,
                                data_type=int,
                                data_names=["value"])

    CAN_BUS_STATE_SCHED = PacketPrototype(PacketID.CAN_BUS_STATE_SCHED,
                                          data_type=int,
                                          data_names=["device_a",
                                                   "device_b",
                                                   "device_c",
                                                   "device_d",
                                                   "device_e",
                                                   "device_f",
                                                   "device_g",
                                                   "device_h",
                                                   "device_i",
                                                   "device_j",
                                                   "device_k",
                                                   "device_l",
                                                   "device_m",
                                                   "device_n",
                                                   "device_o",
                                                   "device_p",
                                                   "device_q",
                                                   "device_r",
                                                   "device_s",
                                                   "device_t",
                                                   "timeout_a",
                                                   "timeout_b",
                                                   "timeout_c",
                                                   "timeout_d",
                                                   "timeout_e",
                                                   "timeout_f",
                                                   "timeout_g",
                                                   "timeout_h",
                                                   "timeout_i",
                                                   "timeout_j",
                                                   "timeout_k",
                                                   "timeout_l",
                                                   "timeout_m",
                                                   "timeout_n",
                                                   "timeout_o",
                                                   "timeout_p",
                                                   "timeout_q",
                                                   "timeout_r",
                                                   "timeout_s",
                                                   "timeout_t"],
                                                   parameter_groups=ParamGroup.BRAVO)

    KM_END_VEL_CAMERA = PacketPrototype(PacketID.KM_END_VEL_CAMERA, data_type=float,
                                        data_names=['x', 'y', 'z', 'yaw', 'pitch', 'roll'])

    POWER_SUPPLY_PARAMETERS = PacketPrototype(PacketID.POWER_SUPPLY_PARAMETERS, data_type=float,
                                              data_names=['output_voltage'])

    CONTROL_RESTRICTED = PacketPrototype(PacketID.CONTROL_RESTRICTED, data_type=int,
                                         data_names=['value'],
                                         data_comments=['Read Only: 0 for no restrictions, 1 for restrictions'])

    JOYSTICK_VALUES = PacketPrototype(PacketID.JOYSTICK_VALUES, data_type=float,
                                         data_names=['x', 'y'])

    DIAGNOSTICS = PacketPrototype(PacketID.DIAGNOSTICS, data_type=int,
                                  data_names=['a', 'b', 'c', 'd', 'e'])

def find_differently_named_packets_prototypes():
    attrlist = inspect.getmembers(Packets)

    for packet in attrlist:
        if isinstance(packet[1], PacketPrototype):
            name = packet[0]
            prototype = packet[1]

            if name != RS1_hardware.get_name_of_packet_id(prototype.packet_id):
                print(
                    f"NAMES NOT THE SAME, PacketPrototype: {name}, PacketID: {RS1_hardware.get_name_of_packet_id(prototype.packet_id)}")


if __name__ == "__main__":
    find_differently_named_packets_prototypes()
    # my_packet = PacketPrototype()
    # my_packet.data_names = ['a', 'b', 'c']
    # print("my_packet.data_length =", my_packet.data_length)

    # Packets.DEVICE_ID.send(1, 1)
    # Packets.KM_DH_PARAMETERS_0.send(1, 1, 2, 3, 4, 5, 6)
    # Packets.SUPPLY_VOLTAGE.send()
    # Packets.ICMU_PARAMETERS.data_names
