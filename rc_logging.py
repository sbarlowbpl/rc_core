import logging

from colorlog import ColoredFormatter

def getLogger(name):
    logger = logging.getLogger(name)
    console_handler = logging.StreamHandler()
    formatter = ColoredFormatter("%(log_color)s%(levelname)-8s%(name)s: \t%(message)s%(reset)s")
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    return logger