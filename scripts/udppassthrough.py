import socket

import serial

UDP_IP = ""
UDP_PORT = 6789

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

data = None
while not data:
    print("\r" + "Waiting for message")
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print("received message:", data)

try:
    serial_port = serial.Serial("/dev/ttyUSB0", baudrate=115200, timeout=3.0)
    print('serial port is open')
except:
    print('failed to connect to serial port')

