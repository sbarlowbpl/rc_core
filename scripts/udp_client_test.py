import socket
import sys

# Create a UDP socket
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.settimeout(0.1)

server_address = ('127.0.0.1', 12870)
message = bytes([0]) #b'This is the message.  It will be repeated.'
time_last_send = 0

while True:

    # Receive response
    try:
        data, server = sock.recvfrom(4096)
        if data:
            print('data:', ["0x%02X" % db for db in data])
            data_rec = True
    except socket.timeout:
        pass
    except BlockingIOError:
        pass
    except Exception as e:
        print(e)
    time.sleep(0.005)

    # Send data
    if time.time() > time_last_send + 1.0:
        print('sending {!r}'.format(message))
        sent = sock.sendto(message, server_address)
        time_last_send = time.time()
