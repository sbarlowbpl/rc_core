import socket

from serial import Serial

TOP_SIDE_PORT = 'COM13'
BOTTOM_SIDE_IP = "192.168.2.2"
BOTTOM_SIDE_PORT = 6789

BOTTOM_SIDE_ADDRESS = (BOTTOM_SIDE_IP, BOTTOM_SIDE_PORT)

if __name__ == '__main__':

    top_serial = Serial(port=TOP_SIDE_PORT, baudrate=115200)

    bottom_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    bottom_socket.setblocking(0)

    while True:
        if top_serial.is_open:
            num_bytes_waiting = top_serial.in_waiting

            if num_bytes_waiting:
                bytes_in = top_serial.read(num_bytes_waiting)

                if bytes_in:
                    bottom_socket.sendto(bytes_in, (BOTTOM_SIDE_IP, BOTTOM_SIDE_PORT))
