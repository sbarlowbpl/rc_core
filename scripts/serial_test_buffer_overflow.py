from serial_device import *

if __name__ == '__main__':
    ser = SerialDevice()
    ser.port = "COM28"  # DTech 485
    # ser.port = "COM35"  # bare board 232
    # ser.port = "COM5"  # Red 485
    # ser.port = "COM27"  # Red 485
    # ser.port = "COM29" # Blue/Silver 232 Prolific
    # ser.port = "COM26"  # Blue/White 232
    # ser.port = "COM38" # Blue/Silver 232
    # ser.port = "COM10" # Blue/White 232

    ser.open()

