import os, sys, time

import socket


# Get personal IP address - set by router
private_ip_address = socket.gethostbyname(socket.gethostname())
print('private_ip_address:', private_ip_address)

# Get localhost (internal loopback)
localhost = socket.gethostbyname('localhost')
print('localhost:', localhost)

# Get localhost (internal loopback)
host_from_ip = socket.gethostbyaddr('127.0.0.1')
print('host_from_ip:', host_from_ip)

# Random IP name - return socket.gaierror if hostname invalid
random_ip_name = 'some ip'
try:
    random_ip_from_name = socket.gethostbyname(random_ip_name)
    print('Random IP test:', 'Test passed:', random_ip_name, random_ip_from_name)
except socket.gaierror as e:
    print('Random IP name test:', 'socket.gaierror:', random_ip_name, e)
except socket.error as e:
    print('Random IP name test:', 'socket.error:', random_ip_name, e)

# Use IP in gethostbyname
random_ip = '192.168.33.220'
try:
    random_ip_from_ip = socket.gethostbyname(random_ip)
    print('Random IP test:', 'Test passed:', random_ip, random_ip_from_ip)
except socket.gaierror as e:
    print('Random IP test:', 'socket.gaierror', random_ip, e)
except socket.error as e:
    print('Random IP test:', 'socket.error', random_ip, e)