import socket
import time

from RC_Core.commconnection_methods import encode_packet, packet_splitter, parsepacket, _parsepacket_bpl
from RC_Core.RS1_hardware import PacketID

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setblocking(False)

    server_address = ('127.0.0.1', 12870)
    start_time = time.time()
    while True:
        packet = encode_packet(1, PacketID.RUN_TIME, [time.time() - 1607757000])
        sock.sendto(packet, server_address)
        print("packet sent:", packet)
        # time.sleep(0.000)


if __name__ == '__main__':
    main()
