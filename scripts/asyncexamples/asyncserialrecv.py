import asyncio
import time
from typing import Any

import serial_asyncio
from kivy.clock import Clock
from kivy.config import Config
from kivy.app import App
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout

from RC_Core.commconnection_methods import packet_splitter, parsepacket


class MainLayout(BoxLayout):
    serial_output = StringProperty("serial_bytes")

    def on_parent(self, inst, value):
        print(value)
        self.connection_manager = ConnectionManager()
        asyncio.create_task(self.init_comms())
        asyncio.create_task(self.async_run())

    async def init_comms(self):
        new_connection = await self.connection_manager.create_serial_connection("COM32")
        new_connection = await self.connection_manager.create_udp_client_connection("0.0.0.0", 12870)
        print("serial conn created")

    async def async_run(self):
        print(await self.get_str())
        while True:
            print("running", time.time())
            for con in self.connection_manager.connections:
                new_packets = con.read()
                print("reader buffer", con, len(new_packets), new_packets)
            await asyncio.sleep(1)

    async def get_str(self):
        return "some string"

    def remove_connection(self):
        connection = self.connection_manager.connections[1]
        connection.protocol.transport.close()
        self.connections.connection_manager.remove(connection)

    def add_connection(self):
        asyncio.create_task(self.init_comms())


class ConnectionManager:
    class SerialConnection(asyncio.Protocol):
        transport: asyncio.transports.BaseTransport

        def __init__(self):
            self.rx_packets: list[list[int, int, tuple]] = []
            self.forward_connections: list[ConnectionManager.SerialConnection] = []
            self.forward: bool = False
            self.parse_packets: bool = True
            self.rx_buffer = b''
            self.reply_address: tuple[str, int] = ('', 0)

        def connection_made(self, transport: asyncio.transports.BaseTransport) -> None:
            """ Keep record of serial transport and prepare to receive data """
            self.transport = transport
            self.rx_buffer = b''

        def datagram_received(self, data, addr):
            self.reply_address = addr
            self.data_received(data)

        def data_received(self, data: bytes) -> None:
            """ This function is called when new bytes arrive """
            if self.forward:
                for con in self.forward_connections:
                    con.send(data)
            self.rx_buffer += data
            # print("data rx", data)
            if self.parse_packets:
                rx_packets, self.rx_buffer = packet_splitter(self.rx_buffer)
                new_packets = [parsepacket(p) for p in rx_packets]
                # [print(round(time.time() - (1607757000 + p[2][0]), 4)) for p in new_packets]
                self.rx_packets += new_packets

        def connection_lost(self, exc) -> None:
            print("Connection rx closed")

        def send(self, data: bytes):
            print("send bytes", data)
            self.transport.serial.write(bytes)

        def close(self):
            self.transport.close()

    class Connection:
        def __init__(self, protocol):
            self.protocol = protocol

        def send(self, device_id, packet_id, data, options=None):
            pass

        def read(self):
            out = self.protocol.rx_packets
            self.protocol.rx_packets = []
            return out

    def __init__(self):
        self.connections: list[ConnectionManager.Connection] = []

    async def create_serial_connection(self, comport):
        loop = asyncio.get_event_loop()
        writer, reader = await serial_asyncio.create_serial_connection(loop, self.SerialConnection, comport, baudrate=115200)
        new_connection = self.Connection(reader)
        self.connections.append(new_connection)
        return new_connection

    async def create_udp_client_connection(self, ip_address: str, port: int):
        loop = asyncio.get_event_loop()
        writer, reader = await loop.create_datagram_endpoint(self.SerialConnection, local_addr=(ip_address, port))
        new_connection = self.Connection(reader)
        self.connections.append(new_connection)
        return new_connection


class AsyncSerialRecvApp(App):
    def build(self):
        return MainLayout()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.slow_callback_duration = 0.02
    loop.run_until_complete(AsyncSerialRecvApp().async_run())
    loop.close()
