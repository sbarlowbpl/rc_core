import time

import serial
import serial.tools.list_ports

comports = serial.tools.list_ports.comports()
[print(p.description) for p in comports]

SERIAL_PORT_A = "COM8"
ser_a = serial.Serial(SERIAL_PORT_A, baudrate=115200)


SERIAL_PORT_B = "COM7"
ser_b = serial.Serial(SERIAL_PORT_B, baudrate=115200)

while True:
    ser_a.write(b'1234')
    time.sleep(0.5)
    print(ser_b.read(4))


