import time

import serial

from RC_Core.commconnection_methods import encode_packet, packet_splitter, parsepacket, _parsepacket_bpl
from RC_Core.RS1_hardware import PacketID


def main():
    SERIAL_PORT = "COM33"
    ser = serial.Serial(SERIAL_PORT, baudrate=115200)

    start_time = time.time()
    while True:
        packet = encode_packet(1, PacketID.RUN_TIME, [time.time() - 1607757000])
        ser.write(packet)
        print(f"packet sent {packet}")
        if ser.in_waiting:
            print(f"Received: {ser.read(ser.in_waiting)}")
        time.sleep(0.01)


if __name__ == '__main__':
    main()
