import cProfile

from RC_Core.commconnection_methods import encode_packet, packet_splitter, parsepacket, _parsepacket_bpl
from RC_Core.enums.packets import PacketID


def get_data_stream():
    packet = b''
    for i in range(10000):
        packet += encode_packet(1, PacketID.POSITION_DEMAND, [1.2345])
    return packet


def parse(rx_bytes):
    rx_packets = packet_splitter(rx_bytes)
    packets = [_parsepacket_bpl(p) for p in rx_packets[0]]


def main():
    rx_stream = get_data_stream()
    parse(rx_stream)


if __name__ == '__main__':
    main()
    cProfile.run("main()")
