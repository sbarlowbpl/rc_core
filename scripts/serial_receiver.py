from serial import Serial

TOP_SIDE_PORT = 'COM17'

if __name__ == '__main__':

    top_serial = Serial(port=TOP_SIDE_PORT, baudrate=115200)

    while True:
        if top_serial.is_open:
            num_bytes_waiting = top_serial.in_waiting

            if num_bytes_waiting:
                bytes_in = top_serial.read(num_bytes_waiting)

                print(f"Received: {bytes_in}")