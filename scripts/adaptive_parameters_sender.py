
import serial
import time

port = "COM33"
baud = 115200

timeout = 0.2

if __name__ == '__main__':

    serial_device = serial.Serial(port, baud)
    serial_device.timeout = 0.1
    serial_device.write_timeout = 0.05
    i = 0

    # Packets for 0x01, 0x02, 0x03, 0x04, 0x05

    device_ids = [0x01, 0x02, 0x03, 0x04, 0x05]
    packets = [b"\x01\x01\x0b\x80?\xcd\xccL=\xac\xc5'7\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x05\x9f\x01$\x1b\x00",
               b"\x01\x01\x0b\x80?\xcd\xccL=\xac\xc5'7\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x05\x9f\x02$>\x00",
               b"\x01\x01\x0b\x80?\xcd\xccL=\xac\xc5'7\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x05\x9f\x03$\xfe\x00",
               b"\x01\x01\x0b\x80?\xcd\xccL=\xac\xc5'7\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x05\x9f\x04$t\x00",
               b"\x01\x01\x0b\x80?\xcd\xccL=\xac\xc5'7\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x05\x9f\x05$\xb4\x00"]

    request_packets = [b'\x06\x9f`\x01\x05>\x00',
                       b'\x06\x9f`\x02\x05\x1b\x00',
                       b'\x06\x9f`\x03\x05\xdb\x00',
                       b'\x06\x9f`\x04\x05Q\x00',
                       b'\x06\x9f`\x05\x05\x91\x00'
                       ]

    for i in range(5):
        device_id = device_ids[i]

        send_packet = packets[i]

        request_packet = packets[i]

        print("Sending Packet")
        print(f"Sending  {send_packet}")
        try:
            serial_device.write(send_packet)

            print("Successfully wrote packet")

        except:
            print(f"Could Not Write")

        time.sleep(0.1)
        print(f"Sending request packet for device_id: {device_id}")

        try:
            serial_device.write(request_packet)

            print("Successfully wrote request_packet")

        except:
            print(f"Could Not Write")

        start_time = time.time()

        read_data = b''
        while time.time() - start_time < timeout:
            read_data += serial_device.read(4096)

        if read_data:
            print(f"Received {read_data}")

            if read_data != send_packet[:(len(read_data))]:
                print(f"Response and sent packet DONT MATCH: \n{read_data}\n{send_packet}")

        else:
            print("Did not get request_back from data")

        time.sleep(0.1)



