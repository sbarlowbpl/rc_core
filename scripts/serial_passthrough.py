from serial import Serial

TOP_SIDE_PORT = 'COM13'
BOTTOM_SIDE_PORT = 'COM28'


if __name__ == '__main__':

    top_serial = Serial(port=TOP_SIDE_PORT, baudrate=115200)
    # top_serial.open()

    bottom_serial = Serial(port=BOTTOM_SIDE_PORT, baudrate=115200)
    # bottom_serial.open()

    while True:
        if top_serial.is_open:
            num_bytes_waiting = top_serial.in_waiting

            if num_bytes_waiting:
                bytes_in = top_serial.read(num_bytes_waiting)

                if bytes_in and bottom_serial.is_open:
                    bottom_serial.write(bytes_in)
