import time

from serial import Serial

PORT = 'COM34'

if __name__ == '__main__':

    message = b'HELLO WORLD'
    top_serial = Serial(port=PORT, baudrate=115200)
    while True:
        if top_serial.is_open:
            top_serial.write(message)

        time.sleep(0.05)