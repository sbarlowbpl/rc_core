import asyncio
from RC_Core.connections import SerialConnection

COMPORT = "COM33"
BAUD = 115200


def bytes_callback(data: bytes):
    print(f"received data: {data}")


async def async_main():
    conn = SerialConnection(COMPORT, BAUD)

    await conn.connect()

    conn.add_bytes_callback(bytes_callback)

    count = 1
    while True:
        print(f"Running main loop {count}")
        count += 1
        await asyncio.sleep(0.5)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(async_main())

