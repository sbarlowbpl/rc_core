from serial import Serial
import socket
import time

ip_address = '192.168.2.2'
port = 6789
destination = (ip_address, port)

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while 1:
    new_bytes, remote_ip = udp_socket.recv(4096)
    print(new_bytes, remote_ip)
    time.sleep(1)
