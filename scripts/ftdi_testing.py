import time

import ftd2xx as ftdi
from ftd2xx import defines as ftdidef

import serial.tools.list_ports

def _parse_ftdi_ports(port):
    try:
        # Get COM port hardware id if COM port of selected name available
        pyserial_comport_hardware_id = None
        comport_list = serial.tools.list_ports.comports()
        total_com_port_count = len(comport_list)
        for port_from_comport_list, desc, hwid in sorted(comport_list):
            # Check if COM port names match
            if port == port_from_comport_list:
                pyserial_comport_hardware_id = hwid

        # If no COM ports match
        if pyserial_comport_hardware_id is None:
            return None

        # Get COM port serial device id from hardware id list of pyserial
        pyserial_comport_serial_id = (pyserial_comport_hardware_id.split("SER=", 1)[1])[:8]

        # Check ftdi device index list for COM port that matches user selected COM port
        for index in range(total_com_port_count):
            ftdi_target = ftdi.open(index)
            ftdi_comport_serial_id = str((ftdi_target.getDeviceInfo())["serial"])
            ftdi_comport_serial_id = (ftdi_comport_serial_id.split("b'", 1)[1]).split("'", 1)[0]

            # If serial device ids match between pyserial and ftd2xx driver port selections,
            # return ftd2xx port index
            if pyserial_comport_serial_id == ftdi_comport_serial_id:
                ftdi_target.close()
                return index
            ftdi_target.close()
        return None
    except:
        return None


COMPORT = "COM12"
if __name__ == '__main__':

    index = _parse_ftdi_ports(COMPORT)

    ftdi_serial = ftdi.open(index)
    ftdi_serial.setBaudRate(115200)
    ftdi_serial.setDataCharacteristics(ftdidef.BITS_8, ftdidef.STOP_BITS_1, ftdidef.PARITY_EVEN)
    ftdi_serial.setTimeouts(0, 0)
    ftdi_serial.setLatencyTimer(2)



    while True:
        time.sleep(0.5)
        print("Writing Hello World")
        msg = b'Hello World'
        try:
            ftdi_serial.write(msg)
            print(ftdi_serial.getStatus())
        except:
            print("ERROR")




