import re
import struct
from typing import Tuple, List, Optional, Union, Set

from cobs import cobs
from crcmod import crcmod
from RC_Core.callbackobject import TC_DEVICE_ID, TC_PACKET_ID, TC_DATA, TC_TIME, TC_CALLBACK
from RC_Core.RS1_hardware import PacketID, Mode
from RC_Core.packetid import Packets



''' METHODS GLOBAL SETTINGS '''
class CommMethodGlobals():
    '''
    List of global variables
    '''
    CRC8_FUNC = crcmod.mkCrcFun(0x14D, initCrc=0xFF, xorOut=0xFF)
    VERBOSE = False
    PARSE_PACKET_ERRORS = 0
    FLOAT_PACKETS = Packets().get_float_packet_ids()
    BPSS_PROTOCOL = False

    @staticmethod
    def set_bpss_protocol():
        '''
        Change to BPSS serial protocol
        :return:
        '''
        CommMethodGlobals.BPSS_PROTOCOL = True

    @staticmethod
    def set_bpl_protocol():
        '''
        Change to BPL serial protocol
        :return:
        '''
        CommMethodGlobals.BPSS_PROTOCOL = False



''' MAIN METHOD FUNCTIONS '''
def encode_packet(device_id: int, packet_id: int, data_in: Union[float, int, List[float], bytearray, bytes]) -> bytes:
    '''
    Encode packet data for transmission
    :param device_id:
    :param packet_id:
    :param data_in:
    :return:
    '''
    if CommMethodGlobals.BPSS_PROTOCOL:
        return _encode_packet_bpss(device_id, packet_id, data_in)
    else:
        return _encode_packet_bpl(device_id, packet_id, data_in)

def parsepacket(packet_in: Union[bytearray,bytes]) -> Tuple[int,int,Union[List[float],bytes,int]]:
    '''
    Decode packet into its components
    :param packet_in:
    :return:
    '''
    if CommMethodGlobals.BPSS_PROTOCOL:
        return _parsepacket_bpss(packet_in)
    else:
        return _parsepacket_bpl(packet_in)

def send_packet(connection, device_id: int, packet_id: int, data: Union[List[float], bytearray, bytes], callback=None):
    '''
    Add packet to send queue
    :param connection:
    :param device_id:
    :param packet_id:
    :param data:
    :param callback:
    :return:
    '''
    if CommMethodGlobals.BPSS_PROTOCOL:
        return _send_packet_bpss(connection, device_id, packet_id, data, callback)
    else:
        return _send_packet_bpl(connection, device_id, packet_id, data, callback)

def packet_splitter(buff: bytes) -> Tuple[List[bytes], Optional[bytes]]:
    '''
    Split bytes/bytearray list from incoming connections into packets
    :param buff: Bytes
    :return:
    '''
    if CommMethodGlobals.BPSS_PROTOCOL:
        return _packet_splitter_bpss(buff)
    else:
        return _packet_splitter_bpl(buff)


_float_packet_ids: Set[int] = set(Packets().get_float_packet_ids())

''' BPL PROTOCOL FUNCTIONS '''
def _encode_packet_bpl(device_id: int, packet_id: int, data_in: Union[float, int, List[float], bytearray, bytes]) -> bytes:
    '''
    Encode packet using BPL serial protocol
    :param device_id:
    :param packet_id:
    :param data_in:
    :return:
    '''

    if isinstance(data_in, list):

        if packet_id in _float_packet_ids:
            data_in = [float(x) for x in data_in]
        else:
            data_in = bytearray([int(x) for x in data_in])



    txPacket: bytes = convert_to_bytes(data_in)
    if CommMethodGlobals.VERBOSE:
        print(txPacket)
    txPacket += bytes([packet_id, device_id, len(txPacket)+4])
    txPacket += bytes([CommMethodGlobals.CRC8_FUNC(txPacket)])
    ## COBS ENCODE + TERMINATOR byte
    packet: bytes = cobs.encode(txPacket) + bytes([0x00])
    if CommMethodGlobals.VERBOSE: print('\nencoded packet check')
    if CommMethodGlobals.VERBOSE: print([ "0x%02x" % b for b in packet ])
    return packet

def _packet_splitter_bpl(buff: bytes) -> Tuple[List[bytes], Optional[bytes]]:
    '''
    Split packet using BPL serial protocol
    :param buff:
    :return:
    '''
    incomplete_packet = None
    packets = re.split(b'\x00', buff)
    if buff[-1] != b'0x00':
        incomplete_packet = packets.pop()
    return packets, incomplete_packet

def _parsepacket_bpl(packet_in: Union[bytearray,bytes]) -> Tuple[int,int,Union[List[float],bytes,int]]:
    '''
    Parse packet using BPL serial protocol
    :param packet_in:
    :return:
    '''
    packet_in = bytearray(packet_in)
    if packet_in != b'' and len(packet_in) > 3:
        try:
            decoded_pckt: bytes = cobs.decode(packet_in)  ## Cobs decode
        except cobs.DecodeError as e:
            return 0, 0, 0
        if decoded_pckt[-2] != len(decoded_pckt): # check that length is correct. Generally would only happen on first packet or due to bad transmission
            CommMethodGlobals.PARSE_PACKET_ERRORS += 1
            print(__name__, 'error count:', CommMethodGlobals.PARSE_PACKET_ERRORS," ###########################",
                            "PARSEPACKET() incorrect Length, length is:", len(decoded_pckt), [ " 0x%02x" % b for b in decoded_pckt ])
        else:
            if CommMethodGlobals.CRC8_FUNC(decoded_pckt[:-1]) == decoded_pckt[-1]:
                rxData = decoded_pckt[:-4]
                if decoded_pckt[-4] in CommMethodGlobals.FLOAT_PACKETS:
                    rxData = struct.unpack(str(int(len(rxData)//4)) + "f", rxData[:(len(rxData)//4) * 4])
                return decoded_pckt[-3], decoded_pckt[-4], rxData
            else:
                print("CRC Error")
                print([" 0x%02x" % b for b in packet_in])
    return 0, 0, 0

def _send_packet_bpl(connection, device_id: int, packet_id: int, data: Union[List[float], bytearray, bytes], callback=None):
    '''
    Add packet to send queue using BPL serial protocol
    :param connection:
    :param device_id:
    :param packet_id:
    :param data:
    :param callback:
    :return:
    '''
    for packet in connection.send_queue:
        if packet[TC_DEVICE_ID] == device_id and packet[TC_PACKET_ID] == packet_id \
                and packet_id not in [PacketID.REQUEST, PacketID.DEVICE_ID]:
            connection.send_queue.remove(packet)
    if [device_id, packet_id, data] not in connection.send_queue or packet_id is PacketID.REQUEST:
        connection.send_queue.append([device_id, packet_id, data, callback])



''' BPSS PROTOCOL FUNCTIONS '''
def _encode_packet_bpss(device_id: int, packet_id: int, data_in: Union[float, int, List[float], bytearray, bytes]) -> bytearray:
    '''
    Encode packet using BPSS serial protocol
    :param device_id:
    :param packet_id:
    :param data_in:
    :return:
    '''
    txData = convert_to_bytes_bpss(data_in)
    datalength = len(txData)
    packet_length = datalength + 4
    txPacket = bytearray(b'#')
    txPacket.extend(b"%02X" % device_id)
    txPacket.extend(b"%02X" % packet_id)
    txPacket.extend(txData)
    manualCrcValue = calculate_checksum(txPacket)
    tx_crc, crc_bytes = convert_crc_to_int(manualCrcValue)
    txPacket.extend(b"%04X" % tx_crc)
    txPacket.extend(b'\r\n')
    packet = txPacket
    if CommMethodGlobals.VERBOSE: print('\nencoded packet check')
    if CommMethodGlobals.VERBOSE: print([ "0x%02x" % b for b in packet ])
    return packet

def _packet_splitter_bpss(buff: bytes) -> Tuple[List[bytes], Optional[bytes]]:
    '''
    Split packet using BPSS serial protocol
    :param buff:
    :return:
    '''
    incomplete_packet = None
    packets = re.split(b'\r\n', buff)
    if buff[-1] != b'\r\n':
        incomplete_packet = packets.pop()
    for i in range(len(packets)):
        if b'$' in packets[i][1:]:
            # print(__name__, 'readdata() packets[i]', packets[i])
            start_index = packets[i].rfind(b'$', 1)
            packets[i] = packets[i][start_index:]
    return packets, incomplete_packet

def _parsepacket_bpss(packet_in: Union[bytearray,bytes]) -> Tuple[int,int,Union[List[float],bytes,int]]:
    '''
    Decode packet using BPSS serial protocol
    :param packet_in:
    :return:
    '''
    packet_in = bytearray(packet_in)
    if packet_in != b'' and len(packet_in) > 10:
        packet = packet_in
        if packet[0:1] != b'$':
            print(__name__, "parsepacket() incorrect Length, packet is:", packet)
        else:
            rx_crc_string = packet[-4:]
            try:
                rx_crc_int: int = int(rx_crc_string, 16)
                data_to_checksum: bytearray = bytearray(packet[:-4])
                crc_check: int = calculate_checksum(data_to_checksum)
            except:
                print(__name__, 'parsepacket() BAD CRC CHECK packet_in', packet_in)
                crc_check = 0
                return 0, 0, 0
            crc_string = hex(crc_check)[2:]
            crc_byte_1 = crc_string[2:]
            crc_byte_2 = crc_string[:2]
            crc_bytes = crc_byte_1 + crc_byte_2
            crc_int = int(crc_bytes, 16)
            if rx_crc_int == crc_int:
                packet = packet[0:-4]
                try:
                    rxDeviceId = int(packet[1:3], 16)
                except:
                    print(__name__, 'FAILED rxDeviceId = int(packet[1:3], 16)', packet)
                    return 0, 0, 0
                packet = packet[3:]
                try:
                    rxPacketId = packet[0:2].decode()
                except:
                    print(__name__, 'FAILED rxPacketId = packet[0:2].decode()', packet)
                    return 0, 0, 0

                try:
                    rxPacketId = int(rxPacketId, 16)
                except:
                    print(__name__, 'FAILED rxPacketId = packet[0:2].decode()', packet)
                    return 0, 0, 0

                packet = packet[2:]
                # print("data:", packet)

                try:
                    rxData = packet.decode()
                except:
                    print(__name__, 'FAILED rxData = packet.decode()', packet)
                    return 0, 0, 0

                if CommMethodGlobals.VERBOSE: print('rxData:', rxData)
                if False:
                    pass
                elif (rxPacketId == PacketID.POSITION_LIMIT
                      or rxPacketId == PacketID.VELOCITY_LIMIT
                      or rxPacketId == PacketID.CURRENT_LIMIT):
                    maxbyte = rxData[8:16]
                    maxbyte = hex_to_float(maxbyte)
                    minbyte = rxData[0:8]
                    minbyte = hex_to_float(minbyte)
                    print('maxbyte is:', maxbyte, 'minbyte is:', minbyte)
                    rxData = maxbyte, minbyte
                elif (rxPacketId == PacketID.KM_END_POS
                      or rxPacketId == PacketID.KM_END_VEL):
                    x = rxData[16:24]
                    y = rxData[8:16]
                    z = rxData[0:8]
                    print('x:', x, 'y:', y, 'z:', z)
                    x = hex_to_float(x)
                    y = hex_to_float(y)
                    z = hex_to_float(z)
                    print('x:', x, 'y:', y, 'z:', z)
                    rxData = x, y, z
                elif rxPacketId == PacketID.MODE:
                    print('MODE packet', "Packet ID:", rxPacketId, "data:", rxData)
                    rxData = [int(rxData, 16)]
                else:
                    if rxPacketId in CommMethodGlobals.FLOAT_PACKETS:
                        # print(__name__, "parsepacket() float packet", rxData, "len(rxData)/8", len(rxData)/8)
                        data_to_process = rxData
                        rxData = []
                        for i in range(int(len(data_to_process) / 8)):
                            this_hex = data_to_process[-8:]
                            data_to_process = data_to_process[:-8]
                            rxData.append(hex_to_float(this_hex))
                            # print(__name__, "parsepacket() this_hex", this_hex)
                    else:
                        print(__name__, "parsepacket() byte packet", rxData)
                        data_to_process = rxData
                        rxData = []
                        for i in range(int(len(data_to_process) / 2)):
                            this_hex = data_to_process[-2:]
                            data_to_process = data_to_process[:-2]
                            rxData.append(int(this_hex, 16))
                            print(__name__, "parsepacket() this_hex", this_hex)
                return rxDeviceId, rxPacketId, rxData
            else:
                print("CRC Error in packet:", packet_in, "crc calculated:", crc_bytes)
    return 0, 0, 0

def _send_packet_bpss(connection, device_id: int, packet_id: int, data: Union[List[float], bytearray, bytes], callback=None):
    '''
    Add packet to send queue using BPSS serial protocol
    :param connection:
    :param device_id:
    :param packet_id:
    :param data:
    :param callback:
    :return:
    '''
    for packet in connection.send_queue:
        if packet[TC_DEVICE_ID] == device_id and packet[TC_PACKET_ID] == packet_id \
                and packet_id not in [PacketID.REQUEST, PacketID.DEVICE_ID]:
            connection.send_queue.remove(packet)
    if [device_id, packet_id, data] not in connection.send_queue or packet_id is PacketID.REQUEST:
        connection.send_queue.append([device_id, packet_id, data, callback])

def calculate_checksum(input_buffer: Union[bytearray, bytes]) -> int:
    """
    Function that computes the CRC16 value for an array of sequential bytes
    stored in memory.
    NB: Types uint8 and uint16 represent unsigned 8 and 16 bit integers
    respectively.
    @param buf Pointer to the start of the buffer to compute the CRC16 for.
    @param len The number of bytes in the buffer to compute the CRC16 for.
    @result The new CRC16 value.
    :param input_buffer:
    """

    poly = 0xA001
    crc: int = 0
    input_to_process = reverse_data_bytes(input_buffer[1:])
    for b in range(0, len(input_to_process), 2):
        two_chars = input_to_process[b:b+2]
        v = int(two_chars, 16)
        for i in range(8):
            if (v & 0x01) ^ (crc & 0x01):
                crc >>= 1
                crc ^= poly
            else:
                crc >>= 1
            v >>= 1
    return crc

def reverse_data_bytes(input_bytes: Union[bytearray, bytes]) -> bytearray:
    '''
    Reverse byte order
    :param input_bytes:
    :return:
    '''
    header, data_to_sort = input_bytes[:4], input_bytes[4:]
    new_data = bytearray(b'')
    for i in range(0, len(data_to_sort), 2):
        start_idx = len(data_to_sort) - i - 2
        end_idx = start_idx + 2
        new_byte = data_to_sort[start_idx:end_idx]
        new_data.extend(new_byte)
    output = header
    output.extend(new_data)
    return output

def convert_crc_to_int(manualCrcValue: int) -> Tuple[int,str]:
    '''
    Convert the CRC value to int type
    :param manualCrcValue:
    :return:
    '''
    crc_string = hex(manualCrcValue)[2:]
    if len(crc_string) == 3:
        crc_string = '0' + crc_string
    crc_byte_1 = crc_string[2:]
    crc_byte_2 = crc_string[:2]
    crc_bytes = crc_byte_1 + crc_byte_2
    tx_crc = int(crc_bytes, 16)
    return tx_crc, crc_bytes

def convert_to_bytes_bpss(parameter: Union[float, int, List[float], bytearray, bytes]) -> bytes:
    '''
    Convert bytes to bpss
    :param parameter:
    :return:
    '''
    if type(parameter).__name__ == 'float':
        bytes_value = float_to_hex(parameter)
    elif type(parameter).__name__ == 'int':
        bytes_value = b"%02X" % parameter
    elif type(parameter).__name__ == 'list':
        # print('converting LIST')
        if len(parameter):
            for val in parameter:
                if type(val).__name__ != 'float':
                    raise ValueError('convert_to_bytes_bpss: input lists must contain only floats')
        else:
            raise ValueError('convert_to_bytes_bpss: cannot convert an empty list to bytes')
        bytes_value = bytearray()
        for val in parameter:
            bytes_value = float_to_hex(val) + bytes_value
    elif type(parameter).__name__ in ['bytearray', 'bytes']:
        bytes_value = b''
        for byte in parameter:
            bytes_value = b"%02X" % byte + bytes_value
    else:
        raise ValueError('covert_to_bytes_bpss: only accepts float, int, List[float], bytearray, or bytes')
    return bytes(bytes_value)

def float_to_hex(f: float) -> bytes:
    '''
    Covert a float to hexidecimal
    :param f:
    :return:
    '''
    output = hex(struct.unpack('<I', struct.pack('<f', f))[0])[2:].upper().encode()
    # print('float_to_hex() output:', output, 'length:', len(output))
    if f == 0:
        return b'00000000'
    # if output is b'0':
    #     return b'00000000'
    return output

def hex_to_float(h: str) -> float:
    '''
    Convert a hexidecimal to float
    :param h:
    :return:
    '''
    # print("hex in:", h)
    try:
        return struct.unpack('!f', bytes.fromhex(h))[0]
    except:
        return 0

def hex_to_string(h: str) -> str:
    '''
    Convert a hexidecimal to string
    :param h:
    :return:
    '''
    hex_data_in = h
    out_string = ''
    while hex_data_in is not '':
        this_char = int(hex_data_in[0:2], 16)
        hex_data_in = hex_data_in[2:]
        out_string = str(chr(this_char)) + out_string
    return out_string

''' OTHER FUNCTIONS '''
# Convert numeric property velocity into bytes
def convert_to_bytes(parameter: Union[float, int, List[float], bytearray, bytes]) -> bytes:
    '''
    Convert different parameters to their byte forms
    :param parameter:
    :return:
    '''
    if type(parameter).__name__ == 'float':
        bytes_value = struct.pack("f", parameter)
    elif type(parameter).__name__ == 'int':
        bytes_value = struct.pack("i", parameter)
    elif type(parameter).__name__ == 'list':
        if len(parameter):
            for val in parameter:
                if type(val).__name__ != 'float':
                    raise ValueError('covert_to_bytes: input lists must contain only floats')
        else:
            raise ValueError('covert_to_bytes: cannot convert an empty list to bytes')
        bytes_value = struct.pack('%sf' % len(parameter), *parameter)
    elif type(parameter).__name__ == 'bytearray':
        bytes_value = bytes(parameter)
    elif type(parameter).__name__ == 'bytes':
        bytes_value = parameter
    else:
        raise ValueError('covert_to_bytes: only accepts float, int, List[float], bytearray, or bytes')
    return bytes_value


if __name__ == '__main__':
    print(_encode_packet_bpss(1, 3, [0.5, 0.6]))


    print(_parsepacket_bpss(b'01033F19999A3F000000E447'))
