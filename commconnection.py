import asyncio
import logging
import platform
import socket
import time
from typing import List, Union, Optional, Any, Set, Callable, Dict, Tuple

import serial
from colorlog import ColoredFormatter
import serial.tools.list_ports

from RC_Core.comm_type import CommType
from RC_Core.connection_controller import DeviceConnection, ConnectionClient

from RC_Core.commconnection_methods import encode_packet, packet_splitter, parsepacket, CommMethodGlobals

# Try to import ftd2xx driver (for bootloader in Windows only), else fake it to stop errors
from RC_Core.enums import CommsProtocol
from RC_Core.enums.packets import PacketID

try:
    import ftd2xx as ftdi
    from ftd2xx import defines as ftdidef
except:
    class ftdi:
        def open(self, **args):
            raise Exception('Cannot use ftd2xx outside Windows!')


    class ftdidef:
        BITS_8 = None
        PARITY_EVEN = None
        PARITY_NONE = None
        STOP_BITS_1 = None

# Try to import pylibftdi driver (for Linux only), else fake it to stop errors
try:
    import pylibftdi as libftdi
except:
    class libftdiftdi:
        def open(self, **args):
            raise Exception('Cannot use ftd2xx outside Windows!')

BUFF_SIZE = 4096

import rc_logging
logger = rc_logging.getLogger(__name__)


class CommConnection:
    """Comm Connection
    Primary communications handler.
    This stores and operates on the difference communications methods.
    Includes open/closing, read/write, and connection method changing.
    If the connection is connected and active, the "connected" flag will be "True"
    """

    connections: list = []
    _connected: bool = False
    uid: int = -1
    name: str = ''
    _type: CommType = CommType.NONE

    @property
    def type(self) -> CommType:
        return self._type

    @type.setter
    def type(self, t: CommType):
        self._type = t

    @property
    def connection_client(self) -> ConnectionClient:
        return ConnectionClient.instance

    @property
    def connected(self):

        if self.direct:
            if self.type == CommType.SERIAL:
                if isinstance(self.serial_device, ftdi.FTD2XX):
                    try:
                        self.serial_device.getDeviceInfo()
                        return True
                    except ftdi.DeviceError:
                        return False

                elif isinstance(self.serial_device, serial.Serial):
                    if self.serial_device.is_open:
                        return True
                    else:
                        return False
            elif self.type == CommType.UDP:
                return True

        if self.device_connection:
            return self.device_connection.connected
        else:
            return False
        # return self._connected

    socket = None
    socket_ip: str = ''
    socket_port: int = 0
    serial_device: Union[Any, serial.Serial, None] = None
    serial_port: str = ''
    baudrate: int = 115200
    byte_size = serial.EIGHTBITS
    parity = serial.PARITY_NONE
    stop_bits = serial.STOPBITS_ONE
    timeout: float = 0

    incomplete_pckt: bytes = b''

    _half_duplex: bool = True
    receive_timeout: float = 0.1
    receive_wait_start_time: float = 0.0
    send_lock: bool = False
    wait_for_request_timer: float = 0.0
    requests_queue_remaining: int = 0
    requests_queue_remaining_previous_state: int = 0
    request_interval_ctr: int = 0
    time_last_read_bytes: float = 0.0
    time_of_last_bytes: float = -1.0
    # enabled: bool = True
    product_request_number: int = 0
    force_stop_passthrough: bool = False

    device_connection: DeviceConnection = None
    _received_bytes: bytearray = None

    _direct = False

    connected_callbacks: Set[Callable[[bool], None]] = None

    _comms_protocol = CommsProtocol.BPL

    @property
    def half_duplex(self):
        return self._half_duplex

    @half_duplex.setter
    def half_duplex(self, state):
        self._half_duplex = state
        if self.task and not self.task.done():
            asyncio.create_task(self.async_set_half_duplex(state))
            return
        if self.device_connection:
            if self.task and not self.task.done():
                asyncio.create_task(self.async_set_half_duplex(state))
                return
            self.device_connection.toggle_half_duplex(state)

    async def async_set_half_duplex(self, state):
        await asyncio.wait_for(self.task, 20.5)
        if self.device_connection:
            logger.info("ASYNC SET HALF DUPLEX")
            self.device_connection.toggle_half_duplex(state)

    @property
    def comms_protocol(self):
        return self._comms_protocol

    @comms_protocol.setter
    def comms_protocol(self, protocol):
        self._comms_protocol = protocol
        if self.task and not self.task.done():
            asyncio.create_task(self.async_set_comms_protocol(protocol))
            return
        if self.device_connection:
            self.device_connection.set_comms_protocol(protocol)

    async def async_set_comms_protocol(self, comms_protocol):
        await asyncio.wait_for(self.task, 15.5)
        if self.device_connection:
            logger.info("ASYNC SET COMMS PROTOCOL")
            self.device_connection.set_comms_protocol(comms_protocol)


    def __init__(self):
        """
        Create connection object
        """
        self.heartbeat_callbacks = {}
        self.packet_callbacks = {}
        logger.debug(f"__init__()")

        CommConnection.uid += 1
        self.uid: int = CommConnection.uid
        self.temporary_callbacks: list = []

        cid = 1
        name_found = True
        while True:
            if self.name is None or self.name == '':
                name = 'Connection ' + str(cid)
                for connection in self.connections:
                    if connection is self:
                        continue
                    if connection.name == name:
                        name_found = False
                        break

                if name_found:
                    self.name = name
                    break
                else:
                    cid += 1
                    name_found = True

        logger.info(f"Creating Connection: {self.name}")

        self.permanent_callbacks: list = []
        self.packets_to_handle: list = []
        self.send_queue: list = []
        self.send_freq_queue: bytearray = bytearray([])
        self.console_output: list = []
        CommConnection.connections.append(self)
        self.set_type(CommType.NONE)

        self.loop = asyncio.get_event_loop()
        self._received_bytes = bytearray(b'')

        self.connected_callbacks = set()

        self.task = self.loop.create_task(self._create_connection())







        # self.add_heartbeat_callback(0x05, PacketID.CURRENT, self.current_callback)

    # def current_callback(self, device_id, packet_id, current):
    #     logger.info(f"__CURERNNT CALLBACK {current}")

    async def _create_connection(self):
        cc: ConnectionClient = self.connection_client

        if self.device_connection is not None:
            for key, callbacks in self.packet_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callbacks:
                    self.device_connection.remove_packet_callback(device_id, packet_id, callback)

            for key, callbacks in self.heartbeat_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callbacks.keys():
                    self.device_connection.remove_heartbeat_callback(device_id, packet_id, callback)

        self.device_connection = await cc.create_connection()

        self.device_connection.add_bytes_callback(self._bytes_callback)

        # add packet callbacks to device_connection
        for key, callbacks in self.packet_callbacks.items():
            device_id = key[0]
            packet_id = key[1]
            for callback in callbacks:
                self.device_connection.add_packet_callback(device_id, packet_id, callback)

        for key, callbacks in self.heartbeat_callbacks.items():
            device_id = key[0]
            packet_id = key[1]
            for callback, freq in callbacks.items():
                self.device_connection.add_heartbeat_callback(device_id, packet_id, callback, freq)

        self._connected = True

        self.device_connection.connected_callbacks.add(self._connected_callback)

        logger.info(f"Created Connection {self.name}")

    def _connected_callback(self, connected):
        logger.debug(f"_connected_callback({connected})")
        for callback in self.connected_callbacks:
            callback(connected)
        return None

    async def remove_connection(self):
        if self._connected_callback in self.device_connection.connected_callbacks:
            self.device_connection.connected_callbacks.remove(self._connected_callback)

        for key, callbacks in self.packet_callbacks.items():
            device_id = key[0]
            packet_id = key[1]
            for callback in callbacks:
                self.remove_packet_callback(device_id, packet_id, callback)

        for key, callbacks in self.heartbeat_callbacks.items():
            device_id = key[0]
            packet_id = key[1]
            for callback in callbacks.keys():
                self.remove_heartbeat_callback(device_id, packet_id, callback)

        await self.connection_client.remove_connection(self.device_connection.connection_device_id)



    reconnect_udp_timestamp: float = 0.0
    reconnect_udp_event: Optional[asyncio.Task] = None

    async def reconnect_udp(self):
        while self.type == CommType.UDP:

            if time.time() > self.reconnect_udp_timestamp:

                from RC_Core.connectionmanager import ConnectionManager
                if ConnectionManager.instance.run_connection_loop:
                    logger.info("Did not receive bytes UDP, CHOOSING TO RECONNECT")
                    self.close()
                    await asyncio.sleep(0.3)
                    self.connect()
                    self.reconnect_udp_timestamp = time.time() + 5
                else:
                    self.reconnect_udp_timestamp = time.time() + 5
                    await asyncio.sleep(5)
            else:
                await asyncio.sleep(self.reconnect_udp_timestamp - time.time())
        pass

    def _bytes_callback(self, data: bytes):

        self.time_of_last_bytes = time.time()

        self._received_bytes += data

        if len(self._received_bytes) > BUFF_SIZE:
            # logger.warning(f"_bytes_callback(), Buffer Overflow. Read Buffer is over {BUFF_SIZE} bytes")

            self._received_bytes = self._received_bytes[-BUFF_SIZE:]

        self.reconnect_udp_timestamp = time.time() + 5.0

    @staticmethod
    def get_printable_connection_list() -> List[str]:
        """
        :return: List of the names of all connections
        """
        output_list: list = []
        for conn in CommConnection.connections:
            output_list.append(conn.name)
        return output_list

    @staticmethod
    def get_connection_with_name(name: str):
        """
        :param name:
        :return: Connection object from name
        """
        for conn in CommConnection.connections:
            if name == conn.name:
                return conn
        return None

    def set_type(self, _type: CommType):

        logger.debug(f"set_type({_type})")
        '''
        Sets the connection type
        :param type: Commtype enum value
        :return:
        '''

        self.type = _type

        if _type == CommType.NONE:
            pass
        elif _type == CommType.SERIAL:
            pass
        elif _type == CommType.TCP:
            pass
        elif _type == CommType.UDP:
            pass
        elif _type == CommType.SERIAL_FTDI:
            pass
            # self.set_methods_serialftdi()
        elif _type == CommType.SERIAL_LIBFTDI:
            pass
            # self.set_methods_seriallibftdi()
        else:
            raise TypeError(f"Unknown Type {_type}")

    async def set_none(self):
        logger.debug(f"set_none()")
        self._received_bytes = bytearray(b'')
        await asyncio.wait_for(self.task, 7.5)
        if self.device_connection:
            response = await self.device_connection.set_type(CommType.NONE, None)
            logger.info(f"Set Connection: {self.name} to NONE")
            return response

    async def set_serial(self,
                         port: str,
                         baudrate: int = 115200,
                         bytesize=serial.EIGHTBITS,
                         parity=serial.PARITY_NONE,
                         stopbits=serial.STOPBITS_ONE):

        self._received_bytes = bytearray(b'')
        logger.debug(f"set_serial({port})")
        await asyncio.wait_for(self.task, 20.0)

        args = [port, baudrate, bytesize, parity, stopbits]

        response = await self.device_connection.set_type(CommType.SERIAL, args)
        logger.info(f"Set Connection: {self.name} to SERIAL")

        return response

    async def set_udp(self,
                      ip_address: str,
                      port: int):

        logger.debug(f"set_udp({ip_address, port})")
        self._received_bytes = bytearray(b'')
        await asyncio.wait_for(self.task, 20.5)

        args = [ip_address, port]

        response = await self.device_connection.set_type(CommType.UDP, args)
        logger.info(f"Set Connection: {self.name} to UDP")

        self.reconnect_udp_timestamp = time.time() + 5.0
        if self.reconnect_udp_event and not self.reconnect_udp_event.done():
            self.reconnect_udp_event.cancel()
        self.reconnect_udp_event = asyncio.create_task(self.reconnect_udp())

        return response

    async def set_tcp(self,
                      ip_address: str,
                      port: int):

        logger.debug(f"set_tcp({ip_address, port})")
        self._received_bytes = bytearray(b'')
        await asyncio.wait_for(self.task, 7.5)
        args = [ip_address, port]

        response = await self.device_connection.set_type(CommType.TCP, args)

        return response

    async def set_serial_ftdi(self,
                              port: str,
                              baudrate: int = 115200,
                              bytesize=ftdidef.BITS_8,
                              parity=ftdidef.PARITY_NONE,
                              stopbits=ftdidef.STOP_BITS_1):

        logger.debug(f"set_serial_ftdi({port})")
        self._received_bytes = bytearray(b'')
        await asyncio.wait_for(self.task, 7.5)
        args = [port, baudrate, bytesize, parity, stopbits]

        response = await self.device_connection.set_type(CommType.SERIAL_FTDI, args)

        return response

    def config_connection(self, port: Union[str, int], ip: str, baudrate: int = 115200, byte_size=serial.EIGHTBITS,
                          parity=serial.PARITY_NONE, stop_bits=serial.STOPBITS_ONE, timeout: float = 3.0,
                          server: bool = False):
        if self.type == CommType.SERIAL:

            self.serial_port = port
            self.baudrate = baudrate
            self.byte_size = byte_size
            self.parity = parity
            self.stop_bits = stop_bits
            self.timeout = timeout

        elif self.type == CommType.UDP:
            self.socket_ip = ip
            self.socket_port = port
            self.timeout = timeout

        elif self.type == CommType.TCP:
            self.socket_ip = ip
            self.socket_port = port
            self.timeout = timeout

        elif self.type == CommType.SERIAL_FTDI:

            self.serial_port = port
            self.baudrate = baudrate
            self.byte_size = byte_size
            self.parity = parity
            self.stop_bits = stop_bits
            self.timeout = timeout

            # if stop_bits == serial.STOPBITS_ONE:
            #     stop_bits = ftdidef.STOP_BITS_1
            # elif stop_bits == serial.STOPBITS_TWO:
            #     stop_bits = ftdidef.STOP_BITS_2
            # if parity == serial.PARITY_NONE:
            #     parity = ftdidef.PARITY_NONE
            # elif parity == serial.PARITY_EVEN:
            #     parity = ftdidef.PARITY_EVEN
            # elif parity == serial.PARITY_ODD:
            #     parity = ftdidef.PARITY_ODD
            # elif parity == serial.PARITY_MARK:
            #     parity = ftdidef.PARITY_MARK
            # elif parity == serial.PARITY_SPACE:
            #     parity = ftdidef.PARITY_SPACE
            #
            # if byte_size == serial.EIGHTBITS:
            #     byte_size = ftdidef.BITS_8
            # elif byte_size == serial.SEVENBITS:
            #     byte_size = ftdidef.BITS_7

        asyncio.create_task(
            self.async_config_connection(port, ip, baudrate, byte_size, parity, stop_bits, timeout, server))

    async def async_config_connection(self, port: Union[str, int], ip: str, baudrate: int = 115200,
                                      byte_size=serial.EIGHTBITS,
                                      parity=serial.PARITY_NONE, stop_bits=serial.STOPBITS_ONE, timeout: float = 3.0,
                                      server: bool = False):
        logger.debug(f"_config_connection()")
        if self.type == CommType.NONE:
            await self.set_none()
            pass
        elif self.type == CommType.SERIAL:

            self.serial_port = port
            self.baudrate = baudrate
            self.byte_size = byte_size
            self.parity = parity
            self.stop_bits = stop_bits
            self.timeout = timeout

            await self.set_serial(port, baudrate, byte_size, parity, stop_bits)
            pass
        elif self.type == CommType.UDP:
            self.socket_ip = ip
            self.socket_port = port
            self.timeout = timeout

            await self.set_udp(ip, port)
            pass
        elif self.type == CommType.TCP:
            self.socket_ip = ip
            self.socket_port = port
            self.timeout = timeout
            await self.set_tcp(ip, port)
            pass
        elif self.type == CommType.SERIAL_FTDI:
            # if stop_bits == serial.STOPBITS_ONE:
            #     stop_bits = ftdidef.STOP_BITS_1
            #
            # if parity == serial.PARITY_NONE:
            #     parity = ftdidef.PARITY_NONE
            #
            # if byte_size == serial.EIGHTBITS:
            #     byte_size = ftdidef.BITS_8

            # if stop_bits == serial.STOPBITS_ONE:
            #     stop_bits = ftdidef.STOP_BITS_1
            # elif stop_bits == serial.STOPBITS_TWO:
            #     stop_bits = ftdidef.STOP_BITS_2
            # if parity == serial.PARITY_NONE:
            #     parity = ftdidef.PARITY_NONE
            # elif parity == serial.PARITY_EVEN:
            #     parity = ftdidef.PARITY_EVEN
            # elif parity == serial.PARITY_ODD:
            #     parity = ftdidef.PARITY_ODD
            # elif parity == serial.PARITY_MARK:
            #     parity = ftdidef.PARITY_MARK
            # elif parity == serial.PARITY_SPACE:
            #     parity = ftdidef.PARITY_SPACE
            #
            # if byte_size == serial.EIGHTBITS:
            #     byte_size = ftdidef.BITS_8
            # elif byte_size == serial.SEVENBITS:
            #     byte_size = ftdidef.BITS_7

            await self.set_serial_ftdi(port, baudrate, byte_size, parity, stop_bits)
            pass
        elif self.type == CommType.SERIAL_LIBFTDI:
            raise NotImplementedError
        else:
            pass

        await self.start()
        return

    ''' Connect to communication method '''

    def connect(self) -> int:
        """
        Attempt to connect
        :return:
        """
        if self.type != CommType.NONE:
            logger.debug(f"_connect()")
            asyncio.create_task(self.start())
        return 0

    async def start(self):
        logger.debug(f"_start()")
        await asyncio.wait_for(self.task, 7.5)
        await self.device_connection.start()
        return 1

    def close(self):
        """
        Close connection
        :return:
        """
        self._stop()

    def _stop(self):
        self._received_bytes = bytearray(b'')
        logger.debug(f"_stop()")
        logger.info(f"Stopping connection {self.name}")
        self.send_freq_queue = bytearray(b'')
        if self.device_connection:
            asyncio.create_task(self.device_connection.stop())

    def reconnect(self) -> int:
        """
        Attempt to reconnect to connection
        :return:
        """
        """ This method should be overwritten for each connection type """
        logger.debug(f"reconnect()")


        return -2
        pass

    ''' Send functions. These can either take in raw or packet data types '''

    def send(self, device_id: int, packet_id: int, data_in: Union[list, bytes, bytearray],
             append_only: bool = False) -> int:
        """
        Attempt to send data on connection
        :param device_id:
        :param packet_id:
        :param data_in:
        :param append_only:
        :return:
        """
        # logger.debug(f"_send({device_id}, {packet_id}, {data_in})")
        if device_id is None or packet_id is None or data_in is None:
            return 0


        if self.direct:

            data = encode_packet(device_id, packet_id, data_in)
            if self.type == CommType.SERIAL:
                if self.serial_device:
                    try:
                        self.serial_device.write(data)
                    except serial.SerialException as e:
                        logger.warning(f"Direct serial connection write error {e}")

            elif self.type == CommType.UDP:
                if self.socket:
                    try:
                        self.socket.sendto(data, (self.socket_ip, self.socket_port))
                    except Exception as e:
                        logger.warning(f"Direct UDP connection write error {e}")
            else:
                raise NotImplementedError

        elif self.device_connection:
            self.device_connection.send(device_id, packet_id, data_in)
            return 1
        return 0

    def send_bytes(self, data_in: Union[bytes, bytearray], append_only=False):
        # logger.debug(f"send_bytes({data_in})")

        if self.direct:
            if self.type == CommType.SERIAL:
                if self.serial_device:
                    try:
                        self.serial_device.write(data_in)
                    except serial.SerialException as e:
                        logger.warning(f"Direct serial connection write error {e}")
            else:
                raise NotImplementedError

        elif self.device_connection:
            self.device_connection.send_bytes(data_in)
            return 1
        return 0

    def read(self, num_bytes: int = 4096) -> Union[List[bytes], list]:
        """
        Attempt to read data on connection
        :param num_bytes:
        :return:
        """
        """ This method should be overwritten for each connection type """
        new_bytes = b'' \
                    b''
        if self.direct:
            if self.type == CommType.SERIAL:
                if self.serial_device:
                    try:
                        new_bytes = self.serial_device.read(num_bytes)
                    except (serial.SerialException, ftdi.DeviceError) as e:
                        logger.warning(f"Direct serial connection read error {e}")
                        try:
                            self.serial_device.close()
                        except ftdi.DeviceError as e:
                            pass
                        return []

            elif self.type == CommType.UDP:
                if self.socket:
                    try:
                        new_bytes = self.socket.recv(num_bytes)
                    except Exception:
                        return []

            else:
                raise NotImplementedError

        elif self.device_connection:
            new_bytes = self._received_bytes[:]
            self._received_bytes = bytearray(b'')

        if new_bytes:
            self.time_of_last_bytes = time.time()
        buff = self.incomplete_pckt + new_bytes
        try:
            packets, incomplete_packet = packet_splitter(buff)
            if incomplete_packet is not None:
                self.incomplete_pckt = incomplete_packet
            else:
                self.incomplete_pckt = b''
            return packets
        except IndexError:
            return []

    def clear_bytes(self):
        self._received_bytes = bytearray(b'')

    def read_bytes(self, num_bytes: int = 4096):

        if self.direct:
            if self.type == CommType.SERIAL:
                if self.serial_device:
                    try:
                        new_bytes = self.serial_device.read(num_bytes)
                        return new_bytes
                    except serial.SerialException as e:
                        logger.warning(f"Direct serial connection read error {e}")
                        return []

            elif self.type == CommType.UDP:
                if self.socket:
                    try:
                        new_bytes = self.socket.recv(num_bytes)
                        return new_bytes
                    except Exception:
                        return []
            else:
                raise NotImplementedError

        elif self.device_connection:
            # if self.type == CommType.SERIAL_FTDI:
            #     data = self.device_connection.ftdi_connection.read_bytes(num_bytes)
            #
            #     if data:
            #         return data
            #     else:
            #         return []

            new_bytes = bytes(self._received_bytes[:num_bytes])
            self._received_bytes = self._received_bytes[num_bytes:]
            if new_bytes:
                self.time_of_last_bytes = time.time()
            return new_bytes
        return []

    def read_raw(self, num_bytes: int = 4096) -> Union[list, bytes]:
        """
        Attempt to read raw data on connection
        :param num_bytes:
        :return:
        """
        """ This method should be overwritten for each connection type """

        return self.read_bytes(num_bytes)

    def send_raw(self, data_in: Union[bytes, bytearray], append_only: bool = False) -> int:
        """
        Attempt to send raw data on connection
        :param data_in:
        :param append_only:
        :return:
        """
        """ This method should be overwritten for each connection type """
        self.send_bytes(data_in, append_only)
        return -2

    async def set(self, device_id: int, packet_id: int, data=Union[List[int], List[float]], timeout=0.2):
        """
        Used to set a parameter on a connection, will return TimeoutError if the response does not come back
        or raises Value error if the response is not the same.

        """
        if self.device_connection:
            return await self.device_connection.set(device_id, packet_id, data, timeout)


    async def request(self, device_id: int, packet_id: int, timeout=0.2) -> Union[List[float], List[int]]:

        if self.direct:
            start_time = time.time()
            self.send(device_id, PacketID.REQUEST, bytearray([packet_id]))

            while time.time() - start_time < timeout:
                time.sleep(0.001)
                packets = self.read()
                for packet in packets:
                    read_device_id, read_packet_id, data = parsepacket(packet)
                    if read_device_id == device_id and read_packet_id == packet_id:
                        return data
            raise TimeoutError

        elif self.device_connection:
            return await self.device_connection.request(device_id, packet_id, timeout)



    def direct_request(self, device_id: int, packet_id: int, timeout=0.2):
        if self.direct:
            start_time = time.time()
            self.send(device_id, PacketID.REQUEST, bytearray([packet_id]))

            while time.time() - start_time < timeout:
                time.sleep(0.001)
                packets = self.read()
                for packet in packets:
                    read_device_id, read_packet_id, data = parsepacket(packet)
                    if read_device_id == device_id and read_packet_id == packet_id:
                        return data
            raise TimeoutError

    @staticmethod
    def get_connection_names_list() -> List[str]:
        '''
        Get list of all connection names
        :return:
        '''
        name_list = []
        for con in CommConnection.connections:
            if con.name == '':
                con.name = 'Connection ' + str(con.uid)
            name_list.append(con.name)
        return name_list

    @staticmethod
    def get_connection_from_name(name: str):
        """
        Get connection object from name
        :param name:
        :return:
        """
        for con in CommConnection.connections:
            if name == con.name:
                return con
        return CommConnection.connections[0]

    def _get_serial_device_matching_port(self, pyserial_list, port):
        """
        Get matching serial device for ftdi
        :param pyserial_list:
        :param port:
        :return:
        """
        for pyserial_dev in pyserial_list:
            if pyserial_dev.device == port:
                return pyserial_dev
        return None

    def _get_libftdi_device_number(self, libftdi_list, pyserial_dev):
        """
        Get device number for libftdi
        :param libftdi_list:
        :param pyserial_dev:
        :return:
        """
        index_number = 0
        for ftdi_dev in libftdi_list:
            print(ftdi_dev[2], pyserial_dev.serial_number)
            if ftdi_dev[2] == pyserial_dev.serial_number:
                return index_number
            index_number += 1
        return None

    @property
    def direct(self):
        return self._direct

    @staticmethod
    def _parse_ftdi_ports(port: str) -> int:
        """
        Return the ftdi port index for give port name.

        If it doesnt exist, it will raises an OSError.
        """

        # Get COM port hardware id if COM port of selected name available
        pyserial_comport_hardware_id = None
        comport_list = serial.tools.list_ports.comports()
        total_com_port_count = len(comport_list)
        for port_from_comport_list, desc, hwid in sorted(comport_list):
            # Check if COM port names match
            if port == port_from_comport_list:
                pyserial_comport_hardware_id = hwid

        # If no COM ports match
        if pyserial_comport_hardware_id is None:
            raise ValueError(f"Unable to find serial ftdi index for {port}")

        # Get COM port serial device id from hardware id list of pyserial
        pyserial_comport_serial_id = (pyserial_comport_hardware_id.split("SER=", 1)[1])[:8]

        # Check ftdi device index list for COM port that matches user selected COM port
        for index in range(total_com_port_count):
            try:
                ftdi_target = ftdi.open(index)
                ftdi_comport_serial_id = str((ftdi_target.getDeviceInfo())["serial"])
                ftdi_comport_serial_id = (ftdi_comport_serial_id.split("b'", 1)[1]).split("'", 1)[0]

                # If serial device ids match between pyserial and ftd2xx driver port selections,
                # return ftd2xx port index
                if pyserial_comport_serial_id == ftdi_comport_serial_id:
                    ftdi_target.close()
                    logger.info(f"Found FTDI Device at {pyserial_comport_serial_id, index}")
                    return index
                ftdi_target.close()
            except ftdi.DeviceError:
                logger.warning(f"Could not open {index}")

        raise ValueError(f"Unable to find serial ftdi index for {port}")


    async def toggle_direct(self, state):
        if state:
            if self._direct:
                logger.warning("Already set to direct mode")
                return

            self._direct = True
            # 1. Stop multiprocessing server connection
            logger.debug(f"_stop()")

            self.send_freq_queue = bytearray(b'')
            if self.device_connection:
                await self.device_connection.stop()

                await asyncio.sleep(0.2)

            # 2. Create serial/socket
            if self.type == CommType.SERIAL:
                try:
                    index = self._parse_ftdi_ports(self.serial_port)

                    byte_size = ftdidef.BITS_8
                    parity = ftdidef.PARITY_NONE
                    stop_bits = ftdidef.STOP_BITS_1

                    if self.stop_bits == serial.STOPBITS_ONE:
                        stop_bits = ftdidef.STOP_BITS_1
                    elif self.stop_bits == serial.STOPBITS_TWO:
                        stop_bits = ftdidef.STOP_BITS_2
                    if self.parity == serial.PARITY_NONE:
                        parity = ftdidef.PARITY_NONE
                    elif self.parity == serial.PARITY_EVEN:
                        parity = ftdidef.PARITY_EVEN
                    elif self.parity == serial.PARITY_ODD:
                        parity = ftdidef.PARITY_ODD
                    elif self.parity == serial.PARITY_MARK:
                        parity = ftdidef.PARITY_MARK
                    elif self.parity == serial.PARITY_SPACE:
                        parity = ftdidef.PARITY_SPACE

                    if self.byte_size == serial.EIGHTBITS:
                        byte_size = ftdidef.BITS_8
                    elif self.byte_size == serial.SEVENBITS:
                        byte_size = ftdidef.BITS_7

                    self.serial_device = ftdi.open(index)
                    self.serial_device.setBaudRate(self.baudrate)
                    self.serial_device.setDataCharacteristics(byte_size, stop_bits, parity)
                    self.serial_device.setTimeouts(2, 2)
                    self.serial_device.setLatencyTimer(2)

                    logger.info("Successfully opened ftdi")

                except Exception:
                    logger.warning(f"Could not open ftdi, reverting to pyserial")
                    try:
                        self.serial_device = serial.Serial(self.serial_port, self.baudrate, self.byte_size, self.parity,
                                                           self.stop_bits)
                        self.serial_device.timeout = 0
                    except Exception:
                        return

            elif self.type == CommType.UDP:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                self.socket.setblocking(False)

                self.socket.bind(("", 0))
            else:
                return

        else:
            if not self._direct:
                logger.warning("Already not in direct mode")
                return
            # 1. Close serial/socket
            if self.type == CommType.SERIAL:
                self.serial_device.close()

            elif self.type == CommType.UDP:
                self.socket.close()
            else:
                return
            # 2. Restart process connection
            if self.device_connection:
                await self.device_connection.start()

            self._direct = False
        pass

    def create_master_arm_slave(self, connection):
        logger.debug(f"create_master_arm_slave({connection})")

        if self.task.done():
            if self.device_connection:
                self.device_connection.create_master_arm_slave(connection)
        else:
            asyncio.create_task(self.async_create_master_arm_slave(connection))

    async def async_create_master_arm_slave(self, connection):
        logger.debug(f"async_create_master_arm_slave({connection})")
        await asyncio.wait_for(self.task, 10.5)
        if self.device_connection:
            self.device_connection.create_master_arm_slave(connection)

    def remove_master_arm_slave(self):
        logger.debug(f"remove_master_arm_slave()")
        if self.device_connection:
            self.device_connection.remove_master_arm_slave()

    a = False
    def send_control(self, device_id, packet_id, data_in, append_only=False):

        if device_id is None or packet_id is None or data_in is None:
            return 0
        if self.direct:

            data = encode_packet(device_id, packet_id, data_in)
            if self.type == CommType.SERIAL:
                if self.serial_device:
                    try:
                        self.serial_device.write(data)
                    except serial.SerialException as e:
                        logger.warning(f"Direct serial connection write error {e}")

            elif self.type == CommType.UDP:
                if self.socket:
                    try:
                        self.socket.sendto(data, (self.socket_ip, self.socket_port))
                    except Exception as e:
                        logger.warning(f"Direct UDP connection write error {e}")
            else:
                raise NotImplementedError

        elif self.device_connection:
            self.device_connection.send_control(device_id, packet_id, data_in)
            return 1
        return 0

    packet_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, Union[List[float], List[int]]], None]]] = None

    def add_packet_callback(self, device_id: int, packet_id: int,
                            callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        key = (device_id, packet_id)
        if key in self.packet_callbacks:
            self.packet_callbacks[key].add(callback)
        else:
            self.packet_callbacks[key] = {callback}

        if self.device_connection:
            self.device_connection.add_packet_callback(device_id, packet_id, callback)

    def remove_packet_callback(self, device_id: int, packet_id: int,
                               callback: Callable[[int, int, Union[List[float], List[int]]], None]):

        key = (device_id, packet_id)
        if key in self.packet_callbacks:
            if callback in self.packet_callbacks[key]:
                self.packet_callbacks[key].remove(callback)
                if len(self.packet_callbacks[key]) == 0:
                    del self.packet_callbacks[key]

        if self.device_connection:
            self.device_connection.remove_packet_callback(device_id, packet_id, callback)

    heartbeat_callbacks: Dict[Tuple[int, int], Dict[Callable[[int, int, Union[List[float], List[int]]], None], float]] = None

    def add_heartbeat_callback(self, device_id: int, packet_id: int,
                            callback: Callable[[int, int, Union[List[float], List[int]]], None], frequency: float = 10.0):
        key = (device_id, packet_id)
        if key in self.heartbeat_callbacks:
            self.heartbeat_callbacks[key][callback] = frequency
        else:
            self.heartbeat_callbacks[key] = {callback: frequency}

        if self.device_connection:
            self.device_connection.add_heartbeat_callback(device_id, packet_id, callback, frequency)

    def remove_heartbeat_callback(self, device_id: int, packet_id: int,
                               callback: Callable[[int, int, Union[List[float], List[int]]], None]):

        key = (device_id, packet_id)
        if key in self.heartbeat_callbacks:
            if callback in self.heartbeat_callbacks[key]:
                self.heartbeat_callbacks[key].pop(callback)

                if len(self.heartbeat_callbacks[key]) == 0:
                    del self.heartbeat_callbacks[key]

        if self.device_connection:
            self.device_connection.remove_heartbeat_callback(device_id, packet_id, callback)

