from enum import Enum


class CommType(Enum):
    ''' CommType
    Numerical list respresenting different communications methods
    '''
    NONE = 0
    SERIAL = 1
    UDP = 2
    TCP = 3
    SERIAL_FTDI = 4     # Windows only
    SERIAL_LIBFTDI = 5  # Linux only