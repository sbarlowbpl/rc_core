import asyncio
import multiprocessing
import time
from asyncio import ReadTransport
from asyncio.proactor_events import BaseProactorEventLoop
from multiprocessing.connection import Connection
from typing import Optional, Set, Callable, Union

from RC_Core import rc_logging

logger = rc_logging.getLogger(__name__)


class PipeProtocol(asyncio.Protocol):
    # Callback for any bytes received
    bytes_callbacks: Set[Callable[[bytes], None]] = None

    # Callbacks for lost connections
    connection_lost_callbacks: Set[Callable[[], None]] = None

    # Callbacks for connections that are made
    connection_made_callbacks: Set[Callable[[], None]] = None

    @property
    def connected(self) -> bool:
        raise NotImplementedError

    def connection_made(self, transport: ReadTransport) -> None:
        raise NotImplementedError

    def connection_lost(self, exc: Optional[Exception]) -> None:
        raise NotImplementedError

    def data_received(self, data: bytes) -> None:
        raise NotImplementedError

    def write(self, data: bytes):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError


class SelectorPipeProtocol(PipeProtocol):
    connection_loop_period = 0.01  # seconds

    def __init__(self, pipe):

        self._loop = asyncio.get_event_loop()
        self._pipe: multiprocessing.connection.Connection = pipe

        # Callback for any bytes received
        self.bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for lost connections
        self.connection_lost_callbacks: Set[Callable[[], None]] = set()

        # Callbacks for connections that are made
        self.connection_made_callbacks: Set[Callable[[], None]] = set()

        self._loop.create_task(self._read_loop())

    @property
    def connected(self) -> bool:
        return not self._pipe.closed

    async def _read_loop(self):

        while self.connected:
            start_time = time.perf_counter()
            # await asyncio.sleep(0)
            try:
                data = b''
                while self._pipe.poll(0.0):  # and time.perf_counter() - start_time < self.connection_loop_period/2:
                    data += self._pipe.recv_bytes()

                if data:
                    self.data_received(data)
            except BrokenPipeError as e:
                logger.warning(f"Pipe Broken {e}")
                self.close()

            await asyncio.sleep(self.connection_loop_period - (time.perf_counter() - start_time))

    def connection_made(self, transport) -> None:
        """ Keep record of transport and prepare to receive data """
        logger.debug(f"Connection Made")
        for callback in self.connection_made_callbacks:
            callback()

    def connection_lost(self, exc: Optional[Exception]) -> None:
        logger.debug(f"Connection Lost {exc}")
        for callback in self.connection_lost_callbacks:
            callback()
        self.close()

    def data_received(self, data: bytes) -> None:
        for callback in self.bytes_callbacks:
            callback(data)

    def write(self, data: bytes):
        self._pipe.send_bytes(data)

    def close(self):
        self._pipe.close()


class ProactorPipeProtocol(PipeProtocol):
    transport: Union[ReadTransport, None] = None

    def __init__(self):
        # Callback for any bytes received
        self.bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for lost connections
        self.connection_lost_callbacks: Set[Callable[[], None]] = set()

        # Callbacks for connections that are made
        self.connection_made_callbacks: Set[Callable[[], None]] = set()

    @property
    def connected(self) -> bool:
        if self.transport and self.transport._sock:
            return not self.transport._sock.closed
        else:
            return False

    # def connection_made(self, transport: ReadTransport, *args) -> None:
    #     """ Keep record of transport and prepare to receive data """
    #     logger.debug(f"Connection Made")
    #     self.transport = transport
    #     for callback in self.connection_made_callbacks:
    #         callback()

    def connection_made(self, transport) -> None:

        """ Keep record of transport and prepare to receive data """
        pass

        self.transport = transport
        logger.debug(f"Connection Made")
        for callback in self.connection_made_callbacks:
            callback()

    def connection_lost(self, exc: Optional[Exception]) -> None:
        logger.debug(f"Connection Lost")
        self.transport = None
        for callback in self.connection_lost_callbacks:
            callback()
        self.close()

    def data_received(self, data: bytes) -> None:
        while self.transport._sock.poll(0):
            data += self.transport._sock.recv_bytes()

        for callback in self.bytes_callbacks:
            callback(data)

    def write(self, data: bytes):
        if self.transport:
            self.transport.write(data)
            # self.transport._sock.send_bytes(data)

    def close(self):
        if self.transport:
            self.transport.close()
        self.transport = None


async def create_pipe_protocol_connection(pipe: Connection) -> PipeProtocol:
    # loop: BaseProactorEventLoop
    conn: PipeProtocol
    loop = asyncio.get_event_loop()

    if pipe.closed:
        raise ConnectionRefusedError("Pipe connection is Closed")

    if not isinstance(loop, BaseProactorEventLoop):
        conn = SelectorPipeProtocol(pipe)
        return conn

    else:

        # trans, conn = await loop.connect_read_pipe(
        #     ProactorPipeProtocol,
        #     pipe
        # )
        conn = ProactorPipeProtocol()
        trans = loop._make_duplex_pipe_transport(pipe,
                                                 conn)
        conn.transport = trans
        return conn
