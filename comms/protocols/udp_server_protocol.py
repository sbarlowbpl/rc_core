import asyncio
import logging
from typing import Set, Callable, Tuple, Optional

from colorlog import ColoredFormatter

from RC_Core import rc_logging

logger = rc_logging.getLogger(__name__)


class UDPServerProtocol(asyncio.DatagramProtocol):
    transport = None
    address: Optional[Tuple[str, int]] = None

    def __init__(self):
        self.bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for lost connections
        self.connection_lost_callbacks: Set[Callable[[], None]] = set()

        # Callbacks for connections that are made
        self.connection_made_callbacks: Set[Callable[[], None]] = set()

    @property
    def connected(self) -> bool:
        if self.transport:
            return True
        else:
            return False

    def connection_made(self, transport):
        """ Keep record of transport and prepare to receive data """
        logger.debug(f"Connection Made")
        self.transport = transport
        for callback in self.connection_made_callbacks:
            callback()

    def connection_lost(self, exc):
        logger.debug(f"Connection Lost {exc}")
        self.transport = None
        for callback in self.connection_lost_callbacks:
            callback()
        self.close()

    def datagram_received(self, data: bytes, addr: Tuple[str, int]) -> None:
        self.address = addr
        for callback in self.bytes_callbacks:
            callback(data)

    def write(self, data: bytes):
        if self.address is None:
            return
        self.transport.sendto(data, self.address)

    def close(self):
        if self.transport:
            self.transport._sock.close()
            self.transport.close()
        self.transport = None
        pass




async def create_udp_server_protocol_connection(ip_address, port):

    loop = asyncio.get_event_loop()
    trans, conn = await loop.create_datagram_endpoint(
        UDPServerProtocol,
        local_addr=(ip_address, port),
        # remote_addr=(ip_address, port)
    )
    # conn.transport = trans
    return conn