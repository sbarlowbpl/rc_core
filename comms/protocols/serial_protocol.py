import asyncio
import logging
from typing import Set, Callable, Optional

from colorlog import ColoredFormatter

from RC_Core import rc_logging
from RC_Core.comms import serial_asyncio

logger = rc_logging.getLogger(__name__)

class SerialProtocol(asyncio.Protocol):
    """
    Serial Protocol:
    Use create_serial_protocol_connection to create a connection.
    When connected, receiving data or lost connection corresponding callbacks occur.
    To bind to said callbacks, add to the callback sets with your callback.
    """
    transport: serial_asyncio.SerialTransport = None

    def __init__(self):
        logger.debug("Serial Connection Initialised")

        # Callback for any bytes received
        self.bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for lost connections
        self.connection_lost_callbacks: Set[Callable[[], None]] = set()

        # Callbacks for connections that are made
        self.connection_made_callbacks: Set[Callable[[], None]] = set()

    @property
    def connected(self) -> bool:
        """ Property to return whether the serial port is open"""
        if self.transport and self.transport.serial and self.transport.serial.is_open:
            return True
        else:
            return False

    def connection_made(self, transport: serial_asyncio.SerialTransport) -> None:
        """ Callback when connection is made"""
        logger.debug(f"Serial Connection Made")
        self.transport = transport
        for callback in self.connection_made_callbacks:
            callback()

    def connection_lost(self, exc: Optional[Exception]) -> None:
        """ Callback called when connection is lost"""
        logger.debug(f"Serial Connection Lost")
        for callback in self.connection_lost_callbacks:
            callback()
        self.close()

    def write(self, data: bytes):
        """ Write to serial with bytes"""
        if self.transport:
            self.transport.serial.write(data)
            self.transport.flush()

    def data_received(self, data: bytes) -> None:
        """ Callback triggered when data is received"""
        for callback in self.bytes_callbacks:
            callback(data)

    def close(self):
        """ Close Connection"""
        self.bytes_callbacks = set()
        self.connection_lost_callbacks = set()
        self.connection_made_callbacks = set()

        if self.transport:
            self.transport.close()


async def create_serial_protocol_connection(*args, **kwargs) -> SerialProtocol:
    loop = asyncio.get_event_loop()
    transport, conn = await serial_asyncio.create_serial_connection(
        loop,
        SerialProtocol,
        *args,
        **kwargs
    )

    conn.transport = transport

    return conn