import asyncio
import logging
import time
from asyncio import BaseTransport, Transport
from typing import Set, Callable, Tuple, Optional

from colorlog import ColoredFormatter

import rc_logging
logger = rc_logging.getLogger(__name__)


class TCPProtocol(asyncio.Protocol):

    transport: Optional[Transport] = None

    def __init__(self):
        self.bytes_callbacks: Set[Callable[[bytes], None]] = set()

        # Callbacks for lost connections
        self.connection_lost_callbacks: Set[Callable[[], None]] = set()

        # Callbacks for connections that are made
        self.connection_made_callbacks: Set[Callable[[], None]] = set()

    @property
    def connected(self) -> bool:
        if self.transport:
            return True
        else:
            return False

    def connection_made(self, transport: Transport):
        """ Keep record of transport and prepare to receive data """
        logger.debug(f"TCP Protocol: Connection Made")
        self.transport = transport
        for callback in self.connection_made_callbacks:
            callback()

    def connection_lost(self, exc):
        logger.debug(f"Connection Lost {exc}")
        self.transport = None
        for callback in self.connection_lost_callbacks:
            callback()
        self.close()

    def data_received(self, data: bytes) -> None:
        for callback in self.bytes_callbacks:
            callback(data)

    def write(self, data: bytes):
        self.transport.write(data)

    def close(self):
        logger.debug("close()")
        if self.transport:
            self.transport.close()
        self.transport = None
        pass


async def create_tcp_protocol_connection(ip: str, port: int):

    logger.debug(f"create_tcp_protocol_connection({ip, port})")
    loop = asyncio.get_running_loop()
    protocol: TCPProtocol
    start_time = time.perf_counter()
    transport, protocol = await loop.create_connection(TCPProtocol, ip, port)
    logger.debug(f"Creating TCP_Protocol took {time.perf_counter() - start_time: .2f} seconds")
    return protocol

