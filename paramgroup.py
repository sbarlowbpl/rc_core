from enum import Flag, auto


class ParamGroup(Flag):

    NONE = 0
    ALPHA = auto()
    BRAVO = auto()
    MA_ROTATE = auto()
    MA_END = auto()
    MA_BASE = auto()
    CONTROLS = auto()
    GAINS = auto()
    LIMITS = auto()
    PARAMS = auto()
    SETTINGS = auto()
    KINEMATICS = auto()
    STATUS = auto()


def is_in_group(flag: ParamGroup, group: ParamGroup) -> bool:
    return bool(flag & group)


def get_groups():
    groups = []
    for group in list(ParamGroup):
        if group is not ParamGroup.NONE:
            groups.append(group)
    return groups


class PGroupsList:
    '''PGroupsList class: Lists all P Groups for ConfigUI.
    '''
    P_GROUPS_LIST = []
    P_GROUPS_CUSTOM_LIST = []

    @staticmethod
    def init_p_groups_list(packet_prototype_list):
        # Get the parameter group list
        if PGroupsList.P_GROUPS_LIST == [] or len(PGroupsList.P_GROUPS_LIST) <= 0:
            PGroupsList.P_GROUPS_LIST = populate_default_param_groups(packet_prototype_list)

    # @staticmethod
    # def populate_p_groups(packets):
    #     for pgroup in PGroupsList.P_GROUPS_LIST:
    #         for packet_name in pgroup['packet_names']:
    #             try:
    #                 packet = getattr(packets, packet_name)
    #                 if packet:
    #                     pgroup['packets'].append(packet)
    #             except Exception as e:
    #                 raise Exception('Could not get packet with name', packet_name, 'e:', e)
    #
    # @staticmethod
    # def populate_p_group_names_from_p_groups_packet_ids(packets):
    #     for pgroup in PGroupsList.P_GROUPS_LIST:
    #         pgroup['packet_names'] = []
    #         for packet_id in pgroup['packet_ids']:
    #             for packet in packets:
    #                 if packet[1] == packet_id:
    #                     pgroup['packet_names'].append(packet[0])
    #                     break


def populate_default_param_groups(p):
    # Get all the default groups from enum class
    groups = list(ParamGroup)

    # Construct the default parameter group list
    param_groups_list = []
    for group in groups:
        if group is not ParamGroup.NONE:
            # Set default parameter group to the fist
            default = False
            if group.value == 1:
                default = True

            name = group.name
            button_name = group.name.lower().replace('_', ' ')
            packet_names = []
            packet_ids_list = []
            packets = []
            for packet in p.get_all():
                if is_in_group(packet.parameter_groups, group):
                    pprotoname = p.get_name_from_packetprototype(packet)
                    if pprotoname is not None:
                        packet_ids_list.append(packet.packet_id)
                        packet_names.append(pprotoname)
                        packets.append(packet)

            # Construct group dictionary
            group_dict = {
                'name': name,
                'default': default,
                'button_name': button_name,
                'packet_names': packet_names,
                'packet_ids': packet_ids_list,
                'packets': packets
            }

            # Append dictionary to param groups list
            param_groups_list.append(group_dict)

    return param_groups_list


if __name__ == '__main__':

    # TODO: rewrite as unittest
    def test_combine_flags():
        my_flag = ParamGroup.ALPHA | ParamGroup.BRAVO
        print(is_in_group(my_flag, ParamGroup.PARAMS))
        print(is_in_group(my_flag, ParamGroup.ALPHA))

    test_combine_flags()

