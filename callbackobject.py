import os, sys, time

from RC_Core.RS1_hardware import PacketID

# import rc_logging
#
import logging
logger = logging.getLogger(__name__)


class TemporaryCallbackIndex:
    TC_DEVICE_ID: int = 0
    TC_PACKET_ID: int = TC_DEVICE_ID + 1
    TC_DATA: int = TC_PACKET_ID + 1
    TC_TIME: int = TC_DATA + 1
    TC_CALLBACK: int = TC_TIME + 1

TC_DEVICE_ID: int = TemporaryCallbackIndex.TC_DEVICE_ID
TC_PACKET_ID: int = TemporaryCallbackIndex.TC_PACKET_ID
TC_DATA: int = TemporaryCallbackIndex.TC_DATA
TC_TIME: int = TemporaryCallbackIndex.TC_TIME
TC_CALLBACK: int = TemporaryCallbackIndex.TC_CALLBACK



class CallbackObject():
    '''Callback Object Class

    Each callback required must use this class as its container, rather than a list which has been deprecated.
    Class handles permanent and temporary callbacks target function handling, timeouts, and retry attempts.
    Callbacks are primarily managed by RC_Core/connectionmanager.py/ConnectionManager()
    '''

    def __init__(self, connection, device_id: int, packet_id: int, callback, data: list =None,
                 timeout: float=0.5, timeout_callback=None, retry_counter: int=0):
        '''
        Create callback object with given specifications.
        :param connection: CommConnection object used for communications.
        :param device_id: Device id that the callback requires.
        :param packet_id: Packet id that the callback requires.
        :param callback: Target callback function.
        :param data: (Optional) If set, the sent data is compared to incoming data.
        :param timeout: (Optional) Timeout for this callback. Defaults to 0.5s. If timeout reached,
        callback object is removed and timeout_callback is called if set.
        :param timeout_callback: (Optional) If timeout is reached, trigger the target function.
        :param retry_counter: (Optional) Requests can be re-sent if retry_counter is greater than 0,
        with retry_counter retry attempts
        '''
        if data is None:
            data = []
        self.connection = connection
        self.device_id: int = device_id
        self.packet_id: int = packet_id
        self.callback = callback
        self.sent_data: list = data
        self.timeout: float = timeout
        self.timeout_callback = timeout_callback
        self.retry_counter: int = retry_counter
        self.time: float = time.time()

    def check_and_run_callback(self, device_id: int, packet_id: int, data: tuple) -> bool:
        '''
        Checks whether the device_id and packet_id match this object's device_id and packet_id.
        If condition is met, it attempts to run this object's target callback function.
        :param device_id: Device ID to compare
        :param packet_id: Packet ID to compare
        :param data: Incoming data
        :return triggered_state: Returns True if conditions met
        '''
        if self.device_id in [device_id, 0x0F, 0x1F, 0xFF] and packet_id == packet_id:
            self._run_callback(data)
            return True
        return False

    def _run_callback(self, data: tuple):
        '''
        Runs the callback function if callable.
        :param data: Incoming data to send to the target callback function.
        '''
        if self.callback and callable(self.callback):
            try:
                ret = self.callback(self.device_id, self.packet_id, data)
                if ret is None or not ret:
                    self.timeout = 0
                    self.timeout_callback = None
                    self.retry_counter = -1
            except Exception as e:
                logger.warning(f"Callback error for {self.device_id, self.packet_id} {e}")
        else:
            self.timeout = 0
            self.timeout_callback = None
            self.retry_counter = -1

    def temporary_callback_functionality(self):
        '''
        Handles timeout functionality if this object is a temporary callback.
        '''
        self.retry_counter -= 1
        if self.retry_counter >= 0:
            # If retry_counter is set greater than 0, request data again.
            if self.connection and self.connection.connected:
                self.connection.send_queue.append([self.device_id, PacketID.REQUEST, bytearray([self.packet_id]), None])
        else:
            if self.timeout_callback:
                return
            else:
                self.timeout = 0
                self.timeout_callback = None

    def check_and_run_timeout_callback(self) -> bool:
        '''
        Handles timeout functionality if this object is a temporary callback.
        :return triggered_state: Returns True timeout reached.
        '''
        if time.time() > (self.time + self.timeout):
            if self.timeout_callback and callable(self.timeout_callback):
                self.timeout_callback()
            return True
        else:
            return False
