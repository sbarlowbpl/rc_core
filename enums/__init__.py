from .modes import Mode

from .packets import PacketID


class CollisionType:
    NO_COLLISION = 0
    SIBLING_COLLISION = 1
    SELF_COLLISION = 2
    OBSTACLE_COLLISION = 3


class CommsProtocol:
    BPL = 0
    BPSS = 1
