"""
modes.py

Contains Modes enum listing values of all Modes.

THIS FILE IS GENERATED BY C:/Users/John Sumskas/Documents/blueprintlab/reachcontrol_build/RC_Core/construct_enum/construct_enums.py
DO NOT EDIT THIS FILE.
Instead, edit operatingMode.h in reach-drivers submodule 
then run C:/Users/John Sumskas/Documents/blueprintlab/reachcontrol_build/RC_Core/construct_enum/construct_enums.py
to update this file.
"""
from enum import IntEnum

from .extenum import UniqueValueEnum


class Mode(IntEnum):
    STANDBY = 0x00
    DISABLED = 0x01
    POSITION_CONTROL = 0x02
    VELOCITY_CONTROL = 0x03
    CURRENT_CONTROL = 0x04
    OPEN_LOOP_CONTROL = 0x05
    INIT = 0x06
    CALIBRATE = 0x07
    FACTORY = 0x08
    COMPLIANT = 0x09
    GRIP = 0x0A
    STALL_DRIVE = 0x0B
    CALIBRATE_TEST = 0x0C
    AUTO_LIMIT = 0x0D
    CALIBRATE_OUTER = 0x0E
    VELOCITY_CONTROL_INNER = 0x0F
    RELATIVE_POSITION_CONTROL = 0x12
    INDEXED_POSITION_CONTROL = 0x13
    POSITION_PRESET = 0x14
    ZERO_VELOCITY = 0x15
    FACTORY_CURRENT_CONTROL = 0x16
    KM_END_POS = 0x17
    KM_END_VEL = 0x18
    KM_END_POS_LOCAL = 0x19
    KM_END_VEL_LOCAL = 0x1A
    VELOCITY_FACTORY = 0x1B
    POSITION_VELOCITY_CONTROL = 0x1C
    POSITION_HOLD = 0x1D
    ABS_POSITION_VELOCITY_CONTROL = 0x1E
    CALIBRATE_OVER_SERIAL = 0x1F
    CALIBRATE_CURRENT_CONTROL = 0x20
    CALIBRATE_INNER_OVER_SERIAL = 0x21
    CALIBRATE_OUTER_OVER_SERIAL = 0x22
    KM_END_VEL_CAMERA = 0x23
    POS_SEQUENCE = 0x24
    DIRECT_CONTROL = 0x25
    PASSIVE = 0x26
    EXIT_PASSIVE = 0x27
    KM_END_VEL_WORK = 0x28
