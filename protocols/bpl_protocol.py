""" bpl_protocol.py

The BPL Protocol class is used in conjunction with RC_Core .connection objects to send and encode bpl packets.
"""
import asyncio
import re
import struct
import time
import traceback
from copy import copy
from math import inf
from typing import Callable, Dict, Tuple, List, Optional, Union, Set
import logging

from cobs import cobs
from crcmod import crcmod

from colorlog import ColoredFormatter

from RC_Core import rc_logging
from RC_Core.bus_state import BusState
from RC_Core.connections import ConnectionBase, SerialConnection
from RC_Core.enums import CommsProtocol
from RC_Core.enums.packets import PacketID
from RC_Core.packetid import Packets

logger = rc_logging.getLogger(__name__)

CRC8_FUNC = crcmod.mkCrcFun(0x14D, initCrc=0xFF, xorOut=0xFF)

HALF_DUPLEX_TIMEOUT = 0.1  # seconds

PRIORITY_TIMEOUT = 0.1

BPL_PROTOCOL = CommsProtocol.BPL
BPSS_PROTOCOL = CommsProtocol.BPSS


class _PacketWaiter:
    """
    This Class is used to help wait for callbacks with a response

    Add instruction_callback to callbacks, then sechedule instruction waiter with a timeout using
    asyncio.wait_for(timeout). This will asyncronously wait for a response, unless timeout is called.
    """
    packet_data: Union[List[float], List[int]] = None

    def __init__(self):
        self.received_response_event: asyncio.Event = asyncio.Event()

    def packet_callback(self, _, __, data):
        # logger.debug(f"received_packet({_, __, data})")
        self.received_response_event.set()
        self.packet_data = data

    async def packet_waiter(self):
        await self.received_response_event.wait()


class BPLProtocol:
    """ BPL Protocol is to handle the sending and receiving of packets in the bpl protocol format.
    This class is agnostic to the data format of packet. It will assume it is receiving a list of bytes.

    """

    protocol_type = BPL_PROTOCOL

    _incomplete_packets = b''

    _connection: ConnectionBase = None
    _packet_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, bytes], None]]] = None

    def __init__(self, connection: ConnectionBase):
        self._packet_callbacks = {}

        self._heartbeat_callbacks = {}

        self._connection = connection

        self._connection.add_bytes_callback(self._bytes_callback)

    @property
    def connection(self):
        return self._connection

    @property
    def half_duplex(self):
        return self._half_duplex

    @half_duplex.setter
    def half_duplex(self, state):
        if self._half_duplex == state:
            return

        if state:
            logger.info("Setting Half Duplex")
            self._half_duplex = True

            if self._half_duplex_task and not self._half_duplex_task.done():
                self._half_duplex_task.cancel()

            self._half_duplex_task = asyncio.create_task(self.half_duplex_loop())
        if not state:
            self._half_duplex = False
            return

    # half duplex loop
    _half_duplex = False
    # half_duplex_device_ids = [0x0D]
    queued_half_duplex_packets: List[Tuple[int, int, Union[bytes, bytearray]]] = []

    # Half duplex bytes (e.g from passthrough)
    queued_half_duplex_bytes: bytes = b''

    _half_duplex_task: asyncio.Task = None

    async def half_duplex_loop(self):
        logger.info("Starting Half Duplex Loop")
        while self.half_duplex:
            packet_bytes = b''
            # logger.info("Running Half duplex loop")

            packet_bytes += self.queued_half_duplex_bytes
            self.queued_half_duplex_bytes = b''

            if not self.queued_half_duplex_packets:
                await asyncio.sleep(0.001)
                continue

            request_packets: Dict[int, Set[int]] = {}

            for device_id, packet_id, data in self.queued_half_duplex_packets:

                if packet_id != PacketID.REQUEST:
                    packet_bytes += encode_packet(device_id, packet_id, data, protocol_type=self.protocol_type)
                else:
                    if device_id in request_packets:
                        request_packets[device_id] = request_packets[device_id].union(set(data))
                    else:
                        request_packets[device_id] = set(data)

            self.queued_half_duplex_packets = []

            packet_waiter_futures = []

            for device_id, data in request_packets.items():
                data_list = list(data)
                for i in range(((len(data)//10) + 1)):
                    packet_bytes += encode_packet(device_id, PacketID.REQUEST, bytes(data_list[i:i+10]), protocol_type=self.protocol_type)
            self._connection.send_bytes(packet_bytes)

            for device_id, packet_ids in request_packets.items():
                for packet_id in packet_ids:
                    packet_waiter_futures.append(asyncio.create_task(self.half_duplex_wait_for_packet(device_id, packet_id, timeout=HALF_DUPLEX_TIMEOUT)))

            # logger.debug(f"waiting for {len(request_packets)} futures")
            if packet_waiter_futures:
                start_time = time.time()
                await asyncio.wait(packet_waiter_futures, return_when=asyncio.ALL_COMPLETED)
                dt = time.time() - start_time

            else:
                await asyncio.sleep(0.001)

    async def half_duplex_wait_for_packet(self, device_id, packet_id, timeout=0.2):
        start_time = time.time()
        pw = _PacketWaiter()
        self.add_packet_callback(device_id, packet_id, pw.packet_callback)
        try:
            await asyncio.wait_for(pw.packet_waiter(), timeout=timeout)
            if pw.packet_data:
                self.remove_packet_callback(device_id, packet_id, pw.packet_callback)
                # if time.time() - start_time > 0.01:
                #     print(f"Got request in {time.time() - start_time}")
                return pw.packet_data

        except asyncio.TimeoutError:
            self.remove_packet_callback(device_id, packet_id, pw.packet_callback)
            # logger.debug(f"Half duplex request Timed out {device_id, packet_id}")
            # raise TimeoutError(f"Did not receive data from {device_id, packet_id} withing {timeout}")

    async def wait_for_packet(self, device_id, packet_id, timeout=0.2):
        pw = _PacketWaiter()
        self.add_packet_callback(device_id, packet_id, pw.packet_callback)
        try:
            await asyncio.wait_for(pw.packet_waiter(), timeout=timeout)
            if pw.packet_data:
                self.remove_packet_callback(device_id, packet_id, pw.packet_callback)
                return pw.packet_data
        except asyncio.TimeoutError:
            self.remove_packet_callback(device_id, packet_id, pw.packet_callback)
            raise TimeoutError(f"Did not receive data from {device_id, packet_id} within {timeout}")

    @connection.setter
    def connection(self, connection: ConnectionBase):
        if connection is self._connection:
            logger.warning(f"{self} is already set to this connection.")
            return

        self._connection.remove_bytes_callback(self._bytes_callback)
        self._connection = connection

        if self._connection is None:
            if self._half_duplex_task and not self._half_duplex_task.done():
                self._half_duplex_task.cancel()

        elif self.half_duplex:
            if self._half_duplex_task and self._half_duplex_task.done():
                self._half_duplex_task = asyncio.create_task(self.half_duplex_loop())
        self._connection.add_bytes_callback(self._bytes_callback)

    def send_packet_bytes(self, device_id: int, packet_id: int, data: Union[bytes, bytearray]):
        """ Send a packet with the data in the form of bytes"""
        # Send a packet, with data as bytes

        if self._half_duplex:
            self.queued_half_duplex_packets.append((device_id, packet_id, data))
            return
        packet_bytes = encode_packet(device_id, packet_id, data, protocol_type=self.protocol_type)
        self._connection.send_bytes(packet_bytes)

    def send_bytes(self, data_bytes: bytes):
        if self._half_duplex:
            self._connection.send_bytes(data_bytes)
            return
        else:
            self._connection.send_bytes(data_bytes)
            return

    def add_packet_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, bytes], None]):
        """ Add a callback upon receiving device_id, packet_id to trigger callback.
        Callback call with arguments, device_id: int, packet_id: int and data: bytes
        """

        if (device_id, packet_id) not in self._packet_callbacks:
            self._packet_callbacks[(device_id, packet_id)] = {callback}
        else:
            self._packet_callbacks[(device_id, packet_id)].add(callback)

    def remove_packet_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, bytes], None]):
        """Remove callback from callbacks dictionary.
        If removing makes entry empty, remove entry too.

        """
        if (device_id, packet_id) in self._packet_callbacks:
            if callback in self._packet_callbacks[(device_id, packet_id)]:
                self._packet_callbacks[(device_id, packet_id)].remove(callback)
                if len(self._packet_callbacks[(device_id, packet_id)]) == 0:
                    del self._packet_callbacks[(device_id, packet_id)]
            else:
                logger.warning(f"remove_packet_callback({(device_id, packet_id)}), f{callback} not found in callbacks.")
            pass
        else:
            logger.warning(f"remove_packet_callback(), f{(device_id, packet_id)} not found in callbacks.")
        pass

    def _bytes_callback(self, data: bytes):
        """ Receiving bytes: Trigger all callbacks"""

        packets, self._incomplete_packets = packet_splitter(self._incomplete_packets + data)

        if not self._packet_callbacks or not packets:
            # Not packet callbacks. No point decoding packets. Return
            return

        # 1. Decode packet
        for packet in packets:
            device_id, packet_id, data = parse_packet(packet, protocol_type=self.protocol_type)
            if device_id == 0 and packet_id == 0:
                # Parsing packet error
                logger.warning("_bytes_callback(): parsing packet error")
                continue

            if (device_id, packet_id) in self._packet_callbacks:
                for callback in self._packet_callbacks[(device_id, packet_id)]:
                    callback(device_id, packet_id, data)

            # Callback to bind to all device_ids
            if (0xFF, packet_id) in self._packet_callbacks:
                for callback in self._packet_callbacks[(0xFF, packet_id)]:
                    callback(device_id, packet_id, data)

    def __del__(self):
        self._connection.remove_bytes_callback(self._bytes_callback)


class BPLPacketProtocol(BPLProtocol):
    """ BPL Protocol is to handle the sending and receiving of packets in the bpl protocol format.

    This class is assuming that it is sending registered packets.
    Callbacks are called with as a list of integers or a list of floats
    You can send packets with data as:
    - Int
    - Float
    - List of floats
    - List of Integers,
    - bytes
    - bytearray
    """
    _float_packet_ids: Set[int] = set(Packets().get_float_packet_ids())
    _packet_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, Union[List[float], List[int]]], None]]] = None

    def add_packet_callback(self,
                            device_id: int,
                            packet_id: int,
                            callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        """ Add a callback upon receiving device_id, packet_id to trigger callback.
        Callback call with arguments, device_id: int, packet_id: int and data: bytes
        """

        if (device_id, packet_id) not in self._packet_callbacks:
            self._packet_callbacks[(device_id, packet_id)] = {callback}
        else:
            self._packet_callbacks[(device_id, packet_id)].add(callback)

    def remove_packet_callback(self,
                               device_id: int,
                               packet_id: int,
                               callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        """Remove callback from callbacks dictionary.
        If removing makes entry empty, remove entry too.

        """
        if (device_id, packet_id) in self._packet_callbacks:
            if callback in self._packet_callbacks[(device_id, packet_id)]:
                self._packet_callbacks[(device_id, packet_id)].remove(callback)
                if len(self._packet_callbacks[(device_id, packet_id)]) == 0:
                    del self._packet_callbacks[(device_id, packet_id)]
            else:
                logger.warning(f"remove_packet_callback({(device_id, packet_id)}), f{callback} not found in callbacks.")
            pass
        else:
            logger.warning(f"remove_packet_callback(), f{(device_id, packet_id)} not found in callbacks.")
        pass

    def send_packet(self,
                    device_id: int,
                    packet_id: int,
                    data: Union[int, float, List[float], List[int], bytes, bytearray]):
        if isinstance(data, bytes) or isinstance(data, bytearray):
            # If packet_data is already in a bytes or bytes form, convert to byte array and leave it as is.
            packet_data = bytes(data)
        else:
            if isinstance(data, List):
                # If data is a list
                # List
                if packet_id in self._float_packet_ids:
                    # If data is a list of floats
                    if self.protocol_type == BPSS_PROTOCOL:
                        packet_data = b''
                        for val in data:
                            packet_data += struct.unpack('<I', struct.pack('<f', val))[0].to_bytes(4, byteorder='big')
                    else:
                        packet_data = bytes(struct.pack('%sf' % len(data), *data))
                else:
                    # If data is an integer list
                    try:
                        packet_data = bytes(data)
                    except TypeError:
                        logger.error(f"ERROR: Expected an integer list got {data}. Converting to integers")
                        data = [int(x) for x in data]
                        packet_data = bytes(data)
            else:
                # if data is a single number
                if packet_id in self._float_packet_ids:
                    # If data is a float
                    data = float(data)
                    if self.protocol_type == BPSS_PROTOCOL:
                        packet_data = struct.unpack('<I', struct.pack('<f', data))[0].to_bytes(4, byteorder='big')
                    else:
                        packet_data = struct.pack("f", data)
                else:
                    # If data is an integer
                    data = int(data)
                    packet_data = struct.pack("i", data)
        self.send_packet_bytes(device_id, packet_id, packet_data)

    async def request(self, device_id: int, request_packet_id: int, timeout=0.2):
        """ Asynchronously wait for a request and respond with the resulting data."""

        pw = _PacketWaiter()
        self.add_packet_callback(device_id, request_packet_id, pw.packet_callback)

        self.send_packet(device_id, PacketID.REQUEST, request_packet_id)

        try:
            await asyncio.wait_for(pw.packet_waiter(), timeout=timeout)
            if pw.packet_data:
                self.remove_packet_callback(device_id, request_packet_id, pw.packet_callback)
                return pw.packet_data
        except asyncio.TimeoutError:
            raise TimeoutError(f"Did not receive response from packet {device_id, request_packet_id}")
        self.remove_packet_callback(device_id, request_packet_id, pw.packet_callback)

    async def set(self, device_id: int, packet_id: int, data: Union[List[float], List[int], float, int], timeout=0.2) -> bool:
        if not isinstance(data, list):
            data = [data]

        self.send_packet(device_id, packet_id, data)

        try:
            response = await self.request(device_id, packet_id, timeout)

            if len(response) < len(data):
                logger.warning(f"set() Response: {response} does not match {data}")
                raise ValueError(f"set() Response: {response} does not match {data}")

            if packet_id in self._float_packet_ids:

                for idx, i in enumerate(data):
                    struct_response = struct.pack("f", response[idx])
                    struct_send = struct.pack("f", i)

                    if struct_response != struct_send:
                        raise ValueError(f"set() Response: {response} does not match {data}")

            else:
                if response[:len(data)] != data:
                    raise ValueError(f"set() Response: {response} does not match {data}")

            return True
        except TimeoutError:
            raise TimeoutError

    heartbeat_frequency = 20
    half_duplex_heartbeat_frequency = 5

    _heartbeat_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, Union[List[float], List[int]]], None]]] = None

    _heartbeat_loop_task: Optional[asyncio.Task] = None

    def add_heartbeat_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        if (device_id, packet_id) not in self._heartbeat_callbacks:
            self._heartbeat_callbacks[(device_id, packet_id)] = {callback}
        else:
            self._heartbeat_callbacks[(device_id, packet_id)].add(callback)

        if self._heartbeat_loop_task is None or self._heartbeat_loop_task.done():
            self._heartbeat_loop_task = asyncio.create_task(self.heartbeat_loop())

    def remove_heartbeat_callback(self, device_id: int, packet_id: int, callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        if (device_id, packet_id) in self._packet_callbacks:
            if callback in self._heartbeat_callbacks[(device_id, packet_id)]:
                self._heartbeat_callbacks[(device_id, packet_id)].remove(callback)
                if len(self._heartbeat_callbacks[(device_id, packet_id)]) == 0:
                    del self._heartbeat_callbacks[(device_id, packet_id)]
            else:
                logger.warning(f"remove_heartbeat_callback({(device_id, packet_id)}), f{callback} not found in callbacks.")
            pass
        else:
            logger.warning(f"remove_heartbeat_callback(), f{(device_id, packet_id)} not found in callbacks.")
        pass

    async def heartbeat_loop(self):

        while len(self._heartbeat_callbacks):
            # Request heartbeats

            for device_id, packet_id in self._heartbeat_callbacks.keys():
                self.send_packet(device_id, PacketID.REQUEST, [packet_id])

            if self.half_duplex:
                await asyncio.sleep(1/self.half_duplex_heartbeat_frequency)
            else:
                await asyncio.sleep(1/self.heartbeat_frequency)
        pass

    def _bytes_callback(self, data: bytes):
        """ Receiving bytes: Overwrite bytes callback to decode float and integer packets and return a list of either"""

        packets, self._incomplete_packets = packet_splitter(self._incomplete_packets + data, protocol_type=self.protocol_type)

        if not self._packet_callbacks or not packets:
            # Not packet callbacks. No point decoding packets. Return
            return

        # 1. Decode packet
        for packet in packets:
            if not packet:
                continue
            device_id, packet_id, data = parse_packet(packet, protocol_type=self.protocol_type)
            if device_id == 0 and packet_id == 0:
                # Parsing packet error
                logger.info("_bytes_callback(): parsing packet error")
                continue

            list_data: Union[List[float], List[int]] = []
            if packet_id in self._float_packet_ids:

                if self.protocol_type == BPSS_PROTOCOL:
                    for _idx in range(0, int(len(data) // 4)):
                        idx = _idx * 4
                        b = data[idx: idx + 4]
                        list_data.append(struct.unpack('<f', b)[0])
                else:
                    if ((len(data)//4) * 4) != len(data):
                        logger.warning(f"Incoming float packet: {device_id, packet_id, data}: length is not a multiple of 4")
                        continue
                    else:
                        list_data = list(struct.unpack(str(int(len(data)/4)) + "f", data[:(len(data)//4) * 4]))
            else:
                list_data = list(data)

            # Send to callbacks.
            if (device_id, packet_id) in self._packet_callbacks:
                for callback in copy(self._packet_callbacks[(device_id, packet_id)]):
                    try:
                        callback(device_id, packet_id, list_data)
                    except Exception as e:
                        logger.error(f"CALLBACK ERROR  {e}")
                        traceback.print_exc()

            if (device_id, packet_id) in self._heartbeat_callbacks:
                for callback in copy(self._heartbeat_callbacks[(device_id, packet_id)]):
                    try:
                        callback(device_id, packet_id, list_data)
                    except Exception as e:
                        logger.error(f"Heartbeat CALLBACK ERROR  {e}")
                        traceback.print_exc()

            # Callback to bound to all device_ids
            if (0xFF, packet_id) in self._packet_callbacks:
                for callback in copy(self._packet_callbacks[(0xFF, packet_id)]):
                    try:
                        callback(device_id, packet_id, list_data)
                    except Exception as e:
                        logger.error(f"CALLBACK ERROR  {e}")
                        traceback.print_exc()

            # Callbacks bound to all packet_ids for a specific device
            if (device_id, 0xFFF) in self._packet_callbacks:
                for callback in copy(self._packet_callbacks[(device_id, 0xFFF)]):
                    try:
                        callback(device_id, packet_id, list_data)
                    except Exception as e:
                        logger.error(f"CALLBACK ERROR  {e}")
                        traceback.print_exc()

            # Callbacks bound to all packet_ids for a all device_ids
            if (0xFF, 0xFFF) in self._packet_callbacks:
                for callback in copy(self._packet_callbacks[(0xFF, 0xFFF)]):
                    try:
                        callback(device_id, packet_id, list_data)
                    except Exception as e:
                        logger.error(f"CALLBACK ERROR  {e}")
                        traceback.print_exc()
                        

def packet_splitter(buff: bytes, protocol_type=BPL_PROTOCOL) -> Tuple[List[bytes], Optional[bytes]]:
    """
    Split packet using BPL serial protocol
    """
    if protocol_type == BPL_PROTOCOL:
        incomplete_packet = None
        packets = re.split(b'\x00', buff)
        if buff[-1] != b'0x00':
            incomplete_packet = packets.pop()
        return packets, incomplete_packet
    else:
        incomplete_packet = None
        packets = re.split(b'\r\n', buff)
        if buff[-1] != b'\r\n':
            incomplete_packet = packets.pop()
        for i in range(len(packets)):
            if b'$' in packets[i][1:]:
                start_index = packets[i].rfind(b'$', 1)
                packets[i] = packets[i][start_index:]
        return packets, incomplete_packet


def parse_packet(packet_in: Union[bytes, bytearray], protocol_type=BPL_PROTOCOL) -> Tuple[int, int, bytes]:
    """
    Parse the packet returning a tuple of [int, int, bytes].
     If unable to parse the packet, then return 0,0,b''.
    """
    if protocol_type == BPL_PROTOCOL:
        packet_in = bytearray(packet_in)

        if packet_in and len(packet_in) > 3:
            try:
                decoded_packet: bytes = cobs.decode(packet_in)
            except cobs.DecodeError as e:
                logger.info(f"parse_packet(): Cobs Decoding Error, {e}")
                return 0, 0, b''

            if decoded_packet[-2] != len(decoded_packet):
                logger.warning(f"parse_packet(): Incorrect length: length is {len(decoded_packet)} "
                               f"in {[hex(x) for x in list(decoded_packet)]}")
                return 0, 0, b''
            else:
                if CRC8_FUNC(decoded_packet[:-1]) == decoded_packet[-1]:
                    rx_data = decoded_packet[:-4]

                    device_id = decoded_packet[-3]
                    packet_id = decoded_packet[-4]
                    rx_data = rx_data
                    return device_id, packet_id, rx_data
                else:
                    logger.warning(f"parse_packet(): CRC error in {[hex(x) for x in list(decoded_packet)]} ")
                    return 0, 0, b''
        return 0, 0, b''
    elif protocol_type == BPSS_PROTOCOL:
        packet_in = bytearray(packet_in)
        if packet_in == b'' or len(packet_in) <= 10:
            return 0, 0, b''

        if packet_in[0:1] != b'$':
            logger.warning(f"Unable to parse bpss packet {packet_in}")
            return 0, 0, b''

        rx_crc = int(packet_in[-4:], 16)

        data_to_checksum = bytearray(packet_in[:-4])

        device_id = int(packet_in[1:3], 16)
        packet_id = int(packet_in[3:5], 16)

        # payload = packet_in[-4:4:-1]
        payload = []
        it = iter(packet_in[5: -4])
        for x in it:
            payload.append(int(bytes([x, next(it)]), 16))

        payload.reverse()

        payload_bytes = bytes(payload)

        crc_data = bytes([device_id, packet_id]) + payload_bytes

        poly = 0xA001
        crc: int = 0

        for v in crc_data:
            for i in range(8):
                if (v & 0x01) ^ (crc & 0x01):
                    crc >>= 1
                    crc ^= poly
                else:
                    crc >>= 1
                v >>= 1

        # swap the order of the bytes
        crc_int = ((crc & 0xFF00) >> 8) + ((crc & 0x00FF) << 8)

        if crc_int != rx_crc:
            logger.warning(f"CRC ERROR in {packet_in}")

        else:
            # device_id
            # packet_id
            # payload = payload_bytes
            return device_id, packet_id, payload_bytes
            pass


def encode_packet(device_id: int, packet_id: int, data: Union[bytes, bytearray], protocol_type=BPL_PROTOCOL):
    if protocol_type == BPL_PROTOCOL:
        tx_packet = bytes(data)
        tx_packet += bytes([packet_id, device_id, len(tx_packet)+4])
        tx_packet += bytes([CRC8_FUNC(tx_packet)])
        packet: bytes = cobs.encode(tx_packet) + b'\x00'
        return packet

    elif protocol_type == BPSS_PROTOCOL:
        packet = bytearray(b"#")

        device_id_bytes = b'%02X' % device_id
        packet_id_bytes = b'%02X' % packet_id

        payload_bytes = b''
        for b in data:
            payload_bytes = b'%02X' % b + payload_bytes

        crc_data = bytes([device_id, packet_id]) + data

        poly = 0xA001
        crc: int = 0

        for v in crc_data:
            for i in range(8):
                if (v & 0x01) ^ (crc & 0x01):
                    crc >>= 1
                    crc ^= poly
                else:
                    crc >>= 1
                v >>= 1

        # swap the order of the bytes
        crc_int = ((crc & 0xFF00) >> 8) + ((crc & 0x00FF) << 8)
        # cast it as a hex bytes
        crc_bytes = b'%04X' % crc_int
        packet += device_id_bytes + packet_id_bytes + payload_bytes + crc_bytes + b'\r\n'

        return packet


# if __name__ == '__main__':
#
#     print(encode_packet(1, 1, b'1', protocol_type=BPSS_PROTOCOL))
#
#     print(parse_packet(b'$01033F19999A3F000000E447', protocol_type=BPSS_PROTOCOL))
#
#     a = b'\x00\x00\x00?\x9a\x99\x19?'
#
#     for _idx in range(0, int(len(a)//4)):
#         idx = _idx * 4
#         b = a[idx: idx+4]
#         print(struct.unpack('<f', b))
#         # print([hex(x) for x in b])
#
#     a = bytes(list(a)[::-1])[0]
#     # print(struct.unpack('!f', a))