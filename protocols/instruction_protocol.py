import asyncio
import pickle
import logging
from multiprocessing.reduction import ForkingPickler
from typing import Dict, Set, Callable, Any, Tuple

from RC_Core import rc_logging
from RC_Core.connections import ConnectionBase
from RC_Core.protocols.bpl_protocol import BPLProtocol

from colorlog import ColoredFormatter

logger = rc_logging.getLogger(__name__)



class InstructionProtocol:
    """
    Protocol to send communication instruction objects using the BPL Structure.

    Currently no support for multiple frames of packets. Entire message must fit in one packet.
    Currently no packet size limit but would
    be nice to limit to 64 bytes total.
    """

    _bpl_protocol: BPLProtocol = None

    instruction_packet_id: int = None

    _instruction_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, list], None]]] = None

    def __init__(self, instruction_packet_id: int, connection: ConnectionBase):
        self.instruction_packet_id = instruction_packet_id
        self._bpl_protocol = BPLProtocol(connection)
        self._instruction_callbacks = {}
        self._permanent_instruction_callbacks = {}

        self._bpl_protocol.add_packet_callback(0xFF, self.instruction_packet_id, self._read_packet_callback)

    @property
    def connection(self):
        return self._bpl_protocol.connection

    @connection.setter
    def connection(self, connection: ConnectionBase):

        self._bpl_protocol.remove_packet_callback(0xFF, self.instruction_packet_id, self._read_packet_callback)
        self._bpl_protocol.connection = connection
        self._bpl_protocol.add_packet_callback(0xFF, self.instruction_packet_id, self._read_packet_callback)

    def send_instruction(self, device_id: int, instruction, parameters: Any):
        # 1. pickle parameters into a bytes
        # 2. encode packets using bpl protocol (device_id, CONNECTION_INSTRUCTION_PACKET_ID, data: Instruction + parameters
        # 3. Send data.
        try:
            param_data = ForkingPickler.dumps(parameters)
        except pickle.PicklingError as e:
            logger.error(f"send_instruction(): Unable to pickle parameters: {parameters} {e}")
            raise e

        try:
            data = bytes(bytearray([instruction]) + param_data)
        except ValueError as e:
            logger.error(f"send_instruction(): Unable to generate data bytes: {e}")
            raise e

        self._bpl_protocol.send_packet_bytes(device_id, self.instruction_packet_id, data)

        pass

    def add_instruction_callback(self, device_id: int, instruction_id: int, callback: Callable[[int, int, Any], None]):
        """ Add a callback upon receiving device_id, instruction_id to trigger callback.
                Callback call with arguments, device_id: int, packet_id: int and data: bytes
        """
        if (device_id, instruction_id) not in self._instruction_callbacks:
            self._instruction_callbacks[(device_id, instruction_id)] = {callback}
        else:
            self._instruction_callbacks[(device_id, instruction_id)].add(callback)

    def remove_instruction_callback(self,
                                    device_id: int,
                                    instruction_id: int,
                                    callback: Callable[[int, int, Any], None]):
        """Remove callback from callbacks dictionary.
               If removing makes entry empty, remove entry too.

               """
        if (device_id, instruction_id) in self._instruction_callbacks:
            if callback in self._instruction_callbacks[(device_id, instruction_id)]:
                self._instruction_callbacks[(device_id, instruction_id)].remove(callback)
                if len(self._instruction_callbacks[(device_id, instruction_id)]) == 0:
                    del self._instruction_callbacks[(device_id, instruction_id)]
            else:
                logger.warning(f"remove_instruction_callback({(device_id, instruction_id)}), "
                               f"f{callback} not found in callbacks.")
        else:
            logger.warning(f"remove_instruction_callback(), f{(device_id, instruction_id)} not found in callbacks.")

    def _read_packet_callback(self, device_id: int, packet_id: int, data: bytes):
        # Convert packet to instruction and parameters.
        """Trigger callbacks when a result is read.
        """

        if packet_id != self.instruction_packet_id:
            logger.error(f"Unexpected Instruction ID: {packet_id}")
            return

        # Break down device_id and data into device_id, instruction_id and parameters

        instruction_id: int = data[0]
        parameter_data = data[1:]

        if (device_id, instruction_id) not in self._instruction_callbacks:
            # No callbacks for instruction

            logger.warning(f"read_packet_callback(): No callbacks for {(device_id, instruction_id)}")
            return

        else:
            parameters = ForkingPickler.loads(parameter_data)
            callbacks = self._instruction_callbacks[(device_id, instruction_id)]
            for callback in callbacks:
                callback(device_id, instruction_id, parameters)


class InstructionServerProtocol(InstructionProtocol):
    """Instruction Server will be the one receiving instructions and responding to them

    Returned values from callbacks will respond immediately to the connection.
    """
    _missed_instruction_callbacks: Set[Callable[[int, int, Any], Any]] = None

    def __init__(self, instruction_packet_id: int, connection: ConnectionBase):
        super().__init__(instruction_packet_id, connection)
        self._missed_instruction_callbacks = set()

    def add_missed_instruction_callback(self, callback: Callable[[int, int, Any], Any]):
        self._missed_instruction_callbacks.add(callback)

    def remove_missed_instruction_callback(self, callback: Callable[[int, int, Any], Any]):
        if callback in self._missed_instruction_callbacks:
            self._missed_instruction_callbacks.remove(callback)
        else:
            logger.warning(f"remove_missed_instruction_callback() {callback} is not in callback list.")

    def _read_packet_callback(self, device_id: int, packet_id: int, data: bytes):
        # Convert packet to instruction and parameters.
        """Trigger callbacks when a result is read.
        """

        if packet_id != self.instruction_packet_id:
            logger.error(f"Unexpected Instruction ID: {packet_id}")
            return

        # Break down device_id and data into device_id, instruction_id and parameters

        instruction_id: int = data[0]
        parameter_data = data[1:]

        parameters = ForkingPickler.loads(parameter_data)

        callbacks = set()
        if (device_id, instruction_id) in self._instruction_callbacks:
            callbacks = self._instruction_callbacks[(device_id, instruction_id)]
        elif (device_id, 0xFF) in self._instruction_callbacks:
            callbacks = self._instruction_callbacks[(device_id, 0xFF)]

        if callbacks:
            for callback in callbacks:
                try:
                    response = callback(device_id, instruction_id, parameters)
                    if response is not None:
                        self.send_instruction(device_id, instruction_id, response)
                except BaseException as e:
                    logger.warning(f"Callback error {(device_id, instruction_id, parameters)}, {e}")
                return
        else:
            # For any instructions that did not have any callbacks (Call missed instruction callbacks)
            for callback in self._missed_instruction_callbacks:
                try:
                    response = callback(device_id, instruction_id, parameters)
                    if response is not None:
                        self.send_instruction(device_id, instruction_id, response)
                except BaseException as e:
                    logger.warning(f"Callback error {(device_id, instruction_id, parameters)}, {e}")
                return


class _InstructionWaiter:
    """
    This Class is used to help wait for callbacks with a response

    Add instruction_callback to callbacks, then sechedule instruction waiter with a timeout using
    asyncio.wait_for(timeout). This will asyncronously wait for a response, unless timeout is called.
    """
    def __init__(self):
        self.response = None
        self.received_response_event: asyncio.Event = asyncio.Event()

    def instruction_callback(self, _, __, r):
        self.received_response_event.set()
        self.response = r

    async def instruction_waiter(self):
        await self.received_response_event.wait()


class InstructionClientProtocol(InstructionProtocol):
    """
    Received callbacks will immediately be deleted.
    """

    _permanent_instruction_callbacks: Dict[Tuple[int, int], Set[Callable[[int, int, list], None]]] = None

    async def async_send_instruction(self,
                                     device_id: int,
                                     instruction,
                                     parameters: Any,
                                     timeout: float = 7.5):
        """ Async send instruction and wait for response. Unless it times out
        If It timeouts, this will raise a timeout Error
        """

        iw = _InstructionWaiter()

        self.add_instruction_callback(device_id, instruction, iw.instruction_callback)
        self.send_instruction(device_id, instruction, parameters)

        try:
            await asyncio.wait_for(iw.instruction_waiter(), timeout=timeout)

            if iw.response:
                return iw.response
        except asyncio.TimeoutError:
            raise TimeoutError(f"Did not receive callback from instruction within {timeout} seconds")
        pass

    def add_permanent_instruction_callback(self,
                                           device_id: int,
                                           instruction_id: int,
                                           callback: Callable[[int, int, Any], None]):
        if (device_id, instruction_id) not in self._instruction_callbacks:
            self._permanent_instruction_callbacks[(device_id, instruction_id)] = {callback}
        else:
            self._permanent_instruction_callbacks[(device_id, instruction_id)].add(callback)

    def remove_permanent_instruction_callback(self,
                                              device_id: int,
                                              instruction_id: int,
                                              callback: Callable[[int, int, Any], None]):
        if (device_id, instruction_id) in self._permanent_instruction_callbacks:
            if callback in self._permanent_instruction_callbacks[(device_id, instruction_id)]:
                self._permanent_instruction_callbacks[(device_id, instruction_id)].remove(callback)
                if len(self._permanent_instruction_callbacks[(device_id, instruction_id)]) == 0:
                    del self._permanent_instruction_callbacks[(device_id, instruction_id)]
            else:
                logger.warning(f"remove_instruction_callback({(device_id, instruction_id)}), "
                               f"f{callback} not found in callbacks.")
        else:
            logger.warning(f"remove_instruction_callback(), f{(device_id, instruction_id)} not found in callbacks.")

    def _read_packet_callback(self, device_id: int, packet_id: int, data: bytes):
        # Convert packet to instruction and parameters.
        """Trigger callbacks when a result is read.
        """

        if packet_id != self.instruction_packet_id:
            logger.error(f"Unexpected Instruction ID: {packet_id}")
            return

        # Break down device_id and data into device_id, instruction_id and parameters

        instruction_id: int = data[0]
        parameter_data = data[1:]

        if (device_id, instruction_id) not in self._instruction_callbacks and \
                (device_id, instruction_id) not in self._permanent_instruction_callbacks:
            # No callbacks for instruction


            # logger.warning(f"client(): read_packet_callback(): No callbacks for {(device_id, instruction_id)}")
            return

        else:
            parameters = ForkingPickler.loads(parameter_data)
            if (device_id, instruction_id) in self._instruction_callbacks:
                callbacks = self._instruction_callbacks[(device_id, instruction_id)]
                for callback in callbacks:
                    callback(device_id, instruction_id, parameters)

                # Remove callback after execution
                del self._instruction_callbacks[(device_id, instruction_id)]

            # Permanent callbacks are used for binding to a variable that can get changed
            if (device_id, instruction_id) in self._permanent_instruction_callbacks:
                callbacks = self._permanent_instruction_callbacks[(device_id, instruction_id)]
                for callback in callbacks:
                    callback(device_id, instruction_id, parameters)



