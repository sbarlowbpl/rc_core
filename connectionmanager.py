import asyncio
import logging
import multiprocessing
import sys
import datetime
from copy import deepcopy
from typing import List

from colorlog import ColoredFormatter

from RC_Core.callbackobject import TC_DEVICE_ID, TC_PACKET_ID, TC_DATA, CallbackObject
from RC_Core.RS1_hardware import PacketID, Mode
import RC_Core.RS1_hardware
import time
import RC_Core.loadsave
from RC_Core.connection_controller import create_connection_server, ConnectionClient

from RC_Core.devicemanager.hid_masterarm.hidmasterarm import HIDMasterArm
from RC_Core.devicemanager.product import LegacyMasterArm
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2
from RC_Core.packetid import Packets

from RC_Core.devicemanager.product import RSProduct, ProductAlpha5, MasterArm, ProductType, \
    ProductBravo7

from RC_Core.commconnection import CommConnection, CommType
from RC_Core.commconnection_methods import parsepacket, send_packet
from RC_Core.datalogger import DataLogging
from RC_Core.devicemanager.product import ProductAlpha3

import rc_logging
logger = rc_logging.getLogger(__name__)

REQUEST_TIMEOUT: float = 0.5
USER_CONNECTIONS_FILENAME: str = "user_connections_multi_settings.json"
FIXED_LOOP_PERIOD: float = 1.0 / 60.0
REQUESTS_INTERVAL: int = 1
HEARTBEAT_TIMEOUT: float = 2.0


class ConnectionManager:
    instance = None
    error_count = 0
    run_connection_loop = False
    emergency_stop = False
    pause_autoconfig = False
    kill_self = False

    connection_server: multiprocessing.Process = None
    connection_client: ConnectionClient = None

    def __init__(self):
        if ConnectionManager.instance is None:
            ConnectionManager.instance = self
        if CommConnection.connections is None or CommConnection.connections == []:
            connection = CommConnection()
            connection.set_type(CommType.NONE)
            connection.name = 'NONE'

    @staticmethod
    def get_instance():
        if ConnectionManager.instance is None:
            ConnectionManager.instance = ConnectionManager()
        return ConnectionManager.instance

    @staticmethod
    def load_user_default_connections():
        loaded_connections_list = RC_Core.loadsave.get_settings_object(USER_CONNECTIONS_FILENAME)
        if loaded_connections_list is None:
            # Load default connections if no input (base NONE instance + 1 extra connection connection)
            connection = CommConnection()
            connection.name = 'Connection ' + str(connection.uid)
            connection.set_type(CommType.NONE)
            connection.half_duplex = True
            logger.info('load_user_default_connections(): Loaded default settings...')
        else:
            for connection_settings in loaded_connections_list:
                try:
                    connection = CommConnection()
                    connection.name = connection_settings['name']
                    connection.half_duplex = connection_settings['half_duplex']
                    if connection_settings['method'] == 'UDP':
                        connection.set_type(CommType.UDP)
                        if connection_settings['ip_port'] == '':
                            socket_port = None
                        else:
                            socket_port = int(connection_settings['ip_port'])
                        connection.config_connection(socket_port, connection_settings['ip_address'])
                    elif connection_settings['method'] == 'TCP':
                        connection.set_type(CommType.TCP)
                        if connection_settings['ip_port'] == '':
                            socket_port = None
                        else:
                            socket_port = int(connection_settings['ip_port'])
                        connection.config_connection(socket_port, connection_settings['ip_address'])
                    elif connection_settings['method'] == 'NONE':
                        connection.set_type(CommType.NONE)
                    else:
                        connection.set_type(CommType.SERIAL)
                        connection.config_connection(connection_settings['method'], connection_settings['ip_address'])
                    logger.info('load_user_default_connections():Loaded user settings for connection: {}'.format(
                        connection.name,
                        connection.type.name,
                        connection.serial_port,
                        connection.socket_ip,
                        str(connection.socket_port)))
                except Exception as e:
                    logger.warning(f'{e}')
                    pass
            print(__name__, 'load_user_default_connections():', 'Loaded user settings... # connections:',
                  len(CommConnection.connections))
        return

    def external_load(self):
        for connection in CommConnection.connections:
            connection.connect()
        self.run_connection_loop = True
        asyncio.create_task(self.communication_loop())


    async def load_connection_server_and_client(self):
        server_pipe, client_pipe = multiprocessing.Pipe()
        self.connection_server = multiprocessing.Process(target=create_connection_server, args=(server_pipe,))
        self.connection_server.start()
        self.connection_client = ConnectionClient(client_pipe)


    ''' Main loop for communication handling '''

    async def communication_loop(self):
        last_time = time.time()
        for connection in CommConnection.connections:
            connection.request_interval_ctr = REQUESTS_INTERVAL
        while not self.kill_self:
            while self.run_connection_loop and not self.emergency_stop:
                await asyncio.sleep(0.02)  # Allow kivy Clock cycles to activate

                # Handle all callbacks from received packets.
                # Action all packets
                for connection in CommConnection.connections:
                    is_passthrough = False
                    if connection.type == CommType.NONE:
                        continue
                    for product in ProductType.get_instances_from_type([ProductType.CONTROL_PASSTHROUGH]):
                        if product.connection == connection and connection != CommConnection.connections[0]:
                            is_passthrough = True
                    for legacy_master_arm in LegacyMasterArm.instances:
                        if legacy_master_arm.connection == connection and connection != CommConnection.connections[0]:
                            is_passthrough = True
                    connection.is_passthrough = is_passthrough

                    # if connection.connected and not connection.is_passthrough:
                    packets_received = self.get_packets(connection)
                    if packets_received and len(packets_received):
                        connection.packets_to_handle += packets_received
                        connection.requests_queue_remaining -= len(packets_received)
                    self.packet_handler(connection)

                for product in RSProduct.instances:
                    if product.connection is None or not product.connection.connected:
                        # Reset device info for some packets
                        for device_id in product.device_ids:
                            try:
                                product.devices[device_id].CURRENT_LIMIT.reset_rx_data()
                                product.devices[device_id].CURRENT_LIMIT_FACTORY.reset_rx_data()
                            except KeyError:
                                # Can happen sometimes on product change
                                pass

                # Main frequency activated loop per connection
                # update dt (delta time) for time dependent operations
                now = time.time()
                dt = now - last_time
                last_time = now

                for hidmasterarm in HIDMasterArm.instances:
                    if hidmasterarm.target_product in ProductType.get_instances_from_type(
                            [ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]) and \
                            (hidmasterarm.target_product.connection is None or
                             not hidmasterarm.target_product.connection.connected):
                        if hidmasterarm.target_product.master_arm == hidmasterarm:
                            self.handle_hid_masterarm(hidmasterarm.target_product)

                for connection in CommConnection.connections:
                    # if not connection.connected:
                    #     continue
                    connection.wait_for_request_timer -= dt

                    # Warn us if the loop is taking too long

                    loops = 1
                    if dt > FIXED_LOOP_PERIOD:
                        pass
                        # print(datetime.datetime.now().time(), '\t', __name__, "\t WARNING: run of main loop took",
                        #       "{:.4f}".format(dt), "secs\texceeded FIXED_LOOP_PERIOD:", FIXED_LOOP_PERIOD, "secs",
                        #       file=sys.stderr)
                        if not connection.half_duplex:
                            loops = int(dt / FIXED_LOOP_PERIOD) + 1
                            # logger.info(f"RUNNING amount {loops}")

                    # Heartbeat Requests timeout
                    if connection.half_duplex:
                        time_check = time.time()
                        if time_check >= (connection.receive_wait_start_time + connection.receive_timeout) and \
                                connection.requests_queue_remaining > 0:
                            print("### Missed request from products on connection", connection.name,
                                  "with remaining packets to receive:", connection.requests_queue_remaining,
                                  'timeout time:',
                                  '{:3.3f} seconds'.format(time_check - connection.receive_wait_start_time))
                            connection.requests_queue_remaining = 0
                            connection.requests_queue_remaining_previous_state = connection.requests_queue_remaining
                        elif time_check < (connection.receive_wait_start_time + connection.receive_timeout) and \
                                connection.requests_queue_remaining > 0:
                            connection.requests_queue_remaining_previous_state = connection.requests_queue_remaining
                            continue
                        elif time_check < (connection.receive_wait_start_time + connection.receive_timeout) and \
                                connection.requests_queue_remaining <= 0 and \
                                connection.requests_queue_remaining_previous_state > 0:
                            print("Time to receive all requests on connection", connection.name,
                                  "\t\ttime:",
                                  '{:3.3f} seconds'.format(time_check - connection.receive_wait_start_time))
                            connection.requests_queue_remaining = 0
                            connection.requests_queue_remaining_previous_state = connection.requests_queue_remaining

                    # If it has been FIXED_LOOP_PERIOD seconds since last running this connection...
                    if connection.wait_for_request_timer <= 0:
                        connection.wait_for_request_timer = FIXED_LOOP_PERIOD
                        connection.request_interval_ctr -= 1

                        if not connection.connected:
                            for product in ProductType.get_instances_from_type(
                                    [ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                                # if product.connection == connection and \
                                #         hasattr(product, "master_arm") and product.master_arm \
                                #         and product.master_arm in \
                                #         ProductType.get_instances_from_type([ProductType.CONTROL_PASSTHROUGH]):
                                #     product.master_arm.do_passthrough(None)
                                if product.connection == connection:
                                    product.set_controls_off()

                            continue
                        if time.time() > (connection.time_of_last_bytes + 3.0):  # Assuming 5.0s of stm32 boot time
                            for product in ProductType.get_instances_from_type(
                                    [ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                                if product.connection == connection:
                                    product.set_controls_off()

                                    for device_id in product.device_ids:
                                        try:
                                            product.devices[device_id].CURRENT_LIMIT.reset_rx_data()
                                            product.devices[device_id].CURRENT_LIMIT_FACTORY.reset_rx_data()
                                        except KeyError:
                                            # Can happen sometimes on product change
                                            pass

                        # FOR ALL PRODUCTS AND CONNECTIONS:
                        # Send any queued packets and exit fixed loop if there were packets sent
                        queued_packets_sent = False
                        queued_request_packet_count = 0
                        if connection and connection.connected:
                            packets_sent_to_queue, queued_request_packet_count = self.send_queued_packets(connection)
                            if packets_sent_to_queue:
                                queued_packets_sent = True
                        if queued_packets_sent:  # exit fixed loop if there were packets sent
                            if connection.half_duplex and queued_request_packet_count:
                                #     self.wait_for_half_duplex(connection, requests_packets_to_send_count)
                                connection.requests_queue_remaining = queued_request_packet_count
                                connection.requests_queue_remaining_previous_state = connection.requests_queue_remaining
                                connection.receive_wait_start_time = time.time()
                            # continue

                        # Send control values or pass master arm
                        self.send_control_values(ProductType.get_instances_from_type(
                            [ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR, ProductType.ALTERNATIVE]), connection)

                        # self.masterarm_passthrough(ProductType.get_instances_from_type(
                        # [ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR, ProductType.ALTERNATIVE]), connection)

                        requests_packets_to_send_count = 0
                        if connection.request_interval_ctr < 1:
                            connection.request_interval_ctr = REQUESTS_INTERVAL
                            total_connection_products = []
                            for product in RSProduct.instances:
                                if product.connection is None or product.connection is not connection or not product.connection.connected:
                                    pass
                                else:
                                    total_connection_products.append(product)
                            for product in total_connection_products:
                                for i in range(loops):
                                    # Request only from 1 device per product on the current connection .
                                    # Select product via product.device_id_send_index (incremented
                                    try:
                                        device_id = product.device_ids[product.device_id_send_index]
                                        # product.devices[device_id]
                                        conn_request_list = []
                                        if product == total_connection_products[connection.product_request_number] and \
                                                product not in ProductType.get_instances_from_type(
                                            [ProductType.CONTROL_PASSTHROUGH]):
                                            # If request received in last HEARTBEAT_TIMEOUT(s), request again
                                            if time.time() < (
                                                    product.devices[device_id].time_last_received + HEARTBEAT_TIMEOUT):
                                                conn_request_list = product.heartbeat_packets[
                                                    product.device_id_send_index]
                                                product.devices[device_id].time_last_heartbeat = time.time()
                                            # If request sent in last HEARTBEAT_TIMEOUT(s), but not received in last HEARTBEAT_TIMEOUT(s), request again
                                            elif time.time() > (
                                                    product.devices[device_id].time_last_heartbeat + HEARTBEAT_TIMEOUT):
                                                conn_request_list = product.heartbeat_packets[
                                                    product.device_id_send_index]
                                                product.devices[device_id].time_last_heartbeat = time.time()
                                            # Skip device_id that has not received a recent packet
                                            elif len(product.heartbeat_packets[0]) > 0:
                                                pass
                                                # print(__name__, 'communication_loop():',
                                                #       'Skipping heartbeat of inactive device (product, device_id):',
                                                #       product.name, device_id)

                                            # Increment or reset send index
                                            product.device_id_send_index += 1
                                            if product.device_id_send_index >= len(product.heartbeat_packets):
                                                product.device_id_send_index = 0
                                    except IndexError:
                                        product.device_id_send_index = 0
                                        continue
                                    except KeyError:
                                        # Can happen when switching products and this triggers
                                        continue
                                    # Append heartbeat packets to list
                                    if conn_request_list:
                                        requests_packets_to_send_count += self.append_request_packet(connection,
                                                                                                     device_id,
                                                                                                     conn_request_list)
                                connection.product_request_number += 1
                                if connection.product_request_number >= len(total_connection_products):
                                    connection.product_request_number = 0
                                # Send requests if request count is greater than 0

                                if requests_packets_to_send_count > 0:
                                    # Get request packets that are still incoming before sending new packets
                                    packets_received = self.get_packets(connection)
                                    if packets_received and len(packets_received):
                                        connection.packets_to_handle += packets_received

                                    # If connection is half_duplex, wait here for packets
                                    if connection.half_duplex:
                                        #     self.wait_for_half_duplex(connection, requests_packets_to_send_count)
                                        connection.requests_queue_remaining = requests_packets_to_send_count
                                        connection.requests_queue_remaining_previous_state = connection.requests_queue_remaining
                                        connection.receive_wait_start_time = time.time()

                                # Callback timeouts
                                for _callback in connection.temporary_callbacks:
                                    if (
                                            _callback in connection.temporary_callbacks) and _callback.check_and_run_timeout_callback():
                                        try:
                                            connection.temporary_callbacks.remove(_callback)
                                        except:
                                            print(__name__, 'communication_loop():',
                                                  'could not remove temporary callback',
                                                  _callback, 'in connection', connection.name)

            else:
                # This runs self.run_connection_loop is set to False
                await asyncio.sleep(1.0)

    ''' Get packets and parse the data in components '''

    def get_packets(self, connection):
        # Read all incoming packets
        packets = connection.read()
        if (packets is None) or (type(packets) is int) or (len(packets) < 1):
            return None
        packets_list = []
        for packet in packets:
            # Get packet information and append to processing list
            device_id, packet_id, data = parsepacket(packet)
            packet += bytes([0])
            if device_id == 0 or packet_id == 0:
                continue
            # print(__name__, f'get_packets() packet: {device_id, packet_id, data}')
            packets_list.append((device_id, packet_id, data))
        return packets_list

    ''' Callback handler function '''

    def packet_handler(self, connection):
        if connection.packets_to_handle is None or len(connection.packets_to_handle) < 1:
            return

        packets_not_for_products = []
        # Check if device ids and connection are related to a product
        for packet in connection.packets_to_handle:
            packet_is_for_product = False
            for product in RSProduct.instances:
                if product.connection != connection:
                    continue
                # Check is device is not in this product and skip if not - handling multiple products on one line
                # try:
                device_id = packet[0]
                if device_id not in product.device_ids:
                    continue

                # PRODUCT TIME WHEN IT IS LAST CONNECTED
                product.last_connected = time.time()

                packet_id = packet[1]
                data_pkt = packet[2]
                if int(device_id) > 0:
                    # If device ID is adequate
                    packet_is_for_product = True
                    try:
                        packet_object = product.devices[device_id].get_by_id(packet_id)
                    except KeyError:
                        continue

                    # If the packet id is not recognised.
                    if packet_object is None:
                        continue
                    packet_object.receive(data_pkt)
                    product.devices[device_id].time_last_received = time.time()
                    self._callback_handler(connection, device_id, packet_id, data_pkt)

                    break

            if not packet_is_for_product:
                packets_not_for_products.append(packet)
        connection.packets_to_handle = []

        # Likely device ids not on any product using the connection
        for packet in packets_not_for_products:
            # Check is device is not in this product and skip if not - handling multiple products on one line
            try:
                device_id = packet[0]
                packet_id = packet[1]
                data_pkt = packet[2]
                if int(device_id) > 0:
                    self._callback_handler(connection, device_id, packet_id, data_pkt)
            except:
                continue
    def send_queued_packets(self, connection):
        queued_request_packet_count = 0
        if len(connection.send_queue) < 1:
            return False, 0
        while len(connection.send_queue) > 0:
            if len(connection.send_queue) > 40:
                print(__name__, 'send_queued_packets():', 'Large send queue count on connection,', connection.name, ':',
                      len(connection.send_queue))
            # Get packet from send list
            packet = connection.send_queue.pop(0)
            if isinstance(packet[TC_DATA], bytearray):
                data_to_recall = list(packet[TC_DATA])
            else:
                data_to_recall = packet[TC_DATA]
            # Send this packet
            connection.send(packet[TC_DEVICE_ID], packet[TC_PACKET_ID], packet[TC_DATA], append_only=True)
            if packet[TC_PACKET_ID] not in [PacketID.REQUEST, PacketID.BOOTLOADER]:
                if len(packet) > 3:
                    callback = packet[3]
                else:
                    callback = None
                # Add callback for the requested packet
                if callback:
                    self._add_send_data_to_temporary_callbacks(connection, packet[TC_DEVICE_ID], packet[TC_PACKET_ID],
                                                               data_to_recall, time.time(), callback)
                    self._send_request_packets(connection, packet[TC_DEVICE_ID], packet[TC_PACKET_ID])
            if packet[TC_PACKET_ID] == PacketID.REQUEST:
                queued_request_packet_count += len(data_to_recall)
        if queued_request_packet_count > 0:
            logger.debug(f'send_queued_packets(): # Requests asked for: {queued_request_packet_count}')
        return True, queued_request_packet_count


    # Master Arm pass through only
    def masterarm_passthrough(self, product_list, this_connection):
        if self.emergency_stop:
            return

        # Get product with master arm if connection of product is this_connection
        product = None
        # If this connection is on a product that has a master arm, use that
        for product_obj in product_list:
            if product_obj.connection and product_obj.connection == this_connection:
                if hasattr(product_obj, "master_arm") and product_obj.master_arm:
                    product = product_obj
                    break

        if product is None:
            # If master arm is bound to a product with no or NONE connection
            for master_arm_obj in MasterArm.instances:
                for product_obj in product_list:
                    if hasattr(product_obj, "master_arm") and product_obj.master_arm and \
                            product_obj.master_arm == master_arm_obj and \
                            product_obj.master_arm.connection == this_connection and \
                            (product_obj.connection is None or not product_obj.connection.connected):
                        product = product_obj
        # If product has master arm, do stuff
        if product and product.master_arm:
            hid_instances = HIDMasterArm.instances
            if product.master_arm in hid_instances:
                self.handle_hid_masterarm(product)

    # Handle HID Master Arm
    def handle_hid_masterarm(self, product):
        '''
        Handle HID master arm settings
        :param product:
        :return:
        '''
        if self.pause_autoconfig:
            return

        # Get default master arm mappings
        rotation = 'upright'
        alternative = 'default'
        if product.inverted:
            rotation = 'inverted'
        if isinstance(product, ProductAlpha3):
            rotation = product.orientation_mode
        elif isinstance(product, ProductBravo7):
            rz_axis_e = product.devices[product.device_ids[4]].LINK_TRANSFORM.rx_data['rz']
            if rz_axis_e and abs(rz_axis_e) > 1.5:
                alternative = 's-bend'
        default_external_device_ids_mapping, default_position_scale_mapping = \
            product.master_arm_mapppings.get_masterarm_mapping(product,
                                                               product.master_arm.masterarm_model_number,
                                                               rotation=rotation,
                                                               alternative=alternative)

        if product.master_arm.use_default_settings:
            position_scale_mapping = default_position_scale_mapping
            external_device_ids_mapping = default_external_device_ids_mapping
            # Set default control packet
            if product in ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR]):
                product.master_arm.masterarm_control_packet = PacketID.INDEXED_POSITION
            elif product in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR]):
                product.master_arm.masterarm_control_packet = PacketID.POSITION_VELOCITY_DEMAND

            # Adjust position scale
            adjusted_position_scale_mapping: List[float] = deepcopy(position_scale_mapping)
            if adjusted_position_scale_mapping and not product.master_arm.masterarm_axis_b_rotate:
                if product in ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                    adjusted_position_scale_mapping[2] = -1.0 * adjusted_position_scale_mapping[2]

            # Make sure velocity scale has same sign as position scale
            velocity_scale_mapping_temp = deepcopy(product.master_arm.target_velocity_scale)
            velocity_scale_mapping: List[float] = [0.0] * len(velocity_scale_mapping_temp)
            if adjusted_position_scale_mapping:
                for i, val in enumerate(velocity_scale_mapping_temp):
                    velocity_scale_mapping[i] = abs(val) if adjusted_position_scale_mapping[i] >= 0 else -abs(val)

            # Set end effector velocity scale - 6.0 for alpha, 25.0 for bravo, or 1.0 for rotate
            if velocity_scale_mapping and velocity_scale_mapping[1] != 0:
                if product in ProductType.get_instances_from_type([ProductType.RS1_GRABBER]):
                    velocity_scale_mapping[1] = velocity_scale_mapping[1] / abs(velocity_scale_mapping[1]) * 6.0
                elif product in ProductType.get_instances_from_type([ProductType.RS2_GRABBER]):
                    velocity_scale_mapping[1] = velocity_scale_mapping[1] / abs(velocity_scale_mapping[1]) * 25.0
                elif product in ProductType.get_instances_from_type([ProductType.RS1_PROBE, ProductType.RS2_PROBE]):
                    velocity_scale_mapping[1] = velocity_scale_mapping[1] / abs(velocity_scale_mapping[1]) * 1.0
            # Set rotate (1.0) and joystick (5.0) relative velocity scale mappings
            if velocity_scale_mapping:
                if product.master_arm.masterarm_axis_b_rotate:
                    velocity_scale_mapping[2] = 1.0 if velocity_scale_mapping[2] >= 0 else -1.0
                else:
                    velocity_scale_mapping[2] = 5.0 if velocity_scale_mapping[2] >= 0 else -5.0

            # Set parameters
            product.master_arm.target_ext_device_ids = external_device_ids_mapping
            product.master_arm.target_position_scale = adjusted_position_scale_mapping
            product.master_arm.target_velocity_scale = velocity_scale_mapping

        else:
            # Use default if master arm mapping settings not set
            if product.master_arm.target_ext_device_ids == []:
                product.master_arm.target_ext_device_ids = default_external_device_ids_mapping
            if product.master_arm.target_position_scale == []:
                product.master_arm.target_position_scale = default_position_scale_mapping

            # Custom settings
            # Make sure velocity scale has same sign as position scale
            velocity_scale_mapping_temp = deepcopy(product.master_arm.target_velocity_scale)
            velocity_scale_mapping: List[float] = [0.0] * len(velocity_scale_mapping_temp)
            if product.master_arm.target_position_scale:
                for i, val in enumerate(velocity_scale_mapping_temp):
                    velocity_scale_mapping[i] = abs(val) if product.master_arm.target_position_scale[i] >= 0 else -abs(
                        val)

            # Set end effector velocity scale - 6.0 for alpha, 25.0 for bravo, or 1.0 for rotate
            if velocity_scale_mapping and velocity_scale_mapping[1] != 0:
                if product in ProductType.get_instances_from_type([ProductType.RS1_GRABBER]):
                    velocity_scale_mapping[1] = velocity_scale_mapping[1] / abs(velocity_scale_mapping[1]) * 6.0
                elif product in ProductType.get_instances_from_type([ProductType.RS2_GRABBER]):
                    velocity_scale_mapping[1] = velocity_scale_mapping[1] / abs(velocity_scale_mapping[1]) * 25.0
                elif product in ProductType.get_instances_from_type([ProductType.RS1_PROBE, ProductType.RS2_PROBE]):
                    velocity_scale_mapping[1] = velocity_scale_mapping[1] / abs(velocity_scale_mapping[1]) * 1.0
            # Set rotate (1.0) and joystick (5.0) relative velocity scale mappings
            if velocity_scale_mapping:
                if product.master_arm.masterarm_axis_b_rotate:
                    velocity_scale_mapping[2] = 1.0 if velocity_scale_mapping[2] >= 0 else -1.0
                else:
                    velocity_scale_mapping[2] = 5.0 if velocity_scale_mapping[2] >= 0 else -5.0

            # Set parameters
            product.master_arm.target_velocity_scale = velocity_scale_mapping

        # Run HID passthrough - which is just creating packets from info to send
        if product and product.connection and product.connection.connected:
            product.master_arm.do_passthrough(product.connection)

    # Send control_values only
    def send_control_values(self, product_list, connection):
        if self.emergency_stop:
            return
        for product in product_list:
            return


            # if product.connection is not None and product.connection.connected and product.connection == connection:
            #     if self.emergency_stop:
            #         continue
            #     for km_mode in [PacketID.KM_END_VEL, PacketID.KM_END_POS, PacketID.KM_END_VEL_LOCAL,
            #                     PacketID.KM_END_VEL_CAMERA]:
            #         do_km = True
            #         for mode in product.control_modes[1:]:
            #             if mode != 0 and mode != km_mode:
            #                 do_km = False
            #         if km_mode in product.control_modes and do_km:
            #             device_id = product.get_device_id_for_kinematics()
            #             packet_id = km_mode
            #             tx_data = product.control_values[packet_id]
            #             if tx_data is not None:
            #                 product.connection.send_control(device_id, packet_id, tx_data, append_only=True)
            #     for i in range(len(product.control_modes)):
            #         control_mode = product.control_modes[i]
            #         if control_mode in [PacketID.POSITION, PacketID.INDEXED_POSITION, PacketID.CURRENT,
            #                             PacketID.VELOCITY, PacketID.OPENLOOP]:
            #             device_id = product.device_ids[i]
            #             packet_id = control_mode
            #             tx_data = product.control_values[control_mode][i]
            #             if tx_data is not None:
            #                 product.connection.send_control(device_id, packet_id, float(tx_data), append_only=True)
            #         elif control_mode in [PacketID.POSITION_VELOCITY_DEMAND]:
            #             device_id = product.device_ids[i]
            #             packet_id = control_mode
            #             tx_data = product.control_values[control_mode][i]
            #             if tx_data not in [None, [None, None]]:
            #                 float_tx_data = [float(tx_data[0]), float(tx_data[1])]
            #                 product.connection.send_control(device_id, packet_id, float_tx_data, append_only=True)

    def append_request_packet(self, connection, device_id, request_list):
        this_request = []
        for packet_index, packet_id in enumerate(request_list):
            this_request.append(packet_id)
        if this_request != [0] * len(this_request):  # if all values in this_request list is not 0
            packets_received = self.get_packets(connection)
            if packets_received is not None:
                connection.packets_to_handle += packets_received
            # self.send_controls(other_products_list)
            if connection.send(device_id, PacketID.REQUEST, bytearray(this_request), append_only=True) > 0:
                return len(this_request)
        return 0

    '''
    PRIVATE FUNCTIONS
    '''

    def _callback_handler(self, connection, device_id, packet_id, pdata):
        callbacks_list = []
        for _callback in (connection.temporary_callbacks + connection.permanent_callbacks):
            callbacks_list.append(_callback)
        for product in RSProduct.instances:
            if product.connection == connection:
                for _callback in product.permanent_callbacks:
                    callbacks_list.append(_callback)

        # Required for bpss stuff
        # for _callback in callbacks_list:
        #     if (_callback.device_id == device_id or _callback.device_id == 0xFF) and \
        #             _callback.packet_id == packet_id:
        #         _callback.check_and_run_callback(device_id, packet_id, pdata)
        #         if _callback in connection.temporary_callbacks:
        #             _callback.temporary_callback_functionality()
        #             if _callback.timeout == 0 and _callback.timeout_callback == None:
        #                 connection.temporary_callbacks.remove(_callback)
        #         if _callback.sent_data:
        #             if PacketID.KM_BOX_OBSTACLE_00 <= packet_id <= PacketID.KM_BOX_OBSTACLE_05:
        #                 success = True
        #                 for i in range(3):
        #                     expected_data_1 = _callback.sent_data[i]
        #                     expected_data_2 = _callback.sent_data[i + 3]
        #                     if (expected_data_1 == pdata[i] and expected_data_2 == pdata[i + 3]) \
        #                             or (expected_data_1 == pdata[i + 3] and expected_data_2 == pdata[i]):
        #                         pass
        #                     else:
        #                         success = False
                    #     if success:
                    #         self.print('Recvd SUCCESS device_id:' + str(device_id) + ' ' + str(
                    #             RC_Core.RS1_hardware.get_name_of_packet_id(packet_id))
                    #                    + ' expected ' + str(_callback.sent_data) + ' got ' + str(list(pdata)),
                    #                    color='green')
                    #     else:
                    #         self.print('Recvd FAIL device_id:' + str(device_id) + ' ' + str(
                    #             RC_Core.RS1_hardware.get_name_of_packet_id(packet_id))
                    #                    + ' expected ' + str(_callback.sent_data) + ' got ' + str(list(pdata)),
                    #                    color='red')
                    #     print(__name__, '_callback_handler():', 'success:', success, success, success,
                    #           'data verified got:', list(pdata),
                    #           'expected:', _callback.sent_data, 'device_id:', device_id,
                    #           'packet_id:', RC_Core.RS1_hardware.get_name_of_packet_id(packet_id),
                    #           'temporary_callbacks', connection.temporary_callbacks)
                    # else:
                    #     if list(pdata) == _callback.sent_data:
                    #         self.print('Recvd SUCCESS device_id:' + str(device_id) + ' '
                    #                    + str(RC_Core.RS1_hardware.get_name_of_packet_id(packet_id))
                    #                    + ' expected ' + str(_callback.sent_data) + ' got ' + str(list(pdata)),
                    #                    color='green')
                    #     else:
                    #         print(__name__, '_callback_handler():', 'data verified FAIL FAIL FAIL FAIL FAIL got:',
                    #               list(pdata),
                    #               'expected:', _callback.sent_data, 'device_id:', device_id, 'packet_id:', packet_id)
                    #         self.print('Recvd FAIL device_id:' + str(device_id) + ' '
                    #                    + str(RC_Core.RS1_hardware.get_name_of_packet_id(packet_id))
                    #                    + ' expected ' + str(_callback.sent_data) + ' got ' + str(list(pdata)),
                    #                    color='red')

        if packet_id == PacketID.HARDWARE_STATUS:
            for i in range(8):
                if pdata[0] & (1 << i):
                    self.error_count += 1
        if packet_id == PacketID.COMS_PROTOCOL:
            print(__name__, '_callback_handler():', "COMS_PROTOCOL PACKET received. Device ID:",
                  device_id, 'data:', pdata)

    def _add_send_data_to_temporary_callbacks(self, connection, device_id, packet_id, data, time_of_send,
                                              callback=None):
        if packet_id != PacketID.REQUEST and data:
            for _callback in connection.temporary_callbacks:
                if (
                        _callback.device_id == device_id and _callback.packet_id == packet_id and _callback.callback == callback):
                    connection.temporary_callbacks.remove(_callback)
            connection.temporary_callbacks.append(CallbackObject(connection, device_id, packet_id, callback))

    def _send_request_packets(self, connection, DeviceID, d0=0, d1=0, d2=0, d3=0, d4=0, d5=0, d6=0, d7=0, d8=0, d9=0,
                              callback=None):
        self._add_requests_to_temporary_callbacks(connection, DeviceID, [d0, d1, d2, d3, d4, d5, d6, d7, d8, d9],
                                                  callback)
        txData = (bytearray([d0, d1, d2, d3, d4, d5, d6, d7, d8, d9]))
        txPacketId = PacketID.REQUEST
        send_packet(connection, DeviceID, txPacketId, txData, callback)

    def _add_requests_to_temporary_callbacks(self, connection, device_id, packet_ids, callback=None):
        for packet_id in packet_ids:
            if packet_id <= 0:
                continue
            previously_set = False
            for callback_object in connection.temporary_callbacks:
                if callback_object.device_id == device_id and callback_object.packet_id == packet_id and \
                        callback_object.callback == callback:
                    previously_set = True
                    break
            if not previously_set:
                connection.temporary_callbacks.append(CallbackObject(connection, device_id, packet_id, callback))


    def remove_self(self):
        self.run_connection_loop = False
        self.kill_self = True
        ConnectionManager.instance = None
