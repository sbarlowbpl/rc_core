# containers constants and classes related specifically to RS1 hardware
import inspect

from enum import unique, IntFlag, auto, IntEnum
from typing import List

# try:
#     from RC_Core.enums.packets import PacketID
# except ModuleNotFoundError:
#     from enums.packets import PacketID



# from RC_Core.enums.modes import Mode

from RC_Core.enums import Mode, CollisionType, PacketID
# class Mode():
#     '''
#     List of all possible modes that the on-board devices may be in
#     '''
#     STANDBY = 0x00
#     DISABLE = 0x01
#     VELOCITY_CONTROL = 0x03
#     POSITION_CONTROL = 0x02
#     OPENLOOP_CONTROL = 0x05
#     FACTORY = 0x08
#     CURRENT_CONTROL = 0x04
#     CALIBRATE = 0x07
#     INIT = 0x06
#     COMPLIANT = 0x09
#     GRIP = 0x0A
#     STALL_DRIVE = 0x0B
#     CALIBRATE_TEST = 0x0C
#     AUTO_LIMIT = 0x0D
#     CALIBRATE_OUTER = 0x0E
#     VELOCITY_CONTROL_INNER = 0x0F
#     RELATIVE_POSITION_CONTROL = 0x12
#     INDEXED_POSITION_CONTROL = 0x13
#
#     POSITION_PRESET = 0x14
#     ZERO_VELOCITY = 0x15
#     FACTORY_CURRENT_CONTROL = 0x16
#     KM_END_POS = 0x17
#     KM_END_VEL = 0x18
#     KM_END_POS_LOCAL = 0x19
#     KM_END_VEL_LOCAL = 0x1A
#     VELOCITY_FACTORY = 0x1B
#     POSITION_VELOCITY_CONTROL = 0x1C
#     POSITION_HOLD = 0x1D
#     ABS_POSITION_VELOCITY_CONTROL = 0x1E
#
#     CALIBRATE_OVER_SERIAL = 0x1F
#     CALIBRATE_CURRENT_CONTROL = 0x20
#
#
#
#     CALIBRATE_INNER_OVER_SERIAL = 0x21
#     CALIBRATE_OUTER_OVER_SERIAL = 0x22
#
#     KM_END_VEL_CAMERA = 0x23
#
#     DIRECT_CONTROL = 0x25
#
#     PASSIVE = 0x25
#     EXIT_PASSIVE = 0x26


def get_name_of_mode(mode):
    '''
    Get the name of the mode from its integer number.
    :param mode:
    :return:
    '''
    packet_list = get_list_of_modes()
    for member in packet_list:
        if member[1] == mode:
            return member[0]
    return 'UNKNOWN'


class Gains():
    KP = 0
    KI = 0
    KD = 0
    KF = 0


@unique
class HardwareStatus(IntFlag):
    """ Maintain the order of these flags.
        Changing them will result in mismatch of names.
    """
    # byte 1
    ERROR_ENCODER_NOT_DETECTED = auto()
    ENCODER_POSITION_ERROR = auto()
    MOTOR_DRIVER_FAULT = auto()
    COMS_CRC_ERROR = auto()
    COMS_SERIAL_ERROR = auto()
    HARDWARE_OVER_TEMPERATURE = auto()
    HARDWARE_OVER_HUMIDITY = auto()
    FLASH_FAILED_READ = auto()
    # byte 2
    MOTOR_DRIVER_OVER_TEMP = auto()
    MOTOR_DRIVER_OVER_CURRENT_UNDER_VOLTAGE = auto()
    HARDWARE_OVER_PRESSURE = auto()
    DEVICE_ID_CONFLICT = auto()
    ENCODER_INNER_POSITION_ERROR = auto()
    MOTOR_OVER_CURRENT = auto()
    MOTOR_NOT_CONNECTED = auto()
    DEVICE_AXIS_CONFLICT = auto()
    # byte 3
    UNKNOWN_ERROR_31 = auto()
    UNKNOWN_ERROR_32 = auto()
    UNKNOWN_ERROR_33 = auto()
    UNKNOWN_ERROR_34 = auto()
    UNKNOWN_ERROR_35 = auto()
    UNKNOWN_ERROR_36 = auto()
    UNKNOWN_ERROR_37 = auto()
    UNKNOWN_ERROR_38 = auto()
    # byte 4 - USE THIS ONE FOR TX2 status
    POSITION_REPORT_NOT_RECEIVED = auto()
    CANBUS_ERROR = auto()
    INVALID_FIRMWARE = auto()
    UNKNOWN_ERROR_44 = auto()
    UNKNOWN_ERROR_45 = auto()
    UNKNOWN_ERROR_46 = auto()
    UNKNOWN_ERROR_47 = auto()
    UNKNOWN_ERROR_48 = auto()


class KinematicsConfigFrames():
    NO_CHANGE = 0
    WORLD = 1
    BASE = 2
    END = 3


class KinematicsFrames():
    NO_CHANGE = 0
    WORLD = 1
    BASE = 2
    END = 3


class KinematicsConfigOrientation():
    NO_CHANGE = 0
    UPRIGHT = 1
    INVERTED = 2


class KinematicsConfigEnable():
    NO_CHANGE = 0
    DISABLE = 1
    ENABLE = 2


class KinematicsConfigObstacleEnable():
    NO_CHANGE = 0
    DISABLE = 1
    ENABLE = 2


class DeviceType:
    '''
    List of all device types
    '''
    ROTATE = 0
    LINEAR = 1
    BUTTONS = 2
    DISABLED_ESTIMATOR = 3
    JOYSTICK_X_PROXY_ESTIMATOR = 4
    JOYSTICK_Y_PROXY_ESTIMATOR = 5
    COMMS_PASSTHROUGH = 6
    COMMS_VIA_SIMULATOR = 7

    COMMS_PASSTHROUGH_UDP = 9


class NCValue:
    NO_CHANGE = 0
    DISABLE = 1
    ENABLE = 2


class test():
    check = 0

    def testFunction(self):
        self.check = self.check + 1
        print(self.check)


def get_list_of_packets() -> List[tuple]:
    ''' Returns alphabetised list of tuples containing all packet ids from PacketID() class
    e.g.: [('BOOTLOADER', 255), ('COMPLIANCE_GAIN', 66), ('COMS_PROTOCOL', 128), ('CURRENT', 5), ...]
    '''
    attrlist = inspect.getmembers(PacketID)
    list_of_packets = []
    for member in attrlist:
        if not member[0].startswith('_'):
            list_of_packets.append(member)
    return list_of_packets


def get_list_of_packet_names() -> List[str]:
    '''
    :return: All packet names (as string)
    '''
    p_list = get_list_of_packets()
    return [p[0] for p in p_list]


def get_list_of_packet_ids() -> List[int]:
    '''
    :return: All packet ids (as integer numbers)
    '''
    p_list = get_list_of_packets()
    return [p[1] for p in p_list]


def get_list_of_modes() -> List[tuple]:
    '''
    :return: Returns list of all modes in format [('STANDBY', 0), ...]
    '''
    attrlist = inspect.getmembers(Mode)
    list_of_packets = []
    for member in attrlist:
        if not member[0].startswith('_'):
            list_of_packets.append(member)
    return list_of_packets


def get_name_of_packet_id(packet_id) -> str:
    '''
    :param packet_id:
    :return: Name from packet_id
    '''
    packet_list = get_list_of_packets()
    for member in packet_list:
        if member[1] == packet_id:
            return member[0]
    return 'UNKNOWN'


def get_packet_from_name(name) -> int:
    '''
    :param name:
    :return: Packet id from its name
    '''
    packet_list = get_list_of_packets()
    for member in packet_list:
        if member[0] == name:
            return member[1]


def get_hardware_status_string(status) -> str:
    ''' Returns string representing the name of the hardware status int provided.
    '''
    attrlist = inspect.getmembers(HardwareStatus)
    for member in attrlist:
        if not member[0].startswith('_'):
            if member[1] == status - 1:
                return member[0]
    return 'UNKNOWN_ERROR'


def get_hardware_status_from_packet_data(packet_data_bytes: bytes) -> list:
    """ Returns list of status flags set in packet_data_bytes.
        To convert result to printable names use:
            for status in result:
                print(status.name)
    """
    int_conversion = int.from_bytes(packet_data_bytes, byteorder="little")
    result = []
    for status in list(HardwareStatus):
        if status & int_conversion:
            result.append(status)
    return result


def get_unassigned_packet_ids():
    packet_list = get_list_of_packets()
    empty_packet_list = list(range(256))
    for packet in packet_list:
        packet_id = packet[1]
        if packet_id in empty_packet_list:
            empty_packet_list.remove(packet_id)
    return empty_packet_list


def check_for_duplicate_packet_ids():
    packets = get_list_of_packets()
    for p in packets:
        [print("DUPLICATE PACKET", p, q) for q in packets if p[1] == q[1] and p[0] != p[0]]


if __name__ == '__main__':
    empty_packets = ["0x%02x" % b for b in get_unassigned_packet_ids()]
    print(len(empty_packets), empty_packets)
    packet_table = ""
    for i, packet_id in enumerate(empty_packets):
        if i > 0:
            if packet_id[2] != empty_packets[i - 1][2]:
                packet_table += "\n"
            packet_table += packet_id + "\t"
    print(packet_table)

    check_for_duplicate_packet_ids()
