
class MasterArmV1:
    '''MasterArm V1 definitions
    '''
    model_numbers: list = [5101, 5102]
    bootloader_parity: str = 'NONE'

class MasterArmV2:
    '''MasterArm V2 definitions
    '''
    model_numbers: list = [4201, 5201, 7201, 5202]    # 4201-4fn, 5201-5fn, [5202,7201]-7fn
    bootloader_parity: str = 'NONE'
    fn4_model_numbers: list = [model_numbers[0]]
    fn5_model_numbers: list = [model_numbers[1]]
    fn7_model_numbers: list = [model_numbers[2], model_numbers[3]]