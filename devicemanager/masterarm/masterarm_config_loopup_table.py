
MasterArmDefaultLookupTable: dict = \
{
    'ProductAlpha5': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlpha5Probe': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, -1.0, -1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlpha5InlineC': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlpha2RotatingGrabber': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlpha2DualBend': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0x00, 0x00, 0xC1, 0xC2, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0x00, 0x00, 0xC1, 0xC2, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0xC1, 0xC2, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0xC1, 0xC2, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0xC1, 0xC2, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0x00, 0x00, 0xC1, 0xC2, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlpha4': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlphaSingleRotate': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductBravo7': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, -1.0, 1.0, 1.0],
                },
                's-bend': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, -1.0, 1.0, 1.0],
                },
                's-bend': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
                's-bend': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
                's-bend': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0],
                },
                's-bend': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0],
                },
                's-bend': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0],
                },
            },
        },
    },
    'ProductBravo5': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductBravoSingleRotate': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, -1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductBravoInlineRotate': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0xC4, 0xC5],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, -1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductAlpha3': {
        '7-function': {
            'cw_positive': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xC3],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0],
                },
            },
            'cw_negative': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xC3],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'pitch_positive': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0xC3, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'pitch_negative': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0x00, 0x00, 0xC3, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0],
                },
            },
        },
        '5-function': {
            'cw_positive': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0],
                },
            },
            'cw_negative': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0xC3, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'pitch_positive': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
            'pitch_negative': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
        '4-function': {
            'cw_positive': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0xC3, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0],
                },
            },
            'cw_negative': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0xC3, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'pitch_positive': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'pitch_negative': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0xC3, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
    'ProductBravo2RotatingGrabber': {
        '7-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0],
                },
            },
        },
        '5-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0],
                },
            },
        },
        '4-function': {
            'upright': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
            'inverted': {
                'default': {
                    'external_device_ids': [0x00, 0xC1, 0xC2, 0x00, 0x00, 0x00, 0x00, 0xCA],
                    'position_scale': [1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                },
            },
        },
    },
}













