import time
from copy import deepcopy
from typing import Union, Tuple

from RC_Core.devicemanager.masterarm.masterarm_config_loopup_table import MasterArmDefaultLookupTable
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2


class MasterArmDefaultConfigurationLookUpTableHandler():
    '''MasterArmDefaultConfigurationLookUpTableHandler
    Lookup table handler for Master Arm default configurations
    '''

    @staticmethod
    def get_masterarm_mapping(product,
                              masterarm_model_number: Union[int, None],
                              rotation: str='upright', alternative: str='default') \
            -> Union[Tuple[None, None], Tuple[list, list]]:
        '''
        Get master arm mapping (external device ids and position scale)
        :param product:
        :param masterarm_model_number:
        :param rotation:
        :param alternative:
        :return:
        '''
        # Set default rotation to cw_positive is Alpha 3 due to various rotations
        if rotation == 'upright' and product.__class__.__name__ == 'ProductAlpha3':
            rotation = 'cw_positive'

        # Get model number name
        masterarm_model_number_name = 'NONE'
        if masterarm_model_number == MasterArmV2.fn7_model_numbers or \
                masterarm_model_number in MasterArmV2.fn7_model_numbers:
            masterarm_model_number_name = '7-function'
        elif masterarm_model_number == MasterArmV2.fn5_model_numbers or \
                masterarm_model_number in MasterArmV2.fn5_model_numbers:
            masterarm_model_number_name = '5-function'
        elif masterarm_model_number == MasterArmV2.fn4_model_numbers or \
                masterarm_model_number in MasterArmV2.fn4_model_numbers:
            masterarm_model_number_name = '4-function'

        # Get mappings
        masterarm_mapping_reference = \
            MasterArmDefaultLookupTable[product.__class__.__name__] \
                [masterarm_model_number_name]\
                [rotation][alternative]

        return deepcopy(masterarm_mapping_reference['external_device_ids']), \
               deepcopy(masterarm_mapping_reference['position_scale'])


if __name__ == '__main__':
    from RC_Core.devicemanager.product import ProductAlpha5, ProductAlpha3
    from RC_Core.commconnection import CommConnection
    comm_fake = CommConnection()
    product = ProductAlpha3()
    madc = MasterArmDefaultConfigurationLookUpTableHandler()
    ts = time.time()
    data = madc.get_masterarm_mapping(product, masterarm_model_number=5201, rotation='pitch_negative')
    print(data, round(time.time()-ts, 6))