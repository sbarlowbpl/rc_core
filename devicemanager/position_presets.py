# from RC_Core.constants import *

# if PROBE_HOLDER_VERSION:
    # DEFAULT_POSITION_PRESET_SETTINGS = [
    #     {
    #         "title": "Brush",
    #         "values": ["2.5", "0", "1.570", "3.141", "0"]
    #     },
    #     {
    #         "title": "Probe",
    #         "values": ["2.5", "0", "3.054", "3.141", "0"]
    #     },
    #     {
    #         "title": "Stow",
    #         "values": ["2.5", "0", "0.017", "3.492", "0"]
    #     }
    # ]
DEFAULT_POSITION_PRESET_SETTINGS = [
    {
        "title": "Stow",
        "values": ["6.398", "4.538", "0.697", "1.570", "6.192"]
    },
    {
        "title": "Deploy",
        "values": ["6.398", "3.229", "2.966", "2.017", "3.112"]
    },
    {
        "title": "Pack",
        "values": ["6.398", "3.666", "2.932", "3.140", "6.221"]
    }
]


class PosPresets():

    def __init__(self, product, pos_preset_settings=None):
        self.product = product
        self.name = ''
        self.positions = []
        for i in range(0, len(product.heartbeat_packets)):
            self.positions.append(0)
        if pos_preset_settings is not None:
            try:
                self.set_name(pos_preset_settings['title'])
                self.set_positions(pos_preset_settings['values'])
            except:
                self.name = ''
                self.positions = [0,0,0,0,0]

    def set_name(self, name):
        if isinstance(name, str):
            self.name = name
        else:
            print(__name__, 'set_name():', 'Input name is not of "str" type, could not set value!')

    def get_positions(self):
        return self.positions

    def set_positions(self, set_positions):
        for i in range(0, len(self.positions)):
            self.positions[i] = set_positions[i]

