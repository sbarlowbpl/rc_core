from RC_Core.packetid import Packets


class RS1_Actuator(Packets):
    '''
    Class used to store data for devices - see product.py
    '''
    device_id: int = None
    time_last_received: float = 0.0
    time_last_heartbeat: float = 0.0
    pass
