from typing import List

import RC_Core.loadsave
from RC_Core.RS1_hardware import get_name_of_packet_id, get_packet_from_name
from RC_Core.devicemanager.hid_masterarm.hidmasterarm import HIDMasterArm
from RC_Core.devicemanager.product import MasterArm

from RC_Core.commconnection import CommConnection, CommType
import RC_Core.devicemanager.product
from RC_Core.devicemanager.product import ProductType
import os.path
from pathlib import Path

from RC_Core.devicemanager.product import LegacyMasterArm




DEVICE_ID_SEARCH_RANGE = []
for i in range(0, 0x30):
    DEVICE_ID_SEARCH_RANGE.append(i)
for i in range(0xA0, 0xC0):
    DEVICE_ID_SEARCH_RANGE.append(i)

USER_OLD_POS_PRESETS_FILENAME = "pos_presets.json"
USER_OLD_INVERTED_FILENAMES = ['r5m_1_inverted.json', 'r5m_inverted.json']
USER_DEVICES_FILENAME = "user_devices_multi_settings.json"
USER_CONNECTIONS_FILENAME = "user_connections_multi_settings.json"


class DeviceManager():
    '''Device Manager
    Used to load and save information for each product
    '''

    @staticmethod
    def load_user_default_devices():
        '''
        Load data from RC save files and create products and connection objects from them
        :return:
        '''
        #todo: Remove old stow/deploy position preset settings
        old_pos_list = RC_Core.loadsave.get_settings_object(USER_OLD_POS_PRESETS_FILENAME)
        loaded_devices_list = RC_Core.loadsave.get_settings_object(USER_DEVICES_FILENAME)
        class_definitions = ProductType.get_class_definitions([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR, ProductType.CONTROL_PASSTHROUGH, ProductType.ALTERNATIVE])
        if loaded_devices_list is None or len(loaded_devices_list) < 1:
            # Load default connections if no input (base NONE instance + 1 extra connection connection)
            device = RC_Core.devicemanager.product.ProductAlpha5()
            device.load_settings(loaded_name="RIGHT ARM", loaded_connection=None, loaded_pos_preset_list=old_pos_list)
            for old_inverted_filename in USER_OLD_INVERTED_FILENAMES:
                old_inverted = RC_Core.loadsave.get_settings_object(old_inverted_filename)
                if old_inverted is not None:
                    device.set_inverted(old_inverted)
                    break
        else:
            master_arm_settings_list = []
            hid_master_arm_settings_list = []
            legacy_master_arm_settings_list = []

            bia5_settings_list = []
            # Load Main devices
            for device_settings in loaded_devices_list:
                device = None
                if device_settings['device_type'] == 'MasterArm':
                    master_arm_settings_list.append(device_settings)
                    continue
                elif device_settings['device_type'] == 'HIDMasterArm':
                    hid_master_arm_settings_list.append(device_settings)
                    continue
                elif device_settings['device_type'] == 'LegacyMasterArm':
                    legacy_master_arm_settings_list.append(device_settings)
                    continue
                else:
                    try:
                        device_ids = device_settings['device_ids']
                        device_type = device_settings['device_type']
                        device_class = None
                        for product_class in class_definitions:
                            if device_type == product_class.__name__:
                                device_class = product_class
                        if device_class is not None:
                            device = device_class(device_ids=device_ids)
                            end_effector_class_instance = None
                            if device_settings['pos_presets'] is not None and len(device_settings['pos_presets']) > 0:
                                device.load_settings(loaded_name=device_settings['name'],
                                                     loaded_connection=device_settings['connection'],
                                                     loaded_pos_preset_list=device_settings['pos_presets'],
                                                     enable_start=device_settings['enabled'],
                                                     end_effector=end_effector_class_instance)
                            else:
                                device.load_settings(loaded_name=device_settings['name'],
                                                     loaded_connection=device_settings['connection'],
                                                     loaded_pos_preset_list=old_pos_list,
                                                     enable_start=device_settings['enabled'],
                                                     end_effector=end_effector_class_instance)

                            # Handle visualisation origin

                            from RC_Core.devicemanager.product import ProductArm
                            if isinstance(device, ProductArm):

                                vis_origin = device_settings.get('vis_3d_origin', [0, 0, 0, 0, 0, 0])
                                # vis_origin = [0, 0.1, 0, 0, 0, 0]
                                if vis_origin and len(vis_origin) == 6:
                                    device.mounting_position_manager.mounting_position = vis_origin

                            from RC_Core.devicemanager.product import ProductAlpha5Probe
                            if isinstance(device, ProductAlpha5Probe):
                                probe_settings = device_settings.get('probe_settings', [25, 100, 25])

                                device.probe_setup_manager.probe_offset = probe_settings[0]
                                device.probe_setup_manager.probe_length = probe_settings[1]
                                device.probe_setup_manager.probe_diameter = probe_settings[2]

                            # try:
                            #     vis_origin = device_settings['vis_3d_origin']
                            #     if vis_origin and len(vis_origin) == 6:
                            #         device.set_visualisation_origin(vis_origin)
                            #     else:
                            #         device.set_visualisation_origin([0, 0, 0, 0, 0, 0])
                            # except KeyError:
                            #     print(__name__, 'load_user_default_devices():',
                            #           'Keyerror: "vis_3d_origin" does not exist json file date for product',
                            #           device.name, device.__class__.__name__,
                            #           '. Setting default origin to [0, 0, 0, 0, 0, 0]')
                            #     # Use if device_settings['vis_3d_origin'] does not exist
                            #     device.set_visualisation_origin([0, 0, 0, 0, 0, 0])
                    except KeyError:
                        print(__name__, "key error when loading device, skipping this device")
                print(__name__, 'load_user_default_devices():', 'Loaded user settings... # devices:',
                      len(RC_Core.devicemanager.product.RSProduct.instances))

            # Load Master Arms and pair to devices
            for master_arm_settings in master_arm_settings_list:
                try:
                    master_arm = MasterArm()
                    master_arm.name = master_arm_settings['name']
                    try:
                        master_arm.load_mapping(master_arm_settings['mapping'])
                    except KeyError as e:
                        master_arm.load_mapping('default')
                        print(__name__, 'load_user_default_devices():', '"mapping" does not exist in masterarm settings. Setting default.')
                    for connection in CommConnection.connections:
                        if connection.name == master_arm_settings['connection']:
                            master_arm.connection = connection
                    if master_arm.connection is None:
                        print(__name__, 'load_user_default_devices():', 'Loaded user settings for master arm:',
                              master_arm_settings['masterarm_pairing'],
                              'No Connection Established')
                    else:
                        print(__name__, 'load_user_default_devices():', 'Loaded user settings for device:',
                              master_arm_settings['masterarm_pairing'],
                              master_arm_settings['connection'])
                    for device in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR, ProductType.RS1_ACTUATOR]):
                        if device.master_arm is None and device.name == master_arm_settings['masterarm_pairing']:
                            device.master_arm = master_arm
                            master_arm.product = device
                except KeyError as e:
                    # Ignore master arm settings if any master arm saved settings do not exist or are incompatible
                    print(__name__, 'load_user_default_devices():', 'Loading of master arm settings dictionary KeyError, ignoring master arms:', e)

            # Load HID Master Arms and pair to devices
            for hid_master_arm_settings in hid_master_arm_settings_list:
                try:
                    master_arm = HIDMasterArm()
                    master_arm.name = hid_master_arm_settings['name']
                    for device in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR, ProductType.RS1_ACTUATOR]):
                        if device.master_arm is None and device.name == hid_master_arm_settings['masterarm_pairing']:
                            master_arm.set_target_product(device)
                    try:
                        master_arm.load_mapping(hid_master_arm_settings['mapping'])
                        if 'axis_b_use_rotate' in hid_master_arm_settings:
                            if type(hid_master_arm_settings['axis_b_use_rotate']) is bool:
                                master_arm.masterarm_axis_b_rotate = hid_master_arm_settings['axis_b_use_rotate']
                            else:
                                master_arm.masterarm_axis_b_rotate = False
                        else:
                            master_arm.masterarm_axis_b_rotate = False
                    except KeyError as e:
                        master_arm.load_mapping('default')
                        print(__name__, 'load_user_default_devices():',
                              '"mapping" does not exist in masterarm settings. Setting default.')
                except KeyError as e:
                    # Ignore master arm settings if any master arm saved settings do not exist or are incompatible
                    print(__name__, 'load_user_default_devices():',
                          'Loading of master arm settings dictionary KeyError, ignoring master arms:', e)

            for legacy_master_arm_settings in legacy_master_arm_settings_list:
                try:
                    master_arm = LegacyMasterArm()
                    master_arm.name = legacy_master_arm_settings['name']
                    for connection in CommConnection.connections:
                        if connection.name == legacy_master_arm_settings['connection']:
                            master_arm.set_connection(connection)
                    for device in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR, ProductType.RS1_ACTUATOR]):
                        if device.master_arm is None and device.name == legacy_master_arm_settings['masterarm_pairing']:
                            device.master_arm = master_arm
                            master_arm.product = device
                except KeyError as e:
                    # Ignore master arm settings if any master arm saved settings do not exist or are incompatible
                    print(__name__, 'load_user_default_devices():',
                          'Loading of master arm settings dictionary KeyError, ignoring master arms:', e)


            for bia5_settings in bia5_settings_list:

                try:
                    bia5 = Bia5Mk8()
                    bia5.name = bia5_settings['name']
                    for connection in CommConnection.connections:
                        if connection.name == bia5_settings['connection']:
                            bia5.set_connection(connection)
                    for ma in MasterArm.instances:
                        if ma.name == bia5_settings['master_arm']:
                            bia5.master_arm = ma
                except KeyError as e:
                    print(__name__, "Error loading bia5")
        #todo: System relies to much on a productr5m existing, so we are force creating one until option made
        if ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR, ProductType.RS1_ACTUATOR]) == []:
            device_make_force = RC_Core.devicemanager.product.ProductAlpha5()
            device_make_force.load_settings(loaded_name="RIGHT ARM", loaded_connection=None, loaded_pos_preset_list=old_pos_list)
        DeviceManager.save_user_settings()
        return

    @staticmethod
    def save_user_settings():
        '''
        Save current RC products and connection objects to RC save files.
        :return:
        '''
        devices_to_save = []
        # Save devices
        for product in RC_Core.devicemanager.product.RSProduct.instances:
            # Ignore master arms until next step
            if isinstance(product, RC_Core.devicemanager.product.MasterArm):
                continue
            if isinstance(product, LegacyMasterArm):
                continue
            if isinstance(product, HIDMasterArm):
                continue
            pos_presets_list = []
            for pos_preset in product.pos_preset_list:
                pos_presets_list.append({
                    'title': pos_preset.name,
                    'values': pos_preset.positions
                })
            # Get heartbeat request packets to save
            heartbeat_list = []
            if product.heartbeat_packets:
                for requests in product.heartbeat_packets:
                    heartbeat_for_device = []
                    for request in requests:
                        request_name = get_name_of_packet_id(request)
                        if request_name and request_name != 'UNKNOWN':
                            heartbeat_for_device.append(request_name)
                    heartbeat_list.append(heartbeat_for_device)
            vis_3d_origin = [0, 0, 0, 0, 0, 0]
            #
            from RC_Core.devicemanager.product import ProductArm

            settings = {'name': product.name,
                        'enabled': True,
                        'device_type': product.__class__.__name__,
                        'connection': product.connection.name,
                        'masterarm_pairing': '',
                        'heartbeat_packets': heartbeat_list,
                        'device_ids': product.device_ids,
                        'pos_presets': pos_presets_list,
                        'end_effector': product.end_effector.__class__.__name__ if hasattr(product, 'end_effector') else None,
                        'mapping': None
                        }


            if isinstance(product, ProductArm):

                vis_3d_origin = list(product.mounting_position_manager.mounting_position)

                settings.update({'vis_3d_origin': vis_3d_origin})

            from RC_Core.devicemanager.product import ProductAlpha5Probe
            if isinstance(product, ProductAlpha5Probe):
                probe_settings = [product.probe_setup_manager.probe_offset,
                                  product.probe_setup_manager.probe_length,
                                  product.probe_setup_manager.probe_diameter]
                settings.update({'probe_settings': probe_settings})
            devices_to_save.append(settings)
        # Save Master Arms
        for master_arm in MasterArm.instances:
            paired_product = None
            for product in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR, ProductType.RS1_ACTUATOR]):
                if product.master_arm and product.master_arm is master_arm:
                    paired_product = product.name
                    break
            save_as_rotate = False
            devices_to_save.append({'name': master_arm.name,
                                    'device_type': master_arm.__class__.__name__,
                                    'connection': master_arm.connection.name if master_arm.connection else None,
                                    'masterarm_pairing': paired_product,
                                    'mapping': master_arm.mapping_name,
                                    'axis_b_use_rotate': save_as_rotate
                                    })
        # Save Master Arms
        for hid_master_arm in HIDMasterArm.instances:
            paired_product = None
            if hid_master_arm.target_product:
                paired_product = hid_master_arm.target_product.name
            devices_to_save.append({'name': hid_master_arm.name,
                                    'enabled': True,
                                    'device_type': hid_master_arm.__class__.__name__,
                                    'connection': None,
                                    'masterarm_pairing': paired_product,
                                    'mapping': hid_master_arm.mapping_name,
                                    'axis_b_use_rotate': hid_master_arm.masterarm_axis_b_rotate
                                    })

        for legacy_master_arm in LegacyMasterArm.instances:
            paired_product = None
            for product in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR, ProductType.RS1_ACTUATOR]):
                if product.master_arm and product.master_arm is legacy_master_arm:
                    paired_product = product.name
                devices_to_save.append({'name': legacy_master_arm.name,
                                        'device_type': legacy_master_arm.__class__.__name__,
                                        'connection': legacy_master_arm.connection.name if legacy_master_arm.connection else None,
                                        'masterarm_pairing': paired_product})
        RC_Core.loadsave.save_settings(devices_to_save, USER_DEVICES_FILENAME)
        connections_to_save = []
        # Save connections
        for connection in CommConnection.connections:
            if connection.uid == 0:
                continue
            method = connection.type.name
            if method == CommType.SERIAL_LIBFTDI.name or method == CommType.SERIAL_FTDI.name:
                method = CommType.SERIAL.name
            if method == CommType.SERIAL.name:
                method = connection.serial_port
            connections_to_save.append({'name': connection.name,
                                        'method': method,
                                        'half_duplex': connection.half_duplex,
                                        'ip_address': connection.socket_ip,
                                        'ip_port': connection.socket_port
                                        })



        # Save settings to file
        RC_Core.loadsave.save_settings(connections_to_save, USER_CONNECTIONS_FILENAME)
        old_pos_preset_file_path = RC_Core.loadsave.get_settings_file_path(USER_OLD_POS_PRESETS_FILENAME)
        if old_pos_preset_file_path:
            if Path(old_pos_preset_file_path).exists():
                os.remove(old_pos_preset_file_path)



