from enum import IntEnum


class EndEffectors(IntEnum):

    ALPHA_PINCER_JAW = 1

    ALPHA_PARALLEL_JAW = 2

    ALPHA_QUAD_JAW = 3

    ALPHA_CUTTER = 4

    BRAVO_GRIPPER = 5

    BRAVO_PARALLEL = 6

    BRAVO_FINGERS = 7

