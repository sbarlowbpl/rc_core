from __future__ import annotations

from copy import copy
from typing import List, Tuple, Union, Dict

from RC_Core.RS1_hardware import PacketID, Mode
from RC_Core.devicemanager.masterarm.masterarm_config_lookup_handler import \
    MasterArmDefaultConfigurationLookUpTableHandler

from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.position_presets import PosPresets, DEFAULT_POSITION_PRESET_SETTINGS
from RC_Core.devicemanager.end_effectors import EndEffectors
from obstacle_visualisation import ObstacleVisualisationManager, ObstacleEditor
from obstacle_visualisation.collision_detector import CollisionDetector

from .base_products import ProductArm, KinematicsEngine
from .devices import RA703, RA703KinematicsBase, MountingBase, RA703Base, PosPresetDevice

MASTER_ARM_ENABLE_FILENAME = "master_enable.json"


class ProductAlphaArm(ProductArm):
    ra_joints: Tuple[Union[RA703, RA703Base, RA703KinematicsBase], ...] = []

    instances: List[ProductAlphaArm] = []

    bootloader_parity: str = 'EVEN'

    @property
    def device_list(self) -> Tuple[RA703, ...]:
        return self.ra_joints

    def __init__(self, *args, **kwargs):
        super(ProductAlphaArm, self).__init__(*args, **kwargs)

        if self not in ProductAlphaArm.instances:
            ProductAlphaArm.instances.append(self)

    def remove_self(self):
        super(ProductAlphaArm, self).remove_self()
        if self in ProductAlphaArm.instances:
            ProductAlphaArm.instances.remove(self)

    @property
    def joints_list(self):
        return self.ra_joints

    def goto_pos_preset(self, index: int):
        if 0 <= index < 4:
            self.control_mode = Mode.POSITION_PRESET
            for device_id in [x.device_id for x in self.joints_list]:
                self.controls[device_id] = (PacketID.POS_PRESET_GO, (index,))

    def capture_pos_preset(self, index: int):
        if 0 <= index < 4:
            for joint in self.joints_list:
                joint.send(PacketID.POS_PRESET_CAPTURE, [index])

    @property
    def ra_base(self) -> RA703Base:
        return self.ra_joints[-1]

    @property
    def mounting_position_device(self) -> MountingBase:
        return self.ra_base

    def get_base_device_id(self) -> int:
        return self.ra_base.device_id

    def get_active_device_ids(self) -> List[int]:
        return list(self.device_ids_list)

    @property
    def pos_preset_name_device(self) -> PosPresetDevice:
        return self.ra_base


class ProductAlphaArmKinematicsEngine(ProductAlphaArm, KinematicsEngine):
    instances: List[ProductAlphaArmKinematicsEngine] = []

    def __init__(self, *args, **kwargs):
        super(ProductAlphaArmKinematicsEngine, self).__init__(*args, **kwargs)
        if self not in ProductAlphaArmKinematicsEngine.instances:
            ProductAlphaArmKinematicsEngine.instances.append(self)

        self.obstacle_visualisation_manager = ObstacleVisualisationManager(self.visualisation,
                                                                           self.get_kinematics_base())

        self.obstacle_editor = ObstacleEditor(self.get_kinematics_base(), self.obstacle_visualisation_manager)

        self.collision_detector = CollisionDetector(self, self.visualisation, self.obstacle_visualisation_manager)

        # Currently only relevant for a selected device.
        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.bind(selected_device=self.on_selected_device)

    def remove_self(self):
        super(ProductAlphaArmKinematicsEngine, self).remove_self()
        if self in ProductAlphaArmKinematicsEngine.instances:
            ProductAlphaArmKinematicsEngine.instances.remove(self)

    def get_kinematics_base(self) -> RA703KinematicsBase:
        return self.ra_joints[-1]

    def goto_pos_preset(self, index: int):
        if 0 <= index < 4:
            self.control_mode = Mode.POSITION_PRESET
            self.controls[self.get_kinematics_base_device_id()] = (PacketID.POS_PRESET_GO, (index,))

    def capture_pos_preset(self, index: int):
        if 0 <= index < 4:
            self.ra_base.send(PacketID.POS_PRESET_CAPTURE, [index])


class ProductAlpha5(ProductAlphaArmKinematicsEngine):
    """
    Reach5Mini class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: List[ProductAlpha5] = []
    display_name: str = 'Alpha 5'

    def __init__(self, device_ids: list = None, **kwargs):
        """
        Initialise Alpha 5 product class
        :param device_ids:
        :param kwargs:
        """
        if device_ids is None:
            device_ids = [1, 2, 3, 4, 5, 6]
        self.ra_joints: Tuple[RA703, RA703, RA703, RA703, RA703KinematicsBase] \
            = (RA703(self.connection, device_ids[0]),
               RA703(self.connection, device_ids[1]),
               RA703(self.connection, device_ids[2]),
               RA703(self.connection, device_ids[3]),
               RA703KinematicsBase(self.connection, device_ids[4]))

        self.end_effector = EndEffectors.ALPHA_PINCER_JAW

        super(ProductAlpha5, self).__init__(device_ids=device_ids, **kwargs)

        self.allowed_km_direct_control_joints = {self.ra_joints[0], self.ra_joints[1]}

        ProductAlpha5.instances.append(self)

        # Add masterarm mappings for various master arm models
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        '''
        Remove this product object from all instances, similar to deleting the product
        :return:
        '''
        super(ProductAlpha5, self).remove_self()
        if self in ProductAlpha5.instances:
            ProductAlpha5.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name: str = '', loaded_connection: CommConnection = None,
                      loaded_pos_preset_list: list = None, enable_start: bool = False, end_effector=None):
        '''
        Load product settings from input details (largely from .json file)
        :param loaded_name: Name
        :param loaded_connection: connection object
        :param loaded_pos_preset_list: (deprecated)
        :param enable_start: (deprecated)
        :param end_effector: end effector type - standard, quad, and pincer jaws for example
        :return:
        '''
        self.name = loaded_name
        # Load position preset (deprecated)
        # todo: remove old in-software position preset stow details
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlpha5Probe(ProductAlphaArmKinematicsEngine):
    """
    Reach5Mini probe class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: list = []
    display_name = 'Alpha 5 Probe'

    def __init__(self, device_ids=None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 3, 4, 5, 6]

        self.ra_joints: Tuple[RA703, RA703, RA703, RA703, RA703KinematicsBase] \
            = (RA703(self.connection, device_ids[0]),
               RA703(self.connection, device_ids[1]),
               RA703(self.connection, device_ids[2]),
               RA703(self.connection, device_ids[3]),
               RA703KinematicsBase(self.connection, device_ids[4]))

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlpha5Probe, self).__init__(device_ids=device_ids, **kwargs)

        self.allowed_km_direct_control_joints = set()

        ProductAlpha5Probe.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

        from probe_setup.probe_setup_manager import ProbeSetupManager
        self.probe_setup_manager = ProbeSetupManager(self.ra_joints[0], self.visualisation)

    def remove_self(self):
        super(ProductAlpha5Probe, self).remove_self()
        if self in ProductAlpha5Probe.instances:
            ProductAlpha5Probe.instances.remove(self)


    def update_vis_probe(self, *largs):
        # Read from the probe, and get the parameters.
        pass

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)


class ProductAlpha5InlineC(ProductAlphaArmKinematicsEngine):
    """
    Alpha5InlineC class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: list = []
    display_name = 'Alpha 5 Incline C'

    def __init__(self, device_ids=None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 3, 4, 5, 6]

        self.ra_joints: Tuple[RA703, RA703, RA703, RA703, RA703KinematicsBase] \
            = (RA703(self.connection, device_ids[0]),
               RA703(self.connection, device_ids[1]),
               RA703(self.connection, device_ids[2]),
               RA703(self.connection, device_ids[3]),
               RA703KinematicsBase(self.connection, device_ids[4]))

        self.end_effector = EndEffectors.ALPHA_PINCER_JAW

        print(__name__, "__init__():", __class__.__name__, self.name)

        super(ProductAlpha5InlineC, self).__init__(device_ids=device_ids, **kwargs)

        self.allowed_km_direct_control_joints = {self.ra_joints[0], self.ra_joints[1]}

        ProductAlpha5InlineC.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()
        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlpha5InlineC, self).remove_self()
        if self in ProductAlpha5InlineC.instances:
            ProductAlpha5InlineC.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlpha2RotatingGrabber(ProductAlphaArm):
    """
    ProductRS1F2_Grabber class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: list = []
    display_name = 'Alpha 2 Grabber'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2]

        self.ra_joints = (RA703(self.connection, device_ids[0]),
                          RA703Base(self.connection, device_ids[1]))

        self.end_effector = EndEffectors.ALPHA_PINCER_JAW

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlpha2RotatingGrabber, self).__init__(device_ids=device_ids, **kwargs)

        ProductAlpha2RotatingGrabber.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()
        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlpha2RotatingGrabber, self).remove_self()
        ProductAlpha2RotatingGrabber.instances.remove(self)
        for prod in ProductAlpha2RotatingGrabber.instances:
            if self == prod:
                ProductAlpha2RotatingGrabber.instances.remove(prod)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlpha2DualBend(ProductAlphaArm):
    """
    ProductRS1F2_DualBend class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: List[ProductAlpha2DualBend] = []
    display_name = 'Alpha 2 Dual Bend'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2]

        self.ra_joints = (RA703(self.connection, device_ids[0]),
                          RA703Base(self.connection, device_ids[1]))

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlpha2DualBend, self).__init__(device_ids=device_ids, **kwargs)


        ProductAlpha2DualBend.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()
        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlpha2DualBend, self).remove_self()
        if self in ProductAlpha2DualBend.instances:
            ProductAlpha2DualBend.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlpha3(ProductAlphaArm):
    """
    ProductRS1F3 class.
    self.device_ids defaults to [1,2,3,4]
    """
    instances: List[ProductAlpha3] = []
    display_name = 'Alpha 3'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 3, 4]

        self.ra_joints = (RA703(self.connection, device_ids[0]),
                          RA703(self.connection, device_ids[1]),
                          RA703Base(self.connection, device_ids[2]))

        self.end_effector = EndEffectors.ALPHA_PINCER_JAW

        print(__name__, "__init__():", "ProductRS1F3 name:", self.name)
        super(ProductAlpha3, self).__init__(device_ids=device_ids, **kwargs)

        ProductAlpha3.instances.append(self)

        # Orientation mode for master_arm passthrough
        self.orientation_mode = "cw_positive"

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlpha3, self).remove_self()
        if self in ProductAlpha3.instances:
            ProductAlpha3.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlpha4(ProductAlphaArm):
    """
    ProductAlpha3 class.
    self.device_ids defaults to [1,2,3,4]
    """
    instances: List[ProductAlpha4] = []
    display_name = 'Alpha 4'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 3, 4]

        self.ra_joints = (RA703(self.connection, device_ids[0]),
                          RA703(self.connection, device_ids[1]),
                          RA703(self.connection, device_ids[2]),
                          RA703Base(self.connection, device_ids[3]))

        self.end_effector = EndEffectors.ALPHA_PINCER_JAW

        print(__name__, "__init__():", "ProductAlpha4 name:", self.name)
        super(ProductAlpha4, self).__init__(device_ids=device_ids, **kwargs)

        ProductAlpha4.instances.append(self)


        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlpha4, self).remove_self()

        if self in ProductAlpha4.instances:
            ProductAlpha4.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlphaSingleRotate(ProductAlphaArm):
    """
    ProductAlphaSingleRotate class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: List[ProductAlphaSingleRotate] = []
    f1_uid = -1
    display_name = 'Alpha Single Rotate'

    # use_master_arm = False

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1]

        self.ra_joints = (RA703Base(self.connection, device_ids[0]),)

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlphaSingleRotate, self).__init__(device_ids=device_ids, **kwargs)

        ProductAlphaSingleRotate.instances.append(self)
        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlphaSingleRotate, self).remove_self()
        if self in ProductAlphaSingleRotate.instances:
            ProductAlphaSingleRotate.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlphaSingle90DegRotate(ProductAlphaArm):
    """
    ProductAlphaSingle90DegRotate class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: List[ProductAlphaSingle90DegRotate] = []
    f1_uid = -1
    display_name = 'Alpha Single 90 Deg Rotate'

    # use_master_arm = False

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1]

        self.ra_joints = (RA703Base(self.connection, device_ids[0]),)

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlphaSingle90DegRotate, self).__init__(device_ids=device_ids, **kwargs)

        ProductAlphaSingle90DegRotate.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlphaSingle90DegRotate, self).remove_self()
        if self in ProductAlphaSingle90DegRotate.instances:
            ProductAlphaSingle90DegRotate.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductAlpha290DegRotate(ProductAlphaArm):
    """
    ProductRS1F2_Grabber class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: List[ProductAlpha290DegRotate] = []
    display_name = 'Alpha 2 90 Deg Rotate'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2]

        self.ra_joints = (RA703(self.connection, device_ids[0]),
                          RA703Base(self.connection, device_ids[1]))

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlpha290DegRotate, self).__init__(device_ids=device_ids, **kwargs)

        ProductAlpha290DegRotate.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlpha290DegRotate, self).remove_self()
        if self in ProductAlpha290DegRotate.instances:
            ProductAlpha290DegRotate.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector


class ProductAlphaGrabber(ProductAlphaArm):
    """
    ProductRS1F2_Grabber class.

    """
    instances: List[ProductAlphaGrabber] = []
    display_name = 'Alpha Grabber'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1]

        self.ra_joints = (RA703Base(self.connection, device_ids[0]),)

        self.end_effector = EndEffectors.ALPHA_PINCER_JAW

        print(__name__, "__init__():", __class__.__name__, self.name)
        super(ProductAlphaGrabber, self).__init__(device_ids=device_ids, **kwargs)


        ProductAlphaGrabber.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()


        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductAlphaGrabber, self).remove_self()
        if self in ProductAlphaGrabber.instances:
            ProductAlphaGrabber.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector