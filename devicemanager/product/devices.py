"""
devices.py

Holds the different kinds of devices and arm coulds have.

Properties of the devices refer value of that property in the arm.
A device is an event dispatcher so that individual properties can be bound to and events can be triggered
 when a value is changed.

Each Device has an attributed connection, and a device id.

The base functions:
- request: Asyncronous. Sends a request, and waits for a response. has options to retry.
- set: Asyncronous. Sends a value, requests, and check if the value requested is the same as the value send.
- send: Non Asyncronous: Sends a value

- save: Send the save command

The properties of a devices are storied as kivy properties, that are initialised as None.
None means that no value has been received.

When being set as kivy properties. They can be bound to by callbacks to trigger when they change.

You have the options of havings lists, for a range of variables, Numeric P

"""
import math

import asyncio
from typing import Optional, Union, List, Tuple

from scipy.spatial.transform import Rotation as R

from kivy.event import EventDispatcher
from kivy.properties import NumericProperty, ObjectProperty, ListProperty, StringProperty, ReferenceListProperty, \
    BooleanProperty

from RC_Core.RS1_hardware import Mode
from RC_Core.commconnection import CommConnection

import rc_logging
from RC_Core.enums.packets import PacketID

logger = rc_logging.getLogger(__name__)


class Device(EventDispatcher):
    device_id: int = NumericProperty(None, allownone=True)
    mode: Optional[int] = NumericProperty(None, allownone=True)
    device_type: Optional[int] = NumericProperty(None, allownone=True)
    connection: Optional[CommConnection] = ObjectProperty(None, allownone=True)

    _save_lock: asyncio.Lock = None

    connected: bool = BooleanProperty(False)

    def __init__(self, connection: Optional[CommConnection], device_id: int, **kwargs):
        super(Device, self).__init__(**kwargs)
        self.device_id = device_id
        self.connection = connection

        self._save_lock = asyncio.Lock()

    async def request(self, packet_id, timeout=0.15, attempts=3) -> Optional[Union[List[float], List[int]]]:
        if self.connection is None or not self.connection.connected:
            return None

        for i in range(attempts):
            try:
                return await self.connection.request(self.device_id, packet_id, timeout)
            except TimeoutError:
                # logger.debug(f"Request attempt {i + 1}")
                pass
                # return None
        # logger.debug(f"Request for Packet|DeviceID: {packet_id}|{self.device_id} "
                     # f"timed out on connection: {self.connection.name}")
        return None

    async def set(self, packet_id: int, data: Union[List[int], List[float]], timeout=0.15, attempts=3) -> bool:
        if self.connection is None:
            return False

        for i in range(attempts):
            try:
                result = await self.connection.set(self.device_id, packet_id, data, timeout)
                if result:
                    return result

            except TimeoutError:
                logger.debug(f"Set for Packet|DeviceID|Data: {packet_id}|{self.device_id}|{data} "
                             f"timed out on connection: {self.connection.name}")

            except ValueError:
                logger.debug(f"Set for Packet|DeviceID|Data: {packet_id}|{self.device_id}|{data} "
                             f"did_not_set correctly: {self.connection.name}")
        return False

    def send(self, packet_id: int, data: Union[List[int], List[float]]) -> bool:
        if self.connection:
            self.connection.send(self.device_id, packet_id, data)
            return True
        return False

    async def get_mode(self):
        packet: Optional[List[int]] = await self.request(PacketID.MODE)
        if packet is not None:
            self.mode = packet[0]
        else:
            self.mode = None

    async def set_mode(self, mode):
        mode = int(mode)
        if await self.set(PacketID.MODE, [mode]):
            self.mode = mode
            return True
        else:
            return False

    async def factory_save(self, timeout=1.5):
        if self.connection is None:
            logger.warning("No Connection")
            return False

        if self._save_lock.locked():
            logger.warning("Device is currently attempting to save, cannot save")

        async with self._save_lock:
            if not await self.set_mode(int(Mode.FACTORY)):
                logger.warning("Setting mode to factory failed")
                return False

            initial_save_packet = await self.request(PacketID.SAVE)
            if initial_save_packet is None:
                logger.warning("Getting initial Num saves failed")
                await self.set_mode(Mode.STANDBY)

                return False
            initial_save_value = initial_save_packet[0]

            self.connection.send(self.device_id, PacketID.SAVE, [0])

            await asyncio.sleep(0.01)

            interval = 0.3
            save_success = False
            save_packet = None
            for i in range(int(timeout // interval)):
                save_packet = await self.request(PacketID.SAVE)

                if save_packet is None:
                    # logger.warning("Getting result Num saves failed")
                    pass
                else:
                    save_success = True
                    break

            if not save_success:
                logger.warning("Getting result Num saves failed")
                await self.set_mode(Mode.STANDBY)
                return False

            save_packet_value = save_packet[0]

            if save_packet_value == initial_save_value + 1:
                await self.set_mode(Mode.STANDBY)
                return True

            else:
                logger.warning(f"Got save response: {save_packet_value}, but expected: {initial_save_value + 1}. "
                               f"Save failed")
                await self.set_mode(Mode.STANDBY)
                return False

    async def get_device_type(self):
        from RC_Core.connectionmanager import ConnectionManager
        if not ConnectionManager.instance.run_connection_loop:
            return
        packet: Optional[List[int]] = await self.request(PacketID.DEVICE_TYPE)
        if packet is not None:
            self.device_type = packet[0]
        else:
            return
            # self.device_type = None

    async def reload(self):
        await self.get_mode()

    def save(self):
        self.send(PacketID.SAVE, [0])

    software_version: Optional[list] = ListProperty(None, allownone=True)

    async def get_software_version(self):
        packet: Optional[List[int]] = await self.request(PacketID.SOFTWARE_VERSION)
        if packet is not None:
            self.software_version = packet
        else:
            self.software_version = None

    session_runtime = NumericProperty(None, allownone=True)
    lifetime_runtime = NumericProperty(None, allownone=True)

    async def get_run_time(self):
        result = await self.request(PacketID.RUN_TIME)
        if result:
            self.session_runtime = result[0]
            if len(result) > 1:
                self.lifetime_runtime = result[1]
            else:
                self.lifetime_runtime = None
        else:
            self.session_runtime = None
            self.lifetime_runtime = None

    pass

    def clear_parameters(self):
        self.session_runtime = None
        self.lifetime_runtime = None

        self.software_version = None
        self.device_type = None
        self.mode = None


class PosPresetDevice(Device):
    pos_preset_name_0 = StringProperty(None, allownone=True)
    pos_preset_name_1 = StringProperty(None, allownone=True)
    pos_preset_name_2 = StringProperty(None, allownone=True)
    pos_preset_name_3 = StringProperty(None, allownone=True)
    pos_preset_names = ReferenceListProperty(pos_preset_name_0, pos_preset_name_1, pos_preset_name_2, pos_preset_name_3)

    async def get_pos_preset_name(self, index: int):
        if not 0 <= index < 4:
            raise IndexError(f"Invalid pos_preset {index}")
        value: List[int] = await self.request(PacketID.POS_PRESET_NAME_0 + index)
        if value:
            self.pos_preset_names[index] = bytes(value).decode()
        else:
            self.pos_presets[index] = None
        pass

    async def get_pos_preset_names(self):
        for i in range(4):
            await self.get_pos_preset_name(i)

    async def set_pos_preset_name(self, index: int, name: str):

        _name = name[:8]
        if not 0 <= index < 4:
            raise IndexError(f"Invalid index {index}")

        data = list(_name.encode())[:8]

        result = await self.set(PacketID.POS_PRESET_NAME_0 + index, data)
        if result:
            self.pos_preset_names[index] = _name
        else:
            self.pos_preset_names[index] = None

    pos_presets: List[Optional[Tuple[float, ...]]] = ListProperty([None] * 6, allownone=True)

    async def get_pos_preset(self, index: int):
        if not 0 <= index < 4:
            raise IndexError(f"Invalid pos_preset index {index}")

        result = await self.request(PacketID.POS_PRESET_SET_0 + index)
        if result:
            self.pos_presets[index] = tuple(result)
        else:
            self.pos_presets[index] = None

    async def get_pos_presets(self):
        for i in range(4):
            await self.get_pos_preset(i)
        pass

    async def set_pos_preset(self, index: int, values: List[float]):
        if not 0 <= index < 4:
            raise IndexError(f"Invalid pos_preset index {index}")
        if len(values) > 8:
            raise ValueError("Length of joints values of pos_preset is invalid")

        _values = [0.0] * 8
        _values[:len(values)] = values
        result = await self.set(PacketID.POS_PRESET_SET_0, _values)
        if result:
            self.pos_presets[index] = tuple(values)
        else:
            self.pos_presets[index] = None

    def clear_pos_presets(self):
        self.pos_preset_name_0 = None
        self.pos_preset_name_1 = None
        self.pos_preset_name_2 = None
        self.pos_preset_name_3 = None

class Joint(Device):
    position: Optional[float] = NumericProperty(None, allownone=True)
    velocity: Optional[float] = NumericProperty(None, allownone=True)
    current: Optional[float] = NumericProperty(None, allownone=True)

    async def get_position(self):
        packet: Optional[List[int]] = await self.request(PacketID.POSITION)
        if packet is not None:
            self.position = packet[0]
        else:
            self.position = None

    def command_position(self, position: float):
        if self.connection is None:
            return
        self.connection.send(self.device_id, PacketID.POSITION, [position])

    async def get_velocity(self):
        packet: Optional[List[int]] = await self.request(PacketID.VELOCITY)
        if packet is not None:
            self.velocity = packet[0]
        else:
            self.velocity = None

    def command_velocity(self, velocity: float):
        if self.connection is None:
            return
        self.connection.send(self.device_id, PacketID.VELOCITY, [velocity])

    async def get_current(self):
        packet: Optional[List[int]] = await self.request(PacketID.CURRENT)
        if packet is not None:
            self.current = packet[0]
        else:
            self.current = None

    def command_relative_position(self, relative_position: float):
        if self.connection is None:
            return
        self.connection.send(self.device_id, PacketID.RELATIVE_POSITION, [relative_position])

    mechanical_version: Optional[list] = ListProperty(None, allownone=True)

    async def get_mechanical_version(self):
        packet: Optional[List[int]] = await self.request(PacketID.MECHANICAL_VERSION)
        if packet is not None:
            self.mechanical_version = packet
        else:
            self.mechanical_version = None

    position_limit_max: Optional[float] = NumericProperty(None, allownone=True)
    position_limit_min: Optional[float] = NumericProperty(None, allownone=True)
    position_limits = ReferenceListProperty(position_limit_max, position_limit_min)

    async def get_position_limits(self):
        result = await self.request(PacketID.POSITION_LIMIT)
        if result:
            self.position_limits = result
            return result
        else:
            self.position_limit_max = None
            self.position_limit_max = None
            return None

    async def set_position_limits(self, max_value: float, min_value: float):
        result = self.set(PacketID.POSITION_LIMIT, [max_value, min_value])

        if result:
            self.position_limits = [max_value, min_value]
            return True
        else:
            await self.get_position_limits()
            return False

    async def set_position_limit_max(self, max_value: float):
        if self.position_limit_min is None:
            return

        result = await self.set(PacketID.POSITION_LIMIT, [max_value, self.position_limit_min])
        if result:
            self.position_limit_max = max_value
            return True
        else:
            await self.get_position_limits()
            return False

    async def set_position_limit_min(self, min_value: float):
        if self.position_limit_max is None:
            return

        result = await self.set(PacketID.POSITION_LIMIT, [self.position_limit_max, min_value])
        if result:
            self.position_limit_min = min_value
            return True
        else:
            await self.get_position_limits()
            return False

    factory_position_limit_max: Optional[float] = NumericProperty(None, allownone=True)
    factory_position_limit_min: Optional[float] = NumericProperty(None, allownone=True)
    factory_position_limits = ReferenceListProperty(factory_position_limit_max, factory_position_limit_min)

    async def get_factory_position_limits(self):
        result = await self.request(PacketID.POSITION_LIMIT_FACTORY)
        if result:
            self.factory_position_limits = result
        else:
            self.factory_position_limit_max = None
            self.factory_position_limit_min = None

    velocity_limit_max: Optional[float] = NumericProperty(None, allownone=True)
    velocity_limit_min: Optional[float] = NumericProperty(None, allownone=True)
    velocity_limits = ReferenceListProperty(velocity_limit_max, velocity_limit_min)

    async def get_velocity_limits(self):
        result = await self.request(PacketID.VELOCITY_LIMIT)
        if result:
            self.velocity_limits = result
        else:
            self.velocity_limits = None

    async def set_velocity_limits(self, max_value: float, min_value: float):
        result = self.set(PacketID.VELOCITY_LIMIT, [max_value, min_value])

        if result:
            self.velocity_limits = [max_value, min_value]
            return True
        else:
            await self.get_velocity_limits()
            return False

    current_limit_max: Optional[float] = NumericProperty(None, allownone=True)
    current_limit_min: Optional[float] = NumericProperty(None, allownone=True)
    current_limits = ReferenceListProperty(current_limit_max, current_limit_min)

    async def get_current_limits(self):
        result = await self.request(PacketID.CURRENT_LIMIT)
        if result:
            self.current_limits = result
        else:
            self.current_limits = None

    async def set_current_limits(self, max_value: float, min_value: float):
        result = self.set(PacketID.CURRENT_LIMIT, [max_value, min_value])

        if result:
            self.current_limits = [max_value, min_value]
            return True
        else:
            await self.get_current_limits()
            return False

    def clear_parameters(self):
        self.position = None
        self.velocity = None
        self.current = None
        self.position_limit_max = None
        self.position_limit_min = None
        self.velocity_limit_max = None
        self.velocity_limit_min = None
        self.current_limit_max = None
        self.current_limit_min = None
        self.factory_position_limit_min = None
        self.factory_position_limit_max = None
        self.mechanical_version = None
        super(Joint, self).clear_parameters()

class MountingBase(Device):
    km_mount_pos_rot = ListProperty(None, allownone=True)

    async def get_km_mount_pos_rot(self):
        result = await self.request(PacketID.KM_MOUNT_POS_ROT)
        if result:
            self.km_mount_pos_rot = result
        else:
            self.km_mount_pos_rot = None

    async def set_km_mount_pos_rot(self, km_mount_pos_rot: List[float]):
        result = await self.set(PacketID.KM_MOUNT_POS_ROT, km_mount_pos_rot)

        if result:
            self.km_mount_pos_rot = km_mount_pos_rot
        else:
            self.km_mount_pos_rot = None

    def clear_mounting_base(self):
        self.km_mount_pos_rot = None


class EndEffector(Device):
    endeffector_offset = ListProperty(None, allownone=True)

    async def get_endeffector_offset(self):
        while self.connection and not self.connection.connected:
            await asyncio.sleep(0.5)

        result = await self.request(PacketID.LINK_END_EFFECTOR_OFFSET, timeout=0.5)

        if result:
            pos = result[0:3]
            rot = list(R.from_rotvec(result[3:6]).as_euler('xyz', degrees=True))

            self.endeffector_offset = pos + rot
        else:
            self.endeffector_offset = None

    async def set_endeffector_offset_rpy(self, endeffector_offset: List[float]):
        pos = endeffector_offset[0:3]
        rot = list(R.from_euler('xyz', endeffector_offset[3:6], degrees=True).as_rotvec())
        data = pos + rot

        result = await self.set(PacketID.LINK_END_EFFECTOR_OFFSET, data)

        if result:
            self.endeffector_offset = endeffector_offset
        else:
            self.endeffector_offset = None

    def clear_endeffector_offset(self):
        self.endeffector_offset = None


class RA703(Joint, PosPresetDevice, MountingBase):

    position_parameters = ListProperty(None, allownone=True)

    async def get_position_parameters(self):
        result = await self.request(PacketID.POSITION_PARAMETERS)

        if result:
            self.position_parameters = result
        else:
            self.position_parameters = None

    async def set_position_parameters(self, position_parameters):

        result = await self.set(PacketID.POSITION_PARAMETERS, position_parameters)

        if result:
            self.position_parameters = position_parameters
            return True
        else:
            self.position_parameters = None
            return False

    async def set_position_parameters_beta(self, beta):
        if self.position_parameters is None:
            if not await self.get_position_parameters():
                return False

        new_pos_parameters = self.position_parameters.copy()
        new_pos_parameters[3] = beta
        return await self.set_position_parameters(new_pos_parameters)

    dh_parameters_0 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_1 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_2 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_3 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_4 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_5 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_6 = ListProperty(defaultvalue=None, allownone=True)
    dh_parameters_7 = ListProperty(defaultvalue=None, allownone=True)

    dh_parameters = ReferenceListProperty(dh_parameters_0,
                                          dh_parameters_1,
                                          dh_parameters_2,
                                          dh_parameters_3,
                                          dh_parameters_4,
                                          dh_parameters_5,
                                          dh_parameters_6,
                                          dh_parameters_7)

    km_float_parameters = ListProperty(defaultvalue=None, allownone=True)

    async def get_dh_parameters(self, index):
        result = await self.request(PacketID.KM_DH_PARAMETERS_0 + index)

        if result is not None:
            self.dh_parameters[index] = result
            return True
        else:
            self.dh_parameters[index] = None
            return False

    async def set_dh_parameters(self, index, dh_parameters):
        result = await self.set(PacketID.KM_DH_PARAMETERS_0 + index, dh_parameters)

        if result:
            self.dh_parameters[index] = dh_parameters
        else:
            self.dh_parameters[index] = None
            await self.get_dh_parameters(index)

    async def set_dh_parameters_d(self, index, d):
        if self.dh_parameters[index] is None:
            if not await self.get_dh_parameters(index):
                return False
        new_dh_params = self.dh_parameters[index].copy()
        new_dh_params[0] = d
        return await self.set_dh_parameters(index, new_dh_params)

    async def set_dh_parameters_a(self, index, a):
        if self.dh_parameters[index] is None:
            if not await self.get_dh_parameters(index):
                return False
        new_dh_params = self.dh_parameters[index].copy()
        new_dh_params[1] = a
        return await self.set_dh_parameters(index, new_dh_params)

    async def get_km_float_parameters(self):
        result = await self.request(PacketID.KM_FLOAT_PARAMETERS)

        if result is not None:
            self.km_float_parameters = result
            return True
        else:
            self.km_float_parameters = None
            return False

    async def set_km_float_parameters(self, km_float_parameters):
        result = await self.set(PacketID.KM_FLOAT_PARAMETERS, km_float_parameters)

        if result:
            self.km_float_parameters = km_float_parameters
        else:
            self.km_float_parameters = None
            await self.get_km_float_parameters()

    async def set_km_float_parameters_radius(self, self_collision_radius):
        if self.km_float_parameters is None:
            if not await self.get_km_float_parameters():
                return False
        new_km_float_params = self.km_float_parameters.copy()
        new_km_float_params[4] = self_collision_radius
        return await self.set_km_float_parameters(new_km_float_params)

    def clear_parameters(self):
        self.position_parameters = None
        self.clear_pos_presets()
        self.clear_mounting_base()
        super(Joint, self).clear_parameters()





class ObstacleBase(Device):
    box_obstacles: Optional[List[Optional[Tuple[float, ...]]]] = ListProperty([None] * 6,
                                                                              allownone=False)
    cylinder_obstacles: Optional[List[Optional[Tuple[float, ...]]]] = ListProperty([None] * 6,
                                                                                   allownone=False)

    async def get_box_obstacle(self, index: int):
        if not 0 <= index < 6:
            logger.warning(f" get_box_obstacle(): Invalid index for box obstacle: {index}")
            return
        packet: Optional[List[float]] = await self.request(PacketID.KM_BOX_OBSTACLE_00 + index)
        if packet is not None:
            self.box_obstacles[index] = tuple(packet[0:6])
            return self.box_obstacles[index]
        else:
            self.box_obstacles[index] = None
            return None

    async def get_cylinder_obstacle(self, index: int):
        if not 0 <= index < 6:
            logger.warning(f" get_box_obstacle(): Invalid index for box obstacle: {index}")
            return
        packet: Optional[List[float]] = await self.request(PacketID.KM_CYLINDER_OBSTACLE_00 + index)
        if packet is not None:
            self.cylinder_obstacles[index] = tuple(packet)
        else:
            self.cylinder_obstacles[index] = None

    async def get_all_box_obstacles(self):
        input_coroutines = []
        for i in range(6):
            input_coroutines.append(self.get_box_obstacle(i))
        res = await asyncio.gather(*input_coroutines)
        return res

    async def get_all_cylinder_obstacles(self):
        input_coroutines = []
        for i in range(6):
            input_coroutines.append(self.get_cylinder_obstacle(i))
        res = await asyncio.gather(*input_coroutines)
        return res

    async def get_all_obstacles(self):
        # await asyncio.gather(self.get_all_box_obstacles(), self.get_all_cylinder_obstacles())
        await self.get_all_box_obstacles()
        await self.get_all_cylinder_obstacles()

    async def set_box_obstacle(self, index: int, coords1: Tuple[float, float, float],
                               coords2: Tuple[float, float, float]) -> bool:
        """

        :raises ValueError: For invalid index
        """
        if not 0 <= index < 6:
            logger.warning(f"Invalid box index: {index}")
            raise ValueError(f"Invalid box index: {index}")
        _coords1 = [min(x, y) for x, y in zip(coords1, coords2)]
        _coords2 = [max(x, y) for x, y in zip(coords1, coords2)]

        result = await self.set(PacketID.KM_BOX_OBSTACLE_00 + index, _coords1 + _coords2)

        if result:
            self.box_obstacles[index] = tuple(_coords1 + _coords2)
            return True
        else:
            logger.info(f"Did not receive response for box_obstacle: {index}, setting to none and then requesting")
            self.box_obstacles[index] = None
            await self.get_box_obstacle(index)
            return False

    async def set_cylinder_obstacle(self, index, point1: Tuple[float, float, float], point2: Tuple[float, float, float],
                                    radius: float):
        if not 0 <= index < 6:
            logger.warning(f"Invalid cylinder index: {index}")
            raise ValueError(f"Invalid cylinder index: {index}")
        result = await self.set(PacketID.KM_CYLINDER_OBSTACLE_00 + index, list(point1) + list(point2) + [radius])
        if result:
            self.cylinder_obstacles[index] = tuple(list(point1) + list(point2) + [radius])
        else:
            logger.info(f"Did not receive response for cylinder obstacle: {index}, setting to none and then requesting")
            self.cylinder_obstacles[index] = None
            await self.get_cylinder_obstacle(index)

    async def clear_box_obstacle(self, index: int):
        await self.set_box_obstacle(index, (0, 0, 0), (0, 0, 0))

    async def clear_cylinder_obstacle(self, index: int):
        await self.set_cylinder_obstacle(index, (0, 0, 0), (0, 0, 0), 0)

    async def clear_box_obstacles(self, factory=False):
        for i in range(0 if factory else 2, 6):
            await self.clear_box_obstacle(i)

    async def clear_cylinder_obstacles(self, factory=False):
        for i in range(0 if factory else 2, 6):
            await self.clear_cylinder_obstacle(i)

    async def clear_all_obstacles(self, factory=False):
        await self.clear_box_obstacles(factory)
        await self.clear_cylinder_obstacles(factory)

    def clear_obstacle_base(self):
        self.box_obstacles = [None] * 6
        self.cylinder_obstacles = [None] * 6


class RA703Base(RA703, MountingBase):
    def __init__(self, *args):
        super(RA703Base, self).__init__(*args)

    def clear_parameters(self):
        self.clear_mounting_base()
        super(RA703Base, self).clear_parameters()


class RA703KinematicsBase(RA703Base, ObstacleBase):
    def __init__(self, *args):
        super(RA703KinematicsBase, self).__init__(*args)
        self.box_obstacles = [None] * 6
        self.cylinder_obstacles = [None] * 6
        self.pos_presets = [None] * 4

    km_end_pos: Optional[List[float]] = ListProperty(None, allownone=True)

    async def get_km_end_pos(self):
        packet: Optional[List[float]] = await self.request(PacketID.KM_END_POS)
        if packet is not None:
            self.km_end_pos = packet
        else:
            self.km_end_pos = None

    def command_km_end_pos(self, command: Tuple[float, float, float, float, float, float]):
        self.send(PacketID.KM_END_POS, list(command))
        pass

    def command_km_end_vel(self, command: Tuple[float, float, float, float, float, float]):
        self.send(PacketID.KM_END_VEL, list(command))

    async def pos_preset_capture(self, index):
        if not 0 <= index < 4:
            raise IndexError(f"Invalid pos_preset index {index}")
        self.send(PacketID.POS_PRESET_CAPTURE, [index])
        await self.get_pos_preset(index)

    def command_pos_preset_go(self, index):
        if not 0 <= index < 4:
            raise IndexError(f"Invalid pos_preset index {index}")
        self.send(PacketID.POS_PRESET_GO, index)

    def clear_parameters(self):
        self.clear_obstacle_base()
        super(RA703KinematicsBase, self).clear_parameters()

class RB703(Joint, PosPresetDevice, EndEffector):

    def clear_parameters(self):
        self.clear_pos_presets()
        super(RB703, self).clear_parameters()


class RB708(MountingBase):

    ip_address = StringProperty(None, allownone=True)
    udp_port = NumericProperty(None, allownone=True)

    async def get_ip_address(self):
        result = await self.request(PacketID.ETH_IP_ADDRESS)

        if result:
            string_data = [str(x) for x in result]
            self.ip_address = ".".join(string_data)
        else:
            self.ip_address = None

    async def set_ip_address_string(self, ip_string: str):

        split_ip = ip_string.split(".")
        if len(split_ip) != 4:
            logger.info(f"set_ip_address_string {ip_string} Incorrect format")
            return False
        for i in split_ip:
            try:
                num = int(i)
            except ValueError:
                logger.info(f"set_ip_address_string {ip_string} Incorrect format")
                return False
            if num < 0 or num > 255:
                logger.info(f"set_ip_address_string {ip_string} Incorrect format")
                return False

        ip_address = [int(x) for x in split_ip]
        await self.set_ip_address_int(ip_address)

    async def set_ip_address_int(self, ip: List[int]):
        if len(ip) != 4:
            return False

        result = await self.set(PacketID.ETH_IP_ADDRESS, ip)

        if result:
            string_data = [str(x) for x in ip]
            self.ip_address = ".".join(string_data)
        else:
            self.ip_address = None
        pass

    async def get_udp_port(self):
        result = await self.request(PacketID.ETH_PORT)

        if result:
            self.udp_port = int(result[0])
        else:
            self.udp_port = None

    async def set_udp_port(self, udp_port: int):
        result = await self.set(PacketID.ETH_PORT, [udp_port])

        if result:
            self.udp_port = udp_port
        else:
            self.udp_port = None

    eth_parameters_mac_address = ListProperty(None, allownone=True)
    eth_parameters_ip_address = StringProperty(None, allownone=True)
    eth_parameters_subnet = StringProperty(None, allownone=True)
    eth_parameters_gateway = StringProperty(None, allownone=True)
    eth_parameters_dns = StringProperty(None, allownone=True)
    eth_parameters_dhcp_mode = NumericProperty(None, allownone=True)

    async def get_eth_parameters(self):
        result = await self.request(PacketID.ETH_PARAMETERS)
        if result:
            if len(result) != 23:
                self.eth_parameters_mac_address = None
                self.eth_parameters_ip_address = None
                self.eth_parameters_subnet = None
                self.eth_parameters_gateway = None
                self.eth_parameters_dns = None
                self.eth_parameters_dhcp_mode = None
                return

            self.eth_parameters_mac_address = result[0:6]

            # IP ADDRESS
            string_data = [str(x) for x in result[6:10]]
            self.eth_parameters_ip_address = ".".join(string_data)

            # SUBNET
            string_data = [str(x) for x in result[10:14]]
            self.eth_parameters_subnet = ".".join(string_data)
            # GATEWAY
            string_data = [str(x) for x in result[14:18]]
            self.eth_parameters_gateway = ".".join(string_data)
            # DNS
            string_data = [str(x) for x in result[18:22]]
            self.eth_parameters_dns = ".".join(string_data)
            # DHCP
            self.eth_parameters_dhcp_mode = result[22]

        else:
            self.eth_parameters_mac_address = None
            self.eth_parameters_ip_address = None
            self.eth_parameters_subnet = None
            self.eth_parameters_gateway = None
            self.eth_parameters_dns = None
            self.eth_parameters_dhcp_mode = None

    async def set_ethernet_parameters(self, mac_address: List[int], ip_address: str, subnet: str, gateway: str,
                                      dns: str, dhcp_mode: int):
        if len(mac_address) != 6:
            logger.warning(f"Invalid mac_address_length: {len(mac_address)}")
            return False

        data = mac_address.copy()
        for address in [ip_address, subnet, gateway, dns]:
            split_address = address.split(".")
            if len(split_address) != 4:
                logger.info(f"set_ethernet_parameters {address} Incorrect format")
                return False
            for i in split_address:
                try:
                    num = int(i)
                    data.append(num)
                except ValueError:
                    logger.info(f"set_ethernet_parameters {address} Incorrect format")
                    return False
                if num < 0 or num > 255:
                    logger.info(f"set_ethernet_parameters {address} Incorrect format")
                    return False
        data.append(dhcp_mode)

        result = await self.set(PacketID.ETH_PARAMETERS, data)

        if result:
            self.eth_parameters_mac_address = mac_address
            self.eth_parameters_ip_address = ip_address
            self.eth_parameters_subnet = subnet
            self.eth_parameters_gateway = gateway
            self.eth_parameters_dns = dns
            self.eth_parameters_dhcp_mode = dhcp_mode
            return True
        else:
            self.eth_parameters_mac_address = None
            self.eth_parameters_ip_address = None
            self.eth_parameters_subnet = None
            self.eth_parameters_gateway = None
            self.eth_parameters_dns = None
            self.eth_parameters_dhcp_mode = None

            return False

    async def set_eth_parameters_ip_subnet_and_gateway(self, ip_address: str, subnet_string: str, gateway_string: str):
        if None in [self.eth_parameters_mac_address,
                    self.eth_parameters_dns,
                    self.eth_parameters_dhcp_mode]:
            if not await self.get_eth_parameters():
                logger.warning("Unable to get ethernet_parameters")
                return

        return await self.set_ethernet_parameters(self.eth_parameters_mac_address,
                                                  ip_address,
                                                  subnet_string,
                                                  gateway_string,
                                                  self.eth_parameters_dns,
                                                  self.eth_parameters_dhcp_mode)

    eth_socket_0_parameters = ListProperty(None, allownone=True)
    eth_socket_1_parameters = ListProperty(None, allownone=True)
    eth_socket_2_parameters = ListProperty(None, allownone=True)
    eth_socket_3_parameters = ListProperty(None, allownone=True)
    eth_socket_4_parameters = ListProperty(None, allownone=True)
    eth_socket_5_parameters = ListProperty(None, allownone=True)
    eth_socket_6_parameters = ListProperty(None, allownone=True)
    eth_socket_7_parameters = ListProperty(None, allownone=True)

    eth_socket_parameters = ReferenceListProperty(eth_socket_0_parameters,
                                                  eth_socket_1_parameters,
                                                  eth_socket_2_parameters,
                                                  eth_socket_3_parameters,
                                                  eth_socket_4_parameters,
                                                  eth_socket_5_parameters,
                                                  eth_socket_6_parameters,
                                                  eth_socket_7_parameters)

    async def get_eth_socket_parameters(self, index: int):

        if not 0 <= index <= 7:
            logger.warning(f"Invalid eth_socket index {index}")
            return False

        result = await self.request(PacketID.ETH_SOCKET_0_PARAMETERS + index)

        if result:
            self.eth_socket_parameters[index] = result
            return True
        else:
            self.eth_socket_parameters[index] = None
            return False

    async def set_eth_socket_parameters_port(self, index: int, port: int):
        if not 0 <= index <= 7:
            logger.warning(f"Invalid eth_socket index {index}")
            return False

        if port > 0xFFFF or port < 0:
            logger.warning(f"Invalid port {port}")
            return False

        if self.eth_socket_parameters[index] is None:
            if not await self.get_eth_parameters():
                return

        port_l = port & 0x00FF

        port_h = port >> 8

        eth_socket_parameters = self.eth_socket_parameters[index].copy()

        eth_socket_parameters[2] = port_h
        eth_socket_parameters[3] = port_l

        result = await self.set(PacketID.ETH_SOCKET_0_PARAMETERS + index, eth_socket_parameters)

        if result:
            self.eth_socket_parameters[index] = eth_socket_parameters
        else:
            self.eth_socket_parameters[index] = None

    def clear_parameters(self):

        self.ip_address = None
        self.udp_port = None

        self.eth_parameters_mac_address = None
        self.eth_parameters_ip_address = None
        self.eth_parameters_subnet = None
        self.eth_parameters_gateway = None
        self.eth_parameters_dns = None
        self.eth_parameters_dhcp_mode = None

        self.eth_socket_parameters = [None]*8

        self.clear_mounting_base()
        super(RB708, self).clear_parameters()


class RBTX2(ObstacleBase, PosPresetDevice, MountingBase):
    def __init__(self, *args, **kwargs):
        self.box_obstacles = [None] * 6
        self.cylinder_obstacles = [None] * 6
        super(RBTX2, self).__init__(*args, **kwargs)



    def clear_parameters(self):
        self.clear_mounting_base()
        self.clear_pos_presets()
        self.clear_obstacle_base()
        super(RBTX2, self).clear_parameters()