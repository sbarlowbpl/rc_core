from __future__ import annotations
import asyncio
from abc import ABCMeta, ABC
from copy import copy, deepcopy
import time
import math
from typing import Dict, List, Tuple, Union, Optional
import numpy
import numpy as np
from scipy.spatial.transform import Rotation as R
import RC_Core.loadsave
from RC_Core.RS1_hardware import PacketID, Mode, DeviceType
from RC_Core.callbackobject import CallbackObject
from RC_Core.commconnection_methods import parsepacket
from RC_Core.devicemanager.masterarm.masterarm_config_lookup_handler import \
    MasterArmDefaultConfigurationLookUpTableHandler
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2

from RC_Core.devicemanager.rs1actuator import RS1_Actuator
from RC_Core.packetid import Packets
from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.position_presets import PosPresets, DEFAULT_POSITION_PRESET_SETTINGS
from RC_Core.devicemanager.end_effectors import EndEffectors
from obstacle_visualisation import ObstacleVisualisationManager, ObstacleEditor
from obstacle_visualisation.collision_detector import CollisionDetector

from .base_products import ProductArm, KinematicsEngine
from .devices import RB703, RB708, RBTX2, Device, ObstacleBase, PosPresetDevice, MountingBase, EndEffector
from .master_arm import MasterArm

MASTER_ARM_ENABLE_FILENAME = "master_enable.json"


class ProductBravoArm(ProductArm, ABC):
    rb_base_708: RB708 = None
    rb_joints: Tuple[RB703, ...] = None

    instances: List[ProductBravoArm] = []

    bootloader_parity = 'NONE'

    def __init__(self, *args, **kwargs):
        super(ProductBravoArm, self).__init__(*args, **kwargs)

        if self not in ProductBravoArm.instances:
            ProductBravoArm.instances.append(self)

    def remove_self(self):
        super(ProductBravoArm, self).remove_self()
        if self in ProductBravoArm.instances:
            ProductBravoArm.instances.remove(self)
    pass

    @property
    def device_list(self) -> Tuple[Device, ...]:
        return self.rb_joints + (self.rb_base_708,)

    @property
    def joints_list(self):
        return self.rb_joints

    def goto_pos_preset(self, index: int):
        if 0 <= index < 4:
            self.control_mode = Mode.POSITION_PRESET
            for device_id in [x.device_id for x in self.joints_list]:
                self.controls[device_id] = (PacketID.POS_PRESET_GO, (index,))

    def capture_pos_preset(self, index: int):
        if 0 <= index < 4:
            for joint in self.joints_list:
                joint.send(PacketID.POS_PRESET_CAPTURE, [index])

    def get_base_device_id(self) -> int:
        return self.rb_base_708.device_id

    def get_active_device_ids(self) -> List[int]:
        return [x.device_id for x in self.rb_joints]

    @property
    def pos_preset_name_device(self) -> PosPresetDevice:
        return self.rb_joints[-1]

    @property
    def mounting_position_device(self) -> MountingBase:
        return self.rb_base_708

    @property
    def endeffector_offset_device(self) -> EndEffector:
        return self.rb_joints[0]


class ProductBravoArmKinematicsEngine(ProductBravoArm, KinematicsEngine):
    rb_base_tx2: RBTX2 = None

    instances: list = []

    def __init__(self, *args, **kwargs):
        super(ProductBravoArmKinematicsEngine, self).__init__(*args, **kwargs)
        if self not in ProductBravoArm.instances:
            ProductBravoArmKinematicsEngine.instances.append(self)

        self.add_heartbeat_callback(self.rb_base_708.device_id, PacketID.RUN_TIME, self.run_time_receive, frequency=1)

        self.obstacle_visualisation_manager = ObstacleVisualisationManager(self.visualisation, self.get_kinematics_base())

        self.obstacle_editor = ObstacleEditor(self.get_kinematics_base(), self.obstacle_visualisation_manager)

        self.collision_detector = CollisionDetector(self, self.visualisation, self.obstacle_visualisation_manager)

        # only Relevant for a selected device
        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.bind(selected_device=self.on_selected_device)

        from tool_visulisation_manager.tool_visualisation_manager import ToolVisualisationManager
        self.tool_visualisation_manager = ToolVisualisationManager(self, self.rb_joints[0])

    def remove_self(self):
        super(ProductBravoArmKinematicsEngine, self).remove_self()
        if self in ProductBravoArmKinematicsEngine.instances:
            ProductBravoArmKinematicsEngine.instances.remove(self)

    def get_kinematics_base(self) -> ObstacleBase:
        return self.rb_base_tx2

    def goto_pos_preset(self, index: int):
        if 0 <= index < 4:
            self.control_mode = Mode.POSITION_PRESET
            self.controls[self.get_kinematics_base_device_id()] = (PacketID.POS_PRESET_GO, (index,))

    def capture_pos_preset(self, index: int):
        if 0 <= index < 4:
            self.rb_base_tx2.send(PacketID.POS_PRESET_CAPTURE, [index])

    def get_base_device_id(self) -> int:
        return self.rb_base_tx2.device_id


    @property
    def device_list(self) -> Tuple[Device, ...]:
        return self.rb_joints + (self.rb_base_708,) + (self.rb_base_tx2,)

    def run_time_receive(self, device_id: int, packet_id: int, data: List):

        if device_id != self.rb_base_708.device_id:
            return
        if packet_id != PacketID.RUN_TIME:
            return

        session_runtime = data[0]

        if self.rb_base_708.session_runtime is None or session_runtime < self.rb_base_708.session_runtime:
            # If a restart has been detected
            self.rb_base_tx2.clear_parameters()
            for j in self.rb_joints:
                j.clear_parameters()

        self.rb_base_708.session_runtime = session_runtime
        if len(data) > 1:
            self.rb_base_708.lifetime_runtime = data[1]
        pass

    @property
    def pos_preset_name_device(self) -> PosPresetDevice:
        return self.rb_base_tx2


class ProductBravo7(ProductBravoArmKinematicsEngine):
    """
    ReachSystem2 7-function class.
    self.device_ids defaults to [1,2,3,4,5,6,7,E]
    """
    instances: list = []
    display_name = 'Bravo 7'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 3, 4, 5, 6, 7, 0xD, 0xE]

        self.rb_joints = (RB703(self.connection, device_ids[0]),
                          RB703(self.connection, device_ids[1]),
                          RB703(self.connection, device_ids[2]),
                          RB703(self.connection, device_ids[3]),
                          RB703(self.connection, device_ids[4]),
                          RB703(self.connection, device_ids[5]),
                          RB703(self.connection, device_ids[6]))

        self.rb_base_708 = RB708(self.connection, device_ids[7])
        self.rb_base_tx2 = RBTX2(self.connection, device_ids[8])

        self.end_effector = EndEffectors.BRAVO_FINGERS

        super(ProductBravo7, self).__init__(device_ids=device_ids, **kwargs)

        self.allowed_km_direct_control_joints = {self.rb_joints[0]}

        ProductBravo7.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()
        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self


    def remove_self(self):
        super(ProductBravo7, self).remove_self()
        if self in ProductBravo7.instances:
            ProductBravo7.instances.remove(self)

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)


class ProductBravo5(ProductBravoArmKinematicsEngine):
    """
    ReachSystem2 5-function class.
    self.device_ids defaults to [1,2,3,4,5]
    """
    instances: list = []
    display_name = 'Bravo 5'

    def __init__(self, device_ids: list = None, **kwargs):

        if device_ids is None:
            device_ids = [1, 2, 3, 4, 5, 0xD, 0xE]

        self.rb_joints = (RB703(self.connection, device_ids[0]),
                          RB703(self.connection, device_ids[1]),
                          RB703(self.connection, device_ids[2]),
                          RB703(self.connection, device_ids[3]),
                          RB703(self.connection, device_ids[4]))

        self.rb_base_708 = RB708(self.connection, device_ids[5])
        self.rb_base_tx2 = RBTX2(self.connection, device_ids[6])

        # print(__name__, "__init__():", "ProductRS2F5 name:", self.name)

        self.end_effector = EndEffectors.BRAVO_FINGERS

        super(ProductBravo5, self).__init__(device_ids=device_ids, **kwargs)

        self.allowed_km_direct_control_joints = {self.rb_joints[0], self.rb_joints[1]}

        ProductBravo5.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()
        # self.set_end_effector(EndEffBravoFingers())
        self.obstacle_visualisation_manager = ObstacleVisualisationManager(self.visualisation, self.get_kinematics_base())

        self.obstacle_editor = ObstacleEditor(self.get_kinematics_base(), self.obstacle_visualisation_manager)

        self.collision_detector = CollisionDetector(self, self.visualisation, self.obstacle_visualisation_manager)

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductBravo5, self).remove_self()
        if self in ProductBravo5.instances:
            ProductBravo5.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)



class ProductBravoSingleRotate(ProductBravoArm):
    """
    ReachSystem2 1-function class.
    self.device_ids defaults to [1,2,3,4,5]
    """
    instances: list = []
    display_name = 'Bravo Single Rotate'

    def __init__(self, device_ids: list = None, **kwargs):

        if device_ids is None:
            device_ids = [1, 0xD]

        self.rb_joints = (RB703(self.connection, device_ids[0]),)

        self.rb_base_708 = RB708(self.connection, device_ids[1])
        self.name = 'Bravo Single Rotate Arm'
        print(__name__, "__init__():", "ProductBravoSingleRotate name:", self.name)
        super(__class__, self).__init__(device_ids=device_ids, **kwargs)

        ProductBravoSingleRotate.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    @property
    def device_list(self) -> Tuple[Device, ...]:
        """ SPECIAL CASE: THIS IS A FACTORY DEVICES TO TEST JOINTS OUTSIDE OF THE ARM.
        REPORT AS IF THERE IS NO 708 CONNECTED"""

        return self.rb_joints

    def remove_self(self):
        super(ProductBravoSingleRotate, self).remove_self()
        if self in ProductBravoSingleRotate.instances:
            ProductBravoSingleRotate.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)


class ProductBravoInlineRotate(ProductBravoArm):
    """
    ReachSystem2 1-function class.
    self.device_ids defaults to [1,2,3,4,5]
    """
    instances: list = []
    display_name = 'Bravo Inline Rotate'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 0xD]

        self.rb_joints = (RB703(self.connection, device_ids[0]),)
        self.rb_base_708 = RB708(self.connection, device_ids[1])

        print(__name__, "__init__():", "ProductBravoInlineRotate name:", self.name)
        super(__class__, self).__init__(device_ids=device_ids, **kwargs)

        ProductBravoInlineRotate.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductBravoInlineRotate, self).remove_self()
        if self in ProductBravoInlineRotate.instances:
            ProductBravoInlineRotate.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)



class ProductBravo2RotatingGrabber(ProductBravoArm):
    """
    ProductRS1F2_Grabber class.
    self.device_ids defaults to [1,2,3,4,5,6]
    """
    instances: list = []
    display_name = 'Bravo 2 Grabber'

    # use_master_arm = False

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 0x0D]

        self.rb_joints = (RB703(self.connection, device_ids[0]),
                          RB703(self.connection, device_ids[1]))
        self.rb_base_708 = RB708(self.connection, device_ids[2])

        print(__name__, "__init__():", __class__.__name__, self.name)
        self.end_effector = EndEffectors.BRAVO_FINGERS

        super(__class__, self).__init__(device_ids=device_ids, **kwargs)

        ProductBravo2RotatingGrabber.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductBravo2RotatingGrabber, self).remove_self()
        if self in ProductBravo2RotatingGrabber.instances:
            ProductBravo2RotatingGrabber.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)


class ProductBravo3(ProductBravoArm):
    """
    ProductRS1F3 class.
    self.device_ids defaults to [1,2,3,4]
    """
    instances: list = []
    display_name = 'Bravo 3'

    def __init__(self, device_ids: list = None, **kwargs):
        if device_ids is None:
            device_ids = [1, 2, 3, 0x0D]

        self.rb_joints = (RB703(self.connection, device_ids[0]),
                          RB703(self.connection, device_ids[1]),
                          RB703(self.connection, device_ids[2]))
        self.rb_base_708 = RB708(self.connection, device_ids[3])

        self.end_effector = EndEffectors.BRAVO_FINGERS

        super(__class__, self).__init__(device_ids=device_ids, **kwargs)

        ProductBravo3.instances.append(self)

        # Add masterarm mappings
        self.master_arm_mapppings = MasterArmDefaultConfigurationLookUpTableHandler()

        from selected_device_manager.selected_device_manager import SelectedDeviceManager
        SelectedDeviceManager.instance.selected_device = self

    def remove_self(self):
        super(ProductBravo3, self).remove_self()
        if self in ProductBravo3.instances:
            ProductBravo3.instances.remove(self)

    #
    #
    #

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False,
                      end_effector=None):
        # Load position presets
        self.name = loaded_name
        if loaded_pos_preset_list is not None and len(loaded_pos_preset_list) > 0:
            for pos_preset_settings in loaded_pos_preset_list:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        else:
            for pos_preset_settings in DEFAULT_POSITION_PRESET_SETTINGS:
                self.pos_preset_list.append(PosPresets(self, pos_preset_settings))
        # Load connection
        if loaded_connection is not None:
            for connection in CommConnection.connections:
                if connection.name == loaded_connection:
                    self.set_connection(connection)
        # Load end effector
        if end_effector:
            self.set_end_effector(end_effector)




