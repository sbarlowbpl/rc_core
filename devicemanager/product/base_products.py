from __future__ import annotations

import asyncio
import json
import math
import time
import traceback
from abc import ABC
from copy import copy
from typing import List, Optional, Tuple, Dict, Union, Set, Any

import numpy as np

from global_settings import GlobalSettings
from mounting_position_manager.mounting_position_manager import MountingPositionManager
from obstacle_visualisation import ObstacleVisualisationManager, ObstacleEditor
from obstacle_visualisation.collision_detector import CollisionDetector
from .devices import Device, ObstacleBase, Joint, MountingBase, PosPresetDevice
from .rs_product import RSProduct, RSControlProduct
from .master_arm import MasterArm
from ...commconnection import CommConnection
from ...enums import Mode, PacketID

from ...packetid import Packets

import rc_logging

from scipy.spatial.transform import Rotation as R

MASTER_ARM_ENABLE_FILENAME = "master_enable.json"

CONTROL_LOOP_FREQUENCY = 20

logger = rc_logging.getLogger(__name__)

KM_CONTROL_MODES = {Mode.KM_END_VEL, Mode.KM_END_POS, Mode.KM_END_VEL_LOCAL, Mode.KM_END_VEL_CAMERA,
                    Mode.KM_END_POS_LOCAL, Mode.KM_END_VEL_WORK}
KM_VELOCITY_MODES = {Mode.KM_END_VEL, Mode.KM_END_VEL_CAMERA, Mode.KM_END_VEL_LOCAL, Mode.KM_END_VEL_WORK}

CONTROL_MODES = {Mode.DIRECT_CONTROL, Mode.KM_END_VEL, Mode.KM_END_POS, Mode.KM_END_VEL_WORK,
                 Mode.KM_END_VEL_LOCAL, Mode.KM_END_VEL_CAMERA,
                 Mode.KM_END_POS_LOCAL, Mode.POSITION_PRESET}

KM_CONTROL_PACKETS = {PacketID.KM_END_VEL,
                      PacketID.KM_END_POS,
                      PacketID.KM_END_VEL_LOCAL,
                      PacketID.KM_END_POS_LOCAL,
                      PacketID.KM_END_VEL_CAMERA,
                      PacketID.KM_END_VEL_WORK}

KM_VELOCITY_PACKETS = {PacketID.KM_END_VEL,
                       PacketID.KM_END_VEL_LOCAL,
                       PacketID.KM_END_VEL_CAMERA,
                       PacketID.KM_END_VEL_WORK}

DIRECT_CONTROL_PACKETS = {PacketID.VELOCITY, PacketID.POSITION, PacketID.OPENLOOP,
                          PacketID.CURRENT, PacketID.POSITION_VELOCITY_DEMAND,
                          PacketID.INDEXED_POSITION, PacketID.RELATIVE_POSITION,
                          PacketID.ABS_POSITION_VELOCITY_DEMAND}


class ProductArm(RSControlProduct):
    instances: List[ProductArm] = []
    _master_arm: Optional[MasterArm] = None
    _end_effector: Optional[int] = None  # Refers to an End effector ENUM, None is no end effector
    visualisation_origin = (0, 0, 0, 0, 0, 0)

    joint_limit_visualisation = None

    visualisation = None

    mounting_position_manager: MountingPositionManager = None

    def __init__(self, *largs, **kwargs):
        super(ProductArm, self).__init__(*largs, **kwargs)
        if self not in ProductArm.instances:
            ProductArm.instances.append(self)

        self.name: str = 'RIGHT ARM'

        with open('arm_configurations.json') as f:
            _arm_configurations = json.load(f)

        # Load arm_configurations
        if self.__class__.__name__ in _arm_configurations:
            self.arm_configuration = _arm_configurations[self.__class__.__name__]
        else:
            logger.warning(f"Arm configuration not found for {self.__class__.__name__}")

        from visualisation.arms.vis_arm import VisArm, VisArmProbe
        from visualisation.control.controller_common import ArmController

        from RC_Core.devicemanager.product import ProductAlpha5Probe
        from visualisation.control.alpha2_dualbend_arm_controller_new import Alpha2DualBendArmControllerNew

        if isinstance(self, ProductAlpha5Probe):
            self.visualisation = VisArmProbe(self, self.arm_configuration['visualisation'])
        else:
            self.visualisation = VisArm(self, self.arm_configuration['visualisation'])

        from RC_Core.devicemanager.product import ProductAlpha2DualBend
        if isinstance(self, ProductAlpha2DualBend):
            self.visualisation.arm_controller = Alpha2DualBendArmControllerNew(self)
        else:
            self.visualisation.arm_controller = ArmController(self)

        if self.end_effector is not None:
            self.visualisation.set_end_effector()

        self.controls_enabled = True

        from visualisation.joint_limits.joint_limit_visualisation import JointLimitVisualisation
        self.joint_limit_visualisation: JointLimitVisualisation = JointLimitVisualisation(self, self.arm_configuration['joint_limit_visualisation'])


        self.mounting_position_manager: MountingPositionManager = MountingPositionManager(self.mounting_position_device, self.visualisation)

        for device_id in self.device_ids_list:
            self.add_heartbeat_callback(device_id, PacketID.MODE, self.mode_receive)
            self.add_heartbeat_callback(device_id, PacketID.POSITION, self.position_receive)

    @property
    def end_effector(self):
        return self._end_effector

    @end_effector.setter
    def end_effector(self, end_effector):
        self._end_effector = end_effector
        if self.visualisation:
            self.visualisation.set_end_effector()

    def remove_self(self):
        super(ProductArm, self).remove_self()
        if self in ProductArm.instances:
            ProductArm.instances.remove(self)

        if self.visualisation:
            self.visualisation.remove_self()
            self.visualisation = None

    @property
    def control_device_ids(self):
        return self.joint_device_ids

    @property
    def device_list(self) -> Tuple[Device, ...]:
        raise NotImplementedError

    @property
    def joints_list(self) -> Tuple[Joint, ...]:
        raise NotImplementedError

    @property
    def joint_device_ids(self) -> Tuple[int, ...]:
        return tuple([x.device_id for x in self.joints_list])

    @property
    def joint_dict(self) -> Dict[int, Joint]:
        joint_dict = {}
        for x in self.joints_list:
            joint_dict[x.device_id] = x
        return joint_dict

    @property
    def deviceid_names(self) -> Dict[str, int]:
        deviceid_names = {}

        for idx, joint in enumerate(self.joints_list):
            name = chr(ord("a") + idx)
            deviceid_names[name] = joint.device_id

        return deviceid_names

    @property
    def DEVICE_ID_TO_JOINT(self) -> Dict[int, str]:
        joint_configs: List[Dict[str, Any]] = self.arm_configuration['visualisation']['joints']

        joint_device_ids = self.joint_device_ids

        DEVICE_ID_TO_JOINT = {}
        if len(joint_configs) != len(joint_device_ids):
            raise ValueError("Length of joint_configs and joint_device_ids dont match")
        for idx, joint_config in enumerate(joint_configs):
            DEVICE_ID_TO_JOINT[joint_device_ids[idx]] = joint_config["name"]

        return DEVICE_ID_TO_JOINT

    @property
    def JOINT_TO_DEVICE_ID(self) -> Dict[str, int]:
        joint_configs: List[Dict[str, Any]] = self.arm_configuration['visualisation']['joints']

        joint_device_ids = self.joint_device_ids

        JOINT_TO_DEVICE_ID = {}
        if len(joint_configs) != len(joint_device_ids):
            raise ValueError("Length of joint_configs and joint_device_ids dont match")
        for idx, joint_config in enumerate(joint_configs):
            JOINT_TO_DEVICE_ID[joint_config["name"]] = joint_device_ids[idx]

        return JOINT_TO_DEVICE_ID

    @property
    def device_dict(self) -> Dict[int, Device]:
        device_dict = {}
        for x in self.device_list:
            device_dict[x.device_id] = x
        return device_dict

    @property
    def device_ids_list(self) -> Tuple[int, ...]:
        return tuple([x.device_id for x in self.device_list])

    def set_end_effector(self, end_effector: int):
        self.end_effector = end_effector

    @property
    def master_arm(self):
        return self._master_arm

    @master_arm.setter
    def master_arm(self, ma):
        self._master_arm = ma

    @RSProduct.connection.setter
    def connection(self, connection: Optional[CommConnection]):
        super(ProductArm, type(self)).connection.fset(self, connection)

        for device in self.device_list:
            device.connection = connection

        if self._master_arm:
            self._master_arm.refresh_slave_connection()


    def goto_pos_preset(self, index: int):
        # Send to pos_preset devices
        raise NotImplementedError

    def capture_pos_preset(self, index: int):
        raise NotImplementedError


    def check_device_types(self):
        asyncio.create_task(self.async_check_device_types())
        return self.device_types

    async def async_check_device_types(self):
        for device in self.device_list:
            await device.get_device_type()

    @property
    def device_types(self) -> List[Optional[int]]:
        return [x.device_type for x in self.device_list]

    def mode_receive(self, device_id, packet_id, data):
        if packet_id != PacketID.MODE:
            return
        if device_id not in self.device_dict:
            return
        self.device_dict[device_id].mode = data[0]

    def position_receive(self, device_id, packet_id, data):
        if packet_id != PacketID.POSITION:
            return
        if device_id not in self.joint_dict:
            return

        self.joint_dict[device_id].position = data[0]

    def set_device_ids(self, device_ids: List[int]):

        self.device_ids = device_ids
        self.init_devices()


        #
        #
        #

        if len(device_ids) < len(self.device_ids_list):
            raise ValueError("List length does not match the amount of device_ids")

        device_ids = device_ids[:len(self.device_ids_list)]

        new_device_id_map: Dict[int, int] = {}

        # Create a map from the old device ids to the new device ids

        # Also change the device device ids.
        for new_device_id, device in zip(device_ids, self.device_list):
            new_device_id_map[device.device_id] = new_device_id

            device.device_id = new_device_id
            device.clear_parameters()

        # Change all permament callbacks to new callbacks with device_ids

        for key, callbacks in copy(self.permanent_callbacks).items():
            device_id = key[0]
            packet_id = key[1]

            for callback in copy(callbacks):
                if device_id in new_device_id_map:
                    new_device_id = new_device_id_map[device_id]
                    self.remove_permanent_callback(device_id, packet_id, callback)
                    self.set_permanent_callback(new_device_id, packet_id, callback)

        for key, callbacks_dict in copy(self.heartbeat_callbacks).items():
            device_id = key[0]
            packet_id = key[1]

            for callback, frequency in copy(callbacks_dict).items():
                if device_id in new_device_id_map:
                    new_device_id = new_device_id_map[device_id]
                    self.remove_heartbeat_callback(device_id, packet_id, callback)
                    self.add_heartbeat_callback(new_device_id, packet_id, callback, frequency)

    @property
    def mounting_position_device(self) -> MountingBase:
        raise NotImplementedError

    @property
    def endeffector_offset_device(self) -> MountingBase:
        raise NotImplementedError

    @property
    def pos_preset_name_device(self) -> PosPresetDevice:
        raise NotImplementedError

    def set_origin(self, origin: list = None):
        """
        Set origin to product and to the arm device. *Needs a verification part of the code.
        :param origin:
        :return:
        """

        return
        if self.visualisation is None:
            return

        if origin is not None:
            self.visualisation_origin: list = origin

        from .product_alpha import ProductAlpha3
        if isinstance(self, ProductAlpha3):
            self.orientation_mode = "cw_positive"
            origin = self.visualisation_origin

            xyz_z = [0., 0., 10.]
            xyz_y = [0., 10., 0.]

            rpy = origin[3:6]
            # r = R.from_euler('xyz', rpy, degrees=True)
            # rpy = r.as_euler('zyx', degrees=True)

            a = math.radians(rpy[0])
            b = math.radians(rpy[1])
            c = math.radians(rpy[2])
            rx = np.array([[1, 0, 0],
                           [0, math.cos(a), -math.sin(a)],
                           [0, math.sin(a), math.cos(a)]])
            ry = np.array([[math.cos(b), 0, math.sin(b)],
                           [0, 1, 0],
                           [-math.sin(b), 0, math.cos(b)]])

            rz = np.array([[math.cos(c), -math.sin(c), 0],
                           [math.sin(c), math.cos(c), 0],
                           [0, 0, 1]])
            xyz_z = rx.dot(xyz_z)
            xyz_z = ry.dot(xyz_z)
            xyz_z = rz.dot(xyz_z)

            xyz_y = rx.dot(xyz_y)
            xyz_y = ry.dot(xyz_y)
            xyz_y = rz.dot(xyz_y)
            z_value = tuple(xyz_z)[2]
            y_value = tuple(xyz_y)[2]

            if abs(z_value) > abs(y_value):
                # USE CW
                if z_value > 0:
                    self.orientation_mode = "cw_positive"
                else:
                    self.orientation_mode = "cw_negative"
            else:
                # USE PITCH
                if y_value > 0:
                    self.orientation_mode = "pitch_positive"
                else:
                    self.orientation_mode = "pitch_negative"
            print(__file__, "Mode set to", self.orientation_mode, y_value, z_value)

        self.send_visualisation_origin_to_product()

    def send_visualisation_origin_to_product(self):
        """
        Sends visualisation position data to the manipulator
        :return:
        """

        return

        if self.connection and self.connection.connected and \
                time.time() < (self.devices[self.get_base_device_id()].time_last_received + 0.5):
            msg = [0.0] * 6
            msg[0] = self.visualisation_origin[0] * 1000.0
            msg[1] = self.visualisation_origin[1] * 1000.0
            msg[2] = self.visualisation_origin[2] * 1000.0
            r = R.from_euler('ZYX', self.visualisation_origin[3:6], degrees=True)
            Rvec = r.as_rotvec()
            msg[3] = Rvec[0]
            msg[4] = Rvec[1]
            msg[5] = Rvec[2]
            self.visualisation.origin_to_send = [round(msg[0]), round(msg[1]), round(msg[2]), round(msg[3], 2),
                                                 round(msg[4], 2), round(msg[5], 2)]
            Packets.KM_MOUNT_POS_ROT.send(self.connection, self.get_base_device_id(), msg)

    def get_origin(self) -> list:
        """
        Get origin from device
        :return:
        """
        return self.visualisation_origin

    def get_origin_from_arm(self, callback):
        """
        Read origin from arm device if there is a connection.
        :return:
        """
        pass

    def has_inverse_kinematics_feature(self) -> bool:
        if isinstance(self, KinematicsEngine):
            return True
        else:
            return False

    def get_base_device_id(self) -> int:
        raise NotImplementedError



class KinematicsEngine(ProductArm, ABC):
    # Manages the visualisation of obstacles, i.e for collision detection or for obstacle editing.
    obstacle_visualisation_manager: ObstacleVisualisationManager = None

    # Manages the editing of obstacles
    obstacle_editor: ObstacleEditor = None

    # Manages the requesting of KM_COLLISION_FLAGS from the arm displays obstacles is relevant.
    collision_detector: CollisionDetector = None

    # Allowed km_direct_control joints, e.g. jaws and wrist.
    allowed_km_direct_control_joints: Set[Joint] = None

    def get_kinematics_base(self) -> ObstacleBase:
        raise NotImplementedError

    def get_kinematics_base_device_id(self) -> int:
        return self.get_kinematics_base().device_id

    def on_selected_device(self, _, selected_device):
        """
        Toggle collision visualisation and obstacle visualisation when this device is selected / unselected.
        """

        if self is selected_device:
            if GlobalSettings.instance.obstacles:
                self.obstacle_visualisation_manager.set_global_mode("wireframe")
            if GlobalSettings.instance.obstacle_collisions:
                self.collision_detector.enabled = True
            pass

        else:
            self.collision_detector.enabled = False
            self.obstacle_visualisation_manager.set_global_mode("off")

    @ProductArm.control_mode.setter
    def control_mode(self, control_mode: int):

        if control_mode not in [Mode.DIRECT_CONTROL,
                                Mode.KM_END_POS, Mode.KM_END_VEL,
                                Mode.KM_END_VEL_CAMERA, Mode.KM_END_VEL_LOCAL, Mode.KM_END_VEL_WORK,
                                Mode.KM_END_POS_LOCAL, Mode.POSITION_PRESET]:
            # Unacceptable mode
            raise ValueError(f"Unexpected mode {control_mode}")

        if self._control_mode == control_mode:
            return

        if self._control_mode == Mode.DIRECT_CONTROL:
            for device_id, control in self.controls.items():
                packet_id, data = control
                if packet_id == PacketID.VELOCITY and \
                        device_id in [x.device_id for x in self.joints_list]:
                    # If it is a  control and a joint
                    self.connection.send_control(device_id, packet_id, [0.0])

        # Kinematics
        elif isinstance(self, KinematicsEngine) and self._control_mode in KM_VELOCITY_MODES:
            for device_id, control in self.controls.items():
                packet_id, data = control

                if device_id == self.get_kinematics_base_device_id() and \
                        packet_id in {PacketID.KM_END_VEL,
                                      PacketID.KM_END_VEL_LOCAL, PacketID.KM_END_VEL_CAMERA, PacketID.KM_END_VEL_WORK}:
                    self.connection.send_control(device_id, packet_id, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        #  If going from Direct to KM, then copy over allowed km_direct_controls or vice versa
        if (self._control_mode == Mode.DIRECT_CONTROL and control_mode in KM_CONTROL_MODES) or \
                (self._control_mode in KM_CONTROL_MODES and control_mode == Mode.DIRECT_CONTROL):
            controls = {}
            for device_id, control in self.controls.items():
                if device_id in [x.device_id for x in self.allowed_km_direct_control_joints]:
                    controls[device_id] = control
            self.controls = controls
        else:
            self.controls = {}

        self._control_mode = control_mode

    async def control_loop(self):
        """
        The control loop, is run at a regular frequency, it latches the last sent commands.
        It can switch between direct_controls, kinematic_controls, position_preset controls.
        """

        from RC_Core.connectionmanager import ConnectionManager

        while self.controls_enabled:
            if ConnectionManager.instance.run_connection_loop:
                if self.control_mode == Mode.DIRECT_CONTROL:
                    # Send Controls
                    for device_id, control in self.controls.items():
                        packet_id, data = control
                        # If it is a  control and a joint
                        if packet_id in DIRECT_CONTROL_PACKETS and \
                                device_id in [x.device_id for x in self.joints_list]:
                            self.connection.send_control(device_id, packet_id, list(data))

                            # Case for relative position, reset the control to zero, after is has been sent
                            if packet_id == PacketID.RELATIVE_POSITION:
                                self.controls[device_id] = (PacketID.RELATIVE_POSITION, (0.0,))

                # KM_CONTROL_MODE
                elif isinstance(self, KinematicsEngine) and self.control_mode in KM_CONTROL_MODES:
                    for device_id, control in self.controls.items():
                        packet_id, data = control

                        if device_id == self.get_kinematics_base_device_id() and \
                                packet_id in KM_CONTROL_PACKETS:
                            print(f"Sending {device_id, packet_id, data}")
                            self.connection.send_control(device_id, packet_id, list(data))

                        # Send direct_control_packet if allowed
                        elif device_id in [x.device_id for x in self.allowed_km_direct_control_joints]:
                            if packet_id in DIRECT_CONTROL_PACKETS:
                                self.connection.send_control(device_id, packet_id, list(data))

                # POSITION_PRESET_MODE
                elif self.control_mode == Mode.POSITION_PRESET:
                    for device_id, control in self.controls.items():
                        packet_id, data = control
                        if packet_id == PacketID.POS_PRESET_GO:
                            self.connection.send_control(device_id, packet_id, list(data))

            # Sleep
            await asyncio.sleep(1 / CONTROL_LOOP_FREQUENCY)

        #
        #
        # Upon exit, send any velocity controls to zero.
        if ConnectionManager.instance.run_connection_loop:
            if self._control_mode == Mode.DIRECT_CONTROL:
                for device_id, control in self.controls.items():
                    packet_id, data = control
                    if packet_id == PacketID.VELOCITY and \
                            device_id in [x.device_id for x in self.joints_list]:
                        # If it is a  control and a joint
                        self.connection.send_control(device_id, packet_id, [0.0])
            elif isinstance(self, KinematicsEngine) and self._control_mode in KM_VELOCITY_MODES:
                for device_id, control in self.controls.items():
                    packet_id, data = control

                    if device_id == self.get_kinematics_base_device_id() and \
                            packet_id in KM_VELOCITY_PACKETS:
                        self.connection.send_control(device_id, packet_id, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    def set_control_value(self, device_id: int, packet_id: int, control_value: Union[float, List[float]]):

        if packet_id in DIRECT_CONTROL_PACKETS:
            if self.control_mode in KM_CONTROL_MODES \
                    and device_id in [x.device_id for x in self.allowed_km_direct_control_joints]:
                pass
            else:
                self.control_mode = Mode.DIRECT_CONTROL

            if not isinstance(control_value, list):
                control_value = [control_value]

            self.controls[device_id] = (packet_id, tuple(control_value))

    def set_kinematics_control_value(self, device_id: int, packet_id: int, data: List[float]):
        if device_id == self.get_kinematics_base_device_id():
            if packet_id == PacketID.KM_END_POS:
                self.control_mode = Mode.KM_END_POS
            elif packet_id == PacketID.KM_END_VEL:
                self.control_mode = Mode.KM_END_VEL
            elif packet_id == PacketID.KM_END_VEL_LOCAL:
                self.control_mode = Mode.KM_END_VEL_LOCAL
            elif packet_id == PacketID.KM_END_VEL_CAMERA:
                self.control_mode = Mode.KM_END_VEL_CAMERA
            elif packet_id == PacketID.KM_END_VEL_WORK:
                self.control_mode = Mode.KM_END_VEL_WORK

            self.controls[device_id] = (packet_id, tuple(data))

    def set_controls_off(self):
        if self._control_mode == Mode.DIRECT_CONTROL:
            for device_id, control in self.controls.items():
                packet_id, data = control
                if packet_id == PacketID.VELOCITY and \
                        device_id in [x.device_id for x in self.joints_list]:
                    # If it is a  control and a joint
                    self.connection.send_control(device_id, packet_id, [0.0])

            # Kinematics
        elif isinstance(self, KinematicsEngine) and self._control_mode in KM_VELOCITY_MODES:
            for device_id, control in self.controls.items():
                packet_id, data = control

                if device_id == self.get_kinematics_base_device_id() and \
                        packet_id in {PacketID.KM_END_VEL,
                                      PacketID.KM_END_VEL_LOCAL,
                                      PacketID.KM_END_VEL_CAMERA,
                                      PacketID.KM_END_VEL_WORK}:
                    self.connection.send_control(device_id, packet_id, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.controls = {}
