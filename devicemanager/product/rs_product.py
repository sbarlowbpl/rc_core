from __future__ import annotations

import asyncio
import itertools
from copy import deepcopy, copy
from typing import Dict, List, Union, Callable, Tuple, Set, Optional

import rc_logging
from RC_Core.commconnection import CommConnection
from RC_Core.enums import Mode
from RC_Core.enums.packets import PacketID
from RC_Core.devicemanager.rs1actuator import RS1_Actuator
logger = rc_logging.getLogger(__name__)

CONTROL_LOOP_FREQUENCY = 20

KM_CONTROL_MODES = {Mode.KM_END_VEL, Mode.KM_END_POS, Mode.KM_END_VEL_LOCAL, Mode.KM_END_VEL_CAMERA,
                    Mode.KM_END_POS_LOCAL}
KM_VELOCITY_MODES = {Mode.KM_END_VEL, Mode.KM_END_VEL_CAMERA, Mode.KM_END_VEL_LOCAL}

CONTROL_MODES = {Mode.DIRECT_CONTROL, Mode.KM_END_VEL, Mode.KM_END_POS,
                 Mode.KM_END_VEL_LOCAL, Mode.KM_END_VEL_CAMERA,
                 Mode.KM_END_POS_LOCAL, Mode.POSITION_PRESET}

KM_CONTROL_PACKETS = {PacketID.KM_END_VEL,
                      PacketID.KM_END_POS,
                      PacketID.KM_END_VEL_LOCAL,
                      PacketID.KM_END_POS_LOCAL,
                      PacketID.KM_END_VEL_CAMERA}

KM_VELOCITY_PACKETS = {PacketID.KM_END_VEL,
                       PacketID.KM_END_VEL_LOCAL,
                       PacketID.KM_END_VEL_CAMERA}

DIRECT_CONTROL_PACKETS = {PacketID.VELOCITY, PacketID.POSITION, PacketID.OPENLOOP,
                          PacketID.CURRENT, PacketID.POSITION_VELOCITY_DEMAND,
                          PacketID.INDEXED_POSITION, PacketID.RELATIVE_POSITION,
                          PacketID.ABS_POSITION_VELOCITY_DEMAND}


class RSProduct:
    """
    Base class for groupings of devices that form one product.
    """
    _new_id = itertools.count()

    instances: List[RSProduct] = []  # Class variable to hold all instances of RSProduct
    _connection: CommConnection = None
    uid: int = 0

    name: str = ''

    permanent_callbacks: \
        Dict[Tuple[int, int], Set[Callable[[int, int, Union[List[float], List[int]]], None]]] = None
    heartbeat_callbacks: \
        Dict[Tuple[int, int], Dict[Callable[[int, int, Union[List[float], List[int]]], None], float]] = None

    #
    #
    #
    device_class = RS1_Actuator
    device_id_send_index: int = 0
    bootloader_parity: str = 'NONE'
    display_name: str = 'RSProduct'
    inverted: bool = False
    serial_number: int = None
    last_connected: float = 0  # Time when last received a packet

    _device_types = []

    def __init__(self, device_ids: Optional[List[int]] = None):
        if self not in RSProduct.instances:
            RSProduct.instances.append(self)

        self.uid = next(RSProduct._new_id)

        # Callback for all packets and all device_ids
        self.permanent_callbacks = {(0xFF, 0xFFF): {self._packet_callback}}
        self.heartbeat_callbacks = {}

        self._connection = CommConnection.connections[0]

        if device_ids is None:
            device_ids = []

        # self.device_types: list = []
        self.control_modes: list = []
        self.control_values: Dict[int, list] = {}
        self.default_heartbeat_packets: list = []
        self.heartbeat_packets: list = []
        self.pos_preset_list: list = []
        self.device_ids: list = []
        self.devices: Dict[int, RS1_Actuator] = {}
        self.km_velocities: list = [0, 0, 0, 0, 0, 0]
        self.visualisation = None
        self.visualisation_origin: list = [0, 0, 0, 0, 0, 0]
        for i in device_ids:
            self.device_ids.append(i)
        self.init_devices()
        self.heartbeat_packets = []

    def init_devices(self):
        self.devices: Dict[RS1_Actuator] = {}
        for device_id in self.device_ids:
            self.devices[device_id] = self.device_class()
            self.devices[device_id].device_id = device_id
            self.devices[device_id].time_last_received = 0.0
            self.devices[device_id].time_last_heartbeat = 0.0

    def set_connection(self, connection_obj: CommConnection):
        self.connection = connection_obj

    @property
    def device_ids_list(self):
        return self.device_ids

    @property
    def connection(self) -> CommConnection:
        return self._connection

    @connection.setter
    def connection(self, connection: CommConnection):
        if self._connection == connection:
            logger.warning("Connection is the same")
            return

        if self._connection:
            for key, callbacks in self.permanent_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callbacks:
                    self._connection.remove_packet_callback(device_id, packet_id, callback)

        if self._connection:
            for key, callbacks in self.heartbeat_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callbacks.keys():
                    self._connection.remove_heartbeat_callback(device_id, packet_id, callback)

        self._connection = connection

        if self._connection:
            for key, callbacks in self.permanent_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback in callbacks:
                    self._connection.add_packet_callback(device_id, packet_id, callback)

        if self._connection:
            for key, callbacks in self.heartbeat_callbacks.items():
                device_id = key[0]
                packet_id = key[1]
                for callback, freq in callbacks.items():
                    self._connection.add_heartbeat_callback(device_id, packet_id, callback, freq)

    @property
    def device_types(self) -> List[int]:
        return self._device_types

    @device_types.setter
    def device_types(self, dev_types):
        self._device_types = dev_types

    def add_packet_to_heartbeat(self, device_id: int, packet_id: int):
        """

        :param int device_id:
        :param int packet_id:
        :return:
        """

        self.add_heartbeat_callback(device_id, packet_id, self._empty_heartbeat_callback)

    def _empty_heartbeat_callback(self, device_id, packet_id, data):
        pass

    def remove_packet_from_heartbeat(self, device_id: int, packet_id: int):
        self.remove_heartbeat_callback(device_id, packet_id, self._empty_heartbeat_callback)

    def remove_self(self):
        if self in RSProduct.instances:
            RSProduct.instances.remove(self)

        for key, callbacks in copy(self.heartbeat_callbacks).items():

            for callback in copy(callbacks).keys():
                self.remove_heartbeat_callback(key[0], key[1], callback)

    def add_heartbeat_callback(self, device_id: int, packet_id: int,
                               callback: Callable[[int, int, Union[List[float], List[int]]], None], frequency=10.0):

        from RC_Core.devicemanager.product import MasterArm
        if isinstance(self, MasterArm):
            # NOTE CANNOT SET HEARTBEATS WITH MASTER ARM, ONLY CALLBACKS
            self.set_permanent_callback(device_id, packet_id, callback)
            return
        key = (device_id, packet_id)
        if key in self.heartbeat_callbacks:
            self.heartbeat_callbacks[key][callback] = frequency
        else:
            self.heartbeat_callbacks[key] = {callback: frequency}

        if self.connection:
            self.connection.add_heartbeat_callback(device_id, packet_id, callback, frequency)

    def remove_heartbeat_callback(self, device_id: int, packet_id: int,
                               callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        from RC_Core.devicemanager.product import MasterArm
        if isinstance(self, MasterArm):
            self.remove_permanent_callback(device_id, packet_id, callback)
            return

        key = (device_id, packet_id)
        if key in self.heartbeat_callbacks:
            if callback in self.heartbeat_callbacks[key]:
                self.heartbeat_callbacks[key].pop(callback)

        if self.connection:
            self.connection.remove_heartbeat_callback(device_id, packet_id, callback)

    def set_permanent_callback(self, device_id: int, packet_id: int,
                               callback: Callable[[int, int, Union[List[float], List[int]]], None]):
        '''
        Add a permanent callback specific to this product
        :param device_id:
        :param packet_id:
        :param callback: function the callback should trigger (if any)
        :return:
        '''
        key = (device_id, packet_id)
        if key in self.permanent_callbacks:
            self.permanent_callbacks[key].add(callback)
        else:
            self.permanent_callbacks[key] = {callback}

        if self.connection:

            self.connection.add_packet_callback(device_id, packet_id, callback)

        # previously_set: bool = False
        # for callback_object in self.permanent_callbacks:
        #     if callback_object.device_id == device_id and callback_object.packet_id == packet_id and \
        #             callback_object.callback == callback:
        #         previously_set = True
        #         break
        # if not previously_set:
        #     self.permanent_callbacks.append(CallbackObject(self.connection, device_id, packet_id, callback))

    def remove_permanent_callback(self, device_id: int, packet_id: int, callback):
        '''
        Remove a permanent callback specific to this product
        :param device_id:
        :param packet_id:
        :param callback: function associated with the callback
        :return:
        '''

        # for callback_object in self.permanent_callbacks:
        #     if callback_object.device_id == device_id and callback_object.packet_id == packet_id and \
        #             callback_object.callback == callback:
        #         self.permanent_callbacks.remove(callback_object)

        key = (device_id, packet_id)
        if key in self.permanent_callbacks:
            if callback in self.permanent_callbacks[key]:
                self.permanent_callbacks[key].remove(callback)
                if len(self.permanent_callbacks[key]) == 0:
                    del self.permanent_callbacks[key]

        if self.connection:
            self.connection.remove_packet_callback(device_id, packet_id, callback)

    def _packet_callback(self, device_id, packet_id, data):
        from RC_Core.datalogger import DataLogging
        # logger.info("received data_logger callback")
        if DataLogging.logging_now:
            for datalogger in DataLogging.instances:
                if datalogger.enable_logger:
                    if self in datalogger.products:
                        if datalogger.log_all_packets:
                            datalogger.packet_callback_with_product(device_id, packet_id, data,
                                                                    self)
                        elif packet_id in datalogger.packet_ids:
                            if datalogger.packet_ids[packet_id]:
                                datalogger.packet_callback_with_product(device_id, packet_id, data,
                                                                        self)

    #
    #
    #

    @staticmethod
    def set_all_controls_off():
        """
        Set all products to stop sending control values.
        :return:
        """
        for product in RSProduct.instances:
            product.set_controls_off()
            # for i, value in enumerate(product.control_modes):
            #     product.control_modes[i] = 0

    def goto_pos_preset(self, idx: int):
        """
        *** To be overwritten in each product ***
        Go to device's pos preset (idx)
        :return:
        """
        pass

    def set_inverted(self, inverted: bool = False):
        self.inverted = inverted

    def set_visualisation_origin(self, origin: list =None):
        return
        if origin is None:
            origin = [0, 0, 0, 0, 0, 0]
        self.visualisation_origin = origin

    def serial_number_callback(self, device_id: int, packet_id: int, data: tuple):
        serial_number = int(data[0])
        if device_id == self.get_base_device_id():
            self.serial_number = serial_number

    def get_active_device_ids(self) -> list:
        """
        :returns list of device_ids that operate on the product
        """
        return self.device_ids

    def get_base_device_id(self) -> int:
        """
        :returns gets the base id (eg. TX2 ID) for the product
        """
        return self.get_active_device_ids()[-1]

    def check_device_types(self):
        """
        *** To be overwritten in each product ***
        Send requests for device type of each axis
        :return:
        """
        return self.device_types

    @staticmethod
    def get_product_with_name(name):
        for prod in RSProduct.instances:
            if name == prod.name:
                return prod

    def set_device_ids(self, device_ids: List[int]):
        '''
        Set device ids
        :param device_ids:
        :return:
        '''
        pass

    def set_control_value(self, device_id: int, packet_id: int, control_value: Union[float, List[float]]):
        """
        Set control value in RSProduct instance. Uses device_id to direct value to correct product.
        :param int packet_id:
        :param int device_id:
        :param float control_value:
        :return:
        """

    def set_velocity(self, device_id: int, velocity: float):
        """
        Set velocity value in RSProduct instance. Uses device_id to direct velocity to correct product.
        :param int device_id:
        :param float velocity:
        :return:
        """
        self.set_control_value(device_id, PacketID.VELOCITY, velocity)

    def set_current(self, device_id: int, current: float):
        """
        Set current value in RSProduct instance. Uses device_id to direct current to correct product.
        :param int device_id:
        :param float current:
        :return:
        """
        self.set_control_value(device_id, PacketID.CURRENT, current)

    def set_position(self, device_id: int, position: float):
        """
        Set position value in RSProduct instance. Uses device_id to direct position to correct product.
        :param int device_id:
        :param float position:
        :return:
        """
        self.set_control_value(device_id, PacketID.POSITION, position)

    def set_openloop(self, device_id: int, openloop: float):
        """
        Set openloop value in RSProduct instance. Uses device_id to direct openloop to correct product.
        :param int device_id:
        :param float openloop:
        :return:
        """
        self.set_control_value(device_id, PacketID.OPENLOOP, openloop)

    def set_indexed_position(self, device_id: int, indexed_positon: float):
        """
        Set position value in RSProduct instance. Uses device_id to direct position to correct product.
        :param int device_id:
        :param float position:
        :return:
        """
        self.set_control_value(device_id, PacketID.INDEXED_POSITION, indexed_positon)

    def set_position_velocity(self, device_id: int, position_velocity: List[float]):
        """
        Set position_velocity values in RSProduct instance. Uses device_id to direct position to correct product.
        :param position_velocity:
        :param int device_id:
        :param Tuple[float, float]) position_velocity:
        :return:
        """
        self.set_control_value(device_id, PacketID.POSITION_VELOCITY_DEMAND, position_velocity)

    def set_kinematics_control_value(self, device_id: int, packet_id: int, list_of_floats: List[float]):
        """
        Set specifiic kinematics control values
        :param int device_id:
        :param int packet_id:
        :param list list_of_floats:
        :return:
        """


    def set_km_end_pos(self, device_id: int, list_of_floats: List[float]):
        """
        Set kinematics global position control values
        :param device_id:
        :param list_of_floats:
        :return:
        """
        self.set_kinematics_control_value(device_id, PacketID.KM_END_POS, list_of_floats)

    def set_km_end_vel(self, device_id: int, list_of_floats: List[float]):
        """
        Set kinematics global velocity control valuess
        :param device_id:
        :param list_of_floats:
        :return:
        """
        self.set_kinematics_control_value(device_id, PacketID.KM_END_VEL, list_of_floats)

    def set_km_end_vel_work(self, device_id: int, list_of_floats: List[float]):
        """
        Set kinematics work frame velocity control valuess
        :param device_id:
        :param list_of_floats:
        :return:
        """
        self.set_kinematics_control_value(device_id, PacketID.KM_END_VEL_WORK, list_of_floats)

    def set_km_end_pos_local(self, device_id: int, list_of_floats: List[float]):
        """
        Set kinematics local position control values
        :param device_id:
        :param list_of_floats:
        :return:
        """
        self.set_kinematics_control_value(device_id, PacketID.KM_END_POS_LOCAL, list_of_floats)

    def set_km_end_vel_local(self, device_id: int, list_of_floats: List[float]):
        """
        Set kinematics local velocity control values
        :param device_id:
        :param list_of_floats:
        :return:
        """
        self.set_kinematics_control_value(device_id, PacketID.KM_END_VEL_LOCAL, list_of_floats)

    def set_km_end_vel_camera(self, device_id: int, list_of_floats: List[float]):
        """
        Set kinematics local velocity control values
        :param device_id:
        :param list_of_floats:
        :return:
        """
        self.set_kinematics_control_value(device_id, PacketID.KM_END_VEL_CAMERA, list_of_floats)

    def get_device_id_from_letter(self, letter: str):
        if len(letter) is not 1:
            return 0

        id_index: int = ord(letter) - ord('a')
        if id_index < len(self.device_ids):
            device_id = self.device_ids[id_index]
            return device_id
        else:
            return 0

    def set_new_heartbeat_array(self, new_heartbeat_array: list):
        """
                To be overwritten in subclasses
                :return:
                """
        for i in range(len(self.heartbeat_packets)):
            self.heartbeat_packets[i] = new_heartbeat_array

    def reset_heartbeat_array(self):
        '''
        Reset to default heartbeat packets
        :return:
        '''
        self.heartbeat_packets = deepcopy(self.default_heartbeat_packets)

    def get_index_from_device_id(self, device_id_index: int):
        """

        :param device_id_index:
        :return: index position of the supplied device id or None if not in self.device_ids
        """
        for i in range(len(self.device_ids)):
            if self.device_ids[i] == device_id_index:
                return i
        return None

    def set_controls_off(self):
        pass


class RSControlProduct(RSProduct):
    instances: List[RSControlProduct] = []

    # Modes: Direct_control, Pos_preset, Kinematics
    _control_mode: int = 0

    # Controls for the control_loop
    controls: Dict[int, Tuple[int, Tuple[float, ...]]] = {}

    control_loop_task: asyncio.Task = None

    def __init__(self, *largs, **kwargs):
        super(RSControlProduct, self).__init__(*largs, **kwargs)
        if self not in RSControlProduct.instances:
            RSControlProduct.instances.append(self)

        self.controls_enabled = True

    def remove_self(self):
        super(RSControlProduct, self).remove_self()
        if self in RSControlProduct.instances:
            RSControlProduct.instances.remove(self)

        self.controls_enabled = False

    @property
    def control_device_ids(self):
        return self.device_ids

    @property
    def control_mode(self):
        return self._control_mode

    @control_mode.setter
    def control_mode(self, control_mode: int):

        if control_mode not in [Mode.DIRECT_CONTROL,
                                Mode.KM_END_POS, Mode.KM_END_VEL,
                                Mode.KM_END_VEL_CAMERA, Mode.KM_END_VEL_LOCAL,
                                Mode.KM_END_POS_LOCAL, Mode.POSITION_PRESET]:
            # Unacceptable mode
            return

        if self._control_mode == control_mode:
            return

        # Reset controls

        # Send zero controls on velocity control mode change)

        # if mode is direct_control:
        # Go through velocity packets and send 0
        # If end effector packet is velocity
        # Go through packet and send zero.

        if self._control_mode == Mode.DIRECT_CONTROL:
            for device_id, control in self.controls.items():
                packet_id, data = control
                if packet_id == PacketID.VELOCITY and \
                        device_id in [x.device_id for x in self.control_device_ids]:
                    # If it is a  control and a joint
                    self.connection.send(device_id, packet_id, [0.0])

        self.controls = {}

        self._control_mode = control_mode

    @property
    def controls_enabled(self):
        return self._controls_enabled

    @controls_enabled.setter
    def controls_enabled(self, state):
        # Send zero controls
        if state:
            if self.control_loop_task is not None and not self.control_loop_task.done():
                self.control_loop_task.cancel()
            self.control_loop_task = asyncio.create_task(self.control_loop())
        self._controls_enabled = state

    async def control_loop(self):
        """
        The control loop, is run at a regular frequency, it latches the last sent commands.
        It can switch between direct_controls, kinematic_controls, position_preset controls.
        """
        from RC_Core.connectionmanager import ConnectionManager
        while self.controls_enabled:

            if ConnectionManager.instance.run_connection_loop:
                if self.control_mode == Mode.DIRECT_CONTROL:
                    # Send Controls
                    for device_id, control in self.controls.items():
                        packet_id, data = control
                        # If it is a  control and a joint
                        if packet_id in DIRECT_CONTROL_PACKETS and \
                                device_id in self.control_device_ids:
                            self.connection.send_control(device_id, packet_id, list(data))
                            # Case for relative position, reset the control to zero, after is has been send
                            if packet_id == PacketID.RELATIVE_POSITION:
                                self.controls[device_id] = (PacketID.RELATIVE_POSITION, (0.0,))

                # POSITION_PRESET_MODE
                elif self.control_mode == Mode.POSITION_PRESET:
                    for device_id, control in self.controls.items():
                        packet_id, data = control
                        if packet_id == PacketID.POS_PRESET_GO:
                            self.connection.send_control(device_id, packet_id, list(data))

            # Sleep
            await asyncio.sleep(1 / CONTROL_LOOP_FREQUENCY)

            #
            #
            # Upon exit, send any velocity controls to zero.

        if ConnectionManager.instance.run_connection_loop:
            from RC_Core.devicemanager.product.base_products import KinematicsEngine
            if self._control_mode == Mode.DIRECT_CONTROL:
                for device_id, control in self.controls.items():
                    packet_id, data = control
                    if packet_id == PacketID.VELOCITY and \
                            device_id in self.control_device_ids:
                        # If it is a  control and a joint
                        self.connection.send_control(device_id, packet_id, [0.0])
            elif isinstance(self, KinematicsEngine) and self._control_mode in KM_VELOCITY_MODES:
                for device_id, control in self.controls.items():
                    packet_id, data = control

                    if device_id == self.get_kinematics_base_device_id() and \
                            packet_id in KM_VELOCITY_PACKETS:
                        self.connection.send_control(device_id, packet_id, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    def set_control_value(self, device_id: int, packet_id: int, control_value: Union[float, List[float]]):

        if packet_id in DIRECT_CONTROL_PACKETS:
            self.control_mode = Mode.DIRECT_CONTROL

            if not isinstance(control_value, list):
                control_value = [control_value]

            self.controls[device_id] = (packet_id, tuple(control_value))


    def set_controls_off(self):
        from RC_Core.devicemanager.product.base_products import KinematicsEngine
        if self._control_mode == Mode.DIRECT_CONTROL:
            for device_id, control in self.controls.items():
                packet_id, data = control
                if packet_id == PacketID.VELOCITY and \
                        device_id in self.control_device_ids:
                    # If it is a  control and a joint
                    self.connection.send_control(device_id, packet_id, [0.0])

            # Kinematics
        elif isinstance(self, KinematicsEngine) and self._control_mode in KM_VELOCITY_MODES:
            for device_id, control in self.controls.items():
                packet_id, data = control

                if device_id == self.get_kinematics_base_device_id() and \
                        packet_id in {PacketID.KM_END_VEL,
                                      PacketID.KM_END_VEL_LOCAL, PacketID.KM_END_VEL_CAMERA}:
                    self.connection.send_control(device_id, packet_id, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.controls = {}