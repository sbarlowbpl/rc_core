from __future__ import annotations
from copy import deepcopy, copy
from typing import Union, Tuple, Optional, Dict

import RC_Core.loadsave
from RC_Core.RS1_hardware import Mode, DeviceType
from RC_Core.devicemanager.masterarm.masterarm_config_lookup_handler import \
    MasterArmDefaultConfigurationLookUpTableHandler
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2, MasterArmV1
from RC_Core.devicemanager.product.devices import Device

from RC_Core.devicemanager.product.rs_product import RSProduct

import asyncio

from RC_Core.commconnection import CommConnection

import rc_logging

logger = rc_logging.getLogger(__name__)

from .ma_devices import MAEnd, MABase, MARotate


class MasterArm(RSProduct):
    '''Master Arm Class

    Defines a Master Arm object, which includes:
    1. Storing of current master arm settings.
    2. Defining functions required to change settings on the master arm.
    3. Handling of incoming data from the master arm, including pushing it to the target manipulator for control.
    4. Connection handling.
    '''
    instances: list = []
    enabled: bool = False
    time_of_last_bytes: float = -1
    stop: bool = False
    masterarm_version: int = 2
    masterarm_model_number: int = 0
    masterarm_version_confirmed: bool = False
    masterarm_is_active: bool = False
    display_name: str = 'MasterArm'

    ma_end: MAEnd = None
    ma_rotates: Tuple[MARotate, ...] = None
    ma_base: MABase = None

    def __init__(self, device_ids: list=None, **kwargs):
        '''
        Handles creation of the MasterArm object.
        :param device_ids: (Optional) Sets the device ids of the master arm if set, otherwise uses default values.
        '''
        self.ma_end = MAEnd(self.connection, 0xC0)
        self.ma_rotates = (MARotate(self.connection, 0xC2),
                           MARotate(self.connection, 0xC3),
                           MARotate(self.connection, 0xC4),
                           MARotate(self.connection, 0xC5),
                           MARotate(self.connection, 0xC6),
                           MARotate(self.connection, 0xC7))
        self.ma_base = MABase(self.connection, 0xCE)

        if device_ids is None:
            device_ids: list = [0xC0, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xCE]
        super(MasterArm, self).__init__(device_ids=device_ids, **kwargs)
        self.masterarm_version: int = 2
        self.type: str = 'MASTERARM'
        self.name: str = 'Master Arm ' + str(len(MasterArm.instances))
        print(__name__, "__init__():", "Master Arm name:", self.name)
        self.device_types: list = [1, 0, 0, 0, 0, 0, 0, 0]
        self.connection = CommConnection.connections[0]
        MasterArm.instances.append(self)
        self.modes: list = [Mode.DISABLED, Mode.DISABLED, Mode.DISABLED, Mode.DISABLED, Mode.DISABLED, Mode.DISABLED, Mode.DISABLED, Mode.DISABLED]
        self.control_modes: list = [0, 0, 0, 0, 0, 0, 0, 0]
        self.use_default_settings: bool = True
        self.mapping_name: str = 'default'
        self.load_mapping(self.mapping_name)

        self.ma_set_lock = asyncio.Lock()

    def clear_parameters(self):
        for i in self.device_list:
            i.clear_parameters()

    def get_master_arm_parameters(self):
        """This function will call an async thread to get the master arm parameters.

        If stop_passthrough is disabled. it will stop the passthrough, until all the parameters are received.
        """
        if self._get_master_arm_parameters_task is not None and not self._get_master_arm_parameters_task.done():
            self._get_master_arm_parameters_task.cancel()
            self._get_master_arm_parameters_task = None

        # stop_passthrough = self.stop_passthrough

        self._get_master_arm_parameters_task = asyncio.create_task(
            self._get_master_arm_parameters())

    _get_master_arm_parameters_task: Optional[asyncio.Task] = None

    async def _get_master_arm_parameters(self):
        # We need:
        # Get ma_base_parameters()
        logger.info("Retrieving Master Arm data")
        print("STARTING get_master_arm_configuration")

        async with self.ma_set_lock:
            self.stop_passthrough = True

            while True:
                if not self.connected:
                    self.stop_passthrough = False
                    return

                from RC_Core.connectionmanager import ConnectionManager
                if ConnectionManager.instance.run_connection_loop == False:
                    logger.info("Connection loop is not running, don't request stuff.")
                    await asyncio.sleep(1.0)
                    continue

                received_config = True
                if self.ma_base.default_pause_mode is None or self.ma_base.pos_preset_device is None:
                    logger.debug("pause_mode")
                    await self.ma_base.get_options()
                    received_config = False
                if self.ma_base.velocity_scale is None:
                    logger.debug("vel_scale")
                    await self.ma_base.get_velocity_scale()
                    received_config = False

                if self.ma_base.position_scale is None:
                    logger.debug("pos_scale")
                    await self.ma_base.get_position_scale()
                    received_config = False

                if self.ma_base.external_device_ids is None:
                    logger.debug("ext_dev_id")
                    await self.ma_base.get_external_device_ids()
                    received_config = False

                if self.ma_base.default_operating_mode is None:
                    logger.debug("operating_mode")
                    await self.ma_base.get_default_operating_mode()
                    received_config = False

                if self.ma_rotates[0].device_type is None:
                    logger.debug("dev_type")
                    await self.ma_rotates[0].get_device_type()
                    received_config = False

                if self.ma_base.model_number is None:
                    logger.debug("model_num")
                    await self.ma_base.get_model_number()
                    received_config = False

                for ma_rotate in self.ma_rotates:
                    logger.debug("ma_rot")
                    if ma_rotate.icmu_direction is None:
                        await ma_rotate.get_icmu_direction()

                    if ma_rotate.position_scale is None:
                        await ma_rotate.get_position_scale()

                if received_config:
                    logger.info("Received master arm configuration")
                    break
                else:
                    logger.info("failed config")

                await asyncio.sleep(1.0)

        self.stop_passthrough = False

    def received_all_parameters(self) -> bool:
        """ return true if all the parameters of the ma has been received"""

        if self.ma_base.default_pause_mode is None or self.ma_base.pos_preset_device is None:
            return False
        if self.ma_base.velocity_scale is None:
            return False

        if self.ma_base.position_scale is None:
            return False

        if self.ma_base.external_device_ids is None:
            return False

        if self.ma_base.default_operating_mode is None:
            return False

        if self.ma_rotates[0].device_type is None:
            return False

        if self.ma_base.model_number is None:
            return False

        return True

    _do_autoconfigure_task: asyncio.Task = None

    def do_autoconfigure(self):
        if self._do_autoconfigure_task is not None and not self._do_autoconfigure_task.done():
            self._do_autoconfigure_task.cancel()
            self.stop_passthrough = False

        self._do_autoconfigure_task = asyncio.create_task(self._autoconfigure())

    async def _autoconfigure(self):
        from RC_Core.devicemanager.product.product_alpha import ProductAlphaArm
        from RC_Core.devicemanager.product.product_bravo import ProductBravoArm
        if not self.received_all_parameters():
            return

        if self.product is None:
            return False

        # Set the default_pause_mode
        from RC_Core.devicemanager.product.base_products import KinematicsEngine
        if self.product is not None:
            if isinstance(self.product, KinematicsEngine):
                self.product: KinematicsEngine
                target_pos_preset_device = self.product.get_kinematics_base_device_id()
            else:
                # Get the device_id_prefix and do 0xPF
                target_pos_preset_device = self.product.get_base_device_id() | 0x0F

        if isinstance(self.product, ProductAlphaArm):
            target_paused_mode = int(Mode.ZERO_VELOCITY)
            target_operating_mode = int(Mode.INDEXED_POSITION_CONTROL)

            end_eff_velocity_scale = 6.0

        elif isinstance(self.product, ProductBravoArm):
            target_paused_mode = int(Mode.POSITION_HOLD)
            target_operating_mode = int(Mode.POSITION_VELOCITY_CONTROL)

            end_eff_velocity_scale = 25.0

        else:
            return False

        if self.ma_rotates[0].device_type == DeviceType.JOYSTICK_X_PROXY_ESTIMATOR:
            wrist_velocity_scale = 5.0

            # invert because proxy x is in the opposite direction
            wrist_position_scale = -1.0

        else:
            wrist_velocity_scale = 1.0
            wrist_position_scale = 1.0

        async with self.ma_set_lock:

            prev_setting = self.stop_passthrough
            self.stop_passthrough = True

            if not await self.ma_base.set_pos_preset_device_and_default_pause_mode(target_pos_preset_device,
                                                                                   target_paused_mode):
                logger.info("Could not set pos_preset")
                self.stop_passthrough = prev_setting
                return False

            if not await self.ma_base.set_default_operating_mode(target_operating_mode):
                logger.info("Could not set operating_mode")
                self.stop_passthrough = prev_setting
                return False

            ext_device_ids, pos_mappings = MasterArmDefaultConfigurationLookUpTableHandler.get_masterarm_mapping(self.product,
                                                                                                                 self.ma_base.model_number)

            pos_mappings[2] = pos_mappings[2] * wrist_position_scale

            if not await self.ma_base.set_external_device_ids(ext_device_ids):
                logger.debug("Could not set external_device_ids")
                self.stop_passthrough = prev_setting
                return False

            if not await self.ma_base.set_position_scale(pos_mappings):

                logger.debug("Could not set position scale")
                self.stop_passthrough = prev_setting
                return False

            velocity_scale = copy(pos_mappings)

            velocity_scale[1] = velocity_scale[1] * end_eff_velocity_scale
            velocity_scale[2] = velocity_scale[2] * wrist_velocity_scale

            if not await self.ma_base.set_velocity_scale(velocity_scale):
                logger.debug("Could not set velocity scale")
                self.stop_passthrough = prev_setting
                return False

            await self.ma_base.factory_save()

        self.stop_passthrough = False

        return True

    @property
    def device_list(self) -> Tuple[Device, ...]:
        return (self.ma_end, ) + self.ma_rotates + (self.ma_base, )

    @property
    def device_dict(self) -> Dict[int, Device]:
        device_dict = {}
        for x in self.device_list:
            device_dict[x.device_id] = x
        return device_dict

    @property
    def device_ids_list(self) -> Tuple[int, ...]:
        return tuple([x.device_id for x in self.device_list])



        # Lock to be used when setting values on the master arm
        # self.ma_set_lock = asyncio.Lock()

    def set_connection(self, connection_obj):
        '''
        Sets the master arm's connection.
        :param connection_obj: Connection object that is set to this master arm object.
        '''

        self.connection = connection_obj
        self.stop_passthrough = False
        self.masterarm_version = 2

    load_mapping_task: asyncio.Task = None

    def load_mapping(self, mapping_name):

        if self.load_mapping_task is not None and not self.load_mapping_task.done():
            self.load_mapping_task.cancel()

        self.load_mapping_task = asyncio.create_task(self.async_load_mapping(mapping_name))

    async def async_load_mapping(self, mapping_name: str):

        self.mapping_name: str = mapping_name
        mappings_list, filenames = self.get_current_mappings()
        if mapping_name not in mappings_list:
            logger.info("Could not find mapping, using default")
            mapping_name = 'default'

        if mapping_name == 'default':
            self.do_autoconfigure()
            return

        mapping_settings = RC_Core.loadsave.get_settings_object(filenames[mappings_list.index(mapping_name)])

        if mapping_settings is None:
            return

        external_device_ids = mapping_settings['external_device_ids']
        position_scale = mapping_settings['position_scale']
        if not isinstance(external_device_ids, list) or not isinstance(position_scale, list):
            logger.warning("save file is not correct")
            return

        if len(external_device_ids) != 8 or len(position_scale) != 8:
            logger.warning("save_file not correct")
            return

        wrist_type = None
        if 'wrist_type' in mapping_settings:
            wrist_type = mapping_settings['wrist_type']
            if wrist_type not in [DeviceType.ROTATE, DeviceType.JOYSTICK_X_PROXY_ESTIMATOR]:
                wrist_type = None

        if wrist_type is None:
            wrist_type = self.ma_rotates[0].device_type

        velocity_scale = copy(position_scale)
        if wrist_type == DeviceType.JOYSTICK_X_PROXY_ESTIMATOR:
            velocity_scale[2] = velocity_scale[2] * 5.0

        async with self.ma_set_lock:
            self.stop_passthrough = True
            if self.product:
                from RC_Core.devicemanager.product.product_alpha import ProductAlphaArm
                from RC_Core.devicemanager.product.product_bravo import ProductBravoArm

                target_paused_mode = None
                target_operating_mode = None

                if isinstance(self.product, ProductAlphaArm):
                    target_paused_mode = Mode.ZERO_VELOCITY
                    target_operating_mode = Mode.INDEXED_POSITION_CONTROL
                    velocity_scale[1] = velocity_scale[1] * 6.0
                elif isinstance(self.product, ProductBravoArm):
                    target_paused_mode = Mode.POSITION_HOLD
                    target_operating_mode = Mode.POSITION_VELOCITY_CONTROL
                    velocity_scale[1] = velocity_scale[1] * 25.0

                if target_paused_mode is not None and target_operating_mode is not None:
                    await self.ma_base.set_pos_preset_device_and_default_pause_mode(self.product.get_base_device_id(), target_paused_mode)
                    await self.ma_base.set_default_operating_mode(target_operating_mode)

            await self.ma_base.set_position_scale(position_scale)
            await self.ma_base.set_velocity_scale(velocity_scale)
            await self.ma_base.set_external_device_ids(external_device_ids)

            if wrist_type:
                await self.ma_rotates[0].set_device_type(wrist_type)
                await self.ma_rotates[0].factory_save()

            await self.ma_base.factory_save()

        self.stop_passthrough = False

    def get_current_mappings(self):
        '''
        Gets list of current mappings that are stored in files.
        :returns output_list, filenames: List of mappings names and their corresponding filenames.
        '''
        output_list: list = []
        # Load all master arm mapping files
        filenames: list = RC_Core.loadsave.get_files_starting_with('masterarm_mapping_')
        for name in filenames:
            # Get mapping name from filename and store in a list.
            output_name: str = name
            output_name = output_name.replace('masterarm_mapping_', '')
            output_name = output_name.replace('.json', '')
            # output_name = output_name.strip('_')
            output_name = output_name.replace('_', ' ')
            output_list.append(output_name)
        return output_list, filenames

    def save_mapping(self, name: str):
        '''
        Saves chosen master arm mapping for this Master Arm object as a file.
        :returns output_list, filenames: List of mappings names and their corresponding filenames.
        '''
        if name and name != '' and 'default' not in name:
            name_ref = name.replace(' ', '_')

            if self.received_all_parameters():
                if name_ref != '':
                    filename = 'masterarm_mapping_' + name_ref + '.json'
                    RC_Core.loadsave.save_settings({'external_device_ids': list(self.ma_base.external_device_ids),
                                                    'position_scale': list(self.ma_base.position_scale),
                                                    'wrist_type': self.ma_rotates[0].device_type},
                                                   filename)

    def get_active_device_ids(self) -> list:
        """
        :returns list of device_ids that operate on the product
        """
        active_device_ids = []
        if self.ma_base.model_number in MasterArmV2.fn4_model_numbers:
            active_device_ids = deepcopy(self.device_ids[0:4])
        elif self.ma_base.model_number in MasterArmV2.fn5_model_numbers + MasterArmV1.model_numbers:
            active_device_ids = deepcopy(self.device_ids[0:5])
        elif self.ma_base.model_number in MasterArmV2.fn7_model_numbers:
            active_device_ids = deepcopy(self.device_ids[0:7])
        for i, dev_id in enumerate(active_device_ids):
            if dev_id == 0xC0:
                active_device_ids[i] = 0xC1
            if dev_id == 0xD0:
                active_device_ids[i] = 0xD1
        return active_device_ids

    def get_base_device_id(self) -> int:
        '''
        :return base_device_id: Base board device id of the master arm
        '''
        return self.ma_base.device_id

    def set_new_heartbeat_array(self, new_heartbeat_array):
        '''
        Sets heartbeat packets (ignored for master arm)
        :param new_heartbeat_array:
        '''
        for i in range(0, len(self.heartbeat_packets)):
            new_heartbeat_copy = copy(new_heartbeat_array)
            self.heartbeat_packets[i] = new_heartbeat_copy
        print(__name__, "set_new_heartbeat_array() heartbeat_packets", self.heartbeat_packets)

    def remove_self(self):
        '''
        MasterArm object removes itself from ReachControl
        '''

        if self._get_master_arm_parameters_task and not self._get_master_arm_parameters_task.done():
            self._get_master_arm_parameters_task.cancel()

        from RC_Core.devicemanager.product import ProductType
        for product_series in [ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]:
            for product_type in product_series:
                for product in product_type.instances:
                    if product.master_arm == self:
                        product.master_arm = None

        if self.connection:
            self.connection.remove_master_arm_slave()

        self.connection = None

        MasterArm.instances.remove(self)





    # product
    _product: Optional[RSProduct] = None

    @property
    def product(self) -> Optional[RSProduct]:
        return self._product

    @product.setter
    def product(self, product):

        logger.debug(f"product({product})")

        # If the products are the same
        if self._product == product:
            logger.warning("Master Arm is already set to this product")
            return

        # If target product connection is the same as the current connection
        if self._product is not None and self.connection:
            if self._product.connection is self.connection:
                logger.warning("Target Product has the same connection as the master_arm")
                product = None

        # If the product connection connections are the same. Then we don't need to reconfigure the passthrough
        if self._product is not None and product is not None:
            if self._product.connection is product.connection:
                logger.info("Product Connection is the same")
                self._product = product
                return

        # Remove the current passthrough connection
        if self.connection:
            self.connection.remove_master_arm_slave()

        # Set the product
        self._product = product

        # Configure the connection.
        if self.connection and self._product and self._product.connection:
            if not self.stop_passthrough:
                self.connection.create_master_arm_slave(self._product.connection)

    def refresh_slave_connection(self):
        logger.debug(f"refresh_slave_connection()")
        if self.connection:
            self.connection.remove_master_arm_slave()

        # Configure the connection.
        if self.connection and self._product and self._product.connection:
            if not self.stop_passthrough:
                self.connection.create_master_arm_slave(self._product.connection)


    # For Cross compatibility, force_stop is the same and stop passthrough
    @property
    def force_stop(self):
        return self.global_stop_passthrough

    @force_stop.setter
    def force_stop(self, fs):
        self.global_stop_passthrough = fs

    # Stop passthrough
    _stop_passthrough: bool = False

    # Property that factory config may set
    _global_stop_passthrough: bool = False

    @property
    def global_stop_passthrough(self):
        return self._global_stop_passthrough

    @global_stop_passthrough.setter
    def global_stop_passthrough(self, gs):
        self._global_stop_passthrough = gs

        if self._stop_passthrough or self._global_stop_passthrough:
            # Call passthrough to stop on the process
            if self.connection:
                self.connection.remove_master_arm_slave()
        else:
            # Call to enable passthrough from the process
            if self.connection and self.product and self.product.connection:
                self.connection.create_master_arm_slave(self.product.connection)
            pass

    @property
    def stop_passthrough(self):
        return self._stop_passthrough

    @stop_passthrough.setter
    def stop_passthrough(self, value):
        logger.debug(f"stop_passthrough({value})")

        self._stop_passthrough = value

        if self._stop_passthrough or self._global_stop_passthrough:
            # Call passthrough to stop on the process
            if self.connection:
                self.connection.remove_master_arm_slave()
        else:
            # Call to enable passthrough from the process
            if self.connection and self.product and self.product.connection:
                self.connection.create_master_arm_slave(self.product.connection)
            pass

    # Connection object
    _connection: Optional[CommConnection] = None


    @property
    def connected(self):
        if self.connection and self.connection.connected:
            return True
        else:
            return False

    def _connected_callback(self, connected: bool):
        """ Callback that is called when the state of a connection is changed"""
        logger.debug(f"_connected_callback({connected})")
        if connected:
            self.get_master_arm_parameters()
        else:
            self.clear_parameters()

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, connection):
        logger.debug(f"connection({connection})")

        if self.connection == connection:
            logger.warning("Master Arm is already set to this connection")
            return

        for device in self.device_list:
            device.connection = connection

        if self._connection:
            if self._connected_callback in self._connection.connected_callbacks:
                self._connection.connected_callbacks.remove(self._connected_callback)
            self._connection.is_passthrough = False
            self._connection.remove_master_arm_slave()

        self._connection = connection

        # Create the master_arm slave
        if self._connection:
            self._connection.connected_callbacks.add(self._connected_callback)
            # On connection, attempt to get master_arm_settings, after getting master_arm_settings,
            # then re-enable the passthrough if needed
            self._stop_passthrough = False
            self.get_master_arm_parameters()





