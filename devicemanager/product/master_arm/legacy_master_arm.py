import asyncio
from copy import deepcopy, copy
from typing import Union, Tuple, Optional, Dict, List

import RC_Core.loadsave
from RC_Core.RS1_hardware import Mode, DeviceType
from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2
from RC_Core.devicemanager.product.rs_product import RSProduct
from RC_Core.packetid import Packets

import asyncio
import logging

from colorlog import ColoredFormatter

from RC_Core.commconnection import CommConnection
from RC_Core.enums.packets import PacketID

import rc_logging
logger = rc_logging.getLogger(__name__)


class LegacyMasterArm:

    instances = []

    name = ""

    _product: Optional[RSProduct] = None

    _connection: Optional[CommConnection] = None

    _stop_passthrough: bool = False

    _stop_passthrough_callback = None

    def set_stop_passthrough_callback(self, callback):
        self._stop_passthrough_callback = callback

    @property
    def stop_passthrough(self):
        return self._stop_passthrough

    @stop_passthrough.setter
    def stop_passthrough(self, value):
        logger.debug(f"stop_passthrough({value})")
        self._stop_passthrough = value
        if self._stop_passthrough_callback:
            self._stop_passthrough_callback(value)

        if self._stop_passthrough:
            # Call passthrough to stop on the process
            if self.connection:
                self.connection.remove_master_arm_slave()
        else:
            # Call to enable passthrough from the process
            if self.connection and self.product and self.product.connection:
                self.connection.create_master_arm_slave(self.product.connection)
            pass

    @property
    def masterarm_is_active(self):
        if self.connection and self.connection.connected and \
                self.product and self.product.connection and self.product.connection.connected and \
                not self.stop_passthrough:
            return True
        else:
            return False

    @property
    def connected(self):
        if self.connection and self.connection.connected:
            return True

    @property
    def force_stop(self):
        return self.stop_passthrough

    @force_stop.setter
    def force_stop(self, fs):
        self.stop_passthrough = fs

    @property
    def force_stop_passthrough(self):
        return self.stop_passthrough

    @force_stop_passthrough.setter
    def force_stop_passthrough(self, fs):
        self.stop_passthrough = fs

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, connection):
        logger.debug(f"connection({connection})")

        if self.connection == connection:
            logger.warning("Master Arm is already set to this connection")
            return

        if self._connection:
            self._connection.is_passthrough = False
            self._connection.remove_master_arm_slave()

        self._connection = connection

        # Create the master_arm slave
        if self._connection:
            if not self.stop_passthrough:
                if self._connection and self.product and self.product.connection:
                    self.connection.create_master_arm_slave(self.product.connection)

    @property
    def product(self):
        return self._product

    @product.setter
    def product(self, product):
        logger.debug(f"product({product})")

        # If the products are the same
        if self._product == product:
            logger.warning("Master Arm is already set to this product")
            return

        # If target product connection is the same as the current connection
        if self._product is not None and self.connection:
            if self._product.connection is self.connection:
                logger.warning("Target Product has the same connection as the master_arm")
                product = None

        # If the product connection connections are the same. Then we don't need to reconfigure the passthrough
        if self._product is not None and product is not None:
            if self._product.connection is product.connection:
                logger.info("Product Connection is the same")
                self._product = product
                return

        # Remove the current passthrough connection
        if self.connection:
            self.connection.remove_master_arm_slave()

        # Set the product
        self._product = product

        # Configure the connection.
        if self.connection and self._product and self._product.connection:
            if not self.stop_passthrough:
                self.connection.create_master_arm_slave(self._product.connection)

    def __init__(self):
        self.name = "Legacy Master Arm" + str(len(self.instances))
        LegacyMasterArm.instances.append(self)

    def set_target_product(self, product):
        self.product = product

    def set_connection(self, connection):
        self.connection = connection

    def remove_self(self):
        LegacyMasterArm.instances.remove(self)

        if self.connection:
            self.connection.remove_master_arm_slave()


