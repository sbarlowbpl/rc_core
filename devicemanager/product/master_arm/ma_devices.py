from copy import copy
from typing import Optional, List, Union, Tuple

from kivy.properties import NumericProperty, BooleanProperty, ListProperty

from RC_Core.enums.packets import PacketID
from RC_Core.devicemanager.product.devices import Device

import rc_logging
logger = rc_logging.getLogger(__name__)


class MARotate(Device):
    icmu_direction: Optional[int] = NumericProperty(None, allownone=True)

    async def get_icmu_direction(self):
        packet: List[int] = await self.request(PacketID.ICMU_PARAMETERS)
        if packet is not None:
            self.icmu_direction = packet[0]

    position_scale: Optional[float] = NumericProperty(None, allownone=True)

    async def get_position_scale(self):
        packet = await self.request(PacketID.POSITION_PARAMETERS)
        if packet is not None:
            self.position_scale = packet[0]

    device_type: Optional[int] = NumericProperty(None, allownone=True)

    async def get_device_type(self):
        packet: List[int] = await self.request(PacketID.DEVICE_TYPE)
        if packet is not None:
            self.device_type = packet[0]
            return self.device_type
        else:
            self.device_type = None

    async def set_device_type(self, device_type: int) -> bool:
        if await self.set(PacketID.DEVICE_TYPE, [device_type]):
            self.device_type = device_type
            return True
        else:
            return False

    # async def reload(self):
    #     await self.get_position_scale()
    #     await self.get_icmu_direction()
    #     await self.get_device_type()
    #     await super(MARotate, self).reload()

    def clear_parameters(self):
        super(MARotate, self).clear_parameters()
        self.icmu_direction = None
        self.position_scale = None
        self.device_type = None


class MAEnd(Device):
    pass


class MABase(Device):
    model_number: Optional[int] = NumericProperty(None, allownone=True)

    async def get_model_number(self):
        logger.debug(f"Requesting_model_number on device_id: {self.device_id}")
        packet: Optional[List[int]] = await self.request(PacketID.MODEL_NUMBER)
        logger.debug(f"Request for model_number on device_id: {self.device_id} returned: {packet}")
        if packet is not None:
            self.model_number = packet[0]
            return self.model_number
        else:
            self.model_number = None
            return None

    jaw_velocity_axis: Optional[int] = NumericProperty(None, allownone=True)  # 0: None, 1: X, 2: Y (Default 2)
    jaw_x_enabled_in_pause_mode: Optional[bool] = BooleanProperty(None, allownone=True)
    jaw_y_enabled_in_pause_mode: Optional[bool] = BooleanProperty(None, allownone=True)
    pos_preset_device: Optional[int] = NumericProperty(None, allownone=True)

    async def set_preset_device(self, pos_preset_device: int) -> bool:
        if None in [self.jaw_velocity_axis,
                    self.jaw_x_enabled_in_pause_mode,
                    self.jaw_y_enabled_in_pause_mode,
                    self.default_pause_mode]:
            if await self.get_options() is None:
                logger.warning("Could not set pause_mode")
                return False

        packet = [self.jaw_velocity_axis,
                  self.jaw_x_enabled_in_pause_mode,
                  self.jaw_y_enabled_in_pause_mode,
                  pos_preset_device,
                  self.default_pause_mode,
                  0, 0, 0, 0, 0]

        if await self.set(PacketID.RC_BASE_OPTIONS, packet):
            self.pos_preset_device = pos_preset_device
            return True

    default_pause_mode: Optional[int] = NumericProperty(None, allownone=True)

    async def set_pause_mode(self, pause_mode: int) -> bool:
        if None in [self.jaw_velocity_axis,
                    self.jaw_x_enabled_in_pause_mode,
                    self.jaw_y_enabled_in_pause_mode,
                    self.pos_preset_device]:
            if await self.get_options() is None:
                logger.warning("Could not set pause_mode")
                return False

        packet = [self.jaw_velocity_axis,
                  self.jaw_x_enabled_in_pause_mode,
                  self.jaw_y_enabled_in_pause_mode,
                  self.pos_preset_device,
                  pause_mode,
                  0, 0, 0, 0, 0]

        if await self.set(PacketID.RC_BASE_OPTIONS, packet):
            self.default_pause_mode = pause_mode
            return True

    async def set_pos_preset_device_and_default_pause_mode(self, pos_preset_device: int, pause_mode: int):
        if None in [self.jaw_velocity_axis,
                    self.jaw_x_enabled_in_pause_mode,
                    self.jaw_y_enabled_in_pause_mode]:
            if await self.get_options() is None:
                logger.warning("Could not set pause_mode")
                return False

        packet = [self.jaw_velocity_axis,
                  self.jaw_x_enabled_in_pause_mode,
                  self.jaw_y_enabled_in_pause_mode,
                  pos_preset_device,
                  pause_mode,
                  0, 0, 0, 0, 0]

        if await self.set(PacketID.RC_BASE_OPTIONS, packet):
            self.pos_preset_device = pos_preset_device
            self.default_pause_mode = pause_mode
            return True

    async def get_options(self):
        packet: Optional[List[int]] = await self.request(PacketID.RC_BASE_OPTIONS)
        if packet is not None:
            # self._external_device_ids = tuple([int(x) for x in packet])
            self.jaw_velocity_axis = packet[0]
            self.jaw_x_enabled_in_pause_mode = bool(packet[1])
            self.jaw_y_enabled_in_pause_mode = bool(packet[2])
            self.pos_preset_device = packet[3]
            self.default_pause_mode = packet[4]
            return packet
        else:
            self.jaw_velocity_axis = None
            self.jaw_x_enabled_in_pause_mode = None
            self.jaw_y_enabled_in_pause_mode = None
            self.pos_preset_device = None
            self.default_pause_mode = None
            return None

    external_device_ids: Optional[List[int]] = ListProperty(None, allownone=True)

    async def get_external_device_ids(self):
        packet: Optional[List[int]] = await self.request(PacketID.RC_BASE_EXT_DEVICE_IDS)
        if packet is not None:
            self.external_device_ids = list(packet)[:8]
            return self.external_device_ids
        else:
            self.external_device_ids = None
            return None

    async def set_external_device_ids(self, device_ids: Union[Tuple[int, ...], List[int]]) -> bool:
        device_ids_list = list(device_ids)

        if await self.set(PacketID.RC_BASE_EXT_DEVICE_IDS, device_ids_list):
            self.external_device_ids = list(device_ids)[:8]
            return True
        else:
            return False

    async def set_external_device_id(self, index: int, device_id: int) -> bool:
        """ Set an external device_id at a certian index,
        This will look at either the current value stored, or if it is one it will attempt to get the value.
        """
        if self.external_device_ids is None:
            await self.get_external_device_ids()

        if self.external_device_ids is None:
            return False

        external_device_ids = copy(self.external_device_ids)
        external_device_ids[index] = device_id
        return await self.set_external_device_ids(external_device_ids)

    velocity_scale: Optional[List[float]] = ListProperty(None, allownone=True)

    async def get_velocity_scale(self):
        packet: Optional[List[float]] = await self.request(PacketID.RC_BASE_VELOCITY_SCALE)
        if packet is not None:
            self.velocity_scale = list(packet)
            return self.velocity_scale
        else:
            self.velocity_scale = None
            return None

    async def set_velocity_scale(self, velocity_scale: Union[Tuple[float, ...], List[float]]) -> bool:
        velocity_scale = list(velocity_scale)

        if await self.set(PacketID.RC_BASE_VELOCITY_SCALE, velocity_scale):
            self.velocity_scale = list(velocity_scale)
            return True
        else:
            return False

    async def set_velocity_scale_index(self, index, scale):
        if self.velocity_scale is None:
            await self.get_velocity_scale()

        if self.velocity_scale is None:
            return False

        velocity_scale = copy(self.velocity_scale)
        velocity_scale[index] = scale
        return await self.set_velocity_scale(velocity_scale)

    position_scale: Optional[List[float]] = ListProperty(None, allownone=True)

    async def get_position_scale(self):
        packet: Optional[List[float]] = await self.request(PacketID.RC_BASE_POSITION_SCALE)
        if packet is not None:
            self.position_scale = list(packet)
            return self.position_scale
        else:
            self.position_scale = None
            return None

    async def set_position_scale(self, position_scale: Union[Tuple[float, ...], List[float]]) -> bool:
        position_scale = list(position_scale)

        if await self.set(PacketID.RC_BASE_POSITION_SCALE, position_scale):
            self.position_scale = list(position_scale)
            return True
        else:
            return False

    async def set_positions_scale_index(self, index, scale):
        if self.position_scale is None:
            await self.get_position_scale()

        if self.position_scale is None:
            return False

        position_scale = copy(self.position_scale)
        position_scale[index] = scale
        return await self.set_position_scale(position_scale)

    default_operating_mode: Optional[int] = NumericProperty(None, allownone=True)  # "Modes: POSITION, VELOCITY, RELATIVE_POSITION

    async def get_default_operating_mode(self):
        packet: Optional[List[int]] = await self.request(PacketID.DEFAULT_OPERATING_MODE)
        if packet is not None:
            self.default_operating_mode = packet[0]
            return self.default_operating_mode
        else:
            self.default_operating_mode = None
            return None

    async def set_default_operating_mode(self, operating_mode: int) -> bool:
        if await self.set(PacketID.DEFAULT_OPERATING_MODE, [operating_mode]):
            self.default_operating_mode = operating_mode
            return True
        else:
            return False

    def clear_parameters(self):
        super(MABase, self).clear_parameters()
        self.model_number = None
        self.jaw_velocity_axis = None  # 0: None, 1: X, 2: Y (Default 2)
        self.jaw_x_enabled_in_pause_mode = None
        self.jaw_y_enabled_in_pause_mode = None
        self.pos_preset_device = None
        self.default_pause_mode = None
        self.velocity_scale = None
        self.position_scale = None
        self.default_operating_mode = None
        self.external_device_ids = None
