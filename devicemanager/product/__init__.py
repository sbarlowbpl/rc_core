from copy import copy, deepcopy

from RC_Core.RS1_hardware import PacketID

from RC_Core.commconnection import CommConnection

MASTER_ARM_ENABLE_FILENAME = "master_enable.json"

from .rs_product import RSProduct, RSControlProduct
from .base_products import ProductArm
from .product_alpha import ProductAlpha5, ProductAlpha5Probe, ProductAlpha5InlineC, \
    ProductAlpha2RotatingGrabber, ProductAlpha2DualBend, ProductAlpha3, ProductAlpha4, \
    ProductAlphaSingleRotate, ProductAlphaSingle90DegRotate, ProductAlpha290DegRotate, ProductAlphaGrabber
from .product_bravo import ProductBravo7, ProductBravo5, ProductBravoSingleRotate, \
    ProductBravo2RotatingGrabber, ProductBravoInlineRotate, ProductBravo3
from .master_arm import MasterArm, LegacyMasterArm


class ProductRT(RSControlProduct):
    """
    ReachTilt class.
    """
    instances: list = []
    rt_uid = -1
    display_name = 'ReachTilt'

    def __init__(self, device_ids: list=None, **kwargs):
        if device_ids is None:
            device_ids = [0xB0]
        super(__class__, self).__init__(device_ids=device_ids)

        from visualisation.arms.vis_arm import EmptyVisArm
        self.visualisation = EmptyVisArm(self)

        self.type = 'REACHTILT'
        self.rt_uid = len(ProductRT.instances)
        self.name = 'Reach Tilt ' + str(self.rt_uid + 1)
        ProductRT.instances.append(self)
        self.default_heartbeat_packets = [[PacketID.POSITION, PacketID.MODE]]
        self.heartbeat_packets = deepcopy(self.default_heartbeat_packets)
        self.control_modes = [0]
        self.control_values = {
            PacketID.VELOCITY: [0],
            PacketID.POSITION: [0],
            PacketID.OPENLOOP: [0],
            PacketID.CURRENT: [0],
            PacketID.INDEXED_POSITION: [0],
            PacketID.POSITION_VELOCITY_DEMAND: [[0, 0]],
        }
        self.device_ids = device_ids
        self.device_types = [0]
        self.name = "REACH TILT"
        self.deviceid_names = {'camera': self.device_ids[0]}

        from hudpanels.modules.camera import HUDPanelCamera
        self.hud = HUDPanelCamera.get_or_create_instance_from_product(self)

    def load_settings(self, loaded_name='', loaded_connection=None, loaded_pos_preset_list=None, enable_start=False, end_effector=None):
        if loaded_name is not None and loaded_name != '':
            self.name = loaded_name
        for connection in CommConnection.connections:
            if connection.name == loaded_connection:
                self.connection = connection

    def set_new_heartbeat_array(self, new_heartbeat_array):
        """
        :return:
        """
        for rm_pkt_id in [PacketID.KM_END_VEL, PacketID.KM_END_POS]:
            if rm_pkt_id in new_heartbeat_array:
                new_heartbeat_array.remove(rm_pkt_id)
        self.heartbeat_packets[0] = new_heartbeat_array
        print(__name__, "set_new_heartbeat_array() heartbeat_packets", self.heartbeat_packets)

    def remove_self(self):
        super(ProductRT, self).remove_self()
        self.hud.remove_self()
        ProductRT.instances.remove(self)
        if self in ProductRT.instances:
            ProductRT.instances.remove(self)


class ProductRL(RSProduct):
    """
    ReachLight class.
    """
    instances: list = []
    display_name = 'ReachLight'

    def __init__(self, device_ids: list=None, **kwargs):
        if device_ids is None:
            device_ids = [0xA1]
        super(__class__, self).__init__(device_ids=device_ids, **kwargs)

        from visualisation.arms.vis_arm import EmptyVisArm
        self.visualisation = EmptyVisArm(self)

        self.type = 'REACHLIGHT'
        ProductRL.instances.append(self)
        self.default_heartbeat_packets = [[PacketID.OPENLOOP]]  # [[PacketID.OPENLOOP, PacketID.MODE]]
        self.heartbeat_packets = deepcopy(self.default_heartbeat_packets)
        self.control_modes = [0]
        self.control_values = {
            PacketID.VELOCITY: [0],
            PacketID.POSITION: [0],
            PacketID.OPENLOOP: [0],
            PacketID.CURRENT: [0],
            PacketID.INDEXED_POSITION: [0],
            PacketID.POSITION_VELOCITY_DEMAND: [[0, 0]],
        }
        self.device_ids = device_ids
        self.device_types = [0]
        self.name = "REACH LIGHT"
        self.deviceid_names = {'lights': self.device_ids[0]}

        from hudpanels.modules.light import HUDPanelLight
        self.hud = HUDPanelLight.get_or_create_instance_from_product(self)


    def load_settings(self, loaded_name: str ='', loaded_connection=None, loaded_pos_preset_list=None, enable_start: bool =False, end_effector=None):
        if loaded_name is not None and loaded_name != '':
            self.name = loaded_name
        for connection in CommConnection.connections:
            if connection.name == loaded_connection:
                self.connection = connection

    def set_new_heartbeat_array(self, new_heartbeat_array: list):
        """
        :return:
        """
        for rm_pkt_id in [PacketID.KM_END_VEL, PacketID.KM_END_POS]:
            if rm_pkt_id in new_heartbeat_array:
                new_heartbeat_array.remove(rm_pkt_id)
        self.heartbeat_packets[0] = new_heartbeat_array
        print(__name__, "set_new_heartbeat_array() heartbeat_packets", self.heartbeat_packets)

    def remove_self(self):
        super(ProductRL, self).remove_self()
        self.hud.remove_self()
        ProductRL.instances.remove(self)
        if self in ProductRL.instances:
            ProductRL.instances.remove(self)


class ProductBravoHub(RSProduct):
    """
    ReachBravoHub class.
    """
    instances: list = []
    display_name = 'ReachBravoHub'

    def __init__(self, device_ids: list=None, **kwargs):
        if device_ids is None:
            device_ids = [0xE0]
        super(ProductBravoHub, self).__init__(device_ids=device_ids, **kwargs)
        self.type = 'REACHHUB'
        ProductBravoHub.instances.append(self)
        self.default_heartbeat_packets = []  # [[PacketID.OPENLOOP, PacketID.MODE]]
        self.heartbeat_packets = deepcopy(self.default_heartbeat_packets)
        self.control_modes = [0]
        self.control_values = {

        }
        self.device_ids = device_ids
        self.device_types = [0]
        self.name = "REACH HUB"
        self.deviceid_names = {'lights': self.device_ids[0]}

    def load_settings(self, loaded_name: str ='', loaded_connection=None, loaded_pos_preset_list=None, enable_start: bool =False, end_effector=None):
        if loaded_name is not None and loaded_name != '':
            self.name = loaded_name
        for connection in CommConnection.connections:
            if connection.name == loaded_connection:
                self.connection = connection

    def set_new_heartbeat_array(self, new_heartbeat_array: list):
        """
        :return:
        """
        for rm_pkt_id in [PacketID.KM_END_VEL, PacketID.KM_END_POS]:
            if rm_pkt_id in new_heartbeat_array:
                new_heartbeat_array.remove(rm_pkt_id)
        self.heartbeat_packets[0] = new_heartbeat_array
        print(__name__, "set_new_heartbeat_array() heartbeat_packets", self.heartbeat_packets)

    def remove_self(self):
        super(__class__, self).remove_self()
        __class__.instances.remove(self)
        for rt in __class__.instances:
            if rt == self:
                __class__.instances.remove(rt)





class ProductType():
    '''ProductType class: Storage system to state what each Product type is - eg. If you want to call only RS1_ACTUATORS'''
    RS1_ACTUATOR: list = [ProductAlpha5, ProductAlpha5Probe, ProductAlpha5InlineC, ProductAlpha2RotatingGrabber, ProductAlpha2DualBend, ProductAlpha3, ProductAlpha4, ProductAlphaSingleRotate, ProductAlphaSingle90DegRotate, ProductAlpha290DegRotate, ProductAlphaGrabber]
    RS1_GRABBER: list = [ProductAlpha5, ProductAlpha5InlineC, ProductAlpha2RotatingGrabber, ProductAlpha3, ProductAlpha4, ProductAlphaGrabber]
    RS1_PROBE: list = [ProductAlpha5Probe, ProductAlpha2DualBend]
    RS1_5FUNCTION: list = [ProductAlpha5, ProductAlpha5Probe, ProductAlpha5InlineC]
    RS2_ACTUATOR: list = [ProductBravo7, ProductBravo5, ProductBravoSingleRotate, ProductBravoInlineRotate, ProductBravo2RotatingGrabber, ProductBravo3]
    RS2_GRABBER: list = [ProductBravo7, ProductBravo5, ProductBravo2RotatingGrabber, ProductBravo3, ProductAlphaGrabber]
    RS2_PROBE: list = []
    ALL_ACTUATORS: list = [ProductAlpha5, ProductAlpha5Probe, ProductAlpha5InlineC, ProductAlpha2RotatingGrabber,
                           ProductAlpha2DualBend, ProductAlpha3, ProductAlpha4, ProductAlphaSingleRotate, ProductAlphaSingle90DegRotate, ProductAlphaGrabber,
                           ProductBravo7, ProductBravo5, ProductBravoSingleRotate, ProductBravoInlineRotate, ProductBravo2RotatingGrabber, ProductAlpha290DegRotate, ProductBravo3]
    CONTROL_PASSTHROUGH: list = [MasterArm]
    ALTERNATIVE: list = [ProductRL, ProductRT]
    FACTORY_ONLY: list = [ProductAlphaSingleRotate, ProductBravoSingleRotate]
    SINGLE_DEVICE_ID: list = [ProductAlphaSingleRotate, ProductAlphaSingle90DegRotate, ProductBravoSingleRotate, ProductBravoInlineRotate]

    @staticmethod
    def get_instances_from_type(list=None):
        if list:
            instances = []
            try:
                for product_classification in list:
                    for product_class in product_classification:
                        for product in product_class.instances:
                            instances.append(product)
                return instances
            except:
                return []
        return []

    @staticmethod
    def get_class_definitions(list=None):
        if list:
            definitions = []
            try:
                for product_classification in list:
                    for product_class in product_classification:
                        definitions.append(product_class)
                return definitions
            except:
                return definitions
        return []


