import os, sys, time

from RC_Core.connectionmanager import ConnectionManager
from RC_Core.devicemanager.hid_masterarm.hidmasterarm import HIDMasterArm
from RC_Core.devicemanager.product import ProductAlpha5
from RC_Core.commconnection import CommConnection, CommType



if __name__ == '__main__':

    connection_manager = ConnectionManager.get_instance()
    connection_manager.external_load()
    product = ProductAlpha5()
    hidmasterarm = HIDMasterArm()
    alpha5_conn = CommConnection()
    alpha5_conn.set_type(CommType.SERIAL)
    alpha5_conn.config_connection('COM14', None)
    product.set_connection(alpha5_conn)
    hidmasterarm.set_target_product(product)
    ts = time.time()
    timeout = 1000.0

    while time.time() < (ts + timeout):
        time.sleep(1.0)
    hidmasterarm.remove_self()
