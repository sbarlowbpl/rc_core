import time
from copy import deepcopy
from typing import List, Union, Tuple

import pywinusb.hid as hid

from RC_Core.devicemanager.hid_masterarm.hidmasterarm_enum import HIDMasterArmEnum


class ChannelWindows():
    '''
    Control of individual HIDMasterArm channels for Windows
    '''

    def __init__(self, device, idx) -> None:
        '''
        Init channel object
        :param device:
        :param idx:
        '''
        self.channel_idx = idx          # Keep track of the number of channels
        self.axis_value = []            # Empty list of axis values
        self.inputs_state = []          # Empty list of inputs
        self.outputs_state = []         # Empty list of outputs

        self.device = device            # Assign device to current instance
        self.device.open()              # Open device for communication

        self.input_reports = self.device.find_input_reports(usage_id=idx)   # Find input reports for current device

        self.device.set_raw_data_handler(self.masterarm_inputs_handler)     # Setup handler to poll masterarm status
        self.out_report = self.device.find_output_reports()                 # Create an output_report object
        self.connected = False                                      # Initialize as disconnected
        self.report_time = 0

    def set_connected(self) -> None:
        """set connection status true by calling this method"""
        self.connected = True

    def close(self) -> None:
        """close connection by calling this method"""
        if self.connected:
            self.clear_outputs()
            self.device.close()     # close the connection and kill the thread
            del self                # delete the object
            self.connected = False

    def clear_outputs(self) -> None:
        """clear all outputs"""
        self.outputs_state = [0, 0, 0, 0]
        buffer = [0] + self.outputs_state  # first item of the list is ID=0x00
        self.out_report[0].set_raw_data(buffer)
        self.out_report[0].send()

    @staticmethod
    def convert(hi_byte, lo_byte) -> int:
        """convert two bytes into 16 bit integer"""
        return (hi_byte << 8) + lo_byte

    def masterarm_inputs_handler(self, data) -> None:
        """process input data for each channel"""
        self.axis_value = []

        if data[0] == HIDMasterArmEnum.HANDLE + 1:
            # Buttons [Stow, Deploy, Pause]
            self.inputs_state = [int(x) for x in bin(data[1])[2:].zfill(3)]  # convert int value to binary list
            # Joystick axis [X, Y]
            self.axis_value.append(None)
            self.axis_value.append(2 * self.convert(data[5], data[4]) / HIDMasterArmEnum.AXIS_SCALE - 1)
            self.axis_value.append(2 * self.convert(data[3], data[2]) / HIDMasterArmEnum.AXIS_SCALE - 1)
            self.axis_value.append(None)
            self.axis_value.append(None)
            self.axis_value.append(None)
            self.axis_value.append(None)
            self.axis_value.append(None)
        elif data[0] == HIDMasterArmEnum.JOINT_VELOCITY + 1:
            # Velocity axis [B, C, D, E, F, G]
            self.axis_value.append(None)
            self.axis_value.append(None)
            self.axis_value.append(HIDMasterArmEnum.VELOCITY_SCALE*(self.convert(data[3], data[2])-HIDMasterArmEnum.AXIS_SCALE/2)/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.VELOCITY_SCALE*(self.convert(data[5], data[4])-HIDMasterArmEnum.AXIS_SCALE/2)/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.VELOCITY_SCALE*(self.convert(data[7], data[6])-HIDMasterArmEnum.AXIS_SCALE/2)/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.VELOCITY_SCALE*(self.convert(data[9], data[8])-HIDMasterArmEnum.AXIS_SCALE/2)/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.VELOCITY_SCALE*(self.convert(data[11], data[10])-HIDMasterArmEnum.AXIS_SCALE/2)/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.VELOCITY_SCALE*(self.convert(data[13], data[12])-HIDMasterArmEnum.AXIS_SCALE/2)/HIDMasterArmEnum.AXIS_SCALE)
            # if self.report_time > 0:
            #     print(__name__, 'masterarm_inputs_handler():', 'Report time:', round(time.time() - self.report_time, 6))
            # self.report_time = time.time()
            # print(__name__, 'masterarm_inputs_handler():', self.axis_value)
        elif data[0] == HIDMasterArmEnum.JOINT_POSITION + 1:
            # Position axis [B, C, D, E, F, G]
            self.axis_value.append(None)
            self.axis_value.append(None)
            self.axis_value.append(HIDMasterArmEnum.POSITION_SCALE*self.convert(data[3], data[2])/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.POSITION_SCALE*self.convert(data[5], data[4])/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.POSITION_SCALE*self.convert(data[7], data[6])/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.POSITION_SCALE*self.convert(data[9], data[8])/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.POSITION_SCALE*self.convert(data[11], data[10])/HIDMasterArmEnum.AXIS_SCALE)
            self.axis_value.append(HIDMasterArmEnum.POSITION_SCALE*self.convert(data[13], data[12])/HIDMasterArmEnum.AXIS_SCALE)

    def get_axis(self) -> list:
        """return the value of the axis 0..2 / return signed integer """
        return deepcopy(self.axis_value)

    def get_input(self) -> list:
        """return the state of the input 0..15 / return 0 or 1 """
        return deepcopy(self.inputs_state)

    def send_out_report(self, data) -> None:
        masterarm_out = [0x00] * HIDMasterArmEnum.LEN
        for i in range(len(data)):
            masterarm_out[i + 1] = data[i]  # Must reserve first byte as this is used by USB hardware

        self.out_report[0].set_raw_data(masterarm_out)
        self.out_report[0].send()



class HIDMasterArmWindowsHandle():
    '''
    Windows handler of HID Master Arm
    '''

    @staticmethod
    def connect() -> Tuple[Union[List[ChannelWindows], None], bool]:
        '''
        Attempt to reconnect
        :return:
        '''
        # STM32_VID = 1155
        # MASTERARM_PID = 22352

        # Reset channels
        channels: List[ChannelWindows] = []
        # Filter HID devices for master arm via vendor ID and product ID and get masterarm channels
        # hid_devices = hid.HidDeviceFilter(VendorId=HIDMasterArmEnum.STM32_VID, product_id=HIDMasterArmEnum.MASTERARM_PID)
        # Get channels and sort by ID
        # try:
        # raw_channels = hid_devices.get_devices()
        raw_channels = []
        all_hids = hid.find_all_hid_devices()
        if all_hids:
            for index, device in enumerate(all_hids):
                if device.vendor_id == HIDMasterArmEnum.STM32_VID and device.product_id == HIDMasterArmEnum.MASTERARM_PID:
                    raw_channels.append(device)
        # except:
        #     pass
        if len(raw_channels) >= 3:
            raw_channel_ids = []
            masterarm_channels = []
            for raw_channel in raw_channels[:3]:
                instance_id = int(raw_channel.instance_id[-1])
                raw_channel_ids.append((instance_id, raw_channel))
            sorted_channel_order = sorted(raw_channel_ids, key=lambda raw_channel_ids: raw_channel_ids[0])
            for i, id_channel in enumerate(sorted_channel_order):
                masterarm_channels.append(id_channel[1])
            for idx, new_channel in enumerate(masterarm_channels):
                channels.append(ChannelWindows(new_channel, idx))
                channels[idx].set_connected()
            connected = True
            return channels, connected
        else:
            connected = False
            return None, connected

    @staticmethod
    def hid_connected(channels) -> bool:
        '''
        Check if HID still connected
        :return:
        '''
        if channels:
            # ts = time.time()
            connected = hid.hid_device_path_exists(channels[0].device.device_path)
            # print(__name__, 'hid_connected():', 'Time taken to check:', round(time.time() - ts, 6))
            return connected
        else:
            return False