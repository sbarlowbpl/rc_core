class HIDMasterArmEnum:
    """ Device enumerations """
    # Number of buttons
    NUM_BUTTONS = 3

    # Scaling
    VELOCITY_SCALE = 10
    POSITION_SCALE = 2*3.14159

    # Button ID's
    PAUSE = 0x02
    STOW = 0x01
    DEPLOY = 0x00

    # LED ID's
    LED_PAUSE = 0x01
    LED_STOW = 0x02
    LED_DEPLOY = 0x03

    # Led's outputs values
    OFF = 0x00  # Off
    ON = 0x01  # On
    FAST_FLASH = 0x02  # Fast blink mode
    SLOW_FLASH = 0x03  # Slow blink mode

    # HID message ID
    ID = 0x00

    # AXIS ID's
    X = 0x00
    Y = 0x01
    B = 0x00
    C = 0x01
    D = 0x02
    E = 0x03
    F = 0x04
    G = 0x05

    # Channel types
    HANDLE = 0x00
    JOINT_VELOCITY = 0x01
    JOINT_POSITION = 0x02

    # Axis scale
    AXIS_SCALE = 65534.0

    # Byte length
    LEN = 65

    # USB Hardware ID's
    STM32_VID = 1155
    MASTERARM_PID = 22352

    DOUBLE_CLICK_TIMEOUT = 0.5
    LONG_PRESS_TIME = 3.0

    BUTTON_PRESSED = 1
    BUTTON_RELEASED = 0