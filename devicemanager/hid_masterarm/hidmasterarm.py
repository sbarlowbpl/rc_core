import os, sys, time, platform
import threading
from copy import deepcopy
from typing import List, Union, Tuple

from RC_Core.devicemanager.masterarm.masterarm_references import MasterArmV2
from RC_Core.packetid import Packets

from RC_Core import loadsave
from RC_Core.commconnection import CommConnection

from RC_Core.RS1_hardware import PacketID, Mode
from RC_Core.devicemanager.hid_masterarm.hidmasterarm_enum import HIDMasterArmEnum

from RC_Core.devicemanager.product import RSProduct, ProductType

HANDLE_CLASS = None
if platform.system() == 'Windows':
    from RC_Core.devicemanager.hid_masterarm.hidmasterarm_windows import HIDMasterArmWindowsHandle, ChannelWindows
    HANDLE_CLASS = HIDMasterArmWindowsHandle
elif platform.system() == 'Linux':
    pass



class HIDMasterArm():
    '''HIDMasterArm
    HID Master Arm Product class
    '''
    instances: list = []

    def __init__(self, use_alt_handle=False, alt_handle_class=None) -> None:
        '''
        Init HID Master Arm and create main thread
        '''
        HIDMasterArm.instances.append(self)
        if use_alt_handle:
            self.handle_class = alt_handle_class
        else:
            self.handle_class = HANDLE_CLASS
        self.channels = []

        # Used to kill main run thread if turned true
        self.kill_self: bool = False
        # Set active state
        self.masterarm_is_active: bool = False
        # Connnection state
        self.connected: bool = False
        self.last_connected_state: bool = False
        self.data_available: bool = False
        self.target_product: Union[RSProduct, None] = None
        self.stop_passthrough: bool = False
        self.force_stop: bool = False
        self.name: str = 'HID Master Arm 0'

        # Other params
        self.indicator_state: List[int] = [0, 0, 0]
        self.active_low_edge_time: float = 0.0
        self.double_click_timeout: float = 0.5
        self.mode: int = HIDMasterArmEnum.PAUSE
        self.pause_release_time: float = 0.0
        self.pause_double_click_set: bool = False
        self.use_default_settings: bool = True
        self.masterarm_model_number: int = MasterArmV2.fn5_model_numbers[0]
        self.masterarm_control_packet: PacketID = PacketID.INDEXED_POSITION
        self.mapping_name: str = 'default'
        self.base_options: Union[list, None] = None
        self.default_operating_mode: int = PacketID.INDEXED_POSITION
        self.last_active_state: bool = False
        self.default_pause_mode: int = Mode.ZERO_VELOCITY

        # Scaling parameters
        self.masterarm_axis_b_rotate: bool = False
        self.target_ext_device_ids: List[int] = [0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0, 0]
        self.target_position_scale: List[float] = [0.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, 1.0]
        self.target_velocity_scale: List[float] = [0.0, 1.0, -1.0, 1.0, 1.0, 1.0, -1.0, 1.0]
        self.indexed_position_origin: List[Union[None,float]] = [None, None, None, None, None, None, None, None]

        # Button states, and rotate velocities and positions
        self.button_states: List[int] = [0] * 3
        self.raw_target_joystick_values: Union[List[float], List[None]] = []
        self.raw_target_velocities: Union[List[float], List[None]] = []
        self.raw_target_positions: Union[List[float], List[None]] = []
        self.last_raw_target_positions: Union[List[float], List[None]] = []
        self.index_position_offsets: Union[List[float], List[None]] = []
        self.output_commands: List[List[int, PacketID, List[float, float]]] = []
        self.last_corrected_rotate_positions: Union[None, List[float]] = None

        # STOW handling
        self.stow_press_time: float = 0
        self.stow_release_time: float = 0.0
        self.stow_save_sent: bool = False
        self.send_stow_save: bool = False
        self.stow_double_pressed: bool = False
        self.stow_command_go: bool = True
        self.pre_stow_active_state: Union[bool, None] = None

        # DEPLOY handling
        self.deploy_press_time: float = 0
        self.deploy_release_time: float = 0.0
        self.deploy_save_sent: bool = False
        self.deploy_stow_save: bool = False
        self.deploy_double_pressed: bool = False
        self.deploy_command_go: bool = True
        self.pre_deploy_active_state: Union[bool, None] = None

        # Create main and connection threads
        self.main_run_loop_thread = threading.Thread(target=self._main_run_loop)
        self.main_run_loop_thread.setDaemon(True)
        self.main_run_loop_thread.start()
        self.connection_loop_thread = threading.Thread(target=self._connection_loop)
        self.connection_loop_thread.setDaemon(True)
        self.connection_loop_thread.start()


    @property
    def product(self):
        return self.target_product

    def connect(self) -> bool:
        '''
        Attempt to reconnect
        :return:
        '''
        # Reset channels
        self.channels: list = []
        self.data_available = False
        # Filter HID devices for master arm via vendor ID and product ID and get masterarm channels
        if self.handle_class:
            self.channels, connected = self.handle_class.connect()
            if self.channels and connected:
                self.connected = True
            else:
                self.connected = False
        else:
            self.connected = False
        return self.connected
        
    def hid_connected(self) -> bool:
        '''
        Check if HID still connected
        :return:
        '''
        if self.handle_class and self.channels:
            connected = self.handle_class.hid_connected(self.channels)
            return connected
        else:
            return False

    def set_target_product(self, product: Union[RSProduct, None]):
        '''
        Set target product
        :return:
        '''
        # Ignore if setting same product
        if self.target_product == product:
            return

        # Pause master arm
        if self.masterarm_is_active:
            self.set_active(False)

        self.target_product = product
        if product is not None:
            self.target_product.master_arm = self
        self.use_default_settings = True
        if self.target_product in ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR]):
            self.masterarm_control_packet = PacketID.INDEXED_POSITION
            self.default_pause_mode = Mode.ZERO_VELOCITY
        elif self.target_product in ProductType.get_instances_from_type([ProductType.RS2_ACTUATOR]):
            self.masterarm_control_packet = PacketID.POSITION_VELOCITY_DEMAND
            self.default_pause_mode = Mode.POSITION_HOLD
        self.reset_control_values()

    def _connection_loop(self) -> None:
        '''
        Refresh connection status loop
        :return:
        '''
        while not self.kill_self:
            time.sleep(0.5)
            # ts = time.time()
            if not self.hid_connected():
                self.connected = self.connect()
            # print(__name__, '_connection_loop():', 'Time taken to check:', round(time.time() - ts, 6))

    def _main_run_loop(self) -> None:
        '''
        Main run thread
        :return:
        '''
        while not self.kill_self:
            time.sleep(0.05)
            # ts = time.time()

            # Recently connected change
            if self.connected != self.last_connected_state:
                if self.connected:
                    print(__name__, 'HIDMasterArm._main_run_loop():', self.name, 'connected')
                    self.masterarm_is_active = False
                else:
                    print(__name__, 'HIDMasterArm._main_run_loop():', self.name, 'disconnected')
                    self.reset_all()
                    self.data_available = False
                self.last_connected_state = self.connected

            # Stop master arm if not connected, no product, or product connection is not connected
            if not self.connected:
                if self.masterarm_is_active:
                    self.masterarm_is_active = False
                    self.reset_all()
                self.reset_control_values()
                self.button_states = [0] * 3
                self.last_connected_state = False
                self.data_available = False
                continue
            # Flash lights if succesfully got data
            elif self.connected and self.channels and not self.data_available:
                data_velocity_channel = self.channels[0].get_axis()
                if data_velocity_channel and len(data_velocity_channel) > 0:
                    self.data_available = True
                    self.set_all()
                    self.slow_flash_all()

            # Check product state
            if self.target_product is None or self.target_product not in \
                    ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                self.set_target_product(None)
                if self.masterarm_is_active:
                    self.masterarm_is_active = False
                    self.slow_flash_all()
                self.reset_control_values()
                continue

            # Handle data
            for ch_i, channel in enumerate(self.channels):
                # Get channel states
                if not self.connected:
                    self.last_connected_state = False
                    self.reset_all()
                    break

                if channel.channel_idx == HIDMasterArmEnum.HANDLE:
                    # Get button states
                    button_states = channel.get_input()
                    if button_states != []:
                        self.handle_button_states(button_states)
                        self.button_states = button_states

                # Check product state
                if self.target_product is None or self.target_product not in \
                        ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                    self.set_target_product(None)
                    self.masterarm_is_active = False
                    self.slow_flash_all()
                    continue

                # Get channel data if active, otherwise reset
                if self.masterarm_is_active:
                    if channel.channel_idx == HIDMasterArmEnum.HANDLE:
                        raw_axis_values = channel.get_axis()
                        if raw_axis_values != []:
                            self.raw_target_joystick_values = raw_axis_values
                    elif channel.channel_idx == HIDMasterArmEnum.JOINT_VELOCITY:
                        raw_axis_values = channel.get_axis()
                        if raw_axis_values != []:
                            self.raw_target_velocities = raw_axis_values
                        if self.masterarm_model_number != MasterArmV2.fn7_model_numbers[0] and \
                                self.raw_target_velocities and self.raw_target_velocities[-1] != 0:
                            self.masterarm_model_number = MasterArmV2.fn7_model_numbers[0]
                    elif channel.channel_idx == HIDMasterArmEnum.JOINT_POSITION:
                        raw_axis_values = channel.get_axis()
                        if raw_axis_values != []:
                            self.raw_target_positions = raw_axis_values
                            if len(self.last_raw_target_positions) != len(self.raw_target_positions):
                                self.last_raw_target_positions = [0] * len(self.raw_target_positions)
                                self.index_position_offsets = [0] * len(self.raw_target_positions)
                else:
                    self.reset_control_values()

            # Stop master arm if not connected, no product, or product connection is not connected
            if not self.connected:
                if self.masterarm_is_active:
                    self.masterarm_is_active = False
                    self.reset_all()
                self.reset_control_values()
                self.button_states = [0] * 3
                self.last_connected_state = False
                continue

            # Check product state
            if self.target_product is None or self.target_product not in \
                    ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                self.set_target_product(None)
                self.masterarm_is_active = False
                self.slow_flash_all()
                continue

            # Convert to values to commands
            self.last_active_state = self.masterarm_is_active
            if not self.masterarm_is_active:
                self.reset_control_values()

    def do_passthrough(self, product_connection: CommConnection):
        '''
        Convert raw data to commands
        :return:
        '''
        # If not active, ignore
        # Send commands
        if self.masterarm_is_active:
            self.stop_passthrough = False
        if not (product_connection and product_connection.connected and self.target_product and
                not self.stop_passthrough and not self.force_stop and self.connected):
            return
        elif not self.masterarm_is_active:
            # Use default operating mode.
            if self.default_pause_mode in [Mode.ZERO_VELOCITY, Mode.VELOCITY_CONTROL]:
                for ext_dev_index, device_id in enumerate(self.target_ext_device_ids):
                    if device_id > 0 and ext_dev_index != 0:
                        Packets.VELOCITY.send(self.target_product.connection, device_id, [0.0])
            elif self.default_pause_mode in [Mode.POSITION_HOLD]:
                for ext_dev_index, device_id in enumerate(self.target_ext_device_ids):
                    if device_id > 0 and ext_dev_index != 0:
                        if ext_dev_index == 1:
                            Packets.VELOCITY.send(self.target_product.connection, device_id, [0.0])
                        elif ext_dev_index == 2:
                            if self.masterarm_axis_b_rotate:
                                Packets.RELATIVE_POSITION.send(self.target_product.connection, device_id, [0.0])
                            else:
                                Packets.VELOCITY.send(self.target_product.connection, device_id, [0.0])
                        else:
                            Packets.RELATIVE_POSITION.send(self.target_product.connection, device_id, [0.0])
            elif self.default_pause_mode in [Mode.RELATIVE_POSITION_CONTROL]:
                for ext_dev_index, device_id in enumerate(self.target_ext_device_ids):
                    if device_id > 0 and ext_dev_index != 0:
                        Packets.VELOCITY.send(self.target_product.connection, device_id, [0.0])
        elif self.stow_save_sent or self.deploy_save_sent:
            return

        self.target_product.set_controls_off()

        if self.masterarm_is_active and not self.stow_save_sent and not self.deploy_save_sent:
            self.set_all()

        # Reset commands
        self.output_commands = []
        if self.raw_target_joystick_values == [] or self.raw_target_velocities == [] or self.raw_target_positions == []:
            return

        # Update corrected directions and commands
        corrected_joystick_velocities = []
        corrected_rotate_velocities = []
        corrected_rotate_positions = []
        corrected_rotate_indexed_positions = []
        try:
            for i in range(len(self.target_ext_device_ids)):
                # Joystick values:
                if self.raw_target_joystick_values[i] is not None:
                    corrected_joystick_velocities.append(self.raw_target_joystick_values[i]*self.target_velocity_scale[i])
                else:
                    corrected_joystick_velocities.append(None)

                # Rotate velocities
                if self.raw_target_velocities[i] is not None:
                    corrected_rotate_velocities.append(self.raw_target_velocities[i] * self.target_velocity_scale[i])
                else:
                    corrected_rotate_velocities.append(None)

                # Rotate positions
                if self.raw_target_positions[i] is not None:
                    corrected_rotate_positions.append(self.raw_target_positions[i] * self.target_position_scale[i])
                else:
                    corrected_rotate_positions.append(None)

                # Rotate indexed position
                if self.indexed_position_origin[i] is None:
                    self.indexed_position_origin[i] = self.raw_target_positions[i]
                    self.last_raw_target_positions[i] = self.raw_target_positions[i]
                    self.index_position_offsets[i] = 0
                if self.raw_target_positions[i] is not None:
                    raw_target_position = self.raw_target_positions[i]
                    dv = raw_target_position - self.last_raw_target_positions[i]
                    # Handle 0/360 boundary changes.
                    if dv < -(0.5 * 2 * 3.1415):
                        dv += (2 * 3.1415)
                    elif dv > (0.5 * 2 * 3.1415):
                        dv -= (2 * 3.1415)
                    self.index_position_offsets[i] += dv
                    corrected_rotate_indexed_positions.append(
                        self.index_position_offsets[i] * self.target_position_scale[i])
                else:
                    corrected_rotate_indexed_positions.append(None)
            self.last_raw_target_positions = deepcopy(self.raw_target_positions)
        except IndexError:
            # Can happen is raw data is being filled while here. Can safely ignore.
            return

        # Handle command outputs, and do not do normal commands is stow/deploy are being used
        if self.stow_command_go or self.deploy_command_go:
            # Do stow/deply, taking stow as primary out of the two
            if self.stow_command_go:
                self.target_product.goto_pos_preset(0)
            elif self.deploy_command_go:
                self.target_product.goto_pos_preset(1)
        else:
            for i, device_id in enumerate(self.target_ext_device_ids):
                # Ignore invalid device ids or first index which has no mapping
                if device_id <= 0 or i == 0:
                    continue

                # Joystick-only forward axis
                if i == 1 and corrected_joystick_velocities[i] is not None:
                    Packets.VELOCITY.send(product_connection, device_id, [corrected_joystick_velocities[i]])

                # Rotate or joystick for axis B
                elif i == 2:
                    if self.masterarm_axis_b_rotate:
                        if corrected_rotate_velocities[i] is not None and \
                                corrected_rotate_indexed_positions[i] is not None:
                            if product_connection and product_connection.connected and \
                                    not self.stop_passthrough and not self.force_stop and self.masterarm_is_active:
                                if self.masterarm_control_packet == PacketID.POSITION_VELOCITY_DEMAND:
                                    Packets.POSITION_VELOCITY_DEMAND.send(product_connection, device_id,
                                                                          [corrected_rotate_indexed_positions[i],
                                                                           corrected_rotate_velocities[i]])
                                elif self.masterarm_control_packet == PacketID.INDEXED_POSITION:
                                    Packets.INDEXED_POSITION.send(product_connection, device_id,
                                                                  [corrected_rotate_indexed_positions[i]])
                                elif self.masterarm_control_packet == PacketID.VELOCITY:
                                    Packets.VELOCITY.send(product_connection, device_id, [corrected_rotate_velocities[i]])
                                elif self.masterarm_control_packet == PacketID.RELATIVE_POSITION and \
                                        self.last_corrected_rotate_positions:
                                    relative_position = corrected_rotate_positions[i] - \
                                                        self.last_corrected_rotate_positions[i]
                                    product_connection.send(device_id, PacketID.RELATIVE_POSITION, [relative_position])
                    else:
                        if corrected_joystick_velocities[i] is not None:
                            Packets.VELOCITY.send(product_connection, device_id, [corrected_joystick_velocities[i]])

                # Other axis, send indexed_position or position_velocity
                elif corrected_rotate_velocities[i] is not None and \
                        corrected_rotate_indexed_positions[i] is not None:
                    if product_connection and product_connection.connected and \
                            not self.stop_passthrough and not self.force_stop and self.masterarm_is_active:
                        if self.masterarm_control_packet == PacketID.POSITION_VELOCITY_DEMAND:
                            Packets.POSITION_VELOCITY_DEMAND.send(product_connection, device_id,
                                                                  [corrected_rotate_indexed_positions[i],
                                                                   corrected_rotate_velocities[i]])
                        elif self.masterarm_control_packet == PacketID.INDEXED_POSITION:
                            Packets.INDEXED_POSITION.send(product_connection, device_id,
                                                                  [corrected_rotate_indexed_positions[i]])
                        elif self.masterarm_control_packet == PacketID.VELOCITY:
                            Packets.VELOCITY.send(product_connection, device_id, [corrected_rotate_velocities[i]])
                        elif self.masterarm_control_packet == PacketID.RELATIVE_POSITION and \
                            self.last_corrected_rotate_positions:
                            relative_position = corrected_rotate_positions[i] - self.last_corrected_rotate_positions[i]
                            product_connection.send(device_id, PacketID.RELATIVE_POSITION, [relative_position])

        # Update values
        if corrected_rotate_positions:
            self.last_corrected_rotate_positions = corrected_rotate_positions


    def reset_control_values(self):
        '''
        Reset control output values
        :return:
        '''
        self.raw_target_joystick_values = []
        self.raw_target_velocities = []
        self.raw_target_positions = []
        self.last_raw_target_positions = []
        self.index_position_offsets = []
        self.last_corrected_rotate_positions = None
        self.output_commands = []
        self.indexed_position_origin = [None, None, None, None, None, None, None, None]

    def set_active(self, active: bool):
        '''
        Handle change of active state
        :param active:
        :return:
        '''
        if self.masterarm_is_active != active:
            self.masterarm_is_active = active
            if self.masterarm_is_active:
                self.set_all()
            else:
                if self.connected:
                    self.slow_flash_all()
                else:
                    self.reset_all()
                self.reset_control_values()

    def handle_button_states(self, button_states: List[int]):
        '''
        Handle button states
        :param button_states:
        :return:
        '''
        self.handle_active(button_states[HIDMasterArmEnum.PAUSE])
        self.handle_position_presets(button_states)
        pass

    def handle_active(self, pause_button):
        '''
        Handle single, double, clicks for pause button
        :param pause_button:
        :return:
        '''
        if self.target_product is None or self.target_product not in \
                ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]) or \
                self.target_product.connection is None or not self.target_product.connection.connected:
            self.set_active(False)
            return

        # Do not do any logic when the cycle has not changed
        if pause_button == self.button_states[HIDMasterArmEnum.PAUSE]:
            return

        self.stow_double_pressed = False
        self.deploy_double_pressed = False

        # If pause button state has changed.
        if pause_button == HIDMasterArmEnum.BUTTON_PRESSED:
            self.set_active(False)     # Always paused when pressed down
            self.slow_flash_all()
            if self.stow_command_go or self.deploy_command_go:
                self.stow_command_go = False
                self.deploy_command_go = False
            # Handle double click
            if time.time() < (self.pause_release_time + HIDMasterArmEnum.DOUBLE_CLICK_TIMEOUT):
                self.pause_double_click_set = True
            else:
                self.pause_double_click_set = False
        else:
            # When released, set active if double press was not set
            if self.pause_double_click_set:
                self.set_active(False)
            else:
                self.set_active(True)
                self.set_all()
            self.pause_double_click_set = False
            self.pause_release_time = time.time()

    def handle_position_presets(self, button_states: List[int]):
        '''
        Handle stow and deploy
        :param button_states:
        :return:
        '''
        if self.target_product is None or self.target_product not in \
                ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]) or \
                self.target_product.connection is None or not self.target_product.connection.connected:
            self.stow_command_go = False
            self.deploy_command_go = False
            return

        # If STOW changed state, record time
        if button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_PRESSED and \
                button_states[HIDMasterArmEnum.STOW] != self.button_states[HIDMasterArmEnum.STOW]:
            self.stow_press_time = time.time()
            self.stow_double_pressed = False
            self.deploy_double_pressed = False
        elif button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_RELEASED and \
                button_states[HIDMasterArmEnum.STOW] != self.button_states[HIDMasterArmEnum.STOW]:
            self.stow_release_time = time.time()
            self.stow_save_sent = False
            if not self.deploy_double_pressed  and (self.deploy_command_go or self.stow_command_go):
                self.deploy_command_go = False
                self.stow_command_go = False
                self.set_active(False)

        # Handle press or release STOW state
        if button_states[HIDMasterArmEnum.STOW] == HIDMasterArmEnum.BUTTON_PRESSED:

            # Handle long press
            if self.stow_press_time and time.time() >= (self.stow_press_time + HIDMasterArmEnum.LONG_PRESS_TIME):
                if button_states[HIDMasterArmEnum.PAUSE] and not self.stow_save_sent:
                    # If ready to save, send CAPTURE in passthrough
                    self.set_active(False)
                    self.target_product.capture_pos_preset(0)
                    # for device_id in self.target_product.pos_preset_devices:
                    #     Packets.POS_PRESET_CAPTURE.send(self.target_product.connection, device_id, [0])
                    self.stow_save_sent = True
                    self.set_active(False)
                    self.fast_flash_all()
                    # print(__name__, 'handle_position_presets():', 'STOW POS_PRESET_CAPTURE')
                    self.stow_command_go = False
                elif self.stow_save_sent:
                    # If save sent, fast flash
                    # self.set_active(False)
                    # print(__name__, 'handle_position_presets():', 'STOW POS_PRESET_CAPTURE')
                    self.fast_flash_all()
                    self.stow_command_go = False
                elif not button_states[HIDMasterArmEnum.PAUSE] and not self.stow_double_pressed:
                    # If save not sent and not paused, then GO
                    self.set_active(True)
                    self.send_stow_save = False
                    self.stow_command_go = True
                else:
                    self.send_stow_save = False

            # Handle Double Press
            elif self.stow_double_pressed or \
                    HIDMasterArmEnum.DOUBLE_CLICK_TIMEOUT > (self.stow_press_time - self.stow_release_time):
                self.stow_double_pressed = True
                self.set_active(False)
                self.stow_command_go = False

            # If not long or double pressed, just GO
            else:
                if not button_states[HIDMasterArmEnum.PAUSE]:
                    self.set_active(True)
                    self.stow_command_go = True

        else:
            # Handle double press released, otherwise do not stow
            if self.stow_save_sent:
                self.set_active(False)
                self.stow_save_sent = False
            if self.stow_double_pressed:
                self.set_active(True)
                self.stow_command_go = True
            else:
                self.stow_command_go = False

        # If DEPLOY changed state, record time
        if button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_PRESSED and \
                button_states[HIDMasterArmEnum.DEPLOY] != self.button_states[HIDMasterArmEnum.DEPLOY]:
            self.deploy_press_time = time.time()
            self.stow_double_pressed = False
            self.deploy_double_pressed = False
        elif button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_RELEASED and \
                button_states[HIDMasterArmEnum.DEPLOY] != self.button_states[HIDMasterArmEnum.DEPLOY]:
            self.deploy_release_time = time.time()
            if not self.deploy_double_pressed  and (self.deploy_command_go or self.stow_command_go):
                self.deploy_command_go = False
                self.stow_command_go = False
                self.set_active(False)
            self.deploy_save_sent = False

        # Handle press or release DEPLOY state
        if button_states[HIDMasterArmEnum.DEPLOY] == HIDMasterArmEnum.BUTTON_PRESSED:

            # Handle long press
            if self.deploy_press_time and time.time() >= (self.deploy_press_time + HIDMasterArmEnum.LONG_PRESS_TIME):
                if button_states[HIDMasterArmEnum.PAUSE] and not self.deploy_save_sent:
                    # If ready to save, send CAPTURE in passthrough
                    self.set_active(False)
                    self.target_product.capture_pos_preset(1)
                    # for device_id in self.target_product.pos_preset_devices:
                    #     Packets.POS_PRESET_CAPTURE.send(self.target_product.connection, device_id, [1])
                    self.set_active(False)
                    self.fast_flash_all()
                    # print(__name__, 'handle_position_presets():', 'DEPLOY POS_PRESET_CAPTURE')
                    self.deploy_save_sent = True
                    self.deploy_command_go = False
                elif self.deploy_save_sent:
                    # If save sent, fast flash
                    # self.set_active(False)
                    # print(__name__, 'handle_position_presets():', 'DEPLOY POS_PRESET_CAPTURE')
                    self.fast_flash_all()
                    self.deploy_command_go = False
                elif not button_states[HIDMasterArmEnum.PAUSE] and not self.deploy_double_pressed:
                    # If save not sent and not paused, then GO
                    self.set_active(True)
                    self.send_deploy_save = False
                    self.deploy_command_go = True
                else:
                    self.send_deploy_save = False

            # Handle Double Press
            elif self.deploy_double_pressed or \
                    HIDMasterArmEnum.DOUBLE_CLICK_TIMEOUT > (self.deploy_press_time - self.deploy_release_time):
                self.deploy_double_pressed = True
                self.set_active(False)
                self.deploy_command_go = False

            # If not long or double pressed, just GO
            else:
                if not button_states[HIDMasterArmEnum.PAUSE]:
                    self.set_active(True)
                    self.deploy_command_go = True

        else:
            # Handle double press released, otherwise do not deploy
            if self.deploy_save_sent:
                self.set_active(False)
                self.deploy_save_sent = False
            if self.deploy_double_pressed:
                self.set_active(True)
                self.deploy_command_go = True
            else:
                self.deploy_command_go = False

    def slow_flash_all(self) -> None:
        """ Slow flash all button indicators """
        if self.indicator_state[0] == HIDMasterArmEnum.SLOW_FLASH:
            return
        self.reset_all()  # Reset all to ensure LED's blink at same rate
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_PAUSE, HIDMasterArmEnum.SLOW_FLASH])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_STOW, HIDMasterArmEnum.SLOW_FLASH])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_DEPLOY, HIDMasterArmEnum.SLOW_FLASH])
        # Set the new indicator state to all slow flash
        self.indicator_state = [HIDMasterArmEnum.SLOW_FLASH] * HIDMasterArmEnum.NUM_BUTTONS
    
    def fast_flash_all(self) -> None:
        """ Fast flash all button indicators """
        if self.indicator_state[0] == HIDMasterArmEnum.FAST_FLASH:
            return
        self.reset_all()  # Reset all to ensure LED's blink at same rate
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_PAUSE, HIDMasterArmEnum.FAST_FLASH])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_STOW, HIDMasterArmEnum.FAST_FLASH])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_DEPLOY, HIDMasterArmEnum.FAST_FLASH])
        # Set the new indicator state to all fast flash
        self.indicator_state = [HIDMasterArmEnum.FAST_FLASH] * HIDMasterArmEnum.NUM_BUTTONS
    
    def reset_all(self) -> None:
        """ Reset all button indicators (all off) """
        if self.indicator_state[0] == HIDMasterArmEnum.OFF:
            return
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_PAUSE, HIDMasterArmEnum.OFF])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_STOW, HIDMasterArmEnum.OFF])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_DEPLOY, HIDMasterArmEnum.OFF])
        # Set the new indicator state to all off
        self.indicator_state = [HIDMasterArmEnum.OFF] * HIDMasterArmEnum.NUM_BUTTONS
    
    def set_all(self) -> None:
        """ Set all button indicators (all on) """
        if self.indicator_state[0] == HIDMasterArmEnum.ON:
            return
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_PAUSE, HIDMasterArmEnum.ON])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_STOW, HIDMasterArmEnum.ON])
        if self.channels:
            self.channels[0].send_out_report([HIDMasterArmEnum.LED_DEPLOY, HIDMasterArmEnum.ON])
        # Set the new indicator state to all on
        self.indicator_state = [HIDMasterArmEnum.ON] * HIDMasterArmEnum.NUM_BUTTONS
    
    def set_indicator(self, indicator_id):
        """ Set single button indicator by indiacator ID """
        if self.channels:
            self.channels[0].send_out_report([indicator_id, HIDMasterArmEnum.ON])
        # Set the new indicator state
        self.indicator_state[indicator_id - 1] = HIDMasterArmEnum.ON
    
    def reset_indicator(self, indicator_id) -> None:
        """ Reset single button indicator by indiacator ID """
        if self.channels:
            self.channels[0].send_out_report([indicator_id, HIDMasterArmEnum.OFF])
        # Set the new indicator state
        self.indicator_state[indicator_id - 1] = HIDMasterArmEnum.OFF
    
    def fast_flash_indicator(self, indicator_id: int) -> None:
        """ Set single button indicator by indiacator ID """
        if self.channels:
            self.channels[0].send_out_report([indicator_id, HIDMasterArmEnum.FAST_FLASH])
        # Set the new indicator state
        self.indicator_state[indicator_id - 1] = HIDMasterArmEnum.FAST_FLASH
    
    def slow_flash_indicator(self, indicator_id):
        """ Slow flash single button indicator by indiacator ID """
        if self.channels:
            self.channels[0].send_out_report([indicator_id, HIDMasterArmEnum.SLOW_FLASH])
        # Set the new indicator state
        self.indicator_state[indicator_id - 1] = HIDMasterArmEnum.SLOW_FLASH
    
    def get_axis_state(self, channel) -> list:
        """ Returns the button state for the current channel.
            If there are no buttons associated with this channel an empty list is returned.
            This empty list has an inherit binary value of zero.
        """
        return channel.get_axis()

    def load_mapping(self, mapping_name: str):
        '''
        Loads the mapping defined by its name if available, otherwise defaults.
        :param mapping_name: Mapping name to set.
        '''
        self.mapping_name: str = mapping_name
        mappings_list, filenames = self.get_current_mappings()
        if self.mapping_name == 'default' or mapping_name not in mappings_list:
            # If mapping is default or mapping is missing, set default.
            self.mapping_name = 'default'
            self.use_default_settings = True
            self.base_options = None
            self.default_operating_mode = None
            return
        else:
            # If mapping corresponding with mapping name is available.
            self.use_default_settings = False
            # Load mapping setting file
            mapping_settings = loadsave.get_settings_object(filenames[mappings_list.index(mapping_name)])
            if mapping_settings:
                # Set target master arm external_device_ids and position_scale
                try:
                    custom_external_device_ids = mapping_settings['external_device_ids']
                    custom_position_scale = mapping_settings['position_scale']
                    if custom_external_device_ids and type(custom_external_device_ids) is list and len(custom_external_device_ids) == 8 \
                            and custom_position_scale and type(custom_position_scale) is list and len(custom_position_scale) == 8:
                        self.target_ext_device_ids = custom_external_device_ids
                        self.target_position_scale = custom_position_scale
                        if 'default_operating_mode' in mapping_settings:
                            if type(mapping_settings['default_operating_mode']) is int:
                                self.default_operating_mode = mapping_settings['default_operating_mode']
                            else:
                                self.default_operating_mode = None
                        else:
                            self.default_operating_mode = None
                        if 'base_options' in mapping_settings:
                            if type(mapping_settings['base_options']) is list:
                                self.base_options = mapping_settings['base_options']
                            else:
                                self.base_options = None
                        else:
                            self.base_options = None
                        return True
                    else:
                        self.mapping_name = 'default'
                        self.use_default_settings = True
                        self.base_options = None
                        print(__name__, 'HIDMasterArm.load_mapping():',
                              'Setting default mapping due to invalid mapping file.')
                        return
                except KeyError as e:
                    # Set default mapping on error - possibly due to settings file is missing data
                    self.mapping_name = 'default'
                    self.use_default_settings = True
                    self.base_options = None
                    print(__name__, 'HIDMasterArm.load_mapping():', 'Setting default mapping due to:', e)
                    return
            else:
                # Set default mapping if missing settings or settings could not be loaded due to file error
                self.mapping_name = 'default'
                self.use_default_settings = True
                self.base_options = None
                print(__name__, 'HIDMasterArm.load_mapping():', 'Setting default mapping due to invalid mapping file.')
                return

    def get_current_mappings(self):
        '''
        Gets list of current mappings that are stored in files.
        :returns output_list, filenames: List of mappings names and their corresponding filenames.
        '''
        output_list: list = []
        # Load all master arm mapping files
        filenames: list = loadsave.get_files_starting_with('masterarm_mapping_')
        for name in filenames:
            # Get mapping name from filename and store in a list.
            output_name: str = name
            output_name = output_name.replace('masterarm_mapping_', '')
            output_name = output_name.replace('.json', '')
            # output_name = output_name.strip('_')
            output_name = output_name.replace('_', ' ')
            output_list.append(output_name)
        return output_list, filenames

    def save_mapping(self, name: str):
        '''
        Saves chosen master arm mapping for this Master Arm object as a file.
        :returns output_list, filenames: List of mappings names and their corresponding filenames.
        '''
        if name and name != '' and 'default' not in name:
            name_ref = name.replace(' ', '_')
            base_options_to_save = self.base_options
            if name_ref != '':
                filename = 'masterarm_mapping_' + name_ref + '.json'
                loadsave.save_settings({'external_device_ids': self.target_ext_device_ids,
                                                'position_scale': self.target_position_scale,
                                                'default_operating_mode': self.default_operating_mode,
                                                'base_options': base_options_to_save},
                                               filename)
                self.base_options = None
                self.default_operating_mode = None
                if self.load_mapping(name):
                    return True
                else:
                    return False
        print(__name__, 'HIDMasterArm.save_mapping():', 'Saving mapping name unsuccessful:', str(name))
        return False

    def remove_self(self) -> None:
        '''
        Remove products and kill all threads
        :return:
        '''
        self.kill_self = True
        self.set_active(False)
        if self.target_product:
            self.target_product.master_arm = None
        self.main_run_loop_thread.join()
        self.connection_loop_thread.join()
        if self in HIDMasterArm.instances:
            HIDMasterArm.instances.remove(self)










