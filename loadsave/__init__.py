import os.path
import platform
import sys
from pathlib import Path
import jsonpickle
# from defaultsettings import *
import rc_logging

logger = rc_logging.getLogger(__name__)

def get_default_settings_directory():
    try:
        local_app_data_path = os.getcwd()
        # bpl_data_path = os.path.join(local_app_data_path, 'defaultsettings')
        bpl_data_path = os.path.abspath(defaultsettings.__file__).strip('__init__.py')
        print(__name__, 'get_default_settings_directory() location:', bpl_data_path)
        # print(__name__,
        #       'get_default_settings_directory() os.path.dirname(os.path.realpath(sys.modules["__main__"].__file__))',
        #       os.path.dirname(os.path.realpath(sys.modules['__main__'].__file__)))
        if not os.path.exists(bpl_data_path):
            os.makedirs(bpl_data_path)
        return bpl_data_path
    except:
        print(__name__, 'get_default_settings_directory() failed')
        return None


def get_default_settings_file_path(filename):
    storage_path = get_default_settings_directory()
    if storage_path is None:
        return None
    path_to_settings = os.path.join(storage_path, filename)
    return path_to_settings


def get_settings_directory():
    try:
        bpl_data_path = ''
        if platform.system() == 'Linux':
            local_app_data_path = os.getenv('HOME')
            bpl_data_path = os.path.join(local_app_data_path, '.reachcontrol')
        elif platform.system() == 'Windows':
            local_app_data_path = os.getenv('LOCALAPPDATA')
            bpl_data_path = os.path.join(local_app_data_path, 'ReachControl')
        # print(__name__, 'get_settings_directory() location:', bpl_data_path)
        if not os.path.exists(bpl_data_path):
            os.makedirs(bpl_data_path)
        return bpl_data_path
    except:
        print(__name__, 'get_settings_directory() failed')
        return None


def get_settings_file_path(filename):
    storage_path = get_settings_directory()
    if storage_path is None:
        return None
    path_to_settings = os.path.join(storage_path, filename)
    return path_to_settings


def get_settings_object(filename):
    file_path = get_settings_file_path(filename)
    if file_path:
        if Path(file_path).exists():
            with open(file_path) as file_to_load:
                json_str = file_to_load.read()
                try:
                    settings_object = jsonpickle.decode(json_str)
                except:
                    return None
                return settings_object

    # file_path = get_default_settings_file_path(filename)
    # if Path(file_path).exists():
    #     with open(file_path) as file_to_load:
    #         json_str = file_to_load.read()
    #         settings_object = jsonpickle.decode(json_str)
    #         return settings_object

    # file_title = filename.split('.')[0]
    # print(__name__, 'get_settings_object() filename, file_title', filename, file_title)
    # module = __import__('defaultsettings.' + file_title, fromlist='default')
    # settings_object = getattr(module, 'default')
    # return settings_object
    return None


def save_settings(object, filename):
    file_path = get_settings_file_path(filename)
    if file_path:
        with open(file_path, 'w') as file_to_save:
            jsonpickle.set_preferred_backend('json')
            jsonpickle.set_encoder_options('simplejson', indent='    ')
            jsonpickle.set_encoder_options('json', indent=4)
            file_to_save.write(jsonpickle.encode(object))


def save_settings_to_path(object, abs_file_path):
    if abs_file_path:
        with open(abs_file_path, 'w') as file_to_save:
            jsonpickle.set_preferred_backend('json')
            jsonpickle.set_encoder_options('simplejson', indent='    ')
            jsonpickle.set_encoder_options('json', indent=4)
            file_to_save.write(jsonpickle.encode(object))


def get_settings_from_path(abs_file_path):
    if abs_file_path:
        if Path(abs_file_path).exists():
            with open(abs_file_path) as file_to_load:
                json_str = file_to_load.read()
                settings_object = jsonpickle.decode(json_str)
                return settings_object
    return None

def get_settings_with_filter_from_path(abs_file_path, filter_func):
    if abs_file_path:
        if Path(abs_file_path).exists():
            with open(abs_file_path) as file_to_load:
                try:
                    json_str = file_to_load.read()
                except OSError as e:
                    logger.error(f"get_settings_with_filter_from_path() error, {e}")
                json_str_filtered = filter_func(json_str)
                settings_object = jsonpickle.decode(json_str_filtered)
                return settings_object
    return None

def get_list_of_settings_files():
    settings_dir = get_settings_directory()
    return os.listdir(settings_dir)


def get_files_starting_with(start_string):
    output_list = []
    for file_name in get_list_of_settings_files():
        if file_name.startswith(start_string):
            output_list.append(file_name)
    return output_list

def delete_filename(filename):
    file_path = get_settings_file_path(filename)
    os.remove(file_path)



if __name__ == "__main__":
    # from time import time
    # save_settings({'time': time(), 'a': [1,2,3], 'b': {1:2, 2:2}}, 'test.json')
    # my_settings = get_settings_object('test.json')
    # print(my_settings)
    # print(my_settings['b'][str(1)])

    # print(get_settings_directory())
    # for file_name in os.listdir(get_settings_directory()):
    #     print(file_name)

    start_string = 'xboxmapping'
    print(get_files_starting_with(start_string))
