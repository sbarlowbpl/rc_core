from typing import List, Dict

import pandas as pd
import datetime
import os
import urllib
import urllib.request
import webbrowser
from urllib.error import HTTPError

import browser_cookie3
import RS1_hardware, loadsave

HEADER_ROWS = 2  # Number of Header rows on the excel document
HEADER_ROW = ["Packet Name", "Packet ID", "Parameter"]

product_config_url_map = {"Alpha": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=ffc97d8a%2Db89f%2D4315%2Db3e0%2Dc2d5d18f18c7",
                          "Bravo": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=8c3e390d%2D5acb%2D4cae%2Db746%2Dcc91b9c1d477",
                          "Master Arm": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=a513ec31%2Dd561%2D4c4a%2Db8b7%2D23522d9765d5",
                          "Reach Tilt": "https://theblueprintlab.sharepoint.com/sites/BlueprintLab/_layouts/15/download.aspx?UniqueId=a89a8daa%2Dafea%2D46c0%2D9982%2D6da261e14bc5"}

sharepoint_url = "https://theblueprintlab.sharepoint.com/sites/BlueprintLab"
destination_file = "config_file.xlsx"

output_directory = "../configfileoutput/"


if __name__ == '__main__':
    print("Config File generator: - This will download the config file from SharePoint, then generate the "
          "config files from it")

    dir_path = os.path.dirname(os.path.realpath(__file__))
    dest_path = os.path.join(dir_path, destination_file)

    if os.path.exists(dest_path):
        print("Removing Existing .xlsx file")
        os.remove(dest_path)

    request_string = ""
    product_list = list(product_config_url_map.keys())
    for idx, product in enumerate(product_list):
        request_string += f"\t [{idx}]: {product}\n"

    invalid = False
    input_index = input(f"Which product would you like to generate config files for?\n{request_string} ")
    product_name = ""
    try:
        product_name = product_list[int(input_index)]
    except (TypeError, IndexError, ValueError):
        invalid = True
    while invalid:
        input_index = input(f"Please select from the following options:\n{request_string}")
        try:
            product_name = product_list[int(input_index)]
            invalid = False
        except (TypeError, IndexError, ValueError):
            invalid = True

    file_url = product_config_url_map[product_name]

    cj = browser_cookie3.load()
    opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))

    try:
        url_open = opener.open(file_url)
    except HTTPError as e:
        print("Error, please log into SharePoint through your default browser, and then retry running this script.")
        webbrowser.open(sharepoint_url)
        exit()

    download = url_open.read()

    print(f"Downloading xlxs file for {product_name}.")
    with open(dest_path, 'wb') as out_file:
        out_file.write(download)

    excel_file = pd.ExcelFile(dest_path)
    # print(excel_file)

    # print out all product codes
    product_sheet = excel_file.parse('Product-Assembly')
    print(product_sheet['ProductCode'])

    # Add entries to products dict
    products = dict()
    for product_code, srs_assembly in product_sheet.set_index('ProductCode').iterrows():
        products[product_code] = srs_assembly.dropna().to_dict()
    print(products)

    # get global packets
    # format
    # global_packets = {
    #   packet_name: {param_name: value, param_name: value, ...},
    #   packet_name: {param_name: value, param_name: value, ...},
    #   ...
    # }
    global_packets: Dict = {}

    # elec_revisions
    # format
    # elec_revisions = {
    #   "EV310": <copy of global_packets>,
    #   "EV012": <copy of global_packets>,
    #   ...
    # }
    # Then fill in additional packets from each EV Packets column
    elec_revisions: Dict = {}

    # Drivetrains
    # Take list of drivetrains and multiply by list of mech_revisions
    # drivetrains = {
    #   "LINEAR": { "MV001": <copy of elec_revisions>,
    #               "MV004": <copy of elec_revisions>,
    #               "MV005": <copy of elec_revisions>,
    #              },
    #   "INLINE_ROTATE": { "MV001": <copy of elec_revisions>,
    #               "MV004": <copy of elec_revisions>,
    #               "MV005": <copy of elec_revisions>,
    #              },
    #   "HIGH_TORQUE": { "MV001": <copy of elec_revisions>,
    #               "MV004": <copy of elec_revisions>,
    #               "MV005": <copy of elec_revisions>,
    #              }
    #   }
    # Then fill in additional packets from each Drivetrain Packets column
    drivetrains: Dict = {}

    # Assembly
    # Generate each of the assemblies from the drivetrains
    # Map assembly to drivetrain with Assembly-Subassembly sheet



