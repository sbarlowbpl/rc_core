"""
A file of partial strings used to identify update files.
"""
from RC_Core.commconnection import CommType
from RC_Core.devicemanager.product.product_alpha import ProductAlphaArm
from RC_Core.devicemanager.product.master_arm.master_arm import MasterArm
from RC_Core.update.update_modules.alpha_update import UpdateAlpha703Serial
from RC_Core.update.update_modules.masterarm_update import UpdateMasterArm705Serial
from RC_Core.update.update_modules.bravo_update import UpdateBravo708703Serial, UpdateBravo708703Udp, \
    UpdateBravoTX2Udp
from RC_Core.devicemanager.product.product_bravo import ProductBravoArmKinematicsEngine, ProductBravoArm, \
    ProductBravoSingleRotate


# Bravo
RS2_703_STR = "RS2_703"
RS2_708_STR = "RS2_708"
RS2_TX2_STR = "RS2_TX2"

# Alpha
RS1_703_STR = "RS1_703"

# Master arm
RC_703_STR = "RC_703"
RC_705_BASE_STR = "RC_705_Rev2_0_BASE"
RC_705_END_STR = "RC_705_Rev2_0_EndEff"


# Update sets
connection_update_sets = {CommType.SERIAL: {RS2_703_STR,
                                            RS2_708_STR,
                                            RS1_703_STR,
                                            RC_705_BASE_STR,
                                            RC_703_STR,
                                            RC_705_END_STR},
                          CommType.UDP:    {RS2_703_STR,
                                            RS2_708_STR,
                                            RS2_TX2_STR}}

product_update_sets = {ProductBravoArmKinematicsEngine: {RS2_703_STR,
                                                         RS2_708_STR,
                                                         RS2_TX2_STR},
                       ProductBravoSingleRotate:        {RS2_703_STR},
                       ProductBravoArm:                 {RS2_703_STR,
                                                         RS2_708_STR},
                       ProductAlphaArm:                 {RS1_703_STR},
                       MasterArm:                       {RC_705_BASE_STR,
                                                         RC_703_STR,
                                                         RC_705_END_STR}}

update_classes_set = {CommType.UDP:    {UpdateBravo708703Udp:     {RS2_708_STR,
                                                                   RS2_703_STR},
                                        UpdateBravoTX2Udp:        {RS2_TX2_STR}},
                      CommType.SERIAL: {UpdateBravo708703Serial:  {RS2_708_STR,
                                                                   RS2_703_STR},
                                        UpdateAlpha703Serial:     {RS1_703_STR},
                                        UpdateMasterArm705Serial: {RC_705_BASE_STR,
                                                                   RC_703_STR,
                                                                   RC_705_END_STR}}
                      }
update_device_id_set = {RS2_703_STR:     {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7},
                        RS2_708_STR:     {0xD},
                        RS2_TX2_STR:     {0xE},
                        RS1_703_STR:     {0x1, 0x2, 0x3, 0x4, 0x5, 0x6},
                        RC_705_BASE_STR: {0xCE},
                        RC_703_STR:      {0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7},
                        RC_705_END_STR:  {0xC0}}
