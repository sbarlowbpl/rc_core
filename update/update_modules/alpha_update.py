import serial
from math import ceil

import time

import asyncio
from typing import Tuple, List, Dict, Union

from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.product import RSProduct
from RC_Core.enums import PacketID
from RC_Core.update.bootloader.bootloader import HEX_Parse, Bootloader, BootloaderException, FirmwareWriter
from RC_Core.update.update_modules.updatebase import UpdateBase
from RC_Core.update.update_modules import update_defines
from RC_Core.connectionmanager import ConnectionManager


def check_hex(firmware):
    """
    Check a hex file for start, end address and size
    :param firmware:
    :return:
    """
    start_address = 0
    end_address = 0
    hex_parser = HEX_Parse()
    lines = len(firmware)
    start = False

    print("Check HEX file in progress(0%)", end='')

    for i in range(lines):
        try:
            state, address, data = hex_parser.parse_line(firmware[i])
        except Exception as e:
            raise
        print("\r" + "Check HEX file in progress({}%)".format(((i * 100) / lines) // 1), end='')
        if start is False and state == hex_parser.PARSE_STATE['DATA']:
            start_address = address
            start = True
        if state == hex_parser.PARSE_STATE['DATA']:
            end_address = address + len(data)

    # 54216 - bin
    size = end_address - start_address
    print("\r" + "Check HEX file in progress(100%) ")
    return start_address, end_address, size


class UpdateAlpha703Serial(UpdateBase):
    _name: str = "Alpha joints - Serial"
    _attempts: int = 3

    def __init__(self, product: RSProduct, update_dict: Dict[str, Tuple[str, List[int]]], **kwargs):
        super(UpdateAlpha703Serial, self).__init__(product, update_dict)
        self.product = product
        self.duration_of_first_transfer = 0.0

        # List of all device ID's being updated
        self.update_device_id_list = list()

        # Check files are available
        if update_defines.RS1_703_STR not in update_dict:
            self.status_errors.append("Could not detect update file. Please try another folder.")
            return

        # Get the 703 update and devices
        self.ra_703_firmware = Bootloader.open_file_direct(update_dict[update_defines.RS1_703_STR][0])
        self.ra_703_devices: List[int] = update_dict[update_defines.RS1_703_STR][1]
        self.update_device_id_list.extend(self.ra_703_devices)

        # Get the connection and create a bootloader object
        self.connection: CommConnection = product.connection
        self.bootloader = Bootloader(self.connection)

    @property
    def name(self):
        return self._name

    async def update(self) -> bool:
        if not self.update_enabled:
            self.status_persist_msg = f"Skipping update: {self._name}"
            return True

        ConnectionManager.get_instance().run_connection_loop = False
        await self._set_parity(self.bootloader.connection, "Even", timeout=2.0)
        update_status = await self._update()
        await self._set_parity(self.bootloader.connection, "None", timeout=0.1)
        ConnectionManager.get_instance().run_connection_loop = True

        if update_status:
            await self._wait_for_devices()

        return update_status

    async def _update(self) -> bool:
        # Check the hex files
        self.status_msg = "Checking hex file"
        try:
            check_hex(self.ra_703_firmware[0])
        except Exception as e:
            self.status_errors.append("invalid Hex file." + str(e))
            return False

        # Request user to power cycle
        if not await self._request_power_cycle():
            return False

        # Add warning to status
        self.status_warnings.append('Starting update. Do not turn off your device.')

        # Upload hex to first device
        start_time = time.time()

        self.status_verbose_msg = "Erasing RS1-703 Firmware"
        try:
            start_address, end_address, size = check_hex(self.ra_703_firmware[0])
            await self.bootloader.erase_sectors_firmware(start_address, size)
            await asyncio.sleep(0.2)
        except BootloaderException as e:
            self.status_errors.append(f"Could not erase firmware." + str(e))
            return False

        firmware_writer = FirmwareWriter(self.bootloader)

        num_updates = ceil(len(self.update_device_id_list) / 2.0)
        rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
        progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng))
        try:
            self.status_verbose_msg = "Updating RS1-703 Firmware"
            await firmware_writer.write_firmware_HEX(self.ra_703_firmware[0], retry_timeout=2.0)
        except Exception as e:
            self.status_errors.append(f"Update failed." + str(e))
            return False
        finally:
            progress_checker_task.cancel()

        self.status_verbose_msg = f"Starting base software."
        for attempt in range(self._attempts):
            try:
                await self.bootloader.go(start_address)
                break
            except BootloaderException as e:
                await asyncio.sleep(0.5)
                if attempt == self._attempts - 1:
                    self.status_warnings.append(f"Failed to start device." + str(e))

        self.duration_of_first_transfer = time.time() - start_time
        return True

    async def _wait_for_devices(self):
        pending_devices = self.ra_703_devices
        elapsed_time = 0.0
        start_time = time.time()
        initial_progress = self.status_percentage
        num_devices = len(self.ra_703_devices)
        timeout = self.duration_of_first_transfer * ceil(((num_devices + num_devices % 2) - 2) / 2.0)

        while pending_devices and elapsed_time < timeout:
            self.status_msg = f"Applying internal update. Waiting for devices: {pending_devices}"
            self.status_percentage = initial_progress + (elapsed_time / timeout) * 100

            for device_id in pending_devices:
                try:
                    if await self.product.connection.request(device_id, PacketID.SOFTWARE_VERSION):
                        pending_devices.remove(device_id)
                except TimeoutError as e:
                    pass

            await asyncio.sleep(0.1)
            elapsed_time = time.time() - start_time

        # Set status to 100%
        self.status_percentage = 100

        if elapsed_time > timeout:
            self.status_persist_msg = f"Failed to get response from all devices."
            return

        self.status_persist_msg = f"{self._name} update complete."

    @staticmethod
    async def _set_parity(conn: CommConnection, parity: str, timeout=0.1):
        await asyncio.sleep(0.2)

        if parity == "Even":
            conn.config_connection(conn.serial_port, "", parity=serial.PARITY_EVEN, timeout=timeout)
        elif parity == "None":
            conn.config_connection(conn.serial_port, "", parity=serial.PARITY_NONE, timeout=timeout)

        await asyncio.sleep(0.2)

    async def _request_power_cycle(self, attempts=10):
        self.status_verbose_msg = "Initialising update"
        self.dispatch('on_start_intervention_request', '[b]Please "power-cycle" now, or wait for update "auto-start".')
        self.status_persist_msg = "[/b]"

        for i in range(attempts):
            try:
                if await self.bootloader.start(timeout=0.2):
                    self.dispatch("on_stop_intervention_request")
                    break
            except BootloaderException:
                self.status_msg = f"Update auto-start in: {attempts - i}    [sec]"

            await asyncio.sleep(0.98)
        else:
            await self._set_parity(self.bootloader.connection, "None")
            self.bootloader.connection.send(0xFF, 0xFF, b'0')
            await self._set_parity(self.bootloader.connection, "Even", 2.0)
            await asyncio.sleep(0.2)

            try:
                if await self.bootloader.start(timeout=3.0):
                    self.dispatch("on_stop_intervention_request")
            except BootloaderException:
                self.status_errors.append(f"Update auto-start failed. This feature may not be compatible.")
                return False
        return True

    async def progress_checker(self, firmware_writer: FirmwareWriter, progress_range: Tuple[float, float]):
        while True:
            percentage = firmware_writer.write_progress/100
            progress = progress_range[0] + percentage * (progress_range[1] - progress_range[0])
            self.status_percentage = progress
            self.status_msg = f"Uploading Firmware to base device: {round(percentage * 100)}%"
            await asyncio.sleep(0.05)

