from abc import ABC, abstractmethod
from typing import Dict, Any

from kivy.event import EventDispatcher
from kivy.properties import NumericProperty, StringProperty, ListProperty, BooleanProperty

from RC_Core import rc_logging
from RC_Core.devicemanager.product import RSProduct

logger = rc_logging.getLogger(__name__)


class UpdateBase(EventDispatcher, ABC):

    status_percentage = NumericProperty(0)
    status_msg = StringProperty('')
    status_persist_msg = StringProperty('')
    status_verbose_msg = StringProperty('')
    status_warnings = ListProperty([])
    status_errors = ListProperty([])

    device_update_disabled_list = ListProperty([])
    update_device_id_list = ListProperty([])

    bootloader_running = BooleanProperty(False)
    update_enabled = BooleanProperty(True)

    def __init__(self, product: RSProduct, updates_list: Dict[Any, tuple], **kwargs):

        super(UpdateBase, self).__init__(**kwargs)

        self.register_event_type('on_child_status_percentage')
        self.register_event_type('on_start_intervention_request')
        self.register_event_type('on_stop_intervention_request')

    @abstractmethod
    async def update(self):
        pass

    def on_child_status_percentage(self, data):
        """ This is dispatched when a child progress bar needs updating"""
        pass

    def on_start_intervention_request(self, status_msg):
        """ This is dispatched when a request to restart the device is needed"""
        pass

    def on_stop_intervention_request(self):
        """ This is dispatched when a request to stop user intervention is needed"""
        pass

    @property
    @abstractmethod
    def name(self):
        pass


def UpdateBaseTest(UpdateBase):

    async def update():
        pass