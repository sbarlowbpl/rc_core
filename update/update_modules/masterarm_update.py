import asyncio
from typing import Tuple, List, Dict

from RC_Core.devicemanager.product import MasterArm
from RC_Core.update.bootloader.bootloader import HEX_Parse, Bootloader, BootloaderException, FirmwareWriter
from RC_Core.update.update_modules.updatebase import UpdateBase
from RC_Core.update.update_modules import update_defines
from RC_Core.connectionmanager import ConnectionManager

# Note: currently we require a power cycle.
# to get around this, dispatch a user request,
# as part of the msg pass a handle to a function that
# should be run after the request has been fulfilled.
#
# A similar technique should be used when a device
# fails to update


def check_hex(firmware):
    """
    Check a hex file for start, end address and size
    :param firmware:
    :return:
    """
    start_address = 0
    end_address = 0
    hex_parser = HEX_Parse()
    lines = len(firmware)
    start = False

    print("Check HEX file in progress(0%)", end='')

    for i in range(lines):
        try:
            state, address, data = hex_parser.parse_line(firmware[i])
        except Exception as e:
            raise
        print("\r" + "Check HEX file in progress({}%)".format(((i * 100) / lines) // 1), end='')
        if start is False and state == hex_parser.PARSE_STATE['DATA']:
            start_address = address
            start = True
        if state == hex_parser.PARSE_STATE['DATA']:
            end_address = address + len(data)

    # 54216 - bin
    size = end_address - start_address
    print("\r" + "Check HEX file in progress(100%) ")
    return start_address, end_address, size


class UpdateMasterArm705Serial(UpdateBase):
    _name: str = "Masterarm base, joints, & handle"
    _attempts: int = 3

    def __init__(self, product: MasterArm, update_dict: Dict[str, Tuple[str, List[int]]], **kwargs):
        super(UpdateMasterArm705Serial, self).__init__(product, update_dict)
        self.product = product

        # List of all device ID's being updated
        self.update_device_id_list = list()

        # Get the 705 base update and devices
        self.ma_705_base_firmware = Bootloader.open_file_direct(update_dict[update_defines.RC_705_BASE_STR][0])
        self.ma_705_base_devices: List[int] = update_dict[update_defines.RC_705_BASE_STR][1]
        self.update_device_id_list.extend(self.ma_705_base_devices)

        # Get the 703 update and devices
        self.ma_703_firmware = Bootloader.open_file_direct(update_dict[update_defines.RC_703_STR][0])
        self.ma_703_devices: List[int] = update_dict[update_defines.RC_703_STR][1]

        # Perform checks for master arm model
        if self.product.get_active_device_ids():
            self.ma_703_devices = list(set([x for x in self.ma_703_devices]).intersection(
                                       set([x for x in self.product.get_active_device_ids()])))

        self.update_device_id_list.extend(self.ma_703_devices)

        # Get the 705 handle update and devices
        self.ma_705_end_firmware = Bootloader.open_file_direct(update_dict[update_defines.RC_705_END_STR][0])
        self.ma_705_end_devices: List[int] = update_dict[update_defines.RC_705_END_STR][1]
        self.update_device_id_list.extend(self.ma_705_end_devices)

        # Get the connection and create a bootloader object
        self.bootloader = Bootloader(self.product.connection)

    @property
    def name(self):
        return self._name

    async def update(self) -> bool:
        if not self.update_enabled:
            self.status_persist_msg = f"Skipping update: {self._name}"
            return True

        ConnectionManager.get_instance().run_connection_loop = False
        update_status = await self._update_all()
        ConnectionManager.get_instance().run_connection_loop = True

        return update_status

    async def _update_all(self) -> bool:
        # Check the hex files
        self.status_msg = "Checking hex files"
        try:
            start_address_705_base, end_address_705_base, size_705_base = check_hex(self.ma_705_base_firmware[0])
            start_address_703, end_address_703, size_703 = check_hex(self.ma_703_firmware[0])
            start_address_705_end, end_address_705_end, size_705_end = check_hex(self.ma_705_end_firmware[0])
        except Exception as e:
            self.status_errors.append("invalid Hex file(s)." + str(e))
            return False

        # Get firmware writer object
        firmware_writer = FirmwareWriter(self.bootloader)

        # Request user to power cycle
        if not await self._request_power_cycle():
            return False

        # Add warning to status
        self.status_warnings.append('Starting update. Do not turn off your device.')

        # Upload firmware to base
        if self.ma_705_base_devices[0] not in self.device_update_disabled_list:
            # Try to erase base firmware
            self.status_verbose_msg = "Erasing base firmware"
            try:
                await self.bootloader.erase_sectors_firmware(start_address_705_base, size_705_base)
                await asyncio.sleep(0.2)
            except BootloaderException as e:
                self.status_errors.append(f"Could not erase firmware for 0x{format(self.ma_705_base_devices[0], '02X')}." + str(e))
                return False

            # Start firmware update
            self.status_verbose_msg = "Updating base firmware"
            num_updates = (len(self.update_device_id_list) - len(self.device_update_disabled_list))
            rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
            progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng, self.ma_705_base_devices[0]))
            try:
                await firmware_writer.write_firmware_HEX(self.ma_705_base_firmware[0], retry_timeout=2.0)
                await asyncio.sleep(0.2)
            except Exception as e:
                self.status_errors.append(f"Firmware upload failed for 0x{format(self.ma_705_base_devices[0], '02X')}." + str(e))
                return False
            finally:
                progress_checker_task.cancel()

        # Start pass-through
        self.status_verbose_msg = f"Starting pass-though to joints"
        for attempt in range(self._attempts):
            try:
                await self.bootloader.passthrough()
                await asyncio.sleep(0.2)
                break
            except BootloaderException as e:
                if attempt == self._attempts - 1:
                    self.status_errors.append("Failed to establish pass-though." + str(e))
                    return False

        # Upload firmware to all joints
        for i, device_id in enumerate(self.ma_703_devices):
            # Check if device should be skipped
            if device_id in self.device_update_disabled_list:
                self.status_verbose_msg = f"Skipping device 0x{format(device_id, '02X')}"
                continue

            # Try to select device
            self.status_verbose_msg = f"Selecting device 0x{format(device_id, '02X')}"
            try:
                await self.bootloader.select(device_id)
            except BootloaderException as e:
                self.status_errors.append(f"Failed to select device 0x{format(device_id, '02X')}." + str(e))
                # TODO: Ask for user input at this point...
                continue

            # Try to erase firmware
            self.status_verbose_msg = f"Erasing firmware for device 0x{format(device_id, '02X')}"
            try:
                await self.bootloader.erase_sectors_firmware(start_address_703, size_703)
            except BootloaderException as e:
                self.status_errors.append(f"Failed to erase firmware for 0x{format(device_id, '02X')}." + str(e))
                # TODO: Ask for user input at this point...
                continue

            # Try write firmware to selected device
            self.status_verbose_msg = f"Writing firmware for device 0x{format(device_id, '02X')}"
            num_updates = (len(self.update_device_id_list) - len(self.device_update_disabled_list))
            rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
            progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng, device_id))
            try:
                await firmware_writer.write_firmware_HEX(self.ma_703_firmware[0], retry_timeout=2.0)
            except Exception as e:
                self.status_errors.append(f"Firmware upload failed for 0x{format(device_id, '02X')}." + str(e))
                return False
            finally:
                progress_checker_task.cancel()

            # Upload was successful
            self.status_verbose_msg = f"Firmware update for device 0x{format(device_id, '02X')} was successful"

        # Update end-effector
        if self.ma_705_end_devices[0] not in self.device_update_disabled_list:
            # Try to select handle
            self.status_verbose_msg = f"Selecting device 0x{format(self.ma_705_end_devices[0], '02X')}"
            try:
                await self.bootloader.select(self.ma_705_end_devices[0])
            except BootloaderException as e:
                self.status_errors.append(f"Failed to select device 0x{format(self.ma_705_end_devices[0], '02X')}." + str(e))
                # TODO: Ask for user input at this point...
                return False

            # Try to erase handle firmware
            self.status_verbose_msg = "Erasing handle firmware"
            try:
                await self.bootloader.erase_sectors_firmware(start_address_705_end, size_705_end)
                await asyncio.sleep(0.2)
            except BootloaderException as e:
                self.status_errors.append(f"Could not erase firmware for 0x{format(self.ma_705_end_devices[0], '02X')}." + str(e))
                return False

            # Start firmware update
            self.status_verbose_msg = "Updating handle firmware"
            num_updates = (len(self.update_device_id_list) - len(self.device_update_disabled_list))
            rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
            progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng, self.ma_705_end_devices[0]))
            try:
                await firmware_writer.write_firmware_HEX(self.ma_705_end_firmware[0], retry_timeout=2.0)
                await asyncio.sleep(0.2)
            except Exception as e:
                self.status_errors.append(f"Firmware upload failed for 0x{format(self.ma_705_end_devices[0], '02X')}." + str(e))
                return False
            finally:
                progress_checker_task.cancel()

        self.status_persist_msg = f"{self._name} update complete."
        return True

    async def _request_power_cycle(self, attempts=30):
        self.status_verbose_msg = "Initialising update"
        self.dispatch('on_start_intervention_request', '[b]Please "unplug" and "re-plug" the Master Arm USB cable.')
        self.status_persist_msg = "[/b]"

        for i in range(attempts):
            try:
                if self.bootloader.connection.connected:
                    if await self.bootloader.start(timeout=0.8):
                        self.dispatch("on_stop_intervention_request")
                        break
            except BootloaderException:
                pass
            finally:
                self.status_msg = f"Update start attempt: ({i + 1}/{attempts})"
                await asyncio.sleep(0.8)
        else:
            self.status_errors.append(f"Update failed to start. Please try again.")
            return False
        return True

    async def progress_checker(self, firmware_writer: FirmwareWriter, progress_range: Tuple[float, float], device_id):
        while True:
            percentage = firmware_writer.write_progress/100
            progress = progress_range[0] + percentage * (progress_range[1] - progress_range[0])
            self.status_percentage = progress
            self.status_msg = f"Uploading Firmware to device 0x{format(device_id, '02X')}: {round(percentage * 100)}%"
            self.dispatch("on_child_status_percentage", (device_id, percentage * 100))
            await asyncio.sleep(0.05)
