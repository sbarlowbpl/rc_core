import os
import itertools
import asyncio
import paramiko

from enum import IntEnum
from typing import Tuple, List, Dict

from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.product import RSProduct
from RC_Core.enums import PacketID
from RC_Core.packetid import Packets
from RC_Core.update.bootloader.bootloader import BootloaderException
from RC_Core.devicemanager.product.product_bravo import ProductBravoSingleRotate
from RC_Core.devicemanager.product.product_bravo import ProductBravo7, ProductBravo5
from RC_Core.update.update_modules import update_defines
from RC_Core.update.update_modules.updatebase import UpdateBase
from RC_Core.connectionmanager import ConnectionManager


class Operation(IntEnum):
    """
    Operation
    BOOTLOADER_BATCH packet command list
    """
    STOP = 0
    WRITE_AND_GO = 1
    VERIFY = 2
    WRITE_AND_WAIT = 3
    GO = 4
    GO_AND_WAIT = 5


_StatusFlags = {
    0:      ['Preparing update', 'INIT'],
    255:    ['Successful', 'SUCCESS'],
    101:    ['Could not select. No response.', 'SELECT_NO_RESPONSE'],
    102:    ['Could not select. Received NACK.', 'SELECT_NACK_RESPONSE'],
    103:    ['Could not select. Received multiple responses.', 'SELECT_MULTIPLE_RESPONSE'],
    104:    ['Select response pending.', 'SELECT_RESPONSE_PENDING'],
    105:    ['Device selected successfully.', 'SELECT_SUCCESS'],
    131:    ['Could not erase software. No response.', 'ERASE_NO_RESPONSE'],
    132:    ['Could not erase software. Received NACK', 'ERASE_NACK_RESPONSE'],
    141:    ['Could not write software. No response.', 'WRITE_NO_RESPONSE'],
    142:    ['Could not write software. Received NACK.', 'WRITE_NACK_RESPONSE'],
    145:    ['Software write in progress', 'WRITE_IN_PROGRESS'],
    161:    ['Could not start software. No response.', 'GO_NO_RESPONSE'],
    162:    ['Could not start software. Received NACK.', 'GO_NACK_RESPONSE'],
    170:    ['Could not erase bootloader. No response.', 'START_NO_RESPONSE'],
    180:    ['Update could not be started. Firmware not found.', 'FIRMWARE_NOT_FOUND'],
}

StatusFlags = IntEnum(value='StatusFlags',
                      names=itertools.chain.from_iterable(
                          itertools.product(v, [k]) for k, v in _StatusFlags.items()))

StatusErrorFlags = [StatusFlags.SELECT_NO_RESPONSE, StatusFlags.SELECT_NACK_RESPONSE, StatusFlags.GO_NO_RESPONSE,
                    StatusFlags.SELECT_MULTIPLE_RESPONSE, StatusFlags.ERASE_NO_RESPONSE, StatusFlags.WRITE_NO_RESPONSE,
                    StatusFlags.ERASE_NACK_RESPONSE, StatusFlags.WRITE_NACK_RESPONSE, StatusFlags.GO_NACK_RESPONSE,
                    StatusFlags.START_NO_RESPONSE, StatusFlags.FIRMWARE_NOT_FOUND]

StatusUpdateFlags = [StatusFlags.INIT, StatusFlags.SUCCESS, StatusFlags.SELECT_SUCCESS,
                     StatusFlags.SELECT_RESPONSE_PENDING, StatusFlags.WRITE_IN_PROGRESS]


class UpdateBravo708703Udp(UpdateBase):
    _name: str = "Bravo base & joints - UDP"
    _attempts: int = 3

    PORT = 22
    USERNAME, PASSWORD = "bravo", "bravo"
    REMOTE_FOLDER = "/home/bravo/firmware/"

    def __init__(self, product: RSProduct, update_dict: Dict[str, Tuple[str, List[int]]], **kwargs):
        super(UpdateBravo708703Udp, self).__init__(product, update_dict)
        self.product = product
        self.connection: CommConnection = product.connection

        self.update_device_id_list = list()
        self.rb_708_device = list()
        self.rb_703_devices = list()
        self.rb_703_path = str()
        self.rb_708_path = str()

        # Check files are available
        if update_defines.RS2_708_STR not in update_dict or update_defines.RS2_703_STR not in update_dict:
            self.status_errors.append("Could not detect all update files. Please try another folder.")
            return

        # Check device is compatible
        if not isinstance(self.product, ProductBravo7) and not isinstance(self.product, ProductBravo5):
            self.status_errors.append("UDP connection not supported. Please use a serial connection.")
            return

        # List of all device ID's being updated
        self.update_device_id_list = list()

        # Get the 708 update and devices
        self.rb_708_device: List[int] = update_dict[update_defines.RS2_708_STR][1]
        self.rb_708_path: str = update_dict[update_defines.RS2_708_STR][0]
        self.update_device_id_list.extend(self.rb_708_device)

        # Get the 703 update and devices
        self.rb_703_devices: List[int] = update_dict[update_defines.RS2_703_STR][1]
        self.rb_703_path: str = update_dict[update_defines.RS2_703_STR][0]
        self.update_device_id_list.extend(self.rb_703_devices)

        # Extract the board and version numbers from file name
        self.batch_pkt_703: List[int] = list()
        self.batch_pkt_708: List[int] = list()
        self.generate_bootloader_batch_packets()

    def generate_bootloader_batch_packets(self):
        ver_str = self.get_version_from_file(self.rb_703_path)
        self.batch_pkt_703 = [7, 0, 3, int(ver_str[0]), int(ver_str[1]), int(ver_str[2]),
                              self.rb_703_devices[0], self.rb_703_devices[-1], 0, 0]

        ver_str = self.get_version_from_file(self.rb_708_path)
        self.batch_pkt_708 = [7, 0, 8, int(ver_str[0]), int(ver_str[1]), int(ver_str[2]),
                              self.rb_708_device[0], self.rb_708_device[-1], 0, 0]

    @staticmethod
    def get_version_from_file(file_path):
        file_name = os.path.basename(file_path).split('.')[0]
        version_str = file_name.lower().split('v')[-1]
        return version_str.split('_')

    @property
    def name(self):
        return self._name

    async def update(self) -> bool:
        if not self.update_enabled:
            self.status_persist_msg = f"Skipping update: {self._name}"
            return True

        self.status_persist_msg = f"Starting base and joints update"

        ConnectionManager.get_instance().run_connection_loop = False
        update_successful = await asyncio.create_task(self._upload_firmware())
        ConnectionManager.get_instance().run_connection_loop = True
        return update_successful

    async def _upload_firmware(self):
        # Attempt to push firmware .hex file to bravo base device
        try:
            transport = paramiko.Transport((self.product.connection.socket_ip, self.PORT))
            transport.connect(None, self.USERNAME, self.PASSWORD)
            sftp = paramiko.SFTPClient.from_transport(transport)
            self.status_msg = f"Uploading {os.path.basename(self.rb_703_path)} to Bravo."
            sftp.put(self.rb_703_path, self.REMOTE_FOLDER + os.path.basename(self.rb_703_path))
            self.status_msg = f"Uploading {os.path.basename(self.rb_708_path)} to Bravo."
            sftp.put(self.rb_708_path, self.REMOTE_FOLDER + os.path.basename(self.rb_708_path))
            self.status_persist_msg = f"Successfully uploaded software to base computer"

            if transport and sftp:
                sftp.close()
                transport.close()

        except (paramiko.ssh_exception.SSHException, EOFError) as e:
            self.status_errors.append(f"Firmware upload error. {e}")
            return False
        except IOError as e:
            self.status_errors.append(f"Not enough disk space on computer. {e}")
            return False

        # Add warning to status
        self.status_warnings.append('Starting update. Do not turn off your device.')

        # Update RS2-708
        self.status_verbose_msg = f"Starting RS2-708 firmware update."
        self.batch_pkt_708[Packets.BOOTLOADER_BATCH.data_names.index('operation')] = Operation.WRITE_AND_WAIT
        self.product.connection.send(self.product.get_base_device_id(), PacketID.BOOTLOADER_BATCH, self.batch_pkt_708)
        await asyncio.sleep(2.0)
        try:
            await self._wait_for_update(self.rb_708_device)
        except Exception as e:
            self.status_errors.append(f"RS2-708 firmware update failed. {e}")
            return False

        # Update RS2-703
        self.status_verbose_msg = f"Starting RS2-703 firmware update"
        self.batch_pkt_703[Packets.BOOTLOADER_BATCH.data_names.index('operation')] = Operation.WRITE_AND_WAIT
        self.product.connection.send(self.product.get_base_device_id(), PacketID.BOOTLOADER_BATCH, self.batch_pkt_703)
        await asyncio.sleep(2.0)
        try:
            await self._wait_for_update(self.rb_703_devices)
        except Exception as e:
            self.status_errors.append(f"RS2-703 firmware update failed. {e}")
            return False

        # Start RS-703 devices
        self.batch_pkt_703[Packets.BOOTLOADER_BATCH.data_names.index('operation')] = Operation.GO_AND_WAIT
        self.product.connection.send(self.product.get_base_device_id(), PacketID.BOOTLOADER_BATCH, self.batch_pkt_703)
        for i in range(35, 0, -1):
            self.status_msg = f"Starting joint software in {i}[sec]"
            await asyncio.sleep(1)

        # Start RS2-708 device
        self.batch_pkt_708[Packets.BOOTLOADER_BATCH.data_names.index('operation')] = Operation.GO
        self.product.connection.send(self.product.get_base_device_id(), PacketID.BOOTLOADER_BATCH, self.batch_pkt_708)
        for i in range(5, 0, -1):
            self.status_msg = f"Starting base software in {i}[sec]"
            await asyncio.sleep(1)

        # Finalise update
        self.status_persist_msg = f"{self._name} update complete."
        return True

    async def _wait_for_update(self, device_list, attempts=100):
        # Wait for update to complete
        self.update_in_progress = True
        while self.update_in_progress:
            for attempt in range(attempts):
                try:
                    await self._handle_batch_report(device_list)
                    break
                except TimeoutError as e:
                    print(__name__, "_wait_for_update()", "attempt=", attempt)
                    if attempt == attempts-1:
                        raise e
                except BootloaderException as e:
                    print(__name__, "_wait_for_update()", f"BootloaderException: {e}")
                    raise e
            await asyncio.sleep(0.1)
        return True

    async def _handle_batch_report(self, device_list):
        try:
            data = await self.product.connection.request(self.product.get_base_device_id(), PacketID.BOOT_BATCH_REPORT, timeout=0.06)
        except TimeoutError:
            raise TimeoutError("Request timed out. Do not power cycle for 5 minutes. Update may continue on arm!")

        current_device = device_list[0]
        for i, device_id in enumerate(device_list):
            upload_status = data[i]

            # Uploading firmware to device
            idx = self.update_device_id_list.index(device_id)
            if 0 < upload_status <= 100:
                self.status_msg = f"Uploading Firmware to device 0x{format(device_id, '02X')}: {round(upload_status)}%"
                self.status_percentage = int(100 / len(self.update_device_id_list) * (idx + upload_status / 100))
                self.dispatch("on_child_status_percentage", (device_id, upload_status))
                break
            elif upload_status > 100 and upload_status != StatusFlags.SELECT_RESPONSE_PENDING:
                self.dispatch("on_child_status_percentage", (device_id, 100))
                self.status_percentage = max(self.status_percentage, int(100/len(self.update_device_id_list)*(idx/100)))
                if device_id is device_list[-1]:
                    self.update_in_progress = False
                else:
                    current_device = device_list[i + 1]

            # Raise errors from status flag
            elif upload_status in StatusErrorFlags:
                raise BootloaderException(f"Device 0x{format(device_id, '02X')}, {StatusFlags(upload_status).name}")

            # Update status from status flag
            elif device_id == current_device and upload_status in StatusUpdateFlags:
                self.status_msg = f"Device 0x{format(device_id, '02X')}: {StatusFlags(upload_status).name}"
