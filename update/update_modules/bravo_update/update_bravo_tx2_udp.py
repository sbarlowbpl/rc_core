import functools
import os

import asyncio
import platform
import socket
import zipfile

import paramiko
from typing import Tuple, List, Dict

from RC_Core.comm_type import CommType
from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.product import RSProduct
from RC_Core.update.bootloader.bootloader import Bootloader
from RC_Core.update.update_modules import update_defines
from RC_Core.update.update_modules.updatebase import UpdateBase

if platform.system() == 'Linux':
    local_app_data_path = os.getenv('HOME')
    BPL_DATA_PATH = os.path.join(local_app_data_path, '.reachcontrol')
elif platform.system() == 'Windows':
    local_app_data_path = os.getenv('LOCALAPPDATA')
    BPL_DATA_PATH = os.path.join(local_app_data_path, 'ReachControl')

USERNAME = 'bravo'
PASSWORD = 'bravo'
PORT = 22


class UpdateBravoTX2Udp(UpdateBase):
    _name: str = "Bravo computer - UDP"

    def __init__(self, product: RSProduct, update_dict: Dict[str, Tuple[str, List[int]]], **kwargs):
        super(UpdateBravoTX2Udp, self).__init__(product, update_dict)
        self.product = product

        # Check files are available
        if update_defines.RS2_TX2_STR not in update_dict:
            self.status_errors.append("Could not detect all update files. Please try another folder.")
            return

        # List of all device ID's being updated
        self.tx2_software_path = update_dict[update_defines.RS2_TX2_STR][0]
        self.tx2_device_id = update_dict[update_defines.RS2_TX2_STR][1][0]
        self.update_device_id_list: List[int] = [self.tx2_device_id]

        # Get the connection and create a bootloader object
        self.connection: CommConnection = product.connection
        self.bootloader = Bootloader(self.connection)

    @property
    def name(self):
        return self._name

    async def update(self):
        # Check zip file contents:
        try:
            self.status_msg = f"Viewing Contents of TX2 Software"
            zf = zipfile.ZipFile(self.tx2_software_path, 'r')
        except BaseException as e:
            self.status_errors.append(f"Unable to open TX2 Software")
            return

        namelist = zf.namelist()

        has_image = False
        if 'image.tar.gz' in namelist:
            self.status_msg = "Found image in TX2 Software"
            has_image = True
        else:
            has_image = False

        # Check product connection
        if self.product is None or self.product.connection is None:
            # TODO: Error
            return False

        # Invalid connection type
        if self.product.connection.type != CommType.UDP:
            # TODO: Error
            return False

        ip_address = self.product.connection.socket_ip

        self.status_msg = "Establishing SSH Connection."
        self.status_percentage = 5
        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            loop = asyncio.get_event_loop()
            future1 = loop.run_in_executor(None, functools.partial(ssh.connect, ip_address, PORT, USERNAME, PASSWORD, timeout=10))
            await future1
        except socket.error as e:
            self.status_errors.append(f"Enable to establish ssh connection to {ip_address}")
            return

        # Now we ar connected
        self.status_msg = "SSH Connection Established"
        self.status_percentage = 10
        try:
            required_image_version = zf.read('image_requirement.txt')
            required_image_version = required_image_version.decode("utf-8")
        except BaseException:
            self.status_errors.append(f"Unable to read image_requirement.txt from zip file")
            return False

        if not has_image:
            # Check the image if it not included in the TX2
            self.status_percentage = 15
            if not await self.check_image_version(required_image_version, ssh):
                # TODO: Error
                return False

            # Apply update
            self.status_percentage = 20
            if not await self.update_software(ssh, zf, ip_address):
                # TODO: Error
                return False
        else:
            # If it has the image update the image to the TX2.
            self.status_percentage = 20
            if not await self.update_software_and_image(ip_address, ssh, required_image_version):
                # TODO: Error
                return False

        # Correct image version is found.
        self.status_percentage = 100
        self.status_msg = "Successfully updated TX2"
        return True

    async def check_image_version(self, required_image_version, ssh):
        self.status_msg = "Checking TX2 Image"

        stdin, stdout, stderr = ssh.exec_command(
            f'''docker inspect "{required_image_version}" > /dev/null 2>&1 || echo "-1"''')

        if stdout.readlines():
            self.status_errors.append(
                f"Image Version: {required_image_version} not found. You require the Software and Image installation")
            return False
        else:
            self.status_msg = "Found Image on TX2"
            return True

    async def update_software_and_image(self, ip_address, ssh, required_image_version):
        transport = paramiko.Transport((ip_address, PORT))
        transport.connect(None, USERNAME, PASSWORD)

        sftp = paramiko.SFTPClient.from_transport(transport)

        remote_folder = "/home/bravo/"

        sftp.put(self.tx2_software_path, remote_folder + "software_with_image.zip",
                 self.transfer_callback(20, 50))
        sftp.close()
        transport.close()
        await self.run_command_remote(ssh, "unzip -o software_with_image.zip && rm image_requirement.txt software_with_image.zip")

        await self.run_command_remote(ssh, "mkdir .temp_software && cd .temp_software && unzip ../software.zip")

        await self.run_command_remote(ssh, f"docker image rm {required_image_version}")
        await self.run_command_remote("docker load < image.tar.gz")

        await self.run_command_remote(ssh, "rm image.tar.gz")

    async def update_software(self, ssh, zf, ip_address):
        remote_folder = "/home/bravo/"
        zf.extract("software.zip", BPL_DATA_PATH)
        zf.extract("install_script.sh", BPL_DATA_PATH)
        transport = paramiko.Transport((ip_address, PORT))
        transport.connect(None, USERNAME, PASSWORD)

        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp.put(os.path.join(BPL_DATA_PATH, "install_script.sh"), remote_folder + "install_script.sh")
        sftp.put(os.path.join(BPL_DATA_PATH, "software.zip"), remote_folder + "software.zip",
                 self.transfer_callback(20, 50))

        sftp.close()
        transport.close()
        os.remove(os.path.join(BPL_DATA_PATH, "software.zip"))
        os.remove(os.path.join(BPL_DATA_PATH, "install_script.sh"))

        await self.run_command_remote(ssh, "chmod +x ./install_script.sh")
        await self.run_command_remote(ssh, "./install_script.sh")
        await self.run_command_remote(ssh, "rm ./install_script.sh")

        return True

    async def run_command_remote(self, ssh, command):
        channel = ssh.get_transport().open_session()
        stdout = channel.makefile('rb', -1)
        channel.exec_command(command)
        while not channel.exit_status_ready():
            status = format(stdout.readline().decode('UTF-8'))
            print(status)
            self.status_msg = status
            if self.status_percentage < 95:
                self.status_percentage += 0.5
            await asyncio.sleep(0.001)
        channel.close()

    def transfer_callback(self, start_progress, end_progress):
        def _transfer_callback(transferred, total):
            # Calculate progress
            self.status_percentage = start_progress + (((transferred / total) * 100) / (start_progress + end_progress))
            self.status_msg = f"Transferred {transferred, total}"

        return _transfer_callback

    def on_status_percentage(self, inst, data):
        self.dispatch("on_child_status_percentage", (self.tx2_device_id, data))
