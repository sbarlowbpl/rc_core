import asyncio
from typing import Tuple, List, Dict

from RC_Core.commconnection import CommConnection
from RC_Core.devicemanager.product import RSProduct
from RC_Core.update.bootloader.bootloader import HEX_Parse, Bootloader, BootloaderException, FirmwareWriter
from RC_Core.devicemanager.product.product_bravo import ProductBravoSingleRotate
from RC_Core.update.update_modules.updatebase import UpdateBase
from RC_Core.connectionmanager import ConnectionManager
from RC_Core.update.update_modules import update_defines


def check_hex(firmware):
    """
    Check a hex file for start, end address and size
    :param firmware:
    :return:
    """
    start_address = 0
    end_address = 0
    hex_parser = HEX_Parse()
    lines = len(firmware)
    start = False

    print("Check HEX file in progress(0%)", end='')

    for i in range(lines):
        try:
            state, address, data = hex_parser.parse_line(firmware[i])
        except Exception as e:
            raise
        print("\r" + "Check HEX file in progress({}%)".format(((i * 100) / lines) // 1), end='')
        if start is False and state == hex_parser.PARSE_STATE['DATA']:
            start_address = address
            start = True
        if state == hex_parser.PARSE_STATE['DATA']:
            end_address = address + len(data)

    # 54216 - bin
    size = end_address - start_address
    print("\r" + "Check HEX file in progress(100%) ")
    return start_address, end_address, size


class UpdateBravo708703Serial(UpdateBase):
    _name: str = "Bravo base & joints - Serial"
    _attempts: int = 3

    def __init__(self, product: RSProduct, update_dict: Dict[str, Tuple[str, List[int]]], **kwargs):
        super(UpdateBravo708703Serial, self).__init__(product, update_dict)
        self.product = product

        # List of all device ID's being updated
        self.update_device_id_list = list()

        # Check files are available
        if update_defines.RS2_708_STR not in update_dict or update_defines.RS2_703_STR not in update_dict:
            self.status_errors.append("Could not detect all update files. Please try another folder.")
            return

        if not isinstance(self.product, ProductBravoSingleRotate):
            # Get the 708 update and devices
            self.rb_708_firmware = Bootloader.open_file_direct(update_dict[update_defines.RS2_708_STR][0])
            self.rb_708_device: List[int] = update_dict[update_defines.RS2_708_STR][1]
            self.update_device_id_list.extend(self.rb_708_device)
        else:
            self.status_warnings.append("This product only supports a direct RS2-485 connection")

        # Get the 703 update and devices
        self.rb_703_firmware = Bootloader.open_file_direct(update_dict[update_defines.RS2_703_STR][0])
        self.rb_703_devices: List[int] = update_dict[update_defines.RS2_703_STR][1]
        self.update_device_id_list.extend(self.rb_703_devices)

        # Get the connection and create a bootloader object
        self.connection: CommConnection = product.connection
        self.bootloader = Bootloader(self.connection)

    @property
    def name(self):
        return self._name

    async def update(self) -> bool:
        if not self.update_enabled:
            self.status_persist_msg = f"Skipping update: {self._name}"
            return True

        ConnectionManager.get_instance().run_connection_loop = False
        if isinstance(self.product, ProductBravoSingleRotate):
            update_status = await self._update_single_rotate()
        else:
            update_status = await self._update_708703()
        ConnectionManager.get_instance().run_connection_loop = True

        return update_status

    async def _update_single_rotate(self) -> bool:
        # Check the hex files
        self.status_msg = "Checking hex file"
        try:
            check_hex(self.rb_703_firmware[0])
        except Exception as e:
            self.status_errors.append("invalid Hex file." + str(e))
            return False

        # Request user to power cycle
        if not await self._request_power_cycle():
            return False

        # Add warning to status
        self.status_warnings.append('Starting update. Do not turn off your device.')

        if self.rb_703_devices[0] not in self.device_update_disabled_list:
            # Try to erase RS2-703
            self.status_verbose_msg = "Erasing RS2-703 Firmware"
            try:
                start_address, end_address, size = check_hex(self.rb_703_firmware[0])
                await self.bootloader.erase_sectors_firmware(start_address, size)
                await asyncio.sleep(0.2)
            except BootloaderException as e:
                self.status_errors.append(f"Could not erase firmware for 0x{format(self.rb_703_devices[0],'02X')}." + str(e))
                return False

            # Get firmware writer object
            firmware_writer = FirmwareWriter(self.bootloader)

            # Start firmware update
            num_updates = (len(self.update_device_id_list) - len(self.device_update_disabled_list))
            rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
            progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng, self.rb_703_devices[0]))
            try:
                self.status_verbose_msg = "Updating RS2-703 Firmware"
                await firmware_writer.write_firmware_HEX(self.rb_703_firmware[0], retry_timeout=2.0)
            except Exception as e:
                self.status_errors.append(f"Update failed for 0x{format(self.rb_703_devices[0],'02X')}." + str(e))
                # TODO: Ask for user input at this point...
            progress_checker_task.cancel()

        self.status_persist_msg = f"{self._name} update complete."
        return True

    async def _update_708703(self) -> bool:
        # Check the hex files
        self.status_msg = "Checking hex files"
        try:
            start_address_703, end_address_703, size_703 = check_hex(self.rb_703_firmware[0])
            start_address_708, end_address_708, size_708 = check_hex(self.rb_708_firmware[0])
        except Exception as e:
            self.status_errors.append("invalid Hex file." + str(e))
            return False

        # Get firmware writer object
        firmware_writer = FirmwareWriter(self.bootloader)

        # Request user to power cycle
        if not await self._request_power_cycle():
            return False

        # Add warning to status
        self.status_warnings.append('Starting update. Do not turn off your device.')

        # Upload firmware to base
        if self.rb_708_device[0] not in self.device_update_disabled_list:
            # Try to erase RS2-708
            self.status_verbose_msg = "Erasing base Firmware"
            try:
                await self.bootloader.erase_sectors_firmware(start_address_708, size_708)
                await asyncio.sleep(0.2)
            except BootloaderException as e:
                self.status_errors.append(f"Could not erase firmware for 0x{format(self.rb_708_device[0],'02X')}." + str(e))
                return False

            # Start firmware update
            self.status_verbose_msg = "Updating base Firmware"
            num_updates = (len(self.update_device_id_list) - len(self.device_update_disabled_list))
            rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
            progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng, self.rb_708_device[0]))
            try:
                await firmware_writer.write_firmware_HEX(self.rb_708_firmware[0], retry_timeout=2.0)
                await asyncio.sleep(0.2)
            except Exception as e:
                self.status_errors.append(f"Firmware upload failed for 0x{format(self.rb_708_device[0], '02X')}." + str(e))
                return False
            finally:
                progress_checker_task.cancel()

        # Start pass-through
        self.status_verbose_msg = f"Starting pass-though to joints"
        for attempt in range(self._attempts):
            try:
                await self.bootloader.passthrough()
                await asyncio.sleep(0.2)
                break
            except BootloaderException as e:
                if attempt == self._attempts-1:
                    self.status_errors.append("Failed to establish pass-though." + str(e))
                    return False

        # Upload firmware to all joints
        for i, device_id in enumerate(self.rb_703_devices):
            # Check if device should be skipped
            if device_id in self.device_update_disabled_list:
                self.status_verbose_msg = f"Skipping device 0x{format(device_id, '02X')}"
                continue

            # Try to select device
            self.status_verbose_msg = f"Selecting device 0x{format(device_id, '02X')}"
            try:
                await self.bootloader.select(device_id)
            except BootloaderException as e:
                self.status_errors.append(f"Failed to select device 0x{format(device_id, '02X')}." + str(e))
                # TODO: Ask for user input at this point...
                continue

            # Try to erase firmware
            self.status_verbose_msg = f"Erasing firmware for device 0x{format(device_id, '02X')}"
            try:
                await self.bootloader.erase_sectors_firmware(start_address_703, size_703)
            except BootloaderException as e:
                self.status_errors.append(f"Failed to erase firmware for 0x{format(device_id, '02X')}." + str(e))
                # TODO: Ask for user input at this point...
                continue

            # Try write firmware to selected device
            self.status_verbose_msg = f"Writing firmware for device 0x{format(device_id, '02X')}"
            num_updates = (len(self.update_device_id_list) - len(self.device_update_disabled_list))
            rng = (self.status_percentage, self.status_percentage + 100 / num_updates)
            progress_checker_task = asyncio.create_task(self.progress_checker(firmware_writer, rng, device_id))
            try:
                await firmware_writer.write_firmware_HEX(self.rb_703_firmware[0], retry_timeout=2.0)
            except Exception as e:
                self.status_errors.append(f"Firmware upload failed for 0x{format(device_id, '02X')}." + str(e))
                return False
            finally:
                progress_checker_task.cancel()

            # Upload was successful
            self.status_verbose_msg = f"Firmware update for device 0x{format(device_id, '02X')} was successful"

        self.status_persist_msg = f"Starting software..."

        # Try to start RS2-703 application firmware
        for i, device_id in enumerate(self.rb_703_devices):
            self.status_verbose_msg = f"Starting software for device 0x{format(device_id, '02X')}"
            for attempt in range(self._attempts):
                try:
                    await self.bootloader.select(device_id)
                    await self.bootloader.go(start_address_703)
                    break
                except BootloaderException as e:
                    await asyncio.sleep(0.5)
                    if attempt == self._attempts-1:
                        self.status_warnings.append(f"Failed to start device 0x{format(device_id, '02X')}." + str(e))

        # Stop pass-through
        self.status_verbose_msg = f"Stopping pass-though to joints"
        try:
            await self.bootloader.stop_passthrough()
        except BootloaderException as e:
            self.status_errors.append("Failed to stop pass-though." + str(e))
            return False

        # Try to start RS2-708 application firmware
        self.status_verbose_msg = f"Starting software for device 0x{format(self.rb_708_device[0], '02X')}"
        for attempt in range(self._attempts):
            try:
                await self.bootloader.go(start_address_708)
                break
            except BootloaderException as e:
                await asyncio.sleep(0.5)
                if attempt == self._attempts-1:
                    self.status_warnings.append(f"Failed to start device 0x{format(self.rb_708_device[0], '02X')}." + str(e))

        self.status_persist_msg = f"{self._name} update complete."
        return True

    async def _request_power_cycle(self, attempts=10):
        self.status_verbose_msg = "Initialising update"
        self.dispatch('on_start_intervention_request', '[b]Please "power-cycle" now, or wait for update "auto-start".')
        self.status_persist_msg = "[/b]"

        for i in range(attempts):
            try:
                if await self.bootloader.start(timeout=0.2):
                    self.dispatch("on_stop_intervention_request")
                    break
            except BootloaderException:
                self.status_msg = f"Update auto-start in: {attempts - i}    [sec]"

            await asyncio.sleep(0.98)
        else:
            self.bootloader.connection.send(0xFF, 0xFF, b'0')
            await asyncio.sleep(0.2)

            try:
                if await self.bootloader.start(timeout=0.2):
                    self.dispatch("on_stop_intervention_request")
            except BootloaderException:
                self.status_errors.append(f"Update auto-start failed. This feature may not be compatible.")
                return False
        return True

    async def progress_checker(self, firmware_writer: FirmwareWriter, progress_range: Tuple[float, float], device_id):
        while True:
            percentage = firmware_writer.write_progress/100
            progress = progress_range[0] + percentage * (progress_range[1] - progress_range[0])
            self.status_percentage = progress
            self.status_msg = f"Uploading Firmware to device 0x{format(device_id, '02X')}: {round(percentage * 100)}%"
            self.dispatch("on_child_status_percentage", (device_id, percentage * 100))
            await asyncio.sleep(0.05)

