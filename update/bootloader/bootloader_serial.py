import sys
import time
import glob
import serial
import serial.tools.list_ports
import logging


class BootloaderSerial(object):
    class BootloaderSerialException(Warning):
        pass

    writeTimeoutError = BootloaderSerialException('serial write timeout')
    readTimeoutError = BootloaderSerialException('serial read timeout')

    """Abstract class that implements basically physical layer with rs232 or serial connection
    """
    def __init__(self, log_level = logging.WARNING):
        #logging.basicConfig(format = u'%(levelname)-8s: Bootloader Serial Port: %(message)s', level = log_level)
        self.log = logging.getLogger("Bootloader Serial Port")
        self.log.level = log_level
        self.serial = serial.Serial()


    def open_port(self, port, timeout = 0):
        """Open serial port"""
        try:
            self.serial.port = port
            self.serial.baudrate = 115200
            self.serial.bytesize = serial.EIGHTBITS
            self.serial.parity = serial.PARITY_NONE
            self.serial.stopbits = serial.STOPBITS_ONE
            self.serial.timeout = timeout
            self.serial.open()
        except Exception as e:
            self.error(e)
            raise self.BootloaderSerialException("Error Open Port: " + port)
        self.serial_settings_backup = self.serial.get_settings()

    def close(self):
        """Close serial port"""
        if (self.serial.isOpen()):
            self.serial.close()
            self.log.warning('serial closed')


    def send_data(self, data):
        """Send bytes"""
        self.log.debug(" send data: " + "".join('{:02X} '.format(b) for b in data))
        try:
            self.serial.write(data)
        except serial.SerialTimeoutException:
            raise self.writeTimeoutError


    def receive_data(self, size):
        """ Read up to num bytes (timeout)
        :returns: the raw data
        """
        if (size == 0):
            raise self.BootloaderSerialException("Error Receive Data: size = 0")

        data = self.serial.read(size)
        if (len(data) == 0):
            self.log.debug(" receive data: " + "could not receive data")
            raise self.readTimeoutError
        elif (len(data) != size):
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
            raise self.readTimeoutError
        else:
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
        return data


    def find_port(self):
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result


    def error(self, e):
        """Serial port error"""
        #self.log.error("{}".format(e))
        self.close()


