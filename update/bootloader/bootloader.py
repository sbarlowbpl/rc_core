import asyncio
import multiprocessing
import time
from multiprocessing.connection import Connection
from typing import List

import serial
from array import *

from RC_Core.commconnection import CommConnection, ftdidef
from RC_Core.connections.serial_ftdi_connection import SerialFTDIConnection
from RC_Core.update.bootloader.async_bootloader import FTDIConnectionWrapper, HEX_Parse, BootloaderStatus

from RC_Core.update.bootloader.nonasyncbootloader import NonAsyncBootloader as NonAsyncBootloader

SECTORS = {
    'Sector 0': {'Address': 0x08000000, 'Size': 16 * 1024},  # Sector 0, 16 Kbytes
    'Sector 1': {'Address': 0x08004000, 'Size': 16 * 1024},  # Sector 1, 16 Kbytes
    'Sector 2': {'Address': 0x08008000, 'Size': 16 * 1024},  # Sector 2, 16 Kbytes
    'Sector 3': {'Address': 0x0800C000, 'Size': 16 * 1024},  # Sector 3, 16 Kbytes
    'Sector 4': {'Address': 0x08010000, 'Size': 64 * 1024},  # Sector 4, 64 Kbytes
    'Sector 5': {'Address': 0x08020000, 'Size': 128 * 1024},  # Sector 5, 128 Kbytes
    'Sector 6': {'Address': 0x08040000, 'Size': 128 * 1024},  # Sector 6, 128 Kbytes
    'Sector 7': {'Address': 0x08060000, 'Size': 128 * 1024},  # Sector 7, 128 Kbytes
    'Sector 8': {'Address': 0x08080000, 'Size': 128 * 1024},  # Sector 8, 128 Kbytes
    'Sector 9': {'Address': 0x080A0000, 'Size': 128 * 1024},  # Sector 9, 128 Kbytes
    'Sector 10': {'Address': 0x080C0000, 'Size': 128 * 1024},  # Sector 10, 128 Kbytes
    'Sector 11': {'Address': 0x080E0000, 'Size': 128 * 1024},  # Sector 11, 128 Kbytes
}

OTP_BLOCKS = {
    'Block 0': {'Address': 0x1FFF7800},  # OTP Block 0, 32 bytes
    'Block 1': {'Address': 0x1FFF7820},  # OTP Block 1, 32 bytes
    'Block 2': {'Address': 0x1FFF7840},  # OTP Block 2, 32 bytes
    'Block 3': {'Address': 0x1FFF7860},  # OTP Block 3, 32 bytes
    'Block 4': {'Address': 0x1FFF7880},  # OTP Block 4, 32 bytes
    'Block 5': {'Address': 0x1FFF78A0},  # OTP Block 5, 32 bytes
    'Block 6': {'Address': 0x1FFF78C0},  # OTP Block 6, 32 bytes
    'Block 7': {'Address': 0x1FFF78E0},  # OTP Block 7, 32 bytes
    'Block 8': {'Address': 0x1FFF7900},  # OTP Block 8, 32 bytes
    'Block 9': {'Address': 0x1FFF7920},  # OTP Block 9, 32 bytes
    'Block 10': {'Address': 0x1FFF7940},  # OTP Block 10, 32 bytes
    'Block 11': {'Address': 0x1FFF7960},  # OTP Block 11, 32 bytes
    'Block 12': {'Address': 0x1FFF7980},  # OTP Block 12, 32 bytes
    'Block 13': {'Address': 0x1FFF79A0},  # OTP Block 13, 32 bytes
    'Block 14': {'Address': 0x1FFF79C0},  # OTP Block 14, 32 bytes
    'Block 15': {'Address': 0x1FFF79E0},  # OTP Block 15, 32 bytes
    'Lock 0': {'Address': 0x1FFF7A00},  # OTP Lock byte, 1 byte
    'Lock 1': {'Address': 0x1FFF7A04},  # OTP Lock byte, 1 byte
    'Lock 2': {'Address': 0x1FFF7A08},  # OTP Lock byte, 1 byte
    'Lock 3': {'Address': 0x1FFF7A0C},  # OTP Lock byte, 1 byte
    'Lock 4': {'Address': 0x1FFF7A10},  # OTP Lock byte, 1 byte
    'Lock 5': {'Address': 0x1FFF7A14},  # OTP Lock byte, 1 byte
    'Lock 6': {'Address': 0x1FFF7A18},  # OTP Lock byte, 1 byte
    'Lock 7': {'Address': 0x1FFF7A1C},  # OTP Lock byte, 1 byte
    'Lock 8': {'Address': 0x1FFF7A20},  # OTP Lock byte, 1 byte
    'Lock 9': {'Address': 0x1FFF7A24},  # OTP Lock byte, 1 byte
    'Lock 10': {'Address': 0x1FFF7A28},  # OTP Lock byte, 1 byte
    'Lock 11': {'Address': 0x1FFF7A2C},  # OTP Lock byte, 1 byte
    'Lock 12': {'Address': 0x1FFF7A30},  # OTP Lock byte, 1 byte
    'Lock 13': {'Address': 0x1FFF7A34},  # OTP Lock byte, 1 byte
    'Lock 14': {'Address': 0x1FFF7A38},  # OTP Lock byte, 1 byte
    'Lock 15': {'Address': 0x1FFF7A3C},  # OTP Lock byte, 1 byte
}

class ID_COMMAND(object):
    GET = bytes([0x00, 0xFF])
    GET_VERSION = bytes([0x00, 0xFE])
    GET_ID = bytes([0x02, 0xFD])
    READ_MEMORY = bytes([0x11, 0xEE])
    GO = bytes([0x21, 0xDE])
    WRITE_MEMORY = bytes([0x31, 0xCE])
    ERASE_MEMORY = bytes([0x44, 0xBB])
    EXTENDED_ERASE_MEMORY = bytes([0x44, 0xBB])
    WRITE_PROTECT = bytes([0x63, 0x9C])
    WRITE_UNPROTECT = bytes([0x73, 0x8C])
    READOUT_PROTECT = bytes([0x82, 0x7D])
    READOUT_UNPROTECT = bytes([0x92, 0x6D])
    PASSTHROUGH = bytes([0xA0, 0x5F])
    STOP_PASSTHROUGH = bytes([0xA1, 0x5E, 0xA1, 0x5E, 0xA1, 0x5E, 0xA1, 0x5E])
    SELECT = bytes([0xB3, 0x4C])


class BootloaderException(BaseException):
    pass


class BootloaderTimeoutException(BootloaderException):
    pass


class BootloaderNACKException(BootloaderException):
    pass


class BootloaderUnexpectedResponseException(BootloaderException):
    pass


class BootloaderMultipleACKException(BootloaderException):
    pass


class CODE_COMMAND(object):
    GET = b'\x00'  # Gets the version and the allowed commands supported by the current version of the bootloader
    GET_VERSION = b'\x01'  # Gets the bootloader version and the Read Protection status of the Flash memory
    GET_ID = b'\x02'  # Gets the chip ID
    READ_MEMORY = b'\x11'  # Reads up to 256 bytes of memory starting from an address specified by the application
    GO = b'\x21'  # Jumps to user application code located in the internal Flash memory or in SRAM
    WRITE_MEMORY = b'\x31'  # Writes up to 256 bytes to the RAM or Flash memory starting from an address specified by the application
    ERASE = b'\x43'  # Erases from one to all the Flash memory pages
    EXTENDED_ERASE = b'\x44'  # Erases from one to all the Flash memory pages using two byte addressing mode (available only for v3.0 usart bootloader versions and above).
    WRITE_PROTECT = b'\x63'  # Enables the write protection for some sectors
    WRITE_UNPROTECT = b'\x73'  # Disables the write protection for all Flash memory sectors
    READOUT_PROTECT = b'\x82'  # Enables the read protection
    READOUT_UNPROTECT = b'\x92'  # Disables the read protection
    XOR = b'\x00'  #
    ACK = b'\x79'  # ACK answer
    NACK = b'\x1F'  # NACK answer
    START = b'\x7F'


class Bootloader:

    connection: CommConnection

    def __init__(self, connection: CommConnection):

        self.connection = connection

    @staticmethod
    def checksum(data: bytes):
        result = 0
        for byte in data:
            result ^= byte
        return bytes([result])

    @staticmethod
    def open_file_direct(filepath=None):
        if filepath is None or filepath == '':
            return None, None
        try:
            format = filepath.split(".")[1]
            file = open(filepath, "rb")  # opening for reading as binary
            if format == "bin":
                data = file.read()
            else:
                data = file.readlines()
            file.close()
            return data, format
        except:
            return None, None

    async def receive_data(self, size, timeout=2.0):

        data = b''
        start_time = time.perf_counter()

        while time.perf_counter() - start_time < timeout:
            response = self.connection.read_bytes()
            if response:
                data += response

                if len(data) >= size:
                    return data[:size]
            await asyncio.sleep(0.0001)
        raise BootloaderTimeoutException(f"Did not receive data")
    
    async def receive_ack(self, timeout=0.5):
        start_time = time.perf_counter()

        while time.perf_counter() - start_time < timeout:
            response = self.connection.read_bytes(num_bytes=1)

            if response:
                if len(response) > 1:
                    # Error, unexpected length
                    raise BootloaderUnexpectedResponseException(f"Received unexpected response {response}")

                if response == CODE_COMMAND.NACK:
                    raise BootloaderNACKException(f"Received NACK Response")

                if response == CODE_COMMAND.ACK:
                    return True
                else:
                    raise BootloaderUnexpectedResponseException(f"Received unexpected response {response}")
            await asyncio.sleep(0.0001)

        raise BootloaderTimeoutException(f"Failed to get ACK Response")

    async def receive_multiple_ack(self, timeout=0.5):

        """ Reads for timeout seconds and expects only ACK responses, counts them and then returns the amount of
        acks returned

        If anything that isn't and ack is seen then it will throw an exception
        """
        start_time = time.perf_counter()
        ack_responses = 0
        while time.perf_counter() - start_time < timeout:
            response = self.connection.read_bytes(num_bytes=1)
            if response:
                if response == CODE_COMMAND.NACK:
                    raise BootloaderNACKException(f"Received NACK Response")

                if response == CODE_COMMAND.ACK:
                    ack_responses += 1
                else:
                    raise BootloaderUnexpectedResponseException(f"Received unexpected response {response}")
            await asyncio.sleep(0.001)
        return ack_responses

    async def start(self, timeout=0.2):
        self.connection.clear_bytes()
        self.connection.send_bytes(CODE_COMMAND.START)
        try:
            await self.receive_ack(timeout)
        except BootloaderException as e:
            raise e

        return True

    async def write_memory(self, start_address: int, data: bytes):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.WRITE_MEMORY
        address = start_address.to_bytes(4, byteorder='big')
        address += self.checksum(address)

        packet = bytes([len(data) -1])
        packet += data
        packet += self.checksum(packet)

        self.connection.send_bytes(cmd_id)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        self.connection.send_bytes(address)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        self.connection.send_bytes(packet)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        return True
        pass

    async def read_memory(self, start_address: int, size: int):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.READ_MEMORY
        address = start_address.to_bytes(4, byteorder='big')
        address += self.checksum(address)

        number_of_bytes = bytes([size-1])
        checksum_number_of_bytes = bytes([0xFF ^ number_of_bytes[0]])

        number_of_bytes += checksum_number_of_bytes

        self.connection.send_bytes(cmd_id)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        self.connection.send_bytes(address)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        self.connection.send_bytes(number_of_bytes)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        try:
            data = await self.receive_data(size)
        except BootloaderException as e:
            raise e
        return data

        pass

    async def erase_memory(self, sectors: List[int], attempts=3):
        self.connection.clear_bytes()

        cmd_id = ID_COMMAND.ERASE_MEMORY

        special_erase = (len(sectors) - 1).to_bytes(2, byteorder='big')
        packet = bytes(special_erase)
        for sector in sectors:
            packet += sector.to_bytes(2, byteorder='big')
        checksum = self.checksum(packet)

        packet += checksum

        for attempt in range(attempts):
            self.connection.send_bytes(cmd_id)
            try:
                await self.receive_ack(0.5)
            except BootloaderException as e:
                await asyncio.sleep(0.2)
                if attempt == attempts-1:
                    raise e
                continue

            self.connection.send_bytes(packet)
            try:
                await self.receive_ack(timeout=2.0)
            except BootloaderException as e:
                await asyncio.sleep(0.2)
                if attempt == attempts-1:
                    raise e
                continue
            break
        return True

    async def readout_unprotect(self):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.READOUT_UNPROTECT
        self.connection.send_bytes(cmd_id)

        # Receive 2 acks
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        return True

    async def readout_protect(self):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.READOUT_PROTECT
        self.connection.send_bytes(cmd_id)

        # Receive 2 acks
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        return True

    async def write_unprotect(self):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.WRITE_UNPROTECT
        self.connection.send_bytes(cmd_id)

        # Receive 2 acks
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        return True

    async def write_protect(self, sectors):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.WRITE_PROTECT

        number_of_sectors = (len(sectors) - 1). to_bytes(1, byteorder='big')
        packet = number_of_sectors

        for sector in sectors:
            packet += sector.to_bytes(1, byteorder='big')
        checksum = self.checksum(packet)
        packet += checksum

        self.connection.send_bytes(cmd_id)
        # Receive 2 acks
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        self.connection.send_bytes(packet)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        return True

    async def get(self):
        self.connection.clear_bytes()

        cmd_id = ID_COMMAND.GET

        self.connection.send_bytes(cmd_id)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        try:
            number_of_bytes = await self.receive_data(1)
        except BootloaderException as e:
            raise e

        try:
            command_codes = await self.receive_data(number_of_bytes + 1)
        except BootloaderException as e:
            raise e

        return command_codes
        pass

    async def get_version(self):
        self.connection.clear_bytes()

        cmd_id = ID_COMMAND.GET_VERSION

        self.connection.send_bytes(cmd_id)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        try:
            version = await self.receive_data(1)
        except BootloaderException as e:
            raise e
        pass

        try:
            dummy = await self.receive_data(2)
            await self.receive_ack()
        except BootloaderException as e:
            raise e
        return version

    async def go(self, start_address):
        self.connection.clear_bytes()

        cmd_id = ID_COMMAND.GO

        address = start_address.to_bytes(4, byteorder='big')

        address += self.checksum(address)

        self.connection.send_bytes(cmd_id)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        self.connection.send_bytes(address)
        try:
            await self.receive_ack()
        except BootloaderException as e:
            raise e

        return True

    async def passthrough(self, attempts=3):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.PASSTHROUGH

        for attempt in range(attempts):
            self.connection.send_bytes(cmd_id)
            try:
                await self.receive_ack(0.5)
                break
            except BootloaderException as e:
                await asyncio.sleep(0.2)
                if attempt == attempts-1:
                    raise e
        return True

    async def stop_passthrough(self, attempts=3):
        self.connection.clear_bytes()
        cmd_id = ID_COMMAND.STOP_PASSTHROUGH

        for attempt in range(attempts):
            self.connection.send_bytes(cmd_id)
            try:
                await self.receive_ack()
                break
            except BootloaderException as e:
                await asyncio.sleep(0.2)
                if attempt == attempts-1:
                    raise e
        return True

    async def select(self, device_id: int, attempts=5):
        """
        Attempt to select the device
        :param device_id:
        :param attempts:
        :return:
        """
        self.connection.clear_bytes()
        command = ID_COMMAND.SELECT
        command += device_id.to_bytes(1, byteorder='big')
        checksum = 0xFF - device_id

        command += checksum.to_bytes(1, byteorder='big')
        command += device_id.to_bytes(1, byteorder='big')
        command += checksum.to_bytes(1, byteorder='big')

        expected_ack = 1

        # Attempt to select device
        for attempt in range(attempts):
            self.connection.send_bytes(command)
            try:
                ack_count = await self.receive_multiple_ack()
                await asyncio.sleep(0.5)
                break
            except BootloaderException as e:
                await asyncio.sleep(0.2)
                if attempt == attempts-1:
                    raise e

        # Check that the correct number of ack received
        if ack_count == expected_ack:
            return True

        elif ack_count == 0:
            raise BootloaderTimeoutException("No ACK received")
        else:
            raise BootloaderMultipleACKException("Multiple ACKs received.")

    @staticmethod
    def get_sector(address):
        """
        Get the sector from the given address
        :param address:
        :return:
        """
        for i, name in enumerate(SECTORS):
            sector = SECTORS[name]
            if address < sector['Address']:
                return i - 1
        return -1

    async def erase_sectors_firmware(self, start_address, size, attempts=3):
        end_address = start_address + size - 1
        start_sector = self.get_sector(start_address)
        end_sector = self.get_sector(end_address)

        sector_codes = list(array('H', range(start_sector, end_sector + 1)))

        for attempt in range(attempts):
            try:
                await self.erase_memory(sector_codes)
                break
            except BootloaderException as e:
                if attempt == attempts-1:
                    raise e


class FirmwareWriter:
    write_progress = 0
    connection: CommConnection

    def __init__(self, bootloader: Bootloader):
        self.bootloader = bootloader
        self.connection = bootloader.connection

    async def write_firmware_HEX(self, firmware, retry_timeout: float = 0.0):
        self.write_progress = 0
        pipe_a, pipe_b = multiprocessing.Pipe()

        self.connection.close()

        await asyncio.sleep(0.1)

        conn = self.connection
        bytesize = conn.byte_size
        parity = conn.parity
        stopbits = conn.stop_bits

        if stopbits == serial.STOPBITS_ONE:
            stopbits = ftdidef.STOP_BITS_1
        elif stopbits == serial.STOPBITS_TWO:
            stopbits = ftdidef.STOP_BITS_2

        if parity == serial.PARITY_NONE:
            parity = ftdidef.PARITY_NONE
        elif parity == serial.PARITY_EVEN:
            parity = ftdidef.PARITY_EVEN
        elif parity == serial.PARITY_ODD:
            parity = ftdidef.PARITY_ODD
        elif parity == serial.PARITY_MARK:
            parity = ftdidef.PARITY_MARK
        elif parity == serial.PARITY_SPACE:
            parity = ftdidef.PARITY_SPACE

        if bytesize == serial.EIGHTBITS:
            bytesize = ftdidef.BITS_8
        elif bytesize == serial.SEVENBITS:
            bytesize = ftdidef.BITS_7

        params = [conn.serial_port, conn.baudrate, bytesize, parity, stopbits]
        process = multiprocessing.Process(target=write_firmware_HEX, args=(params, pipe_b, firmware, retry_timeout))
        process.start()
        while process.is_alive():

            msg = None
            await asyncio.sleep(0.05)
            while pipe_a.poll(0.0):
                msg = pipe_a.recv()
                if isinstance(msg, float):
                    self.write_progress = msg
                if isinstance(msg, Exception):
                    await asyncio.sleep(0.4)
                    print(__name__, "Came from process, attempting to reconnect.")
                    self.connection.connect()
                    await asyncio.sleep(0.4)
                    process.terminate()
                    process.join()
                    raise Exception(msg)
                if isinstance(msg, str):
                    await asyncio.sleep(0.4)
                    print(__name__, "Came from process, attempting to reconnect.")
                    self.connection.connect()
                    await asyncio.sleep(0.4)
                    process.terminate()
                    process.join()
                    return msg


def write_firmware_HEX(params, pipe: Connection, firmware, retry_timeout: float = 0.0):
    comm = SerialFTDIConnection(*params)
    connection = FTDIConnectionWrapper(comm)
    connection.connect()
    print("Multiprocessing write Connected")
    bootloader = NonAsyncBootloader(connection)

    HEX = HEX_Parse()
    print("Write in progress(0%)", end='')
    previous_progress = 0
    i = 0
    last_time_success: float = time.time()
    bootloader.state = BootloaderStatus.WRITE
    while i < len(firmware):
        print("\r" + "Write in progress({}%)".format(((i * 100) / len(firmware)) // 1), end='')
        if i % 5 == 0:
            bootloader.write_progress = ((i * 100) / len(firmware)) // 1

        pipe.send(float(bootloader.write_progress))
        try:
            state, address, data = HEX.parse_line(firmware[i])
            if state == HEX.PARSE_STATE['DATA']:
                bootloader.commands.write_memory(address, data)
                last_time_success = time.time()
                i += 1
                bootloader.state = BootloaderStatus.WRITE
            else:
                last_time_success = time.time()
                i += 1
        except Exception as e:
            bootloader.state = BootloaderStatus.WRITE_ERROR_PAUSE
            if bootloader.commands.bootloaderConnectionManager.connection:
                bootloader.commands.bootloaderConnectionManager.connection.close()
                time.sleep(0.1)
                bootloader.commands.bootloaderConnectionManager.connection.connect()
                time.sleep(0.1)
            if time.time() >= (last_time_success + retry_timeout):
                bootloader.state = BootloaderStatus.NOT_OPERATING
                pipe.send(e)
                comm.disconnect()
                time.sleep(0.1)
                raise e
            else:
                continue
        if previous_progress < int(bootloader.write_progress):
            previous_progress = int(bootloader.write_progress)
            # time.sleep(0.005)
    print("\r" + "Write in progress(100%) ")
    bootloader.write_progress = 100
    pipe.send(float(bootloader.write_progress))
    bootloader.state = BootloaderStatus.NOT_OPERATING
    pipe.send('Write firmware successful with ACK return')

    comm.disconnect()
    time.sleep(1.0)
    return 'Write firmware successful with ACK return'
