from RC_Core.update.bootloader.bootloader_commconnection import BootloaderCommconnection
import logging


class CODE_COMMAND(object):
    GET = b'\x00',  # Gets the version and the allowed commands supported by the current version of the bootloader
    GET_VERSION = b'\x01',  # Gets the bootloader version and the Read Protection status of the Flash memory
    GET_ID = b'\x02',  # Gets the chip ID
    READ_MEMORY = b'\x11',  # Reads up to 256 bytes of memory starting from an address specified by the application
    GO = b'\x21',  # Jumps to user application code located in the internal Flash memory or in SRAM
    WRITE_MEMORY = b'\x31',  # Writes up to 256 bytes to the RAM or Flash memory starting from an address specified by the application
    ERASE = b'\x43',  # Erases from one to all the Flash memory pages
    EXTENDED_ERASE = b'\x44',  # Erases from one to all the Flash memory pages using two byte addressing mode (available only for v3.0 usart bootloader versions and above).
    WRITE_PROTECT = b'\x63',  # Enables the write protection for some sectors
    WRITE_UNPROTECT = b'\x73',  # Disables the write protection for all Flash memory sectors
    READOUT_PROTECT = b'\x82',  # Enables the read protection
    READOUT_UNPROTECT = b'\x92',  # Disables the read protection
    XOR = b'\x00',  #
    ACK = b'\x79',  # ACK answer
    NACK = b'\x1F',  # NACK answer
    START = b'\x7F'


class ID_COMMAND(object):
    GET = bytes([0x00, 0xFF])
    GET_VERSION = bytes([0x00, 0xFE])
    GET_ID = bytes([0x02, 0xFD])
    READ_MEMORY = bytes([0x11, 0xEE])
    GO = bytes([0x21, 0xDE])
    WRITE_MEMORY = bytes([0x31, 0xCE])
    ERASE_MEMORY = bytes([0x44, 0xBB])
    EXTENDED_ERASE_MEMORY = bytes([0x44, 0xBB])
    WRITE_PROTECT = bytes([0x63, 0x9C])
    WRITE_UNPROTECT = bytes([0x73, 0x8C])
    READOUT_PROTECT = bytes([0x82, 0x7D])
    READOUT_UNPROTECT = bytes([0x92, 0x6D])
    PASSTHROUGH = bytes([0xA0, 0x5F])
    STOP_PASSTHROUGH = bytes([0xA1, 0x5E, 0xA1, 0x5E, 0xA1, 0x5E, 0xA1, 0x5E])
    SELECT = bytes([0xB3, 0x4C])


class BootloaderCommands(object):
    class BootloaderCommandsException(Warning):
        """Base class for connection related exceptions."""
        pass

    def __init__(self, connection, commands_log_level=logging.WARNING, connection_log_level=logging.WARNING):
        self.log = logging.getLogger("Bootloader Command")
        self.log.level = commands_log_level
        self.ID_COMMAND = ID_COMMAND()
        self.CODE_COMMAND = CODE_COMMAND()
        self.bootloaderConnectionManager = BootloaderCommconnection(connection, log_level=connection_log_level)

    def set_connection(self, connection):
        if connection:
            self.bootloaderConnectionManager.connection = connection

    @property
    def htonl(self, int):
        return int.to_bytes(4, byteorder='little')

    @property
    def htons(self, int):
        return int.to_bytes(2, byteorder='little')

    def checksum_calc(self, data):
        """ """
        if (not isinstance(data, bytes)):
            raise ValueError("not a valid data type: {!r}".format(type(data).__name__))

        result = 0
        for byte in data:
            result ^= byte
        return bytes([result])

    def send_data(self, step, data):
        """ """
        if (not isinstance(data, bytes)):
            raise ValueError("not a valid data type: {!r}".format(type(data).__name__))
        self.bootloaderConnectionManager.send_data(data)
        return True

    def receive_data(self, step, size):
        """ """
        try:
            data = self.bootloaderConnectionManager.receive_data(size)
        except Exception as e:
            raise self.BootloaderCommandsException("{}: {}".format(step, e))
        return data

    def receive_ACK(self, step):
        """ """
        try:
            code = self.receive_data(step, 1)
        except Exception as e:
            raise
        if (code == CODE_COMMAND.ACK[0]):
            return code
        if (code == CODE_COMMAND.NACK[0]):
            raise self.BootloaderCommandsException("{}: receive NACK".format(step))
        else:
            raise self.BootloaderCommandsException(
                "{}: receive not ACK: ".format(step) + "".join('{:02X} '.format(b) for b in code))
        return code

    def receive_multiple_ACK(self, step, expected_ACK_count, max_check=None):
        if max_check is None:
            max_check = expected_ACK_count + 2
        ACK_COUNT = 0
        for i in range(0, max_check):
            try:
                code = None
                size = 1
                code = self.bootloaderConnectionManager.connection.read_raw(size)
                if (len(code) != size):
                    continue
                if (code == CODE_COMMAND.ACK[0]):
                    ACK_COUNT += 1
                elif (code == CODE_COMMAND.NACK[0]):
                    raise self.BootloaderCommandsException("{}: receive NACK".format(step))
                else:
                    pass
            except self.bootloaderConnectionManager.readTimeoutError as e:
                pass
            except Exception as e:
                raise
        return ACK_COUNT

    def start(self):
        """ """
        try:
            self.send_data("send start code", CODE_COMMAND.START)
            self.receive_ACK("after send start code")
            return 'START successful with ACK return'
        except Exception as e:
            self.log.error("{}".format(e))
            raise

    def write_memory(self, start_address, data):
        """ """
        try:
            id = ID_COMMAND.WRITE_MEMORY
            address = start_address.to_bytes(4, byteorder='big')
            address += self.checksum_calc(address)
            packet = bytes([len(data) - 1])
            packet += data
            packet += self.checksum_calc(packet)

            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.send_data("send address", address)
            self.receive_ACK("after send address")
            self.send_data("send data", packet)
            self.receive_ACK("after send data")
        except Exception as e:
            self.log.error("ERROR WRITE MEMORY: address: {:08X}, data size: {}, {}".format(start_address, len(data), e))
            raise

    def read_memory(self, start_address, size):
        """ """
        try:
            id = ID_COMMAND.READ_MEMORY
            address = start_address.to_bytes(4, byteorder='big')
            address += self.checksum_calc(address)
            number_of_bytes = bytes([size - 1])
            checksum_number_of_bytes = bytes([0xFF ^ number_of_bytes[0]])
            number_of_bytes += checksum_number_of_bytes

            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.send_data("send address", address)
            self.receive_ACK("after send address")
            self.send_data("send number of bytes", number_of_bytes)
            self.receive_ACK("after send number of bytes")
            data = self.receive_data("receive data", size)

        except Exception as e:
            self.log.error("ERROR READ MEMORY: address: {:08X}, {}".format(start_address, e))
            raise
        return data

    def erase_memory(self, sectors):
        """ """
        try:
            id = ID_COMMAND.ERASE_MEMORY
            number_of_sectors = (len(sectors) - 1).to_bytes(2, byteorder='big')
            special_erase = (len(sectors) - 1).to_bytes(2, byteorder='big')
            packet = bytes(special_erase)
            for sector in sectors: packet += sector.to_bytes(2, byteorder='big')
            checksum = self.checksum_calc(packet)
            packet += checksum

            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.send_data("send sector codes", packet)
            self.receive_ACK("after send sector codes")

        except Exception as e:
            self.log.error("ERROR ERASE MEMORY: {}".format(e))
            raise

    def readout_unprotect(self):
        """ """
        try:
            id = ID_COMMAND.READOUT_UNPROTECT
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.receive_ACK("after readout unprotect")
            return 'Read unprotected successful with ACK return'
        except Exception as e:
            self.log.error("ERROR READOUT UNPROTECT: {}".format(e))
            raise

    def readout_protect(self):
        """ """
        try:
            id = ID_COMMAND.READOUT_PROTECT
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.receive_ACK("after readout protect")
            return 'Read protect successful with ACK return'
        except Exception as e:
            self.log.error("ERROR READOUT PROTECT: {}".format(e))
            raise

    def write_unprotect(self):
        """ """
        try:
            id = ID_COMMAND.WRITE_UNPROTECT
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.receive_ACK("after write unprotect")
            return 'Write unprotected successful with ACK return'
        except Exception as e:
            self.log.error("ERROR WRITE UNPROTECT: {}".format(e))
            raise

    def write_protect(self, sectors):
        """ """
        try:
            id = ID_COMMAND.WRITE_PROTECT
            number_of_sectors = (len(sectors) - 1).to_bytes(1, byteorder='big')
            packet = number_of_sectors
            for sector in sectors: packet += sector.to_bytes(1, byteorder='big')
            checksum = self.checksum_calc(packet)
            packet += checksum

            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.send_data("send sector codes", packet)
            self.receive_ACK("after send sector codes")

        except Exception as e:
            self.log.error("ERROR WRITE PROTECT| {}".format(e))
            raise

    def get(self):
        """ """
        try:
            id = ID_COMMAND.GET
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            number_of_bytes = self.receive_data("receive number of bytes", 1)
            command_codes = self.receive_data("receive supported command codes", number_of_bytes + 1)

        except Exception as e:
            self.log.error("ERROR GET| {}".format(e))
            raise
        return command_codes

    def get_version(self):
        try:
            id = ID_COMMAND.GET_VERSION
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            version = self.receive_data("receive number of bytes", 1)
        except Exception as e:
            self.log.error("ERROR GET| {}".format(e))
            raise
        try:
            dummy = self.receive_data("receive zero bytes", 2)
            self.receive_ACK("after receive bytes")
        except Exception as e:
            self.log.error("ERROR GET| {}".format(e))
        return version

    def go(self, start_address):
        """ """
        try:
            id = ID_COMMAND.GO
            address = start_address.to_bytes(4, byteorder='big')
            address += self.checksum_calc(address)
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            self.send_data("send address", address)
            self.receive_ACK("after send address")

        except Exception as e:
            self.log.error("ERROR GO| address: {:08X}| {}".format(start_address, e))
            raise

    def passthrough(self):
        """ Instruct the receiving board to pass all bytes in and out between two comms links """
        try:
            id = ID_COMMAND.PASSTHROUGH
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            return 'PASSTHROUGH successful with ACK return'
        except Exception as e:
            self.log.error("ERROR PASSTHROUGH| {}".format(e))
            raise

    def stop_passthrough(self):
        """ Instruct the receiving board to pass all bytes in and out between two comms links """
        try:
            id = ID_COMMAND.STOP_PASSTHROUGH
            self.send_data("send id", id)
            self.receive_ACK("after send id")
            return 'STOP PASSTHROUGH successful with ACK return'
        except Exception as e:
            self.log.error("ERROR STOP PASSTHROUGH| {}".format(e))
            raise

    def select(self, device_id):
        """ Set a device id that should respond to commands. All other device ids
            should disregard commands except Select commands
        """
        try:
            command = ID_COMMAND.SELECT
            command += device_id.to_bytes(1, byteorder='big')
            ch_sum = 0xFF - device_id
            command += ch_sum.to_bytes(1, byteorder='big')
            # repeat the device id and checksum bytes
            command += device_id.to_bytes(1, byteorder='big')
            command += ch_sum.to_bytes(1, byteorder='big')

            self.send_data("send command", command)
            expected_ACK_count = 1
            ACK_count = self.receive_multiple_ACK("after send id", expected_ACK_count)
            if ACK_count == expected_ACK_count:  # Correct ACKs
                return 1
            elif ACK_count == 0:  # No ACKS
                return 0
            else:  # Multiple ACKs could mean devices are sharing device_id
                return -1
        except Exception as e:
            self.log.error("ERROR SELECT| {}".format(e))
            raise
