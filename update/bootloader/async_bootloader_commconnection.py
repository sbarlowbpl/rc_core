import asyncio
import sys
import time
import logging
from copy import copy

import serial
import socket


class BootloaderCommconnection(object):
    class BootloaderCommconnectionException(Warning):
        pass

    writeTimeoutError = BootloaderCommconnectionException('write timeout')
    readTimeoutError = BootloaderCommconnectionException('read timeout')
    noConnectionError = BootloaderCommconnectionException('No connection or connection is disconnected')
    connection = None

    """Abstract class that implements basically physical layer with connection
    """
    def __init__(self, connection, log_level=logging.WARNING):
        self.connection = connection
        self.log = logging.getLogger("Bootloader Connection")
        self.log.level = log_level

    def send_data(self, data):
        """Send bytes"""
        self.log.debug(" send data: " + "".join('{:02X} '.format(b) for b in data))
        # print(__name__, "send_data():", " send data: " + "".join('{:02X} '.format(b) for b in data))
        try:
            if self.connection.connected:
                self.connection.clear_bytes()
                self.connection.send_freq_queue = bytearray([])
                self.connection.send_raw(data)
        except serial.SerialTimeoutException:
            self.connection.close()
            raise self.writeTimeoutError
        except Exception as e:
            self.connection.close()
            print(__name__, "send_data():", "Error code:", e)
            raise self.noConnectionError

    async def receive_data(self, size):
        """ Read up to num bytes (timeout)
        :returns: the raw data
        """
        if (size == 0):
            raise self.BootloaderCommconnectionException("Error Receive Data: size = 0")
        timeout = time.time() + 3.0
        try:
            data = self.data
            self.data = b''
        except:
            data = b''

        i = 0
        while time.time() < timeout:
            print("__asyncbootloader, read_raw")

            new_data = copy(self.connection.read_raw(size))

            # if (timeout - time.time()) > (0.01 * i):
            #     i += 1
            #     print("running async loop")
            await asyncio.sleep(0.00001)
            # loop = asyncio.get_event_loop()
            # # loop.run_until_complete()
            # task = asyncio.create_task(asyncio.sleep(0.0))
            # asyncio.gather(task)
            if new_data:
                data += new_data
            if len(data) >= size:
                break
        if (len(data) == 0):
            self.log.debug(" receive data: " + "could not receive data")
            raise self.readTimeoutError
        elif (len(data) < size):
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
            raise self.readTimeoutError
        elif (len(data) > size):
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
            self.data = data[size:]
        else:
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
        return data[:size]

    def receive_data_no_async(self, size):
        """ Read up to num bytes (timeout)
        :returns: the raw data
        """
        if (size == 0):
            raise self.BootloaderCommconnectionException("Error Receive Data: size = 0")
        timeout = time.time() + 3.0
        try:
            data = self.data
            self.data = b''
        except:
            data = b''

        while time.time() < timeout:
            print("__asyncbootloader, read_raw")
            new_data = self.connection.read_raw(size)
            if new_data:
                data += new_data
            if len(data) >= size:
                break
        if (len(data) == 0):
            self.log.debug(" receive data: " + "could not receive data")
            raise self.readTimeoutError
        elif (len(data) < size):
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
            raise self.readTimeoutError
        elif (len(data) > size):
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
            self.data = data[size:]
        else:
            self.log.debug(" receive data: " + "".join('{:02X} '.format(b) for b in data))
        return data[:size]

    def error(self, e):
        self.connection.close()
        raise self.noConnectionError


