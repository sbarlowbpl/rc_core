import logging
import os
import time
from array import *
from enum import Enum

from RC_Core.update.bootloader.bootloader_commands import BootloaderCommands
from RC_Core.update.bootloader.bootloader_serial import BootloaderSerial

SECTORS = {
        'Sector 0': {'Address': 0x08000000, 'Size': 16*1024}, # Sector 0, 16 Kbytes
        'Sector 1': {'Address': 0x08004000, 'Size': 16*1024}, # Sector 1, 16 Kbytes
        'Sector 2': {'Address': 0x08008000, 'Size': 16*1024}, # Sector 2, 16 Kbytes
        'Sector 3': {'Address': 0x0800C000, 'Size': 16*1024}, # Sector 3, 16 Kbytes
        'Sector 4': {'Address': 0x08010000, 'Size': 64*1024}, # Sector 4, 64 Kbytes
        'Sector 5': {'Address': 0x08020000, 'Size': 128*1024}, # Sector 5, 128 Kbytes
        'Sector 6': {'Address': 0x08040000, 'Size': 128*1024}, # Sector 6, 128 Kbytes
        'Sector 7': {'Address': 0x08060000, 'Size': 128*1024}, # Sector 7, 128 Kbytes
        'Sector 8': {'Address': 0x08080000, 'Size': 128*1024}, # Sector 8, 128 Kbytes
        'Sector 9': {'Address': 0x080A0000, 'Size': 128*1024}, # Sector 9, 128 Kbytes
        'Sector 10': {'Address': 0x080C0000, 'Size': 128*1024}, # Sector 10, 128 Kbytes
        'Sector 11': {'Address': 0x080E0000, 'Size': 128*1024}, # Sector 11, 128 Kbytes
    }

OTP_BLOCKS = {
    'Block 0':  {'Address': 0x1FFF7800}, # OTP Block 0, 32 bytes
    'Block 1':  {'Address': 0x1FFF7820}, # OTP Block 1, 32 bytes
    'Block 2':  {'Address': 0x1FFF7840}, # OTP Block 2, 32 bytes
    'Block 3':  {'Address': 0x1FFF7860}, # OTP Block 3, 32 bytes
    'Block 4':  {'Address': 0x1FFF7880}, # OTP Block 4, 32 bytes
    'Block 5':  {'Address': 0x1FFF78A0}, # OTP Block 5, 32 bytes
    'Block 6':  {'Address': 0x1FFF78C0}, # OTP Block 6, 32 bytes
    'Block 7':  {'Address': 0x1FFF78E0}, # OTP Block 7, 32 bytes
    'Block 8':  {'Address': 0x1FFF7900}, # OTP Block 8, 32 bytes
    'Block 9':  {'Address': 0x1FFF7920}, # OTP Block 9, 32 bytes
    'Block 10': {'Address': 0x1FFF7940}, # OTP Block 10, 32 bytes
    'Block 11': {'Address': 0x1FFF7960}, # OTP Block 11, 32 bytes
    'Block 12': {'Address': 0x1FFF7980}, # OTP Block 12, 32 bytes
    'Block 13': {'Address': 0x1FFF79A0}, # OTP Block 13, 32 bytes
    'Block 14': {'Address': 0x1FFF79C0}, # OTP Block 14, 32 bytes
    'Block 15': {'Address': 0x1FFF79E0}, # OTP Block 15, 32 bytes
    'Lock 0':   {'Address': 0x1FFF7A00}, # OTP Lock byte, 1 byte
    'Lock 1':   {'Address': 0x1FFF7A04}, # OTP Lock byte, 1 byte
    'Lock 2':   {'Address': 0x1FFF7A08}, # OTP Lock byte, 1 byte
    'Lock 3':   {'Address': 0x1FFF7A0C}, # OTP Lock byte, 1 byte
    'Lock 4':   {'Address': 0x1FFF7A10}, # OTP Lock byte, 1 byte
    'Lock 5':   {'Address': 0x1FFF7A14}, # OTP Lock byte, 1 byte
    'Lock 6':   {'Address': 0x1FFF7A18}, # OTP Lock byte, 1 byte
    'Lock 7':   {'Address': 0x1FFF7A1C}, # OTP Lock byte, 1 byte
    'Lock 8':   {'Address': 0x1FFF7A20}, # OTP Lock byte, 1 byte
    'Lock 9':   {'Address': 0x1FFF7A24}, # OTP Lock byte, 1 byte
    'Lock 10':  {'Address': 0x1FFF7A28}, # OTP Lock byte, 1 byte
    'Lock 11':  {'Address': 0x1FFF7A2C}, # OTP Lock byte, 1 byte
    'Lock 12':  {'Address': 0x1FFF7A30}, # OTP Lock byte, 1 byte
    'Lock 13':  {'Address': 0x1FFF7A34}, # OTP Lock byte, 1 byte
    'Lock 14':  {'Address': 0x1FFF7A38}, # OTP Lock byte, 1 byte
    'Lock 15':  {'Address': 0x1FFF7A3C}, # OTP Lock byte, 1 byte
}



class HEX_Parse(object):
    
    class RECORD_TYPE(object):
        DATA = "00"
        END_OF_FILE = "01"
        EXTENDED_SEGMENT_ADDRESS = "02"
        EXTENDED_LINEAR_ADDRESS = "04"
        START_LINEAR_ADDRESS = "05"

    PARSE_STATE = {
            'READY':0,
            'DATA':1,
            'END_OF_FILE':2,
            'EXTENDED_SEGMENT_ADDRESS':3,
            'EXTENDED_LINEAR_ADDRESS':4,
            'START_LINEAR_ADDRESS':5,
            'ERROR':6,
        }

    class HEXParseException(Warning):
        pass


    def __init__(self):
        self.address_data_located = 0
        self.address_extended_segment = 0
        self.address_extended_linear = 0
        self.address_start_firmware = 0
        self.address_memory_data_located = 0
        self.number_of_data_bytes = 0
        self.checksum = 0
        self.record_type = bytes()
        self.data = bytes()
        self.state = self.PARSE_STATE['READY']

    def parse_line(self, line):
        self.data = bytearray()
        len_line = len(line)
        if len_line < 13:
            raise self.HEXParseException("Error parse HEX line:\n" + line.decode('utf-8') + "\n" + "Line size is {}".format(len_line))
        if chr(line[0]) != ':':
            raise self.HEXParseException("Error parse HEX line:\n" + line.decode('utf-8') + "\n" + line[0] + "!= ':'")
        if chr(line[-1]) != '\n':
            raise self.HEXParseException("Error parse HEX line:\n" + line.decode('utf-8') + "\n" + line[-1] + "!= '\n'")
        if chr(line[-2]) != '\r':
            raise self.HEXParseException("Error parse HEX line:\n" + line.decode('utf-8') + "\n" + line[-2] + "!= '\r'")

        self.number_of_data_bytes = int(line[1:3],16)
        self.address_data_located = int(line[3:7],16)
        self.record_type = line[7:9]
        position = 9

        record_type = self.record_type.decode("utf-8")
        if record_type == self.RECORD_TYPE.DATA:
            self.data = bytearray(self.number_of_data_bytes)
            for i in range(self.number_of_data_bytes):
                self.data[i] = int(line[position : position + 2], 16)
                position += 2
            if self.address_extended_segment == 0:
                self.address_memory_data_located = (self.address_extended_linear << 16) | self.address_data_located
            else:
                self.address_memory_data_located = (self.address_extended_segment << 1) | self.address_data_located
            self.state = self.PARSE_STATE['DATA']

        elif record_type == self.RECORD_TYPE.END_OF_FILE:
            self.state = self.PARSE_STATE['END_OF_FILE']

        elif record_type == self.RECORD_TYPE.EXTENDED_SEGMENT_ADDRESS:
            self.address_extended_segment = int(line[position : position + 4], 16)
            position += 4
            self.state = self.PARSE_STATE['EXTENDED_SEGMENT_ADDRESS']

        elif record_type == self.RECORD_TYPE.EXTENDED_LINEAR_ADDRESS:
            self.address_extended_linear = int(line[position : position + 4], 16)
            self.address_extended_segment = 0
            position += 4
            self.state = self.PARSE_STATE['EXTENDED_LINEAR_ADDRESS']

        elif record_type == self.RECORD_TYPE.START_LINEAR_ADDRESS:
            self.address_start_firmware = int(line[position : position + 8], 16)
            position += 8
            self.state = self.PARSE_STATE['START_LINEAR_ADDRESS']

        else:
            self.state = self.PARSE_STATE['ERROR']
            raise self.HEXParseException("Error parse HEX line:\n" + line.decode('utf-8') + "  Record type: " + record_type)

        self.checksum = int(line[position : position + 2], 16)

        checksum = 0
        for i in range(1,position,2):
            checksum += int(line[i : i + 2], 16)
        checksum = ((~checksum) & 0xFF) + 0x01
        if checksum > 255: checksum -= 256
        checksum = checksum.to_bytes(1, byteorder = 'little')[0]

        if self.checksum != checksum:
            self.state = self.PARSE_STATE['ERROR']
            raise self.HEXParseException("Error parse HEX line:\n" + line.decode('utf-8') + "Checksum: {:02X} != {:02X}".format(self.checksum,checksum))

        return self.state, self.address_memory_data_located, self.data


class BootloaderStatus(Enum):
    NOT_OPERATING = 0
    WRITE = 1
    WRITE_ERROR_PAUSE = 2
    VERIFY = 3
    VERIFY_ERROR_PAUSE = 3
    READ_UNPROTECT = 4
    WRITE_UNPROTECT = 5
    READ_PROTECT = 6
    WRITE_PROTECT = 7




class NonAsyncBootloader(object):

    operation_string = ''
    status_string = ''
    write_progress = 0
    state = BootloaderStatus.NOT_OPERATING
    verify_progress = 0

    class BootloaderException(Warning):
        pass

    def debug_print(self, mode, mess):
            print (mess)

    def __init__(self, connection,
                    bootloader_log_level = logging.WARNING,
                    commands_log_level = logging.WARNING,
                    connection_log_level = logging.WARNING):

        self.log = logging.getLogger("Bootloader Core")
        self.log.level = bootloader_log_level
        self.commands = BootloaderCommands(connection,
                                           commands_log_level = commands_log_level,
                                           connection_log_level = connection_log_level)
        self.sectors = SECTORS

    def find_files_in_folder(self, path, extension, subFolders = True):
        """  Recursive function to find all files of an extension type in a folder (and optionally in all subfolders too)

        path:        Base directory to find files
        extension:   File extension to find
        subFolders:  Bool.  If True, find files in all subfolders under path. If False, only searches files in the specified folder
        """
        path_list = []
        try:   # Trapping a OSError
            for entry in os.scandir(path):
                if entry.is_file() and entry.path.endswith(extension):
                    path_list.append(entry.path)
            for entry in os.scandir(path):
                if entry.is_dir() and subFolders:   # if its a directory, then repeat process as a nested function
                    path_list += self.find_files_in_folder(entry.path, extension, subFolders)

        except OSError:
            print('Cannot access ' + path +'. Probably a permissions error')

        return path_list

    def open_file(self, path = "", choice = None):
        """ """
        dir_name = os.path.dirname(os.path.abspath(__file__))
        print("DIR PATH: " + dir_name)
        extension = ".bin"
        path_list = self.find_files_in_folder(dir_name, extension)
        extension = ".hex"
        path_list += self.find_files_in_folder(dir_name, extension)
        if not path_list:
            raise self.BootloaderException("No files in folder " + dir_name)

        file_list = []
        for path in path_list:
            file_list.append(path.split(dir_name)[1])

        if (choice == None):
            print("Select a file:")
            for i, fname in enumerate(file_list, 1):
                print("[{}]: {}".format(i, fname))
            choice = int(input("Enter a selection number: "))

        #choice = 3    
        choice -= 1  # correcting for python's 0-indexing
        print("You have chosen", file_list[choice])
        format = file_list[choice].split(".")[1]
        file = open(dir_name + file_list[choice], "rb") # opening for reading as binary
        if  format == "bin":
            data = file.read()
        else:
            data = file.readlines()
        file.close()
        return data, format

    def open_file_direct(self, filepath=None):
        if filepath is None or filepath == '':
            return None, None
        try:
            format = filepath.split(".")[1]
            file = open(filepath, "rb")  # opening for reading as binary
            if format == "bin":
                data = file.read()
            else:
                data = file.readlines()
            file.close()
            return data, format
        except:
            return None, None

    def connect(self, port, timeout=3):
        """Open serial port and send 'start' code"""

        # Open default of FTDI usb-uart chipset
        serial = self.commands.serial
        try:
            serial.open_port(port, timeout=timeout)
            print(serial.serial)
            return 'Connected to serial port: ' + port
        except serial.BootloaderSerialException:
            # Revert to pyserial if usart device is not FTDI
            self.commands.serial = BootloaderSerial()
            serial = self.commands.serial
            serial.open_port(port, timeout=timeout)
            if serial.serial.is_open:
                return 'Reverted to pyserial but port is open: ' + port
            else:
                return 'Failed to open port: ' + port
        except Exception as e:
            raise
        self.commands.start()

    def get_sector(self, address):
        """ """
        for i, name in enumerate(self.sectors):
            sector = self.sectors[name]
            if(address < sector['Address']):
                return (i - 1)
        return -1

    def select_sector(self, choice = None):
        """ """
        print("Select a sector:")
        for i, name in enumerate(self.sectors):
            sector = self.sectors[name]
            print("[{}]: {}, {:08X}, {}K".format(i, name, sector['Address'], sector['Size']//1024))
        if (choice == None):
            choice = int(input("Enter a sector number: "))
        name = "Sector {}".format(choice)
        sector = self.sectors[name]
        print("You have chosen {} ({:08X}, {}K)".format(name, sector['Address'], sector['Size']//1024))
        return name

    def enable_write_protection_firmware(self, start_address, size):
        """ """
        end_address = start_address + (size - 1)
        start_sector = self.get_sector(start_address)
        end_sector = self.get_sector(end_address)
        number_of_sectors = (end_sector - start_sector) + 1
        sector_codes = array('H', range(start_sector, end_sector + 1))
        try:
            self.commands.write_protect(sector_codes)
            return 'Write protect successful with ACK return'
        except Exception as e:
            raise

    def erase_sectors_firmware(self, start_address, size):
        """ """
        end_address = start_address + (size - 1)
        start_sector = self.get_sector(start_address)
        end_sector = self.get_sector(end_address)
        number_of_sectors = (end_sector - start_sector) + 1
        sector_codes = array('H', range(start_sector, end_sector + 1))
        self.log.debug("ERASE SECTORS")
        self.log.debug("Sectors: " + "".join('{} '.format(b) for b in sector_codes))
        try:
            self.commands.erase_memory(sector_codes)
            return 'Erase sectors successful with ACK return'
        except Exception as e:
            raise

    def check_HEX(self, firmware):
        """ """
        start_address = 0
        end_address = 0
        size = 0
        HEX = HEX_Parse()
        lines = len(firmware)
        start = False

        print("Check HEX file in progress(0%)", end='')

        for i in range(lines):           
            try:
                state, address, data = HEX.parse_line(firmware[i])
            except Exception as e:
                raise
            print("\r" + "Check HEX file in progress({}%)".format(((i*100)/lines)//1), end='')
            if ((start == False) and (state == HEX.PARSE_STATE['DATA'])):
                start_address = address
                start = True
            #elif ((i == 2) and (state != HEX.PARSE_STATE['DATA'])):
            #    raise self.BootloaderException("check firmware HEX: wrong first data line '" + firmware[2] + "'")
            if (state == HEX.PARSE_STATE['DATA']):
                end_address = address + len(data)
        #54216 - bin
        size = end_address - start_address
        print("\r" + "Check HEX file in progress(100%) ")
        return start_address, end_address, size

    def write_firmware_HEX(self, firmware, retry_timeout: float=0.0):
        """ """
        HEX = HEX_Parse()
        print("Write in progress(0%)", end='')
        previous_progress = 0
        i = 0
        last_time_success: float = time.time()
        self.state = BootloaderStatus.WRITE
        while i < len(firmware):
        # for i in range(len(firmware)):
            print("\r" + "Write in progress({}%)".format(((i*100)/len(firmware))//1), end='')
            self.write_progress = ((i*100)/len(firmware))//1
            try:
                state, address, data = HEX.parse_line(firmware[i])
                if (state == HEX.PARSE_STATE['DATA']):
                    self.commands.write_memory(address, data)
                    last_time_success = time.time()
                    i += 1
                    self.state = BootloaderStatus.WRITE
                else:
                    last_time_success = time.time()
                    i += 1
            except Exception as e:
                self.state = BootloaderStatus.WRITE_ERROR_PAUSE
                if self.commands.bootloaderConnectionManager.connection:
                    self.commands.bootloaderConnectionManager.connection.close()
                    time.sleep(0.1)
                    self.commands.bootloaderConnectionManager.connection.connect()
                    time.sleep(0.1)
                if time.time() >= (last_time_success + retry_timeout):
                    self.state = BootloaderStatus.NOT_OPERATING
                    raise
                else:
                    continue
            if previous_progress < int(self.write_progress):
                previous_progress = int(self.write_progress)
                # time.sleep(0.005)
        print("\r" + "Write in progress(100%) ")
        self.write_progress = 100
        self.state = BootloaderStatus.NOT_OPERATING
        return 'Write firmware successful with ACK return'

    def write_firmware_BIN(self, firmware, start_address, size):
        """ """
        curr_address = start_address
        curr_size = size
        print("Write in progress(0%)", end='')
        while (curr_size > 0):
            print("\r" + "Write in progress({}%)".format(100 - ((curr_size*100)/size)//1), end='')
            packet_size = 256
            if (curr_size < 256): 
                packet_size = curr_size
            try:
                start = curr_address - start_address
                end = start + packet_size
                self.commands.write_memory(curr_address, firmware[start:end])
            except Exception as e: raise
            curr_address += packet_size
            curr_size -= packet_size
        print("\r" + "Write in progress(100%) ")

    def verify_firmware_HEX(self, firmware):
        """ """
        HEX = HEX_Parse()

        print("Verify in progress(0%)", end='')
        self.state = BootloaderStatus.VERIFY
        for i in range(len(firmware)):
            print("\r" + "Verify in progress({}%)".format(((i*100)/len(firmware))//1), end='')
            self.verify_progress = ((i*100)/len(firmware))//1
            try:
                record_type, address, data = HEX.parse_line(firmware[i])
                if (record_type == HEX.PARSE_STATE['DATA']):
                    rx_data = self.commands.read_memory(address, len(data))
                    if (data != rx_data):
                        self.state = BootloaderStatus.NOT_OPERATING
                        raise self.BootloaderException("verify firmware: wrong data, address {:08X}, size {}".format(address, len(data)))
            except Exception as e:
                self.state = BootloaderStatus.NOT_OPERATING
                raise

        print("\r" + "Verify in progress(100%) ")
        self.verify_progress = 100
        self.state = BootloaderStatus.NOT_OPERATING
        return 'Verify firmware successful with ACK return'

    def verify_firmware_BIN(self, firmware, start_address, size):
        """ """
        curr_address = start_address
        curr_size = size
        print("Verify in progress(0%)", end='')
        while (curr_size > 0):
            print("\r" + "Verify in progress({}%)".format(100 - ((curr_size*100)/size)//1), end='')
            self.verify_progress = 100 - ((curr_size*100)/size)//1
            packet_size = 256
            if (curr_size < 256): 
                packet_size = curr_size
            try:
                block = self.commands.read_memory(curr_address, packet_size)
            except Exception as e: raise

            start = curr_address - start_address
            end = start + packet_size
            if (firmware[start : end] != block):
                raise self.BootloaderException("verify firmware: wrong data, address {:08X}, size {}".format(curr_address, packet_size))

            curr_address += packet_size
            curr_size -= packet_size
        print("\r" + "Verify in progress(100%) ")

    def upload_firmware_HEX(self, firmware, enable_write_protection = False, enable_redout_protection = False):
        """ """       
        try:
            start_address, end_address, size = self.check_HEX(firmware)
            print("UPLOAD FIRMWARE HEX")
            print("Address: {:08X}, Size: {}".format(start_address, size))
            self.commands.start()
            self.commands.readout_unprotect()
            self.commands.write_unprotect()
            self.erase_sectors_firmware(start_address, size)
            self.write_firmware_HEX(firmware)
            self.verify_firmware_HEX(firmware)
            if(enable_write_protection):
                self.enable_write_protection_firmware(start_address, size)
            if(enable_redout_protection):
                self.commands.readout_protect()
            self.commands.go(start_address)
            print("UPLOAD FIRMWARE OK")
        except Exception as e:
            raise self.BootloaderException("upload firnware: {}".format(e))

    def upload_firmware_BIN(self, firmware, enable_write_protection = False, enable_redout_protection = False):
        """ """
        size = len(firmware)
        sector_start = self.select_sector()
        start_address = self.sectors[sector_start]['Address']
        try:
            print("UPLOAD FIRMWARE BIN")
            print("Address: {:08X}, Size: {}".format(start_address, size))
            self.commands.start()
            self.commands.readout_unprotect()
            self.commands.write_unprotect()
            self.erase_sectors_firmware(start_address, size)
            self.write_firmware_BIN(firmware, start_address, size)
            self.verify_firmware_BIN(firmware, start_address, size)
            if(enable_write_protection):
                self.enable_write_protection_firmware(start_address, size)
            if(enable_redout_protection):
                self.commands.readout_protect()
            self.commands.go(start_address)
            print("UPLOAD FIRMWARE OK")
        except Exception as e:
            raise self.BootloaderException("upload firnware: {}".format(e))

    def upload_firmware(self, filepath=None, enable_write_protection = False, enable_redout_protection = False):
        """ """
        # firmware, format = self.open_file()
        firmware, format = self.open_file_direct(filepath)
        if format == "bin":
            self.upload_firmware_BIN(firmware, enable_write_protection, enable_redout_protection)
        elif format == "hex":
            self.upload_firmware_HEX(firmware, enable_write_protection, enable_redout_protection)
        else:
            print(__name__, 'upload_firmware():', 'Invalid file or format. Skipping bootloader')

    def set_device_id(self, device_id):
        sector = SECTORS["Sector 3"]
        address = sector["Address"]
        try:
            self.erase_sectors_firmware(address, 1)
            data = bytearray([device_id])
            self.commands.write_memory(address, data)
            return "SET DEVICE ID success"
        except Exception as e:
            self.log.error("ERROR SET DEVICE ID| {}".format(e))
            raise

    def set_otp(self, electrical_rev_maj: int, electrical_rev_submaj: int, electrical_rev_min: int):
        """
        Write to the OTP (one time programmable) memory.
        Format:
            1. uint32_t time_stamp. Convert time.time() to uint32_t.
            2. 3x uint8_t = [rev_maj, rev_submaj, rev_min]

        Total: 7 bytes.

        :param electrical_rev_maj:
        :param electrical_rev_submaj:
        :param electrical_rev_min:
        :return:
        """
        time_stamp_bytes = int(time.time()).to_bytes(4, byteorder='big')
        elec_rev_bytes = bytearray([electrical_rev_maj, electrical_rev_submaj, electrical_rev_min])
        data = time_stamp_bytes + elec_rev_bytes

        sector = OTP_BLOCKS["Block 0"]
        address = sector["Address"]

        try:
            self.commands.write_memory(address, data)
            return "SET OTP success"
        except Exception as e:
            self.log.error("ERROR SET OTP| {}".format(e))
            raise


def main():
    bootloader = NonAsyncBootloader()
    try:
        bootloader.upload_firmware()
    except Exception as e:
        print("BOOTLOADER ERROR:", e)


if __name__ == '__main__':
    main()
