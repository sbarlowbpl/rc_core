import asyncio
import datetime
import os, sys, time
import threading

from RC_Core import loadsave
from RC_Core.RS1_hardware import PacketID, get_name_of_packet_id
from RC_Core.devicemanager.product import ProductAlpha5, RSProduct, ProductType
from RC_Core.packetid import Packets

DATALOGGER_FILENAME = "datalogger_settings.json"

MAX_FREQUENCY = 5

class DataLogging():
    instances = []
    logging_now = False

    def __init__(self, **kwargs):
        DataLogging.instances.append(self)
        self.log_file = None
        self.enable_logger = False
        self.packet_list_as_csv_format = ''
        self.folder_path = ''
        self.file_name = ''
        self.mode = 'All'
        self.products = []
        self.packet_ids = {}
        self.log_frequency = MAX_FREQUENCY
        self.remote_log_frequency = MAX_FREQUENCY
        self.packets_obj = Packets()
        self.last_packet = [0,0]
        self.log_all_packets = False
        asyncio.create_task(self.logging_loop())
        # self.log_thread = threading.Thread(target=self.logging_loop)
        # self.log_thread.setDaemon(True)
        # self.log_thread.start()

    @staticmethod
    def load_settings_from_json_file():
        loaded_datalogger_settings = loadsave.get_settings_object(DATALOGGER_FILENAME)
        if loaded_datalogger_settings:
            for datalogger_settings in loaded_datalogger_settings:
                datalogger_instance = DataLogging()
                try:
                    products_list = []
                    mode = ''
                    if len(datalogger_settings['products']) > 0:
                        product_name = datalogger_settings['products'][0]
                        if product_name == 'All':
                            mode = 'All'
                            for product in ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR, ProductType.ALTERNATIVE]):
                                products_list.append(product)
                        elif product_name == 'All Arms':
                            mode = 'All Arms'
                            for product in ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR]):
                                products_list.append(product)
                        else:
                            product = RSProduct.get_product_with_name(product_name)
                            if product:
                                products_list.append(product)
                        if len(products_list) < 1:
                            mode = 'All'
                            for product in ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR, ProductType.ALTERNATIVE]):
                                products_list.append(product)
                    datalogger_instance.load_values(products=products_list,
                                                     mode=mode,
                                                     packet_ids=datalogger_settings['packet_ids'],
                                                     file_name=datalogger_settings['file_name'],
                                                     folder_path=datalogger_settings['folder_path'],
                                                     enable=datalogger_settings['state'],
                                                     log_all_packets=datalogger_settings['log_all_packets'],
                                                     log_frequency=datalogger_settings['log_frequency'])
                except:
                    DataLogging.instances.remove(datalogger_instance)
        if DataLogging.instances is None or len(DataLogging.instances) < 1:
            datalogger_instance = DataLogging()
            datalogger_instance.load_default_values()

    def load_values(self, products=[], mode='', packet_ids=[], file_name='', folder_path='', enable=False,
                    log_all_packets=True, log_frequency=5):
        self.mode = mode
        for product in products:
            self.add_product(product)
        for packet in packet_ids:
            self.add_packet_id(packet[0], packet[1])
        self.file_name = file_name
        self.folder_path = folder_path
        if enable:
            self.toggle_datalogger()
        self.log_all_packets = log_all_packets
        self.log_frequency = log_frequency

    def load_default_values(self):
        if len(ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR])):
            self.add_product(ProductType.get_instances_from_type([ProductType.RS1_ACTUATOR, ProductType.RS2_ACTUATOR])[0])
        default_packet_ids_list = [[PacketID.KM_END_POS, False], [PacketID.POSITION, False], [PacketID.VELOCITY, False],
                           [PacketID.CURRENT, False], [PacketID.MODE, False]]
        for packet in default_packet_ids_list:
            self.add_packet_id(packet[0], packet[1])
        self.file_name = 'ReachControlLog'
        self.folder_path = ''
        self.log_all_packets = True

    @staticmethod
    def toggle_all_dataloggers():
        if DataLogging.instances:
            DataLogging.logging_now = not DataLogging.logging_now
        else:
            DataLogging.logging_now = False

    @staticmethod
    def save_all_dataloggers_to_json_file():
        datalogger_save_settings_list = []
        for datalogger in DataLogging.instances:
            product_names_list = []
            if datalogger.mode == 'All':
                product_names_list.append('All')
            elif datalogger.mode == 'All Arms':
                product_names_list.append('All Arms')
            else:
                for product in datalogger.products:
                    product_names_list.append(product.name)

            datalogger_save_settings_list.append({'state': datalogger.enable_logger,
                                    'file_name': datalogger.file_name,
                                    'folder_path': datalogger.folder_path,
                                    'products': product_names_list,
                                    'packet_ids': list(datalogger.packet_ids.items()),
                                    'log_all_packets': datalogger.log_all_packets,
                                    'log_frequency': datalogger.log_frequency
                                    })
        loadsave.save_settings(datalogger_save_settings_list, DATALOGGER_FILENAME)

    def toggle_datalogger(self):
        file_open_error_log = None
        if self.enable_logger:
            self.enable_logger = False
            self.close_file()
        else:
            if self.folder_path and self.file_name:
                self.log_file = None
                self.enable_logger = True
            else:
                if not self.folder_path:
                    file_open_error_log = 'No folder chosen'
                elif not self.file_name:
                    file_open_error_log = 'No filename chosen'
                self.enable_logger = False
                self.close_file()
        DataLogging.save_all_dataloggers_to_json_file()
        return file_open_error_log

    def open_file(self):
        file_open_error_log = None
        if self.folder_path and self.file_name:
            try:
                full_path = self.format_log_file_name()
                self.log_file = open(full_path, 'w+')
                if self.log_file:
                    self.log_file.write('Time(h:m:s),Product_Name,Device_ID,Packet_Name,Packet_ID,' +
                                        'Data[0],Data[1],Data[2],Data[3],Data[4],Data[5],Data[6],Data[7],' +
                                        'Data[8],Data[9],Data[10],Data[11],Data[12],Data[13],Data[14],Data[15]\n')
            except Exception as e:
                print(__name__, 'toggle_datalogger():', e)
                self.enable_logger = False
                self.close_file()
                file_open_error_log = e
        return file_open_error_log

    def force_stop_datalogger(self):
        self.enable_logger = False
        self.close_file()

    def format_log_file_name(self):
        full_path = self.folder_path + self.file_name
        tnow = datetime.datetime.now()
        date_string = self.format_date_string(tnow)
        fullstop_location = full_path.rfind('.')
        extension = '.csv'
        if fullstop_location > 0:
            extension = full_path[fullstop_location:]
            full_path = full_path[:fullstop_location]
        full_path += date_string
        full_path += extension
        return full_path

    def format_date_string(self, tnow):
        date_string = '_' + str(tnow.year) + '{:02d}'.format(tnow.month) + '{:02d}'.format(tnow.day) + '_' + \
                      '{:02d}'.format(tnow.hour) + '{:02d}'.format(tnow.minute) + '{:02d}'.format(tnow.second)
        return date_string

    def set_folder_path(self, folder_path):
        self.folder_path = folder_path
        if self.folder_path[-1] != '/':
            self.folder_path += '/'

    def add_product(self, product):
        if product not in self.products:
            # for packet_id in self.packet_ids:
            #     for device_id in product.get_active_device_ids():
            #         product.set_permanent_callback(device_id, packet_id, self.packet_callback)
            self.products.append(product)

    def remove_product(self, product):
        if product in self.products:
            # for packet_id in self.packet_ids:
            #     for device_id in product.get_active_device_ids():
            #         product.remove_permanent_callback(device_id, packet_id, self.packet_callback)
            self.products.remove(product)

    def remove_all_products(self):
        self.products = []

    def add_packet_id(self, packet_id, enabled=True):
        if packet_id in self.packet_ids:
            # if packet[0] == packet_id:
            return

        self.packet_ids[packet_id] = enabled

    def remove_packet_id(self, packet_id):
        if packet_id in self.packet_ids:
            #if packet[0] == packet_id:
            del self.packet_ids[packet_id]


    def enable_packet_id(self, packet_id):

        if packet_id in self.packet_ids:
            self.packet_ids[packet_id] = True

    def disable_packet_id(self, packet_id):
        if packet_id in self.packet_ids:
            self.packet_ids[packet_id] = False

    def packet_callback_with_product(self, device_id, packet_id, data, product):
        if self.enable_logger and DataLogging.logging_now:
            try:
                if self.log_file is None:
                    self.open_file()
                if [device_id, packet_id, data] == self.last_packet:
                    return
                self.last_packet = [device_id, packet_id, data]
                tnow = datetime.datetime.now()
                date_string = str(tnow.hour) + ':' + str(tnow.minute) + ':' + \
                              str(tnow.second + tnow.microsecond/1000000)
                packet_name = get_name_of_packet_id(packet_id)
                product_name = product.name
                # print(__name__, 'packet_callback():', 'Packet:', date_string, device_id, packet_name, packet_id, data)
                packet_str_to_record = date_string + ',' + product_name + ',' + str(device_id) + ',' + packet_name + ',' + str(packet_id)
                if type(data)==list or type(data)==tuple or type(data)==bytes or type(data)==bytearray:
                    for data_elem in data:
                        packet_str_to_record += ','
                        packet_str_to_record += str(data_elem)
                else:
                    packet_str_to_record += ','
                    packet_str_to_record += str(data)
                packet_str_to_record += '\n'
                self.packet_list_as_csv_format += packet_str_to_record
            except Exception as e:
                print(__name__, 'packet_callback():', e)
                self.close_file()

    def packet_callback(self, device_id, packet_id, data):
        if self.enable_logger and DataLogging.logging_now:
            try:
                if self.log_file is None:
                    self.open_file()
                if [device_id, packet_id, data] == self.last_packet:
                    return
                self.last_packet = [device_id, packet_id, data]
                tnow = datetime.datetime.now()
                date_string = str(tnow.hour) + ':' + str(tnow.minute) + ':' + \
                              str(tnow.second + tnow.microsecond/1000000)
                packet_name = get_name_of_packet_id(packet_id)
                product_name = ''
                for product in self.products:
                    if device_id in product.get_active_device_ids():
                        product_name = product.name
                # print(__name__, 'packet_callback():', 'Packet:', date_string, device_id, packet_name, packet_id, data)
                packet_str_to_record = date_string + ',' + product_name + ',' + str(device_id) + ',' + packet_name + ',' + str(packet_id)
                if type(data)==list or type(data)==tuple or type(data)==bytes or type(data)==bytearray:
                    for data_elem in data:
                        packet_str_to_record += ','
                        packet_str_to_record += str(data_elem)
                else:
                    packet_str_to_record += ','
                    packet_str_to_record += str(data)
                packet_str_to_record += '\n'
                self.packet_list_as_csv_format += packet_str_to_record
            except Exception as e:
                print(__name__, 'packet_callback():', e)
                self.close_file()

    async def logging_loop(self):
        while True:
            self.logging_refresh()
            await asyncio.sleep(0.5)

    def logging_refresh(self, *dt):
        if self.enable_logger and DataLogging.logging_now and self.log_file:
            self.log_file.write(self.packet_list_as_csv_format)
            self.packet_list_as_csv_format = ''
            self.log_file.flush()
            os.fsync(self.log_file.fileno())
        else:
            self.packet_list_as_csv_format = ''

    def request_all_at_frequency(self, *dt):
        if self.enable_logger and DataLogging.logging_now and not self.log_all_packets:
            for product in self.products:
                if product.connection and product.connection.connected:
                    for packet_id, enabled in self.packet_ids.items():
                        if enabled:
                            packet_proto = self.packets_obj.get_by_id(packet_id)
                            if packet_proto:
                                for device_id in product.get_active_device_ids():
                                    if device_id != product.get_active_device_ids()[-1]:
                                        if packet_id >= 0xA0 and packet_id <= 0xDF:
                                            continue
                                    packet_proto.request(product.connection, device_id)
                                    # time.sleep(0.02)
        else:
            self.packet_list_as_csv_format = ''

    def close_file(self):
        self.packet_list_as_csv_format = ''
        if self.log_file:
            self.log_file.close()
            self.log_file = None

    '''OVERRIDE THIS IF REMOTE OPERATOR'''
    def remote_kill(self, datalogger_instance):
        pass

    def remove_self(self):
        self.packet_list_as_csv_format = ''
        DataLogging.instances.remove(self)
        self.close_file()
        self.remote_kill(self)

